package com.montages.mcore.qvto;

import java.util.Date;

import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;

public class DateUtil {

	@Operation(contextual = true)
	public static String timestampNowAsString(Object self) {
		Date now = new Date();
		return EcoreFactory.eINSTANCE.convertToString(
				EcorePackage.eINSTANCE.getEDate(), now);
	}

	// This does not work: UMLReflection does not accept string's
	// @Operation(contextual = true)
	// public static Date timestampNow(Object self) {
	// return new Date();
	// }

	@Operation(contextual = true)
	public static Object timestampNowAsObject(Object self) {
		return new Date();
	}
}
