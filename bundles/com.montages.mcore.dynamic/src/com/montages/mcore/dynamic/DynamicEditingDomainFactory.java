package com.montages.mcore.dynamic;

import java.util.HashMap;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.semantics.provider.SemanticsItemProviderAdapterFactory;

import com.montages.common.resource.CustomResourceLocator;
import com.montages.mcore.McorePackage;

public class DynamicEditingDomainFactory {

	public AdapterFactoryEditingDomain createEditingDomain() {

		AdapterFactoryEditingDomain editingDomain = new AdapterFactoryEditingDomain( //
				createAdapterFactory(), //
				new BasicCommandStack(), //
				new HashMap<>());

		editingDomain.getResourceSet().getPackageRegistry().put(McorePackage.eNS_URI, McorePackage.eINSTANCE);
		editingDomain.getResourceSet().getPackageRegistry().put(EditorConfigPackage.eNS_URI,
				EditorConfigPackage.eINSTANCE);

		new CustomResourceLocator((ResourceSetImpl) editingDomain.getResourceSet());

		return editingDomain;
	}

	private ComposedAdapterFactory createAdapterFactory() {

		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		adapterFactory.addAdapterFactory(new DynamicItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new SemanticsItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new EcoreItemProviderAdapterFactory());

		return adapterFactory;
	}

}
