package com.montages.mcore.dynamic;

import static com.montages.common.McoreUtil.stream;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.xocl.delegates.XOCLAnnotationAdapter;

import com.montages.common.resource.CustomResourceLocator;
import com.montages.mcore.MComponent;
import com.montages.mcore.McorePackage;
import com.montages.mcore.util.ResourceService;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.PathUtils;

public class DynamicSetup {

	private final XOCLAnnotationAdapter annotationAdapter = new XOCLAnnotationAdapter();

	private AdapterFactoryEditingDomain domain;
	private URI componentURI;
	private URI instanceURI;
	private final Set<URI> ecores = new HashSet<>();

	public DynamicSetup() {
	}

	public DynamicSetup withDomain(AdapterFactoryEditingDomain domain) {
		this.domain = domain;
		return this;
	}

	public DynamicSetup withComponent(URI uri) {
		this.componentURI = uri;
		return this;
	}

	public DynamicSetup withPackages(URI... uris) {
		this.ecores.addAll(Arrays.asList(uris));
		return this;
	}

	public DynamicSetup withInstance(URI uri) {
		this.instanceURI = uri;
		return this;
	}

	public Resource setup() {
		if (instanceURI == null) {
			throw new IllegalArgumentException("Invalid instance uri");
		}

		MComponent component = null;
		if (componentURI != null) {
			try {
				component = loadComponent(componentURI);
			} catch (Exception e) {
				throw new RuntimeException("Cannot load component from uri " + componentURI);
			}
		}

		final Set<URI> packages;
		if (component != null) {
			packages = getReferencedPackages(component.eResource());
		} else {
			packages = ecores;
		}

		if (packages == null || packages.isEmpty()) {
			throw new IllegalStateException("No ecore packages found");
		}

		registerEcorePackages(component, packages);

		return domain.getResourceSet().getResource(instanceURI, true);
	}

	private MComponent loadComponent(URI uri) {
		McoreEditingDomainFactory factory = McoreEditingDomainFactory.getInstance();
		String id = PathUtils.buildDomainID(uri.trimFragment());

		McoreDiagramEditingDoamin domain = factory.exists(id) ? factory.createEditingDomain(id, new Object()) : null;

		ResourceSet resourceSet;
		if (domain != null) {
			resourceSet = domain.getResourceSet();
		} else {
			resourceSet = ResourceService.createResourceSet();
			new CustomResourceLocator((ResourceSetImpl) resourceSet);
		}

		return (MComponent) resourceSet.getResource(uri.trimFragment(), true).getContents().get(0);
	}

	private Set<URI> getReferencedPackages(Resource component) {
		return stream(component.getAllContents()) //
				.filter(e -> McorePackage.Literals.MPACKAGE.isInstance(e)) //
				.map(e -> e.eGet(McorePackage.Literals.MPACKAGE__INTERNAL_EPACKAGE)) //
				.filter(e -> e instanceof EPackage) //
				.map(e -> (EPackage) e) //
				.filter(e -> e.eResource() != null) //
				.map(pack -> pack.eResource().getURI()) //
				.collect(Collectors.toSet());
	}

	private void registerEcorePackages(MComponent component, Collection<URI> externalPackages) {
		externalPackages.forEach(uri -> domain.getResourceSet().getResource(uri, true));

		// Make sure all ecore packages are loaded
		EcoreUtil.resolveAll(domain.getResourceSet());

		List<ResourceSet> resourceSets = component == null ? singletonList(domain.getResourceSet())
				: asList(component.eResource().getResourceSet(), domain.getResourceSet());

		// For each top package
		domain.getResourceSet().getResources().stream() //
				.flatMap(res -> stream(res.getAllContents())) //
				.filter(obj -> obj instanceof EPackage) //
				.map(obj -> (EPackage) obj) //
				.forEach(pack -> {

					annotationAdapter.adapt(pack);

					// We are now registering factories and setting up URI mappings for each ecore
					// packages we found. This has to be done in the resourceSet that we use for the
					// dynamic editor and the resourceSet used in the MCore editor. This latter is
					// needed because that way the Ecore2MCore converter can correctly load
					// the instance files.

					resourceSets.forEach(resourceSet -> {

						resourceSet.getPackageRegistry().put(pack.getNsURI(), pack);

						resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
								.put(pack.getName().toLowerCase(), new DynamicResourceFactory());
					});
				});
	}

}
