package com.montages.mcore.dynamic;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

public class DynamicResourceFactory extends XMLResourceFactoryImpl {

	@Override
	public Resource createResource(URI uri) {
		XMLResource.XMLMap xmlMap = new XMLMapImpl();
		xmlMap.setIDAttributeName("_uuid");

		XMLResource resource = new XMLResourceImpl(uri) {
			@Override
			protected boolean useUUIDs() {
				return true;
			}
		};

		resource.getDefaultSaveOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		resource.getDefaultLoadOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		return resource;
	}

}