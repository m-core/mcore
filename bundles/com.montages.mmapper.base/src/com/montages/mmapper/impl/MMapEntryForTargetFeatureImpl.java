/**
 */

package com.montages.mmapper.impl;

import com.montages.acore.AComponent;
import com.montages.acore.APackage;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.AProperty;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mmapper.MMap;
import com.montages.mmapper.MMapEntryForTargetFeature;
import com.montages.mmapper.MmapperPackage;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractExpression;
import com.montages.mrules.expressions.MCollectionExpression;
import com.montages.mrules.expressions.MProcessorDefinition;

import com.montages.mrules.impl.MRuleAnnotationImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MMap Entry For Target Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl#getATargetFeature <em>ATarget Feature</em>}</li>
 *   <li>{@link com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl#getMExplicitlyUsedForContainmentTargetFeature <em>MExplicitly Used For Containment Target Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MMapEntryForTargetFeatureImpl extends MRuleAnnotationImpl implements MMapEntryForTargetFeature {
	/**
	 * The cached value of the '{@link #getATargetFeature() <em>ATarget Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetFeature()
	 * @generated
	 * @ordered
	 */
	protected AFeature aTargetFeature;

	/**
	 * This is true if the ATarget Feature reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTargetFeatureESet;

	/**
	 * The cached value of the '{@link #getMExplicitlyUsedForContainmentTargetFeature() <em>MExplicitly Used For Containment Target Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMExplicitlyUsedForContainmentTargetFeature()
	 * @generated
	 * @ordered
	 */
	protected EList<MMap> mExplicitlyUsedForContainmentTargetFeature;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getATargetFeature <em>ATarget Feature</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetFeature
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aTargetFeatureChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingComponent <em>AContaining Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingComponent
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aContainingComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATPackage <em>AT Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackage
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATClassifier <em>AT Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATFeature <em>AT Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeature
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATCoreAStringClass <em>AT Core AString Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATCoreAStringClass
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTCoreAStringClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMandatory <em>AMandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASingular <em>ASingular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATypeLabel <em>AType Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTypeLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aUndefinedTypeConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsCode <em>As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollector <em>Collector</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollector
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression collectorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAnnotation <em>Containing Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression containingAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingExpression <em>Containing Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression containingExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsSubExpression <em>Is Sub Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isSubExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASelfObjectType <em>ASelf Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASelfObjectType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSelfObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASelfObjectPackage <em>ASelf Object Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASelfObjectPackage
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSelfObjectPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATargetObjectType <em>ATarget Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetObjectType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTargetObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATargetSimpleType <em>ATarget Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aTargetSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAObjectObjectType <em>AObject Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAObjectObjectType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aObjectObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnType <em>AExpected Return Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aExpectedReturnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnSimpleType <em>AExpected Return Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aExpectedReturnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isReturnValueMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueSingular <em>Is Return Value Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isReturnValueSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseAsCode <em>Base As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression baseAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainAsCode <em>Chain As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChainCalculatedType <em>AChain Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChainCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aChainCalculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChainCalculatedSimpleType <em>AChain Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChainCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aChainCalculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedSingular <em>Chain Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainCalculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getProcessorDefinition <em>Processor Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorDefinition
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression processorDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTypeMismatch <em>Type Mismatch</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeMismatch
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression typeMismatchDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCodeforSubchains <em>Chain Codefor Subchains</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCodeforSubchains
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainCodeforSubchainsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsOwnXOCLOp <em>Is Own XOCL Op</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsOwnXOCLOp
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isOwnXOCLOpDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotated <em>AAnnotated</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotated
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aAnnotatedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotating <em>AAnnotating</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotating
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aAnnotatingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnTypeOfAnnotation <em>AExpected Return Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aExpectedReturnTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnSimpleTypeOfAnnotation <em>AExpected Return Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnSimpleTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aExpectedReturnSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAElement1 <em>AElement1</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement1
	 * @templateTag DFGFI08
	 * @generated
	 */
	private static OCLExpression aElement1ChoiceConstructionOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MMapEntryForTargetFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getATargetFeature() {
		if (aTargetFeature != null && aTargetFeature.eIsProxy()) {
			InternalEObject oldATargetFeature = (InternalEObject) aTargetFeature;
			aTargetFeature = (AFeature) eResolveProxy(oldATargetFeature);
			if (aTargetFeature != oldATargetFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE, oldATargetFeature,
							aTargetFeature));
			}
		}
		return aTargetFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetATargetFeature() {
		return aTargetFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATargetFeature(AFeature newATargetFeature) {
		AFeature oldATargetFeature = aTargetFeature;
		aTargetFeature = newATargetFeature;
		boolean oldATargetFeatureESet = aTargetFeatureESet;
		aTargetFeatureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE, oldATargetFeature, aTargetFeature,
					!oldATargetFeatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATargetFeature() {
		AFeature oldATargetFeature = aTargetFeature;
		boolean oldATargetFeatureESet = aTargetFeatureESet;
		aTargetFeature = null;
		aTargetFeatureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE, oldATargetFeature, null,
					oldATargetFeatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATargetFeature() {
		return aTargetFeatureESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>ATarget Feature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<AFeature>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let chain: OrderedSet(acore::classifiers::AProperty)  = if aTargetObjectType.oclIsUndefined()
	then OrderedSet{}
	else aTargetObjectType.aAllProperty()
	endif in
	chain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() 
	else r endif)
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<AFeature> evalATargetFeatureChoiceConstruction(List<AFeature> choice) {
		EClass eClass = MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE;
		if (aTargetFeatureChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aTargetFeatureChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, choiceConstruction, helper.getProblems(),
						eClass, "ATargetFeatureChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aTargetFeatureChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, "ATargetFeatureChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<AFeature> result = new ArrayList<AFeature>((Collection<AFeature>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MMap> getMExplicitlyUsedForContainmentTargetFeature() {
		if (mExplicitlyUsedForContainmentTargetFeature == null) {
			mExplicitlyUsedForContainmentTargetFeature = new EObjectContainmentEList.Unsettable.Resolving<MMap>(
					MMap.class, this,
					MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE);
		}
		return mExplicitlyUsedForContainmentTargetFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMExplicitlyUsedForContainmentTargetFeature() {
		if (mExplicitlyUsedForContainmentTargetFeature != null)
			((InternalEList.Unsettable<?>) mExplicitlyUsedForContainmentTargetFeature).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMExplicitlyUsedForContainmentTargetFeature() {
		return mExplicitlyUsedForContainmentTargetFeature != null
				&& ((InternalEList.Unsettable<?>) mExplicitlyUsedForContainmentTargetFeature).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE:
			return ((InternalEList<?>) getMExplicitlyUsedForContainmentTargetFeature()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE:
			if (resolve)
				return getATargetFeature();
			return basicGetATargetFeature();
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE:
			return getMExplicitlyUsedForContainmentTargetFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE:
			setATargetFeature((AFeature) newValue);
			return;
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE:
			getMExplicitlyUsedForContainmentTargetFeature().clear();
			getMExplicitlyUsedForContainmentTargetFeature().addAll((Collection<? extends MMap>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE:
			unsetATargetFeature();
			return;
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE:
			unsetMExplicitlyUsedForContainmentTargetFeature();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE:
			return isSetATargetFeature();
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE:
			return isSetMExplicitlyUsedForContainmentTargetFeature();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aContainingComponent null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AComponent basicGetAContainingComponent() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT;

		if (aContainingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aContainingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AComponent) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTPackage null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public APackage basicGetATPackage() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE;

		if (aTPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTClassifier null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetATClassifier() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER;

		if (aTClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTFeature null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AFeature basicGetATFeature() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__AT_FEATURE;

		if (aTFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AFeature) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTCoreAStringClass null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetATCoreAStringClass() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__AT_CORE_ASTRING_CLASS;

		if (aTCoreAStringClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTCoreAStringClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTCoreAStringClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aClassifier null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAClassifier() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aMandatory null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getAMandatory() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__AMANDATORY;

		if (aMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aSingular null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getASingular() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ASINGULAR;

		if (aSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTypeLabel null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getATypeLabel() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ATYPE_LABEL;

		if (aTypeLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTypeLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTypeLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aUndefinedTypeConstant null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAUndefinedTypeConstant() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__AUNDEFINED_TYPE_CONSTANT;

		if (aUndefinedTypeConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aUndefinedTypeConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedTypeConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asCode null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsCode() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE;

		if (asCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL collector null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MCollectionExpression basicGetCollector() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR;

		if (collectorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				collectorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MCollectionExpression) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL containingAnnotation self
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MRuleAnnotation basicGetContainingAnnotation() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION;

		if (containingAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				containingAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MRuleAnnotation) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL containingExpression null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MAbstractExpression basicGetContainingExpression() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION;

		if (containingExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				containingExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MAbstractExpression) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isComplexExpression null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsComplexExpression() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isSubExpression null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsSubExpression() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION;

		if (isSubExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isSubExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isSubExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aSelfObjectType eContainer().oclAsType(MMap).aSource
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassType basicGetASelfObjectType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASELF_OBJECT_TYPE;

		if (aSelfObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSelfObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSelfObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aSelfObjectPackage if aSelfObjectType.oclIsUndefined()
	then null
	else aSelfObjectType.aContainingPackage
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public APackage basicGetASelfObjectPackage() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASELF_OBJECT_PACKAGE;

		if (aSelfObjectPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSelfObjectPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSelfObjectPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTargetObjectType eContainer().oclAsType(MMap).aTarget
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassType basicGetATargetObjectType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ATARGET_OBJECT_TYPE;

		if (aTargetObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTargetObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTargetObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aTargetSimpleType acore::classifiers::ASimpleType::None
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getATargetSimpleType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ATARGET_SIMPLE_TYPE;

		if (aTargetSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aTargetSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTargetSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aObjectObjectType null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAObjectObjectType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AOBJECT_OBJECT_TYPE;

		if (aObjectObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aObjectObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aObjectObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aExpectedReturnType null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAExpectedReturnType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_TYPE;

		if (aExpectedReturnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aExpectedReturnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aExpectedReturnSimpleType null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getAExpectedReturnSimpleType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_SIMPLE_TYPE;

		if (aExpectedReturnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aExpectedReturnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isReturnValueMandatory null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsReturnValueMandatory() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY;

		if (isReturnValueMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isReturnValueMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isReturnValueSingular null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsReturnValueSingular() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR;

		if (isReturnValueSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isReturnValueSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL baseAsCode null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getBaseAsCode() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE;

		if (baseAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				baseAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL chainAsCode null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getChainAsCode() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE;

		if (chainAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aChainCalculatedType null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAChainCalculatedType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_CALCULATED_TYPE;

		if (aChainCalculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aChainCalculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChainCalculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aChainCalculatedSimpleType null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getAChainCalculatedSimpleType() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE;

		if (aChainCalculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aChainCalculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChainCalculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL chainCalculatedSingular null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getChainCalculatedSingular() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;

		if (chainCalculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainCalculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL processorDefinition null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProcessorDefinition basicGetProcessorDefinition() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;

		if (processorDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				processorDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(processorDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProcessorDefinition) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL typeMismatch null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getTypeMismatch() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MBASE_CHAIN__TYPE_MISMATCH;

		if (typeMismatchDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				typeMismatchDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(typeMismatchDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL chainCodeforSubchains null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getChainCodeforSubchains() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS;

		if (chainCodeforSubchainsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainCodeforSubchainsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCodeforSubchainsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isOwnXOCLOp null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsOwnXOCLOp() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MBASE_CHAIN__IS_OWN_XOCL_OP;

		if (isOwnXOCLOpDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isOwnXOCLOpDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isOwnXOCLOpDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aAnnotated null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AAnnotatable basicGetAAnnotated() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATED;

		if (aAnnotatedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aAnnotatedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AAnnotatable) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aAnnotating null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AAnnotation basicGetAAnnotating() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATING;

		if (aAnnotatingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aAnnotatingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AAnnotation) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aExpectedReturnTypeOfAnnotation null
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAExpectedReturnTypeOfAnnotation() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION;

		if (aExpectedReturnTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aExpectedReturnTypeOfAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aExpectedReturnSimpleTypeOfAnnotation null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getAExpectedReturnSimpleTypeOfAnnotation() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

		if (aExpectedReturnSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aExpectedReturnSimpleTypeOfAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isReturnValueOfAnnotationMandatory null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsReturnValueOfAnnotationMandatory() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY;

		if (isReturnValueOfAnnotationMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isReturnValueOfAnnotationMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueOfAnnotationMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isReturnValueOfAnnotationSingular null
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsReturnValueOfAnnotationSingular() {
		EClass eClass = (MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR;

		if (isReturnValueOfAnnotationSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isReturnValueOfAnnotationSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueOfAnnotationSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AElement1</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<AProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aElement1 
	let annotatedProp: acore::classifiers::AProperty = 
	self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)
	in
	if self.aChainEntryType.oclIsUndefined() then 
	Sequence{} else
	self.aChainEntryType.aAllProperty()->asSequence()
	endif
	
	--
	 * @templateTag INS06
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<AProperty> evalAElement1ChoiceConstruction(List<AProperty> choice) {
		EClass eClass = MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE;
		EStructuralFeature eOverrideStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT1;
		if (aElement1ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			//the actual class.... TODO: is this the right one
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eOverrideStructuralFeature,
					eClass());

			try {
				aElement1ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(MmapperPackage.PLUGIN_ID, choiceConstruction, helper.getProblems(),
						eClass, eOverrideStructuralFeature);
			}
		}
		Query query = OCL_ENV.createQuery(aElement1ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(MmapperPackage.PLUGIN_ID, query, eClass, eOverrideStructuralFeature);
			query.getEvaluationEnvironment().add("choice", choice);
			return (ArrayList<AProperty>) query.evaluate(this);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}
} //MMapEntryForTargetFeatureImpl
