/**
 */
package com.montages.mmapper.impl;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.mmapper.MMap;
import com.montages.mmapper.MMapEntryForTargetFeature;
import com.montages.mmapper.MMapping;
import com.montages.mmapper.MmapperFactory;
import com.montages.mmapper.MmapperPackage;

import com.montages.mrules.MrulesPackage;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.xocl.core.util.XoclErrorHandler;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MmapperPackageImpl extends EPackageImpl implements MmapperPackage {
	/**
	 * The cached OCL constraint restricting the classes of root elements.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails().get("rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID, choiceConstraint, helper.getProblems(),
						MmapperPackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mMappingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mMapEntryForTargetFeatureEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mmapper.MmapperPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MmapperPackageImpl() {
		super(eNS_URI, MmapperFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MmapperPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MmapperPackage init() {
		if (isInited)
			return (MmapperPackage) EPackage.Registry.INSTANCE.getEPackage(MmapperPackage.eNS_URI);

		// Obtain or create and register package
		MmapperPackageImpl theMmapperPackage = (MmapperPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof MmapperPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new MmapperPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MrulesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theMmapperPackage.createPackageContents();

		// Initialize created meta-data
		theMmapperPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMmapperPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MmapperPackage.eNS_URI, theMmapperPackage);
		return theMmapperPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMMapping() {
		return mMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMMapping_MOwnedMap() {
		return (EReference) mMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMMapping_Mname() {
		return (EAttribute) mMappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMMap() {
		return mMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMMap_ASource() {
		return (EReference) mMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMMap_ATarget() {
		return (EReference) mMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMMap_MMapEntryForTargetFeature() {
		return (EReference) mMapEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMMapEntryForTargetFeature() {
		return mMapEntryForTargetFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMMapEntryForTargetFeature_ATargetFeature() {
		return (EReference) mMapEntryForTargetFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMMapEntryForTargetFeature_MExplicitlyUsedForContainmentTargetFeature() {
		return (EReference) mMapEntryForTargetFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmapperFactory getMmapperFactory() {
		return (MmapperFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mMappingEClass = createEClass(MMAPPING);
		createEReference(mMappingEClass, MMAPPING__MOWNED_MAP);
		createEAttribute(mMappingEClass, MMAPPING__MNAME);

		mMapEClass = createEClass(MMAP);
		createEReference(mMapEClass, MMAP__ASOURCE);
		createEReference(mMapEClass, MMAP__ATARGET);
		createEReference(mMapEClass, MMAP__MMAP_ENTRY_FOR_TARGET_FEATURE);

		mMapEntryForTargetFeatureEClass = createEClass(MMAP_ENTRY_FOR_TARGET_FEATURE);
		createEReference(mMapEntryForTargetFeatureEClass, MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE);
		createEReference(mMapEntryForTargetFeatureEClass,
				MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);
		MrulesPackage theMrulesPackage = (MrulesPackage) EPackage.Registry.INSTANCE.getEPackage(MrulesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mMapEntryForTargetFeatureEClass.getESuperTypes().add(theMrulesPackage.getMRuleAnnotation());

		// Initialize classes, features, and operations; add parameters
		initEClass(mMappingEClass, MMapping.class, "MMapping", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMMapping_MOwnedMap(), this.getMMap(), null, "mOwnedMap", null, 0, -1, MMapping.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMMapping_Mname(), ecorePackage.getEString(), "mname", null, 0, 1, MMapping.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mMapEClass, MMap.class, "MMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMMap_ASource(), theClassifiersPackage.getAClassType(), null, "aSource", null, 0, 1,
				MMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMMap_ATarget(), theClassifiersPackage.getAClassType(), null, "aTarget", null, 0, 1,
				MMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMMap_MMapEntryForTargetFeature(), this.getMMapEntryForTargetFeature(), null,
				"mMapEntryForTargetFeature", null, 0, -1, MMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mMapEntryForTargetFeatureEClass, MMapEntryForTargetFeature.class, "MMapEntryForTargetFeature",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMMapEntryForTargetFeature_ATargetFeature(), theClassifiersPackage.getAFeature(), null,
				"aTargetFeature", null, 0, 1, MMapEntryForTargetFeature.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMMapEntryForTargetFeature_MExplicitlyUsedForContainmentTargetFeature(), this.getMMap(), null,
				"mExplicitlyUsedForContainmentTargetFeature", null, 0, -1, MMapEntryForTargetFeature.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source, new String[] { "rootConstraint", "trg.name = \'MMapping\'" });
		addAnnotation(getMMap_ASource(), source, new String[] { "initValue",
				"let subchain1 : mmapper::MMapEntryForTargetFeature = let chain: ecore::EObject = eContainer() in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(mmapper::MMapEntryForTargetFeature)\n    then chain.oclAsType(mmapper::MMapEntryForTargetFeature)\n    else null\n  endif\n  endif in \n if subchain1 = null \n  then null \n else subchain1.aSrcObjectType.oclAsType(acore::classifiers::AClassType) endif \n\n" });
		addAnnotation(getMMap_ATarget(), source, new String[] { "initValue",
				"if self.eContainer().oclIsTypeOf(MMapEntryForTargetFeature)\nthen \nlet classType : acore::classifiers::AClassType =\neContainer().oclAsType(MMapEntryForTargetFeature).aTargetFeature.aClassifier.oclAsType(acore::classifiers::AClassType)\nin\nif (classType.oclIsUndefined()) then null else classType endif\nelse null\nendif" });
		addAnnotation(getMMapEntryForTargetFeature_ATargetFeature(), source, new String[] { "choiceConstruction",
				"let chain: OrderedSet(acore::classifiers::AProperty)  = if aTargetObjectType.oclIsUndefined()\n  then OrderedSet{}\n  else aTargetObjectType.aAllProperty()\nendif in\nchain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() \n else r endif)\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true", "uuidAttributeName", "_uuid" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source, new String[] { "hideAdvancedProperties", "null" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(mMappingEClass, source, new String[] { "mName", "Mapping" });
		addAnnotation(getMMapping_MOwnedMap(), source, new String[] { "mName", "OwnedMap" });
		addAnnotation(getMMapping_Mname(), source, new String[] { "mName", "name" });
		addAnnotation(mMapEClass, source, new String[] { "mName", "Map" });
		addAnnotation(getMMap_ASource(), source, new String[] { "mName", "Source" });
		addAnnotation(getMMap_ATarget(), source, new String[] { "mName", "Target" });
		addAnnotation(mMapEntryForTargetFeatureEClass, source,
				new String[] { "mName", "Map Entry For Target Feature" });
		addAnnotation(getMMapEntryForTargetFeature_ATargetFeature(), source,
				new String[] { "mName", "Target Feature" });
		addAnnotation(getMMapEntryForTargetFeature_MExplicitlyUsedForContainmentTargetFeature(), source,
				new String[] { "mName", "Explicitly Used For Containment Target Feature" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mMapEntryForTargetFeatureEClass, source, new String[] { "aContainingComponentDerive", "null\n",
				"aTPackageDerive", "null\n", "aTClassifierDerive", "null\n", "aTFeatureDerive", "null\n",
				"aTCoreAStringClassDerive", "null\n", "aClassifierDerive", "null\n", "aMandatoryDerive", "null\n",
				"aSingularDerive", "null\n", "aTypeLabelDerive", "null\n", "aUndefinedTypeConstantDerive", "null\n",
				"asCodeDerive", "null\n", "asBasicCodeDerive", "null\n", "collectorDerive", "null\n",
				"containingAnnotationDerive", "self\n", "containingExpressionDerive", "null\n",
				"isComplexExpressionDerive", "null\n", "isSubExpressionDerive", "null\n", "aCalculatedOwnTypeDerive",
				"null\n", "calculatedOwnMandatoryDerive", "null\n", "calculatedOwnSingularDerive", "null\n",
				"aCalculatedOwnSimpleTypeDerive", "null\n", "aSelfObjectTypeDerive",
				"eContainer().oclAsType(MMap).aSource\n", "aSelfObjectPackageDerive",
				"if aSelfObjectType.oclIsUndefined()\n  then null\n  else aSelfObjectType.aContainingPackage\nendif\n",
				"aTargetObjectTypeDerive", "eContainer().oclAsType(MMap).aTarget\n", "aTargetSimpleTypeDerive",
				"acore::classifiers::ASimpleType::None\n", "aObjectObjectTypeDerive", "null\n",
				"aExpectedReturnTypeDerive", "null\n", "aExpectedReturnSimpleTypeDerive", "null\n",
				"isReturnValueMandatoryDerive", "null\n", "isReturnValueSingularDerive", "null\n", "baseAsCodeDerive",
				"null\n", "chainAsCodeDerive", "null\n", "aChainCalculatedTypeDerive", "null\n",
				"aChainCalculatedSimpleTypeDerive", "null\n", "chainCalculatedSingularDerive", "null\n",
				"processorDefinitionDerive", "null\n", "typeMismatchDerive", "null\n", "chainCodeforSubchainsDerive",
				"null\n", "isOwnXOCLOpDerive", "null\n", "aAnnotatedDerive", "null\n", "aAnnotatingDerive", "null\n",
				"aExpectedReturnTypeOfAnnotationDerive", "null\n", "aExpectedReturnSimpleTypeOfAnnotationDerive",
				"null\n", "isReturnValueOfAnnotationMandatoryDerive", "null\n",
				"isReturnValueOfAnnotationSingularDerive", "null\n", "aElement1ChoiceConstruction",
				"\nlet annotatedProp: acore::classifiers::AProperty = \nself.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)\nin\nif self.aChainEntryType.oclIsUndefined() then \nSequence{} else\nself.aChainEntryType.aAllProperty()->asSequence()\nendif\n\n--" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMMapEntryForTargetFeature_ATargetFeature(), source,
				new String[] { "propertySortChoices", "false" });
	}

} //MmapperPackageImpl
