/**
 */

package com.montages.mmapper.impl;

import com.montages.mmapper.MMap;
import com.montages.mmapper.MMapping;
import com.montages.mmapper.MmapperPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MMapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mmapper.impl.MMappingImpl#getMOwnedMap <em>MOwned Map</em>}</li>
 *   <li>{@link com.montages.mmapper.impl.MMappingImpl#getMname <em>Mname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MMappingImpl extends MinimalEObjectImpl.Container implements MMapping {
	/**
	 * The cached value of the '{@link #getMOwnedMap() <em>MOwned Map</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMOwnedMap()
	 * @generated
	 * @ordered
	 */
	protected EList<MMap> mOwnedMap;

	/**
	 * The default value of the '{@link #getMname() <em>Mname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMname()
	 * @generated
	 * @ordered
	 */
	protected static final String MNAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getMname() <em>Mname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMname()
	 * @generated
	 * @ordered
	 */
	protected String mname = MNAME_EDEFAULT;
	/**
	 * This is true if the Mname attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mnameESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmapperPackage.Literals.MMAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MMap> getMOwnedMap() {
		if (mOwnedMap == null) {
			mOwnedMap = new EObjectContainmentEList.Unsettable.Resolving<MMap>(MMap.class, this,
					MmapperPackage.MMAPPING__MOWNED_MAP);
		}
		return mOwnedMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMOwnedMap() {
		if (mOwnedMap != null)
			((InternalEList.Unsettable<?>) mOwnedMap).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMOwnedMap() {
		return mOwnedMap != null && ((InternalEList.Unsettable<?>) mOwnedMap).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMname() {
		return mname;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMname(String newMname) {
		String oldMname = mname;
		mname = newMname;
		boolean oldMnameESet = mnameESet;
		mnameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MmapperPackage.MMAPPING__MNAME, oldMname, mname,
					!oldMnameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMname() {
		String oldMname = mname;
		boolean oldMnameESet = mnameESet;
		mname = MNAME_EDEFAULT;
		mnameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MmapperPackage.MMAPPING__MNAME, oldMname,
					MNAME_EDEFAULT, oldMnameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMname() {
		return mnameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MmapperPackage.MMAPPING__MOWNED_MAP:
			return ((InternalEList<?>) getMOwnedMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MmapperPackage.MMAPPING__MOWNED_MAP:
			return getMOwnedMap();
		case MmapperPackage.MMAPPING__MNAME:
			return getMname();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MmapperPackage.MMAPPING__MOWNED_MAP:
			getMOwnedMap().clear();
			getMOwnedMap().addAll((Collection<? extends MMap>) newValue);
			return;
		case MmapperPackage.MMAPPING__MNAME:
			setMname((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MmapperPackage.MMAPPING__MOWNED_MAP:
			unsetMOwnedMap();
			return;
		case MmapperPackage.MMAPPING__MNAME:
			unsetMname();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MmapperPackage.MMAPPING__MOWNED_MAP:
			return isSetMOwnedMap();
		case MmapperPackage.MMAPPING__MNAME:
			return isSetMname();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mname: ");
		if (mnameESet)
			result.append(mname);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MMappingImpl
