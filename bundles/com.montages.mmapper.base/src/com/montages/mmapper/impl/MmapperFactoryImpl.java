/**
 */
package com.montages.mmapper.impl;

import com.montages.mmapper.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MmapperFactoryImpl extends EFactoryImpl implements MmapperFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MmapperFactory init() {
		try {
			MmapperFactory theMmapperFactory = (MmapperFactory) EPackage.Registry.INSTANCE
					.getEFactory(MmapperPackage.eNS_URI);
			if (theMmapperFactory != null) {
				return theMmapperFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MmapperFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmapperFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case MmapperPackage.MMAPPING:
			return createMMapping();
		case MmapperPackage.MMAP:
			return createMMap();
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE:
			return createMMapEntryForTargetFeature();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MMapping createMMapping() {
		MMappingImpl mMapping = new MMappingImpl();
		return mMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MMap createMMap() {
		MMapImpl mMap = new MMapImpl();
		return mMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MMapEntryForTargetFeature createMMapEntryForTargetFeature() {
		MMapEntryForTargetFeatureImpl mMapEntryForTargetFeature = new MMapEntryForTargetFeatureImpl();
		return mMapEntryForTargetFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmapperPackage getMmapperPackage() {
		return (MmapperPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MmapperPackage getPackage() {
		return MmapperPackage.eINSTANCE;
	}

} //MmapperFactoryImpl
