/**
 */

package com.montages.mmapper;

import com.montages.acore.classifiers.AClassType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MMap</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mmapper.MMap#getASource <em>ASource</em>}</li>
 *   <li>{@link com.montages.mmapper.MMap#getATarget <em>ATarget</em>}</li>
 *   <li>{@link com.montages.mmapper.MMap#getMMapEntryForTargetFeature <em>MMap Entry For Target Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mmapper.MmapperPackage#getMMap()
 * @model annotation="http://www.montages.com/mCore/MCore mName='Map'"
 * @generated
 */

public interface MMap extends EObject {
	/**
	 * Returns the value of the '<em><b>ASource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASource</em>' reference.
	 * @see #isSetASource()
	 * @see #unsetASource()
	 * @see #setASource(AClassType)
	 * @see com.montages.mmapper.MmapperPackage#getMMap_ASource()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Source'"
	 *        annotation="http://www.xocl.org/OCL initValue='let subchain1 : mmapper::MMapEntryForTargetFeature = let chain: ecore::EObject = eContainer() in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(mmapper::MMapEntryForTargetFeature)\n    then chain.oclAsType(mmapper::MMapEntryForTargetFeature)\n    else null\n  endif\n  endif in \n if subchain1 = null \n  then null \n else subchain1.aSrcObjectType.oclAsType(acore::classifiers::AClassType) endif \n\n'"
	 * @generated
	 */
	AClassType getASource();

	/** 
	 * Sets the value of the '{@link com.montages.mmapper.MMap#getASource <em>ASource</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ASource</em>' reference.
	 * @see #isSetASource()
	 * @see #unsetASource()
	 * @see #getASource()
	 * @generated
	 */

	void setASource(AClassType value);

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMap#getASource <em>ASource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetASource()
	 * @see #getASource()
	 * @see #setASource(AClassType)
	 * @generated
	 */
	void unsetASource();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMap#getASource <em>ASource</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ASource</em>' reference is set.
	 * @see #unsetASource()
	 * @see #getASource()
	 * @see #setASource(AClassType)
	 * @generated
	 */
	boolean isSetASource();

	/**
	 * Returns the value of the '<em><b>ATarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ATarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ATarget</em>' reference.
	 * @see #isSetATarget()
	 * @see #unsetATarget()
	 * @see #setATarget(AClassType)
	 * @see com.montages.mmapper.MmapperPackage#getMMap_ATarget()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Target'"
	 *        annotation="http://www.xocl.org/OCL initValue='if self.eContainer().oclIsTypeOf(MMapEntryForTargetFeature)\nthen \nlet classType : acore::classifiers::AClassType =\neContainer().oclAsType(MMapEntryForTargetFeature).aTargetFeature.aClassifier.oclAsType(acore::classifiers::AClassType)\nin\nif (classType.oclIsUndefined()) then null else classType endif\nelse null\nendif'"
	 * @generated
	 */
	AClassType getATarget();

	/** 
	 * Sets the value of the '{@link com.montages.mmapper.MMap#getATarget <em>ATarget</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ATarget</em>' reference.
	 * @see #isSetATarget()
	 * @see #unsetATarget()
	 * @see #getATarget()
	 * @generated
	 */

	void setATarget(AClassType value);

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMap#getATarget <em>ATarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetATarget()
	 * @see #getATarget()
	 * @see #setATarget(AClassType)
	 * @generated
	 */
	void unsetATarget();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMap#getATarget <em>ATarget</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ATarget</em>' reference is set.
	 * @see #unsetATarget()
	 * @see #getATarget()
	 * @see #setATarget(AClassType)
	 * @generated
	 */
	boolean isSetATarget();

	/**
	 * Returns the value of the '<em><b>MMap Entry For Target Feature</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mmapper.MMapEntryForTargetFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MMap Entry For Target Feature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MMap Entry For Target Feature</em>' containment reference list.
	 * @see #isSetMMapEntryForTargetFeature()
	 * @see #unsetMMapEntryForTargetFeature()
	 * @see com.montages.mmapper.MmapperPackage#getMMap_MMapEntryForTargetFeature()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MMapEntryForTargetFeature> getMMapEntryForTargetFeature();

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMap#getMMapEntryForTargetFeature <em>MMap Entry For Target Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMMapEntryForTargetFeature()
	 * @see #getMMapEntryForTargetFeature()
	 * @generated
	 */
	void unsetMMapEntryForTargetFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMap#getMMapEntryForTargetFeature <em>MMap Entry For Target Feature</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MMap Entry For Target Feature</em>' containment reference list is set.
	 * @see #unsetMMapEntryForTargetFeature()
	 * @see #getMMapEntryForTargetFeature()
	 * @generated
	 */
	boolean isSetMMapEntryForTargetFeature();

} // MMap
