/**
 */

package com.montages.mmapper;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MMapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mmapper.MMapping#getMOwnedMap <em>MOwned Map</em>}</li>
 *   <li>{@link com.montages.mmapper.MMapping#getMname <em>Mname</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mmapper.MmapperPackage#getMMapping()
 * @model annotation="http://www.montages.com/mCore/MCore mName='Mapping'"
 * @generated
 */

public interface MMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>MOwned Map</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mmapper.MMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MOwned Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MOwned Map</em>' containment reference list.
	 * @see #isSetMOwnedMap()
	 * @see #unsetMOwnedMap()
	 * @see com.montages.mmapper.MmapperPackage#getMMapping_MOwnedMap()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='OwnedMap'"
	 * @generated
	 */
	EList<MMap> getMOwnedMap();

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMapping#getMOwnedMap <em>MOwned Map</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMOwnedMap()
	 * @see #getMOwnedMap()
	 * @generated
	 */
	void unsetMOwnedMap();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMapping#getMOwnedMap <em>MOwned Map</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MOwned Map</em>' containment reference list is set.
	 * @see #unsetMOwnedMap()
	 * @see #getMOwnedMap()
	 * @generated
	 */
	boolean isSetMOwnedMap();

	/**
	 * Returns the value of the '<em><b>Mname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mname</em>' attribute.
	 * @see #isSetMname()
	 * @see #unsetMname()
	 * @see #setMname(String)
	 * @see com.montages.mmapper.MmapperPackage#getMMapping_Mname()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='name'"
	 * @generated
	 */
	String getMname();

	/** 
	 * Sets the value of the '{@link com.montages.mmapper.MMapping#getMname <em>Mname</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mname</em>' attribute.
	 * @see #isSetMname()
	 * @see #unsetMname()
	 * @see #getMname()
	 * @generated
	 */

	void setMname(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMapping#getMname <em>Mname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMname()
	 * @see #getMname()
	 * @see #setMname(String)
	 * @generated
	 */
	void unsetMname();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMapping#getMname <em>Mname</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mname</em>' attribute is set.
	 * @see #unsetMname()
	 * @see #getMname()
	 * @see #setMname(String)
	 * @generated
	 */
	boolean isSetMname();

} // MMapping
