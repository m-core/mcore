/**
 */
package com.montages.mmapper.util;

import com.montages.mmapper.MmapperPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MmapperXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmapperXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		MmapperPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the MmapperResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new MmapperResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new MmapperResourceFactoryImpl());
		}
		return registrations;
	}

} //MmapperXMLProcessor
