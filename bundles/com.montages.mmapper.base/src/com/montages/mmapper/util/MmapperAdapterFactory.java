/**
 */
package com.montages.mmapper.util;

import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.ATyped;

import com.montages.mmapper.*;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MRulesElement;

import com.montages.mrules.expressions.MAbstractChain;
import com.montages.mrules.expressions.MAbstractExpression;
import com.montages.mrules.expressions.MAbstractExpressionWithBase;
import com.montages.mrules.expressions.MBaseChain;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mmapper.MmapperPackage
 * @generated
 */
public class MmapperAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MmapperPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmapperAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MmapperPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MmapperSwitch<Adapter> modelSwitch = new MmapperSwitch<Adapter>() {
		@Override
		public Adapter caseMMapping(MMapping object) {
			return createMMappingAdapter();
		}

		@Override
		public Adapter caseMMap(MMap object) {
			return createMMapAdapter();
		}

		@Override
		public Adapter caseMMapEntryForTargetFeature(MMapEntryForTargetFeature object) {
			return createMMapEntryForTargetFeatureAdapter();
		}

		@Override
		public Adapter caseMRulesElement(MRulesElement object) {
			return createMRulesElementAdapter();
		}

		@Override
		public Adapter caseAElement(AElement object) {
			return createAElementAdapter();
		}

		@Override
		public Adapter caseATyped(ATyped object) {
			return createATypedAdapter();
		}

		@Override
		public Adapter caseMAbstractExpression(MAbstractExpression object) {
			return createMAbstractExpressionAdapter();
		}

		@Override
		public Adapter caseMAbstractExpressionWithBase(MAbstractExpressionWithBase object) {
			return createMAbstractExpressionWithBaseAdapter();
		}

		@Override
		public Adapter caseMAbstractChain(MAbstractChain object) {
			return createMAbstractChainAdapter();
		}

		@Override
		public Adapter caseMBaseChain(MBaseChain object) {
			return createMBaseChainAdapter();
		}

		@Override
		public Adapter caseAAnnotation(AAnnotation object) {
			return createAAnnotationAdapter();
		}

		@Override
		public Adapter caseMRuleAnnotation(MRuleAnnotation object) {
			return createMRuleAnnotationAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mmapper.MMapping <em>MMapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mmapper.MMapping
	 * @generated
	 */
	public Adapter createMMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mmapper.MMap <em>MMap</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mmapper.MMap
	 * @generated
	 */
	public Adapter createMMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mmapper.MMapEntryForTargetFeature <em>MMap Entry For Target Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mmapper.MMapEntryForTargetFeature
	 * @generated
	 */
	public Adapter createMMapEntryForTargetFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mrules.MRulesElement <em>MRules Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mrules.MRulesElement
	 * @generated
	 */
	public Adapter createMRulesElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.abstractions.AElement
	 * @generated
	 */
	public Adapter createAElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.abstractions.ATyped <em>ATyped</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.abstractions.ATyped
	 * @generated
	 */
	public Adapter createATypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mrules.expressions.MAbstractExpression <em>MAbstract Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mrules.expressions.MAbstractExpression
	 * @generated
	 */
	public Adapter createMAbstractExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase <em>MAbstract Expression With Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mrules.expressions.MAbstractExpressionWithBase
	 * @generated
	 */
	public Adapter createMAbstractExpressionWithBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mrules.expressions.MAbstractChain <em>MAbstract Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mrules.expressions.MAbstractChain
	 * @generated
	 */
	public Adapter createMAbstractChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mrules.expressions.MBaseChain <em>MBase Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mrules.expressions.MBaseChain
	 * @generated
	 */
	public Adapter createMBaseChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.abstractions.AAnnotation <em>AAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.abstractions.AAnnotation
	 * @generated
	 */
	public Adapter createAAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mrules.MRuleAnnotation <em>MRule Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mrules.MRuleAnnotation
	 * @generated
	 */
	public Adapter createMRuleAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MmapperAdapterFactory
