/**
 */
package org.langlets.acore.abstractions.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.provider.AcoreEditPlugin;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link org.langlets.acore.abstractions.AElement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AElementItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AElementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addALabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAKindBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addARenderedKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingComponentPropertyDescriptor(object);
			}
			addTPackageUriPropertyDescriptor(object);
			addTClassifierNamePropertyDescriptor(object);
			addTFeatureNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addTPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTACoreAStringClassPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ALabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addALabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALabel feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aLabel_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ALABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AKind Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAKindBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKind Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aKindBase_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aKindBase_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AKIND_BASE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ARendered Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addARenderedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARendered Kind feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aRenderedKind_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aRenderedKind_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Component feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Component feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aContainingComponent_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aContainingComponent_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT, false, false, false, null,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TPackage Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTPackageUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TPackage Uri feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tPackageUri_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tPackageUri_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TPACKAGE_URI, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the TClassifier Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTClassifierNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TClassifier Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tClassifierName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tClassifierName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TCLASSIFIER_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the TFeature Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTFeatureNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TFeature Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tFeatureName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tFeatureName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TFEATURE_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the TPackage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TPackage feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tPackage_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TPACKAGE, false, false, false, null,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TClassifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TClassifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tClassifier_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TCLASSIFIER, false, false, false, null,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TFeature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TFeature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tFeature_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TFEATURE, false, false, false, null,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TA Core AString Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTACoreAStringClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TA Core AString Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_tACoreAStringClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_tACoreAStringClass_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__TA_CORE_ASTRING_CLASS, false, false, false, null,
						getString("_UI__80ACorePackagePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AElement) object).getTClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AElement.class)) {
		case AbstractionsPackage.AELEMENT__ALABEL:
		case AbstractionsPackage.AELEMENT__AKIND_BASE:
		case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
		case AbstractionsPackage.AELEMENT__TPACKAGE_URI:
		case AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME:
		case AbstractionsPackage.AELEMENT__TFEATURE_NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AbstractionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
