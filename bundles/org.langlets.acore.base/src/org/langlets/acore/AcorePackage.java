/**
 */
package org.langlets.acore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.langlets.acore.abstractions.AbstractionsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.langlets.acore.AcoreFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'AReference\''"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true' uuidAttributeName='_uuid'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='org.langlets'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface AcorePackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "org.langlets.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "acore";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.langlets.org/aCore/ACore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AcorePackage eINSTANCE = org.langlets.acore.impl.AcorePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.langlets.acore.impl.AStructuringElementImpl <em>AStructuring Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.impl.AStructuringElementImpl
	 * @see org.langlets.acore.impl.AcorePackageImpl#getAStructuringElement()
	 * @generated
	 */
	int ASTRUCTURING_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__ALABEL = AbstractionsPackage.ANAMED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__AKIND_BASE = AbstractionsPackage.ANAMED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__ARENDERED_KIND = AbstractionsPackage.ANAMED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__ACONTAINING_COMPONENT = AbstractionsPackage.ANAMED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TPACKAGE_URI = AbstractionsPackage.ANAMED__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TCLASSIFIER_NAME = AbstractionsPackage.ANAMED__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TFEATURE_NAME = AbstractionsPackage.ANAMED__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TPACKAGE = AbstractionsPackage.ANAMED__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TCLASSIFIER = AbstractionsPackage.ANAMED__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TFEATURE = AbstractionsPackage.ANAMED__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__TA_CORE_ASTRING_CLASS = AbstractionsPackage.ANAMED__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__ANAME = AbstractionsPackage.ANAMED__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__AUNDEFINED_NAME_CONSTANT = AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__ABUSINESS_NAME = AbstractionsPackage.ANAMED__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER = AbstractionsPackage.ANAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__AURI = AbstractionsPackage.ANAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AAll Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT__AALL_PACKAGES = AbstractionsPackage.ANAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AStructuring Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT_FEATURE_COUNT = AbstractionsPackage.ANAMED_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___INDENT_LEVEL = AbstractionsPackage.ANAMED___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___INDENTATION_SPACES = AbstractionsPackage.ANAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___INDENTATION_SPACES__INTEGER = AbstractionsPackage.ANAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___STRING_OR_MISSING__STRING = AbstractionsPackage.ANAMED___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___STRING_IS_EMPTY__STRING = AbstractionsPackage.ANAMED___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___APACKAGE_FROM_URI__STRING = AbstractionsPackage.ANAMED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.ANAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.ANAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___ACORE_ASTRING_CLASS = AbstractionsPackage.ANAMED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___ACORE_AREAL_CLASS = AbstractionsPackage.ANAMED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___ACORE_AINTEGER_CLASS = AbstractionsPackage.ANAMED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT___ACORE_AOBJECT_CLASS = AbstractionsPackage.ANAMED___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AStructuring Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTRUCTURING_ELEMENT_OPERATION_COUNT = AbstractionsPackage.ANAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.impl.AAbstractFolderImpl <em>AAbstract Folder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.impl.AAbstractFolderImpl
	 * @see org.langlets.acore.impl.AcorePackageImpl#getAAbstractFolder()
	 * @generated
	 */
	int AABSTRACT_FOLDER = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ALABEL = ASTRUCTURING_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__AKIND_BASE = ASTRUCTURING_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ARENDERED_KIND = ASTRUCTURING_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ACONTAINING_COMPONENT = ASTRUCTURING_ELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TPACKAGE_URI = ASTRUCTURING_ELEMENT__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TCLASSIFIER_NAME = ASTRUCTURING_ELEMENT__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TFEATURE_NAME = ASTRUCTURING_ELEMENT__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TPACKAGE = ASTRUCTURING_ELEMENT__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TCLASSIFIER = ASTRUCTURING_ELEMENT__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TFEATURE = ASTRUCTURING_ELEMENT__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__TA_CORE_ASTRING_CLASS = ASTRUCTURING_ELEMENT__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ANAME = ASTRUCTURING_ELEMENT__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__AUNDEFINED_NAME_CONSTANT = ASTRUCTURING_ELEMENT__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ABUSINESS_NAME = ASTRUCTURING_ELEMENT__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ACONTAINING_FOLDER = ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER;

	/**
	 * The feature id for the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__AURI = ASTRUCTURING_ELEMENT__AURI;

	/**
	 * The feature id for the '<em><b>AAll Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__AALL_PACKAGES = ASTRUCTURING_ELEMENT__AALL_PACKAGES;

	/**
	 * The feature id for the '<em><b>APackage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__APACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AResource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__ARESOURCE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AFolder</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER__AFOLDER = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AAbstract Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER_FEATURE_COUNT = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___INDENT_LEVEL = ASTRUCTURING_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___INDENTATION_SPACES = ASTRUCTURING_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___INDENTATION_SPACES__INTEGER = ASTRUCTURING_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___STRING_OR_MISSING__STRING = ASTRUCTURING_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___STRING_IS_EMPTY__STRING = ASTRUCTURING_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___APACKAGE_FROM_URI__STRING = ASTRUCTURING_ELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ASTRUCTURING_ELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ASTRUCTURING_ELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___ACORE_ASTRING_CLASS = ASTRUCTURING_ELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___ACORE_AREAL_CLASS = ASTRUCTURING_ELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___ACORE_AINTEGER_CLASS = ASTRUCTURING_ELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER___ACORE_AOBJECT_CLASS = ASTRUCTURING_ELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AAbstract Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AABSTRACT_FOLDER_OPERATION_COUNT = ASTRUCTURING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.impl.AComponentImpl <em>AComponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.impl.AComponentImpl
	 * @see org.langlets.acore.impl.AcorePackageImpl#getAComponent()
	 * @generated
	 */
	int ACOMPONENT = 2;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ALABEL = AABSTRACT_FOLDER__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AKIND_BASE = AABSTRACT_FOLDER__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ARENDERED_KIND = AABSTRACT_FOLDER__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ACONTAINING_COMPONENT = AABSTRACT_FOLDER__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TPACKAGE_URI = AABSTRACT_FOLDER__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TCLASSIFIER_NAME = AABSTRACT_FOLDER__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TFEATURE_NAME = AABSTRACT_FOLDER__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TPACKAGE = AABSTRACT_FOLDER__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TCLASSIFIER = AABSTRACT_FOLDER__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TFEATURE = AABSTRACT_FOLDER__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__TA_CORE_ASTRING_CLASS = AABSTRACT_FOLDER__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ANAME = AABSTRACT_FOLDER__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AUNDEFINED_NAME_CONSTANT = AABSTRACT_FOLDER__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ABUSINESS_NAME = AABSTRACT_FOLDER__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ACONTAINING_FOLDER = AABSTRACT_FOLDER__ACONTAINING_FOLDER;

	/**
	 * The feature id for the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AURI = AABSTRACT_FOLDER__AURI;

	/**
	 * The feature id for the '<em><b>AAll Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AALL_PACKAGES = AABSTRACT_FOLDER__AALL_PACKAGES;

	/**
	 * The feature id for the '<em><b>APackage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__APACKAGE = AABSTRACT_FOLDER__APACKAGE;

	/**
	 * The feature id for the '<em><b>AResource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ARESOURCE = AABSTRACT_FOLDER__ARESOURCE;

	/**
	 * The feature id for the '<em><b>AFolder</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AFOLDER = AABSTRACT_FOLDER__AFOLDER;

	/**
	 * The feature id for the '<em><b>AComponent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ACOMPONENT_ID = AABSTRACT_FOLDER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ABase Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ABASE_URI = AABSTRACT_FOLDER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ADefault Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__ADEFAULT_URI = AABSTRACT_FOLDER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AUndefined Id Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AUNDEFINED_ID_CONSTANT = AABSTRACT_FOLDER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AUsed</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AUSED = AABSTRACT_FOLDER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AMain Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AMAIN_PACKAGE = AABSTRACT_FOLDER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AMain Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT__AMAIN_RESOURCE = AABSTRACT_FOLDER_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>AComponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT_FEATURE_COUNT = AABSTRACT_FOLDER_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___INDENT_LEVEL = AABSTRACT_FOLDER___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___INDENTATION_SPACES = AABSTRACT_FOLDER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___INDENTATION_SPACES__INTEGER = AABSTRACT_FOLDER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___STRING_OR_MISSING__STRING = AABSTRACT_FOLDER___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___STRING_IS_EMPTY__STRING = AABSTRACT_FOLDER___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AABSTRACT_FOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AABSTRACT_FOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AABSTRACT_FOLDER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AABSTRACT_FOLDER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___ACORE_ASTRING_CLASS = AABSTRACT_FOLDER___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___ACORE_AREAL_CLASS = AABSTRACT_FOLDER___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___ACORE_AINTEGER_CLASS = AABSTRACT_FOLDER___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___ACORE_AOBJECT_CLASS = AABSTRACT_FOLDER___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT___APACKAGE_FROM_URI__STRING = AABSTRACT_FOLDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>AComponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACOMPONENT_OPERATION_COUNT = AABSTRACT_FOLDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.langlets.acore.impl.AFolderImpl <em>AFolder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.impl.AFolderImpl
	 * @see org.langlets.acore.impl.AcorePackageImpl#getAFolder()
	 * @generated
	 */
	int AFOLDER = 3;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ALABEL = AABSTRACT_FOLDER__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__AKIND_BASE = AABSTRACT_FOLDER__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ARENDERED_KIND = AABSTRACT_FOLDER__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ACONTAINING_COMPONENT = AABSTRACT_FOLDER__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TPACKAGE_URI = AABSTRACT_FOLDER__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TCLASSIFIER_NAME = AABSTRACT_FOLDER__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TFEATURE_NAME = AABSTRACT_FOLDER__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TPACKAGE = AABSTRACT_FOLDER__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TCLASSIFIER = AABSTRACT_FOLDER__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TFEATURE = AABSTRACT_FOLDER__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__TA_CORE_ASTRING_CLASS = AABSTRACT_FOLDER__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ANAME = AABSTRACT_FOLDER__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__AUNDEFINED_NAME_CONSTANT = AABSTRACT_FOLDER__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ABUSINESS_NAME = AABSTRACT_FOLDER__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ACONTAINING_FOLDER = AABSTRACT_FOLDER__ACONTAINING_FOLDER;

	/**
	 * The feature id for the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__AURI = AABSTRACT_FOLDER__AURI;

	/**
	 * The feature id for the '<em><b>AAll Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__AALL_PACKAGES = AABSTRACT_FOLDER__AALL_PACKAGES;

	/**
	 * The feature id for the '<em><b>APackage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__APACKAGE = AABSTRACT_FOLDER__APACKAGE;

	/**
	 * The feature id for the '<em><b>AResource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__ARESOURCE = AABSTRACT_FOLDER__ARESOURCE;

	/**
	 * The feature id for the '<em><b>AFolder</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER__AFOLDER = AABSTRACT_FOLDER__AFOLDER;

	/**
	 * The number of structural features of the '<em>AFolder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER_FEATURE_COUNT = AABSTRACT_FOLDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___INDENT_LEVEL = AABSTRACT_FOLDER___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___INDENTATION_SPACES = AABSTRACT_FOLDER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___INDENTATION_SPACES__INTEGER = AABSTRACT_FOLDER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___STRING_OR_MISSING__STRING = AABSTRACT_FOLDER___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___STRING_IS_EMPTY__STRING = AABSTRACT_FOLDER___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AABSTRACT_FOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AABSTRACT_FOLDER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___APACKAGE_FROM_URI__STRING = AABSTRACT_FOLDER___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AABSTRACT_FOLDER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AABSTRACT_FOLDER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___ACORE_ASTRING_CLASS = AABSTRACT_FOLDER___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___ACORE_AREAL_CLASS = AABSTRACT_FOLDER___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___ACORE_AINTEGER_CLASS = AABSTRACT_FOLDER___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER___ACORE_AOBJECT_CLASS = AABSTRACT_FOLDER___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AFolder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFOLDER_OPERATION_COUNT = AABSTRACT_FOLDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.impl.APackageImpl <em>APackage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.impl.APackageImpl
	 * @see org.langlets.acore.impl.AcorePackageImpl#getAPackage()
	 * @generated
	 */
	int APACKAGE = 4;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ALABEL = ASTRUCTURING_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AKIND_BASE = ASTRUCTURING_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ARENDERED_KIND = ASTRUCTURING_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ACONTAINING_COMPONENT = ASTRUCTURING_ELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TPACKAGE_URI = ASTRUCTURING_ELEMENT__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TCLASSIFIER_NAME = ASTRUCTURING_ELEMENT__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TFEATURE_NAME = ASTRUCTURING_ELEMENT__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TPACKAGE = ASTRUCTURING_ELEMENT__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TCLASSIFIER = ASTRUCTURING_ELEMENT__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TFEATURE = ASTRUCTURING_ELEMENT__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__TA_CORE_ASTRING_CLASS = ASTRUCTURING_ELEMENT__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ANAME = ASTRUCTURING_ELEMENT__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AUNDEFINED_NAME_CONSTANT = ASTRUCTURING_ELEMENT__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ABUSINESS_NAME = ASTRUCTURING_ELEMENT__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ACONTAINING_FOLDER = ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER;

	/**
	 * The feature id for the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AURI = ASTRUCTURING_ELEMENT__AURI;

	/**
	 * The feature id for the '<em><b>AAll Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AALL_PACKAGES = ASTRUCTURING_ELEMENT__AALL_PACKAGES;

	/**
	 * The feature id for the '<em><b>ARoot Object Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AROOT_OBJECT_CLASS = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ACLASSIFIER = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ASub Package</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ASUB_PACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ACONTAINING_PACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>ASpecial Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__ASPECIAL_URI = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AActive Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AACTIVE_PACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AActive Root Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AACTIVE_ROOT_PACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AActive Sub Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE__AACTIVE_SUB_PACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>APackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE_FEATURE_COUNT = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___INDENT_LEVEL = ASTRUCTURING_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___INDENTATION_SPACES = ASTRUCTURING_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___INDENTATION_SPACES__INTEGER = ASTRUCTURING_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___STRING_OR_MISSING__STRING = ASTRUCTURING_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___STRING_IS_EMPTY__STRING = ASTRUCTURING_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___APACKAGE_FROM_URI__STRING = ASTRUCTURING_ELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ASTRUCTURING_ELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ASTRUCTURING_ELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___ACORE_ASTRING_CLASS = ASTRUCTURING_ELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___ACORE_AREAL_CLASS = ASTRUCTURING_ELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___ACORE_AINTEGER_CLASS = ASTRUCTURING_ELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___ACORE_AOBJECT_CLASS = ASTRUCTURING_ELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AClassifier From Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE___ACLASSIFIER_FROM_NAME__STRING = ASTRUCTURING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>APackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APACKAGE_OPERATION_COUNT = ASTRUCTURING_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.langlets.acore.impl.AResourceImpl <em>AResource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.impl.AResourceImpl
	 * @see org.langlets.acore.impl.AcorePackageImpl#getAResource()
	 * @generated
	 */
	int ARESOURCE = 5;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__ALABEL = ASTRUCTURING_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AKIND_BASE = ASTRUCTURING_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__ARENDERED_KIND = ASTRUCTURING_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__ACONTAINING_COMPONENT = ASTRUCTURING_ELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TPACKAGE_URI = ASTRUCTURING_ELEMENT__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TCLASSIFIER_NAME = ASTRUCTURING_ELEMENT__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TFEATURE_NAME = ASTRUCTURING_ELEMENT__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TPACKAGE = ASTRUCTURING_ELEMENT__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TCLASSIFIER = ASTRUCTURING_ELEMENT__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TFEATURE = ASTRUCTURING_ELEMENT__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__TA_CORE_ASTRING_CLASS = ASTRUCTURING_ELEMENT__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__ANAME = ASTRUCTURING_ELEMENT__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AUNDEFINED_NAME_CONSTANT = ASTRUCTURING_ELEMENT__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__ABUSINESS_NAME = ASTRUCTURING_ELEMENT__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__ACONTAINING_FOLDER = ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER;

	/**
	 * The feature id for the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AURI = ASTRUCTURING_ELEMENT__AURI;

	/**
	 * The feature id for the '<em><b>AAll Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AALL_PACKAGES = ASTRUCTURING_ELEMENT__AALL_PACKAGES;

	/**
	 * The feature id for the '<em><b>AObject</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AOBJECT = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AActive Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AACTIVE_RESOURCE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ARoot Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE__AROOT_OBJECT_PACKAGE = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AResource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE_FEATURE_COUNT = ASTRUCTURING_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___INDENT_LEVEL = ASTRUCTURING_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___INDENTATION_SPACES = ASTRUCTURING_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___INDENTATION_SPACES__INTEGER = ASTRUCTURING_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___STRING_OR_MISSING__STRING = ASTRUCTURING_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___STRING_IS_EMPTY__STRING = ASTRUCTURING_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ASTRUCTURING_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___APACKAGE_FROM_URI__STRING = ASTRUCTURING_ELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ASTRUCTURING_ELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ASTRUCTURING_ELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___ACORE_ASTRING_CLASS = ASTRUCTURING_ELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___ACORE_AREAL_CLASS = ASTRUCTURING_ELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___ACORE_AINTEGER_CLASS = ASTRUCTURING_ELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE___ACORE_AOBJECT_CLASS = ASTRUCTURING_ELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AResource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARESOURCE_OPERATION_COUNT = ASTRUCTURING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.AStructuringElement <em>AStructuring Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AStructuring Element</em>'.
	 * @see org.langlets.acore.AStructuringElement
	 * @generated
	 */
	EClass getAStructuringElement();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.AStructuringElement#getAContainingFolder <em>AContaining Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Folder</em>'.
	 * @see org.langlets.acore.AStructuringElement#getAContainingFolder()
	 * @see #getAStructuringElement()
	 * @generated
	 */
	EReference getAStructuringElement_AContainingFolder();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.AStructuringElement#getAUri <em>AUri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUri</em>'.
	 * @see org.langlets.acore.AStructuringElement#getAUri()
	 * @see #getAStructuringElement()
	 * @generated
	 */
	EAttribute getAStructuringElement_AUri();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.AStructuringElement#getAAllPackages <em>AAll Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Packages</em>'.
	 * @see org.langlets.acore.AStructuringElement#getAAllPackages()
	 * @see #getAStructuringElement()
	 * @generated
	 */
	EReference getAStructuringElement_AAllPackages();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.AAbstractFolder <em>AAbstract Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AAbstract Folder</em>'.
	 * @see org.langlets.acore.AAbstractFolder
	 * @generated
	 */
	EClass getAAbstractFolder();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.AAbstractFolder#getAPackage <em>APackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>APackage</em>'.
	 * @see org.langlets.acore.AAbstractFolder#getAPackage()
	 * @see #getAAbstractFolder()
	 * @generated
	 */
	EReference getAAbstractFolder_APackage();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.AAbstractFolder#getAResource <em>AResource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AResource</em>'.
	 * @see org.langlets.acore.AAbstractFolder#getAResource()
	 * @see #getAAbstractFolder()
	 * @generated
	 */
	EReference getAAbstractFolder_AResource();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.AAbstractFolder#getAFolder <em>AFolder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AFolder</em>'.
	 * @see org.langlets.acore.AAbstractFolder#getAFolder()
	 * @see #getAAbstractFolder()
	 * @generated
	 */
	EReference getAAbstractFolder_AFolder();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.AComponent <em>AComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AComponent</em>'.
	 * @see org.langlets.acore.AComponent
	 * @generated
	 */
	EClass getAComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.AComponent#getAComponentId <em>AComponent Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AComponent Id</em>'.
	 * @see org.langlets.acore.AComponent#getAComponentId()
	 * @see #getAComponent()
	 * @generated
	 */
	EAttribute getAComponent_AComponentId();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.AComponent#getABaseUri <em>ABase Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ABase Uri</em>'.
	 * @see org.langlets.acore.AComponent#getABaseUri()
	 * @see #getAComponent()
	 * @generated
	 */
	EAttribute getAComponent_ABaseUri();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.AComponent#getADefaultUri <em>ADefault Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ADefault Uri</em>'.
	 * @see org.langlets.acore.AComponent#getADefaultUri()
	 * @see #getAComponent()
	 * @generated
	 */
	EAttribute getAComponent_ADefaultUri();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.AComponent#getAUndefinedIdConstant <em>AUndefined Id Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUndefined Id Constant</em>'.
	 * @see org.langlets.acore.AComponent#getAUndefinedIdConstant()
	 * @see #getAComponent()
	 * @generated
	 */
	EAttribute getAComponent_AUndefinedIdConstant();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.AComponent#getAUsed <em>AUsed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AUsed</em>'.
	 * @see org.langlets.acore.AComponent#getAUsed()
	 * @see #getAComponent()
	 * @generated
	 */
	EReference getAComponent_AUsed();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.AComponent#getAMainPackage <em>AMain Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AMain Package</em>'.
	 * @see org.langlets.acore.AComponent#getAMainPackage()
	 * @see #getAComponent()
	 * @generated
	 */
	EReference getAComponent_AMainPackage();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.AComponent#getAMainResource <em>AMain Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AMain Resource</em>'.
	 * @see org.langlets.acore.AComponent#getAMainResource()
	 * @see #getAComponent()
	 * @generated
	 */
	EReference getAComponent_AMainResource();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.AComponent#aPackageFromUri(java.lang.String) <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>APackage From Uri</em>' operation.
	 * @see org.langlets.acore.AComponent#aPackageFromUri(java.lang.String)
	 * @generated
	 */
	EOperation getAComponent__APackageFromUri__String();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.AFolder <em>AFolder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AFolder</em>'.
	 * @see org.langlets.acore.AFolder
	 * @generated
	 */
	EClass getAFolder();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.APackage <em>APackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>APackage</em>'.
	 * @see org.langlets.acore.APackage
	 * @generated
	 */
	EClass getAPackage();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.APackage#getARootObjectClass <em>ARoot Object Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ARoot Object Class</em>'.
	 * @see org.langlets.acore.APackage#getARootObjectClass()
	 * @see #getAPackage()
	 * @generated
	 */
	EReference getAPackage_ARootObjectClass();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.APackage#getAClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AClassifier</em>'.
	 * @see org.langlets.acore.APackage#getAClassifier()
	 * @see #getAPackage()
	 * @generated
	 */
	EReference getAPackage_AClassifier();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.APackage#getASubPackage <em>ASub Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASub Package</em>'.
	 * @see org.langlets.acore.APackage#getASubPackage()
	 * @see #getAPackage()
	 * @generated
	 */
	EReference getAPackage_ASubPackage();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.APackage#getAContainingPackage <em>AContaining Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Package</em>'.
	 * @see org.langlets.acore.APackage#getAContainingPackage()
	 * @see #getAPackage()
	 * @generated
	 */
	EReference getAPackage_AContainingPackage();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.APackage#getASpecialUri <em>ASpecial Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ASpecial Uri</em>'.
	 * @see org.langlets.acore.APackage#getASpecialUri()
	 * @see #getAPackage()
	 * @generated
	 */
	EAttribute getAPackage_ASpecialUri();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.APackage#getAActivePackage <em>AActive Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Package</em>'.
	 * @see org.langlets.acore.APackage#getAActivePackage()
	 * @see #getAPackage()
	 * @generated
	 */
	EAttribute getAPackage_AActivePackage();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.APackage#getAActiveRootPackage <em>AActive Root Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Root Package</em>'.
	 * @see org.langlets.acore.APackage#getAActiveRootPackage()
	 * @see #getAPackage()
	 * @generated
	 */
	EAttribute getAPackage_AActiveRootPackage();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.APackage#getAActiveSubPackage <em>AActive Sub Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Sub Package</em>'.
	 * @see org.langlets.acore.APackage#getAActiveSubPackage()
	 * @see #getAPackage()
	 * @generated
	 */
	EAttribute getAPackage_AActiveSubPackage();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.APackage#aClassifierFromName(java.lang.String) <em>AClassifier From Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AClassifier From Name</em>' operation.
	 * @see org.langlets.acore.APackage#aClassifierFromName(java.lang.String)
	 * @generated
	 */
	EOperation getAPackage__AClassifierFromName__String();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.AResource <em>AResource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AResource</em>'.
	 * @see org.langlets.acore.AResource
	 * @generated
	 */
	EClass getAResource();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.AResource#getAObject <em>AObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AObject</em>'.
	 * @see org.langlets.acore.AResource#getAObject()
	 * @see #getAResource()
	 * @generated
	 */
	EReference getAResource_AObject();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.AResource#getAActiveResource <em>AActive Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Resource</em>'.
	 * @see org.langlets.acore.AResource#getAActiveResource()
	 * @see #getAResource()
	 * @generated
	 */
	EAttribute getAResource_AActiveResource();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.AResource#getARootObjectPackage <em>ARoot Object Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ARoot Object Package</em>'.
	 * @see org.langlets.acore.AResource#getARootObjectPackage()
	 * @see #getAResource()
	 * @generated
	 */
	EReference getAResource_ARootObjectPackage();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AcoreFactory getAcoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.langlets.acore.impl.AStructuringElementImpl <em>AStructuring Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.impl.AStructuringElementImpl
		 * @see org.langlets.acore.impl.AcorePackageImpl#getAStructuringElement()
		 * @generated
		 */
		EClass ASTRUCTURING_ELEMENT = eINSTANCE.getAStructuringElement();

		/**
		 * The meta object literal for the '<em><b>AContaining Folder</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER = eINSTANCE.getAStructuringElement_AContainingFolder();

		/**
		 * The meta object literal for the '<em><b>AUri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASTRUCTURING_ELEMENT__AURI = eINSTANCE.getAStructuringElement_AUri();

		/**
		 * The meta object literal for the '<em><b>AAll Packages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASTRUCTURING_ELEMENT__AALL_PACKAGES = eINSTANCE.getAStructuringElement_AAllPackages();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.impl.AAbstractFolderImpl <em>AAbstract Folder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.impl.AAbstractFolderImpl
		 * @see org.langlets.acore.impl.AcorePackageImpl#getAAbstractFolder()
		 * @generated
		 */
		EClass AABSTRACT_FOLDER = eINSTANCE.getAAbstractFolder();

		/**
		 * The meta object literal for the '<em><b>APackage</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AABSTRACT_FOLDER__APACKAGE = eINSTANCE.getAAbstractFolder_APackage();

		/**
		 * The meta object literal for the '<em><b>AResource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AABSTRACT_FOLDER__ARESOURCE = eINSTANCE.getAAbstractFolder_AResource();

		/**
		 * The meta object literal for the '<em><b>AFolder</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AABSTRACT_FOLDER__AFOLDER = eINSTANCE.getAAbstractFolder_AFolder();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.impl.AComponentImpl <em>AComponent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.impl.AComponentImpl
		 * @see org.langlets.acore.impl.AcorePackageImpl#getAComponent()
		 * @generated
		 */
		EClass ACOMPONENT = eINSTANCE.getAComponent();

		/**
		 * The meta object literal for the '<em><b>AComponent Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACOMPONENT__ACOMPONENT_ID = eINSTANCE.getAComponent_AComponentId();

		/**
		 * The meta object literal for the '<em><b>ABase Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACOMPONENT__ABASE_URI = eINSTANCE.getAComponent_ABaseUri();

		/**
		 * The meta object literal for the '<em><b>ADefault Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACOMPONENT__ADEFAULT_URI = eINSTANCE.getAComponent_ADefaultUri();

		/**
		 * The meta object literal for the '<em><b>AUndefined Id Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACOMPONENT__AUNDEFINED_ID_CONSTANT = eINSTANCE.getAComponent_AUndefinedIdConstant();

		/**
		 * The meta object literal for the '<em><b>AUsed</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACOMPONENT__AUSED = eINSTANCE.getAComponent_AUsed();

		/**
		 * The meta object literal for the '<em><b>AMain Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACOMPONENT__AMAIN_PACKAGE = eINSTANCE.getAComponent_AMainPackage();

		/**
		 * The meta object literal for the '<em><b>AMain Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACOMPONENT__AMAIN_RESOURCE = eINSTANCE.getAComponent_AMainResource();

		/**
		 * The meta object literal for the '<em><b>APackage From Uri</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACOMPONENT___APACKAGE_FROM_URI__STRING = eINSTANCE.getAComponent__APackageFromUri__String();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.impl.AFolderImpl <em>AFolder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.impl.AFolderImpl
		 * @see org.langlets.acore.impl.AcorePackageImpl#getAFolder()
		 * @generated
		 */
		EClass AFOLDER = eINSTANCE.getAFolder();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.impl.APackageImpl <em>APackage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.impl.APackageImpl
		 * @see org.langlets.acore.impl.AcorePackageImpl#getAPackage()
		 * @generated
		 */
		EClass APACKAGE = eINSTANCE.getAPackage();

		/**
		 * The meta object literal for the '<em><b>ARoot Object Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APACKAGE__AROOT_OBJECT_CLASS = eINSTANCE.getAPackage_ARootObjectClass();

		/**
		 * The meta object literal for the '<em><b>AClassifier</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APACKAGE__ACLASSIFIER = eINSTANCE.getAPackage_AClassifier();

		/**
		 * The meta object literal for the '<em><b>ASub Package</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APACKAGE__ASUB_PACKAGE = eINSTANCE.getAPackage_ASubPackage();

		/**
		 * The meta object literal for the '<em><b>AContaining Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APACKAGE__ACONTAINING_PACKAGE = eINSTANCE.getAPackage_AContainingPackage();

		/**
		 * The meta object literal for the '<em><b>ASpecial Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APACKAGE__ASPECIAL_URI = eINSTANCE.getAPackage_ASpecialUri();

		/**
		 * The meta object literal for the '<em><b>AActive Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APACKAGE__AACTIVE_PACKAGE = eINSTANCE.getAPackage_AActivePackage();

		/**
		 * The meta object literal for the '<em><b>AActive Root Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APACKAGE__AACTIVE_ROOT_PACKAGE = eINSTANCE.getAPackage_AActiveRootPackage();

		/**
		 * The meta object literal for the '<em><b>AActive Sub Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APACKAGE__AACTIVE_SUB_PACKAGE = eINSTANCE.getAPackage_AActiveSubPackage();

		/**
		 * The meta object literal for the '<em><b>AClassifier From Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APACKAGE___ACLASSIFIER_FROM_NAME__STRING = eINSTANCE.getAPackage__AClassifierFromName__String();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.impl.AResourceImpl <em>AResource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.impl.AResourceImpl
		 * @see org.langlets.acore.impl.AcorePackageImpl#getAResource()
		 * @generated
		 */
		EClass ARESOURCE = eINSTANCE.getAResource();

		/**
		 * The meta object literal for the '<em><b>AObject</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARESOURCE__AOBJECT = eINSTANCE.getAResource_AObject();

		/**
		 * The meta object literal for the '<em><b>AActive Resource</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARESOURCE__AACTIVE_RESOURCE = eINSTANCE.getAResource_AActiveResource();

		/**
		 * The meta object literal for the '<em><b>ARoot Object Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARESOURCE__AROOT_OBJECT_PACKAGE = eINSTANCE.getAResource_ARootObjectPackage();

	}

} //AcorePackage
