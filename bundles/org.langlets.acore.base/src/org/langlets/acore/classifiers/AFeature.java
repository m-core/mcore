/**
 */
package org.langlets.acore.classifiers;

import org.langlets.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AFeature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AFeature#getAStored <em>AStored</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AFeature#getAPersisted <em>APersisted</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AFeature#getAChangeable <em>AChangeable</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AFeature#getAActiveFeature <em>AActive Feature</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AFeature#getAContainingClass <em>AContaining Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAFeature()
 * @model abstract="true"
 * @generated
 */

public interface AFeature extends ATyped, ANamed {
	/**
	 * Returns the value of the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AStored</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AStored</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAFeature_AStored()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAStored();

	/**
	 * Returns the value of the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>APersisted</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>APersisted</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAFeature_APersisted()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAPersisted();

	/**
	 * Returns the value of the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AChangeable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AChangeable</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAFeature_AChangeable()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (aStored) \n  =true \nthen true\n  else false\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAChangeable();

	/**
	 * Returns the value of the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Feature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Feature</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAFeature_AActiveFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveFeature();

	/**
	 * Returns the value of the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Class</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAFeature_AContainingClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClass = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClass getAContainingClass();

} // AFeature
