/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.langlets.acore.abstractions.AbstractionsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.langlets.acore.classifiers.ClassifiersFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='org.langlets.acore'"
 * @generated
 */
public interface ClassifiersPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "org.langlets.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "classifiers";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.langlets.org/aCore/ACore/Classifiers";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.classifiers";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassifiersPackage eINSTANCE = org.langlets.acore.classifiers.impl.ClassifiersPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AClassifierImpl <em>AClassifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AClassifierImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAClassifier()
	 * @generated
	 */
	int ACLASSIFIER = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ALABEL = AbstractionsPackage.ANAMED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AKIND_BASE = AbstractionsPackage.ANAMED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ARENDERED_KIND = AbstractionsPackage.ANAMED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ACONTAINING_COMPONENT = AbstractionsPackage.ANAMED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TPACKAGE_URI = AbstractionsPackage.ANAMED__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TCLASSIFIER_NAME = AbstractionsPackage.ANAMED__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TFEATURE_NAME = AbstractionsPackage.ANAMED__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TPACKAGE = AbstractionsPackage.ANAMED__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TCLASSIFIER = AbstractionsPackage.ANAMED__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TFEATURE = AbstractionsPackage.ANAMED__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__TA_CORE_ASTRING_CLASS = AbstractionsPackage.ANAMED__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ANAME = AbstractionsPackage.ANAMED__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AUNDEFINED_NAME_CONSTANT = AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ABUSINESS_NAME = AbstractionsPackage.ANAMED__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ASPECIALIZED_CLASSIFIER = AbstractionsPackage.ANAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AACTIVE_DATA_TYPE = AbstractionsPackage.ANAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AACTIVE_ENUMERATION = AbstractionsPackage.ANAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AACTIVE_CLASS = AbstractionsPackage.ANAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ACONTAINING_PACKAGE = AbstractionsPackage.ANAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>As Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AS_DATA_TYPE = AbstractionsPackage.ANAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>As Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AS_CLASS = AbstractionsPackage.ANAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AIS_STRING_CLASSIFIER = AbstractionsPackage.ANAMED_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>AClassifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER_FEATURE_COUNT = AbstractionsPackage.ANAMED_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___INDENT_LEVEL = AbstractionsPackage.ANAMED___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___INDENTATION_SPACES = AbstractionsPackage.ANAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___INDENTATION_SPACES__INTEGER = AbstractionsPackage.ANAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___STRING_OR_MISSING__STRING = AbstractionsPackage.ANAMED___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___STRING_IS_EMPTY__STRING = AbstractionsPackage.ANAMED___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___APACKAGE_FROM_URI__STRING = AbstractionsPackage.ANAMED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.ANAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.ANAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_ASTRING_CLASS = AbstractionsPackage.ANAMED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_AREAL_CLASS = AbstractionsPackage.ANAMED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_AINTEGER_CLASS = AbstractionsPackage.ANAMED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_AOBJECT_CLASS = AbstractionsPackage.ANAMED___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER = AbstractionsPackage.ANAMED_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>AClassifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER_OPERATION_COUNT = AbstractionsPackage.ANAMED_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.ADataTypeImpl <em>AData Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.ADataTypeImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getADataType()
	 * @generated
	 */
	int ADATA_TYPE = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ALABEL = ACLASSIFIER__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AKIND_BASE = ACLASSIFIER__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ARENDERED_KIND = ACLASSIFIER__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ACONTAINING_COMPONENT = ACLASSIFIER__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TPACKAGE_URI = ACLASSIFIER__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TCLASSIFIER_NAME = ACLASSIFIER__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TFEATURE_NAME = ACLASSIFIER__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TPACKAGE = ACLASSIFIER__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TCLASSIFIER = ACLASSIFIER__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TFEATURE = ACLASSIFIER__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__TA_CORE_ASTRING_CLASS = ACLASSIFIER__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ANAME = ACLASSIFIER__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AUNDEFINED_NAME_CONSTANT = ACLASSIFIER__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ABUSINESS_NAME = ACLASSIFIER__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ASPECIALIZED_CLASSIFIER = ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AACTIVE_DATA_TYPE = ACLASSIFIER__AACTIVE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AACTIVE_ENUMERATION = ACLASSIFIER__AACTIVE_ENUMERATION;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AACTIVE_CLASS = ACLASSIFIER__AACTIVE_CLASS;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ACONTAINING_PACKAGE = ACLASSIFIER__ACONTAINING_PACKAGE;

	/**
	 * The feature id for the '<em><b>As Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AS_DATA_TYPE = ACLASSIFIER__AS_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>As Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AS_CLASS = ACLASSIFIER__AS_CLASS;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AIS_STRING_CLASSIFIER = ACLASSIFIER__AIS_STRING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>ASpecialized Data Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ASPECIALIZED_DATA_TYPE = ACLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AData Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ADATA_TYPE_PACKAGE = ACLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AData Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE_FEATURE_COUNT = ACLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___INDENT_LEVEL = ACLASSIFIER___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___INDENTATION_SPACES = ACLASSIFIER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___INDENTATION_SPACES__INTEGER = ACLASSIFIER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___STRING_OR_MISSING__STRING = ACLASSIFIER___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___STRING_IS_EMPTY__STRING = ACLASSIFIER___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ACLASSIFIER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ACLASSIFIER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___APACKAGE_FROM_URI__STRING = ACLASSIFIER___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ACLASSIFIER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ACLASSIFIER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_ASTRING_CLASS = ACLASSIFIER___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_AREAL_CLASS = ACLASSIFIER___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_AINTEGER_CLASS = ACLASSIFIER___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_AOBJECT_CLASS = ACLASSIFIER___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AASSIGNABLE_TO__ACLASSIFIER = ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER;

	/**
	 * The number of operations of the '<em>AData Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE_OPERATION_COUNT = ACLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AEnumerationImpl <em>AEnumeration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AEnumerationImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAEnumeration()
	 * @generated
	 */
	int AENUMERATION = 2;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ALABEL = ADATA_TYPE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AKIND_BASE = ADATA_TYPE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ARENDERED_KIND = ADATA_TYPE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ACONTAINING_COMPONENT = ADATA_TYPE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TPACKAGE_URI = ADATA_TYPE__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TCLASSIFIER_NAME = ADATA_TYPE__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TFEATURE_NAME = ADATA_TYPE__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TPACKAGE = ADATA_TYPE__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TCLASSIFIER = ADATA_TYPE__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TFEATURE = ADATA_TYPE__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__TA_CORE_ASTRING_CLASS = ADATA_TYPE__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ANAME = ADATA_TYPE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AUNDEFINED_NAME_CONSTANT = ADATA_TYPE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ABUSINESS_NAME = ADATA_TYPE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ASPECIALIZED_CLASSIFIER = ADATA_TYPE__ASPECIALIZED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AACTIVE_DATA_TYPE = ADATA_TYPE__AACTIVE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AACTIVE_ENUMERATION = ADATA_TYPE__AACTIVE_ENUMERATION;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AACTIVE_CLASS = ADATA_TYPE__AACTIVE_CLASS;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ACONTAINING_PACKAGE = ADATA_TYPE__ACONTAINING_PACKAGE;

	/**
	 * The feature id for the '<em><b>As Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AS_DATA_TYPE = ADATA_TYPE__AS_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>As Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AS_CLASS = ADATA_TYPE__AS_CLASS;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AIS_STRING_CLASSIFIER = ADATA_TYPE__AIS_STRING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>ASpecialized Data Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ASPECIALIZED_DATA_TYPE = ADATA_TYPE__ASPECIALIZED_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AData Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ADATA_TYPE_PACKAGE = ADATA_TYPE__ADATA_TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>ALiteral</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ALITERAL = ADATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AEnumeration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION_FEATURE_COUNT = ADATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___INDENT_LEVEL = ADATA_TYPE___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___INDENTATION_SPACES = ADATA_TYPE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___INDENTATION_SPACES__INTEGER = ADATA_TYPE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___STRING_OR_MISSING__STRING = ADATA_TYPE___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___STRING_IS_EMPTY__STRING = ADATA_TYPE___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ADATA_TYPE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ADATA_TYPE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___APACKAGE_FROM_URI__STRING = ADATA_TYPE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ADATA_TYPE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ADATA_TYPE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_ASTRING_CLASS = ADATA_TYPE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_AREAL_CLASS = ADATA_TYPE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_AINTEGER_CLASS = ADATA_TYPE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_AOBJECT_CLASS = ADATA_TYPE___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AASSIGNABLE_TO__ACLASSIFIER = ADATA_TYPE___AASSIGNABLE_TO__ACLASSIFIER;

	/**
	 * The number of operations of the '<em>AEnumeration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION_OPERATION_COUNT = ADATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AClassImpl <em>AClass</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AClassImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAClass()
	 * @generated
	 */
	int ACLASS = 3;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ALABEL = ACLASSIFIER__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AKIND_BASE = ACLASSIFIER__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ARENDERED_KIND = ACLASSIFIER__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ACONTAINING_COMPONENT = ACLASSIFIER__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TPACKAGE_URI = ACLASSIFIER__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TCLASSIFIER_NAME = ACLASSIFIER__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TFEATURE_NAME = ACLASSIFIER__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TPACKAGE = ACLASSIFIER__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TCLASSIFIER = ACLASSIFIER__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TFEATURE = ACLASSIFIER__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__TA_CORE_ASTRING_CLASS = ACLASSIFIER__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ANAME = ACLASSIFIER__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AUNDEFINED_NAME_CONSTANT = ACLASSIFIER__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ABUSINESS_NAME = ACLASSIFIER__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ASPECIALIZED_CLASSIFIER = ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AACTIVE_DATA_TYPE = ACLASSIFIER__AACTIVE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AACTIVE_ENUMERATION = ACLASSIFIER__AACTIVE_ENUMERATION;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AACTIVE_CLASS = ACLASSIFIER__AACTIVE_CLASS;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ACONTAINING_PACKAGE = ACLASSIFIER__ACONTAINING_PACKAGE;

	/**
	 * The feature id for the '<em><b>As Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AS_DATA_TYPE = ACLASSIFIER__AS_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>As Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AS_CLASS = ACLASSIFIER__AS_CLASS;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AIS_STRING_CLASSIFIER = ACLASSIFIER__AIS_STRING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AAbstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AABSTRACT = ACLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ASpecialized Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ASPECIALIZED_CLASS = ACLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AFeature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AFEATURE = ACLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AOperation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AOPERATION = ACLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AAll Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AALL_FEATURE = ACLASSIFIER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AAll Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AALL_OPERATION = ACLASSIFIER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>ASuper Types Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__ASUPER_TYPES_LABEL = ACLASSIFIER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AAll Stored Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS__AALL_STORED_FEATURE = ACLASSIFIER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>AClass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_FEATURE_COUNT = ACLASSIFIER_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___INDENT_LEVEL = ACLASSIFIER___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___INDENTATION_SPACES = ACLASSIFIER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___INDENTATION_SPACES__INTEGER = ACLASSIFIER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___STRING_OR_MISSING__STRING = ACLASSIFIER___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___STRING_IS_EMPTY__STRING = ACLASSIFIER___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ACLASSIFIER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ACLASSIFIER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___APACKAGE_FROM_URI__STRING = ACLASSIFIER___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ACLASSIFIER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ACLASSIFIER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___ACORE_ASTRING_CLASS = ACLASSIFIER___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___ACORE_AREAL_CLASS = ACLASSIFIER___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___ACORE_AINTEGER_CLASS = ACLASSIFIER___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___ACORE_AOBJECT_CLASS = ACLASSIFIER___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___AASSIGNABLE_TO__ACLASSIFIER = ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER;

	/**
	 * The operation id for the '<em>Create</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___CREATE = ACLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>AFeature From Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___AFEATURE_FROM_NAME__STRING = ACLASSIFIER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>AOperation From Name And Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER = ACLASSIFIER_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>AClass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_OPERATION_COUNT = ACLASSIFIER_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.ATypedImpl <em>ATyped</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.ATypedImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getATyped()
	 * @generated
	 */
	int ATYPED = 4;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ALABEL = AbstractionsPackage.AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AKIND_BASE = AbstractionsPackage.AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ARENDERED_KIND = AbstractionsPackage.AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ACONTAINING_COMPONENT = AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TPACKAGE_URI = AbstractionsPackage.AELEMENT__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TCLASSIFIER_NAME = AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TFEATURE_NAME = AbstractionsPackage.AELEMENT__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TPACKAGE = AbstractionsPackage.AELEMENT__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TCLASSIFIER = AbstractionsPackage.AELEMENT__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TFEATURE = AbstractionsPackage.AELEMENT__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__TA_CORE_ASTRING_CLASS = AbstractionsPackage.AELEMENT__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ACLASSIFIER = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AMANDATORY = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ASINGULAR = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ATYPE_LABEL = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AUNDEFINED_TYPE_CONSTANT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>ATyped</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED_FEATURE_COUNT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___INDENT_LEVEL = AbstractionsPackage.AELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___INDENTATION_SPACES = AbstractionsPackage.AELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___INDENTATION_SPACES__INTEGER = AbstractionsPackage.AELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___STRING_OR_MISSING__STRING = AbstractionsPackage.AELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___STRING_IS_EMPTY__STRING = AbstractionsPackage.AELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___APACKAGE_FROM_URI__STRING = AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_ASTRING_CLASS = AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_AREAL_CLASS = AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_AINTEGER_CLASS = AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_AOBJECT_CLASS = AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>ATyped</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED_OPERATION_COUNT = AbstractionsPackage.AELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AFeatureImpl <em>AFeature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AFeatureImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAFeature()
	 * @generated
	 */
	int AFEATURE = 5;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ALABEL = ATYPED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AKIND_BASE = ATYPED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ARENDERED_KIND = ATYPED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACONTAINING_COMPONENT = ATYPED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TPACKAGE_URI = ATYPED__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TCLASSIFIER_NAME = ATYPED__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TFEATURE_NAME = ATYPED__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TPACKAGE = ATYPED__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TCLASSIFIER = ATYPED__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TFEATURE = ATYPED__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__TA_CORE_ASTRING_CLASS = ATYPED__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACLASSIFIER = ATYPED__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AMANDATORY = ATYPED__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ASINGULAR = ATYPED__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ATYPE_LABEL = ATYPED__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AUNDEFINED_TYPE_CONSTANT = ATYPED__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ANAME = ATYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AUNDEFINED_NAME_CONSTANT = ATYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ABUSINESS_NAME = ATYPED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ASTORED = ATYPED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__APERSISTED = ATYPED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACHANGEABLE = ATYPED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AACTIVE_FEATURE = ATYPED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACONTAINING_CLASS = ATYPED_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>AFeature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_FEATURE_COUNT = ATYPED_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___INDENT_LEVEL = ATYPED___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___INDENTATION_SPACES = ATYPED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___INDENTATION_SPACES__INTEGER = ATYPED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___STRING_OR_MISSING__STRING = ATYPED___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___STRING_IS_EMPTY__STRING = ATYPED___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___APACKAGE_FROM_URI__STRING = ATYPED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_ASTRING_CLASS = ATYPED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_AREAL_CLASS = ATYPED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_AINTEGER_CLASS = ATYPED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_AOBJECT_CLASS = ATYPED___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AFeature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_OPERATION_COUNT = ATYPED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AAttributeImpl <em>AAttribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AAttributeImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAAttribute()
	 * @generated
	 */
	int AATTRIBUTE = 6;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ALABEL = AFEATURE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AKIND_BASE = AFEATURE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ARENDERED_KIND = AFEATURE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACONTAINING_COMPONENT = AFEATURE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TPACKAGE_URI = AFEATURE__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TCLASSIFIER_NAME = AFEATURE__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TFEATURE_NAME = AFEATURE__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TPACKAGE = AFEATURE__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TCLASSIFIER = AFEATURE__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TFEATURE = AFEATURE__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__TA_CORE_ASTRING_CLASS = AFEATURE__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACLASSIFIER = AFEATURE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AMANDATORY = AFEATURE__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ASINGULAR = AFEATURE__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ATYPE_LABEL = AFEATURE__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AUNDEFINED_TYPE_CONSTANT = AFEATURE__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ANAME = AFEATURE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AUNDEFINED_NAME_CONSTANT = AFEATURE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ABUSINESS_NAME = AFEATURE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ASTORED = AFEATURE__ASTORED;

	/**
	 * The feature id for the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__APERSISTED = AFEATURE__APERSISTED;

	/**
	 * The feature id for the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACHANGEABLE = AFEATURE__ACHANGEABLE;

	/**
	 * The feature id for the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AACTIVE_FEATURE = AFEATURE__AACTIVE_FEATURE;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACONTAINING_CLASS = AFEATURE__ACONTAINING_CLASS;

	/**
	 * The feature id for the '<em><b>AData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ADATA_TYPE = AFEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AActive Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AACTIVE_ATTRIBUTE = AFEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AAttribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE_FEATURE_COUNT = AFEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___INDENT_LEVEL = AFEATURE___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___INDENTATION_SPACES = AFEATURE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___INDENTATION_SPACES__INTEGER = AFEATURE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___STRING_OR_MISSING__STRING = AFEATURE___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___STRING_IS_EMPTY__STRING = AFEATURE___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AFEATURE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AFEATURE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___APACKAGE_FROM_URI__STRING = AFEATURE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AFEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AFEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_ASTRING_CLASS = AFEATURE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_AREAL_CLASS = AFEATURE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_AINTEGER_CLASS = AFEATURE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_AOBJECT_CLASS = AFEATURE___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AAttribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE_OPERATION_COUNT = AFEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AReferenceImpl <em>AReference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AReferenceImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAReference()
	 * @generated
	 */
	int AREFERENCE = 7;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ALABEL = AFEATURE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AKIND_BASE = AFEATURE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ARENDERED_KIND = AFEATURE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACONTAINING_COMPONENT = AFEATURE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TPACKAGE_URI = AFEATURE__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TCLASSIFIER_NAME = AFEATURE__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TFEATURE_NAME = AFEATURE__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TPACKAGE = AFEATURE__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TCLASSIFIER = AFEATURE__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TFEATURE = AFEATURE__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__TA_CORE_ASTRING_CLASS = AFEATURE__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACLASSIFIER = AFEATURE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AMANDATORY = AFEATURE__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ASINGULAR = AFEATURE__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ATYPE_LABEL = AFEATURE__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AUNDEFINED_TYPE_CONSTANT = AFEATURE__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ANAME = AFEATURE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AUNDEFINED_NAME_CONSTANT = AFEATURE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ABUSINESS_NAME = AFEATURE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ASTORED = AFEATURE__ASTORED;

	/**
	 * The feature id for the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__APERSISTED = AFEATURE__APERSISTED;

	/**
	 * The feature id for the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACHANGEABLE = AFEATURE__ACHANGEABLE;

	/**
	 * The feature id for the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AACTIVE_FEATURE = AFEATURE__AACTIVE_FEATURE;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACONTAINING_CLASS = AFEATURE__ACONTAINING_CLASS;

	/**
	 * The feature id for the '<em><b>AClass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACLASS = AFEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AContainement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACONTAINEMENT = AFEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AActive Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AACTIVE_REFERENCE = AFEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AReference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE_FEATURE_COUNT = AFEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___INDENT_LEVEL = AFEATURE___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___INDENTATION_SPACES = AFEATURE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___INDENTATION_SPACES__INTEGER = AFEATURE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___STRING_OR_MISSING__STRING = AFEATURE___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___STRING_IS_EMPTY__STRING = AFEATURE___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AFEATURE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AFEATURE___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___APACKAGE_FROM_URI__STRING = AFEATURE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AFEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AFEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_ASTRING_CLASS = AFEATURE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_AREAL_CLASS = AFEATURE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_AINTEGER_CLASS = AFEATURE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_AOBJECT_CLASS = AFEATURE___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AReference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE_OPERATION_COUNT = AFEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AOperationImpl <em>AOperation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AOperationImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAOperation()
	 * @generated
	 */
	int AOPERATION = 8;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ALABEL = ATYPED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AKIND_BASE = ATYPED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ARENDERED_KIND = ATYPED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ACONTAINING_COMPONENT = ATYPED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TPACKAGE_URI = ATYPED__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TCLASSIFIER_NAME = ATYPED__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TFEATURE_NAME = ATYPED__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TPACKAGE = ATYPED__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TCLASSIFIER = ATYPED__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TFEATURE = ATYPED__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__TA_CORE_ASTRING_CLASS = ATYPED__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ACLASSIFIER = ATYPED__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AMANDATORY = ATYPED__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ASINGULAR = ATYPED__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ATYPE_LABEL = ATYPED__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AUNDEFINED_TYPE_CONSTANT = ATYPED__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ANAME = ATYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AUNDEFINED_NAME_CONSTANT = ATYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ABUSINESS_NAME = ATYPED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AParameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__APARAMETER = ATYPED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>AOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_FEATURE_COUNT = ATYPED_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___INDENT_LEVEL = ATYPED___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___INDENTATION_SPACES = ATYPED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___INDENTATION_SPACES__INTEGER = ATYPED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___STRING_OR_MISSING__STRING = ATYPED___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___STRING_IS_EMPTY__STRING = ATYPED___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___APACKAGE_FROM_URI__STRING = ATYPED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_ASTRING_CLASS = ATYPED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_AREAL_CLASS = ATYPED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_AINTEGER_CLASS = ATYPED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_AOBJECT_CLASS = ATYPED___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_OPERATION_COUNT = ATYPED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.langlets.acore.classifiers.impl.AParameterImpl <em>AParameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.AParameterImpl
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAParameter()
	 * @generated
	 */
	int APARAMETER = 9;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ALABEL = ATYPED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AKIND_BASE = ATYPED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ARENDERED_KIND = ATYPED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ACONTAINING_COMPONENT = ATYPED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TPACKAGE_URI = ATYPED__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TCLASSIFIER_NAME = ATYPED__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TFEATURE_NAME = ATYPED__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TPACKAGE = ATYPED__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TCLASSIFIER = ATYPED__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TFEATURE = ATYPED__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__TA_CORE_ASTRING_CLASS = ATYPED__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ACLASSIFIER = ATYPED__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AMANDATORY = ATYPED__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ASINGULAR = ATYPED__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ATYPE_LABEL = ATYPED__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AUNDEFINED_TYPE_CONSTANT = ATYPED__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ANAME = ATYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AUNDEFINED_NAME_CONSTANT = ATYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ABUSINESS_NAME = ATYPED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AParameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER_FEATURE_COUNT = ATYPED_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___INDENT_LEVEL = ATYPED___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___INDENTATION_SPACES = ATYPED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___INDENTATION_SPACES__INTEGER = ATYPED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___STRING_OR_MISSING__STRING = ATYPED___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___STRING_IS_EMPTY__STRING = ATYPED___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ATYPED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___APACKAGE_FROM_URI__STRING = ATYPED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_ASTRING_CLASS = ATYPED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_AREAL_CLASS = ATYPED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_AINTEGER_CLASS = ATYPED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_AOBJECT_CLASS = ATYPED___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AParameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER_OPERATION_COUNT = ATYPED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>AString</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAString()
	 * @generated
	 */
	int ASTRING = 10;

	/**
	 * The meta object id for the '<em>AReal</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAReal()
	 * @generated
	 */
	int AREAL = 11;

	/**
	 * The meta object id for the '<em>AInteger</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAInteger()
	 * @generated
	 */
	int AINTEGER = 12;

	/**
	 * The meta object id for the '<em>ABoolean</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getABoolean()
	 * @generated
	 */
	int ABOOLEAN = 13;

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AClassifier</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier
	 * @generated
	 */
	EClass getAClassifier();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClassifier#getASpecializedClassifier <em>ASpecialized Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASpecialized Classifier</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getASpecializedClassifier()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_ASpecializedClassifier();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AClassifier#getAActiveDataType <em>AActive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Data Type</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAActiveDataType()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AActiveDataType();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AClassifier#getAActiveEnumeration <em>AActive Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Enumeration</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAActiveEnumeration()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AActiveEnumeration();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AClassifier#getAActiveClass <em>AActive Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Class</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAActiveClass()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AActiveClass();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.AClassifier#getAContainingPackage <em>AContaining Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Package</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAContainingPackage()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_AContainingPackage();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.AClassifier#getAsDataType <em>As Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>As Data Type</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAsDataType()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_AsDataType();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.AClassifier#getAsClass <em>As Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>As Class</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAsClass()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_AsClass();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AClassifier#getAIsStringClassifier <em>AIs String Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AIs String Classifier</em>'.
	 * @see org.langlets.acore.classifiers.AClassifier#getAIsStringClassifier()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AIsStringClassifier();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.classifiers.AClassifier#aAssignableTo(org.langlets.acore.classifiers.AClassifier) <em>AAssignable To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AAssignable To</em>' operation.
	 * @see org.langlets.acore.classifiers.AClassifier#aAssignableTo(org.langlets.acore.classifiers.AClassifier)
	 * @generated
	 */
	EOperation getAClassifier__AAssignableTo__AClassifier();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.ADataType <em>AData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AData Type</em>'.
	 * @see org.langlets.acore.classifiers.ADataType
	 * @generated
	 */
	EClass getADataType();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.ADataType#getASpecializedDataType <em>ASpecialized Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASpecialized Data Type</em>'.
	 * @see org.langlets.acore.classifiers.ADataType#getASpecializedDataType()
	 * @see #getADataType()
	 * @generated
	 */
	EReference getADataType_ASpecializedDataType();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.ADataType#getADataTypePackage <em>AData Type Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AData Type Package</em>'.
	 * @see org.langlets.acore.classifiers.ADataType#getADataTypePackage()
	 * @see #getADataType()
	 * @generated
	 */
	EReference getADataType_ADataTypePackage();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AEnumeration <em>AEnumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AEnumeration</em>'.
	 * @see org.langlets.acore.classifiers.AEnumeration
	 * @generated
	 */
	EClass getAEnumeration();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AEnumeration#getALiteral <em>ALiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ALiteral</em>'.
	 * @see org.langlets.acore.classifiers.AEnumeration#getALiteral()
	 * @see #getAEnumeration()
	 * @generated
	 */
	EReference getAEnumeration_ALiteral();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AClass <em>AClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AClass</em>'.
	 * @see org.langlets.acore.classifiers.AClass
	 * @generated
	 */
	EClass getAClass();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AClass#getAAbstract <em>AAbstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AAbstract</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getAAbstract()
	 * @see #getAClass()
	 * @generated
	 */
	EAttribute getAClass_AAbstract();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClass#getASpecializedClass <em>ASpecialized Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASpecialized Class</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getASpecializedClass()
	 * @see #getAClass()
	 * @generated
	 */
	EReference getAClass_ASpecializedClass();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClass#getAFeature <em>AFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AFeature</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getAFeature()
	 * @see #getAClass()
	 * @generated
	 */
	EReference getAClass_AFeature();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClass#getAOperation <em>AOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AOperation</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getAOperation()
	 * @see #getAClass()
	 * @generated
	 */
	EReference getAClass_AOperation();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClass#getAAllFeature <em>AAll Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Feature</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getAAllFeature()
	 * @see #getAClass()
	 * @generated
	 */
	EReference getAClass_AAllFeature();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClass#getAAllOperation <em>AAll Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Operation</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getAAllOperation()
	 * @see #getAClass()
	 * @generated
	 */
	EReference getAClass_AAllOperation();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AClass#getASuperTypesLabel <em>ASuper Types Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ASuper Types Label</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getASuperTypesLabel()
	 * @see #getAClass()
	 * @generated
	 */
	EAttribute getAClass_ASuperTypesLabel();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AClass#getAAllStoredFeature <em>AAll Stored Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Stored Feature</em>'.
	 * @see org.langlets.acore.classifiers.AClass#getAAllStoredFeature()
	 * @see #getAClass()
	 * @generated
	 */
	EReference getAClass_AAllStoredFeature();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.classifiers.AClass#create() <em>Create</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create</em>' operation.
	 * @see org.langlets.acore.classifiers.AClass#create()
	 * @generated
	 */
	EOperation getAClass__Create();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.classifiers.AClass#aFeatureFromName(java.lang.String) <em>AFeature From Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AFeature From Name</em>' operation.
	 * @see org.langlets.acore.classifiers.AClass#aFeatureFromName(java.lang.String)
	 * @generated
	 */
	EOperation getAClass__AFeatureFromName__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.classifiers.AClass#aOperationFromNameAndTypes(java.lang.String, org.langlets.acore.classifiers.AClassifier) <em>AOperation From Name And Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AOperation From Name And Types</em>' operation.
	 * @see org.langlets.acore.classifiers.AClass#aOperationFromNameAndTypes(java.lang.String, org.langlets.acore.classifiers.AClassifier)
	 * @generated
	 */
	EOperation getAClass__AOperationFromNameAndTypes__String_AClassifier();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.ATyped <em>ATyped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ATyped</em>'.
	 * @see org.langlets.acore.classifiers.ATyped
	 * @generated
	 */
	EClass getATyped();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.ATyped#getAClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AClassifier</em>'.
	 * @see org.langlets.acore.classifiers.ATyped#getAClassifier()
	 * @see #getATyped()
	 * @generated
	 */
	EReference getATyped_AClassifier();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.ATyped#getAMandatory <em>AMandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AMandatory</em>'.
	 * @see org.langlets.acore.classifiers.ATyped#getAMandatory()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_AMandatory();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.ATyped#getASingular <em>ASingular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ASingular</em>'.
	 * @see org.langlets.acore.classifiers.ATyped#getASingular()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_ASingular();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.ATyped#getATypeLabel <em>AType Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AType Label</em>'.
	 * @see org.langlets.acore.classifiers.ATyped#getATypeLabel()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_ATypeLabel();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.ATyped#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUndefined Type Constant</em>'.
	 * @see org.langlets.acore.classifiers.ATyped#getAUndefinedTypeConstant()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_AUndefinedTypeConstant();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AFeature <em>AFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AFeature</em>'.
	 * @see org.langlets.acore.classifiers.AFeature
	 * @generated
	 */
	EClass getAFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AFeature#getAStored <em>AStored</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AStored</em>'.
	 * @see org.langlets.acore.classifiers.AFeature#getAStored()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_AStored();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AFeature#getAPersisted <em>APersisted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>APersisted</em>'.
	 * @see org.langlets.acore.classifiers.AFeature#getAPersisted()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_APersisted();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AFeature#getAChangeable <em>AChangeable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AChangeable</em>'.
	 * @see org.langlets.acore.classifiers.AFeature#getAChangeable()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_AChangeable();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AFeature#getAActiveFeature <em>AActive Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Feature</em>'.
	 * @see org.langlets.acore.classifiers.AFeature#getAActiveFeature()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_AActiveFeature();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.AFeature#getAContainingClass <em>AContaining Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Class</em>'.
	 * @see org.langlets.acore.classifiers.AFeature#getAContainingClass()
	 * @see #getAFeature()
	 * @generated
	 */
	EReference getAFeature_AContainingClass();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AAttribute <em>AAttribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AAttribute</em>'.
	 * @see org.langlets.acore.classifiers.AAttribute
	 * @generated
	 */
	EClass getAAttribute();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.AAttribute#getADataType <em>AData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AData Type</em>'.
	 * @see org.langlets.acore.classifiers.AAttribute#getADataType()
	 * @see #getAAttribute()
	 * @generated
	 */
	EReference getAAttribute_ADataType();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AAttribute#getAActiveAttribute <em>AActive Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Attribute</em>'.
	 * @see org.langlets.acore.classifiers.AAttribute#getAActiveAttribute()
	 * @see #getAAttribute()
	 * @generated
	 */
	EAttribute getAAttribute_AActiveAttribute();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AReference <em>AReference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AReference</em>'.
	 * @see org.langlets.acore.classifiers.AReference
	 * @generated
	 */
	EClass getAReference();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.classifiers.AReference#getAClass <em>AClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AClass</em>'.
	 * @see org.langlets.acore.classifiers.AReference#getAClass()
	 * @see #getAReference()
	 * @generated
	 */
	EReference getAReference_AClass();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AReference#getAContainement <em>AContainement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AContainement</em>'.
	 * @see org.langlets.acore.classifiers.AReference#getAContainement()
	 * @see #getAReference()
	 * @generated
	 */
	EAttribute getAReference_AContainement();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.classifiers.AReference#getAActiveReference <em>AActive Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Reference</em>'.
	 * @see org.langlets.acore.classifiers.AReference#getAActiveReference()
	 * @see #getAReference()
	 * @generated
	 */
	EAttribute getAReference_AActiveReference();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AOperation <em>AOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AOperation</em>'.
	 * @see org.langlets.acore.classifiers.AOperation
	 * @generated
	 */
	EClass getAOperation();

	/**
	 * Returns the meta object for the reference list '{@link org.langlets.acore.classifiers.AOperation#getAParameter <em>AParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AParameter</em>'.
	 * @see org.langlets.acore.classifiers.AOperation#getAParameter()
	 * @see #getAOperation()
	 * @generated
	 */
	EReference getAOperation_AParameter();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.classifiers.AParameter <em>AParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AParameter</em>'.
	 * @see org.langlets.acore.classifiers.AParameter
	 * @generated
	 */
	EClass getAParameter();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>AString</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>AString</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getAString();

	/**
	 * Returns the meta object for data type '<em>AReal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>AReal</em>'.
	 * @model instanceClass="double"
	 * @generated
	 */
	EDataType getAReal();

	/**
	 * Returns the meta object for data type '<em>AInteger</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>AInteger</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getAInteger();

	/**
	 * Returns the meta object for data type '<em>ABoolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ABoolean</em>'.
	 * @model instanceClass="boolean"
	 * @generated
	 */
	EDataType getABoolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassifiersFactory getClassifiersFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AClassifierImpl <em>AClassifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AClassifierImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAClassifier()
		 * @generated
		 */
		EClass ACLASSIFIER = eINSTANCE.getAClassifier();

		/**
		 * The meta object literal for the '<em><b>ASpecialized Classifier</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__ASPECIALIZED_CLASSIFIER = eINSTANCE.getAClassifier_ASpecializedClassifier();

		/**
		 * The meta object literal for the '<em><b>AActive Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AACTIVE_DATA_TYPE = eINSTANCE.getAClassifier_AActiveDataType();

		/**
		 * The meta object literal for the '<em><b>AActive Enumeration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AACTIVE_ENUMERATION = eINSTANCE.getAClassifier_AActiveEnumeration();

		/**
		 * The meta object literal for the '<em><b>AActive Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AACTIVE_CLASS = eINSTANCE.getAClassifier_AActiveClass();

		/**
		 * The meta object literal for the '<em><b>AContaining Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__ACONTAINING_PACKAGE = eINSTANCE.getAClassifier_AContainingPackage();

		/**
		 * The meta object literal for the '<em><b>As Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__AS_DATA_TYPE = eINSTANCE.getAClassifier_AsDataType();

		/**
		 * The meta object literal for the '<em><b>As Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__AS_CLASS = eINSTANCE.getAClassifier_AsClass();

		/**
		 * The meta object literal for the '<em><b>AIs String Classifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AIS_STRING_CLASSIFIER = eINSTANCE.getAClassifier_AIsStringClassifier();

		/**
		 * The meta object literal for the '<em><b>AAssignable To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER = eINSTANCE.getAClassifier__AAssignableTo__AClassifier();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.ADataTypeImpl <em>AData Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.ADataTypeImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getADataType()
		 * @generated
		 */
		EClass ADATA_TYPE = eINSTANCE.getADataType();

		/**
		 * The meta object literal for the '<em><b>ASpecialized Data Type</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADATA_TYPE__ASPECIALIZED_DATA_TYPE = eINSTANCE.getADataType_ASpecializedDataType();

		/**
		 * The meta object literal for the '<em><b>AData Type Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADATA_TYPE__ADATA_TYPE_PACKAGE = eINSTANCE.getADataType_ADataTypePackage();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AEnumerationImpl <em>AEnumeration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AEnumerationImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAEnumeration()
		 * @generated
		 */
		EClass AENUMERATION = eINSTANCE.getAEnumeration();

		/**
		 * The meta object literal for the '<em><b>ALiteral</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AENUMERATION__ALITERAL = eINSTANCE.getAEnumeration_ALiteral();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AClassImpl <em>AClass</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AClassImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAClass()
		 * @generated
		 */
		EClass ACLASS = eINSTANCE.getAClass();

		/**
		 * The meta object literal for the '<em><b>AAbstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASS__AABSTRACT = eINSTANCE.getAClass_AAbstract();

		/**
		 * The meta object literal for the '<em><b>ASpecialized Class</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS__ASPECIALIZED_CLASS = eINSTANCE.getAClass_ASpecializedClass();

		/**
		 * The meta object literal for the '<em><b>AFeature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS__AFEATURE = eINSTANCE.getAClass_AFeature();

		/**
		 * The meta object literal for the '<em><b>AOperation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS__AOPERATION = eINSTANCE.getAClass_AOperation();

		/**
		 * The meta object literal for the '<em><b>AAll Feature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS__AALL_FEATURE = eINSTANCE.getAClass_AAllFeature();

		/**
		 * The meta object literal for the '<em><b>AAll Operation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS__AALL_OPERATION = eINSTANCE.getAClass_AAllOperation();

		/**
		 * The meta object literal for the '<em><b>ASuper Types Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASS__ASUPER_TYPES_LABEL = eINSTANCE.getAClass_ASuperTypesLabel();

		/**
		 * The meta object literal for the '<em><b>AAll Stored Feature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS__AALL_STORED_FEATURE = eINSTANCE.getAClass_AAllStoredFeature();

		/**
		 * The meta object literal for the '<em><b>Create</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS___CREATE = eINSTANCE.getAClass__Create();

		/**
		 * The meta object literal for the '<em><b>AFeature From Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS___AFEATURE_FROM_NAME__STRING = eINSTANCE.getAClass__AFeatureFromName__String();

		/**
		 * The meta object literal for the '<em><b>AOperation From Name And Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER = eINSTANCE
				.getAClass__AOperationFromNameAndTypes__String_AClassifier();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.ATypedImpl <em>ATyped</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.ATypedImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getATyped()
		 * @generated
		 */
		EClass ATYPED = eINSTANCE.getATyped();

		/**
		 * The meta object literal for the '<em><b>AClassifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATYPED__ACLASSIFIER = eINSTANCE.getATyped_AClassifier();

		/**
		 * The meta object literal for the '<em><b>AMandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__AMANDATORY = eINSTANCE.getATyped_AMandatory();

		/**
		 * The meta object literal for the '<em><b>ASingular</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__ASINGULAR = eINSTANCE.getATyped_ASingular();

		/**
		 * The meta object literal for the '<em><b>AType Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__ATYPE_LABEL = eINSTANCE.getATyped_ATypeLabel();

		/**
		 * The meta object literal for the '<em><b>AUndefined Type Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__AUNDEFINED_TYPE_CONSTANT = eINSTANCE.getATyped_AUndefinedTypeConstant();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AFeatureImpl <em>AFeature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AFeatureImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAFeature()
		 * @generated
		 */
		EClass AFEATURE = eINSTANCE.getAFeature();

		/**
		 * The meta object literal for the '<em><b>AStored</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__ASTORED = eINSTANCE.getAFeature_AStored();

		/**
		 * The meta object literal for the '<em><b>APersisted</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__APERSISTED = eINSTANCE.getAFeature_APersisted();

		/**
		 * The meta object literal for the '<em><b>AChangeable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__ACHANGEABLE = eINSTANCE.getAFeature_AChangeable();

		/**
		 * The meta object literal for the '<em><b>AActive Feature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__AACTIVE_FEATURE = eINSTANCE.getAFeature_AActiveFeature();

		/**
		 * The meta object literal for the '<em><b>AContaining Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AFEATURE__ACONTAINING_CLASS = eINSTANCE.getAFeature_AContainingClass();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AAttributeImpl <em>AAttribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AAttributeImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAAttribute()
		 * @generated
		 */
		EClass AATTRIBUTE = eINSTANCE.getAAttribute();

		/**
		 * The meta object literal for the '<em><b>AData Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AATTRIBUTE__ADATA_TYPE = eINSTANCE.getAAttribute_ADataType();

		/**
		 * The meta object literal for the '<em><b>AActive Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AATTRIBUTE__AACTIVE_ATTRIBUTE = eINSTANCE.getAAttribute_AActiveAttribute();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AReferenceImpl <em>AReference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AReferenceImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAReference()
		 * @generated
		 */
		EClass AREFERENCE = eINSTANCE.getAReference();

		/**
		 * The meta object literal for the '<em><b>AClass</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AREFERENCE__ACLASS = eINSTANCE.getAReference_AClass();

		/**
		 * The meta object literal for the '<em><b>AContainement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AREFERENCE__ACONTAINEMENT = eINSTANCE.getAReference_AContainement();

		/**
		 * The meta object literal for the '<em><b>AActive Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AREFERENCE__AACTIVE_REFERENCE = eINSTANCE.getAReference_AActiveReference();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AOperationImpl <em>AOperation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AOperationImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAOperation()
		 * @generated
		 */
		EClass AOPERATION = eINSTANCE.getAOperation();

		/**
		 * The meta object literal for the '<em><b>AParameter</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOPERATION__APARAMETER = eINSTANCE.getAOperation_AParameter();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.classifiers.impl.AParameterImpl <em>AParameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.AParameterImpl
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAParameter()
		 * @generated
		 */
		EClass APARAMETER = eINSTANCE.getAParameter();

		/**
		 * The meta object literal for the '<em>AString</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAString()
		 * @generated
		 */
		EDataType ASTRING = eINSTANCE.getAString();

		/**
		 * The meta object literal for the '<em>AReal</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAReal()
		 * @generated
		 */
		EDataType AREAL = eINSTANCE.getAReal();

		/**
		 * The meta object literal for the '<em>AInteger</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getAInteger()
		 * @generated
		 */
		EDataType AINTEGER = eINSTANCE.getAInteger();

		/**
		 * The meta object literal for the '<em>ABoolean</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.classifiers.impl.ClassifiersPackageImpl#getABoolean()
		 * @generated
		 */
		EDataType ABOOLEAN = eINSTANCE.getABoolean();

	}

} //ClassifiersPackage
