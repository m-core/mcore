/**
 */
package org.langlets.acore.classifiers;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AReference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AReference#getAClass <em>AClass</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AReference#getAContainement <em>AContainement</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AReference#getAActiveReference <em>AActive Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAReference()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aClass\n' aActiveFeatureDerive='aActiveReference\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aClassifierCreateColumn='false'"
 * @generated
 */

public interface AReference extends AFeature {
	/**
	 * Returns the value of the '<em><b>AClass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClass</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClass</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAReference_AClass()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClass = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClass getAClass();

	/**
	 * Returns the value of the '<em><b>AContainement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContainement</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContainement</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAReference_AContainement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAContainement();

	/**
	 * Returns the value of the '<em><b>AActive Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Reference</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAReference_AActiveReference()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveReference();

} // AReference
