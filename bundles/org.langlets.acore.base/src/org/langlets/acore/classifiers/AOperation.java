/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AOperation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AOperation#getAParameter <em>AParameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAOperation()
 * @model abstract="true"
 * @generated
 */

public interface AOperation extends ATyped, ANamed {
	/**
	 * Returns the value of the '<em><b>AParameter</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AParameter</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AParameter</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAOperation_AParameter()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AParameter> getAParameter();

} // AOperation
