/**
 */
package org.langlets.acore.classifiers.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.abstractions.ANamed;
import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.classifiers.AClass;
import org.langlets.acore.classifiers.AFeature;
import org.langlets.acore.classifiers.ClassifiersPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AFeature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAName <em>AName</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getABusinessName <em>ABusiness Name</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAStored <em>AStored</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAPersisted <em>APersisted</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAChangeable <em>AChangeable</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAActiveFeature <em>AActive Feature</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AFeatureImpl#getAContainingClass <em>AContaining Class</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AFeatureImpl extends ATypedImpl implements AFeature {
	/**
	 * The default value of the '{@link #getAName() <em>AName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName()
	 * @generated
	 * @ordered
	 */
	protected static final String ANAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedNameConstant() <em>AUndefined Name Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_NAME_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABusinessName() <em>ABusiness Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName()
	 * @generated
	 * @ordered
	 */
	protected static final String ABUSINESS_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAStored() <em>AStored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAStored()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ASTORED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAPersisted() <em>APersisted</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPersisted()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean APERSISTED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAChangeable() <em>AChangeable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChangeable()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ACHANGEABLE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveFeature() <em>AActive Feature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveFeature()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_FEATURE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAName <em>AName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedNameConstant <em>AUndefined Name Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedNameConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABusinessName <em>ABusiness Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBusinessNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAStored <em>AStored</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAStored
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aStoredDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAPersisted <em>APersisted</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPersisted
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aPersistedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChangeable <em>AChangeable</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChangeable
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aChangeableDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveFeature <em>AActive Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingClass <em>AContaining Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.AFEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAName() {
		/**
		 * @OCL aUndefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ANAME;

		if (aNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedNameConstant() {
		/**
		 * @OCL '<A Name Is Undefined> '
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT;

		if (aUndefinedNameConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedNameConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedNameConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABusinessName() {
		/**
		 * @OCL let chain : String = aName in
		if chain.oclIsUndefined()
		then null
		else chain .camelCaseToBusiness()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME;

		if (aBusinessNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBusinessNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBusinessNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAStored() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__ASTORED;

		if (aStoredDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aStoredDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aStoredDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAPersisted() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__APERSISTED;

		if (aPersistedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aPersistedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aPersistedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAChangeable() {
		/**
		 * @OCL if (aStored) 
		=true 
		then true
		else false
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__ACHANGEABLE;

		if (aChangeableDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aChangeableDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChangeableDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveFeature() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__AACTIVE_FEATURE;

		if (aActiveFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClass getAContainingClass() {
		AClass aContainingClass = basicGetAContainingClass();
		return aContainingClass != null && aContainingClass.eIsProxy()
				? (AClass) eResolveProxy((InternalEObject) aContainingClass) : aContainingClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClass basicGetAContainingClass() {
		/**
		 * @OCL let nl: acore::classifiers::AClass = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__ACONTAINING_CLASS;

		if (aContainingClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClass result = (AClass) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ClassifiersPackage.AFEATURE__ANAME:
			return getAName();
		case ClassifiersPackage.AFEATURE__AUNDEFINED_NAME_CONSTANT:
			return getAUndefinedNameConstant();
		case ClassifiersPackage.AFEATURE__ABUSINESS_NAME:
			return getABusinessName();
		case ClassifiersPackage.AFEATURE__ASTORED:
			return getAStored();
		case ClassifiersPackage.AFEATURE__APERSISTED:
			return getAPersisted();
		case ClassifiersPackage.AFEATURE__ACHANGEABLE:
			return getAChangeable();
		case ClassifiersPackage.AFEATURE__AACTIVE_FEATURE:
			return getAActiveFeature();
		case ClassifiersPackage.AFEATURE__ACONTAINING_CLASS:
			if (resolve)
				return getAContainingClass();
			return basicGetAContainingClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ClassifiersPackage.AFEATURE__ANAME:
			return ANAME_EDEFAULT == null ? getAName() != null : !ANAME_EDEFAULT.equals(getAName());
		case ClassifiersPackage.AFEATURE__AUNDEFINED_NAME_CONSTANT:
			return AUNDEFINED_NAME_CONSTANT_EDEFAULT == null ? getAUndefinedNameConstant() != null
					: !AUNDEFINED_NAME_CONSTANT_EDEFAULT.equals(getAUndefinedNameConstant());
		case ClassifiersPackage.AFEATURE__ABUSINESS_NAME:
			return ABUSINESS_NAME_EDEFAULT == null ? getABusinessName() != null
					: !ABUSINESS_NAME_EDEFAULT.equals(getABusinessName());
		case ClassifiersPackage.AFEATURE__ASTORED:
			return ASTORED_EDEFAULT == null ? getAStored() != null : !ASTORED_EDEFAULT.equals(getAStored());
		case ClassifiersPackage.AFEATURE__APERSISTED:
			return APERSISTED_EDEFAULT == null ? getAPersisted() != null : !APERSISTED_EDEFAULT.equals(getAPersisted());
		case ClassifiersPackage.AFEATURE__ACHANGEABLE:
			return ACHANGEABLE_EDEFAULT == null ? getAChangeable() != null
					: !ACHANGEABLE_EDEFAULT.equals(getAChangeable());
		case ClassifiersPackage.AFEATURE__AACTIVE_FEATURE:
			return AACTIVE_FEATURE_EDEFAULT == null ? getAActiveFeature() != null
					: !AACTIVE_FEATURE_EDEFAULT.equals(getAActiveFeature());
		case ClassifiersPackage.AFEATURE__ACONTAINING_CLASS:
			return basicGetAContainingClass() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (derivedFeatureID) {
			case ClassifiersPackage.AFEATURE__ANAME:
				return AbstractionsPackage.ANAMED__ANAME;
			case ClassifiersPackage.AFEATURE__AUNDEFINED_NAME_CONSTANT:
				return AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;
			case ClassifiersPackage.AFEATURE__ABUSINESS_NAME:
				return AbstractionsPackage.ANAMED__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ANAMED__ANAME:
				return ClassifiersPackage.AFEATURE__ANAME;
			case AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
				return ClassifiersPackage.AFEATURE__AUNDEFINED_NAME_CONSTANT;
			case AbstractionsPackage.ANAMED__ABUSINESS_NAME:
				return ClassifiersPackage.AFEATURE__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel aName
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (ClassifiersPackage.Literals.AFEATURE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AFeatureImpl
