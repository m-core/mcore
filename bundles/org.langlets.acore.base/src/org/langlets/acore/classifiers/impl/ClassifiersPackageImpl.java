/**
 */
package org.langlets.acore.classifiers.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.langlets.acore.AcorePackage;

import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.abstractions.impl.AbstractionsPackageImpl;

import org.langlets.acore.classifiers.AAttribute;
import org.langlets.acore.classifiers.AClass;
import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.ADataType;
import org.langlets.acore.classifiers.AEnumeration;
import org.langlets.acore.classifiers.AFeature;
import org.langlets.acore.classifiers.AOperation;
import org.langlets.acore.classifiers.AParameter;
import org.langlets.acore.classifiers.AReference;
import org.langlets.acore.classifiers.ATyped;
import org.langlets.acore.classifiers.ClassifiersFactory;
import org.langlets.acore.classifiers.ClassifiersPackage;

import org.langlets.acore.impl.AcorePackageImpl;

import org.langlets.acore.updates.UpdatesPackage;

import org.langlets.acore.updates.impl.UpdatesPackageImpl;

import org.langlets.acore.values.ValuesPackage;

import org.langlets.acore.values.impl.ValuesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifiersPackageImpl extends EPackageImpl implements ClassifiersPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aClassifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aEnumerationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aStringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aRealEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aIntegerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aBooleanEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClassifiersPackageImpl() {
		super(eNS_URI, ClassifiersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ClassifiersPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClassifiersPackage init() {
		if (isInited)
			return (ClassifiersPackage) EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);

		// Obtain or create and register package
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ClassifiersPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ClassifiersPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theClassifiersPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theClassifiersPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClassifiersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClassifiersPackage.eNS_URI, theClassifiersPackage);
		return theClassifiersPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAClassifier() {
		return aClassifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_ASpecializedClassifier() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AActiveDataType() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AActiveEnumeration() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AActiveClass() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_AContainingPackage() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_AsDataType() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_AsClass() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AIsStringClassifier() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassifier__AAssignableTo__AClassifier() {
		return aClassifierEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getADataType() {
		return aDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getADataType_ASpecializedDataType() {
		return (EReference) aDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getADataType_ADataTypePackage() {
		return (EReference) aDataTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAEnumeration() {
		return aEnumerationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAEnumeration_ALiteral() {
		return (EReference) aEnumerationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAClass() {
		return aClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClass_AAbstract() {
		return (EAttribute) aClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClass_ASpecializedClass() {
		return (EReference) aClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClass_AFeature() {
		return (EReference) aClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClass_AOperation() {
		return (EReference) aClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClass_AAllFeature() {
		return (EReference) aClassEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClass_AAllOperation() {
		return (EReference) aClassEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClass_ASuperTypesLabel() {
		return (EAttribute) aClassEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClass_AAllStoredFeature() {
		return (EReference) aClassEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClass__Create() {
		return aClassEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClass__AFeatureFromName__String() {
		return aClassEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClass__AOperationFromNameAndTypes__String_AClassifier() {
		return aClassEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getATyped() {
		return aTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATyped_AClassifier() {
		return (EReference) aTypedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_AMandatory() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_ASingular() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_ATypeLabel() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_AUndefinedTypeConstant() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAFeature() {
		return aFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_AStored() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_APersisted() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_AChangeable() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_AActiveFeature() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAFeature_AContainingClass() {
		return (EReference) aFeatureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAAttribute() {
		return aAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAttribute_ADataType() {
		return (EReference) aAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAAttribute_AActiveAttribute() {
		return (EAttribute) aAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAReference() {
		return aReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAReference_AClass() {
		return (EReference) aReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAReference_AContainement() {
		return (EAttribute) aReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAReference_AActiveReference() {
		return (EAttribute) aReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAOperation() {
		return aOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAOperation_AParameter() {
		return (EReference) aOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAParameter() {
		return aParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAString() {
		return aStringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAReal() {
		return aRealEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAInteger() {
		return aIntegerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getABoolean() {
		return aBooleanEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersFactory getClassifiersFactory() {
		return (ClassifiersFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aClassifierEClass = createEClass(ACLASSIFIER);
		createEReference(aClassifierEClass, ACLASSIFIER__ASPECIALIZED_CLASSIFIER);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AACTIVE_DATA_TYPE);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AACTIVE_ENUMERATION);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AACTIVE_CLASS);
		createEReference(aClassifierEClass, ACLASSIFIER__ACONTAINING_PACKAGE);
		createEReference(aClassifierEClass, ACLASSIFIER__AS_DATA_TYPE);
		createEReference(aClassifierEClass, ACLASSIFIER__AS_CLASS);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AIS_STRING_CLASSIFIER);
		createEOperation(aClassifierEClass, ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER);

		aDataTypeEClass = createEClass(ADATA_TYPE);
		createEReference(aDataTypeEClass, ADATA_TYPE__ASPECIALIZED_DATA_TYPE);
		createEReference(aDataTypeEClass, ADATA_TYPE__ADATA_TYPE_PACKAGE);

		aEnumerationEClass = createEClass(AENUMERATION);
		createEReference(aEnumerationEClass, AENUMERATION__ALITERAL);

		aClassEClass = createEClass(ACLASS);
		createEAttribute(aClassEClass, ACLASS__AABSTRACT);
		createEReference(aClassEClass, ACLASS__ASPECIALIZED_CLASS);
		createEReference(aClassEClass, ACLASS__AFEATURE);
		createEReference(aClassEClass, ACLASS__AOPERATION);
		createEReference(aClassEClass, ACLASS__AALL_FEATURE);
		createEReference(aClassEClass, ACLASS__AALL_OPERATION);
		createEAttribute(aClassEClass, ACLASS__ASUPER_TYPES_LABEL);
		createEReference(aClassEClass, ACLASS__AALL_STORED_FEATURE);
		createEOperation(aClassEClass, ACLASS___CREATE);
		createEOperation(aClassEClass, ACLASS___AFEATURE_FROM_NAME__STRING);
		createEOperation(aClassEClass, ACLASS___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER);

		aTypedEClass = createEClass(ATYPED);
		createEReference(aTypedEClass, ATYPED__ACLASSIFIER);
		createEAttribute(aTypedEClass, ATYPED__AMANDATORY);
		createEAttribute(aTypedEClass, ATYPED__ASINGULAR);
		createEAttribute(aTypedEClass, ATYPED__ATYPE_LABEL);
		createEAttribute(aTypedEClass, ATYPED__AUNDEFINED_TYPE_CONSTANT);

		aFeatureEClass = createEClass(AFEATURE);
		createEAttribute(aFeatureEClass, AFEATURE__ASTORED);
		createEAttribute(aFeatureEClass, AFEATURE__APERSISTED);
		createEAttribute(aFeatureEClass, AFEATURE__ACHANGEABLE);
		createEAttribute(aFeatureEClass, AFEATURE__AACTIVE_FEATURE);
		createEReference(aFeatureEClass, AFEATURE__ACONTAINING_CLASS);

		aAttributeEClass = createEClass(AATTRIBUTE);
		createEReference(aAttributeEClass, AATTRIBUTE__ADATA_TYPE);
		createEAttribute(aAttributeEClass, AATTRIBUTE__AACTIVE_ATTRIBUTE);

		aReferenceEClass = createEClass(AREFERENCE);
		createEReference(aReferenceEClass, AREFERENCE__ACLASS);
		createEAttribute(aReferenceEClass, AREFERENCE__ACONTAINEMENT);
		createEAttribute(aReferenceEClass, AREFERENCE__AACTIVE_REFERENCE);

		aOperationEClass = createEClass(AOPERATION);
		createEReference(aOperationEClass, AOPERATION__APARAMETER);

		aParameterEClass = createEClass(APARAMETER);

		// Create data types
		aStringEDataType = createEDataType(ASTRING);
		aRealEDataType = createEDataType(AREAL);
		aIntegerEDataType = createEDataType(AINTEGER);
		aBooleanEDataType = createEDataType(ABOOLEAN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AbstractionsPackage theAbstractionsPackage = (AbstractionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI);
		AcorePackage theAcorePackage = (AcorePackage) EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI);
		ValuesPackage theValuesPackage = (ValuesPackage) EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aClassifierEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aDataTypeEClass.getESuperTypes().add(this.getAClassifier());
		aEnumerationEClass.getESuperTypes().add(this.getADataType());
		aClassEClass.getESuperTypes().add(this.getAClassifier());
		aTypedEClass.getESuperTypes().add(theAbstractionsPackage.getAElement());
		aFeatureEClass.getESuperTypes().add(this.getATyped());
		aFeatureEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aAttributeEClass.getESuperTypes().add(this.getAFeature());
		aReferenceEClass.getESuperTypes().add(this.getAFeature());
		aOperationEClass.getESuperTypes().add(this.getATyped());
		aOperationEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aParameterEClass.getESuperTypes().add(this.getATyped());
		aParameterEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());

		// Initialize classes, features, and operations; add parameters
		initEClass(aClassifierEClass, AClassifier.class, "AClassifier", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAClassifier_ASpecializedClassifier(), this.getAClassifier(), null, "aSpecializedClassifier",
				null, 0, -1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AActiveDataType(), ecorePackage.getEBooleanObject(), "aActiveDataType", null, 0,
				1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AActiveEnumeration(), ecorePackage.getEBooleanObject(), "aActiveEnumeration",
				null, 0, 1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AActiveClass(), ecorePackage.getEBooleanObject(), "aActiveClass", null, 0, 1,
				AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClassifier_AContainingPackage(), theAcorePackage.getAPackage(), null, "aContainingPackage",
				null, 0, 1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassifier_AsDataType(), this.getADataType(), null, "asDataType", null, 0, 1,
				AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassifier_AsClass(), this.getAClass(), null, "asClass", null, 0, 1, AClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AIsStringClassifier(), ecorePackage.getEBooleanObject(), "aIsStringClassifier",
				null, 0, 1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getAClassifier__AAssignableTo__AClassifier(), ecorePackage.getEBooleanObject(),
				"aAssignableTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAClassifier(), "aClassifier", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aDataTypeEClass, ADataType.class, "ADataType", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getADataType_ASpecializedDataType(), this.getADataType(), null, "aSpecializedDataType", null, 0,
				-1, ADataType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getADataType_ADataTypePackage(), theAcorePackage.getAPackage(), null, "aDataTypePackage", null,
				0, 1, ADataType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aEnumerationEClass, AEnumeration.class, "AEnumeration", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAEnumeration_ALiteral(), theValuesPackage.getALiteral(), null, "aLiteral", null, 0, -1,
				AEnumeration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aClassEClass, AClass.class, "AClass", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAClass_AAbstract(), ecorePackage.getEBooleanObject(), "aAbstract", null, 0, 1, AClass.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClass_ASpecializedClass(), this.getAClass(), null, "aSpecializedClass", null, 0, -1,
				AClass.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClass_AFeature(), this.getAFeature(), null, "aFeature", null, 0, -1, AClass.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClass_AOperation(), this.getAOperation(), null, "aOperation", null, 0, -1, AClass.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClass_AAllFeature(), this.getAFeature(), null, "aAllFeature", null, 0, -1, AClass.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClass_AAllOperation(), this.getAOperation(), null, "aAllOperation", null, 0, -1,
				AClass.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClass_ASuperTypesLabel(), ecorePackage.getEString(), "aSuperTypesLabel", null, 0, 1,
				AClass.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getAClass_AAllStoredFeature(), this.getAFeature(), null, "aAllStoredFeature", null, 0, -1,
				AClass.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getAClass__Create(), theValuesPackage.getAObject(), "create", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAClass__AFeatureFromName__String(), this.getAFeature(), "aFeatureFromName", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "featureName", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAClass__AOperationFromNameAndTypes__String_AClassifier(), this.getAOperation(),
				"aOperationFromNameAndTypes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "operationName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAClassifier(), "parameterType", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aTypedEClass, ATyped.class, "ATyped", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getATyped_AClassifier(), this.getAClassifier(), null, "aClassifier", null, 0, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_AMandatory(), ecorePackage.getEBooleanObject(), "aMandatory", null, 1, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_ASingular(), ecorePackage.getEBooleanObject(), "aSingular", null, 1, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_ATypeLabel(), ecorePackage.getEString(), "aTypeLabel", null, 0, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_AUndefinedTypeConstant(), ecorePackage.getEString(), "aUndefinedTypeConstant", null, 0,
				1, ATyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aFeatureEClass, AFeature.class, "AFeature", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAFeature_AStored(), ecorePackage.getEBooleanObject(), "aStored", null, 0, 1, AFeature.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAFeature_APersisted(), ecorePackage.getEBooleanObject(), "aPersisted", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAFeature_AChangeable(), ecorePackage.getEBooleanObject(), "aChangeable", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAFeature_AActiveFeature(), ecorePackage.getEBooleanObject(), "aActiveFeature", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAFeature_AContainingClass(), this.getAClass(), null, "aContainingClass", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aAttributeEClass, AAttribute.class, "AAttribute", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAAttribute_ADataType(), this.getADataType(), null, "aDataType", null, 1, 1, AAttribute.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAAttribute_AActiveAttribute(), ecorePackage.getEBooleanObject(), "aActiveAttribute", null, 0,
				1, AAttribute.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aReferenceEClass, AReference.class, "AReference", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAReference_AClass(), this.getAClass(), null, "aClass", null, 1, 1, AReference.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAReference_AContainement(), ecorePackage.getEBooleanObject(), "aContainement", null, 0, 1,
				AReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAReference_AActiveReference(), ecorePackage.getEBooleanObject(), "aActiveReference", null, 0,
				1, AReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aOperationEClass, AOperation.class, "AOperation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAOperation_AParameter(), this.getAParameter(), null, "aParameter", null, 0, -1,
				AOperation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aParameterEClass, AParameter.class, "AParameter", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(aStringEDataType, String.class, "AString", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(aRealEDataType, double.class, "AReal", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(aIntegerEDataType, int.class, "AInteger", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(aBooleanEDataType, boolean.class, "ABoolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getAClassifier__AAssignableTo__AClassifier(), source, new String[] { "body",
				"if (let e0: Boolean = self = aClassifier in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen true\n  else let e0: Boolean = aSpecializedClassifier.aAssignableTo(aClassifier)->reject(oclIsUndefined())->asOrderedSet()->includes(true)   in \n if e0.oclIsInvalid() then null else e0 endif\nendif\n" });
		addAnnotation(getAClassifier_ASpecializedClassifier(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClassifier_AActiveDataType(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassifier_AActiveClass(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassifier_AContainingPackage(), source,
				new String[] { "derive", "let nl: acore::APackage = null in nl\n" });
		addAnnotation(getAClassifier_AsDataType(), source, new String[] { "derive",
				"if (let e0: Boolean = if (aActiveDataType)= true \n then true \n else if (aActiveEnumeration)= true \n then true \nelse if ((aActiveDataType)= null or (aActiveEnumeration)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::ADataType)\n    then chain.oclAsType(acore::classifiers::ADataType)\n    else null\n  endif\n  endif\n  else null\nendif\n" });
		addAnnotation(getAClassifier_AsClass(), source, new String[] { "derive",
				"if (aActiveClass) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClass)\n    then chain.oclAsType(acore::classifiers::AClass)\n    else null\n  endif\n  endif\n  else null\nendif\n" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source, new String[] { "derive",
				"let e1: Boolean = if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif))= false \n then false \n else if (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= false \n then false \nelse if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif)= null or (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getADataType_ASpecializedDataType(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getADataType_ADataTypePackage(), source, new String[] { "derive", "aContainingPackage\n" });
		addAnnotation(getAEnumeration_ALiteral(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClass__Create(), source,
				new String[] { "body", "let nl: acore::values::AObject = null in nl\n" });
		addAnnotation(getAClass__AFeatureFromName__String(), source, new String[] { "body",
				"let fs: OrderedSet(acore::classifiers::AFeature)  = aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | let e0: Boolean = it.aName = featureName in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen null\n  else let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\nendif\n" });
		addAnnotation(getAClass__AOperationFromNameAndTypes__String_AClassifier(), source,
				new String[] { "body", "let nl: acore::classifiers::AOperation = null in nl\n" });
		addAnnotation(getAClass_AAbstract(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClass_ASpecializedClass(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClass_AFeature(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClass_AOperation(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClass_AAllFeature(), source, new String[] { "derive",
				"let e1: OrderedSet(acore::classifiers::AFeature)  = aFeature->asOrderedSet()->union(aSpecializedClass.aAllFeature->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getAClass_AAllOperation(), source, new String[] { "derive",
				"let e1: OrderedSet(acore::classifiers::AOperation)  = aOperation->asOrderedSet()->union(aSpecializedClass.aAllOperation->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getAClass_ASuperTypesLabel(), source, new String[] { "derive",
				"let length: Integer = let chain: OrderedSet(acore::classifiers::AClass)  = aSpecializedClass->asOrderedSet() in\nif chain->size().oclIsUndefined() \n then null \n else chain->size()\n  endif in\nlet superTypeNames: OrderedSet(String)  = aSpecializedClass.aName->reject(oclIsUndefined())->asOrderedSet() in\nif (let e0: Boolean = length = 0 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\' else if (let e0: Boolean = length = 1 in \n if e0.oclIsInvalid() then null else e0 endif)=true then (let e0: String = \'->\'.concat(let chain01: OrderedSet(String)  = superTypeNames in\nif chain01->first().oclIsUndefined() \n then null \n else chain01->first()\n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: String = \'->(\'.concat(listOfStringToStringWithSeparator(superTypeNames)).concat(\')\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n" });
		addAnnotation(getAClass_AAllStoredFeature(), source, new String[] { "derive",
				"aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | it.aStored)->asOrderedSet()->excluding(null)->asOrderedSet() \n" });
		addAnnotation(getATyped_AClassifier(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassifier = null in nl\n" });
		addAnnotation(getATyped_AMandatory(), source, new String[] { "derive", "false\n" });
		addAnnotation(getATyped_ASingular(), source, new String[] { "derive", "false\n" });
		addAnnotation(getATyped_ATypeLabel(), source, new String[] { "derive",
				"if (let e0: Boolean = aClassifier = null in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in \n if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif\n  else (let e0: String = if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif.concat(\'*\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source,
				new String[] { "derive", "\'<A Type Is Undefined>\'\n" });
		addAnnotation(getAFeature_AStored(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAFeature_APersisted(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAFeature_AChangeable(), source,
				new String[] { "derive", "if (aStored) \n  =true \nthen true\n  else false\nendif\n" });
		addAnnotation(getAFeature_AActiveFeature(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAFeature_AContainingClass(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClass = null in nl\n" });
		addAnnotation(getAAttribute_ADataType(), source,
				new String[] { "derive", "let nl: acore::classifiers::ADataType = null in nl\n" });
		addAnnotation(getAAttribute_AActiveAttribute(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAReference_AClass(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClass = null in nl\n" });
		addAnnotation(getAReference_AContainement(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAReference_AActiveReference(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAOperation_AParameter(), source, new String[] { "derive", "OrderedSet{}\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getAClassifier__AAssignableTo__AClassifier(), source,
				new String[] { "propertyCategory", "80 A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassifier_ASpecializedClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AActiveDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AActiveClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AContainingPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AsDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AsClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Package" });
		addAnnotation(getADataType_ASpecializedDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getADataType_ADataTypePackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAEnumeration_ALiteral(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass__Create(), source,
				new String[] { "propertyCategory", "80 A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClass__AFeatureFromName__String(), source,
				new String[] { "propertyCategory", "80 A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClass__AOperationFromNameAndTypes__String_AClassifier(), source,
				new String[] { "propertyCategory", "80 A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClass_AAbstract(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass_ASpecializedClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass_AFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass_AOperation(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass_AAllFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass_AAllOperation(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAClass_ASuperTypesLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "91 A Core Helpers" });
		addAnnotation(getAClass_AAllStoredFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "91 A Core Helpers" });
		addAnnotation(getATyped_AClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getATyped_AMandatory(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getATyped_ASingular(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getATyped_ATypeLabel(), source,
				new String[] { "createColumn", "true", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAFeature_AStored(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAFeature_APersisted(), source,
				new String[] { "createColumn", "true", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAFeature_AChangeable(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAFeature_AActiveFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAFeature_AContainingClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAAttribute_ADataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAAttribute_AActiveAttribute(), source,
				new String[] { "createColumn", "true", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAReference_AClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAReference_AContainement(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAReference_AActiveReference(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
		addAnnotation(getAOperation_AParameter(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Classifiers" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getAClassifier_ASpecializedClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AActiveDataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AActiveClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AContainingPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AsDataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AsClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getADataType_ASpecializedDataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getADataType_ADataTypePackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAEnumeration_ALiteral(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_AAbstract(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_ASpecializedClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_AFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_AOperation(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_AAllFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_AAllOperation(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_ASuperTypesLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClass_AAllStoredFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_AClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_AMandatory(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_ASingular(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_ATypeLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AStored(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_APersisted(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AChangeable(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AActiveFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AContainingClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAttribute_ADataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAttribute_AActiveAttribute(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAReference_AClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAReference_AContainement(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAReference_AActiveReference(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAOperation_AParameter(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aDataTypeEClass, source, new String[] { "aActiveDataTypeDerive", "true\n",
				"aSpecializedClassifierDerive", "aSpecializedDataType->asOrderedSet()\n" });
		addAnnotation(aEnumerationEClass, source,
				new String[] { "aActiveEnumerationDerive", "true\n", "aActiveDataTypeDerive", "false\n" });
		addAnnotation(aClassEClass, source, new String[] { "aActiveClassDerive", "true\n",
				"aSpecializedClassifierDerive", "aSpecializedClass->asOrderedSet()\n", "aLabelDerive",
				"let e1: String = aName.concat(aSuperTypesLabel) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(aTypedEClass, source, new String[] { "aLabelDerive", "aTypeLabel\n" });
		addAnnotation(aAttributeEClass, source,
				new String[] { "aClassifierDerive", "aDataType\n", "aActiveFeatureDerive", "aActiveAttribute\n" });
		addAnnotation(aReferenceEClass, source,
				new String[] { "aClassifierDerive", "aClass\n", "aActiveFeatureDerive", "aActiveReference\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(aTypedEClass, source, new String[] { "aLabelCreateColumn", "true" });
		addAnnotation(aAttributeEClass, source, new String[] { "aClassifierCreateColumn", "false" });
		addAnnotation(aReferenceEClass, source, new String[] { "aClassifierCreateColumn", "false" });
	}

} //ClassifiersPackageImpl
