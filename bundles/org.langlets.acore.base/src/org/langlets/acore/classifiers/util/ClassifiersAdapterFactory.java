/**
 */
package org.langlets.acore.classifiers.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;

import org.langlets.acore.classifiers.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.langlets.acore.classifiers.ClassifiersPackage
 * @generated
 */
public class ClassifiersAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClassifiersPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ClassifiersPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassifiersSwitch<Adapter> modelSwitch = new ClassifiersSwitch<Adapter>() {
		@Override
		public Adapter caseAClassifier(AClassifier object) {
			return createAClassifierAdapter();
		}

		@Override
		public Adapter caseADataType(ADataType object) {
			return createADataTypeAdapter();
		}

		@Override
		public Adapter caseAEnumeration(AEnumeration object) {
			return createAEnumerationAdapter();
		}

		@Override
		public Adapter caseAClass(AClass object) {
			return createAClassAdapter();
		}

		@Override
		public Adapter caseATyped(ATyped object) {
			return createATypedAdapter();
		}

		@Override
		public Adapter caseAFeature(AFeature object) {
			return createAFeatureAdapter();
		}

		@Override
		public Adapter caseAAttribute(AAttribute object) {
			return createAAttributeAdapter();
		}

		@Override
		public Adapter caseAReference(AReference object) {
			return createAReferenceAdapter();
		}

		@Override
		public Adapter caseAOperation(AOperation object) {
			return createAOperationAdapter();
		}

		@Override
		public Adapter caseAParameter(AParameter object) {
			return createAParameterAdapter();
		}

		@Override
		public Adapter caseAElement(AElement object) {
			return createAElementAdapter();
		}

		@Override
		public Adapter caseANamed(ANamed object) {
			return createANamedAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AClassifier
	 * @generated
	 */
	public Adapter createAClassifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.ADataType <em>AData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.ADataType
	 * @generated
	 */
	public Adapter createADataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AEnumeration <em>AEnumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AEnumeration
	 * @generated
	 */
	public Adapter createAEnumerationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AClass <em>AClass</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AClass
	 * @generated
	 */
	public Adapter createAClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.ATyped <em>ATyped</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.ATyped
	 * @generated
	 */
	public Adapter createATypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AFeature <em>AFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AFeature
	 * @generated
	 */
	public Adapter createAFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AAttribute <em>AAttribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AAttribute
	 * @generated
	 */
	public Adapter createAAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AReference <em>AReference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AReference
	 * @generated
	 */
	public Adapter createAReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AOperation <em>AOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AOperation
	 * @generated
	 */
	public Adapter createAOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AParameter <em>AParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AParameter
	 * @generated
	 */
	public Adapter createAParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.abstractions.AElement
	 * @generated
	 */
	public Adapter createAElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.abstractions.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.abstractions.ANamed
	 * @generated
	 */
	public Adapter createANamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ClassifiersAdapterFactory
