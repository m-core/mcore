/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.values.AObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AClass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getAAbstract <em>AAbstract</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getASpecializedClass <em>ASpecialized Class</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getAFeature <em>AFeature</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getAOperation <em>AOperation</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getAAllFeature <em>AAll Feature</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getAAllOperation <em>AAll Operation</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getASuperTypesLabel <em>ASuper Types Label</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClass#getAAllStoredFeature <em>AAll Stored Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aActiveClassDerive='true\n' aSpecializedClassifierDerive='aSpecializedClass->asOrderedSet()\n' aLabelDerive='let e1: String = aName.concat(aSuperTypesLabel) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 * @generated
 */

public interface AClass extends AClassifier {
	/**
	 * Returns the value of the '<em><b>AAbstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAbstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAbstract</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_AAbstract()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAAbstract();

	/**
	 * Returns the value of the '<em><b>ASpecialized Class</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASpecialized Class</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASpecialized Class</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_ASpecializedClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AClass> getASpecializedClass();

	/**
	 * Returns the value of the '<em><b>AFeature</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_AFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFeature> getAFeature();

	/**
	 * Returns the value of the '<em><b>AOperation</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AOperation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AOperation</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_AOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AOperation> getAOperation();

	/**
	 * Returns the value of the '<em><b>AAll Feature</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Feature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Feature</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_AAllFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(acore::classifiers::AFeature)  = aFeature->asOrderedSet()->union(aSpecializedClass.aAllFeature->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFeature> getAAllFeature();

	/**
	 * Returns the value of the '<em><b>AAll Operation</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Operation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Operation</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_AAllOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(acore::classifiers::AOperation)  = aOperation->asOrderedSet()->union(aSpecializedClass.aAllOperation->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AOperation> getAAllOperation();

	/**
	 * Returns the value of the '<em><b>ASuper Types Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASuper Types Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASuper Types Label</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_ASuperTypesLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let length: Integer = let chain: OrderedSet(acore::classifiers::AClass)  = aSpecializedClass->asOrderedSet() in\nif chain->size().oclIsUndefined() \n then null \n else chain->size()\n  endif in\nlet superTypeNames: OrderedSet(String)  = aSpecializedClass.aName->reject(oclIsUndefined())->asOrderedSet() in\nif (let e0: Boolean = length = 0 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\' else if (let e0: Boolean = length = 1 in \n if e0.oclIsInvalid() then null else e0 endif)=true then (let e0: String = \'->\'.concat(let chain01: OrderedSet(String)  = superTypeNames in\nif chain01->first().oclIsUndefined() \n then null \n else chain01->first()\n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: String = \'->(\'.concat(listOfStringToStringWithSeparator(superTypeNames)).concat(\')\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='91 A Core Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getASuperTypesLabel();

	/**
	 * Returns the value of the '<em><b>AAll Stored Feature</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Stored Feature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Stored Feature</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClass_AAllStoredFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | it.aStored)->asOrderedSet()->excluding(null)->asOrderedSet() \n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='91 A Core Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFeature> getAAllStoredFeature();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='let nl: acore::values::AObject = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	AObject create();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let fs: OrderedSet(acore::classifiers::AFeature)  = aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | let e0: Boolean = it.aName = featureName in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen null\n  else let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	AFeature aFeatureFromName(String featureName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let nl: acore::classifiers::AOperation = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	AOperation aOperationFromNameAndTypes(String operationName, AClassifier parameterType);

} // AClass
