/**
 */
package org.langlets.acore;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AStructuring Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.AStructuringElement#getAContainingFolder <em>AContaining Folder</em>}</li>
 *   <li>{@link org.langlets.acore.AStructuringElement#getAUri <em>AUri</em>}</li>
 *   <li>{@link org.langlets.acore.AStructuringElement#getAAllPackages <em>AAll Packages</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.AcorePackage#getAStructuringElement()
 * @model abstract="true"
 * @generated
 */

public interface AStructuringElement extends ANamed {
	/**
	 * Returns the value of the '<em><b>AContaining Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Folder</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Folder</em>' reference.
	 * @see org.langlets.acore.AcorePackage#getAStructuringElement_AContainingFolder()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let chain: ecore::EObject = eContainer() in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::AAbstractFolder)\n    then chain.oclAsType(acore::AAbstractFolder)\n    else null\n  endif\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AAbstractFolder getAContainingFolder();

	/**
	 * Returns the value of the '<em><b>AUri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUri</em>' attribute.
	 * @see org.langlets.acore.AcorePackage#getAStructuringElement_AUri()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: String = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAUri();

	/**
	 * Returns the value of the '<em><b>AAll Packages</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.APackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Packages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Packages</em>' reference list.
	 * @see org.langlets.acore.AcorePackage#getAStructuringElement_AAllPackages()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='AAll Packages'"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='91 A Core Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<APackage> getAAllPackages();

} // AStructuringElement
