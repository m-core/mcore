/**
 */
package org.langlets.acore.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.APackage;
import org.langlets.acore.AcorePackage;

import org.langlets.acore.classifiers.AClassifier;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>APackage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getARootObjectClass <em>ARoot Object Class</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getASubPackage <em>ASub Package</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getAContainingPackage <em>AContaining Package</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getASpecialUri <em>ASpecial Uri</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getAActivePackage <em>AActive Package</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getAActiveRootPackage <em>AActive Root Package</em>}</li>
 *   <li>{@link org.langlets.acore.impl.APackageImpl#getAActiveSubPackage <em>AActive Sub Package</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class APackageImpl extends AStructuringElementImpl implements APackage {
	/**
	 * The default value of the '{@link #getASpecialUri() <em>ASpecial Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASpecialUri()
	 * @generated
	 * @ordered
	 */
	protected static final String ASPECIAL_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActivePackage() <em>AActive Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActivePackage()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_PACKAGE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveRootPackage() <em>AActive Root Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveRootPackage()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_ROOT_PACKAGE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveSubPackage() <em>AActive Sub Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveSubPackage()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_SUB_PACKAGE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #aClassifierFromName <em>AClassifier From Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aClassifierFromName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aClassifierFromNameecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARootObjectClass <em>ARoot Object Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARootObjectClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRootObjectClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASubPackage <em>ASub Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASubPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSubPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingPackage <em>AContaining Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASpecialUri <em>ASpecial Uri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASpecialUri
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSpecialUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActivePackage <em>AActive Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActivePackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActivePackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveRootPackage <em>AActive Root Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveRootPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveRootPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveSubPackage <em>AActive Sub Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveSubPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveSubPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUri <em>AUri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUri
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllPackages <em>AAll Packages</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllPackages
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aAllPackagesDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected APackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcorePackage.Literals.APACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getARootObjectClass() {
		AClassifier aRootObjectClass = basicGetARootObjectClass();
		return aRootObjectClass != null && aRootObjectClass.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aRootObjectClass) : aRootObjectClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetARootObjectClass() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__AROOT_OBJECT_CLASS;

		if (aRootObjectClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRootObjectClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRootObjectClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassifier> getAClassifier() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AClassifier> result = (EList<AClassifier>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<APackage> getASubPackage() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__ASUB_PACKAGE;

		if (aSubPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSubPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSubPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<APackage> result = (EList<APackage>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getAContainingPackage() {
		APackage aContainingPackage = basicGetAContainingPackage();
		return aContainingPackage != null && aContainingPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) aContainingPackage) : aContainingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetAContainingPackage() {
		/**
		 * @OCL let nl: acore::APackage = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__ACONTAINING_PACKAGE;

		if (aContainingPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getASpecialUri() {
		/**
		 * @OCL let nl: String = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__ASPECIAL_URI;

		if (aSpecialUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSpecialUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSpecialUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActivePackage() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__AACTIVE_PACKAGE;

		if (aActivePackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActivePackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActivePackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveRootPackage() {
		/**
		 * @OCL let e1: Boolean = if (aActivePackage)= false 
		then false 
		else if (let e2: Boolean = aContainingPackage = null in 
		if e2.oclIsInvalid() then null else e2 endif)= false 
		then false 
		else if ((aActivePackage)= null or (let e2: Boolean = aContainingPackage = null in 
		if e2.oclIsInvalid() then null else e2 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__AACTIVE_ROOT_PACKAGE;

		if (aActiveRootPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveRootPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveRootPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveSubPackage() {
		/**
		 * @OCL let e1: Boolean = if (aActivePackage)= false 
		then false 
		else if (let e2: Boolean = aContainingPackage <> null in 
		if e2.oclIsInvalid() then null else e2 endif)= false 
		then false 
		else if ((aActivePackage)= null or (let e2: Boolean = aContainingPackage <> null in 
		if e2.oclIsInvalid() then null else e2 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.APACKAGE;
		EStructuralFeature eFeature = AcorePackage.Literals.APACKAGE__AACTIVE_SUB_PACKAGE;

		if (aActiveSubPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveSubPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveSubPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aClassifierFromName(String classifierName) {

		/**
		 * @OCL let classifiers: OrderedSet(acore::classifiers::AClassifier)  = aClassifier->asOrderedSet()->select(it: acore::classifiers::AClassifier | let e0: Boolean = it.aName = classifierName in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let chain: OrderedSet(acore::classifiers::AClassifier)  = classifiers in
		if chain->first().oclIsUndefined() 
		then null 
		else chain->first()
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AcorePackage.Literals.APACKAGE);
		EOperation eOperation = AcorePackage.Literals.APACKAGE.getEOperations().get(0);
		if (aClassifierFromNameecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aClassifierFromNameecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, body, helper.getProblems(),
						AcorePackage.Literals.APACKAGE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierFromNameecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.APACKAGE, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("classifierName", classifierName);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AcorePackage.APACKAGE__AROOT_OBJECT_CLASS:
			if (resolve)
				return getARootObjectClass();
			return basicGetARootObjectClass();
		case AcorePackage.APACKAGE__ACLASSIFIER:
			return getAClassifier();
		case AcorePackage.APACKAGE__ASUB_PACKAGE:
			return getASubPackage();
		case AcorePackage.APACKAGE__ACONTAINING_PACKAGE:
			if (resolve)
				return getAContainingPackage();
			return basicGetAContainingPackage();
		case AcorePackage.APACKAGE__ASPECIAL_URI:
			return getASpecialUri();
		case AcorePackage.APACKAGE__AACTIVE_PACKAGE:
			return getAActivePackage();
		case AcorePackage.APACKAGE__AACTIVE_ROOT_PACKAGE:
			return getAActiveRootPackage();
		case AcorePackage.APACKAGE__AACTIVE_SUB_PACKAGE:
			return getAActiveSubPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AcorePackage.APACKAGE__AROOT_OBJECT_CLASS:
			return basicGetARootObjectClass() != null;
		case AcorePackage.APACKAGE__ACLASSIFIER:
			return !getAClassifier().isEmpty();
		case AcorePackage.APACKAGE__ASUB_PACKAGE:
			return !getASubPackage().isEmpty();
		case AcorePackage.APACKAGE__ACONTAINING_PACKAGE:
			return basicGetAContainingPackage() != null;
		case AcorePackage.APACKAGE__ASPECIAL_URI:
			return ASPECIAL_URI_EDEFAULT == null ? getASpecialUri() != null
					: !ASPECIAL_URI_EDEFAULT.equals(getASpecialUri());
		case AcorePackage.APACKAGE__AACTIVE_PACKAGE:
			return AACTIVE_PACKAGE_EDEFAULT == null ? getAActivePackage() != null
					: !AACTIVE_PACKAGE_EDEFAULT.equals(getAActivePackage());
		case AcorePackage.APACKAGE__AACTIVE_ROOT_PACKAGE:
			return AACTIVE_ROOT_PACKAGE_EDEFAULT == null ? getAActiveRootPackage() != null
					: !AACTIVE_ROOT_PACKAGE_EDEFAULT.equals(getAActiveRootPackage());
		case AcorePackage.APACKAGE__AACTIVE_SUB_PACKAGE:
			return AACTIVE_SUB_PACKAGE_EDEFAULT == null ? getAActiveSubPackage() != null
					: !AACTIVE_SUB_PACKAGE_EDEFAULT.equals(getAActiveSubPackage());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case AcorePackage.APACKAGE___ACLASSIFIER_FROM_NAME__STRING:
			return aClassifierFromName((String) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aUri let specialUri: String = aSpecialUri in
	if (let e0: Boolean = not(stringIsEmpty(specialUri)) in 
	if e0.oclIsInvalid() then null else e0 endif) 
	=true 
	then specialUri else if (aActiveRootPackage)=true then (let e0: String = if aContainingFolder.oclIsUndefined()
	then null
	else aContainingFolder.aUri
	endif.concat('/').concat(aName) in 
	if e0.oclIsInvalid() then null else e0 endif)
	else (let e0: String = if aContainingPackage.oclIsUndefined()
	then null
	else aContainingPackage.aUri
	endif.concat('/').concat(aName) in 
	if e0.oclIsInvalid() then null else e0 endif)
	endif endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAUri() {
		EClass eClass = (AcorePackage.Literals.APACKAGE);
		EStructuralFeature eOverrideFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AURI;

		if (aUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aAllPackages let e1: OrderedSet(acore::APackage)  = let chain11: acore::APackage = self in
	if chain11->asOrderedSet()->oclIsUndefined() 
	then null 
	else chain11->asOrderedSet()
	endif->union(aSubPackage.aAllPackages->asOrderedSet()) ->asOrderedSet()   in 
	if e1->oclIsInvalid() then OrderedSet{} else e1 endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public EList<APackage> getAAllPackages() {
		EClass eClass = (AcorePackage.Literals.APACKAGE);
		EStructuralFeature eOverrideFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AALL_PACKAGES;

		if (aAllPackagesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aAllPackagesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllPackagesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<APackage>) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //APackageImpl
