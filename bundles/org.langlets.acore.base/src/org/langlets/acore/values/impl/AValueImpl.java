/**
 */
package org.langlets.acore.values.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.abstractions.impl.AElementImpl;

import org.langlets.acore.classifiers.AClassifier;

import org.langlets.acore.values.ASlot;
import org.langlets.acore.values.AValue;
import org.langlets.acore.values.ValuesPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AValue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.values.impl.AValueImpl#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.AValueImpl#getAContainingSlot <em>AContaining Slot</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AValueImpl extends AElementImpl implements AValue {
	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingSlot <em>AContaining Slot</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingSlot
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingSlotDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ValuesPackage.Literals.AVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAClassifier() {
		AClassifier aClassifier = basicGetAClassifier();
		return aClassifier != null && aClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aClassifier) : aClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAClassifier() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AVALUE;
		EStructuralFeature eFeature = ValuesPackage.Literals.AVALUE__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AVALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASlot getAContainingSlot() {
		ASlot aContainingSlot = basicGetAContainingSlot();
		return aContainingSlot != null && aContainingSlot.eIsProxy()
				? (ASlot) eResolveProxy((InternalEObject) aContainingSlot) : aContainingSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASlot basicGetAContainingSlot() {
		/**
		 * @OCL let nl: acore::values::ASlot = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AVALUE;
		EStructuralFeature eFeature = ValuesPackage.Literals.AVALUE__ACONTAINING_SLOT;

		if (aContainingSlotDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingSlotDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingSlotDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AVALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASlot result = (ASlot) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ValuesPackage.AVALUE__ACLASSIFIER:
			if (resolve)
				return getAClassifier();
			return basicGetAClassifier();
		case ValuesPackage.AVALUE__ACONTAINING_SLOT:
			if (resolve)
				return getAContainingSlot();
			return basicGetAContainingSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ValuesPackage.AVALUE__ACLASSIFIER:
			return basicGetAClassifier() != null;
		case ValuesPackage.AVALUE__ACONTAINING_SLOT:
			return basicGetAContainingSlot() != null;
		}
		return super.eIsSet(featureID);
	}

} //AValueImpl
