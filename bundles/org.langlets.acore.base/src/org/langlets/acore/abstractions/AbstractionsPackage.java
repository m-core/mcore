/**
 */
package org.langlets.acore.abstractions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.langlets.acore.abstractions.AbstractionsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='org.langlets.acore'"
 * @generated
 */
public interface AbstractionsPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "org.langlets.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "abstractions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.langlets.org/aCore/ACore/Abstractions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.abstractions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AbstractionsPackage eINSTANCE = org.langlets.acore.abstractions.impl.AbstractionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.langlets.acore.abstractions.impl.AElementImpl <em>AElement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.abstractions.impl.AElementImpl
	 * @see org.langlets.acore.abstractions.impl.AbstractionsPackageImpl#getAElement()
	 * @generated
	 */
	int AELEMENT = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ALABEL = 0;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AKIND_BASE = 1;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ARENDERED_KIND = 2;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ACONTAINING_COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TPACKAGE_URI = 4;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TCLASSIFIER_NAME = 5;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TFEATURE_NAME = 6;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TPACKAGE = 7;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TCLASSIFIER = 8;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TFEATURE = 9;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__TA_CORE_ASTRING_CLASS = 10;

	/**
	 * The number of structural features of the '<em>AElement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT_FEATURE_COUNT = 11;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___INDENT_LEVEL = 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___INDENTATION_SPACES = 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___INDENTATION_SPACES__INTEGER = 2;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___STRING_OR_MISSING__STRING = 3;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___STRING_IS_EMPTY__STRING = 4;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = 5;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = 6;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___APACKAGE_FROM_URI__STRING = 7;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = 8;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = 9;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_ASTRING_CLASS = 10;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_AREAL_CLASS = 11;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_AINTEGER_CLASS = 12;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_AOBJECT_CLASS = 13;

	/**
	 * The number of operations of the '<em>AElement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT_OPERATION_COUNT = 14;

	/**
	 * The meta object id for the '{@link org.langlets.acore.abstractions.impl.ANamedImpl <em>ANamed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.acore.abstractions.impl.ANamedImpl
	 * @see org.langlets.acore.abstractions.impl.AbstractionsPackageImpl#getANamed()
	 * @generated
	 */
	int ANAMED = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ALABEL = AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AKIND_BASE = AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ARENDERED_KIND = AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ACONTAINING_COMPONENT = AELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TPACKAGE_URI = AELEMENT__TPACKAGE_URI;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TCLASSIFIER_NAME = AELEMENT__TCLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TFEATURE_NAME = AELEMENT__TFEATURE_NAME;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TPACKAGE = AELEMENT__TPACKAGE;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TCLASSIFIER = AELEMENT__TCLASSIFIER;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TFEATURE = AELEMENT__TFEATURE;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__TA_CORE_ASTRING_CLASS = AELEMENT__TA_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ANAME = AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AUNDEFINED_NAME_CONSTANT = AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ABUSINESS_NAME = AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ANamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED_FEATURE_COUNT = AELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___INDENT_LEVEL = AELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___INDENTATION_SPACES = AELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___INDENTATION_SPACES__INTEGER = AELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___STRING_OR_MISSING__STRING = AELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___STRING_IS_EMPTY__STRING = AELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___APACKAGE_FROM_URI__STRING = AELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_ASTRING_CLASS = AELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_AREAL_CLASS = AELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_AINTEGER_CLASS = AELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_AOBJECT_CLASS = AELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>ANamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED_OPERATION_COUNT = AELEMENT_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.abstractions.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ANamed</em>'.
	 * @see org.langlets.acore.abstractions.ANamed
	 * @generated
	 */
	EClass getANamed();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.ANamed#getAName <em>AName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AName</em>'.
	 * @see org.langlets.acore.abstractions.ANamed#getAName()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_AName();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.ANamed#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUndefined Name Constant</em>'.
	 * @see org.langlets.acore.abstractions.ANamed#getAUndefinedNameConstant()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_AUndefinedNameConstant();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.ANamed#getABusinessName <em>ABusiness Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ABusiness Name</em>'.
	 * @see org.langlets.acore.abstractions.ANamed#getABusinessName()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_ABusinessName();

	/**
	 * Returns the meta object for class '{@link org.langlets.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AElement</em>'.
	 * @see org.langlets.acore.abstractions.AElement
	 * @generated
	 */
	EClass getAElement();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.AElement#getALabel <em>ALabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ALabel</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getALabel()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ALabel();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.AElement#getAKindBase <em>AKind Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AKind Base</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getAKindBase()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_AKindBase();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.AElement#getARenderedKind <em>ARendered Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ARendered Kind</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getARenderedKind()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ARenderedKind();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.abstractions.AElement#getAContainingComponent <em>AContaining Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Component</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getAContainingComponent()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_AContainingComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.AElement#getTPackageUri <em>TPackage Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>TPackage Uri</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTPackageUri()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_TPackageUri();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.AElement#getTClassifierName <em>TClassifier Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>TClassifier Name</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTClassifierName()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_TClassifierName();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.acore.abstractions.AElement#getTFeatureName <em>TFeature Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>TFeature Name</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTFeatureName()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_TFeatureName();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.abstractions.AElement#getTPackage <em>TPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TPackage</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTPackage()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_TPackage();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.abstractions.AElement#getTClassifier <em>TClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TClassifier</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTClassifier()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_TClassifier();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.abstractions.AElement#getTFeature <em>TFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TFeature</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTFeature()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_TFeature();

	/**
	 * Returns the meta object for the reference '{@link org.langlets.acore.abstractions.AElement#getTACoreAStringClass <em>TA Core AString Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>TA Core AString Class</em>'.
	 * @see org.langlets.acore.abstractions.AElement#getTACoreAStringClass()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_TACoreAStringClass();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#indentLevel() <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indent Level</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#indentLevel()
	 * @generated
	 */
	EOperation getAElement__IndentLevel();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#indentationSpaces() <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#indentationSpaces()
	 * @generated
	 */
	EOperation getAElement__IndentationSpaces();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#indentationSpaces(java.lang.Integer) <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#indentationSpaces(java.lang.Integer)
	 * @generated
	 */
	EOperation getAElement__IndentationSpaces__Integer();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#stringOrMissing(java.lang.String) <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Or Missing</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#stringOrMissing(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__StringOrMissing__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#stringIsEmpty(java.lang.String) <em>String Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Is Empty</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#stringIsEmpty(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__StringIsEmpty__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList, java.lang.String) <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Of String To String With Separator</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__ListOfStringToStringWithSeparator__EList_String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList) <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Of String To String With Separator</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAElement__ListOfStringToStringWithSeparator__EList();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aPackageFromUri(java.lang.String) <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>APackage From Uri</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aPackageFromUri(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__APackageFromUri__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aClassifierFromUriAndName(java.lang.String, java.lang.String) <em>AClassifier From Uri And Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AClassifier From Uri And Name</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aClassifierFromUriAndName(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AClassifierFromUriAndName__String_String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aFeatureFromUriAndNames(java.lang.String, java.lang.String, java.lang.String) <em>AFeature From Uri And Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AFeature From Uri And Names</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aFeatureFromUriAndNames(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AFeatureFromUriAndNames__String_String_String();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aCoreAStringClass() <em>ACore AString Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AString Class</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aCoreAStringClass()
	 * @generated
	 */
	EOperation getAElement__ACoreAStringClass();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aCoreARealClass() <em>ACore AReal Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AReal Class</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aCoreARealClass()
	 * @generated
	 */
	EOperation getAElement__ACoreARealClass();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aCoreAIntegerClass() <em>ACore AInteger Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AInteger Class</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aCoreAIntegerClass()
	 * @generated
	 */
	EOperation getAElement__ACoreAIntegerClass();

	/**
	 * Returns the meta object for the '{@link org.langlets.acore.abstractions.AElement#aCoreAObjectClass() <em>ACore AObject Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AObject Class</em>' operation.
	 * @see org.langlets.acore.abstractions.AElement#aCoreAObjectClass()
	 * @generated
	 */
	EOperation getAElement__ACoreAObjectClass();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AbstractionsFactory getAbstractionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.langlets.acore.abstractions.impl.ANamedImpl <em>ANamed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.abstractions.impl.ANamedImpl
		 * @see org.langlets.acore.abstractions.impl.AbstractionsPackageImpl#getANamed()
		 * @generated
		 */
		EClass ANAMED = eINSTANCE.getANamed();

		/**
		 * The meta object literal for the '<em><b>AName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__ANAME = eINSTANCE.getANamed_AName();

		/**
		 * The meta object literal for the '<em><b>AUndefined Name Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__AUNDEFINED_NAME_CONSTANT = eINSTANCE.getANamed_AUndefinedNameConstant();

		/**
		 * The meta object literal for the '<em><b>ABusiness Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__ABUSINESS_NAME = eINSTANCE.getANamed_ABusinessName();

		/**
		 * The meta object literal for the '{@link org.langlets.acore.abstractions.impl.AElementImpl <em>AElement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.acore.abstractions.impl.AElementImpl
		 * @see org.langlets.acore.abstractions.impl.AbstractionsPackageImpl#getAElement()
		 * @generated
		 */
		EClass AELEMENT = eINSTANCE.getAElement();

		/**
		 * The meta object literal for the '<em><b>ALabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__ALABEL = eINSTANCE.getAElement_ALabel();

		/**
		 * The meta object literal for the '<em><b>AKind Base</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__AKIND_BASE = eINSTANCE.getAElement_AKindBase();

		/**
		 * The meta object literal for the '<em><b>ARendered Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__ARENDERED_KIND = eINSTANCE.getAElement_ARenderedKind();

		/**
		 * The meta object literal for the '<em><b>AContaining Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__ACONTAINING_COMPONENT = eINSTANCE.getAElement_AContainingComponent();

		/**
		 * The meta object literal for the '<em><b>TPackage Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__TPACKAGE_URI = eINSTANCE.getAElement_TPackageUri();

		/**
		 * The meta object literal for the '<em><b>TClassifier Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__TCLASSIFIER_NAME = eINSTANCE.getAElement_TClassifierName();

		/**
		 * The meta object literal for the '<em><b>TFeature Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__TFEATURE_NAME = eINSTANCE.getAElement_TFeatureName();

		/**
		 * The meta object literal for the '<em><b>TPackage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__TPACKAGE = eINSTANCE.getAElement_TPackage();

		/**
		 * The meta object literal for the '<em><b>TClassifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__TCLASSIFIER = eINSTANCE.getAElement_TClassifier();

		/**
		 * The meta object literal for the '<em><b>TFeature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__TFEATURE = eINSTANCE.getAElement_TFeature();

		/**
		 * The meta object literal for the '<em><b>TA Core AString Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__TA_CORE_ASTRING_CLASS = eINSTANCE.getAElement_TACoreAStringClass();

		/**
		 * The meta object literal for the '<em><b>Indent Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___INDENT_LEVEL = eINSTANCE.getAElement__IndentLevel();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___INDENTATION_SPACES = eINSTANCE.getAElement__IndentationSpaces();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___INDENTATION_SPACES__INTEGER = eINSTANCE.getAElement__IndentationSpaces__Integer();

		/**
		 * The meta object literal for the '<em><b>String Or Missing</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___STRING_OR_MISSING__STRING = eINSTANCE.getAElement__StringOrMissing__String();

		/**
		 * The meta object literal for the '<em><b>String Is Empty</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___STRING_IS_EMPTY__STRING = eINSTANCE.getAElement__StringIsEmpty__String();

		/**
		 * The meta object literal for the '<em><b>List Of String To String With Separator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = eINSTANCE
				.getAElement__ListOfStringToStringWithSeparator__EList_String();

		/**
		 * The meta object literal for the '<em><b>List Of String To String With Separator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = eINSTANCE
				.getAElement__ListOfStringToStringWithSeparator__EList();

		/**
		 * The meta object literal for the '<em><b>APackage From Uri</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___APACKAGE_FROM_URI__STRING = eINSTANCE.getAElement__APackageFromUri__String();

		/**
		 * The meta object literal for the '<em><b>AClassifier From Uri And Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = eINSTANCE
				.getAElement__AClassifierFromUriAndName__String_String();

		/**
		 * The meta object literal for the '<em><b>AFeature From Uri And Names</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = eINSTANCE
				.getAElement__AFeatureFromUriAndNames__String_String_String();

		/**
		 * The meta object literal for the '<em><b>ACore AString Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_ASTRING_CLASS = eINSTANCE.getAElement__ACoreAStringClass();

		/**
		 * The meta object literal for the '<em><b>ACore AReal Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_AREAL_CLASS = eINSTANCE.getAElement__ACoreARealClass();

		/**
		 * The meta object literal for the '<em><b>ACore AInteger Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_AINTEGER_CLASS = eINSTANCE.getAElement__ACoreAIntegerClass();

		/**
		 * The meta object literal for the '<em><b>ACore AObject Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_AOBJECT_CLASS = eINSTANCE.getAElement__ACoreAObjectClass();

	}

} //AbstractionsPackage
