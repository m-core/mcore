/**
 */
package org.langlets.acore.abstractions.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import org.langlets.acore.abstractions.AbstractionsPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractionsXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractionsXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		AbstractionsPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the AbstractionsResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new AbstractionsResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new AbstractionsResourceFactoryImpl());
		}
		return registrations;
	}

} //AbstractionsXMLProcessor
