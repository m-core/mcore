/**
 */
package org.langlets.acore.abstractions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.langlets.acore.AComponent;
import org.langlets.acore.APackage;

import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.AFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AElement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getALabel <em>ALabel</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getAContainingComponent <em>AContaining Component</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTPackageUri <em>TPackage Uri</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTClassifierName <em>TClassifier Name</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTFeatureName <em>TFeature Name</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTPackage <em>TPackage</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTClassifier <em>TClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTFeature <em>TFeature</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.AElement#getTACoreAStringClass <em>TA Core AString Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement()
 * @model abstract="true"
 * @generated
 */

public interface AElement extends EObject {
	/**
	 * Returns the value of the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ALabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALabel</em>' attribute.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_ALabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='aRenderedKind\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getALabel();

	/**
	 * Returns the value of the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AKind Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AKind Base</em>' attribute.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_AKindBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eClass().name\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAKindBase();

	/**
	 * Returns the value of the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ARendered Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ARendered Kind</em>' attribute.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_ARenderedKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in\nif chain12.toUpperCase().oclIsUndefined() \n then null \n else chain12.toUpperCase()\n  endif) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getARenderedKind();

	/**
	 * Returns the value of the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Component</em>' reference.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_AContainingComponent()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsTypeOf(acore::AComponent)\r\n  then self.eContainer().oclAsType(acore::AComponent)\r\n  else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)\r\n  then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent\r\n  else null endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AComponent getAContainingComponent();

	/**
	 * Returns the value of the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPackage Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPackage Uri</em>' attribute.
	 * @see #isSetTPackageUri()
	 * @see #unsetTPackageUri()
	 * @see #setTPackageUri(String)
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TPackageUri()
	 * @model unsettable="true" transient="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getTPackageUri();

	/** 
	 * Sets the value of the '{@link org.langlets.acore.abstractions.AElement#getTPackageUri <em>TPackage Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TPackage Uri</em>' attribute.
	 * @see #isSetTPackageUri()
	 * @see #unsetTPackageUri()
	 * @see #getTPackageUri()
	 * @generated
	 */
	void setTPackageUri(String value);

	/**
	 * Unsets the value of the '{@link org.langlets.acore.abstractions.AElement#getTPackageUri <em>TPackage Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTPackageUri()
	 * @see #getTPackageUri()
	 * @see #setTPackageUri(String)
	 * @generated
	 */
	void unsetTPackageUri();

	/**
	 * Returns whether the value of the '{@link org.langlets.acore.abstractions.AElement#getTPackageUri <em>TPackage Uri</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TPackage Uri</em>' attribute is set.
	 * @see #unsetTPackageUri()
	 * @see #getTPackageUri()
	 * @see #setTPackageUri(String)
	 * @generated
	 */
	boolean isSetTPackageUri();

	/**
	 * Returns the value of the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TClassifier Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TClassifier Name</em>' attribute.
	 * @see #isSetTClassifierName()
	 * @see #unsetTClassifierName()
	 * @see #setTClassifierName(String)
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TClassifierName()
	 * @model unsettable="true" transient="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getTClassifierName();

	/** 
	 * Sets the value of the '{@link org.langlets.acore.abstractions.AElement#getTClassifierName <em>TClassifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TClassifier Name</em>' attribute.
	 * @see #isSetTClassifierName()
	 * @see #unsetTClassifierName()
	 * @see #getTClassifierName()
	 * @generated
	 */
	void setTClassifierName(String value);

	/**
	 * Unsets the value of the '{@link org.langlets.acore.abstractions.AElement#getTClassifierName <em>TClassifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTClassifierName()
	 * @see #getTClassifierName()
	 * @see #setTClassifierName(String)
	 * @generated
	 */
	void unsetTClassifierName();

	/**
	 * Returns whether the value of the '{@link org.langlets.acore.abstractions.AElement#getTClassifierName <em>TClassifier Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TClassifier Name</em>' attribute is set.
	 * @see #unsetTClassifierName()
	 * @see #getTClassifierName()
	 * @see #setTClassifierName(String)
	 * @generated
	 */
	boolean isSetTClassifierName();

	/**
	 * Returns the value of the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TFeature Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TFeature Name</em>' attribute.
	 * @see #isSetTFeatureName()
	 * @see #unsetTFeatureName()
	 * @see #setTFeatureName(String)
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TFeatureName()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getTFeatureName();

	/** 
	 * Sets the value of the '{@link org.langlets.acore.abstractions.AElement#getTFeatureName <em>TFeature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TFeature Name</em>' attribute.
	 * @see #isSetTFeatureName()
	 * @see #unsetTFeatureName()
	 * @see #getTFeatureName()
	 * @generated
	 */
	void setTFeatureName(String value);

	/**
	 * Unsets the value of the '{@link org.langlets.acore.abstractions.AElement#getTFeatureName <em>TFeature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTFeatureName()
	 * @see #getTFeatureName()
	 * @see #setTFeatureName(String)
	 * @generated
	 */
	void unsetTFeatureName();

	/**
	 * Returns whether the value of the '{@link org.langlets.acore.abstractions.AElement#getTFeatureName <em>TFeature Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TFeature Name</em>' attribute is set.
	 * @see #unsetTFeatureName()
	 * @see #getTFeatureName()
	 * @see #setTFeatureName(String)
	 * @generated
	 */
	boolean isSetTFeatureName();

	/**
	 * Returns the value of the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TPackage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TPackage</em>' reference.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let p: String = tPackageUri in\naPackageFromUri(p)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getTPackage();

	/**
	 * Returns the value of the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TClassifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TClassifier</em>' reference.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let p: String = tPackageUri in\nlet c: String = tClassifierName in\naClassifierFromUriAndName(p, c)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getTClassifier();

	/**
	 * Returns the value of the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TFeature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TFeature</em>' reference.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let p: String = tPackageUri in\nlet c: String = tClassifierName in\nlet f: String = tFeatureName in\naFeatureFromUriAndNames(p, c, f)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AFeature getTFeature();

	/**
	 * Returns the value of the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TA Core AString Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TA Core AString Class</em>' reference.
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#getAElement_TACoreAStringClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T A Core A String Class'"
	 *        annotation="http://www.xocl.org/OCL derive='aCoreAStringClass()\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Package'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getTACoreAStringClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(AElement)\n  then eContainer().oclAsType(AElement).indentLevel() + 1\n  else 0 endif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	Integer indentLevel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\nindentationSpaces(numberOfSpaces)\n'"
	 * @generated
	 */
	String indentationSpaces();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let sizeMinusOne: Integer = let e1: Integer = size - 1 in \n if e1.oclIsInvalid() then null else e1 endif in\nif (let e0: Boolean = size < 1 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\'\n  else (let e0: String = indentationSpaces(sizeMinusOne).concat(\' \') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	String indentationSpaces(Integer size);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pAnnotation="http://www.montages.com/mCore/MCore mName='p'"
	 *        annotation="http://www.xocl.org/OCL body='if (stringIsEmpty(p)) \n  =true \nthen \'MISSING\'\n  else p\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	String stringOrMissing(String p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if ( s.oclIsUndefined()) \n  =true \nthen true else if (let e0: Boolean = \'\' = let e0: String = s.trim() in \n if e0.oclIsInvalid() then null else e0 endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	Boolean stringIsEmpty(String s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementsMany="true"
	 *        annotation="http://www.xocl.org/OCL body='let f:String = elements->asOrderedSet()->first() in\r\nif f.oclIsUndefined()\r\n  then \'\'\r\n  else if elements-> size()=1 then f else\r\n    let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=\'\'|ac.concat(separator).concat(it)) in\r\n    f.concat(rest)\r\n  endif endif\r\n'"
	 * @generated
	 */
	String listOfStringToStringWithSeparator(EList<String> elements, String separator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementsMany="true"
	 *        annotation="http://www.xocl.org/OCL body='let defaultSeparator: String = \', \' in\nlistOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	String listOfStringToStringWithSeparator(EList<String> elements);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aPackageFromUri(packageUri)\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	APackage aPackageFromUri(String packageUri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let p: acore::APackage = aPackageFromUri(uri) in\nif p = null\n  then null\n  else p.aClassifierFromName(name) endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	AClassifier aClassifierFromUriAndName(String uri, String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in\nlet cAsClass: acore::classifiers::AClass = let chain: acore::classifiers::AClassifier = c in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClass)\n    then chain.oclAsType(acore::classifiers::AClass)\n    else null\n  endif\n  endif in\nif cAsClass = null\n  then null\n  else cAsClass.aFeatureFromName(featureName) endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	AFeature aFeatureFromUriAndNames(String uri, String className, String featureName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAStringName: String = \'AString\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreAStringClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreARealName: String = \'AReal\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreARealClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAIntegerName: String = \'AInteger\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreAIntegerClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let aCoreValuesPackageUri: String = \'http://www.langlets.org/ACore/ACore/Values\' in\nlet aCoreAObjectName: String = \'AObject\' in\naClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreAObjectClass();

} // AElement
