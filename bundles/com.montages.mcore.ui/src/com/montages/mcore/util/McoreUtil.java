package com.montages.mcore.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import com.montages.common.CommonPlugin;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MResource;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.PathUtils;

/**
 * Utility classes for actions related to Mcore.
 * 
 */
public class McoreUtil {

	public static EPackage findEPackage(List<EPackage> referencedEPackage, EPackage firstPackage) {
		String nsURI = firstPackage.getNsURI();
		if (nsURI == null) {
			return null;
		}
		for (EPackage p : referencedEPackage) {
			if (nsURI.equals(p.getNsURI())) {
				return p;
			}
		}
		return null;
	}

	public static List<EPackage> collectEPackages(URI componentURI) {
		if (componentURI != null) {
			ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getURIConverter().getURIMap().putAll(CommonPlugin.computeURIMap());

			MComponent component = null;
			Resource resource = resourceSet.getResource(componentURI.trimFragment(), true);

			if (resource != null && !resource.getContents().isEmpty()) {
				EObject root = resource.getContents().get(0);
				component = root instanceof MComponent ? (MComponent) root : null;
			}

			if (component != null) {
				return collectEPackages(component);
			}
		}
		return Collections.<EPackage> emptyList();
	}

	public static List<EPackage> collectEPackages(MComponent component) {
		List<EPackage> packages = new ArrayList<EPackage>();
		Dependencies dependencies = Dependencies.compute(component);
		URIConverter uriConverter = component.eResource().getResourceSet().getURIConverter();
		for (MComponent c : dependencies.sort()) {
			for (MPackage p : c.getOwnedPackage()) {
				if (p.getInternalEPackage() != null) {
					EPackage ePackage = p.getInternalEPackage();
					Resource eResource = ePackage.eResource();
					eResource.setURI(uriConverter.normalize(eResource.getURI()));
					packages.add(ePackage);
				}
			}
		}
		return packages;
	}

	public static List<URI> collectEPackageURIs(MComponent component) {
		List<URI> packages = new ArrayList<URI>();
		Dependencies dependencies = Dependencies.compute(component);
		for (MComponent c: dependencies.sort()) {
			for (MPackage p: c.getOwnedPackage()) {
				if (p.getInternalEPackage() != null) {
					packages.add(URI.createURI(p.getInternalEPackage().getNsURI()));
				}
			}
		}
		return packages;
	}

	public static List<Resource> collectEcoreResources(MComponent component) {
		List<Resource> collected = new ArrayList<Resource>();
		for (MPackage p : component.getOwnedPackage()) {
			if (p.getInternalEPackage() != null) {
				collected.add(p.getInternalEPackage().eResource());
			}
		}
		return collected;
	}

	public static EPackage getRootPackage(MResource mResource) {
		EPackage ePackage = null;
		MPackage mPackage = mResource.getRootObjectPackage();

		if (mPackage == null) {
			if (!mResource.getObject().isEmpty()) {
				MObject object = mResource.getObject().get(0);
				if (object.getTypePackage() != null) {
					mPackage = object.getTypePackage();
				} else if (object.getType() != null) {
					mPackage = object.getType().getContainingPackage();
				}
			}
		}

		if (mPackage != null) {
			ePackage = mPackage.getInternalEPackage();
		}

		return ePackage;
	}

	public static boolean isDiagramURI(URI elementURI) {
		return "mcore_diagram".equals(elementURI.fileExtension());
	}
	
	public static MComponent findComponentInTransactionResources(MComponent component) {
		Resource eResource = component.eResource();
		if (eResource == null) {
			return component;
		} 
		URI uri = eResource.getURI();
		if (uri == null) {
			return component;
		}
		Object owner = new Object();
		McoreDiagramEditingDoamin domain = null;
		uri = ResourceService.normalize(eResource.getResourceSet(), uri);
		try {
			String id = PathUtils.buildDomainID(uri);
			if (McoreEditingDomainFactory.getInstance().exists(id)) {
				domain = McoreEditingDomainFactory.getInstance().createEditingDomain(id, owner);
				return findComponentInTransactionResources(uri, component, domain);
			}
		}finally{ 
			if (domain != null) {
				McoreEditingDomainFactory.getInstance().dispose(domain, owner);
			}
		}
		return component;
	}
	
	public static MComponent findComponentInTransactionResources(URI mcoreURI, MComponent defaultMCore, McoreDiagramEditingDoamin domain) {
		MComponent result;
		if (mcoreURI.isPlatformResource() && domain != null) {
			Resource r = domain.getResourceSet().getResource(mcoreURI, true);
			result = r.getContents().isEmpty() ? defaultMCore : (MComponent)r.getContents().get(0);
		} else {
			result = defaultMCore;
		}
		return result;
	}
}
