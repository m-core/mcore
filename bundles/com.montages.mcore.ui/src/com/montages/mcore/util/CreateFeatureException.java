package com.montages.mcore.util;


public class CreateFeatureException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CreateFeatureException(String message) {
		super(message);
	}
}
