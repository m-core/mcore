package com.montages.mcore.util;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

public class TableEditorInputFactory  implements IElementFactory {

	public static final String ID = TableEditorInputFactory.class.getName();

	public TableEditorInputFactory() {}

	@Override
	public IAdaptable createElement(IMemento memento) {
		return new TableEditorInput(memento);
	}

}