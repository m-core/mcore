package com.montages.mcore.util.mailer;

import java.io.IOException;
import java.net.URL;

public class GMailConfig extends EMailerConfigFromFile {

	public final static String MAIL_SMTP_HOST = "mail.smtp.host";

	public final static String MAIL_SMTP_SOCKETFACTORY_PORT = "mail.smtp.socketFactory.port";

	public final static String MAIL_SMTP_PORT = "mail.smtp.port";

	public final static String MAIL_SMTP_AUTH = "mail.smtp.auth";

	public final static String MAIL_SMTP_SOCKETFACTORY_CLASS = "mail.smtp.socketFactory.class";

	public GMailConfig(String path) throws IOException {
		super(path);
	}

	public GMailConfig(URL url) throws IOException {
		super(url);
	}

	@Override
	public String getSmtpHost() {
		return getAllMailProperties().getProperty(MAIL_SMTP_HOST);
	}

	@Override
	public String getSmtpPort() {
		String smtpPort = getAllMailProperties().getProperty(MAIL_SMTP_PORT);
		return smtpPort == null ? DEFAULT_SMTP_PORT : smtpPort;
	}

	@Override
	public String getSmtpAuth() {
		String smtpAuth = getAllMailProperties().getProperty(MAIL_SMTP_AUTH);
		return smtpAuth == null ? DEFAULT_SMTP_AUTH : smtpAuth;
	}
}
