package com.montages.mcore.util.mailer;

import java.io.File;
import java.io.IOException;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Emailer implements IMailer {

	private final MailConfig myConfig;

	public Emailer(String path) throws IOException {
		this(new EMailerConfigFromFile(path));
	}

	public Emailer() throws IOException {
		this(new EMailerConfigFromFile(EMailerConfigFromFile.DEFAULT_CONFIG_PATH));
	}

	public Emailer(MailConfig mailConfig) {
		myConfig = mailConfig;
		myConfig.getAllMailProperties().setProperty(GMailConfig.MAIL_SMTP_SOCKETFACTORY_CLASS, "javax.net.ssl.SSLSocketFactory");
		myConfig.getAllMailProperties().setProperty(GMailConfig.MAIL_SMTP_SOCKETFACTORY_PORT, myConfig.getSmtpPort());
		myConfig.getAllMailProperties().setProperty(GMailConfig.MAIL_SMTP_HOST, myConfig.getSmtpHost());
		myConfig.getAllMailProperties().setProperty(GMailConfig.MAIL_SMTP_AUTH, myConfig.getSmtpAuth());
		myConfig.getAllMailProperties().setProperty(GMailConfig.MAIL_SMTP_PORT, myConfig.getSmtpPort());
	}

	public void send(String to, MailMessage message) throws MessagingException {
		if (false == myConfig instanceof EMailerConfigFromFile && (myConfig.getTo() == null || myConfig.getTo().isEmpty())) {
			throw new MessagingException("Not expected message format.");
		}
		if (myConfig instanceof EMailerConfigFromFile) {
			((EMailerConfigFromFile) myConfig).setTo(to);
		}
		send(message);
	}

	@Override
	public void send(MailMessage message) throws MessagingException {
		Session session = Session.getDefaultInstance(myConfig.getAllMailProperties(), new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myConfig.getSmtpUser(), myConfig.getSmtpPassword());
			}
		});
		Message mimeMessage = new MimeMessage(session);
		mimeMessage.setFrom(new InternetAddress(myConfig.getFrom()));
		mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(myConfig.getTo()));
		mimeMessage.setSubject(message.getSubject());
		if (message.getAttachments() != null && !message.getAttachments().isEmpty()) {
			setupBodyWithAttacments(mimeMessage, message);
		} else {
			mimeMessage.setText(message.getBody());
		}
		Transport.send(mimeMessage);
	}

	public void send(String subject, String body) throws MessagingException {
		send(new IMailer.StandartMailMessageImpl(subject, body));
	}

	private void setupBodyWithAttacments(Message mimeMessage, MailMessage message) throws MessagingException {
		Multipart mp = new MimeMultipart();
		MimeBodyPart mbp = new MimeBodyPart();
		mbp.setText(message.getBody());
		mp.addBodyPart(mbp);
		for (File nextAttachment : message.getAttachments()) {
			attachFile(mp, nextAttachment);
		}
		mimeMessage.setContent(mp);
	}

	private void attachFile(Multipart mp, File file) throws MessagingException {
		MimeBodyPart mbp = new MimeBodyPart();
		FileDataSource fds = new FileDataSource(file);
		mbp.setDataHandler(new DataHandler(fds));
		mbp.setFileName(fds.getName());
		mp.addBodyPart(mbp);
	}
}
