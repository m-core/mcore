package com.montages.mcore.util.mailer;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class EMailerConfigFromFile extends XMLPropertiesHelper implements MailConfig {

	public final static String DEFAULT_CONFIG_PATH = "settings/mail.config";

	private final Properties myMailProperties;

	public final static String SEND_FROM_KEY = "from";

	public final static String SMTP_USER_KEY = "mail.smtp.user";

	public final static String SMTP_PASSWORD_KEY = "mail.smtp.password";

	private final static String MAIL_SUBJECT_KEY = "mail.subject";

	private final static String MAIL_BODY_KEY = "mail.body";

	private String myTo;

	public EMailerConfigFromFile(String path) throws IOException {
		File config = new File(path);
		if (!config.exists() || config.isDirectory()) {
			throw new IllegalArgumentException("Can't find emailer config file : " + config);
		}
		myMailProperties = readFile(path);
	}

	public EMailerConfigFromFile(URL url) throws IOException {
		myMailProperties = readStream(url.openStream());
	}

	@Override
	public Properties getAllMailProperties() {
		return myMailProperties;
	}

	public void setTo(String to) {
		myTo = to;
	}

	@Override
	public String getTo() {
		return myTo;
	}

	@Override
	public String getFrom() {
		return myMailProperties.getProperty(SEND_FROM_KEY);
	}

	@Override
	public String getHost() {
		return null;
	}

	@Override
	public String getSmtpHost() {
		return null;
	}

	@Override
	public String getSmtpUser() {
		return myMailProperties.getProperty(SMTP_USER_KEY);
	}

	@Override
	public String getSmtpPassword() {
		return myMailProperties.getProperty(SMTP_PASSWORD_KEY);
	}

	@Override
	public String getStoreProtocol() {
		return null;
	}

	@Override
	public String getTransportProtocol() {
		return null;
	}

	@Override
	public String getUser() {
		return null;
	}

	@Override
	public String getSmtpAuth() {
		return null;
	}

	@Override
	public String getSmtpPort() {
		return null;
	}

	@Override
	public String getSubject() {
		return myMailProperties.getProperty(MAIL_SUBJECT_KEY);
	}

	@Override
	public String getBody() {
		return myMailProperties.getProperty(MAIL_BODY_KEY);
	}

}
