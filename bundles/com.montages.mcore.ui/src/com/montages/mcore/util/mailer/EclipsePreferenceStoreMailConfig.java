package com.montages.mcore.util.mailer;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import com.montages.mcore.ui.McoreUIPlugin;

public class EclipsePreferenceStoreMailConfig extends GMailConfig {

	public EclipsePreferenceStoreMailConfig() throws IOException {
		super(constructLocalConfigURL());
	}

	@Override
	public String getSmtpHost() {
		return McoreUIPlugin.getInstance().getPreferenceStore().getString(MAIL_SMTP_HOST);
	}

	@Override
	public String getSmtpPort() {
		return McoreUIPlugin.getInstance().getPreferenceStore().getString(MAIL_SMTP_PORT);
	}

	@Override
	public String getFrom() {
		return McoreUIPlugin.getInstance().getPreferenceStore().getString(SEND_FROM_KEY);
	}

	@Override
	public String getSmtpUser() {
		return McoreUIPlugin.getInstance().getPreferenceStore().getString(SMTP_USER_KEY);
	}

	@Override
	public String getSmtpPassword() {
		return McoreUIPlugin.getInstance().getPreferenceStore().getString(SMTP_PASSWORD_KEY);
	}

	private static URL constructLocalConfigURL() {
		Bundle bundle = Platform.getBundle(McoreUIPlugin.PLUGIN_ID);
		Path path = new Path(EMailerConfigFromFile.DEFAULT_CONFIG_PATH);
		return FileLocator.find(bundle, path, null);
	}

}
