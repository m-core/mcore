package com.montages.mcore.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;

import com.montages.mcore.MComponent;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;

public class MComponentResourceIdMap {

	public static final MComponentResourceIdMap EMPTY_MAP = new MComponentResourceIdMap();

	private final Map<URI, String> myResourceName2Id;

	private final Map<String, String> myResourceId2Id;

	private MComponentResourceIdMap() {
		myResourceName2Id = new HashMap<URI, String>();
		myResourceId2Id = new HashMap<String, String>();
	}

	public void add(URI uri, String id) {
		myResourceName2Id.put(uri, id);
		myResourceId2Id.put(id, id);
	}

	public String findId(URI name) {
		return myResourceName2Id.get(name);
	}

	public String findId(String id) {
		return myResourceId2Id.get(id);
	}

	public static MComponentResourceIdMap mapResourceName2Id(MComponent mComponent) {
		MComponentResourceIdMap result = new MComponentResourceIdMap();
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final IProject container = root.getProject(mComponent.getDerivedBundleName());
		for (MResourceFolder folder : mComponent.getResourceFolder()) {
			mapResourceFolder(container, folder, result);
		}
		return result;
	}

	private static void mapResourceFolder(IContainer container, MResourceFolder folder, MComponentResourceIdMap resourceIdMap) {
		IFolder realFolder = container.getFolder(new Path(folder.getEName()));
		for (MResource mResource : folder.getResource()) {
			URI nextURI = constructURI(container, folder, mResource);
			resourceIdMap.add(nextURI, mResource.getId());
		}
		// Continue on sub folders.
		for (MResourceFolder subFolder : folder.getFolder()) {
			mapResourceFolder(realFolder, subFolder, resourceIdMap);
		}
	}

	private static URI constructURI(IContainer container, MResourceFolder mFolder, MResource mResource) {
		String resourceFileName = mResource.getEName();
		IFolder realFolder = container.getFolder(new Path(mFolder.getEName()));
		IPath resourceFilePath = realFolder.getFullPath().append(resourceFileName);
		return URI.createPlatformResourceURI(resourceFilePath.toString(), false);
	}
}
