package com.montages.mcore.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;

public class MTableEditorVisitor implements IResourceProxyVisitor {

	private List<IResource> models = new ArrayList<IResource>();

	@Override
	public boolean visit(IResourceProxy proxy) throws CoreException {
		IResource resource = proxy.requestResource();
		if (resource.getType() == IResource.PROJECT) {
			String resName = resource.getName();
			if (resName.endsWith(".base") || resName.endsWith(".edit") || resName.endsWith(".editor")) {
				return false;
			} else {
				return true;
			}
		}
		if (isModelFile(resource)) {
			models.add(resource);
		}
		return true;
	}

	public List<IResource> getModels() {
		return models;
	}

	protected boolean isModelFile(IResource resource) {
		boolean isModelFile = resource.getType() == IResource.FILE && resource.getParent().getName().equals("model");
		return isModelFile && "mtableeditor".equals(resource.getFileExtension());
	}
}
