package com.montages.mcore.codegen.ui.conversions;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.montages.mcore.MComponent;

public class ComponentImportData {

	private final MComponent importedComponent;

	private boolean shouldMerge;

	private MComponent componentToMerge;

	private boolean isMergedResourceLoaded = false;

	public ComponentImportData(MComponent component) {
		importedComponent = component;
	}

	public MComponent getComponentToMerge() {
		if (isMergedResourceLoaded) {
			return componentToMerge;
		}
		isMergedResourceLoaded = true;
		URI uri = importedComponent.eResource().getURI();
		uri = importedComponent.eResource().getResourceSet().getURIConverter().normalize(uri);
		Resource resource = null;
		try {
			resource = new ResourceSetImpl().getResource(uri, true);
		} catch(Exception e) {
			// nothing to do
		}
		if (resource == null || resource.getContents().isEmpty()) {
			return null;
		}
		componentToMerge = (MComponent) resource.getContents().get(0);
		return componentToMerge;
	}

	public MComponent getImportedComponent() {
		return importedComponent;
	}

	public boolean shouldMerge() {
		return shouldMerge;
	}

	public void setShouldMerge(boolean shouldMerge) {
		this.shouldMerge = shouldMerge;
	}

	public boolean canBeMerged() {
		return getComponentToMerge() != null && EcoreUtil.getURI(getComponentToMerge()).fragment().equals(EcoreUtil.getURI(importedComponent).fragment());
	}

	public void setURI(URI uri) {
		importedComponent.eResource().setURI(uri);
		isMergedResourceLoaded = false;
	}
}
