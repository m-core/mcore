package com.montages.mcore.codegen.ui.handlers;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import com.montages.mcore.MComponent;
import com.montages.mcore.ecore2mcore.Ecore2Mcore;
import com.montages.mcore.ui.handlers.ResourceSelectionHandler;
import com.montages.mcore.util.ResourceService;

public class Ecore2McoreHandler extends ResourceSelectionHandler {

	@Override
	protected void doExcecute(final Resource selectedResource, ExecutionEvent event) {
		final ResourceSet resourceSet;
		if (selectedResource.getResourceSet() != null) {
			resourceSet = selectedResource.getResourceSet();
		} else {
			resourceSet = new ResourceSetImpl();
		}

		final URI uri = selectedResource.getURI().trimFileExtension().appendFileExtension("mcore");
		final Job job = new Job("Generate Mcore") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				MComponent result = null;
				try {
					 result = new Ecore2Mcore().transform(selectedResource);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					return Status.CANCEL_STATUS;
				}

				if (result != null) {
					Resource resource = resourceSet.createResource(uri);
					resource.getContents().add(result);
					try {
						resource.save(ResourceService.getSaveOptions());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				return Status.OK_STATUS;
			}
		};

		job.setUser(true);
		job.schedule();
	}


}
