package com.montages.mcore.codegen.ui.conversions;

import static com.montages.mcore.util.ResourceService.createResourceSet;
import static com.montages.mcore.util.ResourceService.getMcoreOptions;
import static com.montages.mcore.util.ResourceService.getSaveOptions;
import static com.montages.mcore.util.ResourceService.normalize;
import static com.montages.mcore.util.ResourceService.normalizeResourceURI;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.langlets.plugin.ComponentType;
import org.langlets.plugin.EditorConfigModelAssociationType;
import org.langlets.plugin.ExtensionType;
import org.langlets.plugin.PluginType;
import org.langlets.plugin.util.PluginResourceFactoryImpl;
import org.xocl.editorconfig.EditorConfig;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MEditor;
import com.montages.mcore.MNamedEditor;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.SimpleType;
import com.montages.mcore.ecore2mcore.Ecore2Mcore;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.operations.CreateProject;
import com.montages.mcore.ui.operations.GenerateEcore;
import com.montages.mcore.ui.operations.GenerateEditorConfig;
import com.montages.mcore.ui.operations.GenerateInstanceResources;
import com.montages.mcore.ui.operations.GenerateTableEditor;
import com.montages.mcore.ui.operations.PluginExtensions;
//import com.montages.mcore.util.DiagramUtils;
import com.montages.mcore.util.McoreResourceFactoryImpl;
import com.montages.mcore.util.RenameService;
import com.montages.mcore.util.URIService;
import com.montages.mtableeditor.MEditorConfig;
import com.montages.mtableeditor.MEditorConfigAction;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

/**
 * Importer performs an import from a mcore repository into a list of mcore
 * components.
 * 
 */
public class Importer {

	private final URIService uriService = new URIService();

	private final URI uri;

	private List<ComponentImportData> components;

	private List<MEditorConfig> mEditorConfigs;

	private List<MComponent> platformComponents = new ArrayList<MComponent>();

	private Map<String, EditorConfig> associations;

	private List<Diagram> diagrams;

	private ResourceSet resourceSet = createResourceSet();

	private Resource resource;

	public Importer(URI uri) {
		if (uri == null) {
			this.uri = null;
		} else {
			this.uri = uri.scheme() == null ? URI.createPlatformResourceURI(uri.toString(), false) : uri;
		}
	}

	public Importer(Resource resource) {
		this.uri = resource.getURI();
		this.resource = resource;
		if (resource.getResourceSet() != null) {
			resourceSet = resource.getResourceSet();
		}
	}

	/**
	 * Takes the URI pointing to a resource that contains a single MRepository
	 * element and converts it to a list of MComponent.
	 * 
	 * @param uri
	 * @return List of components
	 */
	protected List<ComponentImportData> importMcore() {
		ExportMCoreResourceFactory exportFactory = new ExportMCoreResourceFactory();
		if (!isMCoreImport())
			return Collections.emptyList();

		resource = resource != null ? resource : exportFactory.createResource(uri);
		if (!resource.isLoaded()) {
			try {
				resource.load(getMcoreOptions(resource.getResourceSet(), null));
			} catch (Exception e) {
				McoreUIPlugin.log(e);
			}
		}
		if (resource != null && !resource.getContents().isEmpty()) {
			EObject root = resource.getContents().get(0);
			if (root instanceof MRepository) {
				return importRepository((MRepository) root);
			} else if (root instanceof MComponent) {
				return importComponent((MComponent) root);
			}
		}

		return Collections.<ComponentImportData>emptyList();
	}

	protected List<ComponentImportData> importComponent(MComponent component) {
		component = EcoreUtil.copy(component);

		if (existInRepository(component)) {
			return Collections.emptyList();
		}

		createMcoreResource(component);

		return Arrays.asList(new ComponentImportData(component));
	}

	/**
	 * Takes a repository and creates for each of its components.
	 * 
	 * @param repository
	 * @return List of components
	 */
	protected List<ComponentImportData> importRepository(MRepository repository) {
		List<ComponentImportData> result = new ArrayList<ComponentImportData>();
		Map<MComponent, Resource> resourceMap = new HashMap<MComponent, Resource>();
		for (MComponent component : repository.getComponent()) {

			URI uri = null;
			if (existInRepository(component)) {
				uri = uriService.createComponentLocationURI(component);
				platformComponents.add(component);
			} else {
				uri = addURIMapping(component);
			}
			Resource resource = resourceSet.createResource(uri);
			resourceMap.put(component, resource);
		}
		List<MComponent> copies = ExportImportUtil.copyEObjects((List<MComponent>) repository.getComponent(),
				resourceMap);
		for (MComponent component : copies) {
			result.add(new ComponentImportData(component));
		}
		resourceSet.getResources().remove(resource);
		return result;
	}

	protected URI getPlatformComponentURI(MComponent component) {
		URI componentLocationURI = uriService.createComponentLocationURI(component);
		return resourceSet.getURIConverter().getURIMap().get(componentLocationURI);
	}

	protected boolean existInRepository(MComponent component) {
		URI resolvedUri = getPlatformComponentURI(component);
		return resolvedUri != null && resolvedUri.isPlatformPlugin();
	}

	protected URI addURIMapping(MComponent component) {
		return uriService.addUriMapping(uriService.createComponentURI(component),
				uriService.createPlatformResource(component), resourceSet.getURIConverter().getURIMap());
	}

	public List<ComponentImportData> importEcore() {
		if (!isEcoreImport()) {
			return Collections.emptyList();
		}
		URI ecoreURI = normalize(resourceSet, uri);
		Resource ecore = resource != null ? resource : resourceSet.getResource(ecoreURI, true);

		Map<Resource, MComponent> transformed = transform(ecore, null);

		List<ComponentImportData> result = new ArrayList<ComponentImportData>();

		for (MComponent component : filterExistingComponents(transformed)) {
			result.add(new ComponentImportData(component));
		}

		return result;
	}

	protected List<MComponent> filterExistingComponents(Map<Resource, MComponent> transformed) {
		Set<MComponent> components = new HashSet<MComponent>(transformed.values());

		List<MComponent> result = new ArrayList<MComponent>();
		for (MComponent m : components) {
			// existing mcores is loaded from plugin.xml and has scheme == "platform"
			if ("mcore".equals(m.eResource().getURI().scheme())) {
				result.add(m);
			}
		}
		return result;
	}

	public Map<String, EditorConfig> getMPackageUUIDToEditorConfigsMap() {
		if (associations != null) {
			return associations;
		}

		associations = new HashMap<String, EditorConfig>();
		ExportMCoreResourceFactory exportFactory = new ExportMCoreResourceFactory();
		Object oldMCoreFactory = exportFactory.registry(exportFactory);
		try {
			Resource resource = resourceSet.createResource(uri);
			if (!resource.isLoaded()) {
				try {
					resource.load(null);
				} catch (Exception e) {
					McoreUIPlugin.log(e);
				}
			}
			ExtensionType extension = null;
			for (EObject element : resource.getContents()) {
				if (element instanceof ExtensionType) {
					extension = (ExtensionType) element;
					break;
				}
			}

			if (extension != null) {
				for (EditorConfigModelAssociationType association : extension.getEditorConfigModelAssociation()) {
					String configName = association.getConfig();
					EditorConfig editorConfig = findEditorConfig(resource.getContents(), configName);
					if (editorConfig != null) {
						associations.put(association.getUri(), editorConfig);
					}
				}
			}
			return associations;
		} finally {
			exportFactory.registry(oldMCoreFactory);
		}
	}

	/**
	 * Read and cache configs from an import file
	 * 
	 * @return
	 */
	public List<MEditorConfig> getMEditrConfigs() {
		if (mEditorConfigs != null) {
			return mEditorConfigs;
		}

		mEditorConfigs = new ArrayList<MEditorConfig>();

		ExportMCoreResourceFactory exportFactory = new ExportMCoreResourceFactory();
		Object oldMCoreFactory = exportFactory.registry(exportFactory);
		Resource resource = null;
		try {
			resource = resourceSet.createResource(uri);
			if (!resource.isLoaded()) {
				try {
					resource.load(null);
				} catch (Exception e) {
					McoreUIPlugin.log(e);
				}
			}
		} finally {
			exportFactory.registry(oldMCoreFactory);
		}

		for (EObject eRootObject : resource.getContents()) {
			if (eRootObject instanceof MEditorConfig) {
				MEditorConfig mEditorConfig = (MEditorConfig) eRootObject;
				String id = EcoreUtil.getURI(mEditorConfig).fragment();
				for (ComponentImportData importData : getImportData()) {
					MNamedEditor mainNamedEditor = importData.getImportedComponent().getMainNamedEditor();
					if (mainNamedEditor == null) {
						continue;
					}
					String currentMTE_ID = EcoreUtil.getURI(mainNamedEditor.getEditor()).fragment();
					if (id.equals(currentMTE_ID)) {
						URI mcoreURI = mainNamedEditor.eResource().getURI();
						mcoreURI = normalize(resourceSet, mcoreURI);
						Resource mteResource = resourceSet
								.createResource(mcoreURI.trimFileExtension().appendFileExtension("mtableeditor"));
						Map<MEditorConfig, Resource> tableToResource = new HashMap<MEditorConfig, Resource>(1);
						tableToResource.put(mEditorConfig, mteResource);
						mEditorConfig = ExportImportUtil
								.copyEObjects(Arrays.asList(new MEditorConfig[] { mEditorConfig }), tableToResource)
								.get(0);
						mteResource.getContents().add(mEditorConfig);
						mainNamedEditor.setEditor(mEditorConfig);
						mEditorConfigs.add(mEditorConfig);
					}
				}
			}
		}

		for (MEditorConfig mEditorConfig : mEditorConfigs) {
			mainLoop: for (Iterator<EObject> it = mEditorConfig.eResource().getAllContents(); it.hasNext();) {
				EObject next = it.next();
				if (!next.eIsProxy()) {
					continue;
				}
				String unresolvedID = EcoreUtil.getURI(next).fragment();
				for (MComponent component : getComponents()) {
					for (Iterator<EObject> componentIterator = component.eResource().getAllContents(); componentIterator
							.hasNext();) {
						EObject forResolve = componentIterator.next();
						if (EcoreUtil.getURI(forResolve).fragment().equals(unresolvedID)) {
							EcoreUtil.resolve(next, forResolve);
							continue mainLoop;
						}
					}
				}
			}
		}
		return mEditorConfigs;
	}

	public List<Diagram> getDiagrams() {
		if (diagrams != null) {
			return diagrams;
		}

		HashMap<String, Diagram> uuidToDiagram = new HashMap<String, Diagram>();
		ExportMCoreResourceFactory exportFactory = new ExportMCoreResourceFactory();
		Object oldMCoreFactory = exportFactory.registry(exportFactory);
		try {
			Resource resource = resourceSet.createResource(uri);
			if (!resource.isLoaded()) {
				try {
					resource.load(null);
				} catch (Exception e) {
					McoreUIPlugin.log(e);
				}
			}
			for (EObject element : resource.getContents()) {
				if (element instanceof Diagram) {
					uuidToDiagram.put(element.eResource().getURIFragment(element), (Diagram) element);
				}
			}
			diagrams = new ArrayList<Diagram>(uuidToDiagram.values());
		} finally {
			exportFactory.registry(oldMCoreFactory);
		}
		return diagrams;
	}

	private void resolveMcoreURIsInDiagrams(List<Diagram> diagrams, List<MComponent> components) {
		for (Diagram diagram : diagrams) {
			resolveMcoreURIs(diagram, components, ElementResolverHelper.mDiagramResolverHelpers);
		}
	}

	private void resolveMcoreURIs(EObject root, List<MComponent> components, ElementResolverHelper[] heplers) {
		for (Iterator<EObject> it = root.eResource().getAllContents(); it.hasNext();) {
			EObject next = it.next();
			for (ElementResolverHelper hepler : heplers) {
				// find helper
				if (!hepler.hasMCoreRelationship(next)) {
					continue;
				}
				// helper was found
				// try to resolve
				String unresolvedID = hepler.getMcoreElementID(next);
				EObject toResolve = null;
				for (MComponent component : components) {
					for (Iterator<EObject> componentIterator = component.eResource().getAllContents(); componentIterator
							.hasNext();) {
						EObject elementToResolve = componentIterator.next();
						if (EcoreUtil.getURI(elementToResolve).fragment().equals(unresolvedID)) {
							// resolved
							toResolve = elementToResolve;
							break;
						}
					}
				}
				hepler.setNewMcoreElement(next, toResolve);
			}
		}
	}

	protected EditorConfig findEditorConfig(List<EObject> elements, String uuid) {
		for (EObject element : elements) {
			if (element instanceof EditorConfig && uuid.equals(element.eResource().getURIFragment(element))) {
				return (EditorConfig) element;
			}
		}
		return null;
	}

	public List<ComponentImportData> getImportData() {
		if (components == null) {
			if (isMCoreImport()) {
				components = importMcore();
			} else if (isEcoreImport()) {
				components = importEcore();
			} else {
				components = Collections.emptyList();
			}
		}
		return components;
	}

	public List<MComponent> getComponents() {
		ArrayList<MComponent> result = new ArrayList<MComponent>();
		for (ComponentImportData importData : getImportData()) {
			result.add(importData.getImportedComponent());
		}
		return result;
	}

	public boolean isMCoreImport() {
		return isMCoreURI(uri);
	}

	public boolean isEcoreImport() {
		return testExtension(uri, "ecore");
	}

	public boolean isMCoreURI(URI uri) {
		return testExtension(uri, "mcore");
	}

	public boolean testExtension(URI uri, String ext) {
		if (uri == null) {
			return false;
		}
		String fileExtension = uri.fileExtension();
		return fileExtension != null && fileExtension.equalsIgnoreCase(ext);
	}

	public void doImport(IProgressMonitor monitor) throws InvocationTargetException {

		final String ignoreComponent = "semantics";
		final List<MComponent> sorted = getComponents();

		getMEditrConfigs();

		monitor.beginTask("Start Import", (sorted.size() * 2) + 1);

		for (MComponent component : sorted) {
			if (component.getName().equals(ignoreComponent))
				break;
			monitor.setTaskName("Import " + component.getCalculatedName());
			// remove legacy
			component.getGeneratedEPackage().clear();

			// save editors condition
			String domainID = PathUtils.buildDomainID(normalizeResourceURI(resourceSet, component));
			McoreDiagramEditingDoamin domain = McoreEditingDomainFactory.getInstance().createEditingDomain(domainID,
					this);
			domain.publish(McoreTransactionEventType.SAVE, this);
			McoreEditingDomainFactory.getInstance().dispose(domain, this);
			//
			// import component and initialize it
			//
			try {
				CreateProject.create(component, SubMonitor.convert(monitor, 1), null);
			} catch (CoreException e) {
				throw new InvocationTargetException(e);
			}
		}

		GenerateEcore.transform(sorted, SubMonitor.convert(monitor, 1));

		for (MComponent component : sorted) {
			if (component.getName().equals(ignoreComponent))
				break;
			// generate configs
			GenerateEditorConfig.generate(component, SubMonitor.convert(monitor, 1), this);
			// generate xml, json resources
			GenerateInstanceResources.generate(component, monitor);
		}

		// saveDiagrams
		Map<String, Object> saveOptions = getSaveOptions();
		for (Diagram d : getDiagrams()) {
			Resource r = importDiagram(d, sorted);
			try {
				r.save(saveOptions);
			} catch (IOException e) {
				McoreUIPlugin.log(e);
			}
		}

		// save MTE configs
		for (MComponent component : sorted) {
			if (component.getName().equals(ignoreComponent))
				break;

			if (component.getNamedEditor().isEmpty()) {
				MEditorConfig mEditorConfig = GenerateTableEditor.generateImport(component,
						SubMonitor.convert(monitor, 1));

				if (!getComponents().isEmpty() && getComponents().get(0) != null) {
					MComponent comp = getComponents().get(0);

					if (comp.getRootPackage() != null && comp.getRootPackage().getResourceRootClass() != null) {
						mEditorConfig.doActionUpdate(MEditorConfigAction.RESET_EDITOR);
					}
				}

				MNamedEditor namedEditor = McoreFactory.eINSTANCE.createMNamedEditor();
				namedEditor.setName("Main");
				namedEditor.setLabel("Main");
				namedEditor.setUserVisible(true);
				namedEditor.setEditor(mEditorConfig);
				component.getNamedEditor().add(namedEditor);
				component.setMainNamedEditor(namedEditor);

				try {
					component.eResource().save(saveOptions);
				} catch (IOException e) {
					McoreUIPlugin.log(e);
				}
			} else {
				try {
					MEditor mainNamedEditor = component.getMainNamedEditor() != null
							? component.getMainNamedEditor().getEditor()
							: null;
					if (false == mainNamedEditor instanceof MEditorConfig) {
						continue;
					}
					resolveMcoreURIs(mainNamedEditor, sorted, ElementResolverHelper.mEditorCongifResolverHelpers);
					mainNamedEditor.eResource().save(saveOptions);

				} catch (IOException e) {
					McoreUIPlugin.log(e);
				}
			}
		}

		monitor.done();

		// TODO: Open most abstract
		if (sorted.size() > 0) {
			int openEditorIndex = 0;
			McoreUIPlugin.openEditor(sorted.get(openEditorIndex));
		}
	}

	/**
	 * Copy diagram into new resource and return it
	 * 
	 * @param diagram    to import
	 * @param components is imported components
	 * @return new Resource with copied diagram
	 */
	private Resource importDiagram(final Diagram diagram, List<MComponent> components) {
		if (components.isEmpty()) {
			return null;
		}

		Resource r = resourceSet.createResource(URI.createURI("diagram.mcore_diagram"));
		HashMap<Diagram, Resource> diagramToResourceMap = new HashMap<Diagram, Resource>();
		diagramToResourceMap.put(diagram, r);
		List<Diagram> diagrams = new ArrayList<Diagram>(1);
		diagrams.add(diagram);

		List<Diagram> copiedDiagrams = ExportImportUtil.copyEObjects(diagrams, diagramToResourceMap);
		if (copiedDiagrams.isEmpty()) {
			return null;
		}

		resolveMcoreURIsInDiagrams(copiedDiagrams, components);

		Diagram copiedDiagram = copiedDiagrams.get(0);
		EObject mPackage = copiedDiagram.getElement();
		if (mPackage == null || mPackage.eIsProxy()) {
			return null;
		}
		URI componentURI = normalizeResourceURI(resourceSet, mPackage);
		URI diagramURI = componentURI.trimSegments(1).appendSegment(diagram.getName());
		r.setURI(diagramURI);
		return r;
	}

	public URI getUri() {
		return uri;
	}

	protected MComponent findMComponentInPlugins(EPackage ePackage) {
		if (ePackage.eResource() == null || ePackage.eResource().getURI() == null) {
			return null;
		}
		URI ecoreUri = ePackage.eResource().getURI();

		// mcore generate files to platform:/resource/<project name>/model/.
		// @see PluginExtensions#addComponent
		String baseFolderPath = ecoreUri.trimSegments(ecoreUri.segmentCount() - 2).toString();
		URI pluginURI = URI.createURI(baseFolderPath + "/plugin.xml");

		Resource res = new PluginResourceFactoryImpl().createResource(pluginURI);
		try {
			res.load(null);
		} catch (Exception e) {
			McoreUIPlugin.log(e);
		}
		if (res != null && !res.getContents().isEmpty()) {
			PluginType pluginType = PluginExtensions.getOrCreatePluginType(res);
			List<ExtensionType> extensions = pluginType.findExtensions(PluginExtensions.COMPONENT_EXT_POINT_ID);

			if (!extensions.isEmpty()) {
				for (ExtensionType type : extensions) {
					Map<String, ?> options = getMcoreOptions(resourceSet);
					for (ComponentType componentType : type.getComponent()) {
						URI mcoreURI = URI.createURI(baseFolderPath + "/" + componentType.getModel());
						Resource mcoreResource = new McoreResourceFactoryImpl().createResource(mcoreURI);
						try {
							mcoreResource.load(options);
						} catch (IOException e) {
							McoreUIPlugin.log(e);
						}
						if (mcoreResource.getContents().isEmpty()) {
							continue;
						}
						MComponent component = (MComponent) mcoreResource.getContents().get(0);
						for (MPackage mPackage : component.getOwnedPackage()) {
							if (compareIgnoreCaseAndSpace(ePackage.getName(), ePackage.getName())
									&& compareIgnoreCaseAndSpace(mPackage.getDerivedNsURI(), ePackage.getNsURI())
									&& compareIgnoreCaseAndSpace(mPackage.getDerivedNsPrefix(),
											ePackage.getNsPrefix())) {
								return component;
							}
						}
					}
				}
			}
		}
		return null;
	}

	private boolean compareIgnoreCaseAndSpace(String s1, String s2) {
		return replaceSpacesToEmptyString(s1).equalsIgnoreCase(replaceSpacesToEmptyString(s2));
	}

	private String replaceSpacesToEmptyString(String s2) {
		return s2.replace(" ", "");
	}

	protected EClassifier resolveReference(EClassifier eObject, URI baseURI) {
		if (eObject == null) {
			return null;
		}
		Resource resource = eObject.eResource();
		if (resource != null && !resource.getContents().isEmpty()) {
			return eObject;
		}

		// URI resourceURI = EcoreUtil.getURI(eObject).trimFragment();
		// if (resourceURI == null) {
		// return null;
		// }
		// resourceURI = resourceURI.resolve(baseURI);
		//
		// Resource r = resourceSet.getResource(resourceURI, true);
		// if (r == null || r.getContents().isEmpty()) {
		// return null;
		// }
		// EClassifier resolvedEType = (EClassifier)
		// r.getEObject(EcoreUtil.getURI(eObject).fragment());
		// return resolvedEType;
		return null;
	}

	private Map<Resource, MComponent> transform(Resource ecore, Map<Resource, MComponent> ecoreToMcore) {
		if (ecoreToMcore == null) {
			ecoreToMcore = new HashMap<Resource, MComponent>();
		}

		// transform
		EcoreUtil.resolveAll(ecore);
		Ecore2Mcore ecore2Mcore = new Ecore2Mcore();
		MComponent component = ecore2Mcore.transform(ecore);

		updateComponentName(ecoreToMcore.values(), component);

		createMcoreResource(component);

		ecoreToMcore.put(ecore, component);

		Map<MProperty, EClassifier> propertyToResolve = collectPropertiesToResolve(component, ecore.getURI());

		for (MProperty p : propertyToResolve.keySet()) {
			if (SimpleType.NONE != p.getSimpleType()) {
				continue;
			}
			EClassifier internalEClassifier = propertyToResolve.get(p);
			Resource eResource = internalEClassifier.eResource();
			MComponent componentToResolve = ecoreToMcore.get(eResource);

			if (componentToResolve == null) {
				// classifier is not resolved yet
				EPackage ePackage = (EPackage) eResource.getContents().get(0);
				componentToResolve = findMComponentInPlugins(ePackage);
				if (componentToResolve == null) {
					componentToResolve = transform(eResource, ecoreToMcore).get(eResource);
				} else {
					ecoreToMcore.put(eResource, componentToResolve);
				}
			}
			if (componentToResolve == null) {
				// cannot resolve classifier. it should be resolved by user
				p.getContainingClassifier().getContainingPackage().getClassifier().add(p.getType());
				continue;
			}

			URI internalEClassifierURI = getEObjectURI(internalEClassifier);
			for (Iterator<EObject> it = componentToResolve.eAllContents(); it.hasNext();) {
				EObject next = it.next();
				if (next instanceof MClassifier) {
					MClassifier classifier = (MClassifier) next;
					if (internalEClassifierURI.equals(getEObjectURI(classifier.getInternalEClassifier()))) {
						p.setType(classifier);
						break;
					}
				}
			}
		}

		// Creates visible workspace and resource for created mcore
		MResourceFolder workspace = ObjectsFactory.eINSTANCE.createMResourceFolder();
		workspace.setName("workspace");
		MResourceFolder project = ObjectsFactory.eINSTANCE.createMResourceFolder();
		project.setName("test");
		component.getResourceFolder().add(workspace);
		workspace.getFolder().add(project);
		MResource mRec = ObjectsFactory.eINSTANCE.createMResource();
		project.getResource().add(mRec);
		mRec.setName("My1");

		// Sets the boolean so the editorconfig gets created
		component.setAvoidRegenerationOfEditorConfiguration(true);
		component.setUseLegacyEditorconfig(false);

		return ecoreToMcore;
	}

	private Map<MProperty, EClassifier> collectPropertiesToResolve(MComponent component, URI base) {
		Map<MProperty, EClassifier> result = new HashMap<MProperty, EClassifier>();

		for (Iterator<EObject> it = component.eAllContents(); it.hasNext();) {
			EObject next = it.next();
			if (next instanceof MProperty) {

				MProperty mProperty = (MProperty) next;
				if (mProperty.getIsReference()) {
					EStructuralFeature feature = mProperty.getInternalEStructuralFeature();
					EReference ref = (EReference) feature;
					EClassifier classifier = resolveReference(ref.getEType(), base);
					if (classifier != null && ref.eResource() != classifier.eResource()) {
						result.put(mProperty, classifier);
					}
				}
				if (mProperty.getIsOperation()) {
					EOperation op = mProperty.getOperationSignature().get(0).getInternalEOperation();
					EClassifier classifier = resolveReference(op.getEType(), base);
					if (classifier != null && op.eResource() != classifier.eResource()) {
						result.put(mProperty, classifier);
					}
				}
			}
		}
		return result;
	}

	private URI getEObjectURI(EObject eObject) {
		if (eObject == null) {
			return null;
		}
		return EcoreUtil.getURI(eObject);
	}

	private Resource createMcoreResource(MComponent component) {
		URI mcoreUri = addURIMapping(component);
		Resource mcoreResource = resourceSet.createResource(mcoreUri);
		mcoreResource.getContents().add(component);
		return mcoreResource;
	}

	private void updateComponentName(Collection<MComponent> existComponents, MComponent component) {
		String suffix = "0";
		boolean checkName = true;
		while (checkName) {
			checkName = false;
			for (MComponent existComponent : existComponents) {
				if (RenameService.isEquals(existComponent, component)) {
					// remove suffix
					String name = component.getName();
					String shortName = component.getShortName();
					if (shortName != null) {
						shortName = removeSuffix(suffix, shortName);
					}
					name = removeSuffix(suffix, name);

					// update usffix
					suffix = Integer.toString(Integer.parseInt(suffix) + 1);

					// append suffix
					if (shortName != null) {
						component.setShortName(shortName + suffix);
					}
					component.setName(name + suffix);
					checkName = true;
					break;
				}
			}
		}
	}

	public void addMergedComponent(MComponent component) {
		ComponentImportData importData = getComponentImportData(component);
		if (importData != null) {
			importData.setShouldMerge(true);
		}
	}

	public void removeMergedComponent(MComponent component) {
		ComponentImportData importData = getComponentImportData(component);
		if (importData != null) {
			importData.setShouldMerge(false);
		}
	}

	public boolean shouldComponentMerged(MComponent component) {
		ComponentImportData importData = getComponentImportData(component);
		if (importData != null) {
			return importData.shouldMerge();
		}
		return false;
	}

	public boolean canBeMerged(MComponent component) {
		ComponentImportData importData = getComponentImportData(component);
		if (importData != null) {
			return importData.canBeMerged();
		}
		return false;
	}

	public boolean canOverwriteComponents() {
		boolean canOverwrite = false;
		for (ComponentImportData importData : getImportData()) {
			if (importData.canBeMerged()) {
				if (importData.getComponentToMerge() != null) {
					canOverwrite = true;
				}
			} else {
				if (importData.getComponentToMerge() != null) {
					return false;
				}
			}
		}
		return canOverwrite;
	}

	protected ComponentImportData getComponentImportData(MComponent component) {
		if (components == null) {
			return null;
		}
		for (ComponentImportData importData : components) {
			if (importData.getImportedComponent().equals(component)) {
				return importData;
			}
		}
		return null;
	}

	public void importPlatformComponents() {
		getComponents().addAll(platformComponents);
	}

	public void updateComponentURI(MComponent component) {
		ComponentImportData importData = getComponentImportData(component);
		if (importData != null) {
			URI uri = addURIMapping(component);
			importData.setURI(uri);
		}
	}

	private String removeSuffix(String suffix, String name) {
		int suffixIndex = name.lastIndexOf(suffix);
		return suffixIndex == -1 ? name : name.substring(0, name.lastIndexOf(suffix));
	}
}
