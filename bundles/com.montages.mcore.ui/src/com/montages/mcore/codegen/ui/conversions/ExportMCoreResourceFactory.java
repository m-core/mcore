package com.montages.mcore.codegen.ui.conversions;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import com.montages.mcore.util.McoreResourceFactoryImpl;
import com.montages.mcore.util.ResourceService;


class ExportMCoreResourceFactory extends McoreResourceFactoryImpl {

	protected Map<String, Object> getExtensoinToFactoryMap() {
		return Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
	}

	@Override
	public Resource createResource(URI uri) {
		return ResourceService.createXMLResourceWithUUID(uri);
	}

	public Object registry(Object factory) {
		return getExtensoinToFactoryMap().put("mcore", factory);
	}
}
