package com.montages.mcore.ui;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.eclipse.ui.wizards.IWizardDescriptor;
import org.eclipse.ui.wizards.IWizardRegistry;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.montages.mcore.MComponent;
import com.montages.mcore.McoreFactory;
import com.montages.transactions.McoreEditingDomainFactory;

public class McoreUIPlugin extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "com.montages.mcore.ui";

	private static final String PROPERTIES = "plugin.properties";

	private static McoreUIPlugin INSTANCE;

	public static final Image MCOMPONENT_IMAGE;
	
	private ScopedPreferenceStore myPreferenceStore;

	static {
		ComposedAdapterFactory adapterFactory = McoreEditingDomainFactory.getInstance().createComposedAdapterFactory();
		MComponent component = McoreFactory.eINSTANCE.createMComponent();
		MCOMPONENT_IMAGE = new AdapterFactoryLabelProvider(adapterFactory).getImage(component);
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		INSTANCE = this;
	}

	public static McoreUIPlugin getInstance() {
		return INSTANCE;
	}

	public static void log(Throwable t) {
		if (getInstance() != null) {
			getInstance().getLog().log(new Status(Status.ERROR, PLUGIN_ID, t.getLocalizedMessage()));
		}
	}

	public static ImageDescriptor getImage(String id) {
		return getInstance().getImageRegistry().getDescriptor(id);
	}

	public static void openWizard(String id, IWizardRegistry registry) {
		openWizard(id, registry, null);
	}

	public static void openWizard(String id, IWizardRegistry registry, IStructuredSelection selection) {
		IWizardDescriptor descriptor = registry.findWizard(id);

		if (descriptor != null) {
			IWizard wizard = null;
			try {
				wizard = descriptor.createWizard();
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}

			if (wizard != null) {
				WizardDialog wd = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), wizard);
				wd.setTitle(wizard.getWindowTitle());
				if (wizard instanceof IWorkbenchWizard) {
					((IWorkbenchWizard) wizard).init(PlatformUI.getWorkbench(), selection);
				}
				wd.open();
			}
		}
	}

	public static Display getDialogDisplay() {
		return PlatformUI.getWorkbench().getModalDialogShellProvider().getShell().getDisplay();
	}

	public static boolean showConfirmDialof(String title, String message) {
		return MessageDialog.openConfirm(getDialogDisplay().getActiveShell(), title, message);
	}

	public static void openEditor(final MComponent component) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				final IWorkbenchPage page = workbenchWindow != null ? workbenchWindow.getActivePage() : null;

				if (page != null) {
					try {
						URI uri = component.eResource().getURI();
						if (!uri.isPlatform()) {
							uri = component.eResource().getResourceSet().getURIConverter().normalize(uri);
						}
						page.openEditor(new URIEditorInput(uri), "com.montages.mcore.presentation.McoreEditorID");
					} catch (PartInitException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		Bundle bundle = Platform.getBundle(PLUGIN_ID);

		Path path = new Path("icons/add.png");
		URL url = FileLocator.find(bundle, path, null);
		ImageDescriptor desc = ImageDescriptor.createFromURL(url);
		reg.put("image_add", desc);

		path = new Path("icons/add_diagram.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_diagram_add", desc);

		path = new Path("icons/delete.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_delete", desc);

		path = new Path("icons/bricks.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_gen", desc);

		path = new Path("icons/run_16x16.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_run", desc);

		path = new Path("icons/bin.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_clean", desc);

		path = new Path("icons/import_wiz.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_import", desc);

		path = new Path("icons/export_wiz.png");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_export", desc);

		path = new Path("icons/lde32x32.gif");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_lde", desc);

		path = new Path("icons/NewEcore.gif");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_ecore", desc);

		path = new Path("icons/TableConfig.gif");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_table", desc);

		path = new Path("icons/McoreDiagramFile.gif");
		url = FileLocator.find(bundle, path, null);
		desc = ImageDescriptor.createFromURL(url);
		reg.put("image_diagram", desc);
	}

	public Properties getProperties() {
		Properties properties = new Properties();
		InputStream fileStream = null;
		try {
			URL url = new URL(URI.createPlatformPluginURI(PLUGIN_ID + "/" + PROPERTIES, false).toString());
			fileStream = url.openConnection().getInputStream();
			properties.load(fileStream);
		} catch (Exception e) {
			log(e);
		} finally {
			if (fileStream != null) {
				try {
					fileStream.close();
				} catch (IOException e) {
					log(e);
				}
			}
		}
		return properties;
	}

	public void storePreference(String key, String value) {
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(PLUGIN_ID);
		preferences.put(key, value);
	}

	public String getPreference(String key) {
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(PLUGIN_ID);
		return preferences.get(key, "");
	}

	public String getMCoreTempFolderPath() {
		IWorkspaceRoot wsRoot = ResourcesPlugin.getWorkspace().getRoot();
		String rootWSPath = wsRoot.getLocation().toString();
		String mCoreTempDirName = getProperties().getProperty("tempdir", ".mcore-export");
		return rootWSPath + mCoreTempDirName;
	}
	
	public IPreferenceStore getPreferenceStore() {
        // Create the preference store lazily.
        if (myPreferenceStore == null) {
			myPreferenceStore = new ScopedPreferenceStore(InstanceScope.INSTANCE, McoreUIPlugin.getInstance().getBundle().getSymbolicName());
        }
        return myPreferenceStore;
    }

	public static void waitAsyncRun(final Runnable r) {
		Display display = Display.getDefault();
		if (display.getThread() == Thread.currentThread()) {
			r.run();
			return;
		}

		final Object lock = new Object();
		final boolean[] ready = new boolean[]{false};
		synchronized(lock) {
			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {
					synchronized (lock) {
						try {
							r.run();
						} catch (Throwable t) {
							log(t);
						} finally {
							ready[0] = true;
							lock.notifyAll();
						}
					}
				}
			});
			while (!ready[0]) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					log(e);
				}
			}
		}
	}
	
}
