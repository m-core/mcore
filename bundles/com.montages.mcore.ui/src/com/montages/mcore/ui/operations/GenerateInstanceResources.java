package com.montages.mcore.ui.operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceStatus;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.emfjson.jackson.resource.JsonResourceFactory;

import com.montages.mcore.MComponent;
import com.montages.mcore.mcore2ecore.MResource2Resource;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.workspace.FileFunctions;

public class GenerateInstanceResources {

	private static final String META_DATA_FOLDER_NAME = ".metadata";

	private static final String PROJECT_FILE_NAME = ".project";

	public static void generate(MComponent component, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final List<Resource> result = new ArrayList<Resource>();
		final IProject project = root.getProject(component.getDerivedBundleName());
		final Transformer tr = new Transformer(monitor);

		if (project != null && project.isAccessible()) {
			for (MResourceFolder folder : component.getResourceFolder()) {
				clearFolder(project.getFolder(new Path(folder.getEName())), monitor);
				tr.transform(project, folder, result);
			}
		}
		tr.finish();

		final Map<String, Object> options = new HashMap<String, Object>();
		options.put(XMLResource.OPTION_ENCODING, "UTF-8");
		result.stream()
			.filter(resource -> !resource.getContents().isEmpty())
			.forEach(resource -> saveResource(resource, options));
	}

	private static void clearFolder(IFolder container, IProgressMonitor monitor) {
		if (!container.exists()) {
			return;
		}
		List<IFolder> subFolders = FileFunctions.collectFolders(container, Arrays.asList(META_DATA_FOLDER_NAME));
		for (IFolder nextFolder : subFolders) {
			for (IFile nextFile : FileFunctions.collectFilesExcludeFilter(nextFolder, Arrays.asList(PROJECT_FILE_NAME))) {
				try {
					nextFile.delete(true, monitor);
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
			clearFolder(nextFolder, monitor);
			try {
				if (nextFolder.members().length == 0) {
					nextFolder.delete(true, monitor);
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

	private static IFolder getFolder(IContainer container, MResourceFolder folder, IProgressMonitor monitor) {
		return container.getFolder(new Path(folder.getEName()));
	}

	private static IFolder getFolderOrCreate(IContainer container, MResourceFolder folder, IProgressMonitor monitor) {
		IFolder realFolder = getFolder(container, folder, monitor);
		if (!realFolder.exists()) {
			try {
				realFolder.create(true, false, monitor);
			} catch (CoreException e) {
				if (e.getStatus().getCode() == IResourceStatus.CASE_VARIANT_EXISTS) {
					IPath path = ((IResourceStatus) e.getStatus()).getPath();
					realFolder = container.getFolder(path);
				} else {
					e.printStackTrace();
				}
			}
		}
		return realFolder;
	}
	
	public static void saveResource(Resource resource, Map<?, ?> options) {
		try {
			resource.save(options);
		} catch (IOException e) {
			McoreUIPlugin.log(e);
		}
	}
	
	public static Resource createJsonResource(IPath resourceFilePath, EObject mResource) {
		URI resourceURI = URI.createPlatformResourceURI(resourceFilePath.toString() + ".json", false);
			
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry()
						.getExtensionToFactoryMap()
						.put("json", new JsonResourceFactory());
		Resource resource = resourceSet.createResource(resourceURI);
		resource.getContents().add(mResource);
		return resource;
	}
	
	public static Resource createXmlResource(URI resourceURI) {
		XMLResourceImpl resource = new XMLResourceImpl(resourceURI) {
			@Override
			protected boolean useUUIDs() {
				return true;
			}
		};
		XMLResource.XMLMap xmlMap = new XMLMapImpl();
		xmlMap.setIDAttributeName("_uuid");
		resource.getDefaultSaveOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		resource.getDefaultLoadOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		return resource;
	}
	
	private static String getResourceFileNameFromMResource(MResource resource) {
		return resource.getEName();
	}

	private static class Transformer {

		private final MResource2Resource t;

		private final IProgressMonitor monitor;

		public Transformer(IProgressMonitor monitor) {
			this.monitor = monitor;
			this.t = new MResource2Resource();
		}

		private void finish() {
			t.finish();
		}

		private void transform(IContainer container, MResourceFolder folder, List<Resource> result) {
			IFolder realFolder = getFolderOrCreate(container, folder, monitor);

			if (realFolder.isAccessible()) {
				// Process MResource
				for (MResource mResource : folder.getResource()) {
					Optional.ofNullable(getResourceFileNameFromMResource(mResource)).ifPresent(resourceFileName -> {
						IPath resourcePath = realFolder.getFullPath().append(resourceFileName);
						
						Resource xmlResource = createResource(resourcePath, mResource, result);
						transformOne(mResource, xmlResource);
						xmlResource.getContents().stream().findFirst()
							.map(eObject -> createJsonResource(resourcePath, EcoreUtil.copy(eObject)))
							.ifPresent(result::add);
					});
				}
				// Continue on sub folders.
				for (MResourceFolder subFolder : folder.getFolder()) {
					transform(realFolder, subFolder, result);
				}
			}
		}

		private void transformOne(MResource mResource, Resource resource) {
			if (resource != null) {
				t.transform(mResource, resource);
			}
		}

		private static Resource createResource(IPath resourceFilePath, MResource mResource, List<Resource> resources) {
			if (mResource.getObject().isEmpty())
				return null;

			Resource resource = null;
			URI resourceURI = URI.createPlatformResourceURI(resourceFilePath.toString(), false);
			resources.add(resource = GenerateInstanceResources.createXmlResource(resourceURI));
			return resource;
		}
	}

}
