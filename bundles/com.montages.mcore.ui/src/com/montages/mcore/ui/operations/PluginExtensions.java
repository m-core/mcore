package com.montages.mcore.ui.operations;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.langlets.plugin.ComponentType;
import org.langlets.plugin.DocumentRoot;
import org.langlets.plugin.EditorConfigModelAssociationType;
import org.langlets.plugin.ExtensionType;
import org.langlets.plugin.PluginFactory;
import org.langlets.plugin.PluginType;
import org.langlets.plugin.util.PluginResourceFactoryImpl;
import org.osgi.framework.Bundle;
import org.xocl.editorconfig.EditorConfig;

import com.montages.mcore.MComponent;

public class PluginExtensions {

	public static final String EDITOR_CONFIG_EXT_POINT_ID = "org.xocl.editorconfig.editorConfigAssociations";

	public static final String COMPONENT_EXT_POINT_ID = "com.montages.component";

	public static void addComponent(MComponent component) {
		PluginType pluginType = getOrCreatePluginType(component.getDerivedBundleName());
		List<ExtensionType> extensions = pluginType.findExtensions(COMPONENT_EXT_POINT_ID);

		if (extensions.isEmpty()) {
			ComponentType componentType = PluginFactory.eINSTANCE.createComponentType();
			componentType.setModel("model/" + component.getEName().toLowerCase() + ".mcore");
			componentType.setUri(component.getDerivedURI());
			pluginType.addExtension(COMPONENT_EXT_POINT_ID, componentType);
		}

		save(pluginType);
	}

	public static void addEditorConfiguration(MComponent component, EPackage ePackage, EditorConfig config) {
		PluginType pluginType = getOrCreatePluginType(component.getDerivedBundleName());
		ExtensionType extensionType = getExtensionPoint(pluginType, EDITOR_CONFIG_EXT_POINT_ID);

		String editorConfigPath = "model/" + config.eResource().getURI().lastSegment();
		createExtension(ePackage, pluginType, extensionType, editorConfigPath);

		save(pluginType);
	}

	protected static List<EditorConfigModelAssociationType> getEditorConfigAssociations(Resource resource) {
		if (resource == null || resource.getContents().isEmpty()) {
			return Collections.emptyList();
		}

		DocumentRoot root = (DocumentRoot) resource.getContents().get(0);
		
		if (root.getPlugin() == null) { 
			return Collections.emptyList();
		}

		ExtensionType extensionType = getExtensionPoint(root.getPlugin(), EDITOR_CONFIG_EXT_POINT_ID);
		return extensionType.getEditorConfigModelAssociation();
	}

	private static void save(PluginType pluginType) {
		final Map<Object, Object> options = new HashMap<Object, Object>();
		options.put(XMLResource.OPTION_EXTENDED_META_DATA, new BasicExtendedMetaData());
		options.put(XMLResource.OPTION_SUPPRESS_DOCUMENT_ROOT, Boolean.TRUE);
		options.put(XMLResource.OPTION_ENCODING, "UTF-8");

		try {
			pluginType.eResource().save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static ExtensionType getExtensionPoint(PluginType pluginType, String extensionID) {
		List<ExtensionType> extensions = pluginType.findExtensions(extensionID);
		final ExtensionType extension;

		if (extensions.isEmpty()) {
			extension = PluginFactory.eINSTANCE.createExtensionType();
			extension.setPoint(extensionID);
			pluginType.getExtension().add(extension);
		} else {
			extension = extensions.get(0);
		}

		return extension;
	}

	private static void createExtension(EPackage ePackage, PluginType pluginType, ExtensionType extension, String editorConfigPath) {
		createAssociation(ePackage, pluginType, extension, editorConfigPath);

		for (Iterator<EObject> iterator = ePackage.eAllContents(); iterator.hasNext();) {
			EObject eObject = iterator.next();
			if (eObject instanceof EPackage) {
				EPackage subPackage = (EPackage) eObject;
				createAssociation(subPackage, pluginType, extension, editorConfigPath);
			}
		}
	}

	private static void createAssociation(EPackage ePackage, PluginType pluginType, ExtensionType extension, String editorConfigPath) {
		EditorConfigModelAssociationType associationType = pluginType.findEditorConfigAssociation(ePackage.getNsURI());
		if (associationType == null) {
			associationType = PluginFactory.eINSTANCE.createEditorConfigModelAssociationType();
			extension.getEditorConfigModelAssociation().add(associationType);
		}

		associationType.setConfig(editorConfigPath);
		associationType.setUri(ePackage.getNsURI());
	}

	public static List<EditorConfigModelAssociationType> getResourceExtensionType(String bundleName) {
		final Resource resource = getResource(bundleName);
		return getEditorConfigAssociations(resource);
	}

	protected static Resource getResource(String bundleName) {
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(bundleName);
		if (!project.exists() || !project.isOpen()) {
			return null;
		}
		final IFile pluginFile = project.getFile("plugin.xml");
		final URI uri = URI.createPlatformResourceURI(pluginFile.getFullPath().toString(), true);
		final Resource resource = new PluginResourceFactoryImpl().createResource(uri);

		if (pluginFile.exists()) {
			try {
				resource.load(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resource;
	}

	public static List<EditorConfigModelAssociationType> getBundleExtensionType(String bundleName) {
		Bundle bundle = Platform.getBundle(bundleName);
		if (bundle == null) {
			return Collections.emptyList();
		}

		URL url = bundle.getResource("plugin.xml");
		final Resource resource = new PluginResourceFactoryImpl().createResource(null);

		try {
			resource.load(url.openStream(), null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return getEditorConfigAssociations(resource);
	}

	private static PluginType getOrCreatePluginType(String bundleName) {
		final Resource resource = getResource(bundleName);

		return getOrCreatePluginType(resource);
	}

	public static PluginType getOrCreatePluginType(final Resource resource) {
		final PluginType pluginType;
		if (resource.getContents().isEmpty()) {
			DocumentRoot root = PluginFactory.eINSTANCE.createDocumentRoot();
			pluginType = PluginFactory.eINSTANCE.createPluginType();
			root.setPlugin(pluginType);
			resource.getContents().add(root);
		} else {
			DocumentRoot root = (DocumentRoot) resource.getContents().get(0);
			if (root.getPlugin() == null) {
				root.setPlugin(PluginFactory.eINSTANCE.createPluginType());
			}
			pluginType = root.getPlugin();
		}

		return pluginType;
	}
}
