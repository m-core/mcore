package com.montages.mcore.ui.operations;

import static com.montages.mcore.ui.workspace.FileFunctions.deleteProjectPreferences;
import static com.montages.mcore.ui.workspace.FileFunctions.getFileFromResource;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.McoreUIPlugin;

public class CleanComponent {

	public static void clean(MComponent component, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		try {
			cleanComponent(component, monitor);
		} catch (CoreException e) {
			McoreUIPlugin.log(e);
		}
		deleteProjectPreferences(getFileFromResource(component.eResource()));
	}

	private static void cleanComponent(MComponent component, IProgressMonitor monitor) throws CoreException {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		cleanProjectFiles(root, component, monitor);
		cleanGeneratedProjects(root, component, monitor);
	}

	private static void cleanProjectFiles(IWorkspaceRoot root, MComponent component, IProgressMonitor monitor) throws CoreException {
		String bundle = component.getDerivedBundleName();

		// delete launch files

		IProject bundleProject = root.getProject(bundle);

		if (bundleProject != null && bundleProject.isAccessible()) {
			IResource launchFile = bundleProject.findMember(component.getEName().toLowerCase().concat(".launch"));
			if (launchFile != null && launchFile.exists()) {
				launchFile.delete(true, monitor);
			}
		}
	}

	private static void cleanGeneratedProjects(IWorkspaceRoot root, MComponent component, IProgressMonitor monitor) throws CoreException {
		String bundle = component.getDerivedBundleName();

		// delete base, edit, editor and update projects

		deleteProject(root, monitor, bundle.concat(".base"));

		deleteProject(root, monitor, bundle.concat(".edit"));

		deleteProject(root, monitor, bundle.concat(".editor"));

		deleteProject(root, monitor, bundle.concat(".update"));
	}

	private static void deleteProject(IWorkspaceRoot root, IProgressMonitor monitor, String bundle) throws CoreException {
		IResource updateProject = root.findMember(bundle);
		if (updateProject != null && updateProject.exists()) {
			updateProject.delete(true, monitor);
		}
	}

}
