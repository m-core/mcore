package com.montages.mcore.ui.operations;

import static com.montages.mcore.util.ResourceService.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.xocl.editorconfig.ui.codegen.actions.GenerateTestWorkspaceAction;
import org.xocl.workbench.ide.builder.XOCLNature;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.RenameService;

public class CreateProject {

	public static void create(MComponent component, IProgressMonitor monitor) throws CoreException {
		create(component, monitor, null);
	}
	public static void create(MComponent component, IProgressMonitor monitor, RenameService renameService) throws CoreException {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot root = workspace.getRoot();
		final IProject newProjectHandle = root.getProject(component.getDerivedBundleName());

		final IProjectDescription description = workspace.newProjectDescription(newProjectHandle.getName());
		// Add the XOCL Nature to the project
		description.setNatureIds(new String[] { XOCLNature.NATURE_ID });

		try {
			createProject(newProjectHandle, SubMonitor.convert(monitor, 50));
		} catch (OperationCanceledException e) {
			McoreUIPlugin.log(e);
			monitor.done();
			throw e;
		} catch (CoreException e) {
			McoreUIPlugin.log(e);
			throw e;
		}

		Resource mcoreResource = component.eResource();
		ResourceSet resourceSet = mcoreResource.getResourceSet();
		URI mcoreURI = normalizeResourceURI(resourceSet, component);

		if (mcoreURI == null) {
			return;
		}

		String uri = mcoreURI.toString().replace("platform:/plugin/", "platform:/resource/");

		try {
			mcoreResource.setURI(URI.createURI(uri));
			mcoreResource.save(getMcoreOptions(resourceSet, renameService));
		} catch (IOException e) {
			McoreUIPlugin.log(e);
			return;
		} finally {
			mcoreResource.setURI(mcoreURI);
		}

		try {
			new GenerateTestWorkspaceAction(newProjectHandle, null).run(SubMonitor.convert(monitor, 50));
		} catch (InvocationTargetException e) {
			McoreUIPlugin.log(e);
			return;
		} catch (InterruptedException e) {
			McoreUIPlugin.log(e);
			return;
		}

		createManifest(newProjectHandle, component, monitor);
		createFile(BUILD_PROPERTIES_TEMPLATE,"build.properties", newProjectHandle, monitor);
		PluginExtensions.addComponent(component);
	}

	private static void createProject(IProject project, IProgressMonitor monitor) throws CoreException, OperationCanceledException {
		if (!project.exists()) {
			project.create(monitor);
		}

		if (monitor.isCanceled()) {
			throw new OperationCanceledException();
		}

		if (!project.isOpen()) {
			project.open(monitor);
		}

		if (monitor.isCanceled()) {
			throw new OperationCanceledException();
		}

		IFolder modelFolder = project.getFolder(new Path("model"));
		if (!modelFolder.exists()) {
			modelFolder.create(false, true, monitor);
		}
	}

	private static String MANIFEST_TEMPLATE = 
			"Manifest-Version: 1.0\n" + //$NON-NLS-1$
			"Bundle-ManifestVersion: 2\n" + //$NON-NLS-1$
			"Bundle-Name: %%pluginName\n" + //$NON-NLS-1$
			"Bundle-SymbolicName: %s;singleton:=true\n" + //$NON-NLS-1$
			"Bundle-Version: 1.0.0.qualifier\n" + //$NON-NLS-1$
			"Require-Bundle: org.xocl.editorconfig,\n com.montages.common\n" + //$NON-NLS-1$
			"Bundle-Vendor: %%providerName\n"; //$NON-NLS-1$

	private static String BUILD_PROPERTIES_TEMPLATE = "bin.includes = META-INF/,\\\n"
			+ "model/,\\\n"
			+ "plugin.xml\n";

	private static void createManifest(IProject project, MComponent component, IProgressMonitor monitor) {
		IFolder manifestFolder = project.getFolder("META-INF");
		if (!manifestFolder.exists()) {
			try {
				manifestFolder.create(true, true, monitor);
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}
		}
		createFile(String.format(MANIFEST_TEMPLATE, component.getDerivedBundleName()), "MANIFEST.MF", manifestFolder, monitor);
	}

	public static void createFile(String content, String name, IContainer folder, IProgressMonitor monitor) {
		IFile manifest = folder.getFile(new Path(name));
		if (!manifest.exists()) {
			try {
				manifest.create(new ByteArrayInputStream(
						content.getBytes()), 
						true, monitor);
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}
		}
	}

	
}
