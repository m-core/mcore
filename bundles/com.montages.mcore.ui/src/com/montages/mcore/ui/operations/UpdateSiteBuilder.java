package com.montages.mcore.ui.operations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.equinox.internal.p2.ui.IProvHelpContextIds;
import org.eclipse.equinox.internal.p2.ui.ProvUI;
import org.eclipse.equinox.internal.p2.ui.dialogs.ISelectableIUsPage;
import org.eclipse.equinox.internal.p2.ui.dialogs.InstallWizard;
import org.eclipse.equinox.internal.p2.ui.dialogs.ProvisioningWizardDialog;
import org.eclipse.equinox.internal.p2.ui.model.IUElementListRoot;
import org.eclipse.equinox.p2.engine.ProvisioningContext;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.operations.InstallOperation;
import org.eclipse.equinox.p2.operations.RepositoryTracker;
import org.eclipse.equinox.p2.ui.LoadMetadataRepositoryJob;
import org.eclipse.equinox.p2.ui.ProvisioningUI;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.internal.core.FeatureModelManager;
import org.eclipse.pde.internal.core.ICoreConstants;
import org.eclipse.pde.internal.core.PDECore;
import org.eclipse.pde.internal.core.PluginModelManager;
import org.eclipse.pde.internal.core.exports.SiteBuildOperation;
import org.eclipse.pde.internal.core.feature.FeatureChild;
import org.eclipse.pde.internal.core.feature.FeaturePlugin;
import org.eclipse.pde.internal.core.feature.WorkspaceFeatureModel;
import org.eclipse.pde.internal.core.ifeature.IFeature;
import org.eclipse.pde.internal.core.ifeature.IFeatureChild;
import org.eclipse.pde.internal.core.ifeature.IFeatureModel;
import org.eclipse.pde.internal.core.ifeature.IFeaturePlugin;
import org.eclipse.pde.internal.core.isite.ISite;
import org.eclipse.pde.internal.core.isite.ISiteCategory;
import org.eclipse.pde.internal.core.isite.ISiteCategoryDefinition;
import org.eclipse.pde.internal.core.isite.ISiteFeature;
import org.eclipse.pde.internal.core.isite.ISiteModel;
import org.eclipse.pde.internal.core.site.SiteCategory;
import org.eclipse.pde.internal.core.site.SiteCategoryDefinition;
import org.eclipse.pde.internal.core.site.WorkspaceSiteModel;
import org.eclipse.pde.internal.core.util.CoreUtility;
import org.eclipse.pde.internal.ui.PDEUIMessages;
import org.eclipse.pde.internal.ui.editor.category.CategorySection;
import org.eclipse.ui.PlatformUI;

import com.montages.mcore.MComponent;
import com.montages.mcore.util.CreateFeatureException;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.ResourceService;
import com.montages.mcore.util.URIService;

/**
 * Class for creation update site project which include:<br>
 * 1) feature.xml with component's project: model, base, edit, editor and
 * includes component's related features<br>
 * 2) site.xml with prepared feature (1)<br>
 * 
 * <br>
 * e.g. <br>
 * <code>
 * UpdateSiteBuilder updateSiteBuilder = new UpdateSiteBuilder(component);<br>
 * IProject updateSiteProject = updateSitebuilder.getUpdateProject();<br>
 * updateSiteBuilder.build(updateSiteProject);<br>
 * </code> <br>
 * And then update site will be added to repositories list you will able to
 * install it in "Install New Software.." action
 */
@SuppressWarnings("restriction")
public class UpdateSiteBuilder {

	private static final String REPOSITORY_PREFIX = "repository_";

	private final MComponent myComponent;

	private final URIConverter myURIComverter;

	private final URIService myUriService;

	private final FeatureModelManager myModelManager;

	private com.montages.mcore.ui.wizards.pages.AvailableIUsPage myMainPage;

	public UpdateSiteBuilder(MComponent component) {
		myURIComverter = ResourceService.createURIConverter();
		myComponent = component;
		myUriService = new URIService();
		myModelManager = PDECore.getDefault().getFeatureModelManager();
	}

	/**
	 * Find site in project, build update site with included features and add
	 * local update site to repository list
	 * 
	 * @param updateProject
	 * @throws CoreException
	 */
	public void build(final IProject updateProject) throws CoreException {
		IFile siteFile = findRepositoryFolder(updateProject).getFile("site.xml");
		WorkspaceSiteModel siteModel = new WorkspaceSiteModel(siteFile);
		if (siteFile.exists()) {
			// load site from file
			siteModel.load();
		} else {
			return;
		}

		ISiteFeature[] siteFeatures = siteModel.getSite().getFeatures();
		ArrayList<IFeatureModel> list = new ArrayList<IFeatureModel>();
		for (int i = 0; i < siteFeatures.length; i++) {
			IFeatureModel model = PDECore.getDefault().getFeatureModelManager()
					.findFeatureModelRelaxed(siteFeatures[i].getId(), siteFeatures[i].getVersion());
			if (model != null)
				list.add(model);
		}

		if (list.isEmpty()) {
			return;
		}
		final BuildUpdateSiteOperation job = new BuildUpdateSiteOperation(list.toArray(new IFeatureModel[list.size()]),
				siteModel, PDEUIMessages.BuildSiteJob_name);
		job.run(new NullProgressMonitor());
	}

	public void setAvailablePage(com.montages.mcore.ui.wizards.pages.AvailableIUsPage page) {
		this.myMainPage = page;
	}

	/**
	 * Change main page for getting ability to set an initial repository
	 */
	private final class McoreInstallWizard extends InstallWizard {
		private McoreInstallWizard(ProvisioningUI ui, InstallOperation operation,
				Collection<IInstallableUnit> initialSelections, LoadMetadataRepositoryJob preloadJob) {
			super(ui, operation, initialSelections, preloadJob);
		}

		protected ISelectableIUsPage createMainPage(IUElementListRoot input, Object[] selections) {
			com.montages.mcore.ui.wizards.pages.AvailableIUsPage availableIUsPage = new com.montages.mcore.ui.wizards.pages.AvailableIUsPage(
					ui, this);
			mainPage = availableIUsPage;
			if (selections != null && selections.length > 0)
				mainPage.setCheckedElements(selections);
			setAvailablePage(availableIUsPage);
			return mainPage;

		}

		protected ProvisioningContext getProvisioningContext() {
			return ((com.montages.mcore.ui.wizards.pages.AvailableIUsPage) mainPage).getProvisioningContext();
		}
	}

	/**
	 * Make the run method as a public
	 */
	private static class BuildUpdateSiteOperation extends SiteBuildOperation {

		public BuildUpdateSiteOperation(IFeatureModel[] features, ISiteModel site, String jobName) {
			super(features, site, jobName);
		}

		@Override
		public IStatus run(IProgressMonitor monitor) {
			return super.run(monitor);
		}
	}

	public void openInstallWizard(IProgressMonitor monitor) throws CoreException {
		// find update site uri
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(getUpdateProjectName(myComponent));
		project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		java.net.URI uri = findRepositoryFolder(project).getLocation().toFile().toURI();

		ProvisioningUI ui = ProvisioningUI.getDefaultUI();
		RepositoryTracker repositoryTracker = ui.getRepositoryTracker();

		// clean up update sites
		java.net.URI[] knownURIs = repositoryTracker.getKnownRepositories(ui.getSession());
		ArrayList<java.net.URI> repositoriesToRemove = new ArrayList<java.net.URI>();
		if (knownURIs != null) {
			for (java.net.URI knownURI : knownURIs) {
				if ("file".equals(knownURI.getScheme())) {
					if (knownURI.getFragment() != null && knownURI.getFragment().contains(REPOSITORY_PREFIX)) {
						repositoriesToRemove.add(knownURI);
					}
				}
			}
		}

		if (!repositoriesToRemove.isEmpty()) {
			repositoryTracker.removeRepositories(
					repositoriesToRemove.toArray(new java.net.URI[repositoriesToRemove.size()]), ui.getSession());
		}

		// a site must be added to the known repositories
		// without this a feature will not be found by installer
		repositoryTracker.addRepository(uri, null, ui.getSession());

		openInstallWizard(ui, Collections.<IInstallableUnit>emptyList(), uri);
	}

	public int openInstallWizard(ProvisioningUI ui, Collection<IInstallableUnit> initialSelections,
			final java.net.URI uri) {
		InstallWizard wizard = new McoreInstallWizard(ui, null, initialSelections, null);
		WizardDialog dialog = new ProvisioningWizardDialog(ProvUI.getDefaultParentShell(), wizard);
		dialog.create();
		PlatformUI.getWorkbench().getHelpSystem().setHelp(dialog.getShell(), IProvHelpContextIds.INSTALL_WIZARD);
		myMainPage.setURI(uri);
		return dialog.open();
	}

	/**
	 * Collect related component then collect related features<br>
	 * If someone cannot be found or created when {@link CreateFeatureException}
	 * will be thrown<br>
	 * 
	 * @return {@link IProject} for component with feature and site.
	 * 
	 * @throws CoreException
	 * @throws CreateFeatureException
	 */
	public IProject generateUpdateProject(IProgressMonitor monitor) throws CoreException {
		Dependencies d = Dependencies.compute(myComponent);
		List<MComponent> allComponents = d.sort();
		allComponents.remove(myComponent);

		// collect related feature
		List<IFeature> includedFeatures = new ArrayList<IFeature>();
		for (MComponent c : allComponents) {
			URI normalizedURI = myURIComverter.normalize(c.eResource().getURI());
			IFeatureModel featureModel = null;
			String updateProjectName = getUpdateProjectName(c);
			if (normalizedURI.isPlatformPlugin()) {
				// feature must be installed in platform
				featureModel = myModelManager.findFeatureModel(updateProjectName);
			} else if (normalizedURI.isPlatformResource()) {
				IProject p = new UpdateSiteBuilder(c).generateUpdateProject(monitor);
				featureModel = myModelManager.getFeatureModel(p);
			}
			if (featureModel == null) {
				throw new CreateFeatureException("MConponent:" + c
						+ " is installed in the platform and doesn't has feature:" + updateProjectName);
			}
			includedFeatures.add(featureModel.getFeature());
		}

		// get update project
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		root.refreshLocal(IResource.DEPTH_ONE, monitor);
		IProject featureProject = root
				.getProject(getUpdateProjectName(myComponent));
		if (!featureProject.exists()) {
			// create new update project
			featureProject.create(new NullProgressMonitor());
			featureProject.open(new NullProgressMonitor());
		}

		IFolder repositoryFolder = cleanUpRepositoryFolder(monitor, featureProject);

		IFile featureFile = featureProject.getFile("feature.xml");
		WorkspaceFeatureModel featureModel = new WorkspaceFeatureModel(featureFile);
		if (featureFile.exists()) {
			// load feature from file
			featureModel.load();
		}

		// create feature
		fillFeature(includedFeatures, featureModel, myComponent);

		featureModel.save();

		// get update site file
		IFile siteFile = repositoryFolder.getFile("site.xml");
		WorkspaceSiteModel siteModel = new WorkspaceSiteModel(siteFile);

		// fill site xml
		fillUpdateSite(featureModel, siteModel);

		siteModel.save();

		CreateProject.createFile("bin.includes = feature.xml\n", "build.properties", featureProject,
				new NullProgressMonitor());

		return featureProject;
	}

	private IFolder cleanUpRepositoryFolder(IProgressMonitor monitor, IProject featureProject) throws CoreException {
		IResource repositoryFolder = findRepositoryFolder(featureProject);
		if (repositoryFolder != null) {
			repositoryFolder.delete(true, monitor);
		}
		IFolder result = featureProject.getFolder(
				REPOSITORY_PREFIX + new SimpleDateFormat("yy_MM_dd_hh_mm_ss").format(Calendar.getInstance().getTime()));
		result.create(true, false, monitor);
		return result;
	}

	private IFolder findRepositoryFolder(IProject project) throws CoreException {
		IResource[] resources = project.members();
		IFolder repositoryFolder = null;
		if (resources != null && resources.length > 0) {
			for (IResource resource : resources) {
				if (resource.getName() != null && resource.getName().startsWith(REPOSITORY_PREFIX)
						&& resource instanceof IFolder) {
					repositoryFolder = (IFolder) resource;
					break;
				}
			}
		}
		return repositoryFolder;
	}

	protected String getUpdateProjectName(MComponent component) {
		URI uri = myUriService.createPlatformResource(component);
		return uri.segment(1).concat(".update");
	}

	protected String[] getMcoreProjectsSet(MComponent component) {
		URI uri = myUriService.createPlatformResource(component);
		if (uri.segmentCount() < 2) {
			return new String[0];
		}
		String mcoreProjectName = uri.segment(1);
		return new String[] { mcoreProjectName, mcoreProjectName.concat(".base"), mcoreProjectName.concat(".edit"),
				mcoreProjectName.concat(".editor") };
	}

	private void fillUpdateSite(WorkspaceFeatureModel featureModel, WorkspaceSiteModel siteModel) throws CoreException {
		ISite site = siteModel.getSite();
		ISiteFeature newFeature = CategorySection.createSiteFeature(siteModel, featureModel);

		ISiteFeature feature = null;
		for (ISiteFeature siteFeature : site.getFeatures()) {
			if (siteFeature.getId().equals(newFeature.getId())) {
				feature = siteFeature;
				break;
			}
		}
		if (feature == null) {
			feature = newFeature;
			site.addFeatures(new ISiteFeature[] { feature });
		}

		ISiteCategoryDefinition category = null;
		for (ISiteCategoryDefinition def : site.getCategoryDefinitions()) {
			if ("MCore category".equals(def.getName())) {
				category = def;
				break;
			}
		}

		if (category == null) {
			category = (SiteCategoryDefinition) siteModel.getFactory().createCategoryDefinition();
			category.setName("MCore category");
			category.setLabel("MCore category");
			site.addCategoryDefinitions(new ISiteCategoryDefinition[] { category });
		}

		boolean hasFeatureMcoreCategory = true;
		for (ISiteCategory siteCategory : feature.getCategories()) {
			ISiteCategoryDefinition def = siteCategory.getDefinition();
			if (def.getLabel().equals(category.getName()) && def.getLabel().equals(category.getLabel())) {
				hasFeatureMcoreCategory = false;
				break;
			}
		}
		if (hasFeatureMcoreCategory) {
			SiteCategory featureCategory = (SiteCategory) siteModel.getFactory().createCategory(feature);
			featureCategory.setName("MCore category");
			featureCategory.setLabel("MCore category");
			feature.addCategories(new ISiteCategory[] { featureCategory });
		}
	}

	private void fillFeature(List<IFeature> includedFeatures, WorkspaceFeatureModel featureModel, MComponent comopnent)
			throws CoreException {
		IFeature feature = featureModel.getFeature();

		String id = getUpdateProjectName(comopnent);
		if (feature.getId() == null) {
			feature.setId(id);
		}
		if (feature.getLabel() == null) {
			feature.setLabel(id);
		}

		if (feature.getVersion() == null) {
			feature.setVersion("1.0.0.qualifier"); //$NON-NLS-1$
		}

		// check already included feature
		for (IFeatureChild child : feature.getIncludedFeatures()) {
			String childID = child.getId();
			IFeature included = null;
			for (IFeature incF : includedFeatures) {
				if (incF.getId().equals(childID)) {
					included = incF;
					break;
				}
			}
			if (included != null) {
				includedFeatures.remove(included);
			}
		}

		if (!includedFeatures.isEmpty()) {
			// add new features
			IFeatureChild[] childer = new IFeatureChild[includedFeatures.size()];

			for (int i = 0; i < includedFeatures.size(); i++) {
				IFeature featureToInclude = includedFeatures.get(i);
				FeatureChild child = (FeatureChild) featureModel.getFactory().createChild();
				child.loadFrom(featureToInclude);
				child.setVersion(ICoreConstants.DEFAULT_VERSION);
				childer[i] = child;
			}

			feature.addIncludedFeatures(childer);
		}

		PluginModelManager pluginModelManager = PluginModelManager.getInstance();

		IFeaturePlugin[] featurePlugins = feature.getPlugins();
		List<IFeaturePlugin> plugins = new ArrayList<IFeaturePlugin>(3);

		// add plug-ins
		for (String pluginToInclude : getMcoreProjectsSet(comopnent)) {
			updateProperties(pluginToInclude);

			IPluginModelBase pluginModel = pluginModelManager.findModel(pluginToInclude);
			if (pluginModel == null) {
				throw new CreateFeatureException(
						"Plugin \"" + pluginToInclude + "\" cannot be found. Please, generate component's bundles.");
			}

			boolean shouldInclude = true;
			for (IFeaturePlugin featurePlugin : featurePlugins) {
				if (featurePlugin.getId().equals(pluginToInclude)) {
					// plug-in already added
					shouldInclude = false;
					break;
				}
			}

			if (shouldInclude) {
				FeaturePlugin plugin = (FeaturePlugin) featureModel.getFactory().createPlugin();
				plugin.loadFrom(pluginModel.getPluginBase());
				plugin.setVersion(ICoreConstants.DEFAULT_VERSION);
				plugin.setUnpack(CoreUtility.guessUnpack(pluginModel.getBundleDescription()));
				plugins.add(plugin);
			}
		}
		if (!plugins.isEmpty()) {
			feature.addPlugins(plugins.toArray(new IFeaturePlugin[plugins.size()]));
		}
	}

	private boolean updateProperties(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(projectName);
		if (!project.exists()) {
			return false;
		}
		IResource[] resources = new IResource[0];
		try {
			resources = project.members();
		} catch (CoreException e) {
			e.printStackTrace();
		}
		IFile f = project.getFile("build.properties");
		if (!f.exists()) {
			try {
				f.create(new ByteArrayInputStream(new byte[0]), true, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		Properties props = new Properties();
		try {
			props.load(f.getContents());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		String includesString = props.getProperty("bin.includes", "");
		ArrayList<String> newIncludes = new ArrayList<String>();
		if (includesString != null && includesString.length() > 0) {
			String includes[] = includesString.split(",");
			newIncludes.addAll(Arrays.asList(includes));
		}
		for (IResource r : resources) {
			String fileName = r.getName();
			if (r instanceof IFile) {
				if (!newIncludes.contains(fileName)) {
					newIncludes.add(fileName);
				}
			} else if (r instanceof IFolder) {
				String folderName = fileName + "/";
				if (!newIncludes.contains(folderName)) {
					newIncludes.add(folderName);
				}
			}
		}
		StringOutputStream stringStream = new StringOutputStream();
		try {
			StringBuilder sb = new StringBuilder();
			for (String next : newIncludes) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				sb.append(next);
			}
			props.setProperty("bin.includes", sb.toString());
			props.store(stringStream, "#");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		try {
			f.setContents(new ByteArrayInputStream(stringStream.getString().getBytes()), 0, null);
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	class StringOutputStream extends OutputStream {

		StringBuilder mBuf = new StringBuilder();

		public String getString() {
			return mBuf.toString();
		}

		@Override
		public void write(int b) throws IOException {
			mBuf.append((char) b);
		}
	}
}
