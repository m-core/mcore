package com.montages.mcore.ui.operations;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.pde.internal.core.PDECore;
import org.eclipse.pde.internal.core.ifeature.IFeatureModel;
import org.eclipse.pde.internal.core.ifeature.IFeaturePlugin;
import org.osgi.framework.Bundle;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.ResourceService;

@SuppressWarnings("restriction")
public class FeatureImporter {

	private final IFeatureModel myFeatureModel;

	private final String componentName;

	public FeatureImporter(MComponent component) {
		if (component.eResource() == null) {
			throw new IllegalArgumentException("Component doesn't have a resource. Component: " + component);
		}
		URI uri = component.eResource().getURI();
		if (uri == null) {
			throw new IllegalArgumentException("Component's resource doesn't have an uri. Component: " + component);
		}
		uri = ResourceService.normalizeResourceURI(component);
		uri = ResourceService.transformResourceToPlugin(uri);
		componentName = uri.segment(1);
		String featureName = componentName.concat(".update");
		IFeatureModel featureModel = PDECore.getDefault().getFeatureModelManager().findFeatureModel(featureName);
		if (featureModel == null) {
			throw new IllegalArgumentException("Component doesn't have installed feature and cannot be imported. Component: " + component + ". Feature:" + featureName);
		}
		myFeatureModel = featureModel;
	}

	public String[] getProjects() {
		IFeaturePlugin[] plugins = myFeatureModel.getFeature().getPlugins();
		String[] names = new String[plugins.length];
		for (int i = 0; i < plugins.length; i++) {
			names[i] = plugins[i].getId();
		}
		return names;
	}

	public void doImport(IProgressMonitor monitor) {
		System.out.println("FeatureImporter.doImport()");
		for (String s: getProjects()) {
			System.out.println(s);
			importBundleToWS(s, monitor);
		}
	}

	public boolean componentExistsInWorkspace() {
		return getProject(componentName).exists();
	}

	private IProject getProject(String name) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		return root.getProject(name);
	}

	public void importBundleToWS(String projectName, IProgressMonitor monitor) {
		Bundle bundle = Platform.getBundle(projectName);
		if (bundle == null) {
			throw new RuntimeException("Project:" + projectName + " cann't be found in platform!");
		}
		IProject p = getProject(projectName);
		if (!p.exists()) {
			try {
				p.create(monitor);
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}
		}
		if (!p.isOpen()) {
			try {
				p.open(monitor);
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}
		}
		for (String s: collectElements(bundle, "")) {
			URL url = bundle.getEntry(s);
			if (s.endsWith("/")) {
				IFolder f = p.getFolder(new Path(s));
				if (!f.exists()) {
					try {
						f.create(true, false, monitor);
					} catch (CoreException e) {
						McoreUIPlugin.log(e);
					}
				}
			} else {
				InputStream is = null;
				try {
					is = url.openConnection().getInputStream();
				} catch (IOException e) {
					McoreUIPlugin.log(e);
				}
				IFile f = p.getFile(new Path(s));
				try {
					if (!f.exists()) {
						f.create(is, true, monitor);
					} else {
						f.setContents(is, 0, monitor);
					}
				} catch (CoreException e) {
					McoreUIPlugin.log(e);
				}
			}
		}
	}

	protected List<String> collectElements(Bundle b, String path) {
		Enumeration<String> children = b.getEntryPaths(path);
		if (children == null) {
			return Collections.emptyList();
		}
		ArrayList<String> result = new ArrayList<String>();
		while (children.hasMoreElements()) {
			String child = (String) children.nextElement();
			result.add(child);
			if (child.endsWith("/")) {
				result.addAll(collectElements(b, child));
			}
		}
		return result;
	}
}
