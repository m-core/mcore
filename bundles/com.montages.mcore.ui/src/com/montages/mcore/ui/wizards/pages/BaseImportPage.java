package com.montages.mcore.ui.wizards.pages;

import java.io.File;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.ui.dialogs.WorkspaceResourceDialog;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.montages.mcore.codegen.ui.conversions.Importer;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.wizards.Messages;

public class BaseImportPage extends WizardPage {

	private static final String WINDOWS_SEPARATOR = "\\"; // $NON-NLS-1$

	private static final String UNIX_SEPARATOR = "/"; // $NON-NLS-1$

	private static final String PLATFORM_URI_PREFIX = "platform:/"; // $NON-NLS-1$

	private static final String FILE_URI_PREFIX = "file:/"; // $NON-NLS-1$

	private static final String[] FILTER_FILE_EXTENTIONS = new String[] { //
			"*.mcore", // $NON-NLS-1$
			"*.ecore", //$NON-NLS-2$
			"*.*" //$NON-NLS-3$
	};

	private static final String[] FILTER_NAMES = new String[] { //
			Messages.BaseImportPage_FilterMcoreFiles, //
			Messages.BaseImportPage_FilterEcoreFiles, //
			Messages.BaseImportPage_FilterAllFiles//
	};
	private Importer importer;

	public BaseImportPage(String pageName, URI modelURI) {
		super(pageName);
		setTitle(Messages.BaseImportPage_Title);
		setDescription(Messages.BaseImportPage_Descr);
		importer = new Importer(modelURI);
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		initializeDialogUnits(parent);

		createImportSection(composite);

		setErrorMessage(null);
		setMessage(null);
		setControl(composite);

		Dialog.applyDialogFont(parent);
		setPageComplete(validatePage());
	}

	public Importer getImporter() {
		return importer;
	}

	private boolean validatePage() {
		if (importer != null && (importer.isMCoreImport() || importer.isEcoreImport())) {
			setErrorMessage(null);
			return true;
		}
		setErrorMessage(Messages.BaseImportPage_NoFileChoosenError);
		return false;
	}

	protected void createImportSection(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));

		Label label = new Label(composite, SWT.NONE);
		label.setText(Messages.MCoreProjectFirstPage_Choose_MCore_FIle);

		Button btnBrowseFilesystem = new Button(composite, SWT.NONE);
		btnBrowseFilesystem.setText(Messages.MCoreProjectFirstPage_Browse_File_System);

		Button btnBrowseWorkspace = new Button(composite, SWT.NONE);
		btnBrowseWorkspace.setText(Messages.MCoreProjectFirstPage_Browse_Workspace);

		Composite loadComposite = new Composite(parent, SWT.NONE);
		loadComposite.setLayout(new GridLayout(1, false));
		loadComposite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));

		final Text textModelURI = new Text(loadComposite, SWT.SINGLE | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			textModelURI.setLayoutData(data);
			String uri = getModelURIAsString();
			if (uri.isEmpty()) {
				uri = getLastExportURI();
				refreshModelURI(uri);
				if (!getModelURIAsString().isEmpty()) {
					textModelURI.setText(uri);
				}
			} else {
				textModelURI.setText(uri);
			}

			textModelURI.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					refreshModelURI(textModelURI.getText());
					setPageComplete(validatePage());
				}
			});
		}

		btnBrowseWorkspace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ViewerFilter extensionFilter = new ViewerFilter() {
					@Override
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return true;
					}
				};
				IFile[] files = WorkspaceResourceDialog.openFileSelection(//
						getShell(), //
						"Import Repository",//
						"Select repository to import",//
						false, // 
						null, // 
						Collections.singletonList(extensionFilter));
				if (files.length == 0 || !isValidWorkspaceResource(files[0])) {
					return;
				}
				textModelURI.setText(URI.createPlatformResourceURI(files[0].getFullPath().toString(), true).toString());
			}
		});

		btnBrowseFilesystem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);

				fileDialog.setFilterNames(FILTER_NAMES);
				fileDialog.setFilterExtensions(FILTER_FILE_EXTENTIONS);
				if (fileDialog.open() != null && fileDialog.getFileNames().length > 0) {
					String[] fileNames = fileDialog.getFileNames();
					String filePath = fileDialog.getFilterPath() + File.separator + fileNames[0];
					if (filePath != null && !filePath.isEmpty()) {
						textModelURI.setText(URI.createFileURI(filePath).toString());
					}
				}
			}
		});
	}

	private void refreshModelURI(String filePath) {
		if (!filePath.startsWith(FILE_URI_PREFIX) && !filePath.startsWith(PLATFORM_URI_PREFIX)) {
			setErrorMessage(Messages.BaseImportPage_UnknownSchemaError);
			return;
		}
		try {
			setNewURI(URI.createURI(filePath));
			setErrorMessage(null);
		} catch (IllegalArgumentException e) {
			setNewURI(null);
			setErrorMessage(Messages.BaseImportPage_WrongFilePathError);
		}
	}

	private String getModelURIAsString() {
		if (importer == null || importer.getUri() == null) {
			return "";
		}
		return importer.getUri().toString();
	}

	protected void setNewURI(URI uri) {
		importer = uri == null ? null : new Importer(uri);
	}

	private boolean isValidWorkspaceResource(IResource resource) {
		if (resource.getType() == IResource.FILE) {
			return resource.getFileExtension().equals("mcore") || resource.getFileExtension().equals("ecore");
		}
		return false;
	}

	private static String getLastExportURI() {
		McoreUIPlugin plugin = McoreUIPlugin.getInstance();
		String lastExportPath = plugin.getPreference(RepositoryExportPage.LAST_EXPORT_PATH);
		String lastExportModel = plugin.getPreference(RepositoryExportPage.LAST_EXPORT_MODEL);

		if (lastExportModel.length() == 0 || lastExportPath.length() == 0) {
			return ""; //$NON-NLS-1$
		}

		String uri = FILE_URI_PREFIX + lastExportPath + UNIX_SEPARATOR + lastExportModel;
		// unix separator '/' is used to split URI's parts, so we replace windows separators
		return uri.replace(WINDOWS_SEPARATOR, UNIX_SEPARATOR); //$NON-NLS-1$ $NON-NLS-2$
	}

	@Override
	public boolean isPageComplete() {
		return importer != null;
	}
}
