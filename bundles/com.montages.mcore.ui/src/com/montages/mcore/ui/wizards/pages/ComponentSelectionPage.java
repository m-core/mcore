package com.montages.mcore.ui.wizards.pages;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

import com.montages.common.CommonPlugin;
import com.montages.mcore.ui.McoreUIPlugin;

public class ComponentSelectionPage extends WizardPage {

	private TableViewer viewer;

	public ComponentSelectionPage(String pageName) {
		super(pageName);
		setTitle("Export");
		setDescription("Select component to export.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		initializeDialogUnits(parent);

		createSelectionSection(composite);

		setErrorMessage(null);
		setMessage(null);
		setControl(composite);

		Dialog.applyDialogFont(parent);
		setPageComplete(validatePage());
	}

	private boolean validatePage() {
		ISelection selection = viewer.getSelection();
		return selection != null && !selection.isEmpty();
	}

	private void createSelectionSection(Composite composite) {
		final Map<URI, URI> uriMap = new HashMap<URI, URI>();
		final Set<URI> models = CommonPlugin.computePluginModels(uriMap);
		models.addAll(CommonPlugin.computeWorkspaceModels(uriMap));

		final Table table = new Table(composite, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			table.setLayoutData(data);
		}
		viewer = new TableViewer(table);

		viewer.setLabelProvider(new LabelProvider() {

			@Override
			public Image getImage(Object element) {
				if (element instanceof URI) {
					return McoreUIPlugin.MCOMPONENT_IMAGE;
				}
				return super.getImage(element);
			}
			@Override
			public String getText(Object element) {
				if (element instanceof URI) {
					return element.toString().replaceAll("mcore:", "http:");
				}
				return super.getText(element);
			}
		});
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setInput(models.toArray());
		viewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				setPageComplete(validatePage());
			}
		}); 
	}
	
	public Object getSelection() {
		return viewer != null ? viewer.getSelection() : null;
	}

}
