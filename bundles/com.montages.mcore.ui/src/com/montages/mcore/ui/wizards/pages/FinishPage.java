package com.montages.mcore.ui.wizards.pages;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class FinishPage extends WizardPage {

	private boolean isImplement = false;
	private boolean isImplementAndRun = false;

	public FinishPage(String pageName) {
		super(pageName);
		setTitle("Finish");
		setDescription("Select action to perform.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		composite.setLayout(new GridLayout(1, false));

		Button btnDoNothing = new Button(composite, SWT.RADIO);
		btnDoNothing.setText("Do Nothing");
		btnDoNothing.setSelection(true);
		btnDoNothing.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				isImplement = false;
				isImplementAndRun = false;
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button btnImplementProject = new Button(composite, SWT.RADIO);
		btnImplementProject.setText("Implement Project");

		btnImplementProject.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				isImplement = true;
				isImplementAndRun = false;
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button btnImplementAndRunProject = new Button(composite, SWT.RADIO);
		btnImplementAndRunProject.setText("Implement and Run Project");

		btnImplementAndRunProject.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				isImplement = false;
				isImplementAndRun = true;
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		setControl(composite);
	}

	public boolean isImplement() {
		return isImplement;
	}

	public boolean isImplementAndRun() {
		return isImplementAndRun;
	}
}
