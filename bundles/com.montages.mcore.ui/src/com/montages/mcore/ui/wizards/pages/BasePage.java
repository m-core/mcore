package com.montages.mcore.ui.wizards.pages;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Text;

import com.montages.mcore.ui.wizards.Messages;

public abstract class BasePage extends WizardPage {

	protected Text textDomainType;
	protected Text textDomainName;
	protected Text textCompName;
	protected Text textDiagramName;

	public BasePage(String pageName) {
		super(pageName);
	}

	protected boolean validatePage() {
		if (getDomainType().isEmpty()) {
			setErrorMessage(Messages.MCoreProjectFirstPage_Domain_Type_Value);
			return false;
		}

		if (getDomainName().isEmpty()) {
			setErrorMessage(Messages.MCoreProjectFirstPage_Domain_Name_Value);
			return false;
		}

		if (getComponentName().isEmpty()) {
			setErrorMessage(Messages.MCoreProjectFirstPage_Component_Name_Value);
			return false;
		}

		if (getDiagramName().isEmpty()) {
			setErrorMessage(Messages.McoreProjectFirstPage_Diagram_Name_Value);
			return false;
		}

		if (ResourcesPlugin.getWorkspace().getRoot().getProject(getProjectName()).exists()) {
			setErrorMessage(Messages.MCoreProjectFirstPage_Project_Already_Exist);
			return false;
		} else {
			setErrorMessage(null);
			return true;
		}
	}

	public String getDomainType() {
		return textDomainType != null ? textDomainType.getText() : "";
	}

	public String getDomainName() {
		return textDomainName != null ? textDomainName.getText() : "";
	}

	public String getComponentName() {
		return textCompName != null ? textCompName.getText() : "";
	}

	public String getDiagramName() {
		return textDiagramName != null ? textDiagramName.getText() : "";
	}

	public String getProjectName() {
		String type = getDomainType().replaceAll("\\s", "").toLowerCase();
		String domain = getDomainName().replaceAll("\\s", "").toLowerCase();
		String name = getComponentName().replaceAll("\\s", "").toLowerCase();

		return type.concat(".") .concat(domain).concat(".").concat(name);
	}
}
