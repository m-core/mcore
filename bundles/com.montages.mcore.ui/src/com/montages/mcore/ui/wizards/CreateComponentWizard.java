package com.montages.mcore.ui.wizards;

import static com.montages.mcore.util.ResourceService.normalizeResourceURI;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import com.montages.mcore.MComponent;
import com.montages.mcore.diagram.part.McoreDiagramEditorUtil;
import com.montages.mcore.mcore2ecore.MObject2EObject;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.operations.CreateProject;
import com.montages.mcore.ui.operations.GenerateComponent;
import com.montages.mcore.ui.operations.GenerateEcore;
import com.montages.mcore.ui.operations.GenerateEditorConfig;
import com.montages.mcore.ui.operations.GenerateInstanceResources;
import com.montages.mcore.ui.operations.GenerateTableEditor;
import com.montages.mcore.ui.operations.RunComponent;
import com.montages.mcore.ui.wizards.pages.FinishPage;
import com.montages.mcore.ui.wizards.pages.MCorePackagePage;
import com.montages.mcore.ui.wizards.pages.NewComponentPage;
import com.montages.mcore.ui.wizards.pages.NewDiagramNamePage;
import com.montages.mcore.util.MComponentResourceIdMap;
import com.montages.mcore.util.McoreUtil;
import com.montages.mcore.util.ResourceService;

public class CreateComponentWizard extends Wizard implements INewWizard {

	public static final String ID = "com.montages.mcore.ui.wizards.MComponentWizard";

	private NewComponentPage startPage;

	private MCorePackagePage contentPage;

	private FinishPage finishPage;

	public CreateComponentWizard() {
		setWindowTitle("Create Component");
	}

	@Override
	public void addPages() {
		addPage(startPage = new NewComponentPage("Start"));
		addPage(contentPage = new MCorePackagePage("Content"));
		addPage(finishPage = new FinishPage("Finish"));
	}

	@Override
	public boolean canFinish() {
		return startPage.isPageComplete() && contentPage.isPageComplete();
	}

	@Override
	public boolean needsProgressMonitor() {
		return true;
	}

	private void createInstanceModelFiles(MComponent component) {
		component = McoreUtil.findComponentInTransactionResources(component);
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(component.getDerivedBundleName());
		EPackage ePackage = component.getOwnedPackage().get(0).getInternalEPackage();
		String modelFileName = "My.".concat(ePackage.getName().toLowerCase());
		IFile modelFile = project.getFile(new Path("workspace").append("test").append(modelFileName));
		EClass eClass = (EClass) ePackage.getEClassifiers().get(0);
		EObject eObject = EcoreUtil.create(eClass);
		EAttribute eAttribute = (EAttribute) eObject.eClass().getEStructuralFeature( "text1" );
		eObject.eSet(eAttribute, MObject2EObject.createValueFromString(eAttribute.getEAttributeType(), "1"));
		
		URI fileURI = URI.createPlatformResourceURI(modelFile.getFullPath().toString(), true);
		Resource resource = GenerateInstanceResources.createXmlResource(fileURI);
		resource.getContents().add(eObject);
		GenerateInstanceResources.saveResource(resource, ResourceService.getSaveOptions());
		
		GenerateInstanceResources.saveResource(GenerateInstanceResources.createJsonResource(modelFile.getFullPath(), eObject), null);
	}

	private void initDiagram(MComponent component, String diagramName, IProgressMonitor monitor) {
		URI mcoreURI = normalizeResourceURI(component);
		if (mcoreURI == null) {
			return;
		}
		URI diagramURI = mcoreURI.trimSegments(1).appendSegment(NewDiagramNamePage.transformDiagramNameToNameInURI(diagramName)).appendFileExtension("mcore_diagram");
		McoreDiagramEditorUtil.createDiagram(diagramURI, mcoreURI, monitor);
	}

	@Override
	public boolean performFinish() {
		final MComponent component = getData().createComponent();
		final String diagramName = startPage.getDiagramName();
		final boolean doImpl = isImplement() || isRun();
		final boolean doRun = isRun();

		if (component != null) {
			try {
				getContainer().run(true, false, new IRunnableWithProgress() {

					@Override
					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
						monitor.beginTask("Create Component", 5);
						try {
							CreateProject.create(component, SubMonitor.convert(monitor, 1));
						} catch (CoreException e) {
							throw new InvocationTargetException(e);
						}

						GenerateTableEditor.generate(component, SubMonitor.convert(monitor, 1));
						GenerateEcore.transform(component, SubMonitor.convert(monitor, 1));
						createInstanceModelFiles(component);
						GenerateEditorConfig.generate(component, SubMonitor.convert(monitor, 1));
						initDiagram(component, diagramName,  SubMonitor.convert(monitor, 1));
						
						if (doImpl) {
							GenerateComponent.generate(component, SubMonitor.convert(monitor, 1));
						}
						if (doRun) {
							RunComponent.run(component, MComponentResourceIdMap.EMPTY_MAP, SubMonitor.convert(monitor, 1));
						}
						monitor.done();
					}
				});
			} catch (InvocationTargetException e) {
				MessageDialog.openError(getShell(), "Error", e.getTargetException().getMessage());
				return false;
			} catch (InterruptedException e) {
				return false;
			}

			McoreUIPlugin.openEditor(component);
		}
		return true;
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {

	}

	private WizardData getData() {
		return new WizardData.Builder().setComponent(startPage.getDomainType(), startPage.getDomainName(), startPage.getComponentName())
				.setPackage(contentPage.getPackageName(), contentPage.getRootClassName(), MCorePackagePage.propertyName).build();
	}

	private boolean isImplement() {
		return finishPage.isImplement();
	}

	private boolean isRun() {
		return finishPage.isImplementAndRun();
	}
}
