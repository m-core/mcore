package com.montages.mcore.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import com.montages.mcore.codegen.ui.conversions.Importer;
import com.montages.mcore.ui.wizards.pages.BaseImportPage;
import com.montages.mcore.ui.wizards.pages.ComponentPreviewPage;

public class ImportRepositoryWizard extends Wizard implements IImportWizard {

	public static final String ID = "com.montages.mcore.ui.wizards.ImportRepositoryWizard";

	private ComponentPreviewPage previewPage;

	private BaseImportPage importPage;

	private URI modelURI;

	public ImportRepositoryWizard() {
		super();
		setWindowTitle("Import");
	}

	@Override
	public void addPages() {
		super.addPages();
		addPage(importPage = new BaseImportPage("MCoreImportFirst", modelURI));
		addPage(previewPage = new ComponentPreviewPage("MCoreImportSecond"));
	}
	
	@Override
	public boolean canFinish() {
		ComponentPreviewPage previewPage = (ComponentPreviewPage)getNextPage(importPage);
		return previewPage != null //
				&& previewPage.isPageComplete();
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		IWizardPage next = super.getNextPage(page);
		if (next instanceof ComponentPreviewPage && page instanceof BaseImportPage) {
			BaseImportPage previous = (BaseImportPage) page;
			ComponentPreviewPage nextPage = ((ComponentPreviewPage)next);
			nextPage.setImporter(previous.getImporter());
		}
		return next;
	}

	@Override
	public boolean needsProgressMonitor() {
		return true;
	}

	@Override
	public boolean performFinish() {
		final Importer importer = previewPage.getImporter();
		try {
			getContainer().run(true, false, new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					if (importer != null) {
						importer.doImport(monitor);
					}
				}
			});
		} catch (InvocationTargetException e) {
			MessageDialog.openError(getShell(), "Error", e.getTargetException().getMessage());
			return false;
		} catch (InterruptedException e) {
			return false;
		}

		return true;
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		if (selection==null || selection.isEmpty()) {
			return;
		}
		createAndSetupModelURI(((String[]) selection.getFirstElement())[0]);
	}
	
	private void createAndSetupModelURI(String filePath) {
		URI result = URI.createFileURI(filePath);
		modelURI = result;
	}
}
