package com.montages.mcore.ui.wizards.pages;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.montages.common.resource.DiagramResourceResolver;
import com.montages.mcore.ui.wizards.Messages;

public class NewDiagramNamePage extends WizardPage {

	public static final String BASE_DIAGRAM_NAME = "Diagram";

	private Text myDiagramName;

	private List<URI> existingDiagrams; 

	public NewDiagramNamePage(String pageName) {
		super(pageName);
		setDescription("Please input a new name for a new diagram");
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		{
			GridLayout layout = new GridLayout(2, false);
			GridData data = new GridData(GridData.FILL_BOTH);
			composite.setLayout(layout);
			composite.setLayoutData(data);
		}

		Label label = new Label(composite, SWT.NONE);
		label.setText("New Diagram Name:");
		{
			GridData data = new GridData();
			data.grabExcessHorizontalSpace = false;
			data.horizontalSpan = 1;
			data.horizontalAlignment = GridData.BEGINNING;
			label.setLayoutData(data);
		}

		myDiagramName = new Text(composite, SWT.SINGLE | SWT.BORDER);
		myDiagramName.setText(createDiagramName(existingDiagrams));
		{
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			data.horizontalSpan = 1;
			myDiagramName.setLayoutData(data);
		}

		myDiagramName.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(validatePage());
			}
		});

		setControl(composite);
		setPageComplete(validatePage());
	}

	public String getDiagramName() {
		if (myDiagramName == null) {
			return "";
		}
		return transformDiagramNameToNameInURI(myDiagramName.getText());
	}

	private boolean validatePage() {
		String diagramName = getDiagramName();
		if (diagramName.isEmpty()) {
			setErrorMessage(Messages.McoreProjectFirstPage_Diagram_Name_Value);
			return false;
		}
		if (existingDiagrams != null && !existingDiagrams.isEmpty()) {
			for (URI uri : existingDiagrams) {
				if (diagramName.equals(getDiagramNameFromURI(uri))) {
					setErrorMessage(Messages.InitNewDiagramPage_Name_Collision);
					return false;
				}
			}
		}
		setErrorMessage(null);
		return true;
	}

	public void init(URI componentURI) {
		existingDiagrams = DiagramResourceResolver.getInstance().collectDiagramForComponent(componentURI);
	}

	protected static String getDiagramNameFromURI(URI uri) {
		return uri.trimFileExtension().lastSegment();
	}

	public static String createDiagramName(List<URI> existingDiagrams) {
		String defaultName = BASE_DIAGRAM_NAME;
		for (int index = 1; index < Integer.MAX_VALUE; index++) {
			String newDiagramName = defaultName + " " + index;
			String newRealDiagramName = transformDiagramNameToNameInURI(newDiagramName);
			boolean isNameExist = false;
			for (URI uri : existingDiagrams) {
				if (newRealDiagramName.equals(getDiagramNameFromURI(uri))) {
					isNameExist = true;
					break;
				}
			}

			if (!isNameExist) {
				return newDiagramName;
			}
		}
		return defaultName;
	}

	public static String transformDiagramNameToNameInURI(String diagramName) {
		diagramName = diagramName.trim();
		if (diagramName.isEmpty()) {
			return diagramName;
		}
		String diagramStart = diagramName.substring(0, 1);
		diagramName = diagramName.replaceFirst(diagramStart, diagramStart.toLowerCase());
		return diagramName.replace(" ", "");
	}
}
