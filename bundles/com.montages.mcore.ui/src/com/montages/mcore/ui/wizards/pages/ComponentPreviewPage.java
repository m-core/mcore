package com.montages.mcore.ui.wizards.pages;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.montages.common.CommonPlugin;
import com.montages.common.resource.McoreResourceResolver;
import com.montages.mcore.MComponent;
import com.montages.mcore.McorePackage;
import com.montages.mcore.codegen.ui.conversions.Importer;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.RenameService;
import com.montages.mcore.util.URIService;

public class ComponentPreviewPage extends WizardPage {

	private Composite compositeImportValues;
	private Text domainTypeText;
	private Text domainNameText;
	private Text componentNameText;
	private ReplaceButtonMergeListener replaceListener;
	private ReuseButtonMergeListener reuseListener;
	private Button replaceButton;
	private Button reuseButton;
	private TableViewer viewer;
	private Importer importer;

	private Map<URI, URI> componentMap;

	private ModifyListener change = new ModifyListener() {
		@Override
		public void modifyText(ModifyEvent e) {
			Text text = (Text) e.getSource();
			Object[] data = (Object[]) text.getData();
			ISelection selection = viewer.getSelection();

			if (data != null && selection instanceof StructuredSelection) {
				MComponent component = (MComponent) data[0];

				if (component.equals(((StructuredSelection) selection).getFirstElement())) {
					for (int i = 1; i < data.length; i++) {
						EAttribute attribute = (EAttribute) data[i];
						component.eSet(attribute, text.getText());
					}
					viewer.update(component, null);
					importer.updateComponentURI(component);
					updateReplaceButton(component);
					setPageComplete(validatePage());
				}
			}
		}
	};

	public ComponentPreviewPage(String pageName) {
		super(pageName);

		setTitle(Messages.ComponentPreviewPage_Title);
		setDescription(Messages.ComponentPreviewPage_Description);
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		initializeDialogUnits(parent);

		createContent(composite);

		setErrorMessage(null);
		setMessage(null);
		setControl(composite);

		Dialog.applyDialogFont(parent);
		setPageComplete(true);
	}

	public void setImporter(Importer importer) {
		this.importer = importer;
	}

	public Importer getImporter() {
		return importer;
	}

	@Override
	public boolean isPageComplete() {
		return super.isPageComplete() && validatePage();
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			refreshContent();
		}
	}

	private void refreshContent() {
		viewer.setInput(getComponents().toArray());
	}

	public Collection<MComponent> getComponents() {
		return importer != null ? importer.getComponents() : Collections.<MComponent>emptyList();
	}

	private String error(String property, String componentURI) {
		return Messages.bind(Messages.ComponentPreviewPage_Error_message, new Object[] { property, componentURI });
	}

	public boolean validatePage() {
		Set<String> messages = new HashSet<String>();
		int messageType = 0;

		Collection<MComponent> components = getComponents();
		if (components == null || components.isEmpty())
			return false;

		Iterator<MComponent> it = components.iterator();
		while (messageType != IMessageProvider.ERROR && it.hasNext()) {
			MComponent c = it.next();
			if (importer.canBeMerged(c)) {
				if (importer.shouldComponentMerged(c)) {
					append(messages, Messages.ComponentPreviewPage_Validate_ComponentOverwrite);
					messageType = IMessageProvider.WARNING;
				} else {
					URI uri = new URIService().createPlatformResource(c);
					URI locatoinFolder = McoreResourceResolver.getLocationFolder(uri);
					if (getComponentMap().containsValue(locatoinFolder)) {
						messageType = IMessageProvider.WARNING;
						append(messages, Messages.ComponentPreviewPage_Validate_ComponentCollision);
					}
				}
			}
			if (c.getDomainType().trim().isEmpty()) {
				messageType = IMessageProvider.ERROR;
				messages.clear();
				append(messages, error(Messages.DomainType_Label, c.getDerivedURI()));
			} else if (c.getDomainName().isEmpty()) {
				messageType = IMessageProvider.ERROR;
				messages.clear();
				append(messages, error(Messages.DomainName_Label, c.getDerivedURI()));
			} else if (c.getName().isEmpty()) {
				messageType = IMessageProvider.ERROR;
				messages.clear();
				append(messages, error(Messages.ComponentName_Label, c.getDerivedURI()));
			}
		}

		it = components.iterator();

		while (messageType != IMessageProvider.ERROR && it.hasNext()) {
			MComponent componentToCompare = it.next();
			Iterator<MComponent> compIt = components.iterator();
			while (compIt.hasNext()) {
				MComponent current = compIt.next();
				if (componentToCompare == current) {
					continue;
				}
				if (RenameService.isEquals(componentToCompare, current)) {
					messageType = IMessageProvider.ERROR;
					append(messages, Messages.ComponentPreviewPage_Validate_URI_Collision);
					break;
				}
			}
		}

		if (!messages.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (String s : messages) {
				if (sb.length() > 0) {
					sb.append(". ");
				}
				sb.append(s);
			}
			setMessage(sb.toString(), messageType);
		} else {
			setMessage(null);
		}

		return messageType != IMessageProvider.ERROR;
	}

	private static void append(Set<String> sb, String s) {
		sb.add(s);
	}

	private void createContent(Composite composite) {
		final Table table = new Table(composite, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			table.setLayoutData(data);
		}
		viewer = new TableViewer(table);

		viewer.setLabelProvider(new LabelProvider() {
			private final Image image = ExtendedImageRegistry.getInstance()
					.getImage(McoreUIPlugin.MCOMPONENT_IMAGE);

			@Override
			public Image getImage(Object element) {
				if (element instanceof MComponent) {
					return image;
				}
				return super.getImage(element);
			}

			@Override
			public String getText(Object element) {
				if (element instanceof MComponent) {
					return ((MComponent) element).getDerivedURI();
				}
				return super.getText(element);
			}
		});
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				TableItem[] items = table.getSelection();
				if (items.length > 0) {
					TableItem selected = items[0];
					MComponent component = (MComponent) selected.getData();
					updateValues(component);
				} else {
					compositeImportValues.setVisible(false);
				}
			}
		});

		createImportValuesArea(composite);
	}

	protected void updateValues(MComponent component) {
		compositeImportValues.setVisible(true);

		domainTypeText.setText(component.getDomainType());
		domainTypeText.setData(new Object[] { component, McorePackage.Literals.MCOMPONENT__DOMAIN_TYPE });
		domainNameText.setText(component.getDomainName());
		domainNameText.setData(new Object[] { component, McorePackage.Literals.MCOMPONENT__DOMAIN_NAME });

		if (component.eIsSet(McorePackage.Literals.MNAMED__SHORT_NAME)) {
			componentNameText.setText(component.getShortName());
			componentNameText.setData(new Object[] { component, McorePackage.Literals.MNAMED__NAME,
					McorePackage.Literals.MNAMED__SHORT_NAME });
		} else {
			componentNameText.setText(component.getName());
			componentNameText.setData(new Object[] { component, McorePackage.Literals.MNAMED__NAME });
		}
		updateReplaceButton(component);
	}

	private void updateReplaceButton(MComponent component) {
		if (importer.canBeMerged(component)) {
			replaceListener.setComponent(component);
			reuseListener.setComponent(component);
			replaceButton.setEnabled(true);
			replaceButton.setSelection(importer.shouldComponentMerged(component));
			reuseButton.setEnabled(true);
			reuseButton.setSelection(!replaceButton.getSelection());
		} else {
			replaceButton.setEnabled(false);
			reuseButton.setEnabled(false);
		}
	}

	private void createImportValuesArea(Composite importModelComposite) {
		compositeImportValues = new Composite(importModelComposite, SWT.NONE);
		GridLayout groupLayout = new GridLayout(2, false);
		groupLayout.verticalSpacing = 8;
		groupLayout.marginTop = 8;

		compositeImportValues.setLayout(groupLayout);
		GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL);
		compositeImportValues.setLayoutData(data);

		domainTypeText = createValueSection(compositeImportValues,
				Messages.DomainType_Label + ":"); //$NON-NLS-1$
		domainNameText = createValueSection(compositeImportValues,
				Messages.DomainName_Label + ":"); //$NON-NLS-1$
		componentNameText = createValueSection(compositeImportValues,
				Messages.ComponentName_Label + ":"); //$NON-NLS-1$

		{
			Composite comp = new Composite(compositeImportValues, SWT.NONE);
			comp.setLayout(new GridLayout(2, true));
			comp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			Composite replace = new Composite(comp, SWT.NONE);
			replace.setLayout(new GridLayout(2, false));
			Label labelReplace = new Label(replace, SWT.NONE);
			labelReplace.setText(Messages.ComponentPreviewPage_ReplaceExistingComponent_Label);

			replaceButton = new Button(replace, SWT.CHECK);
			replaceButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			replaceButton.setEnabled(false);

			replaceListener = new ReplaceButtonMergeListener(replaceButton);
			replaceButton.addSelectionListener(replaceListener);

			Composite reuse = new Composite(comp, SWT.NONE);
			reuse.setLayout(new GridLayout(2, false));
			reuse.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			Label useOriginlabel = new Label(reuse, SWT.NONE);
			useOriginlabel.setText(Messages.ComponentPreviewPage_ReuseExistingComponent_Label);

			reuseButton = new Button(reuse, SWT.CHECK);
			reuseButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			reuseButton.setEnabled(false);
			replaceButton.setSelection(true);

			reuseListener = new ReuseButtonMergeListener(reuseButton);
			reuseButton.addSelectionListener(reuseListener);
			reuseButton.addSelectionListener(new UnselectListener(reuseButton, replaceButton));
			replaceButton.addSelectionListener(new UnselectListener(replaceButton, reuseButton));
		}

		compositeImportValues.setVisible(false);
	}

	private Text createValueSection(Composite parent, String labelText) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(labelText);

		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		text.addModifyListener(change);

		return text;
	}

	private Map<URI, URI> getComponentMap() {
		if (componentMap == null) {
			componentMap = new HashMap<URI, URI>();
			Set<URI> elements = CommonPlugin.computePluginModels(componentMap);
			elements.addAll(CommonPlugin.computeWorkspaceModels(componentMap));
		}
		return componentMap;
	}

	private class ReplaceButtonMergeListener implements SelectionListener {

		private MComponent component;

		private final Button replaceButton;

		public ReplaceButtonMergeListener(Button replaceButton) {
			this.replaceButton = replaceButton;
		}

		public void setComponent(MComponent component) {
			this.component = component;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (importer != null && component != null) {
				if (replaceButton.getSelection()) {
					importer.addMergedComponent(component);
				} else {
					importer.removeMergedComponent(component);
				}
			}
			setPageComplete(validatePage());
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
	}

	private class ReuseButtonMergeListener implements SelectionListener {

		private MComponent component;

		private final Button reuseButton;

		public ReuseButtonMergeListener(Button replaceButton) {
			this.reuseButton = replaceButton;
		}

		public void setComponent(MComponent component) {
			this.component = component;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (importer != null && component != null) {
				if (reuseButton.getSelection()) {
					importer.removeMergedComponent(component);
				} else {
					importer.addMergedComponent(component);
				}
			}
			setPageComplete(validatePage());
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println();
		}
	}

	private static class UnselectListener implements SelectionListener {

		private Button toListen;

		private Button toSelect;

		public UnselectListener(Button toListen, Button toSelect) {
			super();
			this.toListen = toListen;
			this.toSelect = toSelect;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (!toSelect.isEnabled()) {
				return;
			}
			if (toListen.getSelection() && toSelect.getSelection()) {
				toSelect.setSelection(false);
			} else if (!toListen.getSelection() && !toSelect.getSelection()) {
				toSelect.setSelection(true);
			}
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
	}
}
