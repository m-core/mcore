package com.montages.mcore.ui.wizards;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import com.montages.mcore.diagram.part.McoreCreationWizard;

public class AddDiagramWizard extends McoreCreationWizard implements INewWizard {

	public static final String ID = "com.montages.mcore.ui.wizards.AddDiagramWizard";

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		if (selection == null || selection.isEmpty()) {
			selection = new StructuredSelection(ResourcesPlugin.getWorkspace().getRoot());
		}
		super.init(workbench, selection);
	}
}
