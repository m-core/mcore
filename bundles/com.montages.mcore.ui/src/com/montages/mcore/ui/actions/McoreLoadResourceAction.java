package com.montages.mcore.ui.actions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.ui.action.LoadResourceAction;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import com.montages.common.CommonPlugin;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.MTableEditorVisitor;

public class McoreLoadResourceAction extends LoadResourceAction {

	private final ILabelProvider provider = new LabelProvider() {
		@Override
		public String getText(Object element) {
			if (element instanceof URI) {
				URI elementURI = (URI) element;
				if ("mcore".equals(elementURI.fileExtension())) {
					elementURI = elementURI.trimSegments(1);
				}
				return elementURI.toString().replace("mcore://", "http://");
			}
			return super.getText(element);
		};
		@Override
		public Image getImage(Object element) {
			return McoreUIPlugin.MCOMPONENT_IMAGE;
		}
	};

	public McoreLoadResourceAction() {
		super();
	}

	@Override
	public void run() {
		McoreLoadResourceDialog loadResourceDialog =
				new McoreLoadResourceDialog
				(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), provider);

		loadResourceDialog.setFilter("*");
		loadResourceDialog.setMessage("Select a component:");
		loadResourceDialog.setTitle("Select Component");
		loadResourceDialog.setElements(getElements());
		loadResourceDialog.open();

		Object[] result = loadResourceDialog.getResult();
		if (result != null) {
			for (Object nsURI: result) {
				if (nsURI instanceof URI) {
					getEditingDomain().getResourceSet().getResource((URI) nsURI, true);
				}
			}
		}
	}

	private Object[] getElements() {
		ResourceSet rs = getEditingDomain().getResourceSet();
		Map<URI, URI> uriMap = rs.getURIConverter().getURIMap();
		Set<URI> elements = CommonPlugin.computePluginModels(uriMap);
		elements.addAll(CommonPlugin.computeWorkspaceModels(uriMap));
		elements.addAll(computeWorkspaceMTableEditors());

		for (Resource resource: rs.getResources()) {
			URI uri = resource.getURI();

			for (Iterator<URI> it = elements.iterator(); it.hasNext();) {
				URI current = it.next();
				if (current.equals(uri)) { 
					it.remove();
				} else {
					URI normalized = rs.getURIConverter().normalize(current);
					if (uri.equals(normalized)) {
						it.remove();
					}
				}
			}
		}

		return elements.toArray();
	}

	public class McoreLoadResourceDialog extends ElementListSelectionDialog {

		public McoreLoadResourceDialog(Shell parent, ILabelProvider renderer) {
			super(parent, renderer);
		}

	}

	public static Set<URI> computeWorkspaceMTableEditors() {
		final Set<URI> computedWorkspaceModels = new HashSet<URI>();
		final List<IResource> models = new ArrayList<IResource>();
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		MTableEditorVisitor visitor = new MTableEditorVisitor();
		try {
			root.accept(visitor, IResource.FILE);
			models.addAll(visitor.getModels());
		} catch (CoreException e) {
			e.printStackTrace();
		}

		for (IResource res : models) {
			URI locationURI = URI.createPlatformResourceURI(res.getFullPath().toString(), true);
			computedWorkspaceModels.add(locationURI);
		}

		return computedWorkspaceModels;
	}
}
