package com.montages.mcore.ui.workspace;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public class WorkspaceFinder {

	public static List<IResource> findModelsByExtension(IProject[] projects, String extension) {
		final List<IResource> mcores = new ArrayList<IResource>();

		for (IProject project: projects) {
			IFolder modelFolder = project.getFolder("model");

			if (modelFolder != null && modelFolder.isAccessible()) {
				IResource[] members = null;
				IResource member;
				
				try {
					members = modelFolder.members();
				} catch (CoreException e) {
					continue;
				}

				if (members != null) {
					int i = 0;
					boolean added = false;
					while(i < members.length && !added) {
						member = members[i];			
						if (member.getType() == IResource.FILE && extension.equals(member.getFileExtension())) {
							mcores.add(member);
							added = true;
						}
						i++;
					}
				}
			}
		}
		return mcores;
	}
	
	public static List<IResource> findMcores(IProject[] projects) {		
		return findModelsByExtension(projects, "mcore");
	}

	public static List<IResource> findEditorConfigs(IProject[] projects) {
		return findModelsByExtension(projects, "editorconfig");
	}

}
