package com.montages.mcore.ui.handlers;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import com.montages.mcore.MComponent;

public abstract class McoreSelectionHandler extends ResourceSelectionHandler {

	protected abstract Job createJob(MComponent component);

	@Override
	protected void doExcecute(Resource resource, ExecutionEvent event) {
		final MComponent component = getComponent(getRoot(resource));
		if (component != null) {
			final Job job = createJob(component);
			if (job != null) {
				job.setUser(true);
				job.schedule();
			}
		}
	}

	private EObject getRoot(Resource resource) {
		if (resource == null)
			return null;
		else {
			if (!resource.isLoaded()) {
				try {
					resource.load(null);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return resource.getContents().size() == 1 ? resource.getContents().get(0) : null;
		}
	}

	private MComponent getComponent(EObject root) {
		return root instanceof MComponent ? (MComponent) root : null;
	}
}
