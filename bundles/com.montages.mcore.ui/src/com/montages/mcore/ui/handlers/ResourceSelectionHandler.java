package com.montages.mcore.ui.handlers;

import static com.montages.mcore.ui.workspace.FileFunctions.getFileFromResource;
import static com.montages.mcore.ui.workspace.FileFunctions.insureIsSave;
import static org.eclipse.emf.common.util.URI.createPlatformResourceURI;
import static org.eclipse.ui.handlers.HandlerUtil.getActiveMenuSelection;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.montages.common.CommonPlugin;
import com.montages.common.resource.CustomResourceLocator;
import com.montages.mcore.util.McoreResourceFactoryImpl;

public abstract class ResourceSelectionHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final ResourceSet resourceSet = new ResourceSetImpl();
		final IStructuredSelection selection = (IStructuredSelection) getActiveMenuSelection(event);
		final Object firstElement = selection.getFirstElement();

		Resource selectedResource = null;

		if (firstElement instanceof IResource) {
			final IPath locationURI = ((IResource) firstElement).getFullPath();
			selectedResource = resourceSet.createResource(createPlatformResourceURI(locationURI.toString(), true));
		} else if (firstElement instanceof Resource) {
			selectedResource = (Resource) firstElement;
		} else if (firstElement instanceof EObject) {
			selectedResource = ((EObject) firstElement).eResource();
		}

		if (selectedResource != null) {
			selectedResource.getResourceSet()
				.getURIConverter()
				.getURIMap()
				.putAll(CommonPlugin.computeURIMap());
			selectedResource.getResourceSet()
				.getResourceFactoryRegistry()
				.getProtocolToFactoryMap()
				.put("mcore", new McoreResourceFactoryImpl());
			new CustomResourceLocator(
					(ResourceSetImpl) selectedResource.getResourceSet());

			insureIsSave(getFileFromResource(selectedResource));

			doExcecute(selectedResource, event);
		}

		return null;
	}

	protected abstract void doExcecute(Resource selectedResource, ExecutionEvent event);

}
