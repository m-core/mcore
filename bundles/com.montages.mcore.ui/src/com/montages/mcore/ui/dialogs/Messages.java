package com.montages.mcore.ui.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String LOCATION = "com.montages.mcore.ui.dialogs.messages"; //$NON-NLS-1$

	private Messages(){}

	public static String overwriteDialog_Title;

	public static String overwriteDialog_Message;

	static {
		// initialize resource bundle
		NLS.initializeMessages(LOCATION, Messages.class);
	}
}
