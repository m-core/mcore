package com.montages.mcore.ui.views;

import static com.montages.mcore.ui.McoreUIPlugin.openWizard;
import static com.montages.mcore.ui.operations.McoreOperations.GenerateComponentOperation;
import static com.montages.mcore.ui.operations.McoreOperations.GenerateEcoreOperation;
import static com.montages.mcore.ui.operations.McoreOperations.GenerateEditorConfigOperation;
import static com.montages.mcore.ui.operations.McoreOperations.RunComponentOperation;
import static com.montages.mcore.util.ResourceService.createResourceSet;
import static com.montages.mcore.util.ResourceService.normalize;
import static com.montages.mcore.util.ResourceService.transformResourceToPlugin;
import static org.eclipse.jface.dialogs.MessageDialog.openConfirm;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.ui.action.ValidateAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.preference.BooleanPropertyAction;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.montages.common.CommonPlugin;
import com.montages.common.resource.CustomResourceLocator;
import com.montages.common.resource.DiagramResourceResolver;
import com.montages.common.resource.McoreResourceResolver;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.impl.MPropertyImpl;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.dialogs.GenerateEcoreDialog;
import com.montages.mcore.ui.operations.DeleteInternalEcore;
import com.montages.mcore.ui.operations.McoreOperations;
import com.montages.mcore.ui.operations.McoreOperations.Options;
import com.montages.mcore.ui.wizards.CreateComponentWizard;
import com.montages.mcore.ui.wizards.ExportRepositoryWizard;
import com.montages.mcore.ui.wizards.ExportRepositoryWizardData;
import com.montages.mcore.ui.wizards.ImportRepositoryWizard;
import com.montages.mcore.ui.wizards.InitDiagramWizard;
import com.montages.mcore.ui.workspace.FileFunctions;
import com.montages.mcore.util.DiagramLabelProvider;
import com.montages.mcore.util.MComponentExplorerResourceProxyVisitor;
import com.montages.mcore.util.McoreResourceFactoryImpl;
import com.montages.mcore.util.McoreUtil;

/**
 * View showing the list of components available in the workspace and in the plugin registry.
 * 
 */
public class MComponentExplorer extends AbstractTableExplorer<Tree, TreeViewer> {

	public static final String ID = "com.montages.mcore.ui.views.MComponentExplorer"; //$NON-NLS-1$
	public static final String SHOW_REPOSITORY_COMPONENTS_ID = ID + ".show.repository.components"; //$NON-NLS-1$

	private Action addComponentAction;
	
	private Action showHideRepoComponentsAction;

	private Action addDiagramAction;

	private Action importAction;

	private Action exportAction;

	private Action runAction;

	private Action generateAction;

	private Action generateEcoreAction;

	private Action generateEditAction;

	private Action cleanAction;

	private Action deleteAction;

	private Action deleteEcoreAction;

	private Action generateUpdateSiteAction;

	private Action checkOutFromRepoAction;

	private final Map<URI, URI> componentMap = new HashMap<URI, URI>();

	private final URIConverter converter = new ExtensibleURIConverterImpl();

	private final Map<URI, URI> diagramToComponentMap = new HashMap<URI, URI>();
	
	private final Map<URI, Set<URI>> contained2Containing = new HashMap<>();

	private final ExportRepositoryWizardData exportRepositoryWizardData;

	private final Listener openListener = new Listener() {

		@Override
		public void handleEvent(Event event) {
			if (event.keyCode == SWT.CR || event.keyCode == SWT.LF || event.type == SWT.MouseDoubleClick) {
				openEditor(null);
			}
		}
	};


	private void openEditor(String editorID) {
		final TreeItem[] selected = getSelection();

		for (TreeItem element: selected) {
			final URI uri = (URI) element.getData();
			final String ID = editorID == null ? getEditorName(uri) : editorID;

			if (ID == null) {
				return;
			}

			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {
					final IWorkbenchWindow workbenchWindow = getSite().getWorkbenchWindow();
					final IWorkbenchPage page = workbenchWindow.getActivePage();
					URI normalized = componentMap.get(uri);

					try {
						page.openEditor(new URIEditorInput(normalized == null ? uri : normalized), ID);
					} catch (PartInitException e) {
						McoreUIPlugin.log(e);
					}
				}
			});
		}
	}

	private String getEditorName(URI uri) {
		if (uri == null) {
			return null;
		}
		if (isMcoreURI(uri)) {
			return "com.montages.mcore.presentation.McoreEditorID";
		}
		if (McoreUtil.isDiagramURI(uri)) {
			return "com.montages.mcore.diagram.part.McoreDiagramEditorID";
		}
		return null;
	}

	public MComponentExplorer() {
		exportRepositoryWizardData = new ExportRepositoryWizardData();
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);

		Object input = getElements();
		getViewer().setLabelProvider(getLabelProvider());
		getViewer().setContentProvider(new ComponentExplorerContentProvider(diagramToComponentMap, converter));
		getViewer().setComparator(new ComponentComparator());
		getViewer().addFilter(getFilter());
		getViewer().getTree().addListener(SWT.MouseDoubleClick, openListener);
		getViewer().getTree().addListener(SWT.KeyDown, openListener);
		getViewer().setInput(input);
		getViewer().addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				boolean workspaceAction = isSelectionInWorkspace();
				boolean isDiagramSelected = !getListOfDiagramURIs().isEmpty();
				boolean workspaceActionWithoutDiagram = workspaceAction && !isDiagramSelected;
				
				runAction.setEnabled(workspaceActionWithoutDiagram);
				addDiagramAction.setEnabled(workspaceActionWithoutDiagram);
				generateAction.setEnabled(workspaceActionWithoutDiagram);
				generateEcoreAction.setEnabled(workspaceActionWithoutDiagram);
				generateEditAction.setEnabled(workspaceActionWithoutDiagram);
				cleanAction.setEnabled(workspaceActionWithoutDiagram);
				deleteAction.setEnabled(workspaceAction);
				deleteEcoreAction.setEnabled(workspaceActionWithoutDiagram);
				exportAction.setEnabled(!isDiagramSelected);
				generateUpdateSiteAction.setEnabled(workspaceActionWithoutDiagram);
				checkOutFromRepoAction.setEnabled(componentsExistInPlugin() && !isDiagramSelected);
			}
		});
		initializeDnD(getViewer());
	}

	@Override
	protected TreeViewer createVeiwer(Tree parent) {
		return new TreeViewer(parent);
	}

	@Override
	protected Tree createViewerComposete(Composite parent) {
		final Tree tree = new Tree(parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			tree.setLayoutData(data);
		}
		return tree;
	}

	private ILabelProvider getLabelProvider() {
		CompositeLabelProvider compositeLabelProvider = new CompositeLabelProvider();
		compositeLabelProvider.addProvider(new ComponentsLabelProvider());
		compositeLabelProvider.addProvider(new DiagramLabelProvider());
		return compositeLabelProvider;
	}

	@Override
	protected Object getElements() {
		componentMap.clear();
		diagramToComponentMap.clear();
		converter.getURIMap().clear();
		contained2Containing.clear();
		/// collect components
		Set<URI> elements = new HashSet<URI>();
		boolean showRepository = McoreUIPlugin.getInstance().getPreferenceStore().getBoolean(SHOW_REPOSITORY_COMPONENTS_ID);
		if (showRepository) {
			elements.addAll(CommonPlugin.computePluginModels(componentMap));
		}
		Set<URI> workspaceModels = McoreResourceResolver.computeWorkspaceModels(componentMap, new MComponentExplorerResourceProxyVisitor());
		elements.addAll(workspaceModels);
		/// collect diagrams
		diagramToComponentMap.putAll(DiagramResourceResolver.getInstance().collectWorkspaceResource());
		elements.addAll(diagramToComponentMap.keySet());

		converter.getURIMap().putAll(componentMap);
		
		/// map workspace contained resources
		Map<URI, URI> componentMapInverse = inverseMapKeyValue(componentMap);
		ResourceSet resourceSet = getResourceSet();		
		new ArrayList<>(workspaceModels).forEach(parentURI -> {
			MComponent mComponent = loadComponent(parentURI, resourceSet);
			mComponent.getOwnedPackage().get(0).getClassifier().forEach(classifier -> {
				List<MProperty> containmentProps = classifier.allContainmentReferences();
				containmentProps.stream()
					.map(MComponentExplorer::getContainmentComponent).map(MComponent::eResource)
					.map(Resource::getURI)
					.map(componentMapInverse::get)
					.filter(childURI -> !Objects.equals(childURI, parentURI))
					.forEach(childURI -> updateContained2Containing(childURI, parentURI));
			});
		});
		
		return elements.toArray();
	}
	
	private void updateContained2Containing(URI containmentURI, URI containingURI) {
		if (!contained2Containing.containsKey(containmentURI)) {
			contained2Containing.putIfAbsent(containmentURI, new HashSet<URI>());
		}
		contained2Containing.get(containmentURI).add(containingURI);
	}
	
	private static MComponent getContainmentComponent(MProperty property) {
		MClassifier type = Optional.ofNullable(property)
				.filter(MPropertyImpl.class::isInstance).map(MPropertyImpl.class::cast)
				.map(MPropertyImpl::getType)
				.orElse(null);
		return findContainerComponent(type);
	}
	
	private static MComponent findContainerComponent(EObject eObject) {
		EObject parent = eObject == null ? null : eObject.eContainer();
		return parent == null || parent instanceof MComponent 
				? (MComponent) parent : findContainerComponent(parent);
	}
	
	private static Map<URI, URI> inverseMapKeyValue(Map<URI, URI> map) {
		return map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
	}

	/**
	 * Create the actions.
	 */
	@Override
	protected void createActions() {
		addComponentAction = new Action("New Component", McoreUIPlugin.getImage("image_add")) {

			@Override
			public void run() {
				openWizard(CreateComponentWizard.ID, PlatformUI.getWorkbench().getNewWizardRegistry());
			}
		};
		addDiagramAction = new Action("New Diagram for this Component", McoreUIPlugin.getImage("image_diagram_add")) {

			@Override
			public void run() {
				openWizard(InitDiagramWizard.ID, PlatformUI.getWorkbench().getNewWizardRegistry(), getMcoreURISelection());
			}

			private IStructuredSelection getMcoreURISelection() {
				URI uri = getSelectedURI();
				if (!isMcoreURI(uri)) {
					return null;
				}
				return new StructuredSelection(getNormalizedURI(uri));
			}

			@Override
			public boolean isEnabled() {
				IStructuredSelection selectedURI = getMcoreURISelection();
				return selectedURI != null && ((URI)selectedURI.getFirstElement()).isPlatformResource();
			}
		};
		importAction = new Action("Import", McoreUIPlugin.getImage("image_import")) {

			public void run() {
				openWizard(ImportRepositoryWizard.ID, PlatformUI.getWorkbench().getImportWizardRegistry());
			};
		};
		exportAction = new Action("Export", McoreUIPlugin.getImage("image_export")) {

			public void run() {
				exportRepositoryWizardData.setViewerSelection((IStructuredSelection) getViewer().getSelection());
				openWizard(ExportRepositoryWizard.ID, PlatformUI.getWorkbench().getExportWizardRegistry(), new StructuredSelection(exportRepositoryWizardData));
			};
		};
		generateAction = new Action("Bundles", McoreUIPlugin.getImage("image_gen")) {

			@Override
			public void run() {
				final MComponent component = getSelectedComponent();
				if (component == null)
					return;

				Job job = new Job("Mcore Code Generator") {

					protected IStatus run(IProgressMonitor monitor) {
						return GenerateComponentOperation.execute(component, Collections.singletonMap(Options.FORCE_CODE_GENERATION, true), monitor);
					};
				};
				job.setUser(true);
				job.schedule();
			}
		};
		generateEcoreAction = new Action("Generate", McoreUIPlugin.getImage("image_ecore")) {

			public void run() {
				final MComponent component = getSelectedComponent();
				if (component == null)
					return;

				final GenerateEcoreDialog dialog = new GenerateEcoreDialog(getSite().getShell(), component);
				dialog.create();

				if (dialog.open() == Window.OK) {
					final boolean force = dialog.isForceGeneration();
					final List<MComponent> targets = dialog.getTargets();

					Job job = new Job("Generate Ecore") {

						protected IStatus run(IProgressMonitor monitor) {
							return GenerateEcoreOperation.execute(component, targets, Collections.singletonMap(Options.FORCE_ECORE_SAVE, force), monitor);
						};
					};
					job.setUser(true);
					job.schedule();
				}
			};
		};
		generateEditAction = new Action("Editor Config", McoreUIPlugin.getImage("image_table")) {

			public void run() {
				final MComponent component = getSelectedComponent();
				if (component == null)
					return;

				Job job = new Job("Generate Editor Config") {

					protected IStatus run(IProgressMonitor monitor) {
						return GenerateEditorConfigOperation.execute(component, Collections.<Options, Boolean> emptyMap(), monitor);
					};
				};
				job.setUser(true);
				job.schedule();
			}
		};
		runAction = new Action("Run", McoreUIPlugin.getImage("image_run")) {

			public void run() {
				final MComponent component = getSelectedComponent();
				if (component == null)
					return;

				Job job = new Job("Mcore Runtime") {

					protected IStatus run(IProgressMonitor monitor) {
						return RunComponentOperation.execute(component, Collections.singletonMap(Options.FORCE_CODE_GENERATION, true), monitor);
					};
				};
				job.setUser(true);
				job.schedule();
			}
		};
		cleanAction = new Action("Clean", McoreUIPlugin.getImage("image_clean")) {

			public void run() {
				final MComponent component = getSelectedComponent();
				if (component == null)
					return;

				boolean confirm = openConfirm(getSite().getShell(), "Clean Component", "Are you sure you want to clean this component?");
				if (confirm) {
					Job job = new Job("Clean Project") {

						protected IStatus run(IProgressMonitor monitor) {
							return McoreOperations.CleanOperation.execute(component, Collections.<Options, Boolean> emptyMap(), monitor);
						};
					};
					job.setUser(true);
					job.schedule();
				}
			}
		};
		deleteAction = new Action("Delete", McoreUIPlugin.getImage("image_delete")) {

			public void run() {
				final List<MComponent> components = getListOfSelectedComponents();
				final List<URI> diagrams = getListOfDiagramURIs();
				boolean confirm = openConfirm(getSite().getShell(), "Delete Component", "Are you sure you want to delete this component?");
				if (confirm) {
					Job job = new Job("Delete Project") {

						protected IStatus run(IProgressMonitor monitor) {
							for (URI uri : diagrams) {
								IFile diagram = FileFunctions.getFile(uri);
								try {
									diagram.delete(true, monitor);
								} catch (CoreException e) {
									McoreUIPlugin.log(e);
								}
							}
							return McoreOperations.DeleteOperation.execute(null, components, Collections.<Options, Boolean> emptyMap(), monitor);
						};
					};
					job.setUser(true);
					job.schedule();
				}
			};
		};
		deleteEcoreAction = new Action("Delete", McoreUIPlugin.getImage("image_delete")) {

			public void run() {
				final List<MComponent> components = getListOfSelectedComponents();

				boolean confirm = openConfirm(getSite().getShell(), "Delete Ecore References", "Are you sure you want to delete references to ecore files?");
				if (confirm) {
					Job job = new Job("Delete Ecore") {

						protected IStatus run(IProgressMonitor monitor) {
							DeleteInternalEcore.execute(components, monitor);
							return Status.OK_STATUS;
						};
					};
					job.setUser(true);
					job.schedule();
				}
			};
		};
		generateUpdateSiteAction = new Action("Build Update Site") {

			@Override
			public void run() {
				final List<MComponent> components = getListOfSelectedComponents();
				Job job = new Job("Build update site") {

					protected IStatus run(IProgressMonitor monitor) {
						return McoreOperations.BuildUpdateSiteOperation.execute(null, components, Collections.<Options, Boolean> emptyMap(), monitor);
					};
				};
				job.setUser(true);
				job.schedule();
			}

		};
		checkOutFromRepoAction = new Action("Extract Sources to Workspace") {
			
			public void run() {
				final List<MComponent> components = getListOfSelectedComponents();
				Job job = new Job("Extract Sources to Workspace") {

					protected IStatus run(IProgressMonitor monitor) {
						IStatus status = Status.OK_STATUS;
						for (MComponent component: components) {
							if (!McoreOperations.CheckOutOperation.execute(component, null, monitor).isOK()) {
								status = Status.CANCEL_STATUS;
							}
						}
						return status;
					};
				};
				job.setUser(true);
				job.schedule();
			};
		};
		showHideRepoComponentsAction = new BooleanPropertyAction("show MCoponents from local repository", McoreUIPlugin.getInstance().getPreferenceStore(), SHOW_REPOSITORY_COMPONENTS_ID) { //$NON-NLS-1$
			
			@Override
			public void run() {
				super.run();
				refreshComponentsList();
			}
		};
	}
	
	private void refreshComponentsList() {
		getViewer().setInput(getElements());
	}

	/**
	 * Initialize the toolbar.
	 */
	@Override
	protected void initializeToolBar() {
		IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
		tbm.add(addComponentAction);
		tbm.add(importAction);
		tbm.add(exportAction);
		IMenuManager tMenu = getViewSite().getActionBars().getMenuManager();
		tMenu.add(showHideRepoComponentsAction); //$NON-NLS-1$
	}

	@Override
	protected void createContextMenu() {
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {

			public void menuAboutToShow(IMenuManager mgr) {
				mgr.removeAll();
				fillContextMenu(mgr);
			}
		});

		// Create menu.
		Menu menu = menuMgr.createContextMenu(getViewer().getControl());
		getViewer().getControl().setMenu(menu);

		// Register menu for extension.
		getSite().registerContextMenu(menuMgr, getViewer());
		ResourcesPlugin.getWorkspace().addResourceChangeListener(new ComponentsWorkspaceListener());
	}

	private void fillContextMenu(IMenuManager mgr) {
		URI uri = getSelectedURI();
		mgr.setRemoveAllWhenShown(true);

		if (isMcoreURI(uri)) {
			mgr.add(new Action("Open") {

				@Override
				public void run() {
					openEditor(null);
				}
			});
			mgr.add(new Separator());
			mgr.add(addDiagramAction);
			mgr.add(new Separator());
			mgr.add(runAction);
			mgr.add(new Separator());
			mgr.add(new ValidateAction());
			mgr.add(new Separator());

			MenuManager generateMenu = new MenuManager("Generate");
			generateMenu.add(generateAction);
			generateMenu.add(generateEditAction);
			mgr.add(generateMenu);

			MenuManager ecoreMenu = new MenuManager("Ecore");
			ecoreMenu.add(generateEcoreAction);
			ecoreMenu.add(deleteEcoreAction);
			mgr.add(ecoreMenu);

			mgr.add(new Separator());
			mgr.add(importAction);
			mgr.add(exportAction);
			mgr.add(new Separator());
			mgr.add(cleanAction);
			mgr.add(deleteAction);
			mgr.add(generateUpdateSiteAction);
			mgr.add(checkOutFromRepoAction);
		} else {
			mgr.add(new Action("Open") {

				@Override
				public void run() {
					openEditor(null);
				}
			});
			mgr.add(deleteAction);
		}
	}

	/**
	 * Initialize the menu.
	 */
	protected void initializeMenu() {
		//		IMenuManager manager = getViewSite().getActionBars().getMenuManager();
		//		manager.add(addAction);
	}

	private boolean isSelectionInWorkspace() {
		TreeItem[] items = getSelection();
		if (items.length == 0)
			return false;

		boolean allInWorkspace = true;

		for (TreeItem selected : items) {
			URI uri = (URI) selected.getData();
			URIConverter converter = new ExtensibleURIConverterImpl();
			converter.getURIMap().putAll(componentMap);
			URI normalized = converter.normalize(uri);
			allInWorkspace = allInWorkspace && normalized.isPlatformResource();
		}

		return allInWorkspace;
	}

	protected void initializeDnD(TreeViewer viewer) {
		int dndOperations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		Transfer[] dragTransfers = new Transfer[] { FileTransfer.getInstance() };
		Transfer[] dropTransfers = new Transfer[] { FileTransfer.getInstance() };
		viewer.addDragSupport(dndOperations, dragTransfers, new MCompExplorerDragListener(this));
		viewer.addDropSupport(dndOperations, dropTransfers, new ViewerDropAdapter(viewer) {

			@Override
			public void drop(DropTargetEvent event) {
				super.drop(event);
				if (checkData(event.data)) {
					openWizard(ImportRepositoryWizard.ID, PlatformUI.getWorkbench().getImportWizardRegistry(), new StructuredSelection(event.data));
				}
			}

			@Override
			public boolean performDrop(Object data) {
				return checkData(data);
			}

			private boolean checkData(Object data) {
				if (false == data instanceof String[]) {
					return false;
				}
				String[] inputPaths = (String[]) data;
				if (inputPaths.length == 0) {
					return false;
				}
				String mcoreTempDirPath = McoreUIPlugin.getInstance().getMCoreTempFolderPath().replace("/", File.separator);
				for (String input : inputPaths) {
					if (input.startsWith(mcoreTempDirPath)) {
						return false;
					}
				}
				return true;
			}

			@Override
			public boolean validateDrop(Object target, int operation, TransferData transferType) {
				return FileTransfer.getInstance().isSupportedType(transferType);
			}

		});
	}

	protected URI getNormalizedURI(URI uri) {
		return componentMap == null ? null : componentMap.get(uri);
	}

	public List<MComponent> getListOfSelectedComponents() {
		List<MComponent> list = new ArrayList<MComponent>();
		TreeItem[] items = getSelection();

		ResourceSet resourceSet = getResourceSet();
		for (TreeItem item : items) {
			URI uri = (URI) item.getData();
			if (!isMcoreURI(uri)) {
				continue;
			}
			URI normalized = resourceSet.getURIConverter().normalize(uri);

			MComponent c = loadComponent(normalized, resourceSet);
			if (c != null) {
				list.add(c);
			}
		}

		return list;
	}

	public List<URI> getListOfDiagramURIs() {
		List<URI> list = new ArrayList<URI>();
		TreeItem[] items = getSelection();

		for (TreeItem item : items) {
			URI uri = (URI) item.getData();
			if (!McoreUtil.isDiagramURI(uri)) {
				continue;
			}
			list.add(uri);
		}
		return list;
	}

	public MComponent getSelectedComponent() {
		URI uri = getSelectedURI();

		if (!isMcoreURI(uri)) {
			return null;
		}

		ResourceSet resourceSet = getResourceSet();
		URI normalized = normalize(resourceSet, uri);
		return loadComponent(normalized, resourceSet);
	}

	private URI getSelectedURI() {
		URI[] selectedURIs = getSelectedURIs();
		return selectedURIs.length == 0 ? null : selectedURIs[0];
	}

	TreeItem[] getSelection() {
		return getViewer().getTree().getSelection();
	}

	protected ResourceSet getResourceSet() {
		ResourceSet resourceSet = createResourceSet();
		resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().put("mcore", new McoreResourceFactoryImpl());
		new CustomResourceLocator((ResourceSetImpl) resourceSet);

		return resourceSet;
	}

	protected MComponent loadComponent(URI normalizedURI, ResourceSet resourceSet) {
		// Make sure file is saved before loading it
		FileFunctions.insureIsSave(FileFunctions.getFile(normalizedURI));

		Resource resource = resourceSet.getResource(normalizedURI, true);
		if (resource == null || resource.getContents().isEmpty()) {
			return null;
		}

		EObject root = resource.getContents().get(0);
		if (root instanceof MComponent) {
			return (MComponent) root;
		} else if (root instanceof MRepository) {
			return ((MRepository) root).getComponent().get(0);
		} else {
			return null;
		}
	}

	public static class CompositeLabelProvider implements ILabelProvider, IColorProvider {

		private List<ILabelProvider> myInnerProviders;

		private List<ILabelProviderListener> myListeners;

		public CompositeLabelProvider() {
			myInnerProviders = new ArrayList<ILabelProvider>();
			myListeners = new ArrayList<ILabelProviderListener>();
		}

		public void addProvider(ILabelProvider provider) {
			if (provider == null) {
				return;
			}
			myInnerProviders.add(provider);
		}

		@Override
		public void addListener(ILabelProviderListener listener) {
			if (listener == null) {
				return;
			}
			myListeners.add(listener);
			for (ILabelProvider innerProvider: myInnerProviders) {
				innerProvider.addListener(listener);
			}
		}

		@Override
		public void dispose() {
			for (ILabelProvider innerProvider: myInnerProviders) {
				for (ILabelProviderListener listener: myListeners) {
					innerProvider.removeListener(listener);
				}
			}
			myInnerProviders.clear();
			myListeners.clear();
			myInnerProviders = null;
			myListeners = null;
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			if (listener == null) {
				return;
			}
			myListeners.remove(listener);
			for (ILabelProvider innerProvider: myInnerProviders) {
				innerProvider.removeListener(listener);
			}
		}

		@Override
		public Image getImage(Object element) {
			for (ILabelProvider innerProvider: myInnerProviders) {
				Image im = innerProvider.getImage(element);
				if (im != null) {
					return im;
				}
			}
			return null;
		}

		@Override
		public String getText(Object element) {
			for (ILabelProvider innerProvider: myInnerProviders) {
				String text = innerProvider.getText(element);
				if (text != null) {
					return text;
				}
			}
			return null;
		}
		
		@Override
		public Color getForeground(Object element) {
			return myInnerProviders.stream()
				.filter(IColorProvider.class::isInstance).map(IColorProvider.class::cast)
				.findFirst().map(colorProvider -> colorProvider.getForeground(element))
				.orElse(null);
		}

		@Override
		public Color getBackground(Object element) {
			return null;
		}
	}

	public class ComponentsLabelProvider extends StyledCellLabelProvider implements ILabelProvider,	IColorProvider  {

		private final URIConverter converter = new ExtensibleURIConverterImpl();

		@Override
		public Image getImage(Object element) {
			if (element instanceof URI) {
				URI elementURI = (URI) element;
				if (!isMcoreURI(elementURI)) {
					return null;
				}
				return McoreUIPlugin.MCOMPONENT_IMAGE;
			}
			return null;
		}

		@Override
		public String getText(Object element) {
			if (element instanceof URI) {
				URI elementURI = (URI) element;
				converter.getURIMap().putAll(componentMap);
				URI normaized = converter.normalize(elementURI);
				if (!isMcoreURI(elementURI)) {
					return null;
				}
				if (elementURI.fileExtension() != null) {
					elementURI = elementURI.trimSegments(1);
				}

				return elementURI.toString() + (normaized.isPlatformResource() ? " [workspace]" : " [repository]");
			}
			return null;
		}

		@Override
		public Color getForeground(Object element) {
			return contained2Containing.get(element) == null ? null : org.eclipse.draw2d.ColorConstants.lightGray;
		}

		@Override
		public Color getBackground(Object element) {
			return null;
		}
	}

	protected static boolean isMcoreURI(URI elementURI) {
		return elementURI!= null && "mcore".equals(elementURI.fileExtension());
	}

	private URI[] getSelectedURIs() {
		TreeItem[] items = getSelection();
		URI[] uris = new URI[items.length];
		for (int i = 0; i < items.length; i++) {
			uris[i] = (URI)items[i].getData();
		}
		return uris;
	}

	private boolean componentsExistInPlugin() {
		URI[] selectedURIs = getSelectedURIs();
		if (selectedURIs.length == 0) {
			return false;
		}
		for (URI uri : selectedURIs) {
			uri = normalize(getResourceSet(), uri);
			if (!uri.isPlatform()) {
				return false;
			}
			if (uri.isPlatformResource()) {
				uri = transformResourceToPlugin(uri);
			}

			Resource createResource = getResourceSet().getResource(uri, true);
			if (createResource == null) {
				return false;
			}
		}
		return true;
	}

	public class ComponentComparator extends ViewerComparator {

		private final URIConverter converter = new ExtensibleURIConverterImpl();

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			converter.getURIMap().putAll(componentMap);
			if (false == e1 instanceof URI || false == e2 instanceof URI) {
				return 0;
			}
			
			URI left = (URI) e1;
			URI right = (URI) e2;
			
			if (isMcoreURI(left)) {
				return isMcoreURI(right) 
						? compareMcoreWithMcore(left, right)
						: ~compareDiagramWithMcore(right, left);
			} else {
				return isMcoreURI(right) ? compareDiagramWithMcore(left, right) : compareDiagramWithDiagram(left, right);
			}
		}

		protected int compareDiagramWithMcore(URI diagramLeft, URI mcoreRight) {
			URI mcoreLeft = diagramToComponentMap.get(diagramLeft);
			if (mcoreLeft == null) {
				return -1;
			}
			return compareMcoreWithMcore(mcoreLeft, mcoreRight);
		}

		protected int compareMcoreWithMcore(URI mcoreLeft, URI mcoreRight) {
			URI mcoreNormilizedLeft = converter.normalize(mcoreLeft);
			URI mcoreNormilizedRight = converter.normalize(mcoreRight);
			if (mcoreNormilizedLeft.isPlatformPlugin() && mcoreNormilizedRight.isPlatformResource()) {
				return 1;
			}
			if (mcoreNormilizedLeft.isPlatformResource() && mcoreNormilizedRight.isPlatformPlugin()) {
				return -1;
			}
			int contained2ContainingComparingRes = compareContained2ContainingComponent(mcoreLeft, mcoreRight);
			return contained2ContainingComparingRes == 0 
					? mcoreNormilizedLeft.toString().compareTo(mcoreNormilizedRight.toString()) 
					: contained2ContainingComparingRes;
		}

		protected int compareDiagramWithDiagram(URI diagramLeft, URI diagramRight) {
			URI leftMcore = diagramToComponentMap.get(diagramLeft);
			URI rightMcore = diagramToComponentMap.get(diagramRight);
			if (leftMcore == null && rightMcore == null) {
				return 0;
			}
			if (leftMcore == null) {
				return -1;
			}
			if (rightMcore == null) {
				return 1;
			}
			if (!leftMcore.equals(rightMcore)) {
				return compareMcoreWithMcore(leftMcore, rightMcore);
			}
			return diagramLeft.toString().compareTo(diagramRight.toString());
		}
		
		private int compareContained2ContainingComponent(URI mcoreLeft, URI mcoreRight) {
			boolean leftInsideRight = isChildComponent(mcoreLeft, mcoreRight);
			boolean rightInsideLeft = isChildComponent(mcoreRight, mcoreLeft);
			return leftInsideRight == rightInsideLeft ? 0 : leftInsideRight ? 1 : -1;
		}
		
		private boolean isChildComponent(URI mcoreLeft, URI mcoreRight) {
			return Optional.ofNullable(contained2Containing.get(mcoreLeft))
						.filter(leftSet -> leftSet.contains(mcoreRight)).isPresent();
		}
	}

	public class ComponentsWorkspaceListener implements IResourceChangeListener {

		private final IResourceDeltaVisitor visitor = new IResourceDeltaVisitor() {

			@Override
			public boolean visit(IResourceDelta delta) throws CoreException {
				if ("mcore".equals(delta.getResource().getFileExtension()) && delta.getKind() == IResourceDelta.ADDED || delta.getKind() == IResourceDelta.REMOVED
						|| delta.getKind() == IResourceDelta.CHANGED) {

					Display display = PlatformUI.getWorkbench().getDisplay();
					if (!display.isDisposed()) {
						display.asyncExec(new Runnable() {

							@Override
							public void run() {
								if (!getViewer().getControl().isDisposed()) {
									refreshComponentsList();
								}
							}
						});
					}
				}
				return false;
			}
		};
		
		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			if (event != null && event.getDelta() != null) {
				try {
					event.getDelta().accept(visitor);
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static class ComponentExplorerContentProvider implements ITreeContentProvider {

		private Map<URI, URI> myDiagramToComponentMap;

		private URIConverter converter;

		public ComponentExplorerContentProvider(Map<URI, URI> diagramToComponentMap, URIConverter componentURIMap) {
			myDiagramToComponentMap = diagramToComponentMap;
			converter = componentURIMap;
		}

		@Override
		public void dispose() {
			myDiagramToComponentMap = null;
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement == null) {
				return new Object[0];
			}
			Object[] inputElements = inputElement instanceof Object[] ? (Object[]) inputElement : new Object[]{inputElement};
			return collectElements(inputElements);
		}

		private Object[] collectElements(Object[] inputElements) {
			ArrayList<Object> result = new ArrayList<Object>();
			for (Object element: inputElements) {
				if (element instanceof URI && MComponentExplorer.isMcoreURI((URI) element)) {
					result.add(element);
				}
			}
			return result.toArray();
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			if (false == parentElement instanceof URI) {
				return new Object[0];
			}
			return collectRelatedDiagrams(converter.normalize((URI)parentElement)).toArray();
		}

		@Override
		public Object getParent(Object element) {
			if (false == element instanceof URI) {
				return null;
			}
			URI uri = (URI) element;
			return myDiagramToComponentMap.get(uri);
		}

		@Override
		public boolean hasChildren(Object element) {
			if (false == element instanceof URI) {
				return false;
			}
			URI potentialParent = (URI) element;
			
			return ! collectRelatedDiagrams(converter.normalize((URI)potentialParent)).isEmpty();
		}

		
		private List<URI> collectRelatedDiagrams(URI mcoreURI) {
			if (!MComponentExplorer.isMcoreURI(mcoreURI)) {
				return Collections.emptyList();
			}
			ArrayList<URI> result = new ArrayList<URI>();
			for (URI diagramURI: myDiagramToComponentMap.keySet()) {
				URI relatedMcoreURI = myDiagramToComponentMap.get(diagramURI);
				if (mcoreURI.equals(relatedMcoreURI)) {
					result.add(diagramURI);
				}
			}
			return result;
		}
	}
}
