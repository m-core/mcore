package com.montages.mcore.ui.views;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;

/**
 * View providing a search box and a table viewer.
 * 
 */
public abstract class AbstractTableExplorer<C extends Composite, V extends Viewer> extends ViewPart {

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private V viewer;
	private C viewerComposite;
	private ExplorerFilter filter;

	@Override
	public void createPartControl(Composite parent) {
		Composite container = toolkit.createComposite(parent, SWT.NONE);
		toolkit.paintBordersFor(container);
		{
			GridLayout layout = new GridLayout(1, true);
			container.setLayout(layout);
		}

		Composite top = toolkit.createComposite(container, SWT.NONE);
		{
			GridLayout layout = new GridLayout(1, false);
			top.setLayout(layout);
			top.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		{
			final Text searchText = new Text(top, SWT.SEARCH | SWT.ICON_SEARCH | SWT.ICON_CANCEL);
			searchText.setMessage("Filter");
			searchText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

			searchText.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent ke) {
					getFilter().setSearchText(searchText.getText());
					viewer.refresh();
				}
			});
		}
		Composite main = toolkit.createComposite(container, SWT.NONE);
		{
			GridLayout layout = new GridLayout(1, true);
			layout.horizontalSpacing = 15;
			layout.verticalSpacing = 15;
			main.setLayout(layout);
			main.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		}

		viewerComposite = createViewerComposete(main);

		viewer = createVeiwer(viewerComposite);

		createActions();
		initializeToolBar();
		initializeMenu();
		createContextMenu();
	}

	public void dispose() {
		toolkit.dispose();
		super.dispose();
	}

	protected abstract Object getElements();

	protected abstract V createVeiwer(C parent);

	protected abstract C createViewerComposete(Composite parent);

	protected V getViewer() {
		return viewer;
	}

	protected ExplorerFilter getFilter() {
		if (filter == null) {
			filter = new ExplorerFilter();
		}
		return filter;
	}

	protected void createActions() {}

	protected void initializeToolBar() {}

	protected void initializeMenu() {}

	protected void createContextMenu() {}

	@Override
	public void setFocus() {
		if (viewerComposite != null) {
			
		}
		viewerComposite.setFocus();
	}
	
	public class ExplorerFilter extends ViewerFilter {

		private String searchString;

		public void setSearchText(String s) { 
			this.searchString = ".*" + s + ".*";
		}

		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
			if (searchString == null || searchString.length() == 0) {
				return true;
			}
			URI uri = (URI) element;
			if (uri.toString().matches(searchString)) {
				return true;
			}
			return false;
		}

	}

}
