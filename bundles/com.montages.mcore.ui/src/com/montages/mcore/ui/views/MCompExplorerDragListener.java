package com.montages.mcore.ui.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.FileTransfer;

import com.montages.mcore.codegen.ui.conversions.Exporter;
import com.montages.mcore.ui.McoreUIPlugin;

public class MCompExplorerDragListener implements DragSourceListener {

	private MComponentExplorer myExplorer;

	private File myTempMCoreDir;

	public MCompExplorerDragListener(MComponentExplorer explorer) {
		myExplorer = explorer;
	}

	@Override
	public void dragStart(DragSourceEvent event) {
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		if (!FileTransfer.getInstance().isSupportedType(event.dataType))
			return;

		IStructuredSelection selection = (IStructuredSelection) myExplorer.getViewer().getSelection();

		List<URI> toDrop = collectMCoreURIs(selection);
		List<String> files = new ArrayList<String>();
		for (URI uri : toDrop) {

			try {
				File mcoreDir = getMcoreTemDir();
				Exporter exporter = new Exporter(uri);
				exporter.setDiagramToExport(exporter.getAllDiagrams());

				File exportedFile = exporter.exportMCore(mcoreDir.getAbsolutePath());

				files.add(exportedFile.getAbsolutePath());
			} catch (Exception e) {
				McoreUIPlugin.log(e);
			}
		}
		event.data = files.toArray(new String[files.size()]);
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
		if (myTempMCoreDir != null) {
			myTempMCoreDir.delete();
		}
	}

	protected List<URI> collectMCoreURIs(IStructuredSelection selection) {
		List<URI> result = new ArrayList<URI>();
		Iterator<?> it = selection.iterator();
		while (it.hasNext()) {
			Object next = it.next();
			if (next instanceof URI) {
				URI normURI = myExplorer.getNormalizedURI((URI) next);
				if (normURI != null) {
					result.add(normURI);
				}
			}
		}
		return result;
	}

	protected File getMcoreTemDir() {
		if (myTempMCoreDir == null) {
			String mcoreFolderPath = McoreUIPlugin.getInstance().getMCoreTempFolderPath();

			myTempMCoreDir = new File(mcoreFolderPath);

			if (!myTempMCoreDir.exists()) {
				myTempMCoreDir.mkdir();
			}
		}
		return myTempMCoreDir;
	}
}
