package com.montages.mcore.ui.emf;

import static org.eclipse.emf.ecore.util.EcoreUtil.ExternalCrossReferencer.find;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

public class CrossReferenceUtil {

	/*
	 * Returns the sub-list of Resource having references to other resources in the 
	 * provided list. Resources being referenced are removed.
	 * 
	 */	
	public static List<Resource> dropReferenced(List<Resource> resources) {
		List<Resource> referenced = new ArrayList<Resource>();

		for (Resource resource: resources) {
			findReferences(resource, referenced);	
		}

		resources.removeAll(referenced);

		return resources;
	}

	private static List<Resource> findReferences(Resource resource, List<Resource> referenced) {		
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = find(resource);

		for (Iterator<EObject> it = crossReferences.keySet().iterator(); it.hasNext();) {
			EObject o = it.next();
			if (isExternalReference(resource, o)) {
				referenced.add(o.eResource());
			}
		}

		return referenced;
	}

	private static boolean isExternalReference(Resource container, EObject referenced) {
		if (referenced.eResource() == null) return false;
		if (referenced.eResource().equals(container)) return false;

		String referencedURI = referenced.eResource().getURI().toString();
		String ecoreURI = EcorePackage.eINSTANCE.getNsURI();
		String xmlURI = XMLTypePackage.eINSTANCE.getNsURI();

		return !(referencedURI.equals(ecoreURI) || referencedURI.equals(xmlURI));
	}

}
