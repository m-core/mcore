package com.montages.mcore.ui;

import org.eclipse.ui.IStartup;

import com.montages.mcore.mcore2ecore.Mcore2Ecore;

public class UIStartup implements IStartup {

	@Override
	public void earlyStartup() {
		Mcore2Ecore.prepareExecutor();
	}

}
