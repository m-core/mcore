/**
 */
package com.montages.mmapper.provider;

import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.AProperty;

import com.montages.mmapper.MMapEntryForTargetFeature;
import com.montages.mmapper.MmapperFactory;
import com.montages.mmapper.MmapperPackage;

import com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl;

import com.montages.mrules.expressions.ExpressionsPackage;

import com.montages.mrules.provider.MRuleAnnotationItemProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mmapper.MMapEntryForTargetFeature} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MMapEntryForTargetFeatureItemProvider extends MRuleAnnotationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MMapEntryForTargetFeatureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addATargetFeaturePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ATarget Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATargetFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ATarget Feature feature.
		 * The list of possible choices is constructed by OCL let chain: OrderedSet(acore::classifiers::AProperty)  = if aTargetObjectType.oclIsUndefined()
		then OrderedSet{}
		else aTargetObjectType.aAllProperty()
		endif in
		chain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() 
		else r endif)
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MMapEntryForTargetFeature_aTargetFeature_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MMapEntryForTargetFeature_aTargetFeature_feature",
						"_UI_MMapEntryForTargetFeature_type"),
				MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE, true, false, false, null, null,
				null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<AFeature> result = new ArrayList<AFeature>();
				Collection<? extends AFeature> superResult = (Collection<? extends AFeature>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MMapEntryForTargetFeatureImpl) object).evalATargetFeatureChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MMapEntryForTargetFeature.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MMapEntryForTargetFeature"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MMapEntryForTargetFeature) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MMapEntryForTargetFeature.class)) {
		case MmapperPackage.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				MmapperPackage.Literals.MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE,
				MmapperFactory.eINSTANCE.createMMap()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MmapperEditPlugin.INSTANCE;
	}

	/**
	 * This adds a property descriptor for the AElement1 feature.
	 * The list of possible choices is constructed by OCL 
	let annotatedProp: acore::classifiers::AProperty = 
	self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)
	in
	if self.aChainEntryType.oclIsUndefined() then 
	Sequence{} else
	self.aChainEntryType.aAllProperty()->asSequence()
	endif
	
	--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag IPINS01
	 */
	@Override
	protected void addAElement1PropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aElement1_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aElement1_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT1, true, false, false, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<AProperty> choice = new ArrayList<AProperty>(
								(Collection<? extends AProperty>) super.getChoiceOfValues(object));
						choice = ((MMapEntryForTargetFeatureImpl) object).evalAElement1ChoiceConstruction(choice);
						return choice;
					}
				});
	}
}
