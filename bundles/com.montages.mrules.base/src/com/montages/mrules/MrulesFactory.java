/**
 */
package com.montages.mrules;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.MrulesPackage
 * @generated
 */
public interface MrulesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MrulesFactory eINSTANCE = com.montages.mrules.impl.MrulesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MTest Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MTest Rule</em>'.
	 * @generated
	 */
	MTestRule createMTestRule();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MrulesPackage getMrulesPackage();

} //MrulesFactory
