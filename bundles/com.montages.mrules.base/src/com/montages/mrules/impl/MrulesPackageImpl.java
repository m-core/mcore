/**
 */
package com.montages.mrules.impl;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MRulesAnnotationAction;
import com.montages.mrules.MRulesElement;
import com.montages.mrules.MRulesNamed;
import com.montages.mrules.MTestRule;
import com.montages.mrules.MrulesFactory;
import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;

import com.montages.mrules.expressions.impl.ExpressionsPackageImpl;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.xocl.core.util.XoclErrorHandler;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MrulesPackageImpl extends EPackageImpl implements MrulesPackage {
	/**
	 * The cached OCL constraint restricting the classes of root elements.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails().get("rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID, choiceConstraint, helper.getProblems(),
						MrulesPackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTestRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mRuleAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mRulesElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mRulesNamedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mRulesAnnotationActionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mrules.MrulesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MrulesPackageImpl() {
		super(eNS_URI, MrulesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MrulesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MrulesPackage init() {
		if (isInited)
			return (MrulesPackage) EPackage.Registry.INSTANCE.getEPackage(MrulesPackage.eNS_URI);

		// Obtain or create and register package
		MrulesPackageImpl theMrulesPackage = (MrulesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof MrulesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new MrulesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI)
						: ExpressionsPackage.eINSTANCE);

		// Create package meta-data objects
		theMrulesPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();

		// Initialize created meta-data
		theMrulesPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMrulesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MrulesPackage.eNS_URI, theMrulesPackage);
		return theMrulesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTestRule() {
		return mTestRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMRuleAnnotation() {
		return mRuleAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRuleAnnotation_NamedExpression() {
		return (EReference) mRuleAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRuleAnnotation_NamedTuple() {
		return (EReference) mRuleAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRuleAnnotation_NamedConstant() {
		return (EReference) mRuleAnnotationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRuleAnnotation_AExpectedReturnTypeOfAnnotation() {
		return (EReference) mRuleAnnotationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_IsReturnValueOfAnnotationMandatory() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_IsReturnValueOfAnnotationSingular() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_UseExplicitOcl() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_OclChanged() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_OclCode() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRuleAnnotation_DoAction() {
		return (EAttribute) mRuleAnnotationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction() {
		return mRuleAnnotationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRuleAnnotation__VariableFromExpression__MNamedExpression() {
		return mRuleAnnotationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMRulesElement() {
		return mRulesElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesElement_KindLabel() {
		return (EAttribute) mRulesElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesElement_RenderedKindLabel() {
		return (EAttribute) mRulesElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesElement_IndentLevel() {
		return (EAttribute) mRulesElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesElement_Description() {
		return (EAttribute) mRulesElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesElement_AsText() {
		return (EAttribute) mRulesElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRulesElement__IndentationSpaces() {
		return mRulesElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRulesElement__IndentationSpaces__Integer() {
		return mRulesElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMRulesNamed() {
		return mRulesNamedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_SpecialEName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_Name() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_ShortName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_EName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_FullLabel() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_LocalStructuralName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_CalculatedName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_CalculatedShortName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_CorrectName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRulesNamed_CorrectShortName() {
		return (EAttribute) mRulesNamedEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRulesNamed__SameName__MRulesNamed() {
		return mRulesNamedEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRulesNamed__SameShortName__MRulesNamed() {
		return mRulesNamedEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRulesNamed__SameString__String_String() {
		return mRulesNamedEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRulesNamed__StringEmpty__String() {
		return mRulesNamedEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMRulesAnnotationAction() {
		return mRulesAnnotationActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MrulesFactory getMrulesFactory() {
		return (MrulesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mTestRuleEClass = createEClass(MTEST_RULE);

		mRuleAnnotationEClass = createEClass(MRULE_ANNOTATION);
		createEReference(mRuleAnnotationEClass, MRULE_ANNOTATION__NAMED_EXPRESSION);
		createEReference(mRuleAnnotationEClass, MRULE_ANNOTATION__NAMED_TUPLE);
		createEReference(mRuleAnnotationEClass, MRULE_ANNOTATION__NAMED_CONSTANT);
		createEReference(mRuleAnnotationEClass, MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__USE_EXPLICIT_OCL);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__OCL_CHANGED);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__OCL_CODE);
		createEAttribute(mRuleAnnotationEClass, MRULE_ANNOTATION__DO_ACTION);
		createEOperation(mRuleAnnotationEClass, MRULE_ANNOTATION___DO_ACTION_UPDATE__MRULESANNOTATIONACTION);
		createEOperation(mRuleAnnotationEClass, MRULE_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION);

		mRulesElementEClass = createEClass(MRULES_ELEMENT);
		createEAttribute(mRulesElementEClass, MRULES_ELEMENT__KIND_LABEL);
		createEAttribute(mRulesElementEClass, MRULES_ELEMENT__RENDERED_KIND_LABEL);
		createEAttribute(mRulesElementEClass, MRULES_ELEMENT__INDENT_LEVEL);
		createEAttribute(mRulesElementEClass, MRULES_ELEMENT__DESCRIPTION);
		createEAttribute(mRulesElementEClass, MRULES_ELEMENT__AS_TEXT);
		createEOperation(mRulesElementEClass, MRULES_ELEMENT___INDENTATION_SPACES);
		createEOperation(mRulesElementEClass, MRULES_ELEMENT___INDENTATION_SPACES__INTEGER);

		mRulesNamedEClass = createEClass(MRULES_NAMED);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__SPECIAL_ENAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__NAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__SHORT_NAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__ENAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__FULL_LABEL);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__LOCAL_STRUCTURAL_NAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__CALCULATED_NAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__CALCULATED_SHORT_NAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__CORRECT_NAME);
		createEAttribute(mRulesNamedEClass, MRULES_NAMED__CORRECT_SHORT_NAME);
		createEOperation(mRulesNamedEClass, MRULES_NAMED___SAME_NAME__MRULESNAMED);
		createEOperation(mRulesNamedEClass, MRULES_NAMED___SAME_SHORT_NAME__MRULESNAMED);
		createEOperation(mRulesNamedEClass, MRULES_NAMED___SAME_STRING__STRING_STRING);
		createEOperation(mRulesNamedEClass, MRULES_NAMED___STRING_EMPTY__STRING);

		// Create enums
		mRulesAnnotationActionEEnum = createEEnum(MRULES_ANNOTATION_ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);
		AbstractionsPackage theAbstractionsPackage = (AbstractionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theExpressionsPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mTestRuleEClass.getESuperTypes().add(this.getMRuleAnnotation());
		mRuleAnnotationEClass.getESuperTypes().add(theExpressionsPackage.getMBaseChain());
		mRuleAnnotationEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotation());
		mRulesNamedEClass.getESuperTypes().add(this.getMRulesElement());
		mRulesNamedEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());

		// Initialize classes, features, and operations; add parameters
		initEClass(mTestRuleEClass, MTestRule.class, "MTestRule", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mRuleAnnotationEClass, MRuleAnnotation.class, "MRuleAnnotation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMRuleAnnotation_NamedExpression(), theExpressionsPackage.getMNamedExpression(), null,
				"namedExpression", null, 0, -1, MRuleAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMRuleAnnotation_NamedTuple(), theExpressionsPackage.getMAbstractNamedTuple(), null,
				"namedTuple", null, 0, -1, MRuleAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMRuleAnnotation_NamedConstant(), theExpressionsPackage.getMNamedConstant(), null,
				"namedConstant", null, 0, -1, MRuleAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMRuleAnnotation_AExpectedReturnTypeOfAnnotation(), theClassifiersPackage.getAClassifier(),
				null, "aExpectedReturnTypeOfAnnotation", null, 0, 1, MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation(),
				theClassifiersPackage.getASimpleType(), "aExpectedReturnSimpleTypeOfAnnotation", null, 0, 1,
				MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_IsReturnValueOfAnnotationMandatory(), ecorePackage.getEBooleanObject(),
				"isReturnValueOfAnnotationMandatory", null, 1, 1, MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_IsReturnValueOfAnnotationSingular(), ecorePackage.getEBooleanObject(),
				"isReturnValueOfAnnotationSingular", null, 1, 1, MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_UseExplicitOcl(), ecorePackage.getEBooleanObject(), "useExplicitOcl", null, 0,
				1, MRuleAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_OclChanged(), ecorePackage.getEBooleanObject(), "oclChanged", null, 1, 1,
				MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_OclCode(), ecorePackage.getEString(), "oclCode", null, 0, 1,
				MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRuleAnnotation_DoAction(), this.getMRulesAnnotationAction(), "doAction", null, 0, 1,
				MRuleAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction(),
				ecorePackage.getEString(), "doActionUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMRulesAnnotationAction(), "trg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMRuleAnnotation__VariableFromExpression__MNamedExpression(),
				theExpressionsPackage.getMVariableBaseDefinition(), "variableFromExpression", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, theExpressionsPackage.getMNamedExpression(), "namedExpression", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mRulesElementEClass, MRulesElement.class, "MRulesElement", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMRulesElement_KindLabel(), ecorePackage.getEString(), "kindLabel", "", 0, 1,
				MRulesElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesElement_RenderedKindLabel(), ecorePackage.getEString(), "renderedKindLabel", null, 1, 1,
				MRulesElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesElement_IndentLevel(), ecorePackage.getEIntegerObject(), "indentLevel", null, 1, 1,
				MRulesElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesElement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MRulesElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesElement_AsText(), ecorePackage.getEString(), "asText", null, 0, 1, MRulesElement.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMRulesElement__IndentationSpaces(), ecorePackage.getEString(), "indentationSpaces", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMRulesElement__IndentationSpaces__Integer(), ecorePackage.getEString(),
				"indentationSpaces", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "size", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mRulesNamedEClass, MRulesNamed.class, "MRulesNamed", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMRulesNamed_SpecialEName(), ecorePackage.getEString(), "specialEName", null, 0, 1,
				MRulesNamed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_Name(), ecorePackage.getEString(), "name", null, 0, 1, MRulesNamed.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_ShortName(), ecorePackage.getEString(), "shortName", null, 0, 1,
				MRulesNamed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_EName(), ecorePackage.getEString(), "eName", null, 0, 1, MRulesNamed.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_FullLabel(), ecorePackage.getEString(), "fullLabel", null, 0, 1,
				MRulesNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_LocalStructuralName(), ecorePackage.getEString(), "localStructuralName", null, 0,
				1, MRulesNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_CalculatedName(), ecorePackage.getEString(), "calculatedName", null, 0, 1,
				MRulesNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_CalculatedShortName(), ecorePackage.getEString(), "calculatedShortName", null, 0,
				1, MRulesNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_CorrectName(), ecorePackage.getEBooleanObject(), "correctName", null, 0, 1,
				MRulesNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRulesNamed_CorrectShortName(), ecorePackage.getEBooleanObject(), "correctShortName", null, 0,
				1, MRulesNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMRulesNamed__SameName__MRulesNamed(), ecorePackage.getEBooleanObject(), "sameName", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMRulesNamed(), "n", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMRulesNamed__SameShortName__MRulesNamed(), ecorePackage.getEBooleanObject(),
				"sameShortName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMRulesNamed(), "n", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMRulesNamed__SameString__String_String(), ecorePackage.getEBooleanObject(), "sameString",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s2", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMRulesNamed__StringEmpty__String(), ecorePackage.getEBooleanObject(), "stringEmpty", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(mRulesAnnotationActionEEnum, MRulesAnnotationAction.class, "MRulesAnnotationAction");
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.DO);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.INTO_DATA_CONSTANT);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.INTO_LITERAL_CONSTANT);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.INTO_APPLY);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.INTO_COND_OF_IF_THEN_ELSE);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.INTO_ELSE_OF_IF_THEN_ELSE);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.ARGUMENT);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.DATA_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.LITERAL_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.SUB_CHAIN);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.COLLECTION_OP);
		addEEnumLiteral(mRulesAnnotationActionEEnum, MRulesAnnotationAction.WHERE_AS_BASE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source, new String[] { "rootConstraint", "trg.name = \'MTestRule\'" });
		addAnnotation(getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction(), source,
				new String[] { "body", "null\n" });
		addAnnotation(getMRuleAnnotation__VariableFromExpression__MNamedExpression(), source, new String[] { "body",
				"if self.localScopeVariables->isEmpty() then null\r\nelse\r\nself.localScopeVariables->select(x:mrules::expressions::MVariableBaseDefinition| if x.namedExpression<> null  then x.namedExpression = namedExpression else false endif )->first() endif" });
		addAnnotation(getMRuleAnnotation_AExpectedReturnTypeOfAnnotation(), source, new String[] { "derive",
				"null\r\n/*let apa:annotations::MAbstractPropertyAnnotations = eContainer().oclAsType(MAbstractPropertyAnnotations)\r\nin if apa.annotatedProperty.oclIsUndefined()\r\n  then null \r\n  else apa.annotatedProperty.calculatedType\r\nendif */" });
		addAnnotation(getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation(), source, new String[] { "derive",
				"acore::classifiers::ASimpleType::None\r\n /*\r\nif self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.simpleType\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.simpleType\r\n      else acore::classifiers::ASimpleType::None \r\nendif endif endif */" });
		addAnnotation(getMRuleAnnotation_IsReturnValueOfAnnotationMandatory(), source, new String[] { "derive",
				"null\r\n/*if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.mandatory\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.mandatory\r\n      else true\r\nendif endif endif */" });
		addAnnotation(getMRuleAnnotation_IsReturnValueOfAnnotationSingular(), source, new String[] { "derive",
				"null\r\n/* if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\n\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.singular\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.singular\r\n      else true\r\nendif endif endif */" });
		addAnnotation(getMRuleAnnotation_UseExplicitOcl(), source, new String[] { "initValue", "false\n" });
		addAnnotation(getMRuleAnnotation_OclChanged(), source, new String[] { "derive", "false" });
		addAnnotation(getMRuleAnnotation_OclCode(), source, new String[] { "derive",
				"let attentionImplementedinJava: String = \'TODO Replace Java Code\' in\nattentionImplementedinJava\n" });
		addAnnotation(getMRuleAnnotation_DoAction(), source,
				new String[] { "derive", "mrules::MRulesAnnotationAction::Do\n" });
		addAnnotation(getMRulesElement__IndentationSpaces(), source,
				new String[] { "body", "indentationSpaces(indentLevel * 4)" });
		addAnnotation(getMRulesElement__IndentationSpaces__Integer(), source, new String[] { "body",
				"if size < 1 then \'\'\r\nelse self.indentationSpaces(size-1).concat(\' \') endif" });
		addAnnotation(getMRulesElement_KindLabel(), source, new String[] { "derive", "\'TODO\'\n" });
		addAnnotation(getMRulesElement_RenderedKindLabel(), source, new String[] { "derive",
				"/*OVERRITEN in Code for performance*/\nself.indentationSpaces().concat(self.kindLabel)" });
		addAnnotation(getMRulesElement_IndentLevel(), source, new String[] { "derive",
				"if eContainer().oclIsUndefined() \r\n  then 0\r\nelse if eContainer().oclIsKindOf(MRulesElement)\r\n  then eContainer().oclAsType(MRulesElement).indentLevel + 1\r\n  else 0 endif endif" });
		addAnnotation(mRulesNamedEClass, source, new String[] { "label", "eName" });
		addAnnotation(getMRulesNamed__SameName__MRulesNamed(), source,
				new String[] { "body", "sameString(name, n.name)\r\n" });
		addAnnotation(getMRulesNamed__SameShortName__MRulesNamed(), source, new String[] { "body",
				"if stringEmpty(shortName)  then\r\n  sameString(name, n.shortName)\r\nelse if  stringEmpty(n.shortName) then\r\n sameString(shortName, n.name)\r\nelse sameString(shortName, n.shortName)\r\nendif endif" });
		addAnnotation(getMRulesNamed__SameString__String_String(), source, new String[] { "body",
				"s1=s2 \r\nor \r\n(s1.oclIsUndefined() and s2=\'\')\r\nor\r\n(s1=\'\' and s2.oclIsUndefined())" });
		addAnnotation(getMRulesNamed__StringEmpty__String(), source,
				new String[] { "body", "s.oclIsUndefined() or s=\'\'" });
		addAnnotation(getMRulesNamed_EName(), source, new String[] { "derive",
				"if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseLower()\r\nelse self.specialEName.camelCaseLower()\r\nendif" });
		addAnnotation(getMRulesNamed_FullLabel(), source,
				new String[] { "derive", "\'OVERRIDE IN SUBCLASS \'.concat(self.calculatedName)" });
		addAnnotation(getMRulesNamed_LocalStructuralName(), source, new String[] { "derive", "\'\'\n" });
		addAnnotation(getMRulesNamed_CalculatedName(), source,
				new String[] { "derive", "if stringEmpty(name) then \' NAME MISSING\' else name endif" });
		addAnnotation(getMRulesNamed_CalculatedShortName(), source, new String[] { "derive",
				"if stringEmpty(name) or stringEmpty(shortName) then calculatedName else shortName endif" });
		addAnnotation(getMRulesNamed_CorrectName(), source, new String[] { "derive", "not stringEmpty(name)" });
		addAnnotation(getMRulesNamed_CorrectShortName(), source,
				new String[] { "derive", " stringEmpty(shortName)\r\n or (not stringEmpty(name))" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true", "uuidAttributeName", "_uuid" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source, new String[] { "hideAdvancedProperties", "null" });
		addAnnotation(getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn", "true" });
		addAnnotation(getMRuleAnnotation__VariableFromExpression__MNamedExpression(), source,
				new String[] { "propertyCategory", "Helper", "createColumn", "true" });
		addAnnotation(getMRuleAnnotation_AExpectedReturnTypeOfAnnotation(), source,
				new String[] { "propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation(), source,
				new String[] { "propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_IsReturnValueOfAnnotationMandatory(), source,
				new String[] { "propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_IsReturnValueOfAnnotationSingular(), source,
				new String[] { "propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_UseExplicitOcl(), source,
				new String[] { "propertyCategory", "Explicit OCL", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_OclChanged(), source,
				new String[] { "propertyCategory", "Explicit OCL", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_OclCode(), source,
				new String[] { "propertyCategory", "Explicit OCL", "createColumn", "false" });
		addAnnotation(getMRuleAnnotation_DoAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMRulesElement__IndentationSpaces(), source, new String[] {});
		addAnnotation(getMRulesElement__IndentationSpaces__Integer(), source,
				new String[] { "propertyCategory", "Kind", "createColumn", "true" });
		addAnnotation(getMRulesElement_KindLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Kind" });
		addAnnotation(getMRulesElement_RenderedKindLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Kind" });
		addAnnotation(getMRulesElement_IndentLevel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Kind" });
		addAnnotation(getMRulesElement_Description(), source,
				new String[] { "mixedEditor", "true", "propertyCategory", "Documentation", "createColumn", "false" });
		addAnnotation(getMRulesElement_AsText(), source,
				new String[] { "propertyCategory", "Summary As Text", "createColumn", "false" });
		addAnnotation(getMRulesNamed_SpecialEName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special ECore Settings" });
		addAnnotation(getMRulesNamed_Name(), source,
				new String[] { "propertyCategory", "Abbreviation and Name", "createColumn", "false" });
		addAnnotation(getMRulesNamed_ShortName(), source,
				new String[] { "propertyCategory", "Abbreviation and Name", "createColumn", "false" });
		addAnnotation(getMRulesNamed_EName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Abbreviation and Name" });
		addAnnotation(getMRulesNamed_FullLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Abbreviation and Name" });
		addAnnotation(getMRulesNamed_LocalStructuralName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Abbreviation and Name" });
		addAnnotation(getMRulesNamed_CalculatedName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Abbreviation and Name" });
		addAnnotation(getMRulesNamed_CalculatedShortName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Abbreviation and Name" });
		addAnnotation(getMRulesNamed_CorrectName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Validation" });
		addAnnotation(getMRulesNamed_CorrectShortName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Validation" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation((getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction()).getEParameters().get(0), source,
				new String[] { "mName", "trg" });
		addAnnotation(getMRuleAnnotation__VariableFromExpression__MNamedExpression(), source,
				new String[] { "mName", "VariableFromExpression" });
		addAnnotation((getMRuleAnnotation__VariableFromExpression__MNamedExpression()).getEParameters().get(0), source,
				new String[] { "mName", "NamedExpression" });
		addAnnotation(getMRuleAnnotation_AExpectedReturnTypeOfAnnotation(), source,
				new String[] { "mName", "Expected Return Type Of Annotation" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMRuleAnnotation_DoAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesElement_KindLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesElement_RenderedKindLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesElement_IndentLevel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_EName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_FullLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_LocalStructuralName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_CalculatedName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_CalculatedShortName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_CorrectName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRulesNamed_CorrectShortName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mRulesNamedEClass, source, new String[] { "aNameDerive", "eName\n" });
	}

} //MrulesPackageImpl
