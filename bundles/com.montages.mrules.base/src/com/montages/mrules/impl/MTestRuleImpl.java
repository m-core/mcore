/**
 */

package com.montages.mrules.impl;

import com.montages.mrules.MTestRule;
import com.montages.mrules.MrulesPackage;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.ocl.ecore.OCLExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MTest Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public class MTestRuleImpl extends MRuleAnnotationImpl implements MTestRule {

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MTestRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MrulesPackage.Literals.MTEST_RULE;
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}
} //MTestRuleImpl
