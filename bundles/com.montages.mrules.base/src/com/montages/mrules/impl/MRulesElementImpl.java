/**
 */

package com.montages.mrules.impl;

import com.montages.mrules.MRulesElement;
import com.montages.mrules.MrulesPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MRules Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.impl.MRulesElementImpl#getKindLabel <em>Kind Label</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRulesElementImpl#getRenderedKindLabel <em>Rendered Kind Label</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRulesElementImpl#getIndentLevel <em>Indent Level</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRulesElementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRulesElementImpl#getAsText <em>As Text</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MRulesElementImpl extends MinimalEObjectImpl.Container implements MRulesElement {
	/**
	 * The default value of the '{@link #getKindLabel() <em>Kind Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String KIND_LABEL_EDEFAULT = "";

	/**
	 * The default value of the '{@link #getRenderedKindLabel() <em>Rendered Kind Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRenderedKindLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String RENDERED_KIND_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIndentLevel() <em>Indent Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndentLevel()
	 * @generated
	 * @ordered
	 */
	protected static final Integer INDENT_LEVEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * This is true if the Description attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean descriptionESet;

	/**
	 * The default value of the '{@link #getAsText() <em>As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsText()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAsText() <em>As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsText()
	 * @generated
	 * @ordered
	 */
	protected String asText = AS_TEXT_EDEFAULT;

	/**
	 * This is true if the As Text attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean asTextESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getRenderedKindLabel <em>Rendered Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRenderedKindLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression renderedKindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIndentLevel <em>Indent Level</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndentLevel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression indentLevelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MRulesElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MrulesPackage.Literals.MRULES_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getKindLabel() {
		/**
		 * @OCL 'TODO'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULES_ELEMENT;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULES_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULES_ELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRenderedKindLabel() {
		/**
		 * @OCL /*OVERRITEN in Code for performance*\/
		self.indentationSpaces().concat(self.kindLabel)
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULES_ELEMENT;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_ELEMENT__RENDERED_KIND_LABEL;

		if (renderedKindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				renderedKindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULES_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(renderedKindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULES_ELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getIndentLevel() {
		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(MRulesElement)
		then eContainer().oclAsType(MRulesElement).indentLevel + 1
		else 0 endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULES_ELEMENT;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_ELEMENT__INDENT_LEVEL;

		if (indentLevelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				indentLevelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULES_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(indentLevelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULES_ELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Integer result = (Integer) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		boolean oldDescriptionESet = descriptionESet;
		descriptionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MrulesPackage.MRULES_ELEMENT__DESCRIPTION,
					oldDescription, description, !oldDescriptionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDescription() {
		String oldDescription = description;
		boolean oldDescriptionESet = descriptionESet;
		description = DESCRIPTION_EDEFAULT;
		descriptionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MrulesPackage.MRULES_ELEMENT__DESCRIPTION,
					oldDescription, DESCRIPTION_EDEFAULT, oldDescriptionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDescription() {
		return descriptionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsText() {
		return asText;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsText(String newAsText) {
		String oldAsText = asText;
		asText = newAsText;
		boolean oldAsTextESet = asTextESet;
		asTextESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MrulesPackage.MRULES_ELEMENT__AS_TEXT, oldAsText,
					asText, !oldAsTextESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAsText() {
		String oldAsText = asText;
		boolean oldAsTextESet = asTextESet;
		asText = AS_TEXT_EDEFAULT;
		asTextESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MrulesPackage.MRULES_ELEMENT__AS_TEXT, oldAsText,
					AS_TEXT_EDEFAULT, oldAsTextESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAsText() {
		return asTextESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces() {

		/**
		 * @OCL indentationSpaces(indentLevel * 4)
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULES_ELEMENT);
		EOperation eOperation = MrulesPackage.Literals.MRULES_ELEMENT.getEOperations().get(0);
		if (indentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, body, helper.getProblems(),
						MrulesPackage.Literals.MRULES_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULES_ELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces(Integer size) {

		/**
		 * @OCL if size < 1 then ''
		else self.indentationSpaces(size-1).concat(' ') endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULES_ELEMENT);
		EOperation eOperation = MrulesPackage.Literals.MRULES_ELEMENT.getEOperations().get(1);
		if (indentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, body, helper.getProblems(),
						MrulesPackage.Literals.MRULES_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULES_ELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MrulesPackage.MRULES_ELEMENT__KIND_LABEL:
			return getKindLabel();
		case MrulesPackage.MRULES_ELEMENT__RENDERED_KIND_LABEL:
			return getRenderedKindLabel();
		case MrulesPackage.MRULES_ELEMENT__INDENT_LEVEL:
			return getIndentLevel();
		case MrulesPackage.MRULES_ELEMENT__DESCRIPTION:
			return getDescription();
		case MrulesPackage.MRULES_ELEMENT__AS_TEXT:
			return getAsText();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MrulesPackage.MRULES_ELEMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case MrulesPackage.MRULES_ELEMENT__AS_TEXT:
			setAsText((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MrulesPackage.MRULES_ELEMENT__DESCRIPTION:
			unsetDescription();
			return;
		case MrulesPackage.MRULES_ELEMENT__AS_TEXT:
			unsetAsText();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MrulesPackage.MRULES_ELEMENT__KIND_LABEL:
			return KIND_LABEL_EDEFAULT == null ? getKindLabel() != null : !KIND_LABEL_EDEFAULT.equals(getKindLabel());
		case MrulesPackage.MRULES_ELEMENT__RENDERED_KIND_LABEL:
			return RENDERED_KIND_LABEL_EDEFAULT == null ? getRenderedKindLabel() != null
					: !RENDERED_KIND_LABEL_EDEFAULT.equals(getRenderedKindLabel());
		case MrulesPackage.MRULES_ELEMENT__INDENT_LEVEL:
			return INDENT_LEVEL_EDEFAULT == null ? getIndentLevel() != null
					: !INDENT_LEVEL_EDEFAULT.equals(getIndentLevel());
		case MrulesPackage.MRULES_ELEMENT__DESCRIPTION:
			return isSetDescription();
		case MrulesPackage.MRULES_ELEMENT__AS_TEXT:
			return isSetAsText();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case MrulesPackage.MRULES_ELEMENT___INDENTATION_SPACES:
			return indentationSpaces();
		case MrulesPackage.MRULES_ELEMENT___INDENTATION_SPACES__INTEGER:
			return indentationSpaces((Integer) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		if (descriptionESet)
			result.append(description);
		else
			result.append("<unset>");
		result.append(", asText: ");
		if (asTextESet)
			result.append(asText);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MRulesElementImpl
