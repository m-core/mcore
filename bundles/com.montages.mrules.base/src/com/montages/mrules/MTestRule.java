/**
 */

package com.montages.mrules;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTest Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.MrulesPackage#getMTestRule()
 * @model
 * @generated
 */

public interface MTestRule extends MRuleAnnotation {
} // MTestRule
