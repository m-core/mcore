/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNew Object Feature Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MNewObjectFeatureValue#getAFeature <em>AFeature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNewObjectFeatureValue()
 * @model
 * @generated
 */

public interface MNewObjectFeatureValue extends MAbstractTupleEntry {
	/**
	 * Returns the value of the '<em><b>AFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature</em>' reference.
	 * @see #isSetAFeature()
	 * @see #unsetAFeature()
	 * @see #setAFeature(AFeature)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNewObjectFeatureValue_AFeature()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Feature'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='if self.eContainer().oclAsType(mrules::expressions::MNewObject).aNewType.oclIsUndefined() then \r\nOrderedSet{} else\r\nlet type : acore::classifiers::AClassType= \r\nself.eContainer().oclAsType(mrules::expressions::MNewObject).aNewType in\r\ntype.aAllStoredFeature endif'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	AFeature getAFeature();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MNewObjectFeatureValue#getAFeature <em>AFeature</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AFeature</em>' reference.
	 * @see #isSetAFeature()
	 * @see #unsetAFeature()
	 * @see #getAFeature()
	 * @generated
	 */

	void setAFeature(AFeature value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MNewObjectFeatureValue#getAFeature <em>AFeature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAFeature()
	 * @see #getAFeature()
	 * @see #setAFeature(AFeature)
	 * @generated
	 */
	void unsetAFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MNewObjectFeatureValue#getAFeature <em>AFeature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AFeature</em>' reference is set.
	 * @see #unsetAFeature()
	 * @see #getAFeature()
	 * @see #setAFeature(AFeature)
	 * @generated
	 */
	boolean isSetAFeature();

} // MNewObjectFeatureValue
