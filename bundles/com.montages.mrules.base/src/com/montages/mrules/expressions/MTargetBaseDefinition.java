/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTarget Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMTargetBaseDefinition()
 * @model annotation="http://www.montages.com/mCore/MCore mName='MTargetBaseDefinition'"
 *        annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'trg\'\n' calculatedBaseDerive='ExpressionBase::Target' aMandatoryDerive='true\n' aClassifierDerive='if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).aTargetObjectType\r\nelse null\r\nendif\r\n' aSingularDerive='true\n'"
 * @generated
 */

public interface MTargetBaseDefinition extends MAbstractBaseDefinition {
} // MTargetBaseDefinition
