/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSelf Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSelfBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'self\'\n' calculatedBaseDerive='ExpressionBase::SelfObject' aMandatoryDerive='true\n' aClassifierDerive='if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).aSelfObjectType\r\nelse null\r\nendif\r\n' aSingularDerive='true\n'"
 * @generated
 */

public interface MSelfBaseDefinition extends MAbstractBaseDefinition {
} // MSelfBaseDefinition
