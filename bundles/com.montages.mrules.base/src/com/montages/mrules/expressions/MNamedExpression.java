/**
 */

package com.montages.mrules.expressions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNamed Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MNamedExpression#getExpression <em>Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MNamedExpression#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNamedExpression()
 * @model annotation="http://www.montages.com/mCore/MCore mName='MNamedExpression'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Where\'\r\n/*\r\nlet c: annotations::MExprAnnotation = self.eContainer().oclAsType(annotations::MExprAnnotation) in\r\nif c.oclIsUndefined() then \'Let\'\r\n\telse if c.namedExpression->last() = self\r\n\t\tthen \'Definition\'\r\n\t\telse \'Let\' endif\r\nendif\r\n\052/' asBasicCodeDerive='if expression.oclIsUndefined() then \'MISSING EXPRESSION\' else\r\n\r\nlet c: String = expression.asCode in\r\nlet t: String = \r\nif   expression.oclIsKindOf(expressions::MChain) then\r\nif  expression.oclAsType(expressions::MChain).base = ExpressionBase::EContainer and expression.oclAsType(expressions::MChain).aBaseExitSimpleType = acore::classifiers::ASimpleType::Object then \r\naTypeAsOcl( aSelfObjectPackage, expression.aClassifier, acore::classifiers::ASimpleType::None, expression.aSingular ) else\r\n\r\naTypeAsOcl( aSelfObjectPackage, expression.aClassifier, expression.aSimpleType, expression.aSingular )\r\nendif\r\nelse\r\naTypeAsOcl( aSelfObjectPackage, expression.aClassifier, expression.aSimpleType, expression.aSingular )\r\n\r\n\r\n  endif\r\nin\r\nif name.oclIsUndefined() or name=\'\' \r\nthen /* The expression shoudl be returned here, as this is the final result \052/\r\n  if expression.oclIsKindOf(MAbstractExpressionWithBase)\r\n\tthen /* We do a special case for null...since it cannot simply be returned as such \052/\r\n\t\tlet chain: MAbstractExpressionWithBase = expression.oclAsType(MAbstractExpressionWithBase) in\r\n\t\tif chain.base = ExpressionBase::NullValue\r\n\t\t\tthen \'let nl: \'.concat(  aTypeAsOcl(aSelfObjectPackage, containingAnnotation.aExpectedReturnTypeOfAnnotation, containingAnnotation.aExpectedReturnSimpleTypeOfAnnotation, self.isReturnValueSingular) ).concat(\' = null in nl\')\r\n\t\t\telse c endif\r\n  \telse c endif\r\nelse \'let \'.concat(eName).concat(\': \').concat(t).concat(\' = \').concat(c).concat(\' in\')\r\nendif\r\n\r\nendif ' calculatedOwnMandatoryDerive='if expression.oclIsUndefined() then false\r\nelse expression.aMandatory endif' calculatedOwnSingularDerive='\r\nif expression.oclIsUndefined() then true\r\nelse if expression.oclIsTypeOf(expressions::MChain) \r\nthen  if expression.oclAsType(expressions::MChain).processor <> MProcessor::None then true else\r\nexpression.aSingular endif \r\nelse \r\nexpression.aSingular\r\nendif\r\nendif\r\n' aCalculatedOwnSimpleTypeDerive='if expression.oclIsUndefined() then acore::classifiers::ASimpleType::None\r\nelse expression.aSimpleType endif' aCalculatedOwnTypeDerive='if expression.oclIsUndefined() then null\r\nelse expression.aClassifier endif'"
 * @generated
 */

public interface MNamedExpression extends MAbstractLet {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #setExpression(MToplevelExpression)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNamedExpression_Expression()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='defaultValue()'"
	 * @generated
	 */
	MToplevelExpression getExpression();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MNamedExpression#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @generated
	 */

	void setExpression(MToplevelExpression value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MNamedExpression#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MToplevelExpression)
	 * @generated
	 */
	void unsetExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MNamedExpression#getExpression <em>Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression</em>' containment reference is set.
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MToplevelExpression)
	 * @generated
	 */
	boolean isSetExpression();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.MNamedExpressionAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mrules.expressions.MNamedExpressionAction
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNamedExpression_DoAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mrules::expressions::MNamedExpressionAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MNamedExpressionAction getDoAction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='Tuple{base=ExpressionBase::SelfObject}'"
	 * @generated
	 */
	MChain defaultValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='allApplications'"
	 *        annotation="http://www.xocl.org/OCL body='let all: OrderedSet(MApplication) = \r\nself.expression->asOrderedSet()->first().oclAsType(MApplication)->asSequence()->union(  self.expression->closure(oclAsType(MApplication).operands->select(oclIsTypeOf(MApplication))).oclAsType(MApplication))->union(self.expression->closure(oclAsType(MApplication).operands).oclAsType(MApplication)->asSequence())->asOrderedSet()\r\n\r\n --self.expression->closure(oclAsType(MApplication).operands).oclAsType(MApplication) ->union(  self.expression->closure(oclAsType(MApplication).operands->select(oclIsTypeOf(MApplication))).oclAsType(MApplication))\r\n --->reject(oclIsUndefined())->asOrderedSet() \r\n --->append(self.expression->asOrderedSet()->first().oclAsType(MApplication))->asOrderedSet()\r\n -->reject(oclIsUndefined())->asOrderedSet()\r\n in if all->oclIsUndefined() then null else all endif'"
	 * @generated
	 */
	EList<MApplication> allApplications();

} // MNamedExpression
