/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AParameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MParameter Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MParameterBaseDefinition#getAParameter <em>AParameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMParameterBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<par> \'.concat(if aParameter.oclIsUndefined() then \'\' else aParameter.aName\r\nendif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='mrules::expressions::ExpressionBase::Parameter\n' calculatedAsCodeDerive='if aParameter.oclIsUndefined()then \'MISSING PARAMETER\' else aParameter.aName endif' aMandatoryDerive='if aParameter.oclIsUndefined() then true\r\nelse aParameter.aMandatory endif' aSingularDerive='if aParameter.oclIsUndefined() then true\r\nelse aParameter.aSingular endif' aSimpleTypeDerive='if aParameter.oclIsUndefined() then acore::classifiers::ASimpleType::None\r\nelse aParameter.aSimpleType\r\nendif' aClassifierDerive='if aParameter.oclIsUndefined() then null\r\nelse aParameter.aClassifier\r\nendif'"
 * @generated
 */

public interface MParameterBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>AParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AParameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AParameter</em>' reference.
	 * @see #isSetAParameter()
	 * @see #unsetAParameter()
	 * @see #setAParameter(AParameter)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMParameterBaseDefinition_AParameter()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Parameter'"
	 * @generated
	 */
	AParameter getAParameter();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MParameterBaseDefinition#getAParameter <em>AParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AParameter</em>' reference.
	 * @see #isSetAParameter()
	 * @see #unsetAParameter()
	 * @see #getAParameter()
	 * @generated
	 */

	void setAParameter(AParameter value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MParameterBaseDefinition#getAParameter <em>AParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAParameter()
	 * @see #getAParameter()
	 * @see #setAParameter(AParameter)
	 * @generated
	 */
	void unsetAParameter();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MParameterBaseDefinition#getAParameter <em>AParameter</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AParameter</em>' reference is set.
	 * @see #unsetAParameter()
	 * @see #getAParameter()
	 * @see #setAParameter(AParameter)
	 * @generated
	 */
	boolean isSetAParameter();

} // MParameterBaseDefinition
