/**
 */

package com.montages.mrules.expressions;

import com.montages.mrules.MRulesNamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTuple Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMTupleEntry()
 * @model annotation="http://www.montages.com/mCore/MCore mName='MTupleEntry'"
 * @generated
 */

public interface MTupleEntry extends MRulesNamed, MAbstractTupleEntry {
} // MTupleEntry
