/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AEnumeration;

import com.montages.acore.values.ALiteral;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral Value Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MLiteralValueExpr#getAEnumerationType <em>AEnumeration Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MLiteralValueExpr#getAValue <em>AValue</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralValueExpr()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Literal\'\n' isComplexExpressionDerive='false' calculatedOwnMandatoryDerive='true' calculatedOwnSingularDerive='true' aCalculatedOwnTypeDerive='aEnumerationType' aCalculatedOwnSimpleTypeDerive='acore::classifiers::ASimpleType::None' asBasicCodeDerive='if aValue.oclIsUndefined() then \'null\'\r\nelse aValue.aQualifiedName endif'"
 * @generated
 */

public interface MLiteralValueExpr extends MChainOrApplication {
	/**
	 * Returns the value of the '<em><b>AEnumeration Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AEnumeration Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AEnumeration Type</em>' reference.
	 * @see #isSetAEnumerationType()
	 * @see #unsetAEnumerationType()
	 * @see #setAEnumerationType(AEnumeration)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralValueExpr_AEnumerationType()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Enumeration Type'"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.aActiveEnumeration '"
	 * @generated
	 */
	AEnumeration getAEnumerationType();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralValueExpr#getAEnumerationType <em>AEnumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AEnumeration Type</em>' reference.
	 * @see #isSetAEnumerationType()
	 * @see #unsetAEnumerationType()
	 * @see #getAEnumerationType()
	 * @generated
	 */

	void setAEnumerationType(AEnumeration value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralValueExpr#getAEnumerationType <em>AEnumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAEnumerationType()
	 * @see #getAEnumerationType()
	 * @see #setAEnumerationType(AEnumeration)
	 * @generated
	 */
	void unsetAEnumerationType();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralValueExpr#getAEnumerationType <em>AEnumeration Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AEnumeration Type</em>' reference is set.
	 * @see #unsetAEnumerationType()
	 * @see #getAEnumerationType()
	 * @see #setAEnumerationType(AEnumeration)
	 * @generated
	 */
	boolean isSetAEnumerationType();

	/**
	 * Returns the value of the '<em><b>AValue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AValue</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AValue</em>' reference.
	 * @see #isSetAValue()
	 * @see #unsetAValue()
	 * @see #setAValue(ALiteral)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralValueExpr_AValue()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Value'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	ALiteral getAValue();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralValueExpr#getAValue <em>AValue</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AValue</em>' reference.
	 * @see #isSetAValue()
	 * @see #unsetAValue()
	 * @see #getAValue()
	 * @generated
	 */

	void setAValue(ALiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralValueExpr#getAValue <em>AValue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAValue()
	 * @see #getAValue()
	 * @see #setAValue(ALiteral)
	 * @generated
	 */
	void unsetAValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralValueExpr#getAValue <em>AValue</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AValue</em>' reference is set.
	 * @see #unsetAValue()
	 * @see #getAValue()
	 * @see #setAValue(ALiteral)
	 * @generated
	 */
	boolean isSetAValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if self.aEnumerationType.oclIsUndefined() then OrderedSet{}\r\nelse if self.aEnumerationType.aActiveEnumeration then aEnumerationType.aLiteral else OrderedSet{} endif endif'"
	 * @generated
	 */
	EList<ALiteral> getAllowedLiterals();

} // MLiteralValueExpr
