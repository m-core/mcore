/**
 */

package com.montages.mrules.expressions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MAbstractIf#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractIf#getThenPart <em>Then Part</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractIf#getElseifPart <em>Elseif Part</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractIf#getElsePart <em>Else Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractIf()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractIf extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #isSetCondition()
	 * @see #unsetCondition()
	 * @see #setCondition(MChainOrApplication)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractIf_Condition()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 * @generated
	 */
	MChainOrApplication getCondition();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #isSetCondition()
	 * @see #unsetCondition()
	 * @see #getCondition()
	 * @generated
	 */

	void setCondition(MChainOrApplication value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCondition()
	 * @see #getCondition()
	 * @see #setCondition(MChainOrApplication)
	 * @generated
	 */
	void unsetCondition();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getCondition <em>Condition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Condition</em>' containment reference is set.
	 * @see #unsetCondition()
	 * @see #getCondition()
	 * @see #setCondition(MChainOrApplication)
	 * @generated
	 */
	boolean isSetCondition();

	/**
	 * Returns the value of the '<em><b>Then Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Part</em>' containment reference.
	 * @see #isSetThenPart()
	 * @see #unsetThenPart()
	 * @see #setThenPart(MThen)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractIf_ThenPart()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 * @generated
	 */
	MThen getThenPart();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getThenPart <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Part</em>' containment reference.
	 * @see #isSetThenPart()
	 * @see #unsetThenPart()
	 * @see #getThenPart()
	 * @generated
	 */

	void setThenPart(MThen value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getThenPart <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetThenPart()
	 * @see #getThenPart()
	 * @see #setThenPart(MThen)
	 * @generated
	 */
	void unsetThenPart();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getThenPart <em>Then Part</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Then Part</em>' containment reference is set.
	 * @see #unsetThenPart()
	 * @see #getThenPart()
	 * @see #setThenPart(MThen)
	 * @generated
	 */
	boolean isSetThenPart();

	/**
	 * Returns the value of the '<em><b>Elseif Part</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MElseIf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elseif Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elseif Part</em>' containment reference list.
	 * @see #isSetElseifPart()
	 * @see #unsetElseifPart()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractIf_ElseifPart()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MElseIf> getElseifPart();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getElseifPart <em>Elseif Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElseifPart()
	 * @see #getElseifPart()
	 * @generated
	 */
	void unsetElseifPart();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getElseifPart <em>Elseif Part</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Elseif Part</em>' containment reference list is set.
	 * @see #unsetElseifPart()
	 * @see #getElseifPart()
	 * @generated
	 */
	boolean isSetElseifPart();

	/**
	 * Returns the value of the '<em><b>Else Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Part</em>' containment reference.
	 * @see #isSetElsePart()
	 * @see #unsetElsePart()
	 * @see #setElsePart(MElse)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractIf_ElsePart()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 * @generated
	 */
	MElse getElsePart();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getElsePart <em>Else Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Part</em>' containment reference.
	 * @see #isSetElsePart()
	 * @see #unsetElsePart()
	 * @see #getElsePart()
	 * @generated
	 */

	void setElsePart(MElse value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getElsePart <em>Else Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElsePart()
	 * @see #getElsePart()
	 * @see #setElsePart(MElse)
	 * @generated
	 */
	void unsetElsePart();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractIf#getElsePart <em>Else Part</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Else Part</em>' containment reference is set.
	 * @see #unsetElsePart()
	 * @see #getElsePart()
	 * @see #setElsePart(MElse)
	 * @generated
	 */
	boolean isSetElsePart();

} // MAbstractIf
