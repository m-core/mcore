/**
 */

package com.montages.mrules.expressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MProcessor Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MProcessorDefinition#getProcessor <em>Processor</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMProcessorDefinition()
 * @model annotation="http://www.xocl.org/OCL label='if self.processor.oclIsUndefined() \n      then \'\'\n      else self.processor.toString()\n         endif'"
 * @generated
 */

public interface MProcessorDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Processor</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.MProcessor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor</em>' attribute.
	 * @see com.montages.mrules.expressions.MProcessor
	 * @see #isSetProcessor()
	 * @see #unsetProcessor()
	 * @see #setProcessor(MProcessor)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMProcessorDefinition_Processor()
	 * @model unsettable="true"
	 * @generated
	 */
	MProcessor getProcessor();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MProcessorDefinition#getProcessor <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processor</em>' attribute.
	 * @see com.montages.mrules.expressions.MProcessor
	 * @see #isSetProcessor()
	 * @see #unsetProcessor()
	 * @see #getProcessor()
	 * @generated
	 */

	void setProcessor(MProcessor value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MProcessorDefinition#getProcessor <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProcessor()
	 * @see #getProcessor()
	 * @see #setProcessor(MProcessor)
	 * @generated
	 */
	void unsetProcessor();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MProcessorDefinition#getProcessor <em>Processor</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Processor</em>' attribute is set.
	 * @see #unsetProcessor()
	 * @see #getProcessor()
	 * @see #setProcessor(MProcessor)
	 * @generated
	 */
	boolean isSetProcessor();

} // MProcessorDefinition
