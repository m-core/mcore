/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AClassType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNew Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MNewObject#getANewType <em>ANew Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MNewObject#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNewObject()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL abstractEntryDerive='entry->asOrderedSet()\n' asBasicCodeDerive='let c:String= \'Tuple{\' in\r\n c.concat(\r\n    if entry->isEmpty() then \'}\' else  self.entry->iterate(\r\n\t\t  temp1 : MNewObjectFeatureValue; s: String = \'\' | \r\n\t\t  s.concat(if temp1.aFeature.aName.oclIsUndefined() then \'\' else temp1.aFeature.aName endif).concat(\'=\').concat(if temp1.value.asCode.oclIsUndefined() then \'\' else temp1.value.asCode endif).concat(if entry->at(entry->indexOf(temp1)+1).oclIsUndefined() then \'\' else \',\' endif )).concat(\'}\') endif)\r\n' kindLabelDerive='\'New Object\'\n'"
 * @generated
 */

public interface MNewObject extends MAbstractNamedTuple {
	/**
	 * Returns the value of the '<em><b>ANew Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ANew Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ANew Type</em>' reference.
	 * @see #isSetANewType()
	 * @see #unsetANewType()
	 * @see #setANewType(AClassType)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNewObject_ANewType()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='New Type'"
	 * @generated
	 */
	AClassType getANewType();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MNewObject#getANewType <em>ANew Type</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ANew Type</em>' reference.
	 * @see #isSetANewType()
	 * @see #unsetANewType()
	 * @see #getANewType()
	 * @generated
	 */

	void setANewType(AClassType value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MNewObject#getANewType <em>ANew Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetANewType()
	 * @see #getANewType()
	 * @see #setANewType(AClassType)
	 * @generated
	 */
	void unsetANewType();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MNewObject#getANewType <em>ANew Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ANew Type</em>' reference is set.
	 * @see #unsetANewType()
	 * @see #getANewType()
	 * @see #setANewType(AClassType)
	 * @generated
	 */
	boolean isSetANewType();

	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MNewObjectFeatureValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference list.
	 * @see #isSetEntry()
	 * @see #unsetEntry()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNewObject_Entry()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MNewObjectFeatureValue> getEntry();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MNewObject#getEntry <em>Entry</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEntry()
	 * @see #getEntry()
	 * @generated
	 */
	void unsetEntry();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MNewObject#getEntry <em>Entry</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Entry</em>' containment reference list is set.
	 * @see #unsetEntry()
	 * @see #getEntry()
	 * @generated
	 */
	boolean isSetEntry();

} // MNewObject
