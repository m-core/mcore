/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AFeature;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MNewObjectFeatureValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MNew Object Feature Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MNewObjectFeatureValueImpl#getAFeature <em>AFeature</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MNewObjectFeatureValueImpl extends MAbstractTupleEntryImpl implements MNewObjectFeatureValue {
	/**
	 * The cached value of the '{@link #getAFeature() <em>AFeature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeature()
	 * @generated
	 * @ordered
	 */
	protected AFeature aFeature;

	/**
	 * This is true if the AFeature reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aFeatureESet;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAFeature <em>AFeature</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeature
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aFeatureChoiceConstructionOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNewObjectFeatureValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MNEW_OBJECT_FEATURE_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getAFeature() {
		if (aFeature != null && aFeature.eIsProxy()) {
			InternalEObject oldAFeature = (InternalEObject) aFeature;
			aFeature = (AFeature) eResolveProxy(oldAFeature);
			if (aFeature != oldAFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE, oldAFeature, aFeature));
			}
		}
		return aFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetAFeature() {
		return aFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAFeature(AFeature newAFeature) {
		AFeature oldAFeature = aFeature;
		aFeature = newAFeature;
		boolean oldAFeatureESet = aFeatureESet;
		aFeatureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE, oldAFeature, aFeature, !oldAFeatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAFeature() {
		AFeature oldAFeature = aFeature;
		boolean oldAFeatureESet = aFeatureESet;
		aFeature = null;
		aFeatureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE, oldAFeature, null, oldAFeatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAFeature() {
		return aFeatureESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AFeature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<AFeature>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if self.eContainer().oclAsType(mrules::expressions::MNewObject).aNewType.oclIsUndefined() then 
	OrderedSet{} else
	let type : acore::classifiers::AClassType= 
	self.eContainer().oclAsType(mrules::expressions::MNewObject).aNewType in
	type.aAllStoredFeature endif
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<AFeature> evalAFeatureChoiceConstruction(List<AFeature> choice) {
		EClass eClass = ExpressionsPackage.Literals.MNEW_OBJECT_FEATURE_VALUE;
		if (aFeatureChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MNEW_OBJECT_FEATURE_VALUE__AFEATURE;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aFeatureChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AFeatureChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aFeatureChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AFeatureChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<AFeature> result = new ArrayList<AFeature>((Collection<AFeature>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE:
			if (resolve)
				return getAFeature();
			return basicGetAFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE:
			setAFeature((AFeature) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE:
			unsetAFeature();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE__AFEATURE:
			return isSetAFeature();
		}
		return super.eIsSet(featureID);
	}

} //MNewObjectFeatureValueImpl
