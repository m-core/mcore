/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MChainOrApplication;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MChain Or Application</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public abstract class MChainOrApplicationImpl extends MToplevelExpressionImpl implements MChainOrApplication {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MChainOrApplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MCHAIN_OR_APPLICATION;
	}

} //MChainOrApplicationImpl
