/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AEnumeration;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.acore.values.ALiteral;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MLiteralValueExpr;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MLiteral Value Expr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MLiteralValueExprImpl#getAEnumerationType <em>AEnumeration Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MLiteralValueExprImpl#getAValue <em>AValue</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MLiteralValueExprImpl extends MChainOrApplicationImpl implements MLiteralValueExpr {
	/**
	 * The cached value of the '{@link #getAEnumerationType() <em>AEnumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAEnumerationType()
	 * @generated
	 * @ordered
	 */
	protected AEnumeration aEnumerationType;

	/**
	 * This is true if the AEnumeration Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aEnumerationTypeESet;

	/**
	 * The cached value of the '{@link #getAValue() <em>AValue</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAValue()
	 * @generated
	 * @ordered
	 */
	protected ALiteral aValue;

	/**
	 * This is true if the AValue reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aValueESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #getAllowedLiterals <em>Get Allowed Literals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedLiterals
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getAllowedLiteralsBodyOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getAEnumerationType <em>AEnumeration Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAEnumerationType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression aEnumerationTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAValue <em>AValue</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAValue
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aValueChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MLiteralValueExprImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AEnumeration getAEnumerationType() {
		if (aEnumerationType != null && aEnumerationType.eIsProxy()) {
			InternalEObject oldAEnumerationType = (InternalEObject) aEnumerationType;
			aEnumerationType = (AEnumeration) eResolveProxy(oldAEnumerationType);
			if (aEnumerationType != oldAEnumerationType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE, oldAEnumerationType,
							aEnumerationType));
			}
		}
		return aEnumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AEnumeration basicGetAEnumerationType() {
		return aEnumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAEnumerationType(AEnumeration newAEnumerationType) {
		AEnumeration oldAEnumerationType = aEnumerationType;
		aEnumerationType = newAEnumerationType;
		boolean oldAEnumerationTypeESet = aEnumerationTypeESet;
		aEnumerationTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE, oldAEnumerationType, aEnumerationType,
					!oldAEnumerationTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAEnumerationType() {
		AEnumeration oldAEnumerationType = aEnumerationType;
		boolean oldAEnumerationTypeESet = aEnumerationTypeESet;
		aEnumerationType = null;
		aEnumerationTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE, oldAEnumerationType, null,
					oldAEnumerationTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAEnumerationType() {
		return aEnumerationTypeESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>AEnumeration Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type AEnumeration
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.aActiveEnumeration 
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalAEnumerationTypeChoiceConstraint(AEnumeration trg) {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR;
		if (aEnumerationTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				aEnumerationTypeChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "AEnumerationTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(aEnumerationTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass,
					"AEnumerationTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral getAValue() {
		if (aValue != null && aValue.eIsProxy()) {
			InternalEObject oldAValue = (InternalEObject) aValue;
			aValue = (ALiteral) eResolveProxy(oldAValue);
			if (aValue != oldAValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE, oldAValue, aValue));
			}
		}
		return aValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral basicGetAValue() {
		return aValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAValue(ALiteral newAValue) {
		ALiteral oldAValue = aValue;
		aValue = newAValue;
		boolean oldAValueESet = aValueESet;
		aValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE,
					oldAValue, aValue, !oldAValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAValue() {
		ALiteral oldAValue = aValue;
		boolean oldAValueESet = aValueESet;
		aValue = null;
		aValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE,
					oldAValue, null, oldAValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAValue() {
		return aValueESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AValue</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<ALiteral>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL getAllowedLiterals()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<ALiteral> evalAValueChoiceConstruction(List<ALiteral> choice) {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR;
		if (aValueChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR__AVALUE;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aValueChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AValueChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aValueChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AValueChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<ALiteral> result = new ArrayList<ALiteral>((Collection<ALiteral>) query.evaluate(this));
			result.remove(null);
			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ALiteral> getAllowedLiterals() {

		/**
		 * @OCL if self.aEnumerationType.oclIsUndefined() then OrderedSet{}
		else if self.aEnumerationType.aActiveEnumeration then aEnumerationType.aLiteral else OrderedSet{} endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EOperation eOperation = ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR.getEOperations().get(0);
		if (getAllowedLiteralsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				getAllowedLiteralsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getAllowedLiteralsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<ALiteral>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE:
			if (resolve)
				return getAEnumerationType();
			return basicGetAEnumerationType();
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE:
			if (resolve)
				return getAValue();
			return basicGetAValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE:
			setAEnumerationType((AEnumeration) newValue);
			return;
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE:
			setAValue((ALiteral) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE:
			unsetAEnumerationType();
			return;
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE:
			unsetAValue();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE:
			return isSetAEnumerationType();
		case ExpressionsPackage.MLITERAL_VALUE_EXPR__AVALUE:
			return isSetAValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MLITERAL_VALUE_EXPR___GET_ALLOWED_LITERALS:
			return getAllowedLiterals();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Literal'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isComplexExpression false
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsComplexExpression() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory true
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular true
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType aEnumerationType
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType acore::classifiers::ASimpleType::None
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode if aValue.oclIsUndefined() then 'null'
	else aValue.aQualifiedName endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MLiteralValueExprImpl
