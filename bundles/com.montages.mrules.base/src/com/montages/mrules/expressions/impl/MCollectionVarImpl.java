/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.abstractions.ANamed;
import com.montages.acore.abstractions.AVariable;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.mrules.MRulesNamed;
import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MCollectionExpression;
import com.montages.mrules.expressions.MCollectionVar;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MCollection Var</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getAName <em>AName</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getABusinessName <em>ABusiness Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getSpecialEName <em>Special EName</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getShortName <em>Short Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getEName <em>EName</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getFullLabel <em>Full Label</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getLocalStructuralName <em>Local Structural Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getCalculatedName <em>Calculated Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getCalculatedShortName <em>Calculated Short Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getCorrectName <em>Correct Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getCorrectShortName <em>Correct Short Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionVarImpl#getContainingCollection <em>Containing Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MCollectionVarImpl extends MAbstractExpressionImpl implements MCollectionVar {
	/**
	 * The default value of the '{@link #getAName() <em>AName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName()
	 * @generated
	 * @ordered
	 */
	protected static final String ANAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedNameConstant() <em>AUndefined Name Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_NAME_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABusinessName() <em>ABusiness Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName()
	 * @generated
	 * @ordered
	 */
	protected static final String ABUSINESS_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSpecialEName() <em>Special EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialEName()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_ENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialEName() <em>Special EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialEName()
	 * @generated
	 * @ordered
	 */
	protected String specialEName = SPECIAL_ENAME_EDEFAULT;

	/**
	 * This is true if the Special EName attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean specialENameESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The default value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected String shortName = SHORT_NAME_EDEFAULT;

	/**
	 * This is true if the Short Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean shortNameESet;

	/**
	 * The default value of the '{@link #getEName() <em>EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName()
	 * @generated
	 * @ordered
	 */
	protected static final String ENAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getFullLabel() <em>Full Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String FULL_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLocalStructuralName() <em>Local Structural Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalStructuralName()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_STRUCTURAL_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedName() <em>Calculated Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedName()
	 * @generated
	 * @ordered
	 */
	protected static final String CALCULATED_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedShortName() <em>Calculated Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String CALCULATED_SHORT_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCorrectName() <em>Correct Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectName()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECT_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCorrectShortName() <em>Correct Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectShortName()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECT_SHORT_NAME_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameName <em>Same Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameNamemrulesMRulesNamedBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameShortName <em>Same Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameShortName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameShortNamemrulesMRulesNamedBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameString <em>Same String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringEmpty <em>String Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAName <em>AName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedNameConstant <em>AUndefined Name Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedNameConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABusinessName <em>ABusiness Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBusinessNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel <em>Full Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalStructuralName <em>Local Structural Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalStructuralName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localStructuralNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedName <em>Calculated Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedShortName <em>Calculated Short Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedShortName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectName <em>Correct Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectShortName <em>Correct Short Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectShortName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingCollection <em>Containing Collection</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingCollection
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingCollectionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCollectionVarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MCOLLECTION_VAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAName() {
		/**
		 * @OCL aUndefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ANAME;

		if (aNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedNameConstant() {
		/**
		 * @OCL '<A Name Is Undefined> '
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT;

		if (aUndefinedNameConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedNameConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedNameConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABusinessName() {
		/**
		 * @OCL let chain : String = aName in
		if chain.oclIsUndefined()
		then null
		else chain .camelCaseToBusiness()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME;

		if (aBusinessNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBusinessNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBusinessNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialEName() {
		return specialEName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialEName(String newSpecialEName) {
		String oldSpecialEName = specialEName;
		specialEName = newSpecialEName;
		boolean oldSpecialENameESet = specialENameESet;
		specialENameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME,
					oldSpecialEName, specialEName, !oldSpecialENameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpecialEName() {
		String oldSpecialEName = specialEName;
		boolean oldSpecialENameESet = specialENameESet;
		specialEName = SPECIAL_ENAME_EDEFAULT;
		specialENameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME,
					oldSpecialEName, SPECIAL_ENAME_EDEFAULT, oldSpecialENameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpecialEName() {
		return specialENameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MCOLLECTION_VAR__NAME, oldName,
					name, !oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		String oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MCOLLECTION_VAR__NAME, oldName,
					NAME_EDEFAULT, oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortName(String newShortName) {
		String oldShortName = shortName;
		shortName = newShortName;
		boolean oldShortNameESet = shortNameESet;
		shortNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME,
					oldShortName, shortName, !oldShortNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetShortName() {
		String oldShortName = shortName;
		boolean oldShortNameESet = shortNameESet;
		shortName = SHORT_NAME_EDEFAULT;
		shortNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME,
					oldShortName, SHORT_NAME_EDEFAULT, oldShortNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetShortName() {
		return shortNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEName() {
		/**
		 * @OCL if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true
		then self.calculatedShortName.camelCaseLower()
		else self.specialEName.camelCaseLower()
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFullLabel() {
		/**
		 * @OCL 'OVERRIDE IN SUBCLASS '.concat(self.calculatedName)
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalStructuralName() {
		/**
		 * @OCL ''
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__LOCAL_STRUCTURAL_NAME;

		if (localStructuralNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localStructuralNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localStructuralNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCalculatedName() {
		/**
		 * @OCL if stringEmpty(name) then ' NAME MISSING' else name endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__CALCULATED_NAME;

		if (calculatedNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				calculatedNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCalculatedShortName() {
		/**
		 * @OCL if stringEmpty(name) or stringEmpty(shortName) then calculatedName else shortName endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__CALCULATED_SHORT_NAME;

		if (calculatedShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				calculatedShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectName() {
		/**
		 * @OCL not stringEmpty(name)
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__CORRECT_NAME;

		if (correctNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				correctNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectShortName() {
		/**
		 * @OCL  stringEmpty(shortName)
		or (not stringEmpty(name))
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULES_NAMED__CORRECT_SHORT_NAME;

		if (correctShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				correctShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getContainingCollection() {
		MCollectionExpression containingCollection = basicGetContainingCollection();
		return containingCollection != null && containingCollection.eIsProxy()
				? (MCollectionExpression) eResolveProxy((InternalEObject) containingCollection) : containingCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetContainingCollection() {
		/**
		 * @OCL if eContainer().oclIsKindOf(MCollectionExpression)
		then eContainer().oclAsType(MCollectionExpression)
		else null endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_VAR;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MCOLLECTION_VAR__CONTAINING_COLLECTION;

		if (containingCollectionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				containingCollectionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingCollectionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MCollectionExpression result = (MCollectionExpression) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameName(MRulesNamed n) {

		/**
		 * @OCL sameString(name, n.name)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULES_NAMED);
		EOperation eOperation = MrulesPackage.Literals.MRULES_NAMED.getEOperations().get(0);
		if (sameNamemrulesMRulesNamedBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				sameNamemrulesMRulesNamedBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(sameNamemrulesMRulesNamedBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("n", n);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameShortName(MRulesNamed n) {

		/**
		 * @OCL if stringEmpty(shortName)  then
		sameString(name, n.shortName)
		else if  stringEmpty(n.shortName) then
		sameString(shortName, n.name)
		else sameString(shortName, n.shortName)
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULES_NAMED);
		EOperation eOperation = MrulesPackage.Literals.MRULES_NAMED.getEOperations().get(1);
		if (sameShortNamemrulesMRulesNamedBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				sameShortNamemrulesMRulesNamedBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(sameShortNamemrulesMRulesNamedBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("n", n);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameString(String s1, String s2) {

		/**
		 * @OCL s1=s2 
		or 
		(s1.oclIsUndefined() and s2='')
		or
		(s1='' and s2.oclIsUndefined())
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULES_NAMED);
		EOperation eOperation = MrulesPackage.Literals.MRULES_NAMED.getEOperations().get(2);
		if (sameStringecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				sameStringecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(sameStringecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s1", s1);

			evalEnv.add("s2", s2);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean stringEmpty(String s) {

		/**
		 * @OCL s.oclIsUndefined() or s=''
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULES_NAMED);
		EOperation eOperation = MrulesPackage.Literals.MRULES_NAMED.getEOperations().get(3);
		if (stringEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_VAR, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_VAR__ANAME:
			return getAName();
		case ExpressionsPackage.MCOLLECTION_VAR__AUNDEFINED_NAME_CONSTANT:
			return getAUndefinedNameConstant();
		case ExpressionsPackage.MCOLLECTION_VAR__ABUSINESS_NAME:
			return getABusinessName();
		case ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME:
			return getSpecialEName();
		case ExpressionsPackage.MCOLLECTION_VAR__NAME:
			return getName();
		case ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME:
			return getShortName();
		case ExpressionsPackage.MCOLLECTION_VAR__ENAME:
			return getEName();
		case ExpressionsPackage.MCOLLECTION_VAR__FULL_LABEL:
			return getFullLabel();
		case ExpressionsPackage.MCOLLECTION_VAR__LOCAL_STRUCTURAL_NAME:
			return getLocalStructuralName();
		case ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_NAME:
			return getCalculatedName();
		case ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_SHORT_NAME:
			return getCalculatedShortName();
		case ExpressionsPackage.MCOLLECTION_VAR__CORRECT_NAME:
			return getCorrectName();
		case ExpressionsPackage.MCOLLECTION_VAR__CORRECT_SHORT_NAME:
			return getCorrectShortName();
		case ExpressionsPackage.MCOLLECTION_VAR__CONTAINING_COLLECTION:
			if (resolve)
				return getContainingCollection();
			return basicGetContainingCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME:
			setSpecialEName((String) newValue);
			return;
		case ExpressionsPackage.MCOLLECTION_VAR__NAME:
			setName((String) newValue);
			return;
		case ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME:
			setShortName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME:
			unsetSpecialEName();
			return;
		case ExpressionsPackage.MCOLLECTION_VAR__NAME:
			unsetName();
			return;
		case ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME:
			unsetShortName();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_VAR__ANAME:
			return ANAME_EDEFAULT == null ? getAName() != null : !ANAME_EDEFAULT.equals(getAName());
		case ExpressionsPackage.MCOLLECTION_VAR__AUNDEFINED_NAME_CONSTANT:
			return AUNDEFINED_NAME_CONSTANT_EDEFAULT == null ? getAUndefinedNameConstant() != null
					: !AUNDEFINED_NAME_CONSTANT_EDEFAULT.equals(getAUndefinedNameConstant());
		case ExpressionsPackage.MCOLLECTION_VAR__ABUSINESS_NAME:
			return ABUSINESS_NAME_EDEFAULT == null ? getABusinessName() != null
					: !ABUSINESS_NAME_EDEFAULT.equals(getABusinessName());
		case ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME:
			return isSetSpecialEName();
		case ExpressionsPackage.MCOLLECTION_VAR__NAME:
			return isSetName();
		case ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME:
			return isSetShortName();
		case ExpressionsPackage.MCOLLECTION_VAR__ENAME:
			return ENAME_EDEFAULT == null ? getEName() != null : !ENAME_EDEFAULT.equals(getEName());
		case ExpressionsPackage.MCOLLECTION_VAR__FULL_LABEL:
			return FULL_LABEL_EDEFAULT == null ? getFullLabel() != null : !FULL_LABEL_EDEFAULT.equals(getFullLabel());
		case ExpressionsPackage.MCOLLECTION_VAR__LOCAL_STRUCTURAL_NAME:
			return LOCAL_STRUCTURAL_NAME_EDEFAULT == null ? getLocalStructuralName() != null
					: !LOCAL_STRUCTURAL_NAME_EDEFAULT.equals(getLocalStructuralName());
		case ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_NAME:
			return CALCULATED_NAME_EDEFAULT == null ? getCalculatedName() != null
					: !CALCULATED_NAME_EDEFAULT.equals(getCalculatedName());
		case ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_SHORT_NAME:
			return CALCULATED_SHORT_NAME_EDEFAULT == null ? getCalculatedShortName() != null
					: !CALCULATED_SHORT_NAME_EDEFAULT.equals(getCalculatedShortName());
		case ExpressionsPackage.MCOLLECTION_VAR__CORRECT_NAME:
			return CORRECT_NAME_EDEFAULT == null ? getCorrectName() != null
					: !CORRECT_NAME_EDEFAULT.equals(getCorrectName());
		case ExpressionsPackage.MCOLLECTION_VAR__CORRECT_SHORT_NAME:
			return CORRECT_SHORT_NAME_EDEFAULT == null ? getCorrectShortName() != null
					: !CORRECT_SHORT_NAME_EDEFAULT.equals(getCorrectShortName());
		case ExpressionsPackage.MCOLLECTION_VAR__CONTAINING_COLLECTION:
			return basicGetContainingCollection() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MCOLLECTION_VAR__ANAME:
				return AbstractionsPackage.ANAMED__ANAME;
			case ExpressionsPackage.MCOLLECTION_VAR__AUNDEFINED_NAME_CONSTANT:
				return AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;
			case ExpressionsPackage.MCOLLECTION_VAR__ABUSINESS_NAME:
				return AbstractionsPackage.ANAMED__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == MRulesNamed.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME:
				return MrulesPackage.MRULES_NAMED__SPECIAL_ENAME;
			case ExpressionsPackage.MCOLLECTION_VAR__NAME:
				return MrulesPackage.MRULES_NAMED__NAME;
			case ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME:
				return MrulesPackage.MRULES_NAMED__SHORT_NAME;
			case ExpressionsPackage.MCOLLECTION_VAR__ENAME:
				return MrulesPackage.MRULES_NAMED__ENAME;
			case ExpressionsPackage.MCOLLECTION_VAR__FULL_LABEL:
				return MrulesPackage.MRULES_NAMED__FULL_LABEL;
			case ExpressionsPackage.MCOLLECTION_VAR__LOCAL_STRUCTURAL_NAME:
				return MrulesPackage.MRULES_NAMED__LOCAL_STRUCTURAL_NAME;
			case ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_NAME:
				return MrulesPackage.MRULES_NAMED__CALCULATED_NAME;
			case ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_SHORT_NAME:
				return MrulesPackage.MRULES_NAMED__CALCULATED_SHORT_NAME;
			case ExpressionsPackage.MCOLLECTION_VAR__CORRECT_NAME:
				return MrulesPackage.MRULES_NAMED__CORRECT_NAME;
			case ExpressionsPackage.MCOLLECTION_VAR__CORRECT_SHORT_NAME:
				return MrulesPackage.MRULES_NAMED__CORRECT_SHORT_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == AVariable.class) {
			switch (derivedFeatureID) {
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ANAMED__ANAME:
				return ExpressionsPackage.MCOLLECTION_VAR__ANAME;
			case AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
				return ExpressionsPackage.MCOLLECTION_VAR__AUNDEFINED_NAME_CONSTANT;
			case AbstractionsPackage.ANAMED__ABUSINESS_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == MRulesNamed.class) {
			switch (baseFeatureID) {
			case MrulesPackage.MRULES_NAMED__SPECIAL_ENAME:
				return ExpressionsPackage.MCOLLECTION_VAR__SPECIAL_ENAME;
			case MrulesPackage.MRULES_NAMED__NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__NAME;
			case MrulesPackage.MRULES_NAMED__SHORT_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__SHORT_NAME;
			case MrulesPackage.MRULES_NAMED__ENAME:
				return ExpressionsPackage.MCOLLECTION_VAR__ENAME;
			case MrulesPackage.MRULES_NAMED__FULL_LABEL:
				return ExpressionsPackage.MCOLLECTION_VAR__FULL_LABEL;
			case MrulesPackage.MRULES_NAMED__LOCAL_STRUCTURAL_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__LOCAL_STRUCTURAL_NAME;
			case MrulesPackage.MRULES_NAMED__CALCULATED_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_NAME;
			case MrulesPackage.MRULES_NAMED__CALCULATED_SHORT_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__CALCULATED_SHORT_NAME;
			case MrulesPackage.MRULES_NAMED__CORRECT_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__CORRECT_NAME;
			case MrulesPackage.MRULES_NAMED__CORRECT_SHORT_NAME:
				return ExpressionsPackage.MCOLLECTION_VAR__CORRECT_SHORT_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == AVariable.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		if (baseClass == MRulesNamed.class) {
			switch (baseOperationID) {
			case MrulesPackage.MRULES_NAMED___SAME_NAME__MRULESNAMED:
				return ExpressionsPackage.MCOLLECTION_VAR___SAME_NAME__MRULESNAMED;
			case MrulesPackage.MRULES_NAMED___SAME_SHORT_NAME__MRULESNAMED:
				return ExpressionsPackage.MCOLLECTION_VAR___SAME_SHORT_NAME__MRULESNAMED;
			case MrulesPackage.MRULES_NAMED___SAME_STRING__STRING_STRING:
				return ExpressionsPackage.MCOLLECTION_VAR___SAME_STRING__STRING_STRING;
			case MrulesPackage.MRULES_NAMED___STRING_EMPTY__STRING:
				return ExpressionsPackage.MCOLLECTION_VAR___STRING_EMPTY__STRING;
			default:
				return -1;
			}
		}
		if (baseClass == AVariable.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MCOLLECTION_VAR___SAME_NAME__MRULESNAMED:
			return sameName((MRulesNamed) arguments.get(0));
		case ExpressionsPackage.MCOLLECTION_VAR___SAME_SHORT_NAME__MRULESNAMED:
			return sameShortName((MRulesNamed) arguments.get(0));
		case ExpressionsPackage.MCOLLECTION_VAR___SAME_STRING__STRING_STRING:
			return sameString((String) arguments.get(0), (String) arguments.get(1));
		case ExpressionsPackage.MCOLLECTION_VAR___STRING_EMPTY__STRING:
			return stringEmpty((String) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specialEName: ");
		if (specialENameESet)
			result.append(specialEName);
		else
			result.append("<unset>");
		result.append(", name: ");
		if (nameESet)
			result.append(name);
		else
			result.append("<unset>");
		result.append(", shortName: ");
		if (shortNameESet)
			result.append(shortName);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel aName
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_VAR);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MCollectionVarImpl
