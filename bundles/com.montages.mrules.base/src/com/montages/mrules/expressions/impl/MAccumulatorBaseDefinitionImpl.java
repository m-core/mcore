/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.expressions.ExpressionBase;
import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAccumulator;
import com.montages.mrules.expressions.MAccumulatorBaseDefinition;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAccumulator Base Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MAccumulatorBaseDefinitionImpl#getAccumulator <em>Accumulator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MAccumulatorBaseDefinitionImpl extends MAbstractBaseDefinitionImpl implements MAccumulatorBaseDefinition {
	/**
	 * The cached value of the '{@link #getAccumulator() <em>Accumulator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccumulator()
	 * @generated
	 * @ordered
	 */
	protected MAccumulator accumulator;

	/**
	 * This is true if the Accumulator reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean accumulatorESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedBase <em>Calculated Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedAsCode <em>Calculated As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMandatory <em>AMandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASingular <em>ASingular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASimpleType <em>ASimple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAccumulatorBaseDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulator getAccumulator() {
		if (accumulator != null && accumulator.eIsProxy()) {
			InternalEObject oldAccumulator = (InternalEObject) accumulator;
			accumulator = (MAccumulator) eResolveProxy(oldAccumulator);
			if (accumulator != oldAccumulator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR, oldAccumulator, accumulator));
			}
		}
		return accumulator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulator basicGetAccumulator() {
		return accumulator;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccumulator(MAccumulator newAccumulator) {
		MAccumulator oldAccumulator = accumulator;
		accumulator = newAccumulator;
		boolean oldAccumulatorESet = accumulatorESet;
		accumulatorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR, oldAccumulator, accumulator,
					!oldAccumulatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAccumulator() {
		MAccumulator oldAccumulator = accumulator;
		boolean oldAccumulatorESet = accumulatorESet;
		accumulator = null;
		accumulatorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR, oldAccumulator, null,
					oldAccumulatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAccumulator() {
		return accumulatorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR:
			if (resolve)
				return getAccumulator();
			return basicGetAccumulator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR:
			setAccumulator((MAccumulator) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR:
			unsetAccumulator();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR:
			return isSetAccumulator();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL '<accumulator> '.concat(if accumulator.oclIsUndefined() then '' else accumulator.aName endif)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedBase ExpressionBase::Accumulator
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ExpressionBase getCalculatedBase() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__CALCULATED_BASE;

		if (calculatedBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ExpressionBase) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedAsCode if accumulator.oclIsUndefined() then 'MISSING ACCUMULATOR' else accumulator.aName endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedAsCode() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__CALCULATED_AS_CODE;

		if (calculatedAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aMandatory if accumulator.oclIsUndefined() then false
	else accumulator.aMandatory endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getAMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__AMANDATORY;

		if (aMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aSingular if accumulator.oclIsUndefined() then true
	else accumulator.aSingular endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getASingular() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ASINGULAR;

		if (aSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aSimpleType if accumulator.oclIsUndefined() then acore::classifiers::ASimpleType::None
	else accumulator.aSimpleType
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getASimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ASIMPLE_TYPE;

		if (aSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aClassifier if accumulator.oclIsUndefined() then null
	else accumulator.aClassifier
	endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAClassifier() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MAccumulatorBaseDefinitionImpl
