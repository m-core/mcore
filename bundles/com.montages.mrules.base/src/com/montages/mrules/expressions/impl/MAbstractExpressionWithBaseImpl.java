/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.abstractions.AVariable;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.expressions.ExpressionBase;
import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractBaseDefinition;
import com.montages.mrules.expressions.MAbstractExpressionWithBase;
import com.montages.mrules.expressions.MAccumulatorBaseDefinition;
import com.montages.mrules.expressions.MIteratorBaseDefinition;
import com.montages.mrules.expressions.MLiteralConstantBaseDefinition;
import com.montages.mrules.expressions.MObjectReferenceConstantBaseDefinition;
import com.montages.mrules.expressions.MParameterBaseDefinition;
import com.montages.mrules.expressions.MSimpleTypeConstantBaseDefinition;
import com.montages.mrules.expressions.MVariableBaseDefinition;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Expression With Base</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseAsCode <em>Base As Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getBase <em>Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseDefinition <em>Base Definition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getABaseVar <em>ABase Var</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getABaseExitType <em>ABase Exit Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getABaseExitSimpleType <em>ABase Exit Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseExitMandatory <em>Base Exit Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseExitSingular <em>Base Exit Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MAbstractExpressionWithBaseImpl extends MAbstractExpressionImpl implements MAbstractExpressionWithBase {
	/**
	 * The default value of the '{@link #getBaseAsCode() <em>Base As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_AS_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected static final ExpressionBase BASE_EDEFAULT = ExpressionBase.UNDEFINED;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected ExpressionBase base = BASE_EDEFAULT;

	/**
	 * This is true if the Base attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean baseESet;

	/**
	 * The cached value of the '{@link #getABaseVar() <em>ABase Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABaseVar()
	 * @generated
	 * @ordered
	 */
	protected AVariable aBaseVar;

	/**
	 * This is true if the ABase Var reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aBaseVarESet;

	/**
	 * The default value of the '{@link #getABaseExitSimpleType() <em>ABase Exit Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABaseExitSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ABASE_EXIT_SIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The default value of the '{@link #getBaseExitMandatory() <em>Base Exit Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BASE_EXIT_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBaseExitSingular() <em>Base Exit Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BASE_EXIT_SINGULAR_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #baseDefinition$Update <em>Base Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #baseDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression baseDefinition$UpdateexpressionsMAbstractBaseDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForBuiltIn <em>As Code For Built In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForBuiltIn
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForBuiltInBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForConstants <em>As Code For Constants</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForConstants
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForConstantsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForVariables <em>As Code For Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForVariables
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForVariablesBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseAsCode <em>Base As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseDefinition <em>Base Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getBaseDefinition <em>Base Definition</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDefinition
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression baseDefinitionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABaseExitType <em>ABase Exit Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABaseExitType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBaseExitTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABaseExitSimpleType <em>ABase Exit Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABaseExitSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBaseExitSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitMandatory <em>Base Exit Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitSingular <em>Base Exit Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitSingularDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractExpressionWithBaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBaseAsCode() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then ''
		else if baseDefinition.oclIsTypeOf(mrules::expressions::MNumberBaseDefinition)
		then  baseDefinition.oclAsType(mrules::expressions::MNumberBaseDefinition).calculateAsCode(self)
		else baseDefinition.calculatedAsCode
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE;

		if (baseAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				baseAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionBase getBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(ExpressionBase newBase) {
		ExpressionBase oldBase = base;
		base = newBase == null ? BASE_EDEFAULT : newBase;
		boolean oldBaseESet = baseESet;
		baseESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE, oldBase, base, !oldBaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBase() {
		ExpressionBase oldBase = base;
		boolean oldBaseESet = baseESet;
		base = BASE_EDEFAULT;
		baseESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE, oldBase, BASE_EDEFAULT, oldBaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBase() {
		return baseESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinition getBaseDefinition() {
		MAbstractBaseDefinition baseDefinition = basicGetBaseDefinition();
		return baseDefinition != null && baseDefinition.eIsProxy()
				? (MAbstractBaseDefinition) eResolveProxy((InternalEObject) baseDefinition) : baseDefinition;
	}

	/**
	 * 
	 *  
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setBaseDefinition(MAbstractBaseDefinition newBaseDefinition) {
		/* NOTE: After you changed this you must also change basicGetBaseDefinition() in order to retrieve the selected base variable. */

		if (newBaseDefinition == null) {
			unsetBase();
		} else {
			setBase(newBaseDefinition.getCalculatedBase());
			if (newBaseDefinition instanceof MSimpleTypeConstantBaseDefinition) {
				MSimpleTypeConstantBaseDefinition stcbd = (MSimpleTypeConstantBaseDefinition) newBaseDefinition;
				setABaseVar(stcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MLiteralConstantBaseDefinition) {
				MLiteralConstantBaseDefinition lcbd = (MLiteralConstantBaseDefinition) newBaseDefinition;
				setABaseVar(lcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MObjectReferenceConstantBaseDefinition) {
				MObjectReferenceConstantBaseDefinition orcbd = (MObjectReferenceConstantBaseDefinition) newBaseDefinition;
				setABaseVar(orcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MVariableBaseDefinition) {
				MVariableBaseDefinition vbd = (MVariableBaseDefinition) newBaseDefinition;
				setABaseVar(vbd.getNamedExpression());
			} else if (newBaseDefinition instanceof MParameterBaseDefinition) {
				MParameterBaseDefinition pbd = (MParameterBaseDefinition) newBaseDefinition;
				setABaseVar(pbd.getAParameter());
			} else if (newBaseDefinition instanceof MIteratorBaseDefinition) {
				MIteratorBaseDefinition ibd = (MIteratorBaseDefinition) newBaseDefinition;
				setABaseVar(ibd.getIterator());
			} else if (newBaseDefinition instanceof MAccumulatorBaseDefinition) {
				MAccumulatorBaseDefinition abd = (MAccumulatorBaseDefinition) newBaseDefinition;
				setABaseVar(abd.getAccumulator());
			}
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Base Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MAbstractBaseDefinition>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.getScope()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MAbstractBaseDefinition> evalBaseDefinitionChoiceConstruction(List<MAbstractBaseDefinition> choice) {
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		if (baseDefinitionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				baseDefinitionChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "BaseDefinitionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(baseDefinitionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass,
					"BaseDefinitionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MAbstractBaseDefinition> result = new ArrayList<MAbstractBaseDefinition>(
					(Collection<MAbstractBaseDefinition>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinition basicGetBaseDefinition() {
		/**
		 * @OCL let r: mrules::expressions::MAbstractBaseDefinition =
		
		if base=mrules::expressions::ExpressionBase::Undefined 
		then null
		else if base=mrules::expressions::ExpressionBase::SelfObject
		then scopeSelf->first() 
		else if base=mrules::expressions::ExpressionBase::EContainer
		then scopeContainerBase->first() 
		else if base=mrules::expressions::ExpressionBase::Target
		then scopeTrg->first() 
		else if base=mrules::expressions::ExpressionBase::ObjectObject
		then scopeObj->first() 
		else if base=mrules::expressions::ExpressionBase::Source
		then scopeSource->first() 
		else if base=mrules::expressions::ExpressionBase::ConstantValue 
		then scopeSimpleTypeConstants->select(c:mrules::expressions::MSimpleTypeConstantBaseDefinition | c.namedConstant = aBaseVar)->first()  
		else if base=mrules::expressions::ExpressionBase::ConstantLiteralValue
		then scopeLiteralConstants->select(c:mrules::expressions::MLiteralConstantBaseDefinition | c.namedConstant = aBaseVar)->first() 
		else if base=mrules::expressions::ExpressionBase::ConstantObjectReference
		then scopeObjectReferenceConstants->select(c:mrules::expressions::MObjectReferenceConstantBaseDefinition | c.namedConstant = aBaseVar)->first()
		else if base=mrules::expressions::ExpressionBase::Variable
		then scopeVariables->select(c:mrules::expressions::MVariableBaseDefinition | c.namedExpression = aBaseVar)->first()  
		else if base=mrules::expressions::ExpressionBase::Parameter
		then scopeParameters->select(c:mrules::expressions::MParameterBaseDefinition | c.aParameter = aBaseVar)->first()  
		else if base=mrules::expressions::ExpressionBase::Iterator
		then scopeIterator->select(c:mrules::expressions::MIteratorBaseDefinition | c.iterator = aBaseVar)->first()  
		else if base=mrules::expressions::ExpressionBase::Accumulator
		then scopeAccumulator->select(c:mrules::expressions::MAccumulatorBaseDefinition | c.accumulator = aBaseVar)->first()  
		else if base=mrules::expressions::ExpressionBase::OneValue or base=mrules::expressions::ExpressionBase::ZeroValue  or base = mrules::expressions::ExpressionBase::MinusOneValue
		then scopeNumberBase->select(c:mrules::expressions::MNumberBaseDefinition|c.expressionBase = base)->first()
		else scopeBase->select(c:mrules::expressions::MBaseDefinition | c.expressionBase=base)->first() 
		endif endif endif endif endif endif endif endif endif endif endif endif endif endif
		in
		if r.oclIsUndefined() then null else r endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;

		if (baseDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				baseDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractBaseDefinition result = (MAbstractBaseDefinition) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AVariable getABaseVar() {
		if (aBaseVar != null && aBaseVar.eIsProxy()) {
			InternalEObject oldABaseVar = (InternalEObject) aBaseVar;
			aBaseVar = (AVariable) eResolveProxy(oldABaseVar);
			if (aBaseVar != oldABaseVar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR, oldABaseVar, aBaseVar));
			}
		}
		return aBaseVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AVariable basicGetABaseVar() {
		return aBaseVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setABaseVar(AVariable newABaseVar) {
		AVariable oldABaseVar = aBaseVar;
		aBaseVar = newABaseVar;
		boolean oldABaseVarESet = aBaseVarESet;
		aBaseVarESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR, oldABaseVar, aBaseVar,
					!oldABaseVarESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetABaseVar() {
		AVariable oldABaseVar = aBaseVar;
		boolean oldABaseVarESet = aBaseVarESet;
		aBaseVar = null;
		aBaseVarESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR, oldABaseVar, null, oldABaseVarESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetABaseVar() {
		return aBaseVarESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getABaseExitType() {
		AClassifier aBaseExitType = basicGetABaseExitType();
		return aBaseExitType != null && aBaseExitType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aBaseExitType) : aBaseExitType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetABaseExitType() {
		/**
		 * @OCL if baseDefinition.oclIsUndefined() 
		then null
		else 
		/* Trick b/c we need to reference the container here and baseDefinition is generated on the fly *\/
		if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::SelfObject
		then self.aSelfObjectType
		else if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::Target
		then self.aTargetObjectType
		else if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::ObjectObject
		then self.aObjectObjectType
		else if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::Source
		then self.aSrcObjectType
		else if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::EContainer
		then null /* if self.aSelfObjectType.allClassesContainedIn()->size() = 1 then self.aSelfObjectType.allClassesContainedIn()->first() else null endif*\/
		else baseDefinition.aClassifier
		endif endif endif endif
		endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_TYPE;

		if (aBaseExitTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBaseExitTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBaseExitTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getABaseExitSimpleType() {
		/**
		 * @OCL if baseDefinition.oclIsUndefined()  or not (self.aBaseExitType.oclIsUndefined())
		then acore::classifiers::ASimpleType::None
		else if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::Target
		then self.aTargetSimpleType
		else self.baseDefinition.aSimpleType
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_SIMPLE_TYPE;

		if (aBaseExitSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBaseExitSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBaseExitSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBaseExitMandatory() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if baseDefinition.oclIsUndefined()
		then null
		else baseDefinition.aMandatory
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY;

		if (baseExitMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				baseExitMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBaseExitSingular() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then true
		else if baseDefinition.oclIsUndefined()
		then null
		else baseDefinition.aSingular
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR;

		if (baseExitSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				baseExitSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String baseDefinition$Update(MAbstractBaseDefinition trg) {

		/**
		 * @OCL null
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE.getEOperations().get(0);
		if (baseDefinition$UpdateexpressionsMAbstractBaseDefinitionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				baseDefinition$UpdateexpressionsMAbstractBaseDefinitionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(baseDefinition$UpdateexpressionsMAbstractBaseDefinitionBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("trg", trg);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForBuiltIn() {

		/**
		 * @OCL baseAsCode
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE.getEOperations().get(1);
		if (asCodeForBuiltInBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				asCodeForBuiltInBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForBuiltInBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForConstants() {

		/**
		 * @OCL if baseDefinition.oclIsKindOf(mrules::expressions::MSimpleTypeConstantBaseDefinition)
		then let b: mrules::expressions::MSimpleTypeConstantBaseDefinition =  baseDefinition.oclAsType(mrules::expressions::MSimpleTypeConstantBaseDefinition) in
		b.namedConstant.aName
		else if baseDefinition.oclIsKindOf(mrules::expressions::MLiteralConstantBaseDefinition)
		then let b: mrules::expressions::MLiteralConstantBaseDefinition =  baseDefinition.oclAsType(mrules::expressions::MLiteralConstantBaseDefinition) in
		b.namedConstant.aName
		else null endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE.getEOperations().get(2);
		if (asCodeForConstantsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				asCodeForConstantsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForConstantsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForVariables() {

		/**
		 * @OCL let v: mrules::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mrules::expressions::MVariableBaseDefinition) in
		let vName: String = v.namedExpression.aName in
		baseAsCode
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE.getEOperations().get(3);
		if (asCodeForVariablesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				asCodeForVariablesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForVariablesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE:
			return getBaseAsCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			return getBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			if (resolve)
				return getBaseDefinition();
			return basicGetBaseDefinition();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR:
			if (resolve)
				return getABaseVar();
			return basicGetABaseVar();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_TYPE:
			if (resolve)
				return getABaseExitType();
			return basicGetABaseExitType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_SIMPLE_TYPE:
			return getABaseExitSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY:
			return getBaseExitMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR:
			return getBaseExitSingular();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			setBase((ExpressionBase) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			setBaseDefinition((MAbstractBaseDefinition) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR:
			setABaseVar((AVariable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			unsetBase();
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			setBaseDefinition((MAbstractBaseDefinition) null);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR:
			unsetABaseVar();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE:
			return BASE_AS_CODE_EDEFAULT == null ? getBaseAsCode() != null
					: !BASE_AS_CODE_EDEFAULT.equals(getBaseAsCode());
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			return isSetBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			return basicGetBaseDefinition() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR:
			return isSetABaseVar();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_TYPE:
			return basicGetABaseExitType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_SIMPLE_TYPE:
			return getABaseExitSimpleType() != ABASE_EXIT_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY:
			return BASE_EXIT_MANDATORY_EDEFAULT == null ? getBaseExitMandatory() != null
					: !BASE_EXIT_MANDATORY_EDEFAULT.equals(getBaseExitMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR:
			return BASE_EXIT_SINGULAR_EDEFAULT == null ? getBaseExitSingular() != null
					: !BASE_EXIT_SINGULAR_EDEFAULT.equals(getBaseExitSingular());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION:
			return baseDefinition$Update((MAbstractBaseDefinition) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_BUILT_IN:
			return asCodeForBuiltIn();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_CONSTANTS:
			return asCodeForConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_VARIABLES:
			return asCodeForVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (base: ");
		if (baseESet)
			result.append(base);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MAbstractExpressionWithBaseImpl
