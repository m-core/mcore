/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AClassType;

import com.montages.acore.values.AObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Reference Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MObjectReferenceLet#getAObjectType <em>AObject Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant1 <em>AConstant1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant2 <em>AConstant2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant3 <em>AConstant3</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMObjectReferenceLet()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OCL label='asCode'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Object Reference\'\n' asBasicCodeDerive='null /*let t: String =  if aObjectType.oclIsUndefined() then \'MISSING TYPE\' else\r\n\taObjectType.aName endif in\r\nlet a: String = if aConstant1.oclIsUndefined() then \'\' else \'<obj \'.concat(aConstant1.aLabel).concat(\'>\') endif in\r\nlet b: String = if aConstant2.oclIsUndefined() then \'\' else \'<obj \'.concat(aConstant2.aLabel).concat(\'>\') endif in\r\nlet c: String = if aConstant3.oclIsUndefined() then \'\' else \'<obj \'.concat(aConstant3.aLabel).concat(\'>\')  endif in\r\n\r\n if aSingular\r\n  then a.concat(b).concat(c)\r\n  else asSetString(\r\n  \t\'<obj \'.concat(aConstant1.aLabel).concat(\'>\'),\r\n  \t\'<obj \'.concat(aConstant2.aLabel).concat(\'>\'),\r\n  \t\'<obj \'.concat(aConstant3.aLabel).concat(\'>\')\r\n  )\r\n  endif\052/\r\n' calculatedOwnMandatoryDerive='null /*let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) > 0\052/' calculatedOwnSingularDerive='null /*\r\nlet a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) <= 1\r\n\052/' aCalculatedOwnTypeDerive='aObjectType'"
 * @generated
 */

public interface MObjectReferenceLet extends MConstantLet {
	/**
	 * Returns the value of the '<em><b>AObject Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AObject Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AObject Type</em>' reference.
	 * @see #isSetAObjectType()
	 * @see #unsetAObjectType()
	 * @see #setAObjectType(AClassType)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMObjectReferenceLet_AObjectType()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Object Type'"
	 * @generated
	 */
	AClassType getAObjectType();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAObjectType <em>AObject Type</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AObject Type</em>' reference.
	 * @see #isSetAObjectType()
	 * @see #unsetAObjectType()
	 * @see #getAObjectType()
	 * @generated
	 */

	void setAObjectType(AClassType value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAObjectType <em>AObject Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAObjectType()
	 * @see #getAObjectType()
	 * @see #setAObjectType(AClassType)
	 * @generated
	 */
	void unsetAObjectType();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAObjectType <em>AObject Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AObject Type</em>' reference is set.
	 * @see #unsetAObjectType()
	 * @see #getAObjectType()
	 * @see #setAObjectType(AClassType)
	 * @generated
	 */
	boolean isSetAObjectType();

	/**
	 * Returns the value of the '<em><b>AConstant1</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AConstant1</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AConstant1</em>' reference list.
	 * @see #isSetAConstant1()
	 * @see #unsetAConstant1()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMObjectReferenceLet_AConstant1()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Constant 1'"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if aObjectType.oclIsUndefined() then false\r\nelse trg.aClassifier = aObjectType endif'"
	 * @generated
	 */
	EList<AObject> getAConstant1();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant1 <em>AConstant1</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAConstant1()
	 * @see #getAConstant1()
	 * @generated
	 */
	void unsetAConstant1();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant1 <em>AConstant1</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AConstant1</em>' reference list is set.
	 * @see #unsetAConstant1()
	 * @see #getAConstant1()
	 * @generated
	 */
	boolean isSetAConstant1();

	/**
	 * Returns the value of the '<em><b>AConstant2</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AConstant2</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AConstant2</em>' reference list.
	 * @see #isSetAConstant2()
	 * @see #unsetAConstant2()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMObjectReferenceLet_AConstant2()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Constant 2'"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if aObjectType.oclIsUndefined() then false\r\nelse trg.aClassifier = aObjectType endif'"
	 * @generated
	 */
	EList<AObject> getAConstant2();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant2 <em>AConstant2</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAConstant2()
	 * @see #getAConstant2()
	 * @generated
	 */
	void unsetAConstant2();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant2 <em>AConstant2</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AConstant2</em>' reference list is set.
	 * @see #unsetAConstant2()
	 * @see #getAConstant2()
	 * @generated
	 */
	boolean isSetAConstant2();

	/**
	 * Returns the value of the '<em><b>AConstant3</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AConstant3</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AConstant3</em>' reference list.
	 * @see #isSetAConstant3()
	 * @see #unsetAConstant3()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMObjectReferenceLet_AConstant3()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Constant 3'"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if aObjectType.oclIsUndefined() then false\r\nelse trg.aClassifier = aObjectType endif'"
	 * @generated
	 */
	EList<AObject> getAConstant3();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant3 <em>AConstant3</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAConstant3()
	 * @see #getAConstant3()
	 * @generated
	 */
	void unsetAConstant3();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MObjectReferenceLet#getAConstant3 <em>AConstant3</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AConstant3</em>' reference list is set.
	 * @see #unsetAConstant3()
	 * @see #getAConstant3()
	 * @generated
	 */
	boolean isSetAConstant3();

} // MObjectReferenceLet
