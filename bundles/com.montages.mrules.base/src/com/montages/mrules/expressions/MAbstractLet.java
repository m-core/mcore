/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.abstractions.AVariable;

import com.montages.mrules.MRulesNamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An abstract "let" expression in OCL. Note that for these expression asCode returns the corresponding OCL variable name, that canthen be used to build more complex expressions.  Should you need to get the complete let...in statement, then use calculatedLetExpression.
 * <!-- end-model-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractLet()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractLet extends MAbstractExpression, MRulesNamed, AVariable {
} // MAbstractLet
