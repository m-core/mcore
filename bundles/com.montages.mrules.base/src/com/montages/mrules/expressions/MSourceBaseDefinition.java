/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSource Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSourceBaseDefinition()
 * @model annotation="http://www.montages.com/mCore/MCore mName='M SourceBaseDefinition'"
 *        annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aSimpleTypeDerive='let srcSimple:acore::classifiers::ASimpleType =\r\nif eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).aSrcObjectSimpleType\r\nelse null\r\nendif\r\nin \r\nif srcSimple.oclIsUndefined() then acore::classifiers::ASimpleType::None else srcSimple endif ' calculatedBaseDerive='ExpressionBase::Source' calculatedAsCodeDerive='\'src\'' aClassifierDerive='let srcType:acore::classifiers::AClassifier =\r\nif eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).aSrcObjectType\r\nelse null\r\nendif\r\nin \r\nif srcType.oclIsUndefined() then null else srcType endif' aSingularDerive='true\n'"
 * @generated
 */

public interface MSourceBaseDefinition extends MAbstractBaseDefinition {
} // MSourceBaseDefinition
