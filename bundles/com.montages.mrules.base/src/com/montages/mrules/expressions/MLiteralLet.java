/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AEnumeration;

import com.montages.acore.values.ALiteral;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MLiteralLet#getAEnumerationType <em>AEnumeration Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MLiteralLet#getAConstant1 <em>AConstant1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MLiteralLet#getAConstant2 <em>AConstant2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MLiteralLet#getAConstant3 <em>AConstant3</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralLet()
 * @model annotation="http://www.xocl.org/OCL label='asCode'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Literal Constant\'\n' asBasicCodeDerive='let a: String = if aConstant1.oclIsUndefined() then \'\' else aConstant1.aQualifiedName endif in\r\nlet b: String = if aConstant2.oclIsUndefined() then \'\' else aConstant2.aQualifiedName endif in\r\nlet c: String = if aConstant3.oclIsUndefined() then \'\' else aConstant3.aQualifiedName endif in\r\n\r\n if aSingular\r\n  then a.concat(b).concat(c)\r\n  else asSetString(a, b, c)\r\nendif\r\n' calculatedOwnSingularDerive='let a: Integer = if aConstant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if aConstant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if aConstant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) <= 1' calculatedOwnMandatoryDerive='true' aCalculatedOwnTypeDerive='aEnumerationType'"
 * @generated
 */

public interface MLiteralLet extends MConstantLet {
	/**
	 * Returns the value of the '<em><b>AEnumeration Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AEnumeration Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AEnumeration Type</em>' reference.
	 * @see #isSetAEnumerationType()
	 * @see #unsetAEnumerationType()
	 * @see #setAEnumerationType(AEnumeration)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralLet_AEnumerationType()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Enumeration Type'"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.aLiteral->size() > 0'"
	 * @generated
	 */
	AEnumeration getAEnumerationType();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAEnumerationType <em>AEnumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AEnumeration Type</em>' reference.
	 * @see #isSetAEnumerationType()
	 * @see #unsetAEnumerationType()
	 * @see #getAEnumerationType()
	 * @generated
	 */

	void setAEnumerationType(AEnumeration value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAEnumerationType <em>AEnumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAEnumerationType()
	 * @see #getAEnumerationType()
	 * @see #setAEnumerationType(AEnumeration)
	 * @generated
	 */
	void unsetAEnumerationType();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAEnumerationType <em>AEnumeration Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AEnumeration Type</em>' reference is set.
	 * @see #unsetAEnumerationType()
	 * @see #getAEnumerationType()
	 * @see #setAEnumerationType(AEnumeration)
	 * @generated
	 */
	boolean isSetAEnumerationType();

	/**
	 * Returns the value of the '<em><b>AConstant1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AConstant1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AConstant1</em>' reference.
	 * @see #isSetAConstant1()
	 * @see #unsetAConstant1()
	 * @see #setAConstant1(ALiteral)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralLet_AConstant1()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Constant 1'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	ALiteral getAConstant1();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant1 <em>AConstant1</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AConstant1</em>' reference.
	 * @see #isSetAConstant1()
	 * @see #unsetAConstant1()
	 * @see #getAConstant1()
	 * @generated
	 */

	void setAConstant1(ALiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant1 <em>AConstant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAConstant1()
	 * @see #getAConstant1()
	 * @see #setAConstant1(ALiteral)
	 * @generated
	 */
	void unsetAConstant1();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant1 <em>AConstant1</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AConstant1</em>' reference is set.
	 * @see #unsetAConstant1()
	 * @see #getAConstant1()
	 * @see #setAConstant1(ALiteral)
	 * @generated
	 */
	boolean isSetAConstant1();

	/**
	 * Returns the value of the '<em><b>AConstant2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AConstant2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AConstant2</em>' reference.
	 * @see #isSetAConstant2()
	 * @see #unsetAConstant2()
	 * @see #setAConstant2(ALiteral)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralLet_AConstant2()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Constant 2'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	ALiteral getAConstant2();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant2 <em>AConstant2</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AConstant2</em>' reference.
	 * @see #isSetAConstant2()
	 * @see #unsetAConstant2()
	 * @see #getAConstant2()
	 * @generated
	 */

	void setAConstant2(ALiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant2 <em>AConstant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAConstant2()
	 * @see #getAConstant2()
	 * @see #setAConstant2(ALiteral)
	 * @generated
	 */
	void unsetAConstant2();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant2 <em>AConstant2</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AConstant2</em>' reference is set.
	 * @see #unsetAConstant2()
	 * @see #getAConstant2()
	 * @see #setAConstant2(ALiteral)
	 * @generated
	 */
	boolean isSetAConstant2();

	/**
	 * Returns the value of the '<em><b>AConstant3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AConstant3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AConstant3</em>' reference.
	 * @see #isSetAConstant3()
	 * @see #unsetAConstant3()
	 * @see #setAConstant3(ALiteral)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralLet_AConstant3()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Constant 3'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	ALiteral getAConstant3();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant3 <em>AConstant3</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AConstant3</em>' reference.
	 * @see #isSetAConstant3()
	 * @see #unsetAConstant3()
	 * @see #getAConstant3()
	 * @generated
	 */

	void setAConstant3(ALiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant3 <em>AConstant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAConstant3()
	 * @see #getAConstant3()
	 * @see #setAConstant3(ALiteral)
	 * @generated
	 */
	void unsetAConstant3();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralLet#getAConstant3 <em>AConstant3</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AConstant3</em>' reference is set.
	 * @see #unsetAConstant3()
	 * @see #getAConstant3()
	 * @see #setAConstant3(ALiteral)
	 * @generated
	 */
	boolean isSetAConstant3();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='self.aEnumerationType.aLiteral'"
	 * @generated
	 */
	EList<ALiteral> getAllowedLiterals();

} // MLiteralLet
