/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAccumulator Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAccumulatorBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<accumulator> \'.concat(if accumulator.oclIsUndefined() then \'\' else accumulator.aName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='ExpressionBase::Accumulator' calculatedAsCodeDerive='if accumulator.oclIsUndefined() then \'MISSING ACCUMULATOR\' else accumulator.aName endif' aMandatoryDerive='if accumulator.oclIsUndefined() then false\r\nelse accumulator.aMandatory endif' aSingularDerive='if accumulator.oclIsUndefined() then true\r\nelse accumulator.aSingular endif' aSimpleTypeDerive='if accumulator.oclIsUndefined() then acore::classifiers::ASimpleType::None\r\nelse accumulator.aSimpleType\r\nendif' aClassifierDerive='if accumulator.oclIsUndefined() then null\r\nelse accumulator.aClassifier\r\n endif'"
 * @generated
 */

public interface MAccumulatorBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Accumulator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accumulator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accumulator</em>' reference.
	 * @see #isSetAccumulator()
	 * @see #unsetAccumulator()
	 * @see #setAccumulator(MAccumulator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAccumulatorBaseDefinition_Accumulator()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='accumulator'"
	 * @generated
	 */
	MAccumulator getAccumulator();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accumulator</em>' reference.
	 * @see #isSetAccumulator()
	 * @see #unsetAccumulator()
	 * @see #getAccumulator()
	 * @generated
	 */

	void setAccumulator(MAccumulator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAccumulator()
	 * @see #getAccumulator()
	 * @see #setAccumulator(MAccumulator)
	 * @generated
	 */
	void unsetAccumulator();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Accumulator</em>' reference is set.
	 * @see #unsetAccumulator()
	 * @see #getAccumulator()
	 * @see #setAccumulator(MAccumulator)
	 * @generated
	 */
	boolean isSetAccumulator();

} // MAccumulatorBaseDefinition
