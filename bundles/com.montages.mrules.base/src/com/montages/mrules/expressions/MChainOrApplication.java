/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MChain Or Application</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMChainOrApplication()
 * @model abstract="true"
 * @generated
 */

public interface MChainOrApplication extends MToplevelExpression {
} // MChainOrApplication
