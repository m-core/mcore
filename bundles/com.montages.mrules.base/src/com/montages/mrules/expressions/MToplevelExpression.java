/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MToplevel Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMToplevelExpression()
 * @model abstract="true"
 * @generated
 */

public interface MToplevelExpression extends MAbstractExpression {
} // MToplevelExpression
