/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MIterator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMIterator()
 * @model annotation="http://www.xocl.org/OCL label='aName'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Iterator\'\n' calculatedOwnMandatoryDerive='true' calculatedOwnSingularDerive='true' aCalculatedOwnSimpleTypeDerive='if containingCollection.collection.aCalculatedOwnSimpleType.oclIsInvalid()\r\nthen acore::classifiers::ASimpleType::None\r\nelse containingCollection.collection.aCalculatedOwnSimpleType endif' aCalculatedOwnTypeDerive='if containingCollection.collection.aCalculatedOwnType.oclIsUndefined() then null\r\nelse containingCollection.collection.aCalculatedOwnType endif'"
 * @generated
 */

public interface MIterator extends MCollectionVar {
} // MIterator
