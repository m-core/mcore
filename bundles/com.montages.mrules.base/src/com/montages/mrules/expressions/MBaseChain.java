/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.abstractions.AVariable;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MBase Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MBaseChain#getTypeMismatch <em>Type Mismatch</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MBaseChain#getCallArgument <em>Call Argument</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MBaseChain#getSubExpression <em>Sub Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MBaseChain#getContainedCollector <em>Contained Collector</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MBaseChain#getChainCodeforSubchains <em>Chain Codefor Subchains</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MBaseChain#getIsOwnXOCLOp <em>Is Own XOCL Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aChainEntryTypeDerive='self.aBaseExitType' calculatedOwnMandatoryDerive='let m1: Boolean = \r\n  if aElement1.oclIsUndefined() then true else aElement1.aMandatory endif in\r\nlet m2: Boolean =\r\n   if aElement2.oclIsUndefined() then true else aElement2.aMandatory endif in\r\nlet m3: Boolean = \r\n  if aElement3.oclIsUndefined() then true else aElement3.aMandatory endif in\r\nbaseExitMandatory and m1 and m2 and m3' calculatedOwnSingularDerive='if self.processorIsSet() \r\n     then  self.processorReturnsSingular()\r\n     else self.chainCalculatedSingular endif' aCalculatedOwnTypeDerive='if aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::None or aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::Object then\r\nif aCastType.oclIsUndefined() \r\n  then aChainCalculatedType\r\n  else aCastType\r\nendif\r\nelse \r\nnull\r\nendif' aCalculatedOwnSimpleTypeDerive='let p:mrules::expressions::MProcessor = processor in\nif p <> mrules::expressions::MProcessor::None\n  then\n     if      p = mrules::expressions::MProcessor::ToInteger or\n             p = mrules::expressions::MProcessor::Size  or\n             p = mrules::expressions::MProcessor::Round or\n             p = mrules::expressions::MProcessor::Floor or\n             p = mrules::expressions::MProcessor::Year or\n             p = mrules::expressions::MProcessor::Minute or\n             p = mrules::expressions::MProcessor::Month or\n             p = mrules::expressions::MProcessor::Second or\n             p = mrules::expressions::MProcessor::Day or\n             p = mrules::expressions::MProcessor::Hour\n       then acore::classifiers::ASimpleType::Integer\n     else if p = mrules::expressions::MProcessor::ToString or  \n             p = mrules::expressions::MProcessor::ToYyyyMmDd or\n             p = mrules::expressions::MProcessor::ToHhMm or\n             p = mrules::expressions::MProcessor::AllLowerCase or\n             p = mrules::expressions::MProcessor::AllUpperCase or\n             p = mrules::expressions::MProcessor::CamelCaseLower or\n             p = mrules::expressions::MProcessor::CamelCaseToBusiness or\n             p = mrules::expressions::MProcessor::CamelCaseUpper or\n             p = mrules::expressions::MProcessor::FirstUpperCase or\n             p = mrules::expressions::MProcessor::Trim\n       then acore::classifiers::ASimpleType::String\n     else if p = mrules::expressions::MProcessor::ToReal or\n             p = mrules::expressions::MProcessor::OneDividedBy or\n             p = mrules::expressions::MProcessor::Sum\n       then acore::classifiers::ASimpleType::Real\n     else if p = mrules::expressions::MProcessor::ToBoolean or\n             p = mrules::expressions::MProcessor::IsEmpty or\n             p = mrules::expressions::MProcessor::NotEmpty or\n             p = mrules::expressions::MProcessor::Not  or\n             p = mrules::expressions::MProcessor::IsFalse or\n             p = mrules::expressions::MProcessor::IsTrue or\n             p = mrules::expressions::MProcessor::IsZero or\n             p = mrules::expressions::MProcessor::IsOne or\n             p = mrules::expressions::MProcessor::NotNull or\n             p = mrules::expressions::MProcessor::IsNull or\n             p = mrules::expressions::MProcessor::IsInvalid or\n             p = mrules::expressions::MProcessor::And or\n             p= mrules::expressions::MProcessor::Or\n       then acore::classifiers::ASimpleType::Boolean\n      else if p = mrules::expressions::MProcessor::ToDate \n       then acore::classifiers::ASimpleType::Date\n     else if self.aLastElement=null\n       then self.aBaseExitSimpleType\n     else   self.aLastElement.aSimpleType\n     endif endif endif endif endif endif\n       \n  else if self.aLastElement=null\n     then self.aBaseExitSimpleType\n  else\n          self.aLastElement.aSimpleType\n  endif endif' isComplexExpressionDerive='false' asBasicCodeDerive='if (not self.subExpression->isEmpty()) \r\n     then let mirrorOrder:OrderedSet(mrules::expressions::MSubChain) = self.subExpression->iterate( sub:mrules::expressions::MSubChain; d: OrderedSet(mrules::expressions::MSubChain) = OrderedSet{} | d->prepend(sub)) in\r\n              let subString:String = self.subExpression\r\n                        ->iterate(x:mrules::expressions::MSubChain ; s: String = \'\' | \r\n                                  let t: String = aTypeAsOcl( x.previousExpression.aSelfObjectPackage, x.previousExpression.aCalculatedOwnType, \r\n                                                                         x.previousExpression.aCalculatedOwnSimpleType, x.previousExpression.calculatedOwnSingular ) in\r\n                                  \'let \'.concat(x.uniqueSubchainName()).concat(\' : \').concat(t).concat(\' = \').concat(s) )  in \r\n              let codeString:String =\r\n                      chainCodeforSubchains\r\n                      .concat(subExpression->iterate(x:mrules::expressions::MSubChain ; subStr: String =\'\' | subStr.concat(x.asCode))) in  \r\n              subString.concat(codeString)\r\n  else chainCodeforSubchains endif' aChainCalculatedTypeDerive='if self.aLastElement=null then self.aBaseExitType else\r\nself.aLastElement.aClassifier endif' aChainCalculatedSimpleTypeDerive='if self.aLastElement=null then self.aBaseExitSimpleType\r\nelse self.aLastElement.aSimpleType endif' element1CorrectDerive='if aElement1.oclIsUndefined() then true else\r\n--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else\r\nif aChainEntryType.oclIsUndefined() then false \r\n  else aChainEntryType.aAllProperty()->includes(aElement1)\r\n    and  \r\n   (if aElement1.aOperation->isEmpty()\r\n      then  true\r\n      else if aElement2.oclIsUndefined()\r\n            then aElement1.aOperation\r\n                        ->exists(s:acore::classifiers::AOperation|\r\n                                s.aParameter->size()=self.callArgument->size())\r\n             else aElement1.aOperation\r\n                         ->exists(s:acore::classifiers::AOperation|s.aParameter->size()=0) endif endif)\r\nendif endif \r\n--endif' element2CorrectDerive='if aElement2.oclIsUndefined() then true else\r\n--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else\r\nif aElement2EntryType.oclIsUndefined() then false \r\n  else aElement2EntryType.aAllProperty()->includes(self.aElement2)\r\n      and  \r\n   (if aElement2.aOperation->isEmpty()\r\n      then  true\r\n      else if aElement3.oclIsUndefined()\r\n            then aElement2.aOperation\r\n                        ->exists(s:acore::classifiers::AOperation |\r\n                                s.aParameter->size()=self.callArgument->size())\r\n             else aElement2.aOperation\r\n                         ->exists(s:acore::classifiers::AOperation |s.aParameter->size()=0) endif endif)\r\nendif endif \r\n--endif' element3CorrectDerive='if aElement3.oclIsUndefined() then true else\r\n--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else\r\nif aElement3EntryType.oclIsUndefined() then false\r\n  else aElement3EntryType.aAllProperty()->includes(self.aElement3)\r\n     and  \r\n   (if aElement3.aOperation->isEmpty()\r\n      then  true\r\n      else aElement3.aOperation\r\n                        ->exists(s:acore::classifiers::AOperation|\r\n                                s.aParameter->size()=self.callArgument->size()) endif)\r\nendif endif \r\n--endif' kindLabelDerive='let kind: String = \'CHAIN\' in\nkind\n' collectorDerive='containedCollector' baseAsCodeDerive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \r\n    if e.oclIsInvalid() then null else e endif) \r\n  =true \r\nthen \'\'\r\n  else if baseDefinition.oclIsTypeOf(mrules::expressions::MNumberBaseDefinition)\r\n  then  baseDefinition.oclAsType(mrules::expressions::MNumberBaseDefinition).calculateAsCode(self)\r\n  else if baseDefinition.oclIsTypeOf(mrules::expressions::MContainerBaseDefinition)\r\n  then baseDefinition.calculatedAsCode.concat(if not self.aBaseExitType.oclIsUndefined() then \'.oclAsType(\'.concat(self.aBaseExitType.aName).concat(\')\')else \'\' endif)\r\n  else baseDefinition.calculatedAsCode\r\nendif\r\nendif\r\nendif\r\n' chainCalculatedSingularDerive='let s1: Boolean = \r\n  if aElement1.oclIsUndefined() then true else aElement1.aSingular endif in\r\nlet s2: Boolean =\r\n   if aElement2.oclIsUndefined() then true else aElement2.aSingular endif in\r\nlet s3: Boolean = \r\n  if aElement3.oclIsUndefined() then true else aElement3.aSingular endif in\r\n\r\n(baseExitSingular and s1 and s2 and s3) ' aClassifierDerive='if collector.oclIsUndefined() and self.subExpression->isEmpty()\r\nthen aCalculatedOwnType\r\n  else if collector.oclIsUndefined()\r\n  then subExpression->last().aCalculatedOwnType\r\n  else collector.aClassifier\r\nendif\r\nendif\r\n' aSingularDerive='if self.subExpression->isEmpty() and collector.oclIsUndefined()\r\nthen calculatedOwnSingular\r\n  else if collector.oclIsUndefined()\r\n  then subExpression->last().calculatedOwnSingular\r\n  else collector.aSingular\r\nendif\r\nendif' aSimpleTypeDerive='if self.collector.oclIsUndefined() and self.subExpression->isEmpty() \r\nthen aCalculatedOwnSimpleType\r\n  else if collector.oclIsUndefined()\r\n  then subExpression->last().aCalculatedOwnSimpleType\r\n  else collector.aSimpleType\r\nendif\r\nendif\r\n' processorInitValue='mrules::expressions::MProcessor::None\n' aElement1ChoiceConstruction='\nlet annotatedProp: acore::classifiers::AProperty = \nself.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)\nin\nif self.aChainEntryType.oclIsUndefined() then \nSequence{} else\nself.aChainEntryType.aAllProperty()->asSequence()endif\n\n--'"
 * @generated
 */

public interface MBaseChain extends MAbstractExpressionWithBase, MAbstractChain {
	/**
	 * Returns the value of the '<em><b>Type Mismatch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Mismatch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Mismatch</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain_TypeMismatch()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='typeMismatch'"
	 *        annotation="http://www.xocl.org/OCL derive='self.ownToApplyMismatch()'"
	 * @generated
	 */
	Boolean getTypeMismatch();

	/**
	 * Returns the value of the '<em><b>Call Argument</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MCallArgument}.
	 * It is bidirectional and its opposite is '{@link com.montages.mrules.expressions.MCallArgument#getCall <em>Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Argument</em>' containment reference list.
	 * @see #isSetCallArgument()
	 * @see #unsetCallArgument()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain_CallArgument()
	 * @see com.montages.mrules.expressions.MCallArgument#getCall
	 * @model opposite="call" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MCallArgument> getCallArgument();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MBaseChain#getCallArgument <em>Call Argument</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCallArgument()
	 * @see #getCallArgument()
	 * @generated
	 */
	void unsetCallArgument();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MBaseChain#getCallArgument <em>Call Argument</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Call Argument</em>' containment reference list is set.
	 * @see #unsetCallArgument()
	 * @see #getCallArgument()
	 * @generated
	 */
	boolean isSetCallArgument();

	/**
	 * Returns the value of the '<em><b>Sub Expression</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSubChain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Expression</em>' containment reference list.
	 * @see #isSetSubExpression()
	 * @see #unsetSubExpression()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain_SubExpression()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MSubChain> getSubExpression();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MBaseChain#getSubExpression <em>Sub Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSubExpression()
	 * @see #getSubExpression()
	 * @generated
	 */
	void unsetSubExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MBaseChain#getSubExpression <em>Sub Expression</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Sub Expression</em>' containment reference list is set.
	 * @see #unsetSubExpression()
	 * @see #getSubExpression()
	 * @generated
	 */
	boolean isSetSubExpression();

	/**
	 * Returns the value of the '<em><b>Contained Collector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Collector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Collector</em>' containment reference.
	 * @see #isSetContainedCollector()
	 * @see #unsetContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain_ContainedCollector()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='null'"
	 * @generated
	 */
	MCollectionExpression getContainedCollector();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MBaseChain#getContainedCollector <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Collector</em>' containment reference.
	 * @see #isSetContainedCollector()
	 * @see #unsetContainedCollector()
	 * @see #getContainedCollector()
	 * @generated
	 */

	void setContainedCollector(MCollectionExpression value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MBaseChain#getContainedCollector <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainedCollector()
	 * @see #getContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @generated
	 */
	void unsetContainedCollector();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MBaseChain#getContainedCollector <em>Contained Collector</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Contained Collector</em>' containment reference is set.
	 * @see #unsetContainedCollector()
	 * @see #getContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @generated
	 */
	boolean isSetContainedCollector();

	/**
	 * Returns the value of the '<em><b>Chain Codefor Subchains</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain Codefor Subchains</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain Codefor Subchains</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain_ChainCodeforSubchains()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Chain Code for Subchains'"
	 *        annotation="http://www.xocl.org/OCL derive='let code: String = \r\n     if baseDefinition.oclIsKindOf(mrules::expressions::MBaseDefinition) \r\n\t     then asCodeForBuiltIn()\r\n\t else if baseDefinition.oclIsKindOf(mrules::expressions::MContainerBaseDefinition) \r\n\t     then asCodeForOthers()\r\n\t else if (baseDefinition.oclIsKindOf(mrules::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mrules::expressions::MLiteralConstantBaseDefinition)) \r\n\t     then asCodeForConstants()\r\n\t else if baseDefinition.oclIsKindOf(mrules::expressions::MVariableBaseDefinition) \r\n\t     then asCodeForVariables()\r\n\t     else asCodeForOthers() endif endif endif endif in\r\nlet res: String = if aSingular then \'null\' else \'OrderedSet{}\' endif in\r\nlet chainTypeString: String = \r\n     aTypeAsOcl(aSelfObjectPackage, aChainCalculatedType, aChainCalculatedSimpleType, chainCalculatedSingular) in\r\nlet chainTypeStringSingular: String = \r\n     aTypeAsOcl(aSelfObjectPackage, aChainCalculatedType, aChainCalculatedSimpleType, true) in\r\n--if (castType.oclIsUndefined() or chainCalculatedType.oclIsUndefined())\r\n\t--then \r\nif self.typeMismatch or not(self.aCastType.oclIsUndefined())-- and self.containedCollector.oclIsUndefined() and\r\n\t --(if self.castType.oclIsUndefined() then true else castType.allSubTypes()->excludes(chainCalculatedType)endif)\r\n     then self.autoCastWithProc()\r\nelse if self.processor=mrules::expressions::MProcessor::None \r\n     then code \r\nelse if self.isPrefixProcessor() or self.isPostfixProcessor() \r\n     then if self.processor = mrules::expressions::MProcessor::Not \r\n                  then let businessLog : String = \' \'  in\r\n\t\t                  businessLog.concat(\'if (\').concat(code).concat(\')= true \\n then false \\n else if (\').concat(code).concat(\')= false \\n then true \\n else null endif endif \\n \')\r\n\t\t     else if self.processor = mrules::expressions::MProcessor::OneDividedBy \r\n\t\t           then let businessLog : String = \' \'  in\r\n\t\t                   businessLog.concat(\'(1 / (\').concat(code).concat(\'))\')\r\n\t\t     else if self.isPostfixProcessor() \r\n\t\t            then let businessLog : String = \' \'  in\r\n\t\t                    businessLog.concat(code).concat(self.procAsCode())\r\n\t\t            else code endif endif endif\r\n     else let variableName : String = \r\n             self.uniqueChainNumber() in\r\n\t\t    \'let \'.concat(variableName).concat(\': \').concat(chainTypeString).concat(\' = \').concat(\r\n\t\t    code).concat(\' in\\n\').concat(    -- Accessing feature is not unary:\r\n            \'if \').concat(variableName).concat(\r\n            if self.isOwnXOCLOperator() \r\n                 then \'.oclIsUndefined() or \'.concat(variableName) else \'\'endif).concat(\r\n            if self.isProcessorSetOperator() \r\n                 then \'->\'\r\n            else if not(self.isProcessorCheckEqualOperator()) \r\n                 then \'.\' else \'\' endif endif).concat(\r\n            if self.isCustomCodeProcessor() \r\n                 then \'isEmpty()\' \r\n                 else self.procAsCode() endif).concat\r\n            (\r\n            if self.isProcessorCheckEqualOperator() \r\n\t\t         then \' then true else false \' \r\n\t\t    else           \r\n\t\t            if self.isCustomCodeProcessor()  \r\n\t\t                 then \'\' \r\n\t\t                 else if not(self.processorReturnsSingular())  then \'->\' else \'.\' endif.concat(\r\n\t\t                        \'oclIsUndefined() \\n \') endif.concat( \r\n\t\t            \'then null \\n else \').concat(-- todo   processor does not influence calculatedSingular\r\n\t\t            variableName).concat(\r\n\t\t            if self.isProcessorSetOperator() then \'->\' else \'.\' endif).concat\r\n\t\t            ( \r\n\t\t            --  \"->\" for Set , \".\" for unary ,  nothing if we check for a value\r\n\t\t            if self.isCustomCodeProcessor() \r\n\t\t               then let checkPart : String = if  processor= mrules::expressions::MProcessor::And then \'false\' else \'true\' endif in\r\n                               let elsePart : String = if processor= mrules::expressions::MProcessor::And then \'true\' else \'false\' endif in\r\n                               let iterateBase: String = \'iterate( x:\'.concat(chainTypeStringSingular).concat(\'; s:\') in\r\n                               if self.processor = mrules::expressions::MProcessor::And or processor = mrules::expressions::MProcessor::Or \r\n                                    then iterateBase.concat(chainTypeStringSingular).concat(\r\n                                            \'= \').concat(elsePart).concat(\r\n                                           \'|  if ( x) = \').concat(checkPart).concat( \' \\n then \').concat(checkPart).concat(\'\\n\').concat(\r\n                                            \'else if (s)=\').concat(checkPart).concat(\'\\n\').concat(\'then \').concat(checkPart).concat(\'\\n\').concat(\r\n                                            \' else if x =null then null \\n\').concat(\'else if s =null then null\').concat(\r\n                                            \' else \').concat(elsePart).concat(\' endif endif endif endif)\')\r\n                                   else iterateBase.concat(chainTypeString).concat(\'= \').concat(\'OrderedSet{} | if x.oclIsUndefined() then s else if x=\').concat(\r\n                                           --CHANGE ->first and ->last \r\n                                           --.concat(code).concat(if processor=MProcessor::Head then \'->first() \' else \'->last() \' endif).concat(\'then s else s->including(x)->asOrderedSet() endif endif)\'))\r\n                                           code).concat(if processor=mrules::expressions::MProcessor::Head then \'->last() \' else \'->first() \' endif).concat(\r\n                                           \'then s else s->including(x)->asOrderedSet() endif endif)\') endif\r\n                      else self.procAsCode() endif\r\n                      ) endif\r\n              ).concat( \r\n             \'\\n  endif\') endif endif endif\r\n'"
	 * @generated
	 */
	String getChainCodeforSubchains();

	/**
	 * Returns the value of the '<em><b>Is Own XOCL Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Own XOCL Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Own XOCL Op</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMBaseChain_IsOwnXOCLOp()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='IsOwnXOCLOp'"
	 *        annotation="http://www.xocl.org/OCL derive='processor = expressions::MProcessor::CamelCaseLower or\r\nprocessor = expressions::MProcessor::CamelCaseToBusiness or\r\nprocessor = mrules::expressions::MProcessor::CamelCaseUpper'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='false'"
	 * @generated
	 */
	Boolean getIsOwnXOCLOp();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='autoCastWithProc'"
	 *        annotation="http://www.xocl.org/OCL body=' let code: String = if baseDefinition.oclIsKindOf(mrules::expressions::MBaseDefinition) \r\n\tthen asCodeForBuiltIn()\r\n\telse if (baseDefinition.oclIsKindOf(mrules::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mrules::expressions::MLiteralConstantBaseDefinition)) then asCodeForConstants()\r\n\telse if baseDefinition.oclIsKindOf(mrules::expressions::MVariableBaseDefinition) then asCodeForVariables()\r\n\telse asCodeForOthers() endif endif endif in\r\nlet apply: mrules::expressions::MApplication= if self.eContainer().oclIsTypeOf(mrules::expressions::MApplication ) then self.eContainer().oclAsType(mrules::expressions::MApplication) else null endif in\r\nlet chainTypeString: String =aTypeAsOcl(aSelfObjectPackage, aChainCalculatedType, aChainCalculatedSimpleType, chainCalculatedSingular) in\r\nlet chainTypeStringSingular: String = aTypeAsOcl(aSelfObjectPackage, aChainCalculatedType, aChainCalculatedSimpleType, true) in\r\n\r\n --let castType: MClassifier =  if apply.oclIsUndefined() then self.expectedReturnType else if self.castType.oclIsUndefined() then apply.operands->first().calculatedOwnType else self.castType endif endif\r\n  let castType: acore::classifiers::AClassifier =  if self.aCastType.oclIsUndefined() then (if apply.oclIsUndefined() then self.aExpectedReturnType else apply.operands->first().aCalculatedOwnType  endif) else self.aCastType endif  -- CastType has to be preferred to autocast\r\n \r\n in\r\n   let castTypeString: String  = aTypeAsOcl(aSelfObjectPackage, aCastType, acore::classifiers::ASimpleType::None, chainCalculatedSingular) in\r\n\t\t  let castTypeStringSingular: String = aTypeAsOcl(aSelfObjectPackage, aCastType, acore::classifiers::ASimpleType::None, true) in\r\n\t\r\n\tlet opChangesReturn : Boolean = apply.operands->first().aSimpleType <> apply.aSimpleType or apply.operands->first().aClassifier <> apply.aClassifier\r\nin\r\n\t\t        let variableName : String = self.uniqueChainNumber() in  \r\n\t\t        \r\n\t\t        -- chain name\r\n\t\t        let chainName: String = \r\n\t\t        if self.processor= mrules::expressions::MProcessor::None then \'\' else\r\n\t\t\'let \'.concat(variableName).concat(\': \').concat(if self.chainCalculatedSingular then castTypeStringSingular else castTypeString endif).concat(\' = \') endif\r\n\t\t   in\r\n\t-- chainname end\t\r\n\r\nif  \r\n\r\nnot(self.aCastType.oclIsUndefined()) or \r\n--new\r\n(if (not(apply.oclIsUndefined()) and (apply.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::Boolean and self.aSimpleType <> acore::classifiers::ASimpleType::Boolean  or (apply.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::Integer and self.aSimpleType <> acore::classifiers::ASimpleType::Integer)))then \r\n   if apply.operands->first().aCalculatedOwnType.oclIsUndefined() then false\r\n    else    apply.operands->first().aCalculatedOwnType.oclAsType(acore::classifiers::AClassType).aAllSpecializedClass()->excludes(self.aCalculatedOwnType) and apply.operands->first().aCalculatedOwnType<> self.aCalculatedOwnType\r\n        endif else false endif)\r\n   --new     \r\n         or\r\n\r\n( if apply.oclIsUndefined() then self.aExpectedReturnType <> self.aClassifier and self.aExpectedReturnSimpleType = self.aSimpleType               --   check is dont have an apply but chain only    :::::::.changed OwnType for Collection\r\n else ((apply.aCalculatedOwnSimpleType = self.aCalculatedOwnSimpleType) or opChangesReturn) and apply.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::None endif)      --check if Applytype differs from chain and type is a Classifier\r\n  \r\nthen\r\n\t\tchainName.concat(\'let chain: \').concat(chainTypeString).concat(\' = \').concat(code).concat(\' in\\n\').concat(\r\n\t\t\t  if chainCalculatedSingular then \'if chain.oclIsUndefined()\'.concat(\'\\n\').concat(\'  then null\\n  else \') else \'\' endif\r\n\t\t    ).concat(\r\n\t\t\t  if chainCalculatedSingular\r\n\t\t\t    then\r\n\t\t\t      \'if chain.oclIsKindOf(\'.concat(castTypeStringSingular).concat(\')\\n    then chain.oclAsType(\').concat(castTypeStringSingular).concat(\')\\n    else null\\n  endif\')\r\n\t\t\t    else \r\n\r\n \'chain->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: OrderedSet(\').concat(castTypeStringSingular).concat(\')=OrderedSet{} | if i.oclIsKindOf(\').concat(castTypeStringSingular).concat(\') then r->including(i.oclAsType(\').concat(castTypeStringSingular).concat(\')\').concat(\')->asOrderedSet() \\n else r endif)\')\r\n\t\t\t     endif\r\n\t\t\t .concat(\r\n\t\t\t  if chainCalculatedSingular then \'\\n  endif\' else \'\' endif\r\n\t\t    ).concat(if self.processor= MProcessor::None then \'\' else \' in\\n\'.concat(    -- Accessing feature is not unary\r\n\t\t\r\n\t\t\'if \').concat(variableName).concat(if self.isOwnXOCLOperator() then \'.oclIsUndefined() or \'.concat(variableName) else \'\'endif).concat(if self.isProcessorSetOperator() then \'->\' else if not(self.isProcessorCheckEqualOperator()) then \'.\' else \'\' endif endif).concat(self.procAsCode())\r\n\t\t.concat(if self.isProcessorCheckEqualOperator() then \' then true else false \' \r\n\t\t\r\n\t\t\r\n\t\telse (if processor=mrules::expressions::MProcessor::AsOrderedSet then \'->\' else \'.\' endif).concat(\'oclIsUndefined() \\n then null \\n else \'  -- todo   processor does not influence calculatedSingular\r\n.concat(variableName).concat(\r\n\t\t      if self.isProcessorSetOperator() then \'->\' else \'.\' endif --  \"->\" for Set , \".\" for unary ,  nothing if we check for a value\r\n\t\t      ).concat(self.procAsCode())) endif) \r\n\t\t      \r\n\t\t      .concat( \r\n\t\t\t \'\\n  endif\' )  \r\n\t\t     endif))\r\n\t\t    else\r\n\t\t    \r\n\t\t    let procString : String =  if self.processor <> mrules::expressions::MProcessor::None then\r\n\t\t --  let a: String =  \'->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: String = \\\'\\\' | r.concat(i.toString()) )\')  in\r\n\t\t  \t\'let \'.concat(variableName).concat(\': \').concat(chainTypeString).concat(\' = \').concat(code).concat(\' in\\n\').concat(    -- Accessing feature is not unary\r\n\t\t\r\n\t\t\'if \').concat(variableName).concat(if self.isOwnXOCLOperator() then \'.oclIsUndefined() or \'.concat(variableName) else \'\'endif).concat(if self.isProcessorSetOperator() then \'->\' else if not(self.isProcessorCheckEqualOperator()) then \'.\' else \'\' endif endif).concat(self.procAsCode())\r\n\t\t.concat(if self.isProcessorCheckEqualOperator() then \' then true else false \' \r\n\t\t\r\n\t\t\r\n\t\telse (if processor=mrules::expressions::MProcessor::AsOrderedSet then \'->\' else \'.\' endif).concat(\'oclIsUndefined() \\n then null \\n else \'  -- todo   processor does not influence calculatedSingular\r\n.concat(variableName).concat(\r\n\t\t      if self.isProcessorSetOperator() then \'->\' else \'.\' endif --  \"->\" for Set , \".\" for unary ,  nothing if we check for a value\r\n\t\t      ).concat(self.procAsCode())) endif) \r\n\t\t      \r\n\t\t      .concat( \r\n\t\t\t \'\\n  endif\'  )  else code endif in\r\n\t\t\t procString.concat\r\n\t\t\t (if self.chainCalculatedSingular or self.processor <> mrules::expressions::MProcessor::None then \'.toString()\' else  \'->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: String = \\\'\\\' | r.concat(i.toString()) )\')\r\n\t\t\t  endif)\r\n\t\t\t\r\n\t\t     \r\n\t\tendif\r\n'"
	 * @generated
	 */
	String autoCastWithProc();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='OwnToApplyMismatch'"
	 *        annotation="http://www.xocl.org/OCL body='if self.eContainer().oclIsTypeOf(mrules::expressions::MIf) then false   -- Implement IF typemismatch\r\nelse\r\nif self.eContainer().oclIsTypeOf(mrules::expressions::MApplication) and self.containedCollector.oclIsUndefined()\r\nthen \r\nlet app: mrules::expressions::MApplication = self.eContainer().oclAsType(mrules::expressions::MApplication) in\r\nlet opChangesReturn : Boolean = app.operands->first().aSimpleType <> app.aSimpleType or app.operands->first().aClassifier <> app.aClassifier\r\n--let opChangesReturn : Boolean = app.operands->first().calculatedOwnSimpleType <> app.calculatedOwnSimpleType or app.operands->first().calculatedOwnType <> app.calculatedOwnType\r\nin\r\n\r\nif self.base = mrules::expressions::ExpressionBase::SelfObject or  self.base = mrules::expressions::ExpressionBase::Variable\r\nthen -- builtin been casted  : TODO  add Parameter,Iterator etc...\r\n\r\nif (app.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::Boolean and self.aSimpleType <> acore::classifiers::ASimpleType::Boolean) or (app.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::Integer and self.aSimpleType <> acore::classifiers::ASimpleType::Integer)  then \r\n   if app.operands->first().aCalculatedOwnType.oclIsUndefined() then (self.aCalculatedOwnSimpleType <>  app.operands->first().aCalculatedOwnSimpleType and app.aSimpleType<> acore::classifiers::ASimpleType::Real)\r\n    else    app.operands->first().aCalculatedOwnType.oclAsType(acore::classifiers::AClassType).aAllGeneralizedClass()->excludes(self.aCalculatedOwnType) and app.operands->first().aCalculatedOwnType<> self.aCalculatedOwnType\r\n        endif\r\n\r\nelse\r\n\r\n\r\nif ((app.aCalculatedOwnSimpleType = self.aCalculatedOwnSimpleType) or opChangesReturn) and (app.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::None)-- CHanged to App.calcOwnType\r\nthen\r\napp.operands->first().aCalculatedOwnType.oclAsType(acore::classifiers::AClassType).aAllGeneralizedClass()->excludes(self.aCalculatedOwnType) and app.operands->first().aCalculatedOwnType<> self.aCalculatedOwnType\r\nelse if  ((app.aCalculatedOwnSimpleType <> self.aCalculatedOwnSimpleType) or opChangesReturn) and  (app.aSimpleType <> acore::classifiers::ASimpleType::None and (app.aSimpleType <> acore::classifiers::ASimpleType::Real))\r\n-- (app.calculatedSimpleType <> SimpleType::None  and app.calculatedSimpleType <> SimpleType::Boolean)\r\nthen true else false endif\r\n\r\n endif\r\n \r\nendif\r\n\r\nelse\r\n\r\nif self.base = ExpressionBase::SelfObject \r\nthen\r\n(app.aCalculatedOwnSimpleType <> self.aCalculatedOwnSimpleType) and opChangesReturn\r\nelse \r\nfalse\r\nendif\r\nendif\r\n\r\n\r\nelse if not(self.eContainer().oclIsTypeOf(MApplication)) and self.containedCollector.oclIsUndefined() \r\nand self.base = ExpressionBase::SelfObject then     --TODO  add parameter,iterator etc\r\n--self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType or     FOR TYPE\r\nif self.eContainer().oclIsTypeOf(MNamedExpression) and self.eContainer().oclIsTypeOf(MNamedExpression).oclIsTypeOf(mcore::annotations::MResult) and\r\nself.eContainer().oclAsType(MNamedExpression).eContainer().oclAsType(mrules::MRuleAnnotation).namedExpression->asSequence()->last() = self.eContainer() \r\n --   or self.eContainer().oclIsTypeOf(MCollectionExpression)\r\n then\r\nif self.aExpectedReturnType.oclIsUndefined() then self.aExpectedReturnType = self.aCalculatedOwnType and self.aExpectedReturnSimpleType <> self.aCalculatedOwnSimpleType \r\nelse\r\nself.aExpectedReturnType.oclAsType(acore::classifiers::AClassType).aAllGeneralizedClass()->excludes(self.aClassifier) and  self.aExpectedReturnType <>self.aCalculatedOwnType and self.aExpectedReturnSimpleType = self.aCalculatedOwnSimpleType \r\nendif \r\nelse false endif\r\n\r\n\r\nelse\r\n\r\nfalse endif endif\r\nendif\r\n\r\n--if (app.calculatedOwnSimpleType = SimpleType::Double and self.calculatedOwnSimpleType= SimpleType::Integer) then false\r\n--else\r\n --    app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType\r\n--endif\r\n--endif\r\n--else\r\n --null\r\n-- endif'"
	 * @generated
	 */
	Boolean ownToApplyMismatch();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='\t\t        let variableName : String = \'chain\'.concat(if self.eContainer().oclAsType(mrules::expressions::MApplication).oclIsUndefined() then \'\' else  self.eContainer().oclAsType(mrules::expressions::MApplication).uniqueApplyNumber().toString() endif).concat(if self.eContainer().oclAsType(mrules::expressions::MApplication).oclIsUndefined() then \'\' else\r\n\t\t        \r\n\t\t         if (self.eContainer().oclIsTypeOf(mrules::expressions::MApplication))  then \r\n\t\t          if (self.eContainer().oclAsType(mrules::expressions::MApplication).operands->isEmpty()) and self.oclIsTypeOf(mrules::expressions::MChain) then \' \'  else \r\n\t\t           self.eContainer().oclAsType(mrules::expressions::MApplication).operands->iterate(i:mrules::expressions::MChainOrApplication; r: OrderedSet(MBaseChain)=OrderedSet{} | if i.oclIsKindOf(MBaseChain) then r->including(i.oclAsType(MBaseChain))->asOrderedSet() else r endif )->indexOf(self).toString()\r\n\t\t           endif else \'\' \r\n\t\t           endif endif ) in variableName\r\n\t\t           '"
	 * @generated
	 */
	String uniqueChainNumber();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='false\n'"
	 * @generated
	 */
	Boolean reuseFromOtherNoMoreUsedChain(MBaseChain archetype);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='false\n'"
	 * @generated
	 */
	Boolean resetToBase(ExpressionBase base, AVariable baseVar);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model archetypeAnnotation="http://www.montages.com/mCore/MCore mName='archetype'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	String reuseFromOtherNoMoreUsedChainAsUpdate(MBaseChain archetype);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if processor.oclIsUndefined() then false \r\nelse\r\nprocessor=expressions::MProcessor::IsOne or\r\nprocessor=expressions::MProcessor::IsZero or\r\nprocessor = expressions::MProcessor::IsFalse or\r\nprocessor = expressions::MProcessor::IsTrue or\r\nprocessor = expressions::MProcessor::NotNull \r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isProcessorCheckEqualOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if processor.oclIsUndefined() then false \r\nelse\r\nprocessor=expressions::MProcessor::Not or\r\nprocessor = expressions::MProcessor::OneDividedBy\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isPrefixProcessor();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='isPostfixProcessor'"
	 *        annotation="http://www.xocl.org/OCL body='if processor.oclIsUndefined() then false \r\nelse\r\nprocessor=expressions::MProcessor::PlusOne or\r\nprocessor=expressions::MProcessor::MinusOne or\r\nprocessor=expressions::MProcessor::TimesMinusOne or\r\nprocessor=expressions::MProcessor::IsNull or\r\nprocessor=expressions::MProcessor::IsInvalid\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isPostfixProcessor();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='if length()=0 then baseAsCode\r\nelse if length()=1 then codeForLength1() \r\nelse if length()=2 then codeForLength2() \r\nelse if length()=3 then codeForLength3() \r\nelse \'ERROR\' endif endif endif endif '"
	 * @generated
	 */
	String asCodeForOthers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stepRequired="true"
	 *        stepAnnotation="http://www.montages.com/mCore/MCore mName='step'"
	 *        annotation="http://www.xocl.org/OCL body='let element: acore::classifiers::AProperty = if step=1 then aElement1\r\n\telse if step=2 then aElement2 \r\n\t\telse if step=3 then aElement3\r\n\t\t\telse null endif endif endif in\r\n\t\r\nif element.oclIsUndefined() then \'ERROR\' else \r\nif not element.aOperation->isEmpty()\r\nthen if step=length()\r\n\tthen\r\n\t\tlet p: String = let pp: String  = callArgument->iterate(\r\n\t\t\tx: mrules::expressions::MCallArgument; s: String = \'\' | \r\n\t\t  \ts.concat(\', \').concat(x.asCode)\r\n\t\t  ) in if pp.size()>2 then pp.substring(3,pp.size()) else pp endif in\r\n\t\telement.aName.concat(\'(\').concat(p).concat(\')\') \r\n\telse\t\t\r\n\t\telement.aName.concat(\'()\') endif\t\t\r\nelse \r\n\telement.aName \r\nendif endif'"
	 * @generated
	 */
	String unsafeElementAsCode(Integer step);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length1'"
	 *        annotation="http://www.xocl.org/OCL body='let chain:mrules::expressions::MBaseChain = self.oclAsType(mrules::expressions::MBaseChain) in\r\n\r\nlet b: String = if (chain.base = mrules::expressions::ExpressionBase::SelfObject ) then \'\' else chain.baseAsCode.concat(\'.\') endif in \r\nb.concat(unsafeChainStepAsCode(1)).concat(if chain.chainCalculatedSingular then \'\' else \'->asOrderedSet()\' endif)'"
	 * @generated
	 */
	String codeForLength1();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length2'"
	 *        annotation="http://www.xocl.org/OCL body='\r\nlet chain: mrules::expressions::MBaseChain = self.oclAsType(mrules::expressions::MBaseChain) in\r\nlet abstract:  mrules::expressions::MAbstractExpressionWithBase = self.oclAsType(mrules::expressions::MAbstractExpressionWithBase) in\r\nlet b0: String = if (abstract.base = mrules::expressions::ExpressionBase::SelfObject ) then \'\' else abstract.baseAsCode.concat(\'.\') endif in \r\nlet b:String = if b0.oclIsUndefined() then \'PROBLEM WITH BASE\' else b0 endif in\r\nif aElement1.aSingular and aElement2.aSingular then\r\n  let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,2)).concat(\'\\nendif\')\r\n\r\nelse if aElement1.aSingular and (not aElement2.aSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,2).concat(\'\\nendif\'))\r\n\r\nelse  if (not aElement1.aSingular) and aElement2.aSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse  if (not aElement1.aSingular) and (not aElement2.aSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->asOrderedSet()\')\r\n\r\nelse null\r\n\r\nendif endif endif endif'"
	 * @generated
	 */
	String codeForLength2();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length3'"
	 *        annotation="http://www.xocl.org/OCL body='let chain: mrules::expressions::MBaseChain = self.oclAsType(mrules::expressions::MBaseChain) in\r\nlet abstract:  mrules::expressions::MAbstractExpressionWithBase = self.oclAsType(mrules::expressions::MAbstractExpressionWithBase) in\r\n\r\n\r\nlet b: String = if (abstract.base = mrules::expressions::ExpressionBase::SelfObject ) then \'\' else abstract.baseAsCode.concat(\'.\') endif in \r\n\r\nif aElement1.aSingular and aElement2.aSingular and aElement3.aSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'\\nendif\')\r\n\r\nelse if aElement1.aSingular and aElement2.aSingular and (not aElement3.aSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'\\nendif\')\r\n\r\nelse if aElement1.aSingular and (not aElement2.aSingular) and aElement3.aSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\\nendif\')\r\n\r\nelse if aElement1.aSingular and (not aElement2.aSingular) and (not aElement3.aSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'->asOrderedSet()\\nendif\')\r\n\r\nelse if (not aElement1.aSingular) and aElement2.aSingular and aElement3.aSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n  \r\nelse if (not aElement1.aSingular) and aElement2.aSingular and (not aElement3.aSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(if chain.subExpression->isEmpty() and not(self.processorIsSet()) then \'\' else \'->asOrderedSet()\' endif)\r\n\r\nelse if (not aElement1.aSingular) and (not aElement2.aSingular) and aElement3.aSingular then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse if (not aElement1.aSingular) and (not aElement2.aSingular) and (not aElement3.aSingular) then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse null\r\nendif endif endif endif endif endif endif endif '"
	 * @generated
	 */
	String codeForLength3();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='let v: mrules::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mrules::expressions::MVariableBaseDefinition) in\r\nlet vName: String = v.namedExpression.eName in\r\n/*mcore to mrules migration TODO:\r\nlength() not recognized!!\r\nif v.aSingular and (length() > 0)\r\n  then \r\n    \'if \'.concat(vName).concat(\' = null\\n  then null\\n  else \').concat(asCodeForOthers()).concat(\' endif\')\r\n  else asCodeForOthers()\r\n  \052/\r\n  \'TODO mcore to mrules mit in asCodeForVarilables in MBaseChain\'\r\n/*endif \052/'"
	 * @generated
	 */
	String asCodeForVariables();

} // MBaseChain
