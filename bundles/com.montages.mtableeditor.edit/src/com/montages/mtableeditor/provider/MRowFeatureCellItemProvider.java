/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mcore.MProperty;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MtableeditorPackage;
import com.montages.mtableeditor.impl.MRowFeatureCellImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mtableeditor.MRowFeatureCell} object.
 * <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class MRowFeatureCellItemProvider extends MCellConfigItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MRowFeatureCellItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFeaturePropertyDescriptor(object);
			addCellEditBehaviorPropertyDescriptor(object);
			addCellLocateBehaviorPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addMaximumSizePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAverageSizePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addMinimumSizePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSizeOfPropertyValuePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Feature feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature feature.
		 * The list of possible choices is constraint by OCL trg.containingClassifier = class or trg.containingClassifier.allSuperTypes()->includes(class) or
		trg.containingClassifier.allSubTypes()->includes(class)
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRowFeatureCell_feature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRowFeatureCell_feature_feature",
								"_UI_MRowFeatureCell_type"),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE, true, false, true, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<MProperty> result = new ArrayList<MProperty>();
						Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						for (Iterator<MProperty> iterator = result.iterator(); iterator.hasNext();) {
							MProperty trg = iterator.next();
							if (trg == null) {
								continue;
							}
							if (!((MRowFeatureCellImpl) object).evalFeatureChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Cell Edit Behavior feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addCellEditBehaviorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cell Edit Behavior feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRowFeatureCell_cellEditBehavior_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRowFeatureCell_cellEditBehavior_feature",
						"_UI_MRowFeatureCell_type"),
				MtableeditorPackage.Literals.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Cell Locate Behavior feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCellLocateBehaviorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cell Locate Behavior feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRowFeatureCell_cellLocateBehavior_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MRowFeatureCell_cellLocateBehavior_feature", "_UI_MRowFeatureCell_type"),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Maximum Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaximumSizePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Maximum Size feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRowFeatureCell_maximumSize_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRowFeatureCell_maximumSize_feature",
								"_UI_MRowFeatureCell_type"),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL__MAXIMUM_SIZE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_WidthCalculatingPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Average Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAverageSizePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Average Size feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRowFeatureCell_averageSize_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRowFeatureCell_averageSize_feature",
								"_UI_MRowFeatureCell_type"),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL__AVERAGE_SIZE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_WidthCalculatingPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Minimum Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinimumSizePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Minimum Size feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRowFeatureCell_minimumSize_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRowFeatureCell_minimumSize_feature",
								"_UI_MRowFeatureCell_type"),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL__MINIMUM_SIZE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_WidthCalculatingPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Size Of Property Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSizeOfPropertyValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Size Of Property Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRowFeatureCell_sizeOfPropertyValue_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MRowFeatureCell_sizeOfPropertyValue_feature", "_UI_MRowFeatureCell_type"),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_WidthCalculatingPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns MRowFeatureCell.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MRowFeatureCell"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MRowFeatureCell) object).getALabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MRowFeatureCell.class)) {
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
		case MtableeditorPackage.MROW_FEATURE_CELL__MAXIMUM_SIZE:
		case MtableeditorPackage.MROW_FEATURE_CELL__AVERAGE_SIZE:
		case MtableeditorPackage.MROW_FEATURE_CELL__MINIMUM_SIZE:
		case MtableeditorPackage.MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MtableeditorItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
