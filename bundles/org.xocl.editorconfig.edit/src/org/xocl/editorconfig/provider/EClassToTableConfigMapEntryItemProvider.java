/**
 * <copyright>
 * </copyright>
 *
 * $Id: EClassToTableConfigMapEntryItemProvider.java,v 1.11 2011/07/20 17:05:00 mtg.kutter Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;
import org.xocl.editorconfig.impl.EClassToTableConfigMapEntryImpl;

/**
 * This is the item provider adapter for a {@link java.util.Map.Entry} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EClassToTableConfigMapEntryItemProvider extends
		ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassToTableConfigMapEntryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addKeyPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addIntendedPackagePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Key feature.
		 * The list of possible choices is constraint by OCL let p:ecore::EPackage = self.intendedPackage
		in 
		if p.oclIsUndefined() then true
		else if p.eClassifiers->includes(trg) then true
		else p.eSubpackages.eClassifiers->includes(trg)
		endif endif
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EClassToTableConfigMapEntry_key_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_EClassToTableConfigMapEntry_key_feature",
								"_UI_EClassToTableConfigMapEntry_type"),
						EditorConfigPackage.Literals.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__KEY,
						true, false, true, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<EClass> result = new ArrayList<EClass>();
						Collection<? extends EClass> superResult = (Collection<? extends EClass>) super
								.getChoiceOfValues(object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
								result);
						Resource resource = ((EObject) object).eResource();
						if (resource != null) {
							ResourceSet resourceSet = resource.getResourceSet();
							if (resourceSet != null) {
								Collection<EObject> visited = new HashSet<EObject>(
										eObjects);
								Registry packageRegistry = resourceSet
										.getPackageRegistry();
								for (Iterator<String> i = packageRegistry
										.keySet().iterator(); i.hasNext();) {
									collectReachableObjectsOfType(
											visited,
											eObjects,
											packageRegistry.getEPackage(i
													.next()),
											EditorConfigPackage.Literals.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__KEY
													.getEType());
								}
								result = (List<EClass>) (List<?>) eObjects;
							}
						}
						for (Iterator<EClass> iterator = result.iterator(); iterator
								.hasNext();) {
							EClass trg = iterator.next();
							if (trg == null
									|| !((EClassToTableConfigMapEntryImpl) object)
											.evalTypedKeyChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EClassToTableConfigMapEntry_value_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_EClassToTableConfigMapEntry_value_feature",
								"_UI_EClassToTableConfigMapEntry_type"),
						EditorConfigPackage.Literals.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__VALUE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Intended Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntendedPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EClassToTableConfigMapEntry_intendedPackage_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_EClassToTableConfigMapEntry_intendedPackage_feature",
								"_UI_EClassToTableConfigMapEntry_type"),
						EditorConfigPackage.Literals.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__INTENDED_PACKAGE,
						true, false, true, null, null, null));
	}

	/**
	 * This returns EClassToTableConfigMapEntry.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/EClassToTableConfigMapEntry"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		@SuppressWarnings("unchecked")
		Map.Entry<EClass, TableConfig> eClassToTableConfigMapEntry = (Map.Entry<EClass, TableConfig>) object;
		EClass eClass = eClassToTableConfigMapEntry.getKey();
		TableConfig tableConfig = eClassToTableConfigMapEntry.getValue();
		return "" + (eClass != null ? eClass.getName() : eClass) + " -> "
				+ (tableConfig != null ? tableConfig.getLabel() : tableConfig);
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EditorConfigEditPlugin.INSTANCE;
	}

}
