/**
 * <copyright>
 * </copyright>
 *
 * $Id: CellConfigItemProvider.java,v 1.1 2010/11/30 08:04:12 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.impl.CellConfigImpl;

/**
 * This is the item provider adapter for a {@link org.xocl.editorconfig.CellConfig} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CellConfigItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFontPropertyDescriptor(object);
			addColorPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Font feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFontPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Font feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_CellConfig_font_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_CellConfig_font_feature", "_UI_CellConfig_type"),
				EditorConfigPackage.Literals.CELL_CONFIG__FONT, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Color feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Color feature.
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_CellConfig_color_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_CellConfig_color_feature", "_UI_CellConfig_type"),
				EditorConfigPackage.Literals.CELL_CONFIG__COLOR, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null) {
			@Override
			public OCLExpressionContext getOCLExpressionContext(Object object) {
				return ((CellConfigImpl) object).getColorOCLExpressionContext();
			}
		});
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((CellConfig) object).getColor();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<"
				+ containingFeatureName + ">" : "<" + containingFeatureName
				+ ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CellConfig.class)) {
		case EditorConfigPackage.CELL_CONFIG__FONT:
		case EditorConfigPackage.CELL_CONFIG__COLOR:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EditorConfigEditPlugin.INSTANCE;
	}

}
