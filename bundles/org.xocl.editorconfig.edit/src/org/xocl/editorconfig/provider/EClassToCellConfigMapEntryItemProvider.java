/**
 * <copyright>
 * </copyright>
 *
 * $Id: EClassToCellConfigMapEntryItemProvider.java,v 1.9 2010/12/08 04:13:50 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.EditorConfigFactory;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl;

/**
 * This is the item provider adapter for a {@link java.util.Map.Entry} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EClassToCellConfigMapEntryItemProvider extends ItemProviderAdapter
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassToCellConfigMapEntryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addKeyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Key feature.
		 * The list of possible choices is constraint by OCL let cc:editorconfig::ColumnConfig = self.eContainer().oclAsType(editorconfig::ColumnConfig)
		in 
		let tc:editorconfig::TableConfig = cc.eContainer().oclAsType(editorconfig::TableConfig) in
		let  ic:ecore::EClass = tc.intendedClass in 
		let intended:Boolean = if not ic.oclIsUndefined()
		then   
		  trg=ic or trg.eAllSuperTypes->includes(ic)
		else
		let ip:ecore::EPackage = tc.intendedPackage in
		if ip.oclIsUndefined() then true 
		else 
		  if ip.eClassifiers->includes(trg) then true
		  else ip.eSubpackages.eClassifiers->includes(trg)
		  endif
		endif
		endif
		in
		let unique:Boolean = 
		self.key = trg or not (cc.cellConfigMap.key->includes(trg))
		in 
		intended and unique
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EClassToCellConfigMapEntry_key_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_EClassToCellConfigMapEntry_key_feature",
								"_UI_EClassToCellConfigMapEntry_type"),
						EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY,
						true, false, true, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<EClass> result = new ArrayList<EClass>();
						Collection<? extends EClass> superResult = (Collection<? extends EClass>) super
								.getChoiceOfValues(object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
								result);
						Resource resource = ((EObject) object).eResource();
						if (resource != null) {
							ResourceSet resourceSet = resource.getResourceSet();
							if (resourceSet != null) {
								Collection<EObject> visited = new HashSet<EObject>(
										eObjects);
								Registry packageRegistry = resourceSet
										.getPackageRegistry();
								for (Iterator<String> i = packageRegistry
										.keySet().iterator(); i.hasNext();) {
									collectReachableObjectsOfType(
											visited,
											eObjects,
											packageRegistry.getEPackage(i
													.next()),
											EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY
													.getEType());
								}
								result = (List<EClass>) (List<?>) eObjects;
							}
						}
						for (Iterator<EClass> iterator = result.iterator(); iterator
								.hasNext();) {
							EClass trg = iterator.next();
							if (trg == null
									|| !((EClassToCellConfigMapEntryImpl) object)
											.evalTypedKeyChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EClassToCellConfigMapEntry.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/EClassToCellConfigMapEntry"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		@SuppressWarnings("unchecked")
		Map.Entry<EClass, CellConfig> eClassToCellConfigMapEntry = (Map.Entry<EClass, CellConfig>) object;
		EClass eClass = eClassToCellConfigMapEntry.getKey();
		return "" + (eClass != null ? eClass.getName() : eClass);
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Map.Entry.class)) {
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
						EditorConfigFactory.eINSTANCE.createRowFeatureCell()));

		newChildDescriptors
				.add(createChildParameter(
						EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
						EditorConfigFactory.eINSTANCE.createOclCell()));

		newChildDescriptors
				.add(createChildParameter(
						EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
						EditorConfigFactory.eINSTANCE.createEditProviderCell()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EditorConfigEditPlugin.INSTANCE;
	}

}
