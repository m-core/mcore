/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigItemProvider.java,v 1.6 2009/07/04 12:25:24 kutterxocl Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigFactory;
import org.xocl.editorconfig.EditorConfigPackage;

/**
 * This is the item provider adapter for a {@link org.xocl.editorconfig.EditorConfig} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EditorConfigItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addHeaderTableConfigPropertyDescriptor(object);
			addSuperConfigPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Header Table Config feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHeaderTableConfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Header Table Config feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EditorConfig_headerTableConfig_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_EditorConfig_headerTableConfig_feature",
								"_UI_EditorConfig_type"),
						EditorConfigPackage.Literals.EDITOR_CONFIG__HEADER_TABLE_CONFIG,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Super Config feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuperConfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Super Config feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_EditorConfig_superConfig_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_EditorConfig_superConfig_feature",
						"_UI_EditorConfig_type"),
				EditorConfigPackage.Literals.EDITOR_CONFIG__SUPER_CONFIG, true,
				false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(EditorConfigPackage.Literals.EDITOR_CONFIG__TABLE_CONFIG);
			childrenFeatures
					.add(EditorConfigPackage.Literals.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EditorConfig.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/EditorConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return "<" + containingFeatureName + ">";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EditorConfig.class)) {
		case EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG:
		case EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				EditorConfigPackage.Literals.EDITOR_CONFIG__TABLE_CONFIG,
				EditorConfigFactory.eINSTANCE.createTableConfig()));

		newChildDescriptors
				.add(createChildParameter(
						EditorConfigPackage.Literals.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP,
						EditorConfigFactory.eINSTANCE
								.create(EditorConfigPackage.Literals.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY)));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EditorConfigEditPlugin.INSTANCE;
	}

}
