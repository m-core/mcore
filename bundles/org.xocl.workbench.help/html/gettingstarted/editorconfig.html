<html>
<head>
<meta name="copyright"
    content="Copyright (c) Montages AG">
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="STYLESHEET" href="../book.css" charset="ISO-8859-1"
    type="text/css">
<title>XOCL User Guide - Configuring Table Editor</title>
<script language="JavaScript"
    src="PLUGINS_ROOT/org.eclipse.help/livehelp.js"></script>
</head>

<body>

<h2>Table Editor <code>.editorconfig</code> file</h2>


<h3>Introduction</h3>

<p>Table editor is highly configurable. Cells arrangement and contents displayed are configured
by means of an <i>Editor Config</i> configuration file (extension <code>.editorconfig</code>). The file is 
created automatically when you run <i>XOCL &gt; Implement</i> action on a <code>.ecore</code> or <code>.editorconfig</code> file,
and by default is configured so that the table editor will show all features for each <i>EClass</i> in the 
model being implemented.</p> 

<p>Here is an example of a table editor:</p>

        <img alt="Table Editor" src="./img/tableEditor_default.png">

<h4>Editing Editor Config</h4> 

<p>You can manually tweak the <i>Editor Config</i> file created automatically to change default table
editor appearance and behavior, as explained below. If you do so, you must pay attention not to overwrite your changes 
next time you run the <i>XOCL &gt; Implement</i> action. Three options are available:</p>

<ul>
    <li><i>Discard existing <i>Editor Config</i>uration</i>: Existing <code>.editorconfig</code> file is replaced by a new one that is
        automatically generated.</li>
        
    <li><i>Keep <i>Editor Config</i>uration</i>: Existing <code>.editorconfig</code> is left untouched. Note that, if new features were added to the model, 
        they won't be displayed in the resulting table editor, unless they are added manually as explained below.
        This option is used to preserve any manual configuration done on the <i>Editor Config</i>.</li>
        
    <li><i>Merge with existing <i>Editor Config</i>uration</i>: A new <code>.editorconfig</code> file is generated automatically, then all <i>Table Config</i> (see below)
        that are present in the old <code>.editorconfig</code> but in the new one are added.
        This option lets you manually configure the Table Editor by renaming <i>Table Config</i> created automatically. Still, if new
        classes are added to the model, new <i>Table Config</i> will be generated automatically for them.</li>
</ul>

<h4>Creating new Editor Config</h4>

<p>You can create an empty <i>Editor Config</i> to use, to do so choose:<br/><br/> 

<i>File &gt; New &gt; Other... &gt; MCore Model Creation Wizards &gt; EditorConfig Model</i></p>


<h4>Choosing which Editor Config to use</h4>

<p>You can instruct Eclipse to use a specific <i>Editor Config</i> for a model by using 
<code>org.xocl<code>.editorconfig</code><code>.editorconfig</code>Associations</code> extension point.</p>

<p>When you implement a model, XOCL generates a <code>plugin.xml</code> file inside your XOCL project.
By default, this file is configured so the automatically created <i>Editor Config</i> is 
assigned to edit the models in the project.</p>

<p>You can explicitly assign an <i>Editor Config</i> of your choice by editing <code>editorConfigModelAssociation</code>
for the URI of your model. Should you want to use models from other plug-ins, you can assign an
editor to them here as well, by adding corresponding <code>editorConfigModelAssociation</code>.</p>

<pre>
  &lt;extension point=&quot;org.xocl.editorconfig.editorconfigAssociations&quot;&gt;
    &lt;editorConfigModelAssociation config=&quot;model/my.editorconfig&quot; uri=&quot;http://org.example.my&quot;/&gt;
  &lt;/extension&gt;
</pre>

<p>Note that re-implementing your models will overwrite the <code>plugin.xml</code> file. It is possible to avoid that, by creating a
simple text file named <code>XOCLGENERATENOT</code> in your project home folder. Inside the file, you can list,
one on each row, the files NOT to overwrite when implementing your models. The names are relative to the workspace.</p>

<p>For example, if you have a project named <code>com.mycompany.xocl</code> in your workspace and you alter
its <code>plugin.xml</code> file as descirbed above, to prevent it from being overwritten you must:</p>

<ol>
    <li>Create a text file named <code>XOCLGENERATENOT</code> in your project root folder.</li>

    <li>Add the following line to it:<br/><br/>
    
        <code>/com.mycompany.xocl/plugin.xml</code></li>
</ol>

<h3>Editor Config reference</h3>

<p>This section describes the structure of an <i>Editor Config</i> (<code>.editorconfig</code>) file.</p>

<p>Following is a class diagram that shows the classes described below.</p>   

        <img alt="Table Editor class diagram" src="./img/tableEditor_classdiagram.png">



<h4>Editor Config</h4>

<p>This is the root class for <i>Editor Config</i> files.</p>

<ul>
	<li><i>Header <i>Table Config</i></i>: Allows to specify a <i>Table Config</i> which headers will be used to 
	   populate the headers for the table editor.
	
	<li><i>Super Config</i>: Reserved for future use.
</ul>

<p>Inside an <i>Editor Config</i>, a table editor is configured to render specific <i>EClass</i> instances.
This is done by creating a <i>Table Config</i> for each of the class being displayed. Also,
it's possible to reference existing <i>Table Config</i> to use by creating a
<i>EClass</i> to <i>Table Config Map Entry</i>.</p>
	
	
<h5>Table Config</h5>

<p><i>Table Config</i> is the basic unit of configuration in table editor.</p>

<p>One and each <i>EClass</i> displayed within the editor is rendered accordingly to the <i>Table Config</i>
associated to it.<p>
		
<ul>		
	<li><i>Intended Class &amp; Package</i>: These are used to define the <i>EClass</i> associated to the <i>Table Config</i>.
		Instances of the class will be rendered using the <i>Table Config</i> specified here.
		Note that the <code>.editorconfig</code> editor will show two columns for each of these properties. Those
		labeled &quot;(select)&quot; are used to select a value for the field, those labeled &quot;(locate)&quot; will
		locate the corresponding <i>EClass</i> or package in the editor, should you click on them.<br/><br/>
		
        <img alt="Select and Locate columns in Table Editor" src="./img/tableEditor_SelectLocate.png">
    </li>
	<li><i>Display Default Column</i>: (default false) The first column in the table editor will show an icon
		for the <i>EClass</i> displayed in the row. If this property is set to true, next to
		the icon a text will appear shoving the name of the containing feature (if any) in angle brackets
		and the name of the node (if any).<br/><br/> 
		
		<img alt="Table Editor with Display Default Column set to true" src="./img/tableEditor_DisplayDefaultColumn_true.png">
    </li>		
	<li><i>Display Default Header</i>: (default true) Controls if headers will show in table editor.<br/><br/> 
        
        <img alt="Table Editor with Display Default Header set to false" src="./img/tableEditor_DisplayDefaultHeader_false.png">
    </li>
	<li><i>Label</i>: Header for the first column with the class (which usually is named after the corresponding
		<i>EClass</i>).
    </li>
	<li><i>Name</i>: Name for the <i>Table Config</i>.</li>
</ul>
	
<p><i>Table Config</i> holds a list of <i>Column Config</i>, which specify how every column in the table has to be rendered.</p>	
    
    
<h6>Column Config</h6>
 
<p><i>Column Config</i> specify how each column in a <i>Table Config</i> has to be rendered.</p>
 
<ul>
 	<li><i>Label</i>: The header for the column. This defaults to the name of the feature shown in the column.</li>

 	<li><i>Name</i>: Unique name for the <i>Column Config</i>.</li>
 	
 	<li><i>Width</i>: Size of the column in pixel, or 0 to use default width.</li>
</ul> 	

<p>Inside a <i>Column Config</i>, one or more <i>EClass to Cell Config Map Entry</i> can be added, to describe the behavior
of column cells.</p>

 	
<h7>EClass to Cell Config Map Entry</h7>

<p>This class specifies the actual content being shown in the cell and the corresponding cell behavior.</p>

<ul>
	<li><i>Key</i>: The <i>EClass</i> from which to fetch data when displaying cell contents. This must be the <i>EClass</i> specified as
		&quot;<i>Intended Class</i>&quot; for the <i>Table Config</i> this column belongs to, or one of its subclasses.</li>
</ul>

<p>For each <i>Column Config</i>, more than one <i>EClass to Cell Config Map Entry</i> can be added, so the column behavior can be
made different depending on the actual subclass being shown (for example, a field in a sub-class could be read only
while it's editable in the root <i>EClass</i>).</>  	

<p>Inside each cell, it is possible to show one of the class features, a calculated value or default editor labels.</p>


<h8>Row Feature Cell</h8>

<p>Add this to an <i>EClass to Cell Config Map Entry</i> to show a feature of the <i>EClass</i> assigned 
as &quot;<i>Key</i>&quot; for <i>EClass to Cell Config Map Entry</i>.</p>

<ul>
	<li><i>Feature</i>: The feature being shown in the cell.</li>

    <li><i>Cell Locate Behavior</i>: Describes what happens when a cell showing a reference is clicked.
        If the cell is showing an attribute, this property is ignored.
    
        <ul>
        <li><i>None</i>: Nothing happens (see below).</li>
            
        <li><i>Selection</i>:The target of the reference (if any) is selected in the editor.</li>
        </ul>
    </li>
			
	<li><i>Cell Edit Behavior</i>: Describes what happens when user tries to edit a cell.
	   
	    <ul>
		<li><i>Selection</i>:
		      If the cell contains an attribute, users can edit cell contents. For references, 
		      users can enter &quot;<a href="tableeditor.html#RawEdit">Raw selection editing</a>&quot; 
		      by clicking on the cell while holding the Alt key down.
		      The cell turns yellow and user is allowed to pick up the target(s) of the reference, by clicking onto it.
        </li>
		      
		<li><i>PullDown</i>:
              If the cell contains an attribute, users can edit cell contents. For references, 
		      the editor displays a pull-down allowing to choose possible target for the reference. 
		      Note that if <i>Cell Locate Behavior</i> is set to &quot;<i>Selection</i>&quot; for a reference, 
		      users must hold down Alt key while clicking for the pull-down to appear 
		      (otherwise locate behavior takes precedence).
        </li>
		       
		<li><i>ReadOnly</i>:
            The cell is read-only.
        </li>
        </ul>
    </li>
</ul>

    
<h8>OCL Cell</h8>

<p>Add this to an <i>EClass to Cell Config Map Entry</i> to display the value returned by an OCL expression.</p>

<ul>
	<li><i>Expression</i>: an OCL expression which returns a string that will be shown in the column.
		Inside the expression, <code>self</code> refers to the object assigned to the cell,
		which is an instance of the <i>EClass</i> used as &quot;<i>Key</i>&quot; in 
		<i>EClass to Cell Config Map Entry</i>.

	<li><i>Cell Locate Behavior</i>: Reserved for future use.
</ul>

		
<h8>Edit Provider Cell</h8>

<p>Add this to an <i>EClass to Cell Config Map Entry</i> to show  the default label returned by the EMF editors.</p>




<h6>EReference To Table Config Map Entry</h6>    

References for object being shown inside a table editor are rendered using table nesting.
A new table editor is nested inside the current one, to display features of referenced objects. 

Thus, it is necessary to provide a <i>Table Config</i> to specify how such referenced objects are displayed, the 
<i>EReference To Table Config Map Entry</i> serves this purpose.
    
    <li><i>Key</i>: The reference which objects will be rendered using given <i>Table Config</i>.
         Note that the &quot;(locate)/(select)&quot; columns are available here as well.
    
    <li><i>Value</i>: The <i>Table Config</i> to use for rendering the referenced objects.
         Note that the &quot;(locate)/(select)&quot; columns are available here as well.
         
    <li><i>Label</i>: Label to use as first column of the nested table.



<h5>EClass to Table Config Map Entry</h5>

<p>It is possible to assign to an <i>EClass</i> a <i>Table Config</i> defined elsewhere (in another <code>.editorconfig</code> file), and 
conversely to assign a specific <i>Table Config</i> to an existing <i>EClass</i>. 
To do so, create an <i>EClass</i> to <i>Table Config Map Entry</i>.</p>
	
<ul>	
    <li><i>Intended Package & Key</i>: Use these to specify the <i>EClass</i> and its package to be shown using
        the selected <i>Table Config</i>. Note that the &quot;(locate)/(select)&quot; columns are available here as well.
        
    <li><i>Value</i>: the <i>Table Config</i> to use for rendering the selected <i>EClass</i>.
</ul>

</body>
</html>	