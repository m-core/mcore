/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.xocl.core.packages.IPackageResourceProvider;

/**
 * @author Max Stepanov
 *
 */
public class PlatformPackageResourceProvider implements IPackageResourceProvider {

	/* (non-Javadoc)
	 * @see org.xocl.core.packages.IPackageResourceProvider#findResourceURI(java.lang.String)
	 */
	public URI findResourceURI(String nsURI, EObject eObject) {
		Map<String, URI> ePackageNsURItoGenModelLocationMap = EcorePlugin.getEPackageNsURIToGenModelLocationMap(false);
		URI location = ePackageNsURItoGenModelLocationMap.get(nsURI);
		if (location != null) {
            ResourceSet resourceSet = new ResourceSetImpl();
            resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(false));
            EcoreUtil.resolveAll(resourceSet.getResource(location, true));
            for (Resource resource : resourceSet.getResources()) {
            	for (EPackage ePackage : getAllPackages(resource)) {
            		if (nsURI.equals(ePackage.getNsURI())) {
            			return resource.getURI();
            		}
            	}
            }
		}
		return null;
	}

    private Collection<EPackage> getAllPackages(Resource resource) {
		List<EPackage> result = new ArrayList<EPackage>();
		for (TreeIterator<?> j = new EcoreUtil.ContentTreeIterator<Object>(resource.getContents()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<? extends EObject> getEObjectChildren(
					EObject eObject) {
				return eObject instanceof EPackage ? ((EPackage) eObject)
						.getESubpackages().iterator() : Collections
						.<EObject> emptyList().iterator();
			}
		}; j.hasNext();) {
			Object content = j.next();
			if (content instanceof EPackage) {
				result.add((EPackage) content);
			}
		}
		return result;
    }

}
