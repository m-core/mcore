/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.core.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.xocl.core.XOCLCorePlugin;
import org.xocl.core.packages.IPackageResourceProvider;

/**
 * @author Max Stepanov
 *
 */
public class CommonPackageResourceProvider implements IPackageResourceProvider {

	private static final String EXTENSION_POINT_ID = XOCLCorePlugin.PLUGIN_ID + ".package_resource_provider"; //$NON-NLS-1$
	private static final String TAG_PROVIDER = "provider"; //$NON-NLS-1$
	private static final String ATT_CLASS = "class"; //$NON-NLS-1$

	private List<IPackageResourceProvider> providers = new ArrayList<IPackageResourceProvider>();
	
	/**
	 * 
	 */
	public CommonPackageResourceProvider() {
		readElements();
		providers.add(new PlatformPackageResourceProvider());
	}

	
	/* (non-Javadoc)
	 * @see org.xocl.core.packages.IPackageResourceProvider#findResourceURIForNamespace(java.lang.String)
	 */
	public URI findResourceURI(String nsURI, EObject eObject) {
		return findFromProviders(nsURI, eObject);
	}


	private URI findFromProviders(String nsURI, EObject eObject) {
		URI uri = null;
		for (IPackageResourceProvider provider : providers) {
			uri = provider.findResourceURI(nsURI, eObject);
			if (uri != null) {
				break;
			}
		}
		return uri;
	}

	private void readElements() {
		IConfigurationElement[] elements = Platform.getExtensionRegistry()
							.getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (int i = 0; i < elements.length; ++i) {
			readElement(elements[i]);
		}
	}
	
	private void readElement(IConfigurationElement element) {
		if (!element.getName().equals(TAG_PROVIDER)) {
			return;
		}
		
		String clazz = element.getAttribute(ATT_CLASS);
		if (clazz == null || clazz.length() == 0) {
			return;
		}
		Object object = null;
		try {
			object = element.createExecutableExtension(ATT_CLASS);
		} catch (CoreException e) {
			XOCLCorePlugin.log(e.getStatus());
		}
		if (object instanceof IPackageResourceProvider) {
			providers.add((IPackageResourceProvider) object);
		}
		
	}
	
}
