/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;

/**
 * @author Max Stepanov
 *
 */
public final class ExternalReference<T extends EObject> {
	
	private URI uri;
	
	private ExternalReference(URI uri) {
		this.uri = uri;
	}
	
	/**
	 * @return the uri
	 */
	public URI getURI() {
		return uri;
	}

	@SuppressWarnings("unchecked")
	public T getEObject(ResourceSet resourceSet) {
		return (T) resourceSet.getEObject(uri, true);
	}
	
	public boolean IsSameEObject(EObject eObject) {
		return uri.equals(getURI((eObject)));
	}

	private static URI getURI(EObject eObject) {
		if (eObject == null) {
			return null;
		}
		URI uri = null;
		Resource resource = eObject.eResource();
		if (resource instanceof XMLResource) {
			XMLHelper xmlHelper = new XMLHelperImpl((XMLResource) resource);
			String href = xmlHelper.getHREF(eObject);
			uri = resource.getURI().appendFragment(href.substring(1));
		}
		return uri;
	}

	public static ExternalReference<? extends EObject> create(EObject eObject) {
		URI uri = getURI(eObject);
		if (uri != null) {
			return new ExternalReference<EObject>(uri);
		}
		return null;
	}
	
}
