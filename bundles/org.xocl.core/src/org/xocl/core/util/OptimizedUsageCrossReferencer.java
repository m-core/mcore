package org.xocl.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.impl.EClassImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentsEList;
import org.eclipse.emf.ecore.util.ECrossReferenceEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class OptimizedUsageCrossReferencer extends
		EcoreUtil.UsageCrossReferencer {

	private static final long serialVersionUID = -6766467368879755248L;

	public OptimizedUsageCrossReferencer(ResourceSet resourceSet) {
		super(resourceSet);
	}

	public OptimizedUsageCrossReferencer(EObject eObject) {
		super(eObject);
	}

	public OptimizedUsageCrossReferencer(Resource resource) {
		super(resource);
	}

	@Override
	public Map<EObject, Collection<EStructuralFeature.Setting>> findAllUsage(
			Collection<?> eObjectsOfInterest) {
		return super.findAllUsage(eObjectsOfInterest);
	}

	@Override
	public Collection<Setting> findUsage(EObject eObject) {
		return super.findUsage(eObject);
	}

	protected EContentsEList.FeatureIterator<EObject> getCrossReferences(
			EObject eObject) {

		ECrossReferenceEList<EObject> optimized = changeableCrossReferenceList(eObject);
		return (EContentsEList.FeatureIterator<EObject>) optimized.iterator();
	}

	public static <T> ECrossReferenceEList<T> changeableCrossReferenceList(
			EObject eObject) {
		EStructuralFeature[] allCrossRefs = ((EClassImpl.FeatureSubsetSupplier) eObject
				.eClass().getEAllStructuralFeatures()).crossReferences();

		if (allCrossRefs == null) {
			return ECrossReferenceEList.<T> emptyCrossReferenceEList();
		}
		EStructuralFeature[] changeableFeatures = filterUnchangeable(allCrossRefs);
		return new ECrossReferenceEListExt<T>(eObject, changeableFeatures);
	}

	private static EStructuralFeature[] filterUnchangeable(
			EStructuralFeature[] features) {
		ArrayList<EStructuralFeature> result = null;
		int sizeNeeded = features.length;
		for (EStructuralFeature next : features) {
			if (next.isChangeable()) {
				if (result == null) {
					result = new ArrayList<EStructuralFeature>(sizeNeeded);
				}
				result.add(next);
			} else {
				sizeNeeded--;
			}
		}
		return result == null ? NO_FEATURES : (EStructuralFeature[]) result
				.toArray(new EStructuralFeature[result.size()]);
	}

	private static final EStructuralFeature[] NO_FEATURES = new EStructuralFeature[0];

	private static class ECrossReferenceEListExt<T> extends
			ECrossReferenceEList<T> {
		public ECrossReferenceEListExt(EObject eObject,
				EStructuralFeature[] features) {
			super(eObject, features);
		}
	}

}
