package org.xocl.core.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class OptimizedEcoreUtil {

	/**
	 * Copy of the {@link EcoreUtil#delete(EObject, boolean)} with the only
	 * difference of using {@link OptimizedUsageCrossReferencer} to avoid time
	 * consuming computation of the references that are unchangeable and ignored
	 * in results anyway.
	 * 
	 * @see EcoreUtil#delete(EObject, boolean)
	 */
	public static void delete(EObject eObject) {
		EObject rootEObject = EcoreUtil.getRootContainer(eObject);
		Resource resource = rootEObject.eResource();

		Collection<EStructuralFeature.Setting> usages;
		if (resource == null) {
			usages = new OptimizedUsageCrossReferencer(rootEObject)
					.findUsage(eObject);
		} else {
			ResourceSet resourceSet = resource.getResourceSet();
			if (resourceSet == null) {
				usages = new OptimizedUsageCrossReferencer(resource)
						.findUsage(eObject);
			} else {
				usages = new OptimizedUsageCrossReferencer(resourceSet)
						.findUsage(eObject);
			}
		}

		for (EStructuralFeature.Setting setting : usages) {
			if (setting.getEStructuralFeature().isChangeable()) {
				EcoreUtil.remove(setting, eObject);
			}
		}

		EcoreUtil.remove(eObject);
	}

	/**
	 * Copy of the {@link EcoreUtil#delete(EObject)} with the only difference of
	 * using {@link OptimizedUsageCrossReferencer} to avoid time consuming
	 * computation of the references that are unchangeable and ignored in
	 * results anyway.
	 * 
	 * @see EcoreUtil#delete(EObject)
	 */
	public static void delete(EObject eObject, boolean recursive) {
		if (recursive) {
			EObject rootEObject = EcoreUtil.getRootContainer(eObject);
			Resource resource = rootEObject.eResource();

			Set<EObject> eObjects = new HashSet<EObject>();
			Set<EObject> crossResourceEObjects = new HashSet<EObject>();
			eObjects.add(eObject);
			for (@SuppressWarnings("unchecked")
			TreeIterator<InternalEObject> j = (TreeIterator<InternalEObject>) (TreeIterator<?>) eObject
					.eAllContents(); j.hasNext();) {
				InternalEObject childEObject = j.next();
				if (childEObject.eDirectResource() != null) {
					crossResourceEObjects.add(childEObject);
				} else {
					eObjects.add(childEObject);
				}
			}

			final Map<EObject, Collection<EStructuralFeature.Setting>> usages;
			if (resource == null) {
				usages = new OptimizedUsageCrossReferencer(rootEObject)
						.findAllUsage(eObjects);
			} else {
				ResourceSet resourceSet = resource.getResourceSet();
				if (resourceSet == null) {
					usages = new OptimizedUsageCrossReferencer(rootEObject)
							.findAllUsage(eObjects);
				} else {
					usages = new OptimizedUsageCrossReferencer(resourceSet)
							.findAllUsage(eObjects);
				}
			}

			for (Map.Entry<EObject, Collection<EStructuralFeature.Setting>> entry : usages
					.entrySet()) {
				EObject deletedEObject = entry.getKey();
				Collection<EStructuralFeature.Setting> settings = entry
						.getValue();
				for (EStructuralFeature.Setting setting : settings) {
					if (!eObjects.contains(setting.getEObject())
							&& setting.getEStructuralFeature().isChangeable()) {
						EcoreUtil.remove(setting, deletedEObject);
					}
				}
			}

			EcoreUtil.remove(eObject);

			for (EObject crossResourceEObject : crossResourceEObjects) {
				EcoreUtil.remove(crossResourceEObject.eContainer(),
						crossResourceEObject.eContainmentFeature(),
						crossResourceEObject);
			}
		} else {
			delete(eObject);
		}
	}
}
