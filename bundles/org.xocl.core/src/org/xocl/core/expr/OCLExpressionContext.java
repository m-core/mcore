/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.expr;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.types.OCLStandardLibrary;

/**
 * @author Max Stepanov
 *
 */
public class OCLExpressionContext {

	private static OCLStandardLibrary<EClassifier> oclStandardLibrary;
	private static EClass eObjectClass;
	
	private EClassifier context;
	private Variable[] variables;
	private EClassifier[] resultTypes;
	private boolean isEditable;
	
	/**
	 * @param context
	 * @param resultTypes
	 * @param variables
	 */

	public OCLExpressionContext(EClassifier context, Variable[] variables, EClassifier[] resultTypes) {
		this(context, variables, resultTypes, true);
	}

	public OCLExpressionContext(EClassifier context, Variable[] variables, EClassifier[] resultTypes, boolean editable) {
		this.context = context;
		this.resultTypes = resultTypes;
		this.variables = variables;
		this.isEditable = editable;
	}

	/**
	 * @return the context
	 */
	public EClassifier getContext() {
		return context;
	}

	/**
	 * @return the variables
	 */
	public Variable[] getVariables() {
		return variables;
	}

	/**
	 * @return the resultTypes
	 */
	public EClassifier[] getResultTypes() {
		return resultTypes;
	}

	/**
	 * @return the OCL standard library
	 */
	public static OCLStandardLibrary<EClassifier> getOCLStandardLibrary() {
		if (oclStandardLibrary == null) {
			oclStandardLibrary = EcoreEnvironmentFactory.INSTANCE.createEnvironment().getOCLStandardLibrary();
		}
		return oclStandardLibrary;
	}
	
	public static EClassifier getEObjectClass() {
		if (eObjectClass == null) {
			eObjectClass = EcoreFactory.eINSTANCE.createEClass();
			eObjectClass.setName("EObject");
		}
		return eObjectClass;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean editable) {
		isEditable = editable;
	}
}
