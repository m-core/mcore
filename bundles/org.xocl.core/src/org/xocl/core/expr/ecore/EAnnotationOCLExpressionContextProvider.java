/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.expr.ecore;

import java.util.Map;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.xocl.core.expr.IOCLExpressionContextProvider;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.expr.XOCLElementContext;

/**
 * @author Max Stepanov
 *
 */
public class EAnnotationOCLExpressionContextProvider implements IOCLExpressionContextProvider {
	
	/**
	 * 
	 */
	private EAnnotationOCLExpressionContextProvider() {
	}

	/* (non-Javadoc)
	 * @see org.xocl.core.expr.IOCLExpressionContextProvider#getOCLExpressionContext(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public OCLExpressionContext getOCLExpressionContext(Object object) {
		if (object instanceof EStringToStringMapEntryImpl) {
			EAnnotation eAnnotation = (EAnnotation) ((EObject) object).eContainer();
			Map.Entry<String, String> mapEntry = (Map.Entry<String, String>)object;
			String detailKey = mapEntry.getKey();
			try {
				XOCLElementContext elementContext = XOCLElementContext.getElementContext(eAnnotation, detailKey);
				if (elementContext != null) {
					return new OCLExpressionContext(elementContext.getContextClassifier(), elementContext.getVariables(), elementContext.getResultTypes());
				}
			} catch (DiagnosticException e) {
				// Do nothing. We shouldn't get here in runtime!
			}
		}
		return null;
	}

	public static class Factory implements IAdapterFactory {
		
		/* (non-Javadoc)
		 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object, java.lang.Class)
		 */
		@SuppressWarnings("unchecked")
		public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
			if (adapterType == IOCLExpressionContextProvider.class) {
				if (adaptableObject instanceof EAttribute) {
					EClassifier eClassifier = (EClassifier) ((EObject) adaptableObject).eContainer();
					if ("EStringToStringMapEntry".equals(eClassifier.getName()) && "value".equals(((EAttribute)adaptableObject).getName())) {
						return (T)new EAnnotationOCLExpressionContextProvider();
					}
				}
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
		 */
		public Class<?>[] getAdapterList() {
			return new Class[] { IOCLExpressionContextProvider.class };
		}
		
	}

}
