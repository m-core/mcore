/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.expr;


/**
 * @author Max Stepanov
 *
 */
public interface IOCLExpressionContextProvider {

	public OCLExpressionContext getOCLExpressionContext(Object object);
}
