/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.expr;

import java.text.MessageFormat;
import java.util.Iterator;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageRegistryImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.ocl.Environment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.BagType;
import org.eclipse.ocl.ecore.CollectionType;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.OrderedSetType;
import org.eclipse.ocl.ecore.SequenceType;
import org.eclipse.ocl.ecore.SetType;
import org.eclipse.ocl.ecore.TupleType;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.ecore.VoidType;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.eclipse.ocl.utilities.UMLReflection;
import org.xocl.core.XOCLPlugin;
import org.xocl.core.util.DelegatingPackageRegistry;
import org.xocl.core.util.XoclLibrary;

/**
 * @author Max Stepanov
 *
 */
public final class OCLExpressionUtils {

	private OCLExpressionUtils() {
	}
	
	public static Diagnostic validateExpression(String expression, XOCLElementContext context, ResourceSet resourceSet) {
		BasicDiagnostic diagnostic = new BasicDiagnostic();
		try {
			OCL.Query oclQuery = prepareExpression(expression, context, resourceSet);
			EClassifier resultType = oclQuery.resultType();
			EClassifier[] requiredTypes = context.getResultTypes();
			if (checkTypes(requiredTypes, resultType, diagnostic)
					&& diagnostic.recomputeSeverity() == Diagnostic.OK) {
				return BasicDiagnostic.OK_INSTANCE;
			}
		} catch (ParserException e) {
			return BasicDiagnostic.toDiagnostic(e);
		}
		return diagnostic;
	}
	
	public static boolean checkTypes(EClassifier[] requiredTypes, EClassifier type, DiagnosticChain diagnostics) {
		BasicDiagnostic unitedDiagnostic = new BasicDiagnostic();
		for (EClassifier requiredType : requiredTypes) {
			BasicDiagnostic diagnostic = new BasicDiagnostic();
			if (checkType(requiredType, type, diagnostic)) {
				diagnostics.addAll(diagnostic);
				return true;
			}
			unitedDiagnostic.addAll(diagnostic);
		}
		diagnostics.addAll(unitedDiagnostic);
		return false;
	}
	
	
	public static boolean checkType(EClassifier requiredType, EClassifier type, DiagnosticChain diagnostics) {
		if (requiredType != null && requiredType.equals(type)) {
			return true;
		}
		if (type instanceof VoidType) {
			//while all types conforms to OclVoid, we don't want to accept null's as a valid instances of Collections
			//in favor of empty collections
			if (false == requiredType instanceof CollectionType) {
				return true;
			}
			//collections are checked in separate clause below
		}
		
		if (requiredType instanceof EClass && type instanceof TupleType) {
			EClass eClass = (EClass) requiredType;
			EList<EStructuralFeature> tupleProperties = ((TupleType) type).oclProperties();
			EList<EStructuralFeature> eClassProperties = eClass.getEAllStructuralFeatures();
			for (Iterator<EStructuralFeature> i = eClassProperties.iterator(); i.hasNext(); ) {
				EStructuralFeature eFeature = i.next();
				boolean found = false;
				for (Iterator<EStructuralFeature> j = tupleProperties.iterator(); j.hasNext(); ) {
					EStructuralFeature eTupleFeature = j.next();
					if (eFeature.getName().equals(eTupleFeature.getName())) {
						found = true;
						EClassifier eTupleType = eTupleFeature.getEType();
						if (eFeature.isMany() && eTupleType instanceof CollectionType) {
							CollectionType requiredCollectionType = getTypedElementCollectionType(eFeature);
							if (!checkCollectionType(requiredCollectionType, (CollectionType) eTupleType, diagnostics)) {
								return false;
							}
							eTupleType = ((CollectionType) eTupleType).getElementType();
						}
						if (!checkType(eFeature.getEType(), eTupleType, diagnostics)) {
							return false;
						}
						break;
					}
				}
				if (!found && eFeature.isRequired()
						&& (eFeature.getDefaultValueLiteral() == null || eFeature.getDefaultValueLiteral().length() == 0)
						&& !(eFeature.getEType() instanceof EDataType)) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.WARNING, XOCLPlugin.PLUGIN_ID, 0,
							MessageFormat.format(Messages.OCLExpressionUtils_0, new Object[] {
									eFeature.getName(),
									requiredType.getName()
								}),
							null));
				}
			}
			for (Iterator<EStructuralFeature> j = tupleProperties.iterator(); j.hasNext(); ) {
				EStructuralFeature eTupleFeature = j.next();
				if (eClass.getEStructuralFeature(eTupleFeature.getName()) == null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.WARNING, XOCLPlugin.PLUGIN_ID, 0,
							MessageFormat.format(Messages.OCLExpressionUtils_1, new Object[] {
									eTupleFeature.getName(),
									eClass.getName()
							}),
						null));
				}
			}
		} else if (requiredType instanceof EClass && type instanceof EClass) {
			if (!((EClass) requiredType).isSuperTypeOf((EClass) type)) {
				diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
						MessageFormat.format(Messages.OCLExpressionUtils_2, new Object[] {
								type.getName(),
								requiredType.getName()
						}),
					null));
				return false;
			}
		} else if (requiredType instanceof CollectionType && type instanceof CollectionType) {
			return checkCollectionType((CollectionType) requiredType, (CollectionType) type, diagnostics)
				&& checkType(
					((CollectionType) requiredType).getElementType(),
					((CollectionType) type).getElementType(),
					diagnostics);
		} else if (!(requiredType instanceof CollectionType || requiredType instanceof EClass)
				&& requiredType instanceof EDataType && (type instanceof EDataType || type instanceof VoidType)) {
			if (requiredType instanceof EEnum) {
				if (!(type instanceof EEnum) || !requiredType.getName().equals(type.getName())) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
							MessageFormat.format(Messages.OCLExpressionUtils_3, new Object[] {
									type.getName(),
									requiredType.getName()
							}),
						null));
					return false;
				}
			} else {
				//ADDED BY PWK AS WORKAROUND
				try {
					if (!((EDataType) requiredType).getInstanceClass().isAssignableFrom(((EDataType) type).getInstanceClass())) {
						Environment<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> env = OCL.newInstance().getEnvironment();
						// TODO: The following standard type checking could be propagated to the outer scope
						if ((TypeUtil.getRelationship(env, requiredType, type) & UMLReflection.SUPERTYPE) == 0) {
							diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
									MessageFormat.format(Messages.OCLExpressionUtils_4, new Object[] {
											type.getName(),
											requiredType.getName()
									}),
									null));
							return false;
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
					MessageFormat.format(Messages.OCLExpressionUtils_5, new Object[] {
							requiredType.getName(),
							type.getName()
					}),
				null));
			return false;
		}
		return true;
	}

	public static boolean checkCollectionType(CollectionType requiredType, CollectionType type, DiagnosticChain diagnostics) {
		if (requiredType instanceof OrderedSetType) {
			if (type instanceof OrderedSetType) {
				return true;
			}
		} else if (requiredType instanceof SequenceType) {
			if (type instanceof SequenceType) {
				return true;
			}
		} else if (requiredType instanceof SetType) {
			if (type instanceof SetType) {
				return true;
			}
		} else if (requiredType instanceof BagType) {
			if (type instanceof BagType) {
				return true;
			}
		} else if (requiredType instanceof CollectionType) {
			return true;
		}
		
		diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
				MessageFormat.format(Messages.OCLExpressionUtils_6, new Object[] {
						requiredType.getName(),
						type.getName()
				}),
			null));
		return false;
	}
	
	public static CollectionType getTypedElementCollectionType(ETypedElement element) {
		
/*		
 * 		In Ecore we only support ordered=true and unique=true kind
 * 		of lists. If we enforce that typing rule on OCL, this forces
 * 		us to constantly write '->asOrderedSet()' at the end of any
 * 		expression. In order to prevent this, we relax the typing rule
 * 		to just collection, but we implicitly convert every kind of list
 * 		returned from OCL into an ordered set at the end of evaluation.
 * 
		if (element.isOrdered() && element.isUnique()) {
			return EcoreFactory.eINSTANCE.createOrderedSetType();
		} else if (element.isOrdered() && !element.isUnique()) {
			return EcoreFactory.eINSTANCE.createSequenceType();
		} else if (!element.isOrdered() && element.isUnique()) {
			return EcoreFactory.eINSTANCE.createSetType();
		} else if (!element.isOrdered() && !element.isUnique()) {
			return EcoreFactory.eINSTANCE.createBagType();
		}
*/
		return EcoreFactory.eINSTANCE.createCollectionType();
	}
	
	public static final SequenceType createSequenceType(EClassifier elementType) {
		SequenceType sequenceType = EcoreFactory.eINSTANCE.createSequenceType();
		sequenceType.setElementType(elementType);
		return sequenceType;
	}

	private static OCL.Query prepareExpression(String expression, XOCLElementContext context, ResourceSet resourceSet) throws ParserException {
		OCL ocl = createOCL(resourceSet, context.getVariables());
		OCL.Helper oclHelper = ocl.createOCLHelper();
		oclHelper.setContext(context.getContextClassifier());
		OCLExpression oclExpression = oclHelper.createQuery(expression);
		OCL.Query oclQuery = ocl.createQuery(oclExpression);
		return oclQuery;
	}
	
	private static OCL createOCL(ResourceSet resourceSet, Variable[] variables) {
		return configureOCL(OCL.newInstance(new XoclLibrary.XoclEnvironmentFactory(
				createPackageRegistry(EcoreEnvironmentFactory.INSTANCE.getEPackageRegistry(),
        				resourceSet)
        		)), variables);
	}

	public static OCL configureOCL(OCL ocl, Variable[] variables) {
		if (variables != null && variables.length > 0) {
			for (int i = 0; i < variables.length; ++i) {
				ocl.getEnvironment().addElement(variables[i].getName(), variables[i], true);
			}
		}
		ParsingOptions.setOption(ocl.getEnvironment(),
				ParsingOptions.implicitRootClass(ocl.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());					
		return ocl;
	}
	
	public static EPackage.Registry createPackageRegistry(EPackage.Registry delegateRegistry, ResourceSet resourceSet) {
		EPackageRegistryImpl registry = new DelegatingPackageRegistry(delegateRegistry);

		for (Iterator<Resource> i = resourceSet.getResources().iterator(); i.hasNext(); ) {
			for (Iterator<EObject> j = i.next().getContents().iterator(); j.hasNext(); ) {
				EObject eObject = j.next();
				if (eObject instanceof EPackage) {
					EPackage ePackage = (EPackage) eObject;
					registry.put(ePackage.getNsURI(), ePackage);
				}
			}
		}	
		return registry;
	}

}
