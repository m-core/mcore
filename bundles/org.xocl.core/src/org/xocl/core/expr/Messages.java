package org.xocl.core.expr;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.core.expr.messages"; //$NON-NLS-1$
	public static String OCLExpressionUtils_0;
	public static String OCLExpressionUtils_1;
	public static String OCLExpressionUtils_2;
	public static String OCLExpressionUtils_3;
	public static String OCLExpressionUtils_4;
	public static String OCLExpressionUtils_5;
	public static String OCLExpressionUtils_6;
	public static String XOCLElementContext_TypeNotSetError;
	public static String XOCLElementContext_UnknownAnnotationError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
