/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.edit.provider;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.xocl.core.expr.IOCLExpressionContextProvider;
import org.xocl.core.expr.OCLExpressionContext;

/**
 * @author Max Stepanov
 *
 */
public class ItemPropertyDescriptor extends org.eclipse.emf.edit.provider.ItemPropertyDescriptor
									implements IOCLExpressionContextProvider {

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature) {
		super(adapterFactory, displayName, description, feature);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description,
			EReference[] parentReferences) {
		super(adapterFactory, displayName, description, parentReferences);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable) {
		super(adapterFactory, displayName, description, feature, isSettable);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EReference[] parentReferences) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 * @param isSettable
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description,
			EReference[] parentReferences, boolean isSettable) {
		super(adapterFactory, displayName, description, parentReferences,
				isSettable);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param staticImage
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, Object staticImage) {
		super(adapterFactory, displayName, description, feature, isSettable,
				staticImage);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param category
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, String category) {
		super(adapterFactory, displayName, description, feature, isSettable,
				category);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 * @param isSettable
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EReference[] parentReferences,
			boolean isSettable) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences, isSettable);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 * @param isSettable
	 * @param category
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description,
			EReference[] parentReferences, boolean isSettable, String category) {
		super(adapterFactory, displayName, description, parentReferences,
				isSettable, category);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param staticImage
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			Object staticImage) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable, staticImage);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param category
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			String category) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable, category);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param staticImage
	 * @param category
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, Object staticImage, String category) {
		super(adapterFactory, displayName, description, feature, isSettable,
				staticImage, category);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, String category, String[] filterFlags) {
		super(adapterFactory, displayName, description, feature, isSettable,
				category, filterFlags);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 * @param isSettable
	 * @param category
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EReference[] parentReferences,
			boolean isSettable, String category) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences, isSettable, category);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 * @param isSettable
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description,
			EReference[] parentReferences, boolean isSettable, String category,
			String[] filterFlags) {
		super(adapterFactory, displayName, description, parentReferences,
				isSettable, category, filterFlags);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param staticImage
	 * @param category
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			Object staticImage, String category) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable, staticImage, category);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable, category, filterFlags);
	}

	/**
	 * @param adapterFactory
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param staticImage
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, Object staticImage, String category,
			String[] filterFlags) {
		super(adapterFactory, displayName, description, feature, isSettable,
				staticImage, category, filterFlags);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param parentReferences
	 * @param isSettable
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EReference[] parentReferences,
			boolean isSettable, String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences, isSettable, category, filterFlags);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param staticImage
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			Object staticImage, String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable, staticImage, category, filterFlags);
	}

	/**
	 * @param adapterFactory
	 * @param resourceLocator
	 * @param displayName
	 * @param description
	 * @param feature
	 * @param isSettable
	 * @param multiLine
	 * @param sortChoices
	 * @param staticImage
	 * @param category
	 * @param filterFlags
	 */
	public ItemPropertyDescriptor(AdapterFactory adapterFactory,
			ResourceLocator resourceLocator, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			boolean multiLine, boolean sortChoices, Object staticImage,
			String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description,
				feature, isSettable, multiLine, sortChoices, staticImage,
				category, filterFlags);
	}
	
	/**
	 * Wraps existing {@link ItemPropertyDescriptor}, this constructor should be used
	 * in generated NOT addXXXPropertyDescritorCode to wrap the descriptor created by default 
	 * generated code. 
	 * <p/>
	 * This will allows to avoid problems when generated NOT descriptor sticks to some category 
	 * while the genmodel is already changed to have a new one.
	 * <p/>
	 * See e.g, https://mtg.bugz.cloudforge.com/bugz/show_bug.cgi?id=743 
	 * 
	 * @param copy descriptor to get all the data from
	 */
	public ItemPropertyDescriptor(ItemPropertyDescriptor copy) {
		this(copy.adapterFactory, copy.resourceLocator, copy.displayName, //
				copy.description, copy.feature, copy.isSettable, //
				copy.multiLine, copy.sortChoices, copy.staticImage, //
				copy.category, copy.filterFlags);
	}

	/**
	 * Provides OCL expression context for property cell editor. Subclasses may override.
	 * @param object
	 * @return
	 */
	public OCLExpressionContext getOCLExpressionContext(Object object) {
		return null;
	}
}
