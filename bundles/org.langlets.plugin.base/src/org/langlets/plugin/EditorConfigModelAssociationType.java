/**
 */
package org.langlets.plugin;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Editor Config Model Association Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.plugin.EditorConfigModelAssociationType#getConfig <em>Config</em>}</li>
 *   <li>{@link org.langlets.plugin.EditorConfigModelAssociationType#getUri <em>Uri</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.plugin.PluginPackage#getEditorConfigModelAssociationType()
 * @model extendedMetaData="name='EditorConfigModelAssociationType' kind='empty'"
 * @generated
 */
public interface EditorConfigModelAssociationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Config</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Config</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Config</em>' attribute.
	 * @see #setConfig(String)
	 * @see org.langlets.plugin.PluginPackage#getEditorConfigModelAssociationType_Config()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='config' namespace='##targetNamespace'"
	 * @generated
	 */
	String getConfig();

	/**
	 * Sets the value of the '{@link org.langlets.plugin.EditorConfigModelAssociationType#getConfig <em>Config</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Config</em>' attribute.
	 * @see #getConfig()
	 * @generated
	 */
	void setConfig(String value);

	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see org.langlets.plugin.PluginPackage#getEditorConfigModelAssociationType_Uri()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='uri' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUri();

	/**
	 * Sets the value of the '{@link org.langlets.plugin.EditorConfigModelAssociationType#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	void setUri(String value);

} // EditorConfigModelAssociationType
