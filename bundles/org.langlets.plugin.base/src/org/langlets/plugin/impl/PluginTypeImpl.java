/**
 */
package org.langlets.plugin.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.langlets.plugin.ComponentType;
import org.langlets.plugin.EditorConfigModelAssociationType;
import org.langlets.plugin.ExtensionType;
import org.langlets.plugin.PluginFactory;
import org.langlets.plugin.PluginPackage;
import org.langlets.plugin.PluginType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.plugin.impl.PluginTypeImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link org.langlets.plugin.impl.PluginTypeImpl#getExtension <em>Extension</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PluginTypeImpl extends MinimalEObjectImpl.Container implements PluginType {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PluginTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PluginPackage.Literals.PLUGIN_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, PluginPackage.PLUGIN_TYPE__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtensionType> getExtension() {
		return getGroup().list(PluginPackage.Literals.PLUGIN_TYPE__EXTENSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtension(String point, ComponentType component) {
		ExtensionType extensionType = PluginFactory.eINSTANCE.createExtensionType();
		extensionType.setPoint(point);
		extensionType.getComponent().add(component);
		getExtension().add(extensionType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtension(String point, EditorConfigModelAssociationType editorConfig) {
		ExtensionType extensionType = PluginFactory.eINSTANCE.createExtensionType();
		extensionType.setPoint(point);
		extensionType.getEditorConfigModelAssociation().add(editorConfig);
		getExtension().add(extensionType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ExtensionType> findExtensions(String point) {
		EList<ExtensionType> found = new BasicEList<ExtensionType>();
		Iterator<ExtensionType> it = getExtension().iterator();
		while(it.hasNext()) {
			ExtensionType current = it.next();
			if (current.getPoint().equals(point)) {
				found.add(current);
			}
		}
		return found;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EditorConfigModelAssociationType findEditorConfigAssociation(String uri) {
		EList<ExtensionType> extensions = findExtensions("org.xocl.editorconfig.editorConfigAssociations");
		EditorConfigModelAssociationType found = null;
		Iterator<ExtensionType> it = extensions.iterator();
		while(it.hasNext() && found == null) {
			ExtensionType current = it.next();
			Iterator<EditorConfigModelAssociationType> assocs = current.getEditorConfigModelAssociation().iterator();
			while(assocs.hasNext() && found == null) {
				EditorConfigModelAssociationType assoc = assocs.next();
				if (assoc.getUri().equals(uri)) {
					found = assoc;
				}
			}
		}
		return found;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PluginPackage.PLUGIN_TYPE__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case PluginPackage.PLUGIN_TYPE__EXTENSION:
				return ((InternalEList<?>)getExtension()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PluginPackage.PLUGIN_TYPE__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case PluginPackage.PLUGIN_TYPE__EXTENSION:
				return getExtension();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PluginPackage.PLUGIN_TYPE__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case PluginPackage.PLUGIN_TYPE__EXTENSION:
				getExtension().clear();
				getExtension().addAll((Collection<? extends ExtensionType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PluginPackage.PLUGIN_TYPE__GROUP:
				getGroup().clear();
				return;
			case PluginPackage.PLUGIN_TYPE__EXTENSION:
				getExtension().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PluginPackage.PLUGIN_TYPE__GROUP:
				return group != null && !group.isEmpty();
			case PluginPackage.PLUGIN_TYPE__EXTENSION:
				return !getExtension().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case PluginPackage.PLUGIN_TYPE___ADD_EXTENSION__STRING_COMPONENTTYPE:
				addExtension((String)arguments.get(0), (ComponentType)arguments.get(1));
				return null;
			case PluginPackage.PLUGIN_TYPE___ADD_EXTENSION__STRING_EDITORCONFIGMODELASSOCIATIONTYPE:
				addExtension((String)arguments.get(0), (EditorConfigModelAssociationType)arguments.get(1));
				return null;
			case PluginPackage.PLUGIN_TYPE___FIND_EXTENSIONS__STRING:
				return findExtensions((String)arguments.get(0));
			case PluginPackage.PLUGIN_TYPE___FIND_EDITOR_CONFIG_ASSOCIATION__STRING:
				return findEditorConfigAssociation((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //PluginTypeImpl
