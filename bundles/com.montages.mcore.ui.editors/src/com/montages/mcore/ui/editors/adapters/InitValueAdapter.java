package com.montages.mcore.ui.editors.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.delegates.XOCLAnnotationAdapter;
import org.xocl.delegates.eval.XOCLSettingEvaluator;

public class InitValueAdapter extends EContentAdapter {

	private final EditingDomain editingDomain;

	public InitValueAdapter(EditingDomain editingDomain) {
		this.editingDomain = editingDomain;
	}

	@Override
	public void notifyChanged(Notification notification) {
		super.notifyChanged(notification);
		int type = notification.getEventType();

		if (type == Notification.ADD || type == Notification.SET) {
			Object notifier = notification.getNotifier();
			Object value = notification.getNewValue();

			if (notifier instanceof EObject && value instanceof EObject) {
				EObject target = (EObject) value;

				Map<EStructuralFeature, String> initFeatures = new HashMap<EStructuralFeature, String>();
				for (EStructuralFeature feature: target.eClass().getEAllStructuralFeatures()) {
					// only execute init if no values have been set 
					// for the target object
					if (!((EObject) target).eIsSet(feature)) {
						EAnnotation annotation = feature.getEAnnotation(XOCLAnnotationAdapter.MODIFIED);
						if (annotation != null && annotation.getDetails().containsKey("initValue")) {
							String expression = annotation.getDetails().get("initValue");
							initFeatures.put(feature, expression);
						}
					}
				}

				List<Command> commands = new ArrayList<Command>();
				for (EStructuralFeature feature: initFeatures.keySet()) {
					Object result = new XOCLSettingEvaluator(feature).eval((InternalEObject) target, initFeatures.get(feature));
					if (result != null) {
						if (feature.isMany()) {
							commands.add(AddCommand.create(editingDomain, target, feature, (Collection<?>) result));
						} else {
							commands.add(SetCommand.create(editingDomain, target, feature, result));
						}
					}
				}

				for (Command command: commands) {
					editingDomain.getCommandStack().execute(command);
				}
			}
		}
	}

}
