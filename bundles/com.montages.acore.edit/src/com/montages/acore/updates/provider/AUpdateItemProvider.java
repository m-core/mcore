/**
 */
package com.montages.acore.updates.provider;

import com.montages.acore.provider.AcoreEditPlugin;

import com.montages.acore.updates.AUpdate;
import com.montages.acore.updates.AUpdateMode;
import com.montages.acore.updates.UpdatesPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.updates.AUpdate} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AUpdateItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AUpdateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAObjectPropertyDescriptor(object);
			addAUpdateModePropertyDescriptor(object);
			addAValuePropertyDescriptor(object);
			addADummy2PropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AObject feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AObject feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AUpdate_aObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AUpdate_aObject_feature",
								"_UI_AUpdate_type"),
						UpdatesPackage.Literals.AUPDATE__AOBJECT, false, false, false, null,
						getString("_UI_zACorePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AUpdate Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUpdateModePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUpdate Mode feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AUpdate_aUpdateMode_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AUpdate_aUpdateMode_feature",
								"_UI_AUpdate_type"),
						UpdatesPackage.Literals.AUPDATE__AUPDATE_MODE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AValue feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AValue feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AUpdate_aValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AUpdate_aValue_feature",
								"_UI_AUpdate_type"),
						UpdatesPackage.Literals.AUPDATE__AVALUE, false, false, false, null,
						getString("_UI_zACorePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ADummy2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addADummy2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ADummy2 feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AUpdate_aDummy2_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AUpdate_aDummy2_feature",
								"_UI_AUpdate_type"),
						UpdatesPackage.Literals.AUPDATE__ADUMMY2, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		AUpdateMode labelValue = ((AUpdate) object).getAUpdateMode();
		String label = labelValue == null ? null : labelValue.toString();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AUpdate.class)) {
		case UpdatesPackage.AUPDATE__AUPDATE_MODE:
		case UpdatesPackage.AUPDATE__ADUMMY2:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

}
