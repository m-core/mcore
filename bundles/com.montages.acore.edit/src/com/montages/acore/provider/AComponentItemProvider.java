/**
 */
package com.montages.acore.provider;

import com.montages.acore.AComponent;
import com.montages.acore.AcorePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.AComponent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AComponentItemProvider extends AAbstractFolderItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addAComponentIdPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABaseUriPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addADefaultUriPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedIdConstantPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUsedPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAMainPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAMainResourcePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AComponent Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAComponentIdPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AComponent Id feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AComponent_aComponentId_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aComponentId_feature",
								"_UI_AComponent_type"),
						AcorePackage.Literals.ACOMPONENT__ACOMPONENT_ID, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABase Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABaseUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABase Uri feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AComponent_aBaseUri_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aBaseUri_feature",
								"_UI_AComponent_type"),
						AcorePackage.Literals.ACOMPONENT__ABASE_URI, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ADefault Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addADefaultUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ADefault Uri feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AComponent_aDefaultUri_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aDefaultUri_feature",
								"_UI_AComponent_type"),
						AcorePackage.Literals.ACOMPONENT__ADEFAULT_URI, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Id Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedIdConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Id Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AComponent_aUndefinedIdConstant_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aUndefinedIdConstant_feature",
						"_UI_AComponent_type"),
				AcorePackage.Literals.ACOMPONENT__AUNDEFINED_ID_CONSTANT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUsed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUsedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUsed feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AComponent_aUsed_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aUsed_feature",
								"_UI_AComponent_type"),
						AcorePackage.Literals.ACOMPONENT__AUSED, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AMain Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAMainPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AMain Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AComponent_aMainPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aMainPackage_feature",
								"_UI_AComponent_type"),
						AcorePackage.Literals.ACOMPONENT__AMAIN_PACKAGE, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AMain Resource feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAMainResourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AMain Resource feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AComponent_aMainResource_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AComponent_aMainResource_feature",
								"_UI_AComponent_type"),
						AcorePackage.Literals.ACOMPONENT__AMAIN_RESOURCE, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AComponent) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AComponent.class)) {
		case AcorePackage.ACOMPONENT__ACOMPONENT_ID:
		case AcorePackage.ACOMPONENT__ABASE_URI:
		case AcorePackage.ACOMPONENT__ADEFAULT_URI:
		case AcorePackage.ACOMPONENT__AUNDEFINED_ID_CONSTANT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AcoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
