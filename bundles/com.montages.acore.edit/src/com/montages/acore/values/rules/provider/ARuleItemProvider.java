/**
 */
package com.montages.acore.values.rules.provider;

import com.montages.acore.provider.AcoreEditPlugin;

import com.montages.acore.values.rules.ARule;
import com.montages.acore.values.rules.RulesPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.values.rules.ARule} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ARuleItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ARuleItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addASelfContextPropertyDescriptor(object);
			addATrgContextPropertyDescriptor(object);
			addATrgContextSingularPropertyDescriptor(object);
			addASrcContextPropertyDescriptor(object);
			addASrcContextSingularPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ASelf Context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASelfContextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASelf Context feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ARule_aSelfContext_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ARule_aSelfContext_feature",
								"_UI_ARule_type"),
						RulesPackage.Literals.ARULE__ASELF_CONTEXT, false, false, false, null,
						getString("_UI_zACoreRulesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ATrg Context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATrgContextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ATrg Context feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ARule_aTrgContext_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ARule_aTrgContext_feature",
								"_UI_ARule_type"),
						RulesPackage.Literals.ARULE__ATRG_CONTEXT, false, false, false, null,
						getString("_UI_zACoreRulesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ATrg Context Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATrgContextSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ATrg Context Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ARule_aTrgContextSingular_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ARule_aTrgContextSingular_feature",
						"_UI_ARule_type"),
				RulesPackage.Literals.ARULE__ATRG_CONTEXT_SINGULAR, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreRulesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ASrc Context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASrcContextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASrc Context feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ARule_aSrcContext_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ARule_aSrcContext_feature",
								"_UI_ARule_type"),
						RulesPackage.Literals.ARULE__ASRC_CONTEXT, true, false, true, null,
						getString("_UI_zACoreRulesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ASrc Context Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASrcContextSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASrc Context Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ARule_aSrcContextSingular_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ARule_aSrcContextSingular_feature",
						"_UI_ARule_type"),
				RulesPackage.Literals.ARULE__ASRC_CONTEXT_SINGULAR, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreRulesPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		Boolean labelValue = ((ARule) object).getATrgContextSingular();
		String label = labelValue == null ? null : labelValue.toString();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ARule.class)) {
		case RulesPackage.ARULE__ATRG_CONTEXT_SINGULAR:
		case RulesPackage.ARULE__ASRC_CONTEXT_SINGULAR:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

}
