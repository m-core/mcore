/**
 */
package com.montages.acore.abstractions.provider;

import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.provider.AcoreEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.abstractions.AElement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AElementItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AElementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addALabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAKindBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addARenderedKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingComponentPropertyDescriptor(object);
			}
			addATPackageUriPropertyDescriptor(object);
			addATClassifierNamePropertyDescriptor(object);
			addATFeatureNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addATPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATCoreAStringClassPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ALabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addALabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALabel feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aLabel_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ALABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AKind Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAKindBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKind Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aKindBase_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aKindBase_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AKIND_BASE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ARendered Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addARenderedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARendered Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aRenderedKind_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aRenderedKind_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Component feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Component feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aContainingComponent_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aContainingComponent_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Package Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATPackageUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Package Uri feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aTPackageUri_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTPackageUri_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE_URI, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AT Classifier Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATClassifierNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Classifier Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTClassifierName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTClassifierName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AT Feature Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATFeatureNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Feature Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTFeatureName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTFeatureName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_FEATURE_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AT Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTPackage_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Classifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTClassifier_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Feature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTFeature_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_FEATURE, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Core AString Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATCoreAStringClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Core AString Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTCoreAStringClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTCoreAStringClass_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CORE_ASTRING_CLASS, false, false, false, null,
						getString("_UI_zACorePackagePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AElement) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AElement.class)) {
		case AbstractionsPackage.AELEMENT__ALABEL:
		case AbstractionsPackage.AELEMENT__AKIND_BASE:
		case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
		case AbstractionsPackage.AELEMENT__AT_PACKAGE_URI:
		case AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME:
		case AbstractionsPackage.AELEMENT__AT_FEATURE_NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AbstractionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
