package com.montages.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.Diagram;

public class McoreUtil {

	public static URI getMcoreURIFromDiagram(URI diagramFileURI) {
		if (diagramFileURI == null) {
			return null;
		}
		ResourceSetImpl resourceSet = new ResourceSetImpl();
		Resource diagramRes = resourceSet.getResource(diagramFileURI, true);
		if (diagramRes == null || diagramRes.getContents().isEmpty()) {
			return null;
		}
		if (diagramRes.getContents().isEmpty()) {
			return null;
		}
		Diagram diagram = (Diagram) diagramRes.getContents().get(0);
		EObject diagramSemantic = diagram.getElement();
		resourceSet.getURIConverter().getURIMap().putAll(CommonPlugin.computeURIMap());
		URI mcoreURI = EcoreUtil.getURI(diagramSemantic).trimFragment();
		return resourceSet.getURIConverter().normalize(mcoreURI);
	}

	public static <K, T extends K> void unsafeListsMerge(List<K> baseList, List<T> newList) {
		// remove deleted folders
		ArrayList<K> resourceFolders = new ArrayList<K>(baseList);
		resourceFolders.removeAll(newList);
		baseList.removeAll(resourceFolders);

		// add new folders
		newList.removeAll(baseList);
		baseList.addAll(newList);
	}

	public static <T> Stream<T> stream(Iterator<T> it) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(it, Spliterator.ORDERED), true);
	}
}
