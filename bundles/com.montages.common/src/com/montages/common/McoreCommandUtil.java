package com.montages.common;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

public class McoreCommandUtil {

	public static void executeUnsafeCommand(Command cmd, TransactionalEditingDomain domain) {
		if (domain != null) {
			domain.getCommandStack().execute(cmd);
		} else {
			cmd.execute();
		}
	}

}
