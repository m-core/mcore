package com.montages.common.resource;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;

public class McoreResourceProxyVisitor implements IResourceProxyVisitor {

	private static final String MODEL_FOLDER = "model";

	private static final String MODEL_FILE_EXTENSION = "mcore";

	private final List<IResource> myModels;

	public McoreResourceProxyVisitor() {
		myModels = new ArrayList<IResource>();
	}

	@Override
	public boolean visit(IResourceProxy proxy) throws CoreException {
		IResource resource = proxy.requestResource();
		if (resource.getType() == IResource.PROJECT) {
			String resName = resource.getName();
			if (resName.endsWith(".base") || resName.endsWith(".edit") || resName.endsWith(".editor")) {
				return false;
			} else {
				return true;
			}
		}
		if (isModelFile(resource)) {
			myModels.add(resource);
		}
		return true;
	}

	public List<IResource> getModels() {
		return myModels;
	}

	protected boolean isModelFile(IResource resource) {
		boolean isModelFile = resource.getType() == IResource.FILE && resource.getParent().getName().equals(MODEL_FOLDER);
		return isModelFile && MODEL_FILE_EXTENSION.equals(resource.getFileExtension());
	}
}