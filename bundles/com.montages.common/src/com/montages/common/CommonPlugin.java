package com.montages.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.osgi.framework.BundleContext;

import com.montages.common.resource.EcoreResourceResolver;
import com.montages.common.resource.McoreResourceResolver;

public class CommonPlugin extends Plugin {

	private static CommonPlugin INSTANCE;
	private static BundleContext context;

	@Override
	public void start(BundleContext ctx) throws Exception {
		super.start(ctx);
		INSTANCE = this;
		context = ctx;
	}

	@Override
	public void stop(BundleContext ctx) throws Exception {
		INSTANCE = null;
		context = null;
		super.stop(ctx);
	}

	public static CommonPlugin getInstance() {
		return INSTANCE;
	}

	public static Map<URI, URI> computeURIMap() {
		final Map<URI, URI> result = new HashMap<URI, URI>();
		computePluginModels(result);
		computeWorkspaceModels(result);
		result.putAll(EcoreResourceResolver.computeWorkspaceResourceMap());
		result.putAll(EcorePlugin.computePlatformURIMap(false));

		return result;
	}

	public static Set<URI> computePluginModels(Map<URI, URI> uriMap) {
		return McoreResourceResolver.computePluginModels(context, uriMap);
	}
	
	public static Set<URI> computeWorkspaceModels(Map<URI, URI> uriMap) {
		if (uriMap == null) {
			uriMap = new HashMap<URI, URI>();
		}
		return McoreResourceResolver.computeWorkspaceModels(uriMap);
	}

}
