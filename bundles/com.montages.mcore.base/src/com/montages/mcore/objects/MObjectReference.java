/**
 */
package com.montages.mcore.objects;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MObjectReference#getReferencedObject <em>Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObjectReference#getTypeOfReferencedObject <em>Type Of Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObjectReference#getIdOfReferencedObject <em>Id Of Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObjectReference#getChoosableReference <em>Choosable Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMObjectReference()
 * @model annotation="http://www.xocl.org/OCL label='let o:MObject=self.referencedObject in\r\nif o.oclIsUndefined() then \'OBJECT REFERENCE MISSING\' \r\nelse o.id.concat(\r\nif o.type.oclIsUndefined() then \': TYPE MISSING\' \r\nelse \': \'.concat(o.type.name) endif\r\n)\r\nendif'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Object Reference\'\n' correctAndDefinedValueDerive='let var1: Boolean = let e1: Boolean = if (let chain11: mcore::objects::MObject = referencedObject in\nif chain11 <> null then true else false \n  endif)= false \n then false \n else if (correctlyTypedReferencedObject())= false \n then false \n else if (correctMultiplicity())= false \n then false \nelse if ((let chain11: mcore::objects::MObject = referencedObject in\nif chain11 <> null then true else false \n  endif)= null or (correctlyTypedReferencedObject())= null or (correctMultiplicity())= null) = true \n then null \n else true endif endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n'"
 * @generated
 */

public interface MObjectReference extends MValue {
	/**
	 * Returns the value of the '<em><b>Referenced Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Object</em>' reference.
	 * @see #isSetReferencedObject()
	 * @see #unsetReferencedObject()
	 * @see #setReferencedObject(MObject)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObjectReference_ReferencedObject()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='let p:MProperty=self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() \r\n  then true \r\n  else if p.type.oclIsUndefined() \r\n     then if p.simpleType = SimpleType::Object \r\n        then true \r\n        else false endif\r\n     else if p.type.simpleType = SimpleType::Object\r\n       then true\r\n       else (trg.type = p.type) \r\n           or p.type.allSubTypes()->includes(trg.type)\r\nendif endif endif'"
	 * @generated
	 */
	MObject getReferencedObject();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObjectReference#getReferencedObject <em>Referenced Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Object</em>' reference.
	 * @see #isSetReferencedObject()
	 * @see #unsetReferencedObject()
	 * @see #getReferencedObject()
	 * @generated
	 */
	void setReferencedObject(MObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObjectReference#getReferencedObject <em>Referenced Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetReferencedObject()
	 * @see #getReferencedObject()
	 * @see #setReferencedObject(MObject)
	 * @generated
	 */
	void unsetReferencedObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObjectReference#getReferencedObject <em>Referenced Object</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Referenced Object</em>' reference is set.
	 * @see #unsetReferencedObject()
	 * @see #getReferencedObject()
	 * @see #setReferencedObject(MObject)
	 * @generated
	 */
	boolean isSetReferencedObject();

	/**
	 * Returns the value of the '<em><b>Type Of Referenced Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Of Referenced Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Of Referenced Object</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObjectReference_TypeOfReferencedObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.referencedObject.oclIsUndefined() then null else\r\nself.referencedObject.type endif'"
	 * @generated
	 */
	MClassifier getTypeOfReferencedObject();

	/**
	 * Returns the value of the '<em><b>Id Of Referenced Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Of Referenced Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Of Referenced Object</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObjectReference_IdOfReferencedObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.referencedObject.oclIsUndefined() then \'OBJECT REFERENCE MISSING\' else\r\nself.referencedObject.id endif'"
	 * @generated
	 */
	String getIdOfReferencedObject();

	/**
	 * Returns the value of the '<em><b>Choosable Reference</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choosable Reference</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choosable Reference</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObjectReference_ChoosableReference()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let o:MObject = self.referencedObject in\r\nlet source:MObject = self.containingObject in\r\nif o.oclIsUndefined() \r\n  or source.type.oclIsUndefined()\r\n    then OrderedSet{}\r\n    else if o.type.oclIsUndefined()\r\n      or source.type.oclIsUndefined()\r\n     then OrderedSet{} \r\n     else source.type.allFeaturesWithStorage()\r\n           ->select(p:MProperty|\r\n                 (p.containment = false) and\r\n                 ( if p.type.oclIsUndefined() \r\n                    then false \r\n                    else ( p.type = o.type) \r\n                       or p.type.allSubTypes()->includes(o.type) endif)\r\n                )\r\nendif endif\r\n'"
	 * @generated
	 */
	EList<MProperty> getChoosableReference();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let pi:MPropertyInstance= self.containingPropertyInstance in\r\nlet p:MProperty = pi.property in\r\nif p.oclIsUndefined() then \r\n   pi.internalLiteralValue->isEmpty()\r\n    and pi.internalDataValue->isEmpty()\r\n    and pi.internalContainedObject->isEmpty()\r\nelse\r\nlet o:MObject = self.referencedObject in\r\nif p.oclIsUndefined() or o.oclIsUndefined() then false else\r\nif p.kind = mcore::PropertyKind::Reference\r\nthen p.type = o.type or p.type.allSubTypes()->includes(o.type)\r\nelse false endif\r\nendif endif'"
	 * @generated
	 */
	Boolean correctlyTypedReferencedObject();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let p:MProperty = self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then true else\r\nlet pos:Integer = self.containingPropertyInstance.internalReferencedObject->indexOf(self) in \r\nif p.singular then pos=1 else true endif endif'"
	 * @generated
	 */
	Boolean correctMultiplicity();

} // MObjectReference
