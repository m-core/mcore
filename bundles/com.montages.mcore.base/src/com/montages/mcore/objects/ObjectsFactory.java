/**
 */
package com.montages.mcore.objects;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.objects.ObjectsPackage
 * @generated
 */
public interface ObjectsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ObjectsFactory eINSTANCE = com.montages.mcore.objects.impl.ObjectsFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>MResource Folder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MResource Folder</em>'.
	 * @generated
	 */
	MResourceFolder createMResourceFolder();

	/**
	 * Returns a new object of class '<em>MResource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MResource</em>'.
	 * @generated
	 */
	MResource createMResource();

	/**
	 * Returns a new object of class '<em>MObject</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MObject</em>'.
	 * @generated
	 */
	MObject createMObject();

	/**
	 * Returns a new object of class '<em>MProperty Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MProperty Instance</em>'.
	 * @generated
	 */
	MPropertyInstance createMPropertyInstance();

	/**
	 * Returns a new object of class '<em>MObject Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MObject Reference</em>'.
	 * @generated
	 */
	MObjectReference createMObjectReference();

	/**
	 * Returns a new object of class '<em>MLiteral Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLiteral Value</em>'.
	 * @generated
	 */
	MLiteralValue createMLiteralValue();

	/**
	 * Returns a new object of class '<em>MData Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MData Value</em>'.
	 * @generated
	 */
	MDataValue createMDataValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ObjectsPackage getObjectsPackage();

} //ObjectsFactory
