/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MObject Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectReferenceImpl#getReferencedObject <em>Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectReferenceImpl#getTypeOfReferencedObject <em>Type Of Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectReferenceImpl#getIdOfReferencedObject <em>Id Of Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectReferenceImpl#getChoosableReference <em>Choosable Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MObjectReferenceImpl extends MValueImpl
		implements MObjectReference {
	/**
	 * The cached value of the '{@link #getReferencedObject() <em>Referenced Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedObject()
	 * @generated
	 * @ordered
	 */
	protected MObject referencedObject;

	/**
	 * This is true if the Referenced Object reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean referencedObjectESet;

	/**
	 * The default value of the '{@link #getIdOfReferencedObject() <em>Id Of Referenced Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdOfReferencedObject()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_OF_REFERENCED_OBJECT_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctlyTypedReferencedObject <em>Correctly Typed Referenced Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctlyTypedReferencedObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctlyTypedReferencedObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctMultiplicity <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctMultiplicity
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctMultiplicityBodyOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getReferencedObject <em>Referenced Object</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedObject
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression referencedObjectChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTypeOfReferencedObject <em>Type Of Referenced Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOfReferencedObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression typeOfReferencedObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIdOfReferencedObject <em>Id Of Referenced Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdOfReferencedObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression idOfReferencedObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChoosableReference <em>Choosable Reference</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoosableReference
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression choosableReferenceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectAndDefinedValue <em>Correct And Defined Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectAndDefinedValue
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctAndDefinedValueDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MOBJECT_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getReferencedObject() {
		if (referencedObject != null && referencedObject.eIsProxy()) {
			InternalEObject oldReferencedObject = (InternalEObject) referencedObject;
			referencedObject = (MObject) eResolveProxy(oldReferencedObject);
			if (referencedObject != oldReferencedObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT,
							oldReferencedObject, referencedObject));
			}
		}
		return referencedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetReferencedObject() {
		return referencedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedObject(MObject newReferencedObject) {
		MObject oldReferencedObject = referencedObject;
		referencedObject = newReferencedObject;
		boolean oldReferencedObjectESet = referencedObjectESet;
		referencedObjectESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT,
					oldReferencedObject, referencedObject,
					!oldReferencedObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReferencedObject() {
		MObject oldReferencedObject = referencedObject;
		boolean oldReferencedObjectESet = referencedObjectESet;
		referencedObject = null;
		referencedObjectESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT,
					oldReferencedObject, null, oldReferencedObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReferencedObject() {
		return referencedObjectESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Referenced Object</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let p:MProperty=self.containingPropertyInstance.property in
	if p.oclIsUndefined() 
	then true 
	else if p.type.oclIsUndefined() 
	 then if p.simpleType = SimpleType::Object 
	    then true 
	    else false endif
	 else if p.type.simpleType = SimpleType::Object
	   then true
	   else (trg.type = p.type) 
	       or p.type.allSubTypes()->includes(trg.type)
	endif endif endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalReferencedObjectChoiceConstraint(MObject trg) {
		EClass eClass = ObjectsPackage.Literals.MOBJECT_REFERENCE;
		if (referencedObjectChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ObjectsPackage.Literals.MOBJECT_REFERENCE__REFERENCED_OBJECT;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				referencedObjectChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"ReferencedObjectChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(referencedObjectChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "ReferencedObjectChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getTypeOfReferencedObject() {
		MClassifier typeOfReferencedObject = basicGetTypeOfReferencedObject();
		return typeOfReferencedObject != null
				&& typeOfReferencedObject.eIsProxy()
						? (MClassifier) eResolveProxy(
								(InternalEObject) typeOfReferencedObject)
						: typeOfReferencedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetTypeOfReferencedObject() {
		/**
		 * @OCL if self.referencedObject.oclIsUndefined() then null else
		self.referencedObject.type endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT_REFERENCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT;

		if (typeOfReferencedObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				typeOfReferencedObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT_REFERENCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(typeOfReferencedObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT_REFERENCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIdOfReferencedObject() {
		/**
		 * @OCL if self.referencedObject.oclIsUndefined() then 'OBJECT REFERENCE MISSING' else
		self.referencedObject.id endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT_REFERENCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT;

		if (idOfReferencedObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				idOfReferencedObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT_REFERENCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(idOfReferencedObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT_REFERENCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getChoosableReference() {
		/**
		 * @OCL let o:MObject = self.referencedObject in
		let source:MObject = self.containingObject in
		if o.oclIsUndefined() 
		or source.type.oclIsUndefined()
		then OrderedSet{}
		else if o.type.oclIsUndefined()
		or source.type.oclIsUndefined()
		then OrderedSet{} 
		else source.type.allFeaturesWithStorage()
		->select(p:MProperty|
		     (p.containment = false) and
		     ( if p.type.oclIsUndefined() 
		        then false 
		        else ( p.type = o.type) 
		           or p.type.allSubTypes()->includes(o.type) endif)
		    )
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT_REFERENCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT_REFERENCE__CHOOSABLE_REFERENCE;

		if (choosableReferenceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				choosableReferenceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT_REFERENCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(choosableReferenceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT_REFERENCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MProperty> result = (EList<MProperty>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctlyTypedReferencedObject() {

		/**
		 * @OCL let pi:MPropertyInstance= self.containingPropertyInstance in
		let p:MProperty = pi.property in
		if p.oclIsUndefined() then 
		pi.internalLiteralValue->isEmpty()
		and pi.internalDataValue->isEmpty()
		and pi.internalContainedObject->isEmpty()
		else
		let o:MObject = self.referencedObject in
		if p.oclIsUndefined() or o.oclIsUndefined() then false else
		if p.kind = mcore::PropertyKind::Reference
		then p.type = o.type or p.type.allSubTypes()->includes(o.type)
		else false endif
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT_REFERENCE);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT_REFERENCE
				.getEOperations().get(0);
		if (correctlyTypedReferencedObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctlyTypedReferencedObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT_REFERENCE, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(correctlyTypedReferencedObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT_REFERENCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctMultiplicity() {

		/**
		 * @OCL let p:MProperty = self.containingPropertyInstance.property in
		if p.oclIsUndefined() then true else
		let pos:Integer = self.containingPropertyInstance.internalReferencedObject->indexOf(self) in 
		if p.singular then pos=1 else true endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT_REFERENCE);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT_REFERENCE
				.getEOperations().get(1);
		if (correctMultiplicityBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctMultiplicityBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT_REFERENCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctMultiplicityBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT_REFERENCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT:
			if (resolve)
				return getReferencedObject();
			return basicGetReferencedObject();
		case ObjectsPackage.MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT:
			if (resolve)
				return getTypeOfReferencedObject();
			return basicGetTypeOfReferencedObject();
		case ObjectsPackage.MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT:
			return getIdOfReferencedObject();
		case ObjectsPackage.MOBJECT_REFERENCE__CHOOSABLE_REFERENCE:
			return getChoosableReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT:
			setReferencedObject((MObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT:
			unsetReferencedObject();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT_REFERENCE__REFERENCED_OBJECT:
			return isSetReferencedObject();
		case ObjectsPackage.MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT:
			return basicGetTypeOfReferencedObject() != null;
		case ObjectsPackage.MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT:
			return ID_OF_REFERENCED_OBJECT_EDEFAULT == null
					? getIdOfReferencedObject() != null
					: !ID_OF_REFERENCED_OBJECT_EDEFAULT
							.equals(getIdOfReferencedObject());
		case ObjectsPackage.MOBJECT_REFERENCE__CHOOSABLE_REFERENCE:
			return !getChoosableReference().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MOBJECT_REFERENCE___CORRECTLY_TYPED_REFERENCED_OBJECT:
			return correctlyTypedReferencedObject();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let o:MObject=self.referencedObject in
	if o.oclIsUndefined() then 'OBJECT REFERENCE MISSING' 
	else o.id.concat(
	if o.type.oclIsUndefined() then ': TYPE MISSING' 
	else ': '.concat(o.type.name) endif
	)
	endif
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ObjectsPackage.Literals.MOBJECT_REFERENCE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Object Reference'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ObjectsPackage.Literals.MOBJECT_REFERENCE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL correctAndDefinedValue let var1: Boolean = let e1: Boolean = if (let chain11: mcore::objects::MObject = referencedObject in
	if chain11 <> null then true else false 
	endif)= false 
	then false 
	else if (correctlyTypedReferencedObject())= false 
	then false 
	else if (correctMultiplicity())= false 
	then false 
	else if ((let chain11: mcore::objects::MObject = referencedObject in
	if chain11 <> null then true else false 
	endif)= null or (correctlyTypedReferencedObject())= null or (correctMultiplicity())= null) = true 
	then null 
	else true endif endif endif endif in 
	if e1.oclIsInvalid() then null else e1 endif in
	var1
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectAndDefinedValue() {
		EClass eClass = (ObjectsPackage.Literals.MOBJECT_REFERENCE);
		EStructuralFeature eOverrideFeature = ObjectsPackage.Literals.MVALUE__CORRECT_AND_DEFINED_VALUE;

		if (correctAndDefinedValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctAndDefinedValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctAndDefinedValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MObjectReferenceImpl
