/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.XUpdate;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.impl.MNamedImpl;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceAction;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>MResource</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceImpl#getObject <em>Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceImpl#getRootObjectPackage <em>Root Object Package</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceImpl#getContainingFolder <em>Containing Folder</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MResourceImpl extends MNamedImpl
		implements MResource, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected EList<MObject> object;

	/**
	 * The cached value of the '{@link #getRootObjectPackage() <em>Root Object Package</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRootObjectPackage()
	 * @generated
	 * @ordered
	 */
	protected MPackage rootObjectPackage;

	/**
	 * This is true if the Root Object Package reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean rootObjectPackageESet;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * This is true if the Id attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean idESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MResourceAction DO_ACTION_EDEFAULT = MResourceAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateobjectsMResourceActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #update
	 * <em>Update</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updatemcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #updateObjects <em>Update Objects</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #updateObjects
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updateObjectsmcoreMPropertymcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #updateContainmentToStoredFeature <em>Update Containment To Stored Feature</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #updateContainmentToStoredFeature
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updateContainmentToStoredFeaturemcoreMPropertyobjectsMObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #updateStoredToContainmentFeature <em>Update Stored To Containment Feature</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #updateStoredToContainmentFeature
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updateStoredToContainmentFeaturemcoreMPropertyobjectsMObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #addObjects <em>Add Objects</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #addObjects
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addObjectsmcoreMPropertyobjectsMObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getContainingFolder <em>Containing Folder</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getContainingFolder
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingFolderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of
	 * '{@link #getDoAction <em>Do Action</em>}' property. Is combined with the
	 * choice constraint definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel
	 * <em>Kind Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MRESOURCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getObject() {
		if (object == null) {
			object = new EObjectContainmentEList.Unsettable.Resolving<MObject>(
					MObject.class, this, ObjectsPackage.MRESOURCE__OBJECT);
		}
		return object;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObject() {
		if (object != null)
			((InternalEList.Unsettable<?>) object).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObject() {
		return object != null && ((InternalEList.Unsettable<?>) object).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		boolean oldIdESet = idESet;
		idESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MRESOURCE__ID, oldId, id, !oldIdESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetId() {
		String oldId = id;
		boolean oldIdESet = idESet;
		id = ID_EDEFAULT;
		idESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MRESOURCE__ID, oldId, ID_EDEFAULT,
					oldIdESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetId() {
		return idESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceAction getDoAction() {
		/**
		 * @OCL let do: mcore::objects::MResourceAction = mcore::objects::MResourceAction::Do in
		do
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MRESOURCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MRESOURCE__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MRESOURCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MRESOURCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MResourceAction result = (MResourceAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setDoAction(MResourceAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MResourceAction.OPEN_EDITOR_VALUE:

			break;
		case MResourceAction.FIX_ALL_TYPES_VALUE:
			/*
			 * ATTENTION: REPEATED IN: MComponent do...CLASS MObject do... FIX
			 * TYPE MResource do... FIX ALL TYPES
			 */
			if (!getObject().isEmpty()) {
				MComponent c = (MComponent) this.eResource().getContents()
						.get(0);
				MObject root = getObject().get(0);
				MPackage p = null;
				if (root.getType() == null) {
					if (c.getOwnedPackage().isEmpty()) {
						p = McoreFactory.eINSTANCE.createMPackage();
						c.getOwnedPackage().add(p);
						p.setName(c.getCalculatedName());
						this.setRootObjectPackage(p);
					} else {
						if (getRootObjectPackage() == null) {
							p = c.getOwnedPackage().get(0);
							setRootObjectPackage(p);
						} else {
							p = getRootObjectPackage();
						}
					}
				} else {
					p = root.getType().getContainingPackage();
				}
				root.setCreateTypeInPackage(p);
				setRootObjectPackage(p);
				p.setResourceRootClass(root.getType());
			}
			break;
		case MResourceAction.OBJECT_VALUE:
			MObject newObject = ObjectsFactory.eINSTANCE.createMObject();
			this.getObject().add(newObject);
			break;

		default:
			break;
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MResourceAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL if self.object->isEmpty() then OrderedSet{
	mcore::objects::MResourceAction::Do, 
	mcore::objects::MResourceAction::OpenEditor,
	mcore::objects::MResourceAction::FixAllTypes,
	mcore::objects::MResourceAction::Object
	} else OrderedSet{
	mcore::objects::MResourceAction::Do, 
	mcore::objects::MResourceAction::OpenEditor,
	mcore::objects::MResourceAction::FixAllTypes
	}  endif                           
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MResourceAction> evalDoActionChoiceConstruction(
			List<MResourceAction> choice) {
		EClass eClass = ObjectsPackage.Literals.MRESOURCE;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ObjectsPackage.Literals.MRESOURCE__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MResourceAction> result = new ArrayList<MResourceAction>(
					(Collection<MResourceAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MResourceAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.objects.MResourceAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE.getMResource_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getRootObjectPackage() {
		if (rootObjectPackage != null && rootObjectPackage.eIsProxy()) {
			InternalEObject oldRootObjectPackage = (InternalEObject) rootObjectPackage;
			rootObjectPackage = (MPackage) eResolveProxy(oldRootObjectPackage);
			if (rootObjectPackage != oldRootObjectPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE,
							oldRootObjectPackage, rootObjectPackage));
			}
		}
		return rootObjectPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetRootObjectPackage() {
		return rootObjectPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootObjectPackage(MPackage newRootObjectPackage) {
		MPackage oldRootObjectPackage = rootObjectPackage;
		rootObjectPackage = newRootObjectPackage;
		boolean oldRootObjectPackageESet = rootObjectPackageESet;
		rootObjectPackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE,
					oldRootObjectPackage, rootObjectPackage,
					!oldRootObjectPackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRootObjectPackage() {
		MPackage oldRootObjectPackage = rootObjectPackage;
		boolean oldRootObjectPackageESet = rootObjectPackageESet;
		rootObjectPackage = null;
		rootObjectPackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE,
					oldRootObjectPackage, null, oldRootObjectPackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRootObjectPackage() {
		return rootObjectPackageESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolder getContainingFolder() {
		MResourceFolder containingFolder = basicGetContainingFolder();
		return containingFolder != null && containingFolder.eIsProxy()
				? (MResourceFolder) eResolveProxy(
						(InternalEObject) containingFolder)
				: containingFolder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolder basicGetContainingFolder() {
		/**
		 * @OCL if   self.eContainer().oclIsKindOf(MResourceFolder) 
		then self.eContainer().oclAsType(MResourceFolder)
		else null
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MRESOURCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MRESOURCE__CONTAINING_FOLDER;

		if (containingFolderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingFolderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MRESOURCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingFolderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MRESOURCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MResourceFolder result = (MResourceFolder) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean update(MProperty property) {

		MClassifier rootClass = null;

		// if (property.getPropertyBehavior() == PropertyBehavior.STORED)
		// rootClass = this.object.get(0).getType();

		// TODO: implement this method (no OCL found!)
		// Ensure that you remove @generated or mark it @generated NOT

		Iterator<MObject> itr = this.object.iterator();
		EList<MObject> containmentObjects = new BasicEList<MObject>();
		MClassifier classifier = property.getContainingClassifier();

		while (itr.hasNext()) {

			MObject nextObject = itr.next();
			EList<MPropertyInstance> propertyList = nextObject
					.getPropertyInstance();
			// ALL of following assumes nextObject has type!

			if (nextObject.getType() != null) {
				// Initialize RootObject MInstances
				for (MProperty initProp : nextObject.getType()
						.allContainmentReferences()) {
					boolean propExists = false;
					for (int i = 0; i < nextObject.getPropertyInstance()
							.size(); i++) {
						if (propertyList.get(i) != null
								&& propertyList.get(i).getProperty() != null
								&& (initProp.equals(
										propertyList.get(i).getProperty())))
							propExists = true;
					}
					if (!propExists) {
						propertyList.add(ObjectsFactory.eINSTANCE
								.createMPropertyInstance());
						propertyList.get(propertyList.size() - 1)
								.setProperty(initProp);

						// Change MDataValue, so it is not initialized when the
						// MPropertyInstance is a CONTAINMENT ( We initialize
						// the property as firstChoosableProperty..think about
						// it!

					}

				}
				Iterator<MPropertyInstance> iter = nextObject
						.getPropertyInstance().iterator();
				if (nextObject.isRootObject())
					rootClass = nextObject.getType();
				while (iter.hasNext()) {
					MPropertyInstance property1 = iter.next();

					if (!property.getContainment()
							&& property1.getProperty().getType() != null
							&& (property1.getProperty().getType() == property
									.getType()
									|| !property1.getProperty().getType()
											.allSubTypes().isEmpty()
											&& property1.getProperty().getType()
													.allSubTypes()
													.contains(property
															.getType()))) {

						EList<MObject> allInstances = property.getType()
								.getInstance();

						for (MObject obj : allInstances) {
							if (obj.getContainingPropertyInstance()
									.getProperty() == property) {
								obj.getContainingPropertyInstance()
										.getInternalReferencedObject()
										.add(ObjectsFactory.eINSTANCE
												.createMObjectReference());
								obj.getContainingPropertyInstance()
										.getInternalReferencedObject()
										.get(obj.getContainingPropertyInstance()
												.getInternalReferencedObject()
												.size() - 1)
										.setReferencedObject(obj);
							}

						}
						property1.getInternalContainedObject()
								.addAll(allInstances);
						return true;
					}

					// Complete Property was deleted
					else if (property1.getWrongLocalKey()
							&& property1.getProperty() == null
							|| (!property1.choosableProperties()
									.contains(property1.getProperty()))) {
						containmentObjects
								.addAll(property1.getInternalContainedObject());
					} else {

						for (MObject obj : property1
								.getInternalContainedObject())
							if (obj.getType() == property.getType())
								containmentObjects.add(obj);
					}

					property1.getInternalContainedObject()
							.removeAll(containmentObjects);

					if (property1.getProperty().getType() != null && (property1
							.getInternalContainedObject().isEmpty()
							&& !property1.choosableProperties()
									.contains(property1.getProperty()))) {
						iter.remove();
					}

				}
			}

		}

		if (containmentObjects.size() > 0) {
			// Search the object, that contains the new Property
			// Nehmen rootClass und suchen in allen contained Properties die
			// Klasse, die das Objekt jetzt contained

			// The classifier that contains the new property is contained in :

			// IMPORTANT // Add check that we don't contain our selves
			EList<MClassifier> classhierachy = new BasicEList<MClassifier>();
			if (classifier == null
					|| classifier.allClassesContainedIn().size() == 0)
				classhierachy.add(rootClass);
			else {

				classhierachy.add(property.getContainingClassifier());

				MClassifier currentClass = classhierachy.get(0);

				while (currentClass.allClassesContainedIn().size() != 0) {

					if (currentClass.allClassesContainedIn()
							.get(0) == currentClass) {
						if (currentClass.allClassesContainedIn().size() == 1) {
							// System.out.println("contains itself and nothing
							// else");
							break;
						} else
							currentClass = currentClass.allClassesContainedIn()
									.get(1);
					} else
						currentClass = currentClass.allClassesContainedIn()
								.get(0);

					classhierachy.add(currentClass);
					if (classifier.allClassesContainedIn().size() == 0) {
						classhierachy.add(rootClass);
						break;
					}

				}

			}
			this.addAllObjects(classhierachy, object.get(0),
					containmentObjects);
		}

		return true;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean updateObjects(MProperty property,
			MProperty correspondingProperty) {

		if (property == null || correspondingProperty == null) {
			System.out.println("one is null");
			return null;
		}
		if (!property.getType().getCanApplyDefaultContainment()) {
			System.out.println(correspondingProperty.getType()
					.getCanApplyDefaultContainment());
			System.out.println("cannot Apply");
			return null;
		}
		if (property.getType() == null) {
			return null;
		}

		if (property.getType().getIntelligentNearestInstance().isEmpty())
			return null;

		if (property.getType().getIntelligentInstance().isEmpty()) {
			return false;
		}
		if (property.getType().getIntelligentInstantiationPropertyHierarchy()
				.isEmpty())
			return null;
		else {
			EList<MProperty> listPropertyInHierarchy = property.getType()
					.getIntelligentInstantiationPropertyHierarchy();
			MProperty lastPropInHierarchy = listPropertyInHierarchy
					.get(listPropertyInHierarchy.size() - 1);

			if (lastPropInHierarchy == null)
				return null;
			// else if ((listPropertyInHierarchy
			// .get(listPropertyInHierarchy.size() - 1) == property)) // Just
			// copy ??
			// return null;

		}

		MObject rootObj = this.getObject().get(0);
		/*
		 * If the property is stored, all containments are copied into
		 * rootcontainment The stored property will reference its former objects
		 */
		if (property.getPropertyBehavior() == PropertyBehavior.STORED) {
			return updateContainmentToStoredFeature(property, rootObj);

		}

		/*
		 * If property is contained and needs to swap its instances with
		 * correspondingProperty
		 */
		if (!property.getType().getObjectReferences().isEmpty()
				&& ((correspondingProperty != null && correspondingProperty
						.getContainingClassifier() != null)
						|| property
								.getPropertyBehavior() == PropertyBehavior.CONTAINED)) {

			return updateStoredToContainmentFeature(property, rootObj);

		}

		/*
		 * Flat default containment, stored->containment if no references exist
		 */

		return addObjects(property, rootObj);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean updateContainmentToStoredFeature(MProperty property,
			MObject rootObj) {

		EList<MObject> allInstances = property.getType()
				.getIntelligentInstance();

		for (MObject obj : allInstances) {
			if (obj.getContainingPropertyInstance().getProperty() == property) {
				obj.getContainingPropertyInstance()
						.getInternalReferencedObject()
						.add(ObjectsFactory.eINSTANCE.createMObjectReference());
				obj.getContainingPropertyInstance()
						.getInternalReferencedObject()
						.get(obj.getContainingPropertyInstance()
								.getInternalReferencedObject().size() - 1)
						.setReferencedObject(obj);
			}

		}
		if (rootObj.getPropertyInstance().isEmpty()
				|| rootObj.propertyInstanceFromProperty(property.getType()
						.getInstantiationPropertyHierarchy().get(0)) == null) {
			rootObj.getPropertyInstance()
					.add(ObjectsFactory.eINSTANCE.createMPropertyInstance());
			rootObj.getPropertyInstance()
					.get(rootObj.getPropertyInstance().size() - 1)
					.setProperty(property.getType()
							.getInstantiationPropertyHierarchy().get(0));
		}

		return rootObj
				.propertyInstanceFromProperty(property.getType()
						.getInstantiationPropertyHierarchy().get(0))
				.getInternalContainedObject().addAll(allInstances);

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean updateStoredToContainmentFeature(
			MProperty correspondingProperty, MObject rootObj) {

		EList<MObject> refObj = correspondingProperty.getType()
				.getObjectReferences();
		EList<MPropertyInstance> instances = correspondingProperty.getType()
				.getObjectReferencePropertyInstances();

		ArrayList<MObjectReference> deleteRefs = new ArrayList<MObjectReference>();

		for (MObject obj : refObj)
			for (MPropertyInstance ins : instances) {
				deleteRefs.clear();
				for (MObjectReference ref : ins.getInternalReferencedObject())
					if (obj.getReferencedBy().contains(ref)) {
						ins.getInternalContainedObject().add(obj);
						deleteRefs.add(ref);

					}
				ins.getInternalReferencedObject().removeAll(deleteRefs);

			}

		/*
		 * If all objects were copied ( Every object was referenced)
		 */
		if (correspondingProperty.getType().getOutstandingToCopyObjects()
				.isEmpty()) {
			System.out.println("return ");
			// rootObj.getPropertyInstance().removeAll(
			// rootObj.getWrongPropertyInstance());
			return true;
		}

		/*
		 * If there are further objects to copy, we will look for one containing
		 * object, that didn't store any instance of the property before The
		 * unreferencedobjects will be moved there If no such object exists, a
		 * new one is created
		 */
		MObject lastObj = null;
		for (MObject obj : correspondingProperty.getType()
				.getIntelligentNearestInstance())
			if (obj.propertyInstanceFromProperty(
					correspondingProperty) == null) {
				obj.getPropertyInstance().add(
						ObjectsFactory.eINSTANCE.createMPropertyInstance());
				obj.getPropertyInstance()
						.get(obj.getPropertyInstance().size() - 1)
						.setProperty(correspondingProperty);
				lastObj = obj;
				break;
			} else if (obj
					.propertyInstanceFromProperty(correspondingProperty) != null
					&& obj.propertyInstanceFromProperty(correspondingProperty)
							.getInternalContainedObject().isEmpty()) {
				lastObj = obj;
				break;
			}

		if (lastObj != null) {
			lastObj.propertyInstanceFromProperty(correspondingProperty)
					.getInternalContainedObject().addAll(correspondingProperty
							.getType().getOutstandingToCopyObjects());
			rootObj.getPropertyInstance()
					.removeAll(rootObj.getWrongPropertyInstance());
			return true;
		} else {

			EList<MObject> allInstances = correspondingProperty.getType()
					.getIntelligentInstance();
			lastObj = allInstances.get(allInstances.size() - 1);
			EList<MObject> objectsToCopy = correspondingProperty.getType()
					.getOutstandingToCopyObjects();

			EList<MProperty> propertyInstantiation = correspondingProperty
					.getType().getIntelligentInstantiationPropertyHierarchy();

			EList<MObject> nearestInstanceOfProperty = propertyInstantiation
					.get(0).getType().getIntelligentNearestInstance();

			if (!nearestInstanceOfProperty.isEmpty()) {
				MObject newObj = ObjectsFactory.eINSTANCE.createMObject();

				// We only have hierarchy of one. Need to check that for
				// multiple layers

				nearestInstanceOfProperty.get(0).getContainingPropertyInstance()
						.getInternalContainedObject().add(newObj);
				newObj.propertyInstanceFromProperty(correspondingProperty)
						.getInternalContainedObject().addAll(objectsToCopy);

			}

			rootObj.getPropertyInstance()
					.removeAll(rootObj.getWrongPropertyInstance());

			return true;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean addObjects(MProperty property, MObject rootObj) {

		EList<MObject> objsToCopy = property.getType().getIntelligentInstance();
		EList<MProperty> props = property.getType()
				.getIntelligentInstantiationPropertyHierarchy();
		EList<MObject> nearestInstance = property.getType()
				.getIntelligentNearestInstance();
		MObject trivialObj = nearestInstance.get(0);
		EList<MPropertyInstance> objPropList = trivialObj.getPropertyInstance();
		MProperty lastPropInHierarchy = props.get(props.size() - 1);
		MPropertyInstance propInstanceFromSlot = trivialObj
				.propertyInstanceFromProperty(lastPropInHierarchy);

		while (property.getType().getIntelligentNearestInstance().get(0)
				.getType() != property.getType()) {

			if (lastPropInHierarchy == property)
				System.out.println("not allowed");

			props = property.getType()
					.getIntelligentInstantiationPropertyHierarchy();
			lastPropInHierarchy = props.get(props.size() - 1);
			nearestInstance = property.getType()
					.getIntelligentNearestInstance();
			trivialObj = nearestInstance.get(0);
			objPropList = trivialObj.getPropertyInstance();
			propInstanceFromSlot = trivialObj
					.propertyInstanceFromProperty(lastPropInHierarchy);

			if (propInstanceFromSlot == null) {
				objPropList.add(
						ObjectsFactory.eINSTANCE.createMPropertyInstance());
				objPropList.get(objPropList.size() - 1)
						.setProperty(lastPropInHierarchy);
				propInstanceFromSlot = objPropList.get(objPropList.size() - 1);
			}

			if (lastPropInHierarchy == property || (props.size() == 1
					&& (property.getType() == lastPropInHierarchy.getType()
							|| property.getType().getSuperType().contains(
									lastPropInHierarchy.getType())))) {
				System.out.println("here");
				propInstanceFromSlot.getInternalContainedObject()
						.addAll(objsToCopy);
				if (!propInstanceFromSlot.getContainingObject().isRootObject())
					rootObj.getPropertyInstance()
							.removeAll(rootObj.getWrongPropertyInstance());
				return true;
			}

			// Does MObject and then infinite loop
			// Creates one too much Object => Check for supertype except for
			// propInstanceFromSlot !
			if (propInstanceFromSlot.getInternalContainedObject().isEmpty()
					&& propInstanceFromSlot.getProperty() != property)
				propInstanceFromSlot.getInternalContainedObject()
						.add(ObjectsFactory.eINSTANCE.createMObject());

		}

		return null;

	}

	@SuppressWarnings("unchecked")
	public void addAllObjects(EList<MClassifier> classhierachy,
			MObject firstObj, EList<MObject> containmentObjects) {

		if (containmentObjects.isEmpty() || firstObj == null
				|| containmentObjects == null)
			return;

		boolean isCreated = false;
		boolean createdMPropertyInstance = false;
		MClassifier commonSuperClass = null;

		Iterator<MPropertyInstance> itr = firstObj.getPropertyInstance()
				.iterator();

		// Common supertype of containmentObjects
		for (MObject superObjects : containmentObjects) {
			if (commonSuperClass == null)
				commonSuperClass = superObjects.getType();

			if ((superObjects.getType() != commonSuperClass)
					&& (!commonSuperClass.allSubTypes()
							.contains(superObjects.getType())))
				commonSuperClass = commonSuperClass.allSuperTypes().get(0); // Improve
			// to
			// check
			// every
			// supertype
		}

		while (itr.hasNext()) {
			MPropertyInstance nextObj = itr.next();

			if (nextObj.getProperty() != null)
				if ((nextObj.getProperty().getType() == containmentObjects
						.get(0).getType())
						|| commonSuperClass.allSubTypes()
								.contains(nextObj.getProperty().getType()) // ADDED
						|| commonSuperClass == nextObj.getProperty().getType() // ADDED
						|| (classhierachy.size() >= 2 && nextObj.getProperty()
								.getType() == classhierachy
										.get(classhierachy.size() - 2))) {
					createdMPropertyInstance = true;
				}

		}

		if (firstObj.getPropertyInstance().isEmpty()
				|| !createdMPropertyInstance) {
			for (MProperty s : firstObj.getType().allContainmentReferences())
				if (s.getType() == containmentObjects.get(0).getType()
						|| commonSuperClass.allSubTypes().contains(s.getType()) // ADDED
						|| commonSuperClass == s.getType()
						|| (classhierachy.size() >= 2
								&& (s.getType() == classhierachy
										.get(classhierachy.size() - 2)))) {
					firstObj.getPropertyInstance().add(
							ObjectsFactory.eINSTANCE.createMPropertyInstance());
					firstObj.getPropertyInstance()
							.get(firstObj.getPropertyInstance().size() - 1)
							.setProperty(s);

				}
		}

		// System.out.println("before propertySLot");
		// System.out.println(firstObj.getId());

		for (MProperty s : firstObj.getType().allContainmentReferences()) {

			if (containmentObjects == null || containmentObjects.isEmpty())
				return;

			if (classhierachy.size() == 1 && (!containmentObjects.isEmpty()
					&& (s.getType() == containmentObjects.get(0).getType())
					|| commonSuperClass.allSubTypes().contains(s.getType())
					|| s.getType().allSubTypes().contains(commonSuperClass) // ADDED
					|| commonSuperClass == s.getType()))

			{

				for (MPropertyInstance slot : firstObj.getPropertyInstance()) {
					if (containmentObjects == null
							|| containmentObjects.isEmpty())
						return;

					if (slot.getProperty() != null) {
						if (slot.getProperty().getType() == containmentObjects
								.get(0).getType()
								|| commonSuperClass.allSubTypes()
										.contains(s.getType())
								|| s.getType().allSubTypes()
										.contains(commonSuperClass) // ADDED
								|| commonSuperClass == s.getType()) {
							if (!slot.getInternalReferencedObject().isEmpty()) {
								Iterator<MObjectReference> iter = slot
										.getInternalReferencedObject()
										.iterator();
								EList<MObjectReference> removeObjects = new BasicEList<MObjectReference>();
								while (iter.hasNext()) {
									MObjectReference reference = iter.next();

									if (reference.isSetReferencedObject()
											&& reference
													.getReferencedObject() != null) {
										if (containmentObjects
												.contains(reference
														.getReferencedObject())) {

											slot.getInternalContainedObject()
													.add(containmentObjects
															.get(containmentObjects
																	.indexOf(
																			reference
																					.getReferencedObject())));
											containmentObjects.remove(reference
													.getReferencedObject());
											removeObjects.add(reference);
										}
									} else
										removeObjects.add(reference);

								}
								slot.getInternalReferencedObject()
										.removeAll(removeObjects);

							} else {

								if (slot.getProperty().getContainment()
										&& (slot.getProperty()
												.getType() == commonSuperClass
												|| commonSuperClass
														.getSuperType()
														.contains(slot
																.getProperty()
																.getType()))) {
									if (firstObj
											.getContainingPropertyInstance() != null
											&& firstObj
													.getContainingPropertyInstance()
													.getInternalContainedObject() != null
											&& firstObj
													.getContainingPropertyInstance()
													.getInternalContainedObject()
													.size() == 1) {

										slot.getInternalContainedObject()
												.addAll(containmentObjects);
										containmentObjects = null;

									} else {
										slot.getInternalContainedObject()
												.addAll(containmentObjects);

									}
								}

								/*
								 * if (firstObj.getContainingPropertyInstance()
								 * == null) {
								 * System.out.println(property.getProperty().
								 * getType().toString());
								 * property.getInternalContainedObject().addAll(
								 * containmentObjects); } else if
								 * (firstObj.getContainingPropertyInstance()
								 * .getInternalContainedObject().size() == 1) {
								 * 
								 * 
								 * property.getInternalContainedObject().addAll(
								 * containmentObjects); containmentObjects =
								 * null; }
								 */
							}

							isCreated = true;
						}
					}
				}
				return;
			} else
				for (MPropertyInstance obj : firstObj.getPropertyInstance()) {
					for (MObject object : obj.getInternalContainedObject())
						addAllObjects(classhierachy, object,
								containmentObjects);
				}
		}

		for (MPropertyInstance property : firstObj.getPropertyInstance()) {
			if (property != null && property.getProperty() != null
					&& property.getProperty().getType() != null
					&& !containmentObjects.isEmpty()) {

				for (MProperty s : property.getProperty().getType()
						.allContainmentReferences()) {
					if (s.getType() == containmentObjects.get(0).getType()) {
						isCreated = true;
						if (property.getInternalContainedObject().size() == 0) {
							property.getInternalContainedObject().add(
									ObjectsFactory.eINSTANCE.createMObject());
							property.getInternalContainedObject()
									.get(property.getInternalContainedObject()
											.size() - 1)
									.setType(property.getProperty().getType());
						}

						// Check for storing features, subtract them from
						// containmentobjects

						for (MObject obj : property
								.getInternalContainedObject()) {
							addAllObjects(classhierachy, obj,
									containmentObjects);
						}

						// addAllObjects(classhierachy,
						// property.getInternalContainedObject().get(0),
						// containmentObjects);

					}
					if (classhierachy.size() >= 2 && (property.getProperty()
							.getType() == classhierachy
									.get(classhierachy.size() - 2))) {
						classhierachy.remove(classhierachy.size() - 1);
						if (property.getInternalContainedObject().size() == 0) {
							property.getInternalContainedObject().add(
									ObjectsFactory.eINSTANCE.createMObject());
							property.getInternalContainedObject()
									.get(property.getInternalContainedObject()
											.size() - 1)
									.setType(property.getProperty().getType());

						}

						for (MObject obj : property
								.getInternalContainedObject()) {
							addAllObjects(classhierachy, obj,
									containmentObjects);
						}
						// addAllObjects(classhierachy,property.getInternalContainedObject().get(0),containmentObjects);
						// check for storing features, subtract them from
						// containmentObjects
					}

				}

			}

		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE__OBJECT:
			return ((InternalEList<?>) getObject()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE__OBJECT:
			return getObject();
		case ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE:
			if (resolve)
				return getRootObjectPackage();
			return basicGetRootObjectPackage();
		case ObjectsPackage.MRESOURCE__CONTAINING_FOLDER:
			if (resolve)
				return getContainingFolder();
			return basicGetContainingFolder();
		case ObjectsPackage.MRESOURCE__ID:
			return getId();
		case ObjectsPackage.MRESOURCE__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE__OBJECT:
			getObject().clear();
			getObject().addAll((Collection<? extends MObject>) newValue);
			return;
		case ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE:
			setRootObjectPackage((MPackage) newValue);
			return;
		case ObjectsPackage.MRESOURCE__ID:
			setId((String) newValue);
			return;
		case ObjectsPackage.MRESOURCE__DO_ACTION:
			setDoAction((MResourceAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE__OBJECT:
			unsetObject();
			return;
		case ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE:
			unsetRootObjectPackage();
			return;
		case ObjectsPackage.MRESOURCE__ID:
			unsetId();
			return;
		case ObjectsPackage.MRESOURCE__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE__OBJECT:
			return isSetObject();
		case ObjectsPackage.MRESOURCE__ROOT_OBJECT_PACKAGE:
			return isSetRootObjectPackage();
		case ObjectsPackage.MRESOURCE__CONTAINING_FOLDER:
			return basicGetContainingFolder() != null;
		case ObjectsPackage.MRESOURCE__ID:
			return isSetId();
		case ObjectsPackage.MRESOURCE__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MRESOURCE___DO_ACTION$_UPDATE__MRESOURCEACTION:
			return doAction$Update((MResourceAction) arguments.get(0));
		case ObjectsPackage.MRESOURCE___UPDATE__MPROPERTY:
			return update((MProperty) arguments.get(0));
		case ObjectsPackage.MRESOURCE___UPDATE_OBJECTS__MPROPERTY_MPROPERTY:
			return updateObjects((MProperty) arguments.get(0),
					(MProperty) arguments.get(1));
		case ObjectsPackage.MRESOURCE___UPDATE_CONTAINMENT_TO_STORED_FEATURE__MPROPERTY_MOBJECT:
			return updateContainmentToStoredFeature(
					(MProperty) arguments.get(0), (MObject) arguments.get(1));
		case ObjectsPackage.MRESOURCE___UPDATE_STORED_TO_CONTAINMENT_FEATURE__MPROPERTY_MOBJECT:
			return updateStoredToContainmentFeature(
					(MProperty) arguments.get(0), (MObject) arguments.get(1));
		case ObjectsPackage.MRESOURCE___ADD_OBJECTS__MPROPERTY_MOBJECT:
			return addObjects((MProperty) arguments.get(0),
					(MObject) arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		if (idESet)
			result.append(id);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL (if self.name.oclIsUndefined() then 'NAME MISSING' else
	 *      self.calculatedShortName endif) .concat('.') .concat(if
	 *      self.object->isEmpty() then 'OBJECT MISSING' else if
	 *      self.object->first().type.oclIsUndefined() then 'TYPE OF OBJECT
	 *      MISSING' else self.object->first().type.containingPackage.eName
	 *      endif endif)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ObjectsPackage.Literals.MRESOURCE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Resource'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ObjectsPackage.Literals.MRESOURCE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL eName let nSPrefix: String = if (let chain: OrderedSet(mcore::objects::MObject)  = object->asOrderedSet() in
	if chain->notEmpty().oclIsUndefined() 
	then null 
	else chain->notEmpty()
	endif) 
	=true 
	then let subchain1 : mcore::objects::MObject = let chain: OrderedSet(mcore::objects::MObject)  = object->asOrderedSet() in
	if chain->first().oclIsUndefined() 
	then null 
	else chain->first()
	endif in 
	if subchain1 = null 
	then null 
	else if subchain1.type.containingPackage.oclIsUndefined()
	then null
	else subchain1.type.containingPackage.derivedNsPrefix
	endif endif 
	else if (let chain: mcore::MPackage = rootObjectPackage in
	if chain <> null then true else false 
	endif)=true then if rootObjectPackage.oclIsUndefined()
	then null
	else rootObjectPackage.derivedNsPrefix
	endif
	else 'ROOT OBJECT PACKAGE MISSING'
	endif endif in
	let namePlusNSPrefix: String = let e1: String = calculatedShortName.concat('.').concat(nSPrefix) in 
	if e1.oclIsInvalid() then null else e1 endif in
	namePlusNSPrefix
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (ObjectsPackage.Literals.MRESOURCE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ObjectsPackage.Literals.MRESOURCE__OBJECT,
				ObjectsPackage.Literals.MRESOURCE__ROOT_OBJECT_PACKAGE,
				ObjectsPackage.Literals.MRESOURCE__ID };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MRESOURCE,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} // MResourceImpl
