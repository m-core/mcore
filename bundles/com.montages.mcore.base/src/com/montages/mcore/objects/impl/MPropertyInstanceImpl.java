/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;
import com.montages.mcore.impl.MRepositoryElementImpl;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MPropertyInstanceAction;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MProperty Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getInternalDataValue <em>Internal Data Value</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getInternalLiteralValue <em>Internal Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getInternalReferencedObject <em>Internal Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getInternalContainedObject <em>Internal Contained Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getContainingObject <em>Containing Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getDuplicateInstance <em>Duplicate Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getAllInstancesOfThisProperty <em>All Instances Of This Property</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getWrongInternalContainedObjectDueToKind <em>Wrong Internal Contained Object Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getWrongInternalObjectReferenceDueToKind <em>Wrong Internal Object Reference Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getWrongInternalLiteralValueDueToKind <em>Wrong Internal Literal Value Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getWrongInternalDataValueDueToKind <em>Wrong Internal Data Value Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getNumericIdOfPropertyInstance <em>Numeric Id Of Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getDerivedContainment <em>Derived Containment</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getLocalKey <em>Local Key</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getKey <em>Key</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getPropertyNameOrLocalKeyDefinition <em>Property Name Or Local Key Definition</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getWrongLocalKey <em>Wrong Local Key</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getDerivedKind <em>Derived Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getPropertyKindAsString <em>Property Kind As String</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getCorrectPropertyKind <em>Correct Property Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getDoAddValue <em>Do Add Value</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getSimpleTypeFromValues <em>Simple Type From Values</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl#getEnumerationFromValues <em>Enumeration From Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MPropertyInstanceImpl extends MRepositoryElementImpl
		implements MPropertyInstance, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getInternalDataValue() <em>Internal Data Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalDataValue()
	 * @generated
	 * @ordered
	 */
	protected EList<MDataValue> internalDataValue;

	/**
	 * The cached value of the '{@link #getInternalLiteralValue() <em>Internal Literal Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalLiteralValue()
	 * @generated
	 * @ordered
	 */
	protected EList<MLiteralValue> internalLiteralValue;

	/**
	 * The cached value of the '{@link #getInternalReferencedObject() <em>Internal Referenced Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalReferencedObject()
	 * @generated
	 * @ordered
	 */
	protected EList<MObjectReference> internalReferencedObject;

	/**
	 * The cached value of the '{@link #getInternalContainedObject() <em>Internal Contained Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalContainedObject()
	 * @generated
	 * @ordered
	 */
	protected EList<MObject> internalContainedObject;

	/**
	 * The default value of the '{@link #getDuplicateInstance() <em>Duplicate Instance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuplicateInstance()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DUPLICATE_INSTANCE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNumericIdOfPropertyInstance() <em>Numeric Id Of Property Instance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumericIdOfPropertyInstance()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMERIC_ID_OF_PROPERTY_INSTANCE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedContainment() <em>Derived Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedContainment()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DERIVED_CONTAINMENT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLocalKey() <em>Local Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalKey()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocalKey() <em>Local Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalKey()
	 * @generated
	 * @ordered
	 */
	protected String localKey = LOCAL_KEY_EDEFAULT;

	/**
	 * This is true if the Local Key attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean localKeyESet;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected MProperty property;

	/**
	 * This is true if the Property reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean propertyESet;

	/**
	 * The default value of the '{@link #getKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKey()
	 * @generated
	 * @ordered
	 */
	protected static final String KEY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getPropertyNameOrLocalKeyDefinition() <em>Property Name Or Local Key Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyNameOrLocalKeyDefinition()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getWrongLocalKey() <em>Wrong Local Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrongLocalKey()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean WRONG_LOCAL_KEY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedKind() <em>Derived Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedKind()
	 * @generated
	 * @ordered
	 */
	protected static final PropertyKind DERIVED_KIND_EDEFAULT = PropertyKind.ATTRIBUTE;

	/**
	 * The default value of the '{@link #getPropertyKindAsString() <em>Property Kind As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyKindAsString()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTY_KIND_AS_STRING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCorrectPropertyKind() <em>Correct Property Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectPropertyKind()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECT_PROPERTY_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MPropertyInstanceAction DO_ACTION_EDEFAULT = MPropertyInstanceAction.DO;

	/**
	 * The default value of the '{@link #getDoAddValue() <em>Do Add Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAddValue()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DO_ADD_VALUE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSimpleTypeFromValues() <em>Simple Type From Values</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeFromValues()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SIMPLE_TYPE_FROM_VALUES_EDEFAULT = SimpleType.NONE;

	/**
	 * The parsed OCL expression for the body of the '{@link #propertyNameOrLocalKeyDefinition$Update <em>Property Name Or Local Key Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #propertyNameOrLocalKeyDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression propertyNameOrLocalKeyDefinition$UpdateecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateobjectsMPropertyInstanceActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAddValue$Update <em>Do Add Value$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAddValue$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAddValue$UpdateecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctProperty <em>Correct Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctProperty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctMultiplicity <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctMultiplicity
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctMultiplicityBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #propertyValueAsString <em>Property Value As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #propertyValueAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression propertyValueAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #internalReferencedObjectsAsString <em>Internal Referenced Objects As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #internalReferencedObjectsAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression internalReferencedObjectsAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #internalDataValuesAsString <em>Internal Data Values As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #internalDataValuesAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression internalDataValuesAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #internalLiteralValuesAsString <em>Internal Literal Values As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #internalLiteralValuesAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression internalLiteralValuesAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #choosableProperties <em>Choosable Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #choosableProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression choosablePropertiesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #propertyNameOrLocalKeyDefinitionUpdate <em>Property Name Or Local Key Definition Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #propertyNameOrLocalKeyDefinitionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression propertyNameOrLocalKeyDefinitionUpdateecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getProperty <em>Property</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression propertyChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getPropertyNameOrLocalKeyDefinition <em>Property Name Or Local Key Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyNameOrLocalKeyDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression propertyNameOrLocalKeyDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingObject <em>Containing Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDuplicateInstance <em>Duplicate Instance</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuplicateInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression duplicateInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllInstancesOfThisProperty <em>All Instances Of This Property</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllInstancesOfThisProperty
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allInstancesOfThisPropertyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getWrongInternalContainedObjectDueToKind <em>Wrong Internal Contained Object Due To Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrongInternalContainedObjectDueToKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression wrongInternalContainedObjectDueToKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getWrongInternalObjectReferenceDueToKind <em>Wrong Internal Object Reference Due To Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrongInternalObjectReferenceDueToKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression wrongInternalObjectReferenceDueToKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getWrongInternalLiteralValueDueToKind <em>Wrong Internal Literal Value Due To Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrongInternalLiteralValueDueToKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression wrongInternalLiteralValueDueToKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getWrongInternalDataValueDueToKind <em>Wrong Internal Data Value Due To Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrongInternalDataValueDueToKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression wrongInternalDataValueDueToKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getWrongLocalKey <em>Wrong Local Key</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWrongLocalKey
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression wrongLocalKeyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKey <em>Key</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKey
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression keyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNumericIdOfPropertyInstance <em>Numeric Id Of Property Instance</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumericIdOfPropertyInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression numericIdOfPropertyInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedKind <em>Derived Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedContainment <em>Derived Containment</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedContainment
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedContainmentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getPropertyKindAsString <em>Property Kind As String</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyKindAsString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression propertyKindAsStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectPropertyKind <em>Correct Property Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectPropertyKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctPropertyKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getDoAction <em>Do Action</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAddValue <em>Do Add Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAddValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doAddValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeFromValues <em>Simple Type From Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeFromValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression simpleTypeFromValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEnumerationFromValues <em>Enumeration From Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationFromValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression enumerationFromValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPropertyInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MPROPERTY_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getProperty() {
		if (property != null && property.eIsProxy()) {
			InternalEObject oldProperty = (InternalEObject) property;
			property = (MProperty) eResolveProxy(oldProperty);
			if (property != oldProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY,
							oldProperty, property));
			}
		}
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setProperty(MProperty newProperty) {
		MProperty oldProperty = property;
		property = newProperty;
		boolean oldPropertyESet = propertyESet;
		propertyESet = true;
		//update LocalKey
		if (newProperty != null && newProperty.getName() != null) {
			//reset localKey, as it is same as property name
			if (newProperty.getName() == getLocalKey()) {
				setLocalKey(null);
			}
		} else {
			//set localKey, if the property is set to null, IFF the property name is set!
			if (oldProperty != null && oldProperty.getName() != null
					&& !oldProperty.getName().equals("")) {
				setLocalKey(oldProperty.getCalculatedShortName());
			}
		}

		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY, oldProperty,
					property, !oldPropertyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unsetProperty() {
		MProperty oldProperty = property;
		boolean oldPropertyESet = propertyESet;
		property = null;
		propertyESet = false;
		//update LocalKey
		//set localKey, if the property is set to null, IFF the property name is set!
		if (oldProperty != null && oldProperty.getName() != null
				&& !oldProperty.getName().equals("")) {
			setLocalKey(oldProperty.getCalculatedShortName());
		}

		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY, oldProperty,
					null, oldPropertyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProperty() {
		return propertyESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Property</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.choosableProperties()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalPropertyChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		if (propertyChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				propertyChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"PropertyChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(propertyChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "PropertyChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPropertyNameOrLocalKeyDefinition() {
		/**
		 * @OCL if ( property.oclIsUndefined()) 
		=true 
		then localKey
		else if property.oclIsUndefined()
		then null
		else property.calculatedShortName
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION;

		if (propertyNameOrLocalKeyDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				propertyNameOrLocalKeyDefinitionDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(propertyNameOrLocalKeyDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setPropertyNameOrLocalKeyDefinition(
			String newPropertyNameOrLocalKeyDefinition) {
		if (getProperty() == null) {
			this.setLocalKey(newPropertyNameOrLocalKeyDefinition);
		} else {
			String sn = getProperty().getShortName();
			if ((sn == null) || sn.trim().equals("")) {
				getProperty().setName(newPropertyNameOrLocalKeyDefinition);
			} else {
				getProperty().setShortName(newPropertyNameOrLocalKeyDefinition);
			}

		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MDataValue> getInternalDataValue() {
		if (internalDataValue == null) {
			internalDataValue = new EObjectContainmentEList.Unsettable.Resolving<MDataValue>(
					MDataValue.class, this,
					ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE);
		}
		return internalDataValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalDataValue() {
		if (internalDataValue != null)
			((InternalEList.Unsettable<?>) internalDataValue).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalDataValue() {
		return internalDataValue != null
				&& ((InternalEList.Unsettable<?>) internalDataValue).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralValue> getInternalLiteralValue() {
		if (internalLiteralValue == null) {
			internalLiteralValue = new EObjectContainmentEList.Unsettable.Resolving<MLiteralValue>(
					MLiteralValue.class, this,
					ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE);
		}
		return internalLiteralValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalLiteralValue() {
		if (internalLiteralValue != null)
			((InternalEList.Unsettable<?>) internalLiteralValue).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalLiteralValue() {
		return internalLiteralValue != null
				&& ((InternalEList.Unsettable<?>) internalLiteralValue).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReference> getInternalReferencedObject() {
		if (internalReferencedObject == null) {
			internalReferencedObject = new EObjectContainmentEList.Unsettable.Resolving<MObjectReference>(
					MObjectReference.class, this,
					ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT);
		}
		return internalReferencedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalReferencedObject() {
		if (internalReferencedObject != null)
			((InternalEList.Unsettable<?>) internalReferencedObject).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalReferencedObject() {
		return internalReferencedObject != null
				&& ((InternalEList.Unsettable<?>) internalReferencedObject)
						.isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getInternalContainedObject() {
		if (internalContainedObject == null) {
			internalContainedObject = new EObjectContainmentEList.Unsettable.Resolving<MObject>(
					MObject.class, this,
					ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT);
		}
		return internalContainedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalContainedObject() {
		if (internalContainedObject != null)
			((InternalEList.Unsettable<?>) internalContainedObject).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalContainedObject() {
		return internalContainedObject != null
				&& ((InternalEList.Unsettable<?>) internalContainedObject)
						.isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getContainingObject() {
		MObject containingObject = basicGetContainingObject();
		return containingObject != null && containingObject.eIsProxy()
				? (MObject) eResolveProxy((InternalEObject) containingObject)
				: containingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetContainingObject() {
		/**
		 * @OCL self.eContainer().oclAsType(MObject)
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__CONTAINING_OBJECT;

		if (containingObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MObject result = (MObject) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getDuplicateInstance() {
		/**
		 * @OCL self.allInstancesOfThisProperty->size()>1
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DUPLICATE_INSTANCE;

		if (duplicateInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				duplicateInstanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(duplicateInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyInstance> getAllInstancesOfThisProperty() {
		/**
		 * @OCL if property.oclIsUndefined() then OrderedSet{}
		else self.containingObject.propertyInstance->select(p:MPropertyInstance|p.property=property) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY;

		if (allInstancesOfThisPropertyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				allInstancesOfThisPropertyDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allInstancesOfThisPropertyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyInstance> result = (EList<MPropertyInstance>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getWrongInternalContainedObjectDueToKind() {
		/**
		 * @OCL if property.oclIsUndefined() then OrderedSet{}
		else if property.containment then OrderedSet{}
		else self.internalContainedObject endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND;

		if (wrongInternalContainedObjectDueToKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				wrongInternalContainedObjectDueToKindDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(wrongInternalContainedObjectDueToKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReference> getWrongInternalObjectReferenceDueToKind() {
		/**
		 * @OCL if self.property.oclIsUndefined() 
		then OrderedSet{}
		else if self.property.containment 
		  or (self.property.kind <> PropertyKind::Reference) 
		then self.internalReferencedObject
		     ->reject(r:MObjectReference|r.oclIsUndefined()) 
		else OrderedSet{} endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND;

		if (wrongInternalObjectReferenceDueToKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				wrongInternalObjectReferenceDueToKindDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(wrongInternalObjectReferenceDueToKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReference> result = (EList<MObjectReference>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralValue> getWrongInternalLiteralValueDueToKind() {
		/**
		 * @OCL if property.oclIsUndefined() 
		then OrderedSet{}
		else if self.property.type.oclIsUndefined()
		then OrderedSet{}
		else if self.property.type.kind <> ClassifierKind::Enumeration
		then self.internalLiteralValue->reject(l:MLiteralValue| l.literalValue.oclIsUndefined()) 
		else OrderedSet{} endif endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND;

		if (wrongInternalLiteralValueDueToKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				wrongInternalLiteralValueDueToKindDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(wrongInternalLiteralValueDueToKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralValue> result = (EList<MLiteralValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MDataValue> getWrongInternalDataValueDueToKind() {
		/**
		 * @OCL if property.oclIsUndefined() 
		then OrderedSet{}
		else if self.property.type.oclIsUndefined()
		then OrderedSet{}
		else if self.property.type.kind <> ClassifierKind::DataType
		then self.internalDataValue->reject(d:MDataValue| d.dataValue.oclIsUndefined() or ( d.dataValue='')) 
		else OrderedSet{} endif endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND;

		if (wrongInternalDataValueDueToKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				wrongInternalDataValueDueToKindDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(wrongInternalDataValueDueToKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MDataValue> result = (EList<MDataValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalKey() {
		return localKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalKey(String newLocalKey) {
		String oldLocalKey = localKey;
		localKey = newLocalKey;
		boolean oldLocalKeyESet = localKeyESet;
		localKeyESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY, oldLocalKey,
					localKey, !oldLocalKeyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLocalKey() {
		String oldLocalKey = localKey;
		boolean oldLocalKeyESet = localKeyESet;
		localKey = LOCAL_KEY_EDEFAULT;
		localKeyESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY, oldLocalKey,
					LOCAL_KEY_EDEFAULT, oldLocalKeyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLocalKey() {
		return localKeyESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getWrongLocalKey() {
		/**
		 * @OCL let localKeyNotSet:Boolean=self.localKey.oclIsUndefined() or (self.localKey='') in
		if self.property.oclIsUndefined()
		then if internalReferencedObject->notEmpty() 
		 or internalContainedObject->notEmpty()
		 then false
		 else  localKeyNotSet endif
		else if localKeyNotSet
		then false
		else self.property.eName <> self.localKey.camelCaseLower() 
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_LOCAL_KEY;

		if (wrongLocalKeyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				wrongLocalKeyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(wrongLocalKeyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getKey() {
		/**
		 * @OCL /*todo: check for trimmed versions of empty strings...SYSTEM*\/
		if  not property.oclIsUndefined()
		then self.property.eName 
		else if not ((self.localKey='') or self.localKey.oclIsUndefined())
		    then self.localKey.camelCaseLower()
		    else 	if self.derivedKind<>PropertyKind::Reference
		    	             then 'KEY MISSING'
		    	         else if self.internalReferencedObject->notEmpty()
		    	              then let o:MObject = 
		    	                            self.internalReferencedObject->first().referencedObject in
		    	                       if o.oclIsUndefined()
		    	                               then 'KEY MISSING'
		    	                       else if o.nature.oclIsUndefined() or o.nature=''
		    	                                then if not o.type.oclIsUndefined()
		    	                                             then o.type.eName
		    	                                             else' KEY MISSING' endif
		    	                       else o.nature.camelCaseLower() endif endif
		    	         else if self.internalContainedObject->notEmpty()  
		    	               then let o:MObject = 
		    	                            self.internalContainedObject->first() in
		   	                       if o.nature.oclIsUndefined() or o.nature=''
		    	                                then if not o.type.oclIsUndefined()
		    	                                             then o.type.eName
		    	                                             else' KEY MISSING' endif
		    	                       else o.nature.camelCaseLower() endif
		    	         else 'KEY MISSING' endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__KEY;

		if (keyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				keyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(keyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNumericIdOfPropertyInstance() {
		/**
		 * @OCL if self.eContainer().oclIsKindOf(MObject) 
		then let o:MObject=self.eContainer().oclAsType(MObject) in
		let pos:Integer= o.propertyInstance->indexOf(self) in
		if pos.oclIsUndefined() 
		then 'POS NOT CALCULATABLE'  
		else if pos<10 
		then '0'.concat(pos.toString()) 
		else pos.toString() endif  endif
		else 'PROPERTY INSTANCE NOT CONTAINED IN OBJECT' endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE;

		if (numericIdOfPropertyInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				numericIdOfPropertyInstanceDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(numericIdOfPropertyInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyKind getDerivedKind() {
		/**
		 * @OCL if not self.property.oclIsUndefined() 
		then self.property.kind
		else if self.internalContainedObject->isEmpty()
		 and self.internalDataValue->isEmpty()
		 and self.internalLiteralValue->isEmpty()
		 and self.internalReferencedObject->isEmpty()
		then PropertyKind::Undefined
		else if self.internalContainedObject->notEmpty()
		 and self.internalDataValue->isEmpty()
		 and self.internalLiteralValue->isEmpty()
		 and self.internalReferencedObject->isEmpty()
		then PropertyKind::Reference
		else if self.internalContainedObject->isEmpty()
		 and self.internalDataValue->notEmpty()
		 and self.internalLiteralValue->isEmpty()
		 and self.internalReferencedObject->isEmpty()
		then PropertyKind::Attribute
		else if self.internalContainedObject->isEmpty()
		 and self.internalDataValue->isEmpty()
		 and self.internalLiteralValue->notEmpty()
		 and self.internalReferencedObject->isEmpty()
		then PropertyKind::Attribute
		else if self.internalContainedObject->isEmpty()
		 and self.internalDataValue->isEmpty()
		 and self.internalLiteralValue->isEmpty()
		 and self.internalReferencedObject->notEmpty()
		then PropertyKind::Reference
		else PropertyKind::Ambiguous endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DERIVED_KIND;

		if (derivedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			PropertyKind result = (PropertyKind) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getDerivedContainment() {
		/**
		 * @OCL if not self.property.oclIsUndefined() 
		then self.property.containment
		else if self.internalContainedObject->notEmpty()
		 and self.internalDataValue->isEmpty()
		 and self.internalLiteralValue->isEmpty()
		 and self.internalReferencedObject->isEmpty()
		then true
		else false endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DERIVED_CONTAINMENT;

		if (derivedContainmentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedContainmentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedContainmentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPropertyKindAsString() {
		/**
		 * @OCL let notStorable:Boolean =
		if self.property.oclIsUndefined()=false
		then not (self.property.hasStorage=true) else false endif in
		if notStorable
		then  'ERROR: PROPERTY NOT STORABLE'
		else  if self.derivedKind=PropertyKind::Attribute 
		then 'Attribute'
		else if self.derivedKind=PropertyKind::Reference
		then if self.derivedContainment 
		then 'Containment'
		else  'Reference' endif
		else if self.derivedKind=PropertyKind::Operation
		then 'ERROR:PROPERTY IS OPERATION' else if self.derivedKind=PropertyKind::Ambiguous
		then 'ERROR:VALUES AMBIGUOUS' 
		else if self.derivedKind = PropertyKind::TypeMissing
		then 'Untyped Property'
		else 'ERROR: SET VALUE OR PROPERTY' 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING;

		if (propertyKindAsStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				propertyKindAsStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(propertyKindAsStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectPropertyKind() {
		/**
		 * @OCL self.derivedKind=PropertyKind::Attribute 
		or self.derivedKind=PropertyKind::Reference
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND;

		if (correctPropertyKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				correctPropertyKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctPropertyKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstanceAction getDoAction() {
		/**
		 * @OCL let do: mcore::objects::MPropertyInstanceAction = mcore::objects::MPropertyInstanceAction::Do in
		do
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyInstanceAction result = (MPropertyInstanceAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MPropertyInstanceAction newDoAction) {
		MDataValue dv = null;
		switch (newDoAction.getValue()) {
		case MPropertyInstanceAction.CLEAR_VALUES_VALUE:
			getInternalContainedObject().clear();
			getInternalDataValue().clear();
			getInternalLiteralValue().clear();
			getInternalReferencedObject().clear();
			break;
		case MPropertyInstanceAction.INTO_UNARY_VALUE:
			if (getProperty() != null) {
				getProperty().setSingular(true);
			}
			break;
		case MPropertyInstanceAction.INTO_NARY_VALUE:
			if (getProperty() != null) {
				getProperty().setSingular(false);
			}
			break;
		case MPropertyInstanceAction.INTO_CONTAINMENT_VALUE:
			if (getProperty() != null
					&& !getProperty().getHasSimpleDataType()) {
				getProperty().setPropertyBehavior(PropertyBehavior.CONTAINED);
				//the following is in principle not needed, due to default containment behavior.
				for (MObjectReference mObjectRef : getInternalReferencedObject()) {
					MObject obj = mObjectRef.getReferencedObject();
					if (obj != null) {
						getInternalContainedObject().add(obj);
					}
				}
				getInternalReferencedObject().clear();
			} else if (this.getDerivedKind() == PropertyKind.REFERENCE
					&& this.getDerivedContainment() == false) {
				for (MObjectReference mObjectRef : getInternalReferencedObject()) {
					MObject obj = mObjectRef.getReferencedObject();
					if (obj != null) {
						getInternalContainedObject().add(obj);
					}
				}
				getInternalReferencedObject().clear();
			}
			break;
		case MPropertyInstanceAction.ADD_PROPERTY_TO_TYPE_VALUE:
			if (this.getProperty() == null
					&& this.getContainingObject().getType() != null) {
				MClassifier classToBeExtended = this.getContainingObject()
						.getType();
				if (classToBeExtended.getKind() == ClassifierKind.CLASS_TYPE) {
					MProperty newProperty = McoreFactory.eINSTANCE
							.createMProperty();
					classToBeExtended.getProperty().add(newProperty);
					newProperty.setName(this.getLocalKey());
					//setting type based on derived kind needs to be done BEFORE setting property of slot!
					switch (this.getDerivedKind().getValue()) {
					case PropertyKind.ATTRIBUTE_VALUE:
						if (this.getInternalLiteralValue().isEmpty()) {
							newProperty.setSimpleType(
									this.getSimpleTypeFromValues());
							if (this.getInternalDataValue().size() > 1) {
								newProperty.setSingular(false);
							} else {
								newProperty.setSingular(false);
							}
						} else {
							/*TODO Derive literal enumeration from literal values */
							if (!this.getInternalLiteralValue().isEmpty()) {
								if (this.getInternalLiteralValue().get(0)
										.getLiteralValue() != null) {
									newProperty.setTypeDefinition(this
											.getInternalLiteralValue().get(0)
											.getLiteralValue()
											.getContainingEnumeration());
								}
							}
							if (this.getInternalLiteralValue().size() > 1) {
								newProperty.setSingular(false);
							} else {
								newProperty.setSingular(false);
							}
						}
						newProperty
								.setPropertyBehavior(PropertyBehavior.STORED);
						break;
					case PropertyKind.REFERENCE_VALUE:
						if (this.getKindLabel().equals("Reference")) {
							MObject o = null;
							if (!this.getInternalReferencedObject().isEmpty()) {
								o = this.getInternalReferencedObject().get(0)
										.getReferencedObject();
								//Type has to be set before behavior is set.
								if (o != null && o.getType() != null) {
									newProperty.setTypeDefinition(o.getType());
								}
								newProperty.setPropertyBehavior(
										PropertyBehavior.STORED);
								if (this.getInternalReferencedObject()
										.size() > 1) {
									newProperty.setSingular(false);
								} else {
									newProperty.setSingular(false);
								}
							} else if (!this.getInternalContainedObject()
									.isEmpty()) {
								o = this.getInternalContainedObject().get(0);
								//Type has to be set before behavior is set
								if (o != null && o.getType() != null) {
									newProperty.setTypeDefinition(o.getType());
								}
								//setting singular of new property may in the future influence
								//the default containment logic. We may only remove default containment for adding n-ary
								//containments. Thus we set it first.
								if (this.getInternalContainedObject()
										.size() > 1) {
									newProperty.setSingular(false);
								} else {
									newProperty.setSingular(false);
								}
								newProperty.setPropertyBehavior(
										PropertyBehavior.CONTAINED);
								//this could have deleted a "default containment", and created 
								//a new property slot for newProperty in this object, in which some orpheline objects where moved
								//->that new slot has to be removed, and objects in it have to be moved to this slots contained objects.
								for (MPropertyInstance additionalNewSlot : this
										.getContainingObject()
										.getPropertyInstance()) {
									if (additionalNewSlot
											.getProperty() == newProperty) {
										this.getInternalContainedObject()
												.addAll(additionalNewSlot
														.getInternalContainedObject());
										this.getContainingObject()
												.getPropertyInstance()
												.remove(additionalNewSlot);
										break;
									}
								}
							}
						}
						//Its a containment and it could be necessary to add a new Class with its properties
						else {
							MObject o = null;
							if (!this.getInternalReferencedObject().isEmpty()) {
								o = this.getInternalReferencedObject().get(0)
										.getReferencedObject();
							} else {
								if (!this.getInternalContainedObject()
										.isEmpty()) {
									o = this.getInternalContainedObject()
											.get(0);
								}
							}
							if (o != null) {
								if (o.getType() != null) {
									newProperty.setTypeDefinition(o.getType());
								} else {
									MComponent mComp = this
											.getContainingObject()
											.getTypePackage()
											.getContainingComponent();
									MClassifier mClass = McoreFactory.eINSTANCE
											.createMClassifier();
									mClass.setName(
											o.getClassNameOrNatureDefinition());
									mComp.getRootPackage().getClassifier()
											.add(mClass);
									o.setType(mClass);
									newProperty.setTypeDefinition(mClass);
									if (o.getPropertyInstance().isEmpty() || o
											.getPropertyInstance() == null) {
										MProperty mProp = McoreFactory.eINSTANCE
												.createMProperty();
										mProp.setName("Text 1");
										mProp.setSimpleType(SimpleType.STRING);
										mClass.getProperty().add(mProp);
										mProp.setPropertyBehavior(
												PropertyBehavior.STORED);
										mProp.setMultiplicityCase(
												MultiplicityCase.ZERO_ONE);
									} else {
										for (MPropertyInstance mIn : o
												.getPropertyInstance()) {
											MProperty mProp = McoreFactory.eINSTANCE
													.createMProperty();
											mProp.setName(mIn
													.getPropertyNameOrLocalKeyDefinition());
											mProp.setSimpleType(mIn
													.getSimpleTypeFromValues());
											mClass.getProperty().add(mProp);
											mProp.setPropertyBehavior(
													PropertyBehavior.STORED);
											mProp.setMultiplicityCase(
													MultiplicityCase.ZERO_ONE);
											mIn.setProperty(mProp);
										}
										newProperty.setPropertyBehavior(
												PropertyBehavior.CONTAINED);
										if (this.getInternalReferencedObject()
												.size() > 1) {
											newProperty.setSingular(false);
										} else {
											newProperty.setSingular(false);
										}

										//Saves the data value and adds them to the new Property Instance
										for (MPropertyInstance mIn : o
												.getPropertyInstance()) {
											for (MObject mOb : mClass
													.getPropertyInstances()
													.get(0)
													.getInternalContainedObject()) {
												for (MPropertyInstance mNewInProp : mOb
														.getPropertyInstance()) {
													if (mNewInProp.getProperty()
															.equals(mIn
																	.getProperty())) {
														mNewInProp
																.getInternalDataValue()
																.clear();
														mNewInProp
																.getInternalDataValue()
																.addAll(mIn
																		.getInternalDataValue());
													}
												}
											}
										}
									}
								}
							}
						}
						break;
					default:
						break;
					}
					this.setProperty(newProperty);

				}
			}
			break;
		case MPropertyInstanceAction.STRING_VALUE_VALUE:
			dv = ObjectsFactory.eINSTANCE.createMDataValue();
			this.getInternalDataValue().add(dv);
			dv.setDataValue("ENTER VALUE");
			break;
		case MPropertyInstanceAction.BOOLEAN_VALUE_VALUE:
			dv = ObjectsFactory.eINSTANCE.createMDataValue();
			this.getInternalDataValue().add(dv);
			dv.setDataValue("false");
			break;
		case MPropertyInstanceAction.INTEGER_VALUE_VALUE:
			dv = ObjectsFactory.eINSTANCE.createMDataValue();
			this.getInternalDataValue().add(dv);
			dv.setDataValue("0");
			break;
		case MPropertyInstanceAction.REAL_VALUE_VALUE:
			dv = ObjectsFactory.eINSTANCE.createMDataValue();
			this.getInternalDataValue().add(dv);
			dv.setDataValue("3.5");
			break;
		case MPropertyInstanceAction.DATE_VALUE_VALUE:
			dv = ObjectsFactory.eINSTANCE.createMDataValue();
			this.getInternalDataValue().add(dv);
			dv.setDataValue("1983-02-14T06:55:19.000+0200");
			break;
		case MPropertyInstanceAction.LITERAL_VALUE_VALUE:
			MLiteralValue lv = ObjectsFactory.eINSTANCE.createMLiteralValue();
			this.getInternalLiteralValue().add(lv);
			break;
		case MPropertyInstanceAction.REFERENCED_OBJECT_VALUE_VALUE:
			MObjectReference or = ObjectsFactory.eINSTANCE
					.createMObjectReference();
			this.getInternalReferencedObject().add(or);
			break;
		case MPropertyInstanceAction.CONTAINED_OBJECT_VALUE:
			if (getProperty() != null && getProperty().getType() != null
					&& getProperty().getType().getAbstractClass())
				break;
			MObject newO = ObjectsFactory.eINSTANCE.createMObject();
			this.getInternalContainedObject().add(newO);
			if (getProperty() == null)
				newO.setNature(
						"Class" + this.getInternalContainedObject().size());

			if (getProperty() != null && this.getProperty().getSingular()
					&& getInternalContainedObject().size() > 1)
				getProperty().setDoAction(MPropertyAction.INTO_NARY);
			break;
		case MPropertyInstanceAction.CLASS_WITH_STRING_ATTRIBUTE_VALUE:
			if (getProperty() != null) {
				getProperty().setDoAction(
						MPropertyAction.CLASS_WITH_STRING_PROPERTY);
			} else {
				MPropertyInstance newPropIns = ObjectsFactory.eINSTANCE
						.createMPropertyInstance();
				MObject newContainedObj = ObjectsFactory.eINSTANCE
						.createMObject();
				newPropIns.getInternalContainedObject().add(newContainedObj);
				newPropIns.setPropertyNameOrLocalKeyDefinition(
						this.getPropertyNameOrLocalKeyDefinition() + " Of "
								+ this.getContainingObject()
										.getClassNameOrNatureDefinition());
				this.getContainingObject().getPropertyInstance()
						.add(newPropIns);
				newContainedObj.getPropertyInstance().add(this);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MPropertyInstanceAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let onlyDataValues:Boolean = internalLiteralValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in
	let onlyLiteralValues:Boolean = internalDataValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in
	let onlyReferencedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalContainedObject->isEmpty()  in
	let onlyContainedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalReferencedObject->isEmpty()   in
	
	let start:OrderedSet(MPropertyInstanceAction) = OrderedSet{MPropertyInstanceAction::Do} in start->append(
	if containingObject.type.oclIsUndefined() then null else
	if property.oclIsUndefined() then  
	    MPropertyInstanceAction::AddPropertyToType else
	if containingObject.type.allFeaturesWithStorage()->excludes(property) then
	    MPropertyInstanceAction::AddPropertyToType else null endif endif endif)->append(
	if not property.oclIsUndefined() then 
	if property.calculatedSingular then
	    MPropertyInstanceAction::IntoNary else MPropertyInstanceAction::IntoUnary endif else null endif)->append(
	if property.oclIsUndefined() then 
	 if self.derivedKind = PropertyKind::Reference and self.derivedContainment = false then     
	       MPropertyInstanceAction::IntoContainment else null endif
	else if property.kind = PropertyKind::Reference and property.containment = false then 
	       MPropertyInstanceAction::IntoContainment else null endif endif)->append(
	if ((not property.oclIsUndefined()) and property.calculatedSimpleType = SimpleType::String) or self.simpleTypeFromValues = SimpleType::String
	then MPropertyInstanceAction::ClassWithStringAttribute
	else null endif)->append(
	if property.oclIsUndefined() then 
	if onlyDataValues then
	    MPropertyInstanceAction::StringValue else null endif else
	if property.calculatedSimpleType = SimpleType::String then
	    if (not property.singular) or internalDataValue->isEmpty() then 
	    MPropertyInstanceAction::StringValue else null endif else null endif endif)->append(
	if property.oclIsUndefined() then 
	if onlyDataValues then
	    MPropertyInstanceAction::BooleanValue else null endif else
	if property.calculatedSimpleType = SimpleType::Boolean then 
	    if (not property.singular) or internalDataValue->isEmpty() then 
	    MPropertyInstanceAction::BooleanValue else null endif else null endif endif)->append(
	if property.oclIsUndefined() then 
	if onlyDataValues then
	    MPropertyInstanceAction::IntegerValue else null endif else
	if property.calculatedSimpleType = SimpleType::Integer then 
	    if (not property.singular) or internalDataValue->isEmpty() then 
	    MPropertyInstanceAction::IntegerValue else null endif else null endif endif)->append(
	if property.oclIsUndefined() then 
	if onlyDataValues then
	    MPropertyInstanceAction::RealValue else null endif else
	if property.calculatedSimpleType = SimpleType::Double then 
	    if (not property.singular) or internalDataValue->isEmpty() then 
	    MPropertyInstanceAction::RealValue else null endif else null endif endif)->append(
	if property.oclIsUndefined() then 
	if onlyDataValues then
	    MPropertyInstanceAction::DateValue else null endif else
	if property.calculatedSimpleType = SimpleType::Date then 
	     if (not property.singular) or internalDataValue->isEmpty() then 
	    MPropertyInstanceAction::DateValue else null endif else null endif endif)->append(
	if property.oclIsUndefined() then 
	if onlyReferencedObjects then
	    MPropertyInstanceAction::ReferencedObjectValue else null endif else
	if property.kind = PropertyKind::Reference and property.containment = false then 
	     if (not property.singular) or internalReferencedObject->isEmpty() then 
	    MPropertyInstanceAction::ReferencedObjectValue else null endif else null endif endif)->append(
	if property.oclIsUndefined() then 
	if onlyContainedObjects then
	    MPropertyInstanceAction::ContainedObject else null endif else
	if property.kind = PropertyKind::Reference and property.containment = true then 
	    MPropertyInstanceAction::ContainedObject else null endif endif)->append(
	    if internalDataValue->notEmpty() or internalLiteralValue->notEmpty() or internalReferencedObject->notEmpty() or internalContainedObject->notEmpty() then 
	MPropertyInstanceAction::ClearValues else null endif)->excluding(null)
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MPropertyInstanceAction> evalDoActionChoiceConstruction(
			List<MPropertyInstanceAction> choice) {
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MPropertyInstanceAction> result = new ArrayList<MPropertyInstanceAction>(
					(Collection<MPropertyInstanceAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getDoAddValue() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DO_ADD_VALUE;

		if (doAddValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doAddValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doAddValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAddValue(Boolean newDoAddValue) {
		if (this.getProperty() != null) {
			if (getProperty().getCalculatedSimpleType() == SimpleType.STRING) {
				this.setDoAction(MPropertyInstanceAction.STRING_VALUE);
			} else if (getProperty()
					.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
				this.setDoAction(MPropertyInstanceAction.BOOLEAN_VALUE);
			} else if (getProperty()
					.getCalculatedSimpleType() == SimpleType.INTEGER) {
				this.setDoAction(MPropertyInstanceAction.INTEGER_VALUE);
			} else if (getProperty()
					.getCalculatedSimpleType() == SimpleType.DOUBLE) {
				this.setDoAction(MPropertyInstanceAction.REAL_VALUE);
			} else if (getProperty()
					.getCalculatedSimpleType() == SimpleType.DATE) {
				this.setDoAction(MPropertyInstanceAction.DATE_VALUE);
			} else if (getProperty().getType() != null
					&& getProperty().getType()
							.getKind() == ClassifierKind.ENUMERATION
					&& getProperty().getContainment() == false) {
				this.setDoAction(MPropertyInstanceAction.LITERAL_VALUE);
			} else if (getProperty().getType() != null
					&& getProperty().getContainment() == true) {
				this.setDoAction(MPropertyInstanceAction.CONTAINED_OBJECT);
			} else if (getProperty().getType() != null) {
				this.setDoAction(
						MPropertyInstanceAction.REFERENCED_OBJECT_VALUE);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SimpleType getSimpleTypeFromValues() {

		if (!this.getInternalLiteralValue().isEmpty()
				|| !this.getInternalReferencedObject().isEmpty()
				|| !this.getInternalContainedObject().isEmpty()
				|| this.getInternalDataValue().isEmpty()) {
			return SimpleType.NONE;
		} else {
			SimpleType s = null;
			for (Iterator iterator = internalDataValue.iterator(); iterator
					.hasNext();) {
				MDataValue mDataValue = (MDataValue) iterator.next();
				Integer i = null;
				try {
					i = Integer.valueOf(mDataValue.getDataValue());
				} catch (NumberFormatException e) {
					// TODO: handle exception
				}
				if (i != null) {
					if (s == null || s == SimpleType.INTEGER) {
						s = SimpleType.INTEGER;
					} else {
						if (s != SimpleType.DOUBLE) {
							s = SimpleType.STRING;
							break;
						}
					}
				} else {
					Double d = null;
					try {
						d = Double.valueOf(mDataValue.getDataValue());
					} catch (NumberFormatException e) {
						// TODO: handle exception
					}
					if (d != null) {
						if (s == null || s == SimpleType.INTEGER
								|| s == SimpleType.DOUBLE) {
							s = SimpleType.DOUBLE;
						} else {
							s = SimpleType.STRING;
							break;
						}
					} else {
						Boolean b = null;
						//somehow did not work:
						//						try {
						//							b = Boolean.valueOf(mDataValue.getDataValue());
						//						} catch (NumberFormatException e) {
						//							// TODO: handle exception
						//						}
						if (mDataValue.getDataValue()
								.equalsIgnoreCase("True")) {
							b = true;
						} else if (mDataValue.getDataValue()
								.equalsIgnoreCase("False")) {
							b = false;
						}
						if (b != null) {
							if (s == null || s == SimpleType.BOOLEAN) {
								s = SimpleType.BOOLEAN;
							} else {
								s = SimpleType.STRING;
								break;
							}
						} else {
							/* TODO no idea how to parse dates...Date d = null;
							 
							try {
								d = Date.
							} catch (NumberFormatException e) {
								// TODO: handle exception
							}
							if (b != null) {
								if (s == null || s == SimpleType.BOOLEAN) {
									s = SimpleType.BOOLEAN;
								} else {
									s = SimpleType.STRING;
									break;
								}
							} else {
								Date d = null;
							}*/
							s = SimpleType.STRING;
							break;
						}
					}
				}

			}
			if (s == null) {
				s = SimpleType.STRING;
			}
			return s;
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getEnumerationFromValues() {
		MClassifier enumerationFromValues = basicGetEnumerationFromValues();
		return enumerationFromValues != null && enumerationFromValues.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) enumerationFromValues)
				: enumerationFromValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetEnumerationFromValues() {
		/**
		 * @OCL /*Overwritten in Java *\/ null
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES;

		if (enumerationFromValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				enumerationFromValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(enumerationFromValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate propertyNameOrLocalKeyDefinition$Update(String trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		java.lang.String triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE
						.getMPropertyInstance_PropertyNameOrLocalKeyDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MPropertyInstanceAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.objects.MPropertyInstanceAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE.getMPropertyInstance_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAddValue$Update(Boolean trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		java.lang.Boolean triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE.getMPropertyInstance_DoAddValue(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctProperty() {

		/**
		 * @OCL let t:MClassifier=self.containingObject.type in
		let p:MProperty=self.property in
		if t.oclIsUndefined() or p.oclIsUndefined() then false
		else t.kind=ClassifierKind::ClassType and t.allProperties()->includes(p) 
		and not(p.kind = PropertyKind::Operation) endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(3);
		if (correctPropertyBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctPropertyBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctPropertyBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctMultiplicity() {

		/**
		 * @OCL let p:MProperty=self.property in
		if p.oclIsUndefined() then false
		else if p.kind = PropertyKind::Attribute then
		self.internalContainedObject->size()=0 
		and
		(if p.singular then self.internalDataValue->size() <2 else
		if p.mandatory then self.internalDataValue->size() > 0 else true endif endif)
		and 
		self.internalReferencedObject->size()=0
		else if p.kind = PropertyKind::Reference then 
		if p.containment = false then
		self.internalContainedObject->size()=0 
		and
		self.internalDataValue->size() = 0 
		and 
		(if p.singular then self.internalReferencedObject->size() <2 else
		if p.mandatory then self.internalReferencedObject->size() > 0 else true 
		endif   endif)
		else
		(if p.singular then self.internalContainedObject->size() <2 else
		if p.mandatory then self.internalContainedObject->size() > 0 else true 
		endif   endif)
		and
		self.internalDataValue->size() = 0 
		and 
		self.internalReferencedObject->size() = 0
		endif
		else false
		endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(4);
		if (correctMultiplicityBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctMultiplicityBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctMultiplicityBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String propertyValueAsString() {

		/**
		 * @OCL if self.property.oclIsUndefined() then '' else
		if self.property.kind=mcore::PropertyKind::Reference then
		self.internalReferencedObjectsAsString()
		else
		if self.property.kind=mcore::PropertyKind::Attribute then
		if self.property.type.oclIsUndefined() then
		if not (self.property.simpleType =SimpleType::None) then 
		self.internalDataValuesAsString() else 'ERROR3' endif
		else      
		if self.property.type.kind=mcore::ClassifierKind::DataType then
		self.internalDataValuesAsString() else
		if self.property.type.kind=mcore::ClassifierKind::Enumeration then
		 self.internalLiteralValuesAsString() else
		   'ERROR1' endif endif endif
		else 'ERROR2' 
		endif 
		endif endif
		/*
		if self.type.oclIsUndefined() then '' else
		if self.type.idProperty->isEmpty() then '' else
		self.type.idProperty->iterate(p:mcore::MProperty;res:String=''|
		(if res='' then '' else res.concat('|') endif).concat(self.propertyValueAsString(p)))
		endif endif*\/
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(5);
		if (propertyValueAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				propertyValueAsStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(propertyValueAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String internalReferencedObjectsAsString() {

		/**
		 * @OCL let vs:OrderedSet(objects::MObjectReference) = self.internalReferencedObject in
		if vs->isEmpty() then '' else
		vs->iterate(r:objects::MObjectReference;res:String=''|
		(if res='' then '' else res.concat(',') endif)
		  .concat(if r.referencedObject.oclIsUndefined() then 
		    'OBJECT MISSING' else 
		    if r.referencedObject=self.containingObject then 'self' else
		    let c:mcore::MClassifier=r.typeOfReferencedObject in 
		    if c.oclIsUndefined() then r.referencedObject.id else
		      if c.allIdProperties()->isEmpty() then
		        r.referencedObject.id 
		      else if c.allIdProperties()->size()>1 then
		       '('.concat(r.referencedObject.id ).concat(')')
		       else  r.referencedObject.id
		      endif  endif endif endif endif))
		endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(6);
		if (internalReferencedObjectsAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				internalReferencedObjectsAsStringBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(internalReferencedObjectsAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String internalDataValuesAsString() {

		/**
		 * @OCL let vs:OrderedSet(objects::MDataValue) = self.internalDataValue in
		if vs->isEmpty() then '' else
		vs->iterate(r:objects::MDataValue;res:String=''|
		(if res='' then '' else res.concat(',') endif)
		  .concat(if r.dataValue.oclIsUndefined() then 
		                   'VALUE MISSING' else r.dataValue endif))
		endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(7);
		if (internalDataValuesAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				internalDataValuesAsStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(internalDataValuesAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String internalLiteralValuesAsString() {

		/**
		 * @OCL let vs:OrderedSet(objects::MLiteralValue) = self.internalLiteralValue in
		if vs->isEmpty() then '' else
		vs->iterate(r:objects::MLiteralValue;res:String=''|
		(if res='' then '' else res.concat(',') endif)
		  .concat(if r.literalValue.oclIsUndefined() then 
		                    'LITERAL MISSING' else 
		                    r.literalValue.eName endif))
		endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(8);
		if (internalLiteralValuesAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				internalLiteralValuesAsStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(internalLiteralValuesAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> choosableProperties() {

		/**
		 * @OCL /*see as well allFeaturesWithStorage of type.*\/
		let t:MClassifier =containingObject.type in
		if t.oclIsUndefined() 
		then if self.property.oclIsUndefined() then OrderedSet{} else self.property->asOrderedSet() endif
		else
		containingObject.type.allProperties()->
		reject(p:MProperty| 
		p.kind=PropertyKind::Operation or 
		not( p.hasStorage=true) or
		(not (p = self.property) and self.containingObject.propertyInstance.property->includes(p)))
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EOperation eOperation = ObjectsPackage.Literals.MPROPERTY_INSTANCE
				.getEOperations().get(9);
		if (choosablePropertiesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				choosablePropertiesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(choosablePropertiesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate propertyNameOrLocalKeyDefinitionUpdate(
			String propertyNameOrLocalKeyDefinition) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		XUpdate currentTrigger = null;

		if (getProperty() == null) {
			currentTrigger = transition.addAttributeUpdate(this,
					ObjectsPackage.eINSTANCE.getMPropertyInstance_LocalKey(),
					XUpdateMode.REDEFINE, null,
					//:=
					propertyNameOrLocalKeyDefinition, null, null);
		} else {
			String sn = getProperty().getShortName();
			if ((sn == null) || sn.trim().equals("")) {
				currentTrigger = transition.addAttributeUpdate(getProperty(),
						McorePackage.Literals.MNAMED__NAME,
						XUpdateMode.REDEFINE, null,
						//:=
						propertyNameOrLocalKeyDefinition, null, null);
			} else {
				currentTrigger = transition.addAttributeUpdate(getProperty(),
						McorePackage.Literals.MNAMED__SHORT_NAME,
						XUpdateMode.REDEFINE, null,
						//:=
						propertyNameOrLocalKeyDefinition, null, null);
			}

		}
		return currentTrigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE:
			return ((InternalEList<?>) getInternalDataValue())
					.basicRemove(otherEnd, msgs);
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE:
			return ((InternalEList<?>) getInternalLiteralValue())
					.basicRemove(otherEnd, msgs);
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT:
			return ((InternalEList<?>) getInternalReferencedObject())
					.basicRemove(otherEnd, msgs);
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT:
			return ((InternalEList<?>) getInternalContainedObject())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE:
			return getInternalDataValue();
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE:
			return getInternalLiteralValue();
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT:
			return getInternalReferencedObject();
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT:
			return getInternalContainedObject();
		case ObjectsPackage.MPROPERTY_INSTANCE__CONTAINING_OBJECT:
			if (resolve)
				return getContainingObject();
			return basicGetContainingObject();
		case ObjectsPackage.MPROPERTY_INSTANCE__DUPLICATE_INSTANCE:
			return getDuplicateInstance();
		case ObjectsPackage.MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY:
			return getAllInstancesOfThisProperty();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND:
			return getWrongInternalContainedObjectDueToKind();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND:
			return getWrongInternalObjectReferenceDueToKind();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND:
			return getWrongInternalLiteralValueDueToKind();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND:
			return getWrongInternalDataValueDueToKind();
		case ObjectsPackage.MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE:
			return getNumericIdOfPropertyInstance();
		case ObjectsPackage.MPROPERTY_INSTANCE__DERIVED_CONTAINMENT:
			return getDerivedContainment();
		case ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY:
			return getLocalKey();
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY:
			if (resolve)
				return getProperty();
			return basicGetProperty();
		case ObjectsPackage.MPROPERTY_INSTANCE__KEY:
			return getKey();
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION:
			return getPropertyNameOrLocalKeyDefinition();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_LOCAL_KEY:
			return getWrongLocalKey();
		case ObjectsPackage.MPROPERTY_INSTANCE__DERIVED_KIND:
			return getDerivedKind();
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING:
			return getPropertyKindAsString();
		case ObjectsPackage.MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND:
			return getCorrectPropertyKind();
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ACTION:
			return getDoAction();
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ADD_VALUE:
			return getDoAddValue();
		case ObjectsPackage.MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES:
			return getSimpleTypeFromValues();
		case ObjectsPackage.MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES:
			if (resolve)
				return getEnumerationFromValues();
			return basicGetEnumerationFromValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE:
			getInternalDataValue().clear();
			getInternalDataValue()
					.addAll((Collection<? extends MDataValue>) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE:
			getInternalLiteralValue().clear();
			getInternalLiteralValue()
					.addAll((Collection<? extends MLiteralValue>) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT:
			getInternalReferencedObject().clear();
			getInternalReferencedObject()
					.addAll((Collection<? extends MObjectReference>) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT:
			getInternalContainedObject().clear();
			getInternalContainedObject()
					.addAll((Collection<? extends MObject>) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY:
			setLocalKey((String) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY:
			setProperty((MProperty) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION:
			setPropertyNameOrLocalKeyDefinition((String) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ACTION:
			setDoAction((MPropertyInstanceAction) newValue);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ADD_VALUE:
			setDoAddValue((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE:
			unsetInternalDataValue();
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE:
			unsetInternalLiteralValue();
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT:
			unsetInternalReferencedObject();
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT:
			unsetInternalContainedObject();
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY:
			unsetLocalKey();
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY:
			unsetProperty();
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION:
			setPropertyNameOrLocalKeyDefinition(
					PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_EDEFAULT);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ADD_VALUE:
			setDoAddValue(DO_ADD_VALUE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE:
			return isSetInternalDataValue();
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE:
			return isSetInternalLiteralValue();
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT:
			return isSetInternalReferencedObject();
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT:
			return isSetInternalContainedObject();
		case ObjectsPackage.MPROPERTY_INSTANCE__CONTAINING_OBJECT:
			return basicGetContainingObject() != null;
		case ObjectsPackage.MPROPERTY_INSTANCE__DUPLICATE_INSTANCE:
			return DUPLICATE_INSTANCE_EDEFAULT == null
					? getDuplicateInstance() != null
					: !DUPLICATE_INSTANCE_EDEFAULT
							.equals(getDuplicateInstance());
		case ObjectsPackage.MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY:
			return !getAllInstancesOfThisProperty().isEmpty();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND:
			return !getWrongInternalContainedObjectDueToKind().isEmpty();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND:
			return !getWrongInternalObjectReferenceDueToKind().isEmpty();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND:
			return !getWrongInternalLiteralValueDueToKind().isEmpty();
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND:
			return !getWrongInternalDataValueDueToKind().isEmpty();
		case ObjectsPackage.MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE:
			return NUMERIC_ID_OF_PROPERTY_INSTANCE_EDEFAULT == null
					? getNumericIdOfPropertyInstance() != null
					: !NUMERIC_ID_OF_PROPERTY_INSTANCE_EDEFAULT
							.equals(getNumericIdOfPropertyInstance());
		case ObjectsPackage.MPROPERTY_INSTANCE__DERIVED_CONTAINMENT:
			return DERIVED_CONTAINMENT_EDEFAULT == null
					? getDerivedContainment() != null
					: !DERIVED_CONTAINMENT_EDEFAULT
							.equals(getDerivedContainment());
		case ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY:
			return isSetLocalKey();
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY:
			return isSetProperty();
		case ObjectsPackage.MPROPERTY_INSTANCE__KEY:
			return KEY_EDEFAULT == null ? getKey() != null
					: !KEY_EDEFAULT.equals(getKey());
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION:
			return PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_EDEFAULT == null
					? getPropertyNameOrLocalKeyDefinition() != null
					: !PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_EDEFAULT
							.equals(getPropertyNameOrLocalKeyDefinition());
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_LOCAL_KEY:
			return WRONG_LOCAL_KEY_EDEFAULT == null ? getWrongLocalKey() != null
					: !WRONG_LOCAL_KEY_EDEFAULT.equals(getWrongLocalKey());
		case ObjectsPackage.MPROPERTY_INSTANCE__DERIVED_KIND:
			return getDerivedKind() != DERIVED_KIND_EDEFAULT;
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING:
			return PROPERTY_KIND_AS_STRING_EDEFAULT == null
					? getPropertyKindAsString() != null
					: !PROPERTY_KIND_AS_STRING_EDEFAULT
							.equals(getPropertyKindAsString());
		case ObjectsPackage.MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND:
			return CORRECT_PROPERTY_KIND_EDEFAULT == null
					? getCorrectPropertyKind() != null
					: !CORRECT_PROPERTY_KIND_EDEFAULT
							.equals(getCorrectPropertyKind());
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ADD_VALUE:
			return DO_ADD_VALUE_EDEFAULT == null ? getDoAddValue() != null
					: !DO_ADD_VALUE_EDEFAULT.equals(getDoAddValue());
		case ObjectsPackage.MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES:
			return getSimpleTypeFromValues() != SIMPLE_TYPE_FROM_VALUES_EDEFAULT;
		case ObjectsPackage.MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES:
			return basicGetEnumerationFromValues() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION$_UPDATE__STRING:
			return propertyNameOrLocalKeyDefinition$Update(
					(String) arguments.get(0));
		case ObjectsPackage.MPROPERTY_INSTANCE___DO_ACTION$_UPDATE__MPROPERTYINSTANCEACTION:
			return doAction$Update((MPropertyInstanceAction) arguments.get(0));
		case ObjectsPackage.MPROPERTY_INSTANCE___DO_ADD_VALUE$_UPDATE__BOOLEAN:
			return doAddValue$Update((Boolean) arguments.get(0));
		case ObjectsPackage.MPROPERTY_INSTANCE___CORRECT_PROPERTY:
			return correctProperty();
		case ObjectsPackage.MPROPERTY_INSTANCE___CORRECT_MULTIPLICITY:
			return correctMultiplicity();
		case ObjectsPackage.MPROPERTY_INSTANCE___PROPERTY_VALUE_AS_STRING:
			return propertyValueAsString();
		case ObjectsPackage.MPROPERTY_INSTANCE___INTERNAL_REFERENCED_OBJECTS_AS_STRING:
			return internalReferencedObjectsAsString();
		case ObjectsPackage.MPROPERTY_INSTANCE___INTERNAL_DATA_VALUES_AS_STRING:
			return internalDataValuesAsString();
		case ObjectsPackage.MPROPERTY_INSTANCE___INTERNAL_LITERAL_VALUES_AS_STRING:
			return internalLiteralValuesAsString();
		case ObjectsPackage.MPROPERTY_INSTANCE___CHOOSABLE_PROPERTIES:
			return choosableProperties();
		case ObjectsPackage.MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_UPDATE__STRING:
			return propertyNameOrLocalKeyDefinitionUpdate(
					(String) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (localKey: ");
		if (localKeyESet)
			result.append(localKey);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if self.derivedContainment
	/*then  '<...'.concat('').concat('('.concat(self.internalContainedObject->size().toString())).concat(' ').concat(self.key).concat('(s))...>') *\/
	then  '<'.concat(self.key).concat('>') 
	else self.key.concat('="').concat(
	if self.derivedKind=mcore::PropertyKind::Attribute 
	then
	 if self.property.oclIsUndefined() 
	   then  self.internalDataValuesAsString()
	 else if self.property.type.oclIsUndefined() 
	   then  self.internalDataValuesAsString()  
	   else  if self.property.type.kind=mcore::ClassifierKind::DataType 
	       then  self.internalDataValuesAsString()    
	       else self.internalLiteralValuesAsString() endif endif endif
	else self.internalReferencedObjectsAsString() endif 
	.concat('"')
	)
	endif
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ObjectsPackage.Literals.MPROPERTY_INSTANCE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel propertyKindAsString
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ObjectsPackage.Literals.MPROPERTY_INSTANCE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE,
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE,
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT,
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT,
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MPROPERTY_INSTANCE,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}

} //MPropertyInstanceImpl
