/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MInitialization Order</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMInitializationOrder()
 * @model annotation="http://www.xocl.org/OCL label='let prefix: String = \'init order =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Init Order\'\n'"
 * @generated
 */

public interface MInitializationOrder extends MExprAnnotation {
} // MInitializationOrder
