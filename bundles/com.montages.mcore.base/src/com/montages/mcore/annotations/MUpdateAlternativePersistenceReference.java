
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate Alternative Persistence Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdateAlternativePersistenceReference()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Alternative Persistence Reference\'\n'"
 * @generated
 */

public interface MUpdateAlternativePersistenceReference
		extends MUpdatePersistence {

} // MUpdateAlternativePersistenceReference
