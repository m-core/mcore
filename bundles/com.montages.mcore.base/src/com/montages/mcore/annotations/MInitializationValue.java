/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MInitialization Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMInitializationValue()
 * @model annotation="http://www.xocl.org/OCL label='let prefix: String = \'init value =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Init Value\'\n'"
 * @generated
 */

public interface MInitializationValue extends MExprAnnotation {
} // MInitializationValue
