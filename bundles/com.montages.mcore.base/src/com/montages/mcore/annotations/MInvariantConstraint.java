/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MNamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MInvariant Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MInvariantConstraint#getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMInvariantConstraint()
 * @model annotation="http://www.xocl.org/OCL label='(if self.containingClassifierAnnotations.constraint->indexOf(self)=1\r\n  then \'constraint(s): \' else \'                    \' endif)\r\n  .concat(self.eName)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Constraint\'\n' expectedReturnTypeOfAnnotationDerive='null' expectedReturnSimpleTypeOfAnnotationDerive='SimpleType::Boolean' isReturnValueOfAnnotationMandatoryDerive='true' isReturnValueOfAnnotationSingularDerive='true'"
 * @generated
 */

public interface MInvariantConstraint extends MExprAnnotation, MNamed {
	/**
	 * Returns the value of the '<em><b>Containing Classifier Annotations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Classifier Annotations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Classifier Annotations</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMInvariantConstraint_ContainingClassifierAnnotations()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(MClassifierAnnotations)'"
	 * @generated
	 */
	MClassifierAnnotations getContainingClassifierAnnotations();

} // MInvariantConstraint
