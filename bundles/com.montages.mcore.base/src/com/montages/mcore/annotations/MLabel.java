/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLabel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MLabel#getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMLabel()
 * @model annotation="http://www.xocl.org/OCL label='let prefix: String = \'label =  \' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Label\'\n'"
 * @generated
 */

public interface MLabel extends MExprAnnotation {
	/**
	 * Returns the value of the '<em><b>Containing Classifier Annotations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Classifier Annotations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Classifier Annotations</em>' reference.
	 * @see #isSetContainingClassifierAnnotations()
	 * @see #unsetContainingClassifierAnnotations()
	 * @see #setContainingClassifierAnnotations(MClassifierAnnotations)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMLabel_ContainingClassifierAnnotations()
	 * @model unsettable="true"
	 * @generated
	 */
	MClassifierAnnotations getContainingClassifierAnnotations();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MLabel#getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing Classifier Annotations</em>' reference.
	 * @see #isSetContainingClassifierAnnotations()
	 * @see #unsetContainingClassifierAnnotations()
	 * @see #getContainingClassifierAnnotations()
	 * @generated
	 */
	void setContainingClassifierAnnotations(MClassifierAnnotations value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MLabel#getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainingClassifierAnnotations()
	 * @see #getContainingClassifierAnnotations()
	 * @see #setContainingClassifierAnnotations(MClassifierAnnotations)
	 * @generated
	 */
	void unsetContainingClassifierAnnotations();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MLabel#getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Containing Classifier Annotations</em>' reference is set.
	 * @see #unsetContainingClassifierAnnotations()
	 * @see #getContainingClassifierAnnotations()
	 * @see #setContainingClassifierAnnotations(MClassifierAnnotations)
	 * @generated
	 */
	boolean isSetContainingClassifierAnnotations();

} // MLabel
