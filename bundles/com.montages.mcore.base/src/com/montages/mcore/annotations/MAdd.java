/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAdd</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MAdd#getDefaultCommandMode <em>Default Command Mode</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAdd#getAddCommand <em>Add Command</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAdd()
 * @model
 * @generated
 */

public interface MAdd extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Default Command Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.annotations.DefaultCommandMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Command Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Command Mode</em>' attribute.
	 * @see com.montages.mcore.annotations.DefaultCommandMode
	 * @see #isSetDefaultCommandMode()
	 * @see #unsetDefaultCommandMode()
	 * @see #setDefaultCommandMode(DefaultCommandMode)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAdd_DefaultCommandMode()
	 * @model unsettable="true"
	 * @generated
	 */
	DefaultCommandMode getDefaultCommandMode();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAdd#getDefaultCommandMode <em>Default Command Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Command Mode</em>' attribute.
	 * @see com.montages.mcore.annotations.DefaultCommandMode
	 * @see #isSetDefaultCommandMode()
	 * @see #unsetDefaultCommandMode()
	 * @see #getDefaultCommandMode()
	 * @generated
	 */
	void setDefaultCommandMode(DefaultCommandMode value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAdd#getDefaultCommandMode <em>Default Command Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDefaultCommandMode()
	 * @see #getDefaultCommandMode()
	 * @see #setDefaultCommandMode(DefaultCommandMode)
	 * @generated
	 */
	void unsetDefaultCommandMode();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAdd#getDefaultCommandMode <em>Default Command Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Default Command Mode</em>' attribute is set.
	 * @see #unsetDefaultCommandMode()
	 * @see #getDefaultCommandMode()
	 * @see #setDefaultCommandMode(DefaultCommandMode)
	 * @generated
	 */
	boolean isSetDefaultCommandMode();

	/**
	 * Returns the value of the '<em><b>Add Command</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MAddCommand}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Add Command</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Add Command</em>' containment reference list.
	 * @see #isSetAddCommand()
	 * @see #unsetAddCommand()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAdd_AddCommand()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MAddCommand> getAddCommand();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAdd#getAddCommand <em>Add Command</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAddCommand()
	 * @see #getAddCommand()
	 * @generated
	 */
	void unsetAddCommand();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAdd#getAddCommand <em>Add Command</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Add Command</em>' containment reference list is set.
	 * @see #unsetAddCommand()
	 * @see #getAddCommand()
	 * @generated
	 */
	boolean isSetAddCommand();

} // MAdd
