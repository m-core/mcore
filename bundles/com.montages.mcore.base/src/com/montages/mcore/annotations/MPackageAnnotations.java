/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPackage Annotations</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPackageAnnotations()
 * @model
 * @generated
 */

public interface MPackageAnnotations extends MRepositoryElement {
} // MPackageAnnotations
