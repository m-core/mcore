/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MNamed;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.updates.UpdateMode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getObject <em>Object</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getValue <em>Value</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getMode <em>Mode</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getFeaturePackageFilter <em>Feature Package Filter</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getFeatureClassFilter <em>Feature Class Filter</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getFeature <em>Feature</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getContainingPropertyAnnotations <em>Containing Property Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getLocation <em>Location</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MUpdate#getMUpdateCondition <em>MUpdate Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate()
 * @model annotation="http://www.xocl.org/OCL label='\'update LHS. \'.concat(\r\nif self.feature.oclIsUndefined() then \'FEATURE MISSING\' else self.feature.eName endif).concat(\r\n  if self.mode=updates::UpdateMode::Redefine then \' := RHS\'\r\n  else if self.mode=updates::UpdateMode::Add then \' += RHS\'\r\n  else if self.mode=updates::UpdateMode::Remove then \' -= RHS\' else \' ERROR UPDATE MODE \'\r\n  endif endif endif\r\n  )'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Update\'\n' nameInitValue='let pos:Integer=\r\n  self.containingPropertyAnnotations.update->indexOf(self)  \r\nin\r\n  \'Update \'.concat(\r\n    if pos<10 then \'0\'.concat(pos.toString()) else pos.toString() endif)' shortNameInitValue='null'"
 * @generated
 */

public interface MUpdate extends MNamed, MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mcore.annotations.MUpdateObject#getContainingUpdateAnnotation <em>Containing Update Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' containment reference.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see #setObject(MUpdateObject)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_Object()
	 * @see com.montages.mcore.annotations.MUpdateObject#getContainingUpdateAnnotation
	 * @model opposite="containingUpdateAnnotation" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MUpdateObject getObject();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getObject <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' containment reference.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see #getObject()
	 * @generated
	 */
	void setObject(MUpdateObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getObject <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObject()
	 * @see #getObject()
	 * @see #setObject(MUpdateObject)
	 * @generated
	 */
	void unsetObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getObject <em>Object</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object</em>' containment reference is set.
	 * @see #unsetObject()
	 * @see #getObject()
	 * @see #setObject(MUpdateObject)
	 * @generated
	 */
	boolean isSetObject();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mcore.annotations.MUpdateValue#getContainingUpdateAnnotation <em>Containing Update Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(MUpdateValue)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_Value()
	 * @see com.montages.mcore.annotations.MUpdateValue#getContainingUpdateAnnotation
	 * @model opposite="containingUpdateAnnotation" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MUpdateValue getValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(MUpdateValue value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(MUpdateValue)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getValue <em>Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' containment reference is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(MUpdateValue)
	 * @generated
	 */
	boolean isSetValue();

	/**
	 * Returns the value of the '<em><b>Location</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mcore.annotations.MUpdatePersistenceLocation#getContainingUpdateAnnotation <em>Containing Update Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' containment reference.
	 * @see #isSetLocation()
	 * @see #unsetLocation()
	 * @see #setLocation(MUpdatePersistenceLocation)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_Location()
	 * @see com.montages.mcore.annotations.MUpdatePersistenceLocation#getContainingUpdateAnnotation
	 * @model opposite="containingUpdateAnnotation" containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='location'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='true'"
	 * @generated
	 */
	MUpdatePersistenceLocation getLocation();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getLocation <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' containment reference.
	 * @see #isSetLocation()
	 * @see #unsetLocation()
	 * @see #getLocation()
	 * @generated
	 */

	void setLocation(MUpdatePersistenceLocation value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getLocation <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLocation()
	 * @see #getLocation()
	 * @see #setLocation(MUpdatePersistenceLocation)
	 * @generated
	 */
	void unsetLocation();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getLocation <em>Location</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Location</em>' containment reference is set.
	 * @see #unsetLocation()
	 * @see #getLocation()
	 * @see #setLocation(MUpdatePersistenceLocation)
	 * @generated
	 */
	boolean isSetLocation();

	/**
	 * Returns the value of the '<em><b>Alternative Persistence Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alternative Persistence Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alternative Persistence Root</em>' containment reference.
	 * @see #isSetAlternativePersistenceRoot()
	 * @see #unsetAlternativePersistenceRoot()
	 * @see #setAlternativePersistenceRoot(MUpdateAlternativePersistenceRoot)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_AlternativePersistenceRoot()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='true'"
	 * @generated
	 */
	MUpdateAlternativePersistenceRoot getAlternativePersistenceRoot();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alternative Persistence Root</em>' containment reference.
	 * @see #isSetAlternativePersistenceRoot()
	 * @see #unsetAlternativePersistenceRoot()
	 * @see #getAlternativePersistenceRoot()
	 * @generated
	 */

	void setAlternativePersistenceRoot(MUpdateAlternativePersistenceRoot value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAlternativePersistenceRoot()
	 * @see #getAlternativePersistenceRoot()
	 * @see #setAlternativePersistenceRoot(MUpdateAlternativePersistenceRoot)
	 * @generated
	 */
	void unsetAlternativePersistenceRoot();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Alternative Persistence Root</em>' containment reference is set.
	 * @see #unsetAlternativePersistenceRoot()
	 * @see #getAlternativePersistenceRoot()
	 * @see #setAlternativePersistenceRoot(MUpdateAlternativePersistenceRoot)
	 * @generated
	 */
	boolean isSetAlternativePersistenceRoot();

	/**
	 * Returns the value of the '<em><b>Alternative Persistence Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alternative Persistence Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alternative Persistence Reference</em>' containment reference.
	 * @see #isSetAlternativePersistenceReference()
	 * @see #unsetAlternativePersistenceReference()
	 * @see #setAlternativePersistenceReference(MUpdateAlternativePersistenceReference)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_AlternativePersistenceReference()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='true'"
	 * @generated
	 */
	MUpdateAlternativePersistenceReference getAlternativePersistenceReference();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alternative Persistence Reference</em>' containment reference.
	 * @see #isSetAlternativePersistenceReference()
	 * @see #unsetAlternativePersistenceReference()
	 * @see #getAlternativePersistenceReference()
	 * @generated
	 */

	void setAlternativePersistenceReference(
			MUpdateAlternativePersistenceReference value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAlternativePersistenceReference()
	 * @see #getAlternativePersistenceReference()
	 * @see #setAlternativePersistenceReference(MUpdateAlternativePersistenceReference)
	 * @generated
	 */
	void unsetAlternativePersistenceReference();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Alternative Persistence Reference</em>' containment reference is set.
	 * @see #unsetAlternativePersistenceReference()
	 * @see #getAlternativePersistenceReference()
	 * @see #setAlternativePersistenceReference(MUpdateAlternativePersistenceReference)
	 * @generated
	 */
	boolean isSetAlternativePersistenceReference();

	/**
	 * Returns the value of the '<em><b>MUpdate Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MUpdate Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUpdate Condition</em>' containment reference.
	 * @see #isSetMUpdateCondition()
	 * @see #unsetMUpdateCondition()
	 * @see #setMUpdateCondition(MUpdateCondition)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_MUpdateCondition()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Condition' createColumn='true'"
	 * @generated
	 */
	MUpdateCondition getMUpdateCondition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getMUpdateCondition <em>MUpdate Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MUpdate Condition</em>' containment reference.
	 * @see #isSetMUpdateCondition()
	 * @see #unsetMUpdateCondition()
	 * @see #getMUpdateCondition()
	 * @generated
	 */

	void setMUpdateCondition(MUpdateCondition value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getMUpdateCondition <em>MUpdate Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMUpdateCondition()
	 * @see #getMUpdateCondition()
	 * @see #setMUpdateCondition(MUpdateCondition)
	 * @generated
	 */
	void unsetMUpdateCondition();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getMUpdateCondition <em>MUpdate Condition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MUpdate Condition</em>' containment reference is set.
	 * @see #unsetMUpdateCondition()
	 * @see #getMUpdateCondition()
	 * @see #setMUpdateCondition(MUpdateCondition)
	 * @generated
	 */
	boolean isSetMUpdateCondition();

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.updates.UpdateMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see com.montages.mcore.updates.UpdateMode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #setMode(UpdateMode)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_Mode()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL initValue='let p:MProperty= containingPropertyAnnotations.annotatedProperty in\r\nif p.oclIsUndefined() then updates::UpdateMode::Redefine else \r\nif p.singular then updates::UpdateMode::Redefine else updates::UpdateMode::Add endif endif'"
	 * @generated
	 */
	UpdateMode getMode();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see com.montages.mcore.updates.UpdateMode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #getMode()
	 * @generated
	 */
	void setMode(UpdateMode value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMode()
	 * @see #getMode()
	 * @see #setMode(UpdateMode)
	 * @generated
	 */
	void unsetMode();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getMode <em>Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mode</em>' attribute is set.
	 * @see #unsetMode()
	 * @see #getMode()
	 * @see #setMode(UpdateMode)
	 * @generated
	 */
	boolean isSetMode();

	/**
	 * Returns the value of the '<em><b>Feature Package Filter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Package Filter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Package Filter</em>' reference.
	 * @see #isSetFeaturePackageFilter()
	 * @see #unsetFeaturePackageFilter()
	 * @see #setFeaturePackageFilter(MPackage)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_FeaturePackageFilter()
	 * @model unsettable="true"
	 * @generated
	 */
	MPackage getFeaturePackageFilter();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeaturePackageFilter <em>Feature Package Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Package Filter</em>' reference.
	 * @see #isSetFeaturePackageFilter()
	 * @see #unsetFeaturePackageFilter()
	 * @see #getFeaturePackageFilter()
	 * @generated
	 */
	void setFeaturePackageFilter(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeaturePackageFilter <em>Feature Package Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFeaturePackageFilter()
	 * @see #getFeaturePackageFilter()
	 * @see #setFeaturePackageFilter(MPackage)
	 * @generated
	 */
	void unsetFeaturePackageFilter();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeaturePackageFilter <em>Feature Package Filter</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Feature Package Filter</em>' reference is set.
	 * @see #unsetFeaturePackageFilter()
	 * @see #getFeaturePackageFilter()
	 * @see #setFeaturePackageFilter(MPackage)
	 * @generated
	 */
	boolean isSetFeaturePackageFilter();

	/**
	 * Returns the value of the '<em><b>Feature Class Filter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Class Filter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Class Filter</em>' reference.
	 * @see #isSetFeatureClassFilter()
	 * @see #unsetFeatureClassFilter()
	 * @see #setFeatureClassFilter(MClassifier)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_FeatureClassFilter()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.kind=ClassifierKind::ClassType and\r\nif self.featurePackageFilter.oclIsUndefined() \r\nthen true else\r\ntrg.containingPackage = self.featurePackageFilter endif'"
	 * @generated
	 */
	MClassifier getFeatureClassFilter();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeatureClassFilter <em>Feature Class Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Class Filter</em>' reference.
	 * @see #isSetFeatureClassFilter()
	 * @see #unsetFeatureClassFilter()
	 * @see #getFeatureClassFilter()
	 * @generated
	 */
	void setFeatureClassFilter(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeatureClassFilter <em>Feature Class Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFeatureClassFilter()
	 * @see #getFeatureClassFilter()
	 * @see #setFeatureClassFilter(MClassifier)
	 * @generated
	 */
	void unsetFeatureClassFilter();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeatureClassFilter <em>Feature Class Filter</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Feature Class Filter</em>' reference is set.
	 * @see #unsetFeatureClassFilter()
	 * @see #getFeatureClassFilter()
	 * @see #setFeatureClassFilter(MClassifier)
	 * @generated
	 */
	boolean isSetFeatureClassFilter();

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #setFeature(MProperty)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_Feature()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.kind<>PropertyKind::Operation\r\nand\r\nif self.featureClassFilter.oclIsUndefined()\r\n  then \r\n    if self.featurePackageFilter.oclIsUndefined() \r\n      then true\r\n      else trg.containingClassifier.containingPackage=\r\n        self.featurePackageFilter endif\r\n  else featureClassFilter.allFeatures()->includes(trg) endif' color=' feature.oclIsUndefined()\n'"
	 * @generated
	 */
	MProperty getFeature();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFeature()
	 * @see #getFeature()
	 * @see #setFeature(MProperty)
	 * @generated
	 */
	void unsetFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getFeature <em>Feature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Feature</em>' reference is set.
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @see #setFeature(MProperty)
	 * @generated
	 */
	boolean isSetFeature();

	/**
	 * Returns the value of the '<em><b>Containing Property Annotations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Property Annotations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Property Annotations</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_ContainingPropertyAnnotations()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(MPropertyAnnotations)'"
	 * @generated
	 */
	MPropertyAnnotations getContainingPropertyAnnotations();

	/**
	 * Returns the value of the '<em><b>Alternative Persistence Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alternative Persistence Package</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alternative Persistence Package</em>' containment reference.
	 * @see #isSetAlternativePersistencePackage()
	 * @see #unsetAlternativePersistencePackage()
	 * @see #setAlternativePersistencePackage(MUpdateAlternativePersistencePackage)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdate_AlternativePersistencePackage()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='true'"
	 * @generated
	 */
	MUpdateAlternativePersistencePackage getAlternativePersistencePackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alternative Persistence Package</em>' containment reference.
	 * @see #isSetAlternativePersistencePackage()
	 * @see #unsetAlternativePersistencePackage()
	 * @see #getAlternativePersistencePackage()
	 * @generated
	 */

	void setAlternativePersistencePackage(
			MUpdateAlternativePersistencePackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAlternativePersistencePackage()
	 * @see #getAlternativePersistencePackage()
	 * @see #setAlternativePersistencePackage(MUpdateAlternativePersistencePackage)
	 * @generated
	 */
	void unsetAlternativePersistencePackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MUpdate#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Alternative Persistence Package</em>' containment reference is set.
	 * @see #unsetAlternativePersistencePackage()
	 * @see #getAlternativePersistencePackage()
	 * @see #setAlternativePersistencePackage(MUpdateAlternativePersistencePackage)
	 * @generated
	 */
	boolean isSetAlternativePersistencePackage();

} // MUpdate
