/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MModelElement;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.objects.MObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Annotion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MAbstractAnnotion#getGeneralReference <em>General Reference</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAbstractAnnotion#getGeneralContent <em>General Content</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAbstractAnnotion#getAnnotatedElement <em>Annotated Element</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAbstractAnnotion#getContainingAbstractPropertyAnnotations <em>Containing Abstract Property Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractAnnotion()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractAnnotion extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>General Reference</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>General Reference</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>General Reference</em>' reference list.
	 * @see #isSetGeneralReference()
	 * @see #unsetGeneralReference()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractAnnotion_GeneralReference()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special General Annotation' createColumn='false'"
	 * @generated
	 */
	EList<MObject> getGeneralReference();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAbstractAnnotion#getGeneralReference <em>General Reference</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGeneralReference()
	 * @see #getGeneralReference()
	 * @generated
	 */
	void unsetGeneralReference();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAbstractAnnotion#getGeneralReference <em>General Reference</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>General Reference</em>' reference list is set.
	 * @see #unsetGeneralReference()
	 * @see #getGeneralReference()
	 * @generated
	 */
	boolean isSetGeneralReference();

	/**
	 * Returns the value of the '<em><b>General Content</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>General Content</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>General Content</em>' containment reference list.
	 * @see #isSetGeneralContent()
	 * @see #unsetGeneralContent()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractAnnotion_GeneralContent()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special General Annotation' createColumn='true'"
	 * @generated
	 */
	EList<MObject> getGeneralContent();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAbstractAnnotion#getGeneralContent <em>General Content</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGeneralContent()
	 * @see #getGeneralContent()
	 * @generated
	 */
	void unsetGeneralContent();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAbstractAnnotion#getGeneralContent <em>General Content</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>General Content</em>' containment reference list is set.
	 * @see #unsetGeneralContent()
	 * @see #getGeneralContent()
	 * @generated
	 */
	boolean isSetGeneralContent();

	/**
	 * Returns the value of the '<em><b>Annotated Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotated Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotated Element</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractAnnotion_AnnotatedElement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.containingAbstractPropertyAnnotations.oclIsUndefined() and self.containingAbstractPropertyAnnotations.oclIsKindOf(MPropertyAnnotations) \n\tthen self.containingAbstractPropertyAnnotations.oclAsType(MPropertyAnnotations).property\n\telse \n\t\tif not self.containingAbstractPropertyAnnotations.oclIsUndefined() and self.containingAbstractPropertyAnnotations.oclIsKindOf(MOperationAnnotations)\n\t\t\tthen self.containingAbstractPropertyAnnotations.oclAsType(MOperationAnnotations).operationSignature\n\t\t\telse \n\t\t\t\tif self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(MClassifierAnnotations) \n\t\t\t\t\tthen  self.oclAsType(ecore::EObject).eContainer().oclAsType(MClassifierAnnotations).classifier\n\t\t\t\t\telse \n\t\t\t\t\t\tif self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(MModelElement) \n\t\t\t\t\t\t\tthen self.oclAsType(ecore::EObject).eContainer().oclAsType(MModelElement) \n\t\t\t\t\t\t\telse null \n\t\t\t\t\t\tendif\n\t\t\t\tendif\n\t\tendif\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MModelElement getAnnotatedElement();

	/**
	 * Returns the value of the '<em><b>Containing Abstract Property Annotations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Abstract Property Annotations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Abstract Property Annotations</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractAnnotion_ContainingAbstractPropertyAnnotations()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsKindOf(MAbstractPropertyAnnotations) then\r\n self.eContainer().oclAsType(MAbstractPropertyAnnotations)\r\n else null endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MAbstractPropertyAnnotations getContainingAbstractPropertyAnnotations();

} // MAbstractAnnotion
