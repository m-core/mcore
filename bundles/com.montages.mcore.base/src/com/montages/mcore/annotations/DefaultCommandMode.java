/**
 */
package com.montages.mcore.annotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Default Command Mode</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.annotations.AnnotationsPackage#getDefaultCommandMode()
 * @model
 * @generated
 */
public enum DefaultCommandMode implements Enumerator {
	/**
	 * The '<em><b>Hide Defaultfor Explicit Add</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIDE_DEFAULTFOR_EXPLICIT_ADD_VALUE
	 * @generated
	 * @ordered
	 */
	HIDE_DEFAULTFOR_EXPLICIT_ADD(0, "HideDefaultforExplicitAdd", "Hide Default for Explicit Add"),

	/**
	 * The '<em><b>Hide Every Default Add</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIDE_EVERY_DEFAULT_ADD_VALUE
	 * @generated
	 * @ordered
	 */
	HIDE_EVERY_DEFAULT_ADD(1, "HideEveryDefaultAdd", "Hide Every Default Add"),

	/**
	 * The '<em><b>Show Every Default Add</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHOW_EVERY_DEFAULT_ADD_VALUE
	 * @generated
	 * @ordered
	 */
	SHOW_EVERY_DEFAULT_ADD(2, "ShowEveryDefaultAdd", "Show Every Default Add"),

	/**
	 * The '<em><b>Show Defaultfor Explicit Add</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHOW_DEFAULTFOR_EXPLICIT_ADD_VALUE
	 * @generated
	 * @ordered
	 */
	SHOW_DEFAULTFOR_EXPLICIT_ADD(3, "ShowDefaultforExplicitAdd", "Show Default for Explicit Add");

	/**
	 * The '<em><b>Hide Defaultfor Explicit Add</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hide Defaultfor Explicit Add</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIDE_DEFAULTFOR_EXPLICIT_ADD
	 * @model name="HideDefaultforExplicitAdd" literal="Hide Default for Explicit Add"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Hide Default for Explicit Add'"
	 * @generated
	 * @ordered
	 */
	public static final int HIDE_DEFAULTFOR_EXPLICIT_ADD_VALUE = 0;

	/**
	 * The '<em><b>Hide Every Default Add</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hide Every Default Add</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIDE_EVERY_DEFAULT_ADD
	 * @model name="HideEveryDefaultAdd" literal="Hide Every Default Add"
	 * @generated
	 * @ordered
	 */
	public static final int HIDE_EVERY_DEFAULT_ADD_VALUE = 1;

	/**
	 * The '<em><b>Show Every Default Add</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Show Every Default Add</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHOW_EVERY_DEFAULT_ADD
	 * @model name="ShowEveryDefaultAdd" literal="Show Every Default Add"
	 * @generated
	 * @ordered
	 */
	public static final int SHOW_EVERY_DEFAULT_ADD_VALUE = 2;

	/**
	 * The '<em><b>Show Defaultfor Explicit Add</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Show Defaultfor Explicit Add</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHOW_DEFAULTFOR_EXPLICIT_ADD
	 * @model name="ShowDefaultforExplicitAdd" literal="Show Default for Explicit Add"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Show Default for Explicit Add'"
	 * @generated
	 * @ordered
	 */
	public static final int SHOW_DEFAULTFOR_EXPLICIT_ADD_VALUE = 3;

	/**
	 * An array of all the '<em><b>Default Command Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DefaultCommandMode[] VALUES_ARRAY = new DefaultCommandMode[] {
			HIDE_DEFAULTFOR_EXPLICIT_ADD, HIDE_EVERY_DEFAULT_ADD,
			SHOW_EVERY_DEFAULT_ADD, SHOW_DEFAULTFOR_EXPLICIT_ADD, };

	/**
	 * A public read-only list of all the '<em><b>Default Command Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DefaultCommandMode> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Default Command Mode</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DefaultCommandMode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DefaultCommandMode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Default Command Mode</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DefaultCommandMode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DefaultCommandMode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Default Command Mode</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DefaultCommandMode get(int value) {
		switch (value) {
		case HIDE_DEFAULTFOR_EXPLICIT_ADD_VALUE:
			return HIDE_DEFAULTFOR_EXPLICIT_ADD;
		case HIDE_EVERY_DEFAULT_ADD_VALUE:
			return HIDE_EVERY_DEFAULT_ADD;
		case SHOW_EVERY_DEFAULT_ADD_VALUE:
			return SHOW_EVERY_DEFAULT_ADD;
		case SHOW_DEFAULTFOR_EXPLICIT_ADD_VALUE:
			return SHOW_DEFAULTFOR_EXPLICIT_ADD;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DefaultCommandMode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //DefaultCommandMode
