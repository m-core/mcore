/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MOperationSignature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MOperation Annotations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MOperationAnnotations#getResult <em>Result</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MOperationAnnotations#getOperationSignature <em>Operation Signature</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MOperationAnnotations#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MOperationAnnotations#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MOperationAnnotations#getOverriddenIn <em>Overridden In</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMOperationAnnotations()
 * @model annotation="http://www.xocl.org/OCL label='if self.operationSignature.oclIsUndefined() then \'OP SIGNATURE MISSING\' else self.operationSignature.eLabel endif'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL annotatedPropertyDerive='if self.operationSignature.oclIsUndefined() then null\r\nelse self.operationSignature.operation endif' kindLabelDerive='if self.operationSignature.oclIsUndefined() then \n\'OPERATION of ?\'\nelse \'OPERATION of \'.concat(annotatedProperty.containingClassifier.eName) endif\n\n--self.annotatedProperty.containingClassifier.eName.concat(\'-OPERATION\') endif\n--if self.operationSignature.oclIsUndefined() then \'Select Signature\' else self.operationSignature.eLabel endif .concat(\'-Semantics\')'"
 * @generated
 */

public interface MOperationAnnotations extends MAbstractPropertyAnnotations {
	/**
	 * Returns the value of the '<em><b>Operation Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Signature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Signature</em>' reference.
	 * @see #isSetOperationSignature()
	 * @see #unsetOperationSignature()
	 * @see #setOperationSignature(MOperationSignature)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMOperationAnnotations_OperationSignature()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='self.containingClassifier.allOperationSignatures()' choiceConstraint='trg=self.operationSignature or \r\nself.containingClassifier.allOperationAnnotations().operationSignature->excludes(trg)'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotated' createColumn='false'"
	 * @generated
	 */
	MOperationSignature getOperationSignature();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOperationSignature <em>Operation Signature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation Signature</em>' reference.
	 * @see #isSetOperationSignature()
	 * @see #unsetOperationSignature()
	 * @see #getOperationSignature()
	 * @generated
	 */
	void setOperationSignature(MOperationSignature value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOperationSignature <em>Operation Signature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperationSignature()
	 * @see #getOperationSignature()
	 * @see #setOperationSignature(MOperationSignature)
	 * @generated
	 */
	void unsetOperationSignature();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOperationSignature <em>Operation Signature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operation Signature</em>' reference is set.
	 * @see #unsetOperationSignature()
	 * @see #getOperationSignature()
	 * @see #setOperationSignature(MOperationSignature)
	 * @generated
	 */
	boolean isSetOperationSignature();

	/**
	 * Returns the value of the '<em><b>Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' containment reference.
	 * @see #isSetResult()
	 * @see #unsetResult()
	 * @see #setResult(MResult)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMOperationAnnotations_Result()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MResult getResult();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getResult <em>Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' containment reference.
	 * @see #isSetResult()
	 * @see #unsetResult()
	 * @see #getResult()
	 * @generated
	 */
	void setResult(MResult value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getResult <em>Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResult()
	 * @see #getResult()
	 * @see #setResult(MResult)
	 * @generated
	 */
	void unsetResult();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getResult <em>Result</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Result</em>' containment reference is set.
	 * @see #unsetResult()
	 * @see #getResult()
	 * @see #setResult(MResult)
	 * @generated
	 */
	boolean isSetResult();

	/**
	 * Returns the value of the '<em><b>Overriding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overriding</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overriding</em>' attribute.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMOperationAnnotations_Overriding()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.operationSignature.oclIsUndefined() then false else \r\nnot(self.operationSignature.containingClassifier = self.containingClassifier) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Overriding' createColumn='false'"
	 * @generated
	 */
	Boolean getOverriding();

	/**
	 * Returns the value of the '<em><b>Overrides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overrides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overrides</em>' reference.
	 * @see #isSetOverrides()
	 * @see #unsetOverrides()
	 * @see #setOverrides(MOperationAnnotations)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMOperationAnnotations_Overrides()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Overriding'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MOperationAnnotations getOverrides();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOverrides <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overrides</em>' reference.
	 * @see #isSetOverrides()
	 * @see #unsetOverrides()
	 * @see #getOverrides()
	 * @generated
	 */
	void setOverrides(MOperationAnnotations value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOverrides <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOverrides()
	 * @see #getOverrides()
	 * @see #setOverrides(MOperationAnnotations)
	 * @generated
	 */
	void unsetOverrides();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOverrides <em>Overrides</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Overrides</em>' reference is set.
	 * @see #unsetOverrides()
	 * @see #getOverrides()
	 * @see #setOverrides(MOperationAnnotations)
	 * @generated
	 */
	boolean isSetOverrides();

	/**
	 * Returns the value of the '<em><b>Overridden In</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MOperationAnnotations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden In</em>' reference list.
	 * @see #isSetOverriddenIn()
	 * @see #unsetOverriddenIn()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMOperationAnnotations_OverriddenIn()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Overriding'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MOperationAnnotations> getOverriddenIn();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOverriddenIn <em>Overridden In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOverriddenIn()
	 * @see #getOverriddenIn()
	 * @generated
	 */
	void unsetOverriddenIn();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MOperationAnnotations#getOverriddenIn <em>Overridden In</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Overridden In</em>' reference list is set.
	 * @see #unsetOverriddenIn()
	 * @see #getOverriddenIn()
	 * @generated
	 */
	boolean isSetOverriddenIn();

} // MOperationAnnotations
