/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.annotations.AnnotationsPackage
 * @generated
 */
public interface AnnotationsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AnnotationsFactory eINSTANCE = com.montages.mcore.annotations.impl.AnnotationsFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>MPackage Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPackage Annotations</em>'.
	 * @generated
	 */
	MPackageAnnotations createMPackageAnnotations();

	/**
	 * Returns a new object of class '<em>MClassifier Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MClassifier Annotations</em>'.
	 * @generated
	 */
	MClassifierAnnotations createMClassifierAnnotations();

	/**
	 * Returns a new object of class '<em>MProperty Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MProperty Annotations</em>'.
	 * @generated
	 */
	MPropertyAnnotations createMPropertyAnnotations();

	/**
	 * Returns a new object of class '<em>MOperation Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MOperation Annotations</em>'.
	 * @generated
	 */
	MOperationAnnotations createMOperationAnnotations();

	/**
	 * Returns a new object of class '<em>MGeneral Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MGeneral Annotation</em>'.
	 * @generated
	 */
	MGeneralAnnotation createMGeneralAnnotation();

	/**
	 * Returns a new object of class '<em>MGeneral Detail</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MGeneral Detail</em>'.
	 * @generated
	 */
	MGeneralDetail createMGeneralDetail();

	/**
	 * Returns a new object of class '<em>MString As Ocl Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MString As Ocl Annotation</em>'.
	 * @generated
	 */
	MStringAsOclAnnotation createMStringAsOclAnnotation();

	/**
	 * Returns a new object of class '<em>MString As Ocl Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MString As Ocl Variable</em>'.
	 * @generated
	 */
	MStringAsOclVariable createMStringAsOclVariable();

	/**
	 * Returns a new object of class '<em>MLayout Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLayout Annotation</em>'.
	 * @generated
	 */
	MLayoutAnnotation createMLayoutAnnotation();

	/**
	 * Returns a new object of class '<em>MLabel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLabel</em>'.
	 * @generated
	 */
	MLabel createMLabel();

	/**
	 * Returns a new object of class '<em>MTrg Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MTrg Annotation</em>'.
	 * @generated
	 */
	MTrgAnnotation createMTrgAnnotation();

	/**
	 * Returns a new object of class '<em>MBoolean Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MBoolean Annotation</em>'.
	 * @generated
	 */
	MBooleanAnnotation createMBooleanAnnotation();

	/**
	 * Returns a new object of class '<em>MInvariant Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MInvariant Constraint</em>'.
	 * @generated
	 */
	MInvariantConstraint createMInvariantConstraint();

	/**
	 * Returns a new object of class '<em>MResult</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MResult</em>'.
	 * @generated
	 */
	MResult createMResult();

	/**
	 * Returns a new object of class '<em>MAdd</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MAdd</em>'.
	 * @generated
	 */
	MAdd createMAdd();

	/**
	 * Returns a new object of class '<em>MAdd Command</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MAdd Command</em>'.
	 * @generated
	 */
	MAddCommand createMAddCommand();

	/**
	 * Returns a new object of class '<em>MUpdate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate</em>'.
	 * @generated
	 */
	MUpdate createMUpdate();

	/**
	 * Returns a new object of class '<em>MUpdate Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Condition</em>'.
	 * @generated
	 */
	MUpdateCondition createMUpdateCondition();

	/**
	 * Returns a new object of class '<em>MUpdate Persistence Location</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Persistence Location</em>'.
	 * @generated
	 */
	MUpdatePersistenceLocation createMUpdatePersistenceLocation();

	/**
	 * Returns a new object of class '<em>MUpdate Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Object</em>'.
	 * @generated
	 */
	MUpdateObject createMUpdateObject();

	/**
	 * Returns a new object of class '<em>MUpdate Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Value</em>'.
	 * @generated
	 */
	MUpdateValue createMUpdateValue();

	/**
	 * Returns a new object of class '<em>MInitialization Order</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MInitialization Order</em>'.
	 * @generated
	 */
	MInitializationOrder createMInitializationOrder();

	/**
	 * Returns a new object of class '<em>MInitialization Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MInitialization Value</em>'.
	 * @generated
	 */
	MInitializationValue createMInitializationValue();

	/**
	 * Returns a new object of class '<em>MHighlight Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MHighlight Annotation</em>'.
	 * @generated
	 */
	MHighlightAnnotation createMHighlightAnnotation();

	/**
	 * Returns a new object of class '<em>MChoice Construction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MChoice Construction</em>'.
	 * @generated
	 */
	MChoiceConstruction createMChoiceConstruction();

	/**
	 * Returns a new object of class '<em>MChoice Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MChoice Constraint</em>'.
	 * @generated
	 */
	MChoiceConstraint createMChoiceConstraint();

	/**
	 * Returns a new object of class '<em>MUpdate Alternative Persistence Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Alternative Persistence Package</em>'.
	 * @generated
	 */
	MUpdateAlternativePersistencePackage createMUpdateAlternativePersistencePackage();

	/**
	 * Returns a new object of class '<em>MUpdate Alternative Persistence Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Alternative Persistence Root</em>'.
	 * @generated
	 */
	MUpdateAlternativePersistenceRoot createMUpdateAlternativePersistenceRoot();

	/**
	 * Returns a new object of class '<em>MUpdate Alternative Persistence Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUpdate Alternative Persistence Reference</em>'.
	 * @generated
	 */
	MUpdateAlternativePersistenceReference createMUpdateAlternativePersistenceReference();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AnnotationsPackage getAnnotationsPackage();

} //AnnotationsFactory
