/**
 */
package com.montages.mcore.annotations.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import com.montages.mcore.MNamed;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.annotations.*;
import com.montages.mcore.expressions.MAbstractChain;
import com.montages.mcore.expressions.MAbstractExpression;
import com.montages.mcore.expressions.MAbstractExpressionWithBase;
import com.montages.mcore.expressions.MBaseChain;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractAnnotion;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.annotations.MAdd;
import com.montages.mcore.annotations.MAddCommand;
import com.montages.mcore.annotations.MAnnotation;
import com.montages.mcore.annotations.MBooleanAnnotation;
import com.montages.mcore.annotations.MChoiceConstraint;
import com.montages.mcore.annotations.MChoiceConstruction;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MGeneralDetail;
import com.montages.mcore.annotations.MHighlightAnnotation;
import com.montages.mcore.annotations.MInitializationOrder;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MLabel;
import com.montages.mcore.annotations.MLayoutAnnotation;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPackageAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.annotations.MStringAsOclAnnotation;
import com.montages.mcore.annotations.MStringAsOclVariable;
import com.montages.mcore.annotations.MTrgAnnotation;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.MUpdateObject;
import com.montages.mcore.annotations.MUpdateValue;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.annotations.AnnotationsPackage
 * @generated
 */
public class AnnotationsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AnnotationsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AnnotationsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationsSwitch<Adapter> modelSwitch = new AnnotationsSwitch<Adapter>() {
		@Override
		public Adapter caseMPackageAnnotations(MPackageAnnotations object) {
			return createMPackageAnnotationsAdapter();
		}

		@Override
		public Adapter caseMClassifierAnnotations(
				MClassifierAnnotations object) {
			return createMClassifierAnnotationsAdapter();
		}

		@Override
		public Adapter caseMAbstractPropertyAnnotations(
				MAbstractPropertyAnnotations object) {
			return createMAbstractPropertyAnnotationsAdapter();
		}

		@Override
		public Adapter caseMPropertyAnnotations(MPropertyAnnotations object) {
			return createMPropertyAnnotationsAdapter();
		}

		@Override
		public Adapter caseMOperationAnnotations(MOperationAnnotations object) {
			return createMOperationAnnotationsAdapter();
		}

		@Override
		public Adapter caseMAbstractAnnotion(MAbstractAnnotion object) {
			return createMAbstractAnnotionAdapter();
		}

		@Override
		public Adapter caseMGeneralAnnotation(MGeneralAnnotation object) {
			return createMGeneralAnnotationAdapter();
		}

		@Override
		public Adapter caseMGeneralDetail(MGeneralDetail object) {
			return createMGeneralDetailAdapter();
		}

		@Override
		public Adapter caseMStringAsOclAnnotation(
				MStringAsOclAnnotation object) {
			return createMStringAsOclAnnotationAdapter();
		}

		@Override
		public Adapter caseMStringAsOclVariable(MStringAsOclVariable object) {
			return createMStringAsOclVariableAdapter();
		}

		@Override
		public Adapter caseMLayoutAnnotation(MLayoutAnnotation object) {
			return createMLayoutAnnotationAdapter();
		}

		@Override
		public Adapter caseMAnnotation(MAnnotation object) {
			return createMAnnotationAdapter();
		}

		@Override
		public Adapter caseMExprAnnotation(MExprAnnotation object) {
			return createMExprAnnotationAdapter();
		}

		@Override
		public Adapter caseMLabel(MLabel object) {
			return createMLabelAdapter();
		}

		@Override
		public Adapter caseMTrgAnnotation(MTrgAnnotation object) {
			return createMTrgAnnotationAdapter();
		}

		@Override
		public Adapter caseMBooleanAnnotation(MBooleanAnnotation object) {
			return createMBooleanAnnotationAdapter();
		}

		@Override
		public Adapter caseMInvariantConstraint(MInvariantConstraint object) {
			return createMInvariantConstraintAdapter();
		}

		@Override
		public Adapter caseMResult(MResult object) {
			return createMResultAdapter();
		}

		@Override
		public Adapter caseMAdd(MAdd object) {
			return createMAddAdapter();
		}

		@Override
		public Adapter caseMAddCommand(MAddCommand object) {
			return createMAddCommandAdapter();
		}

		@Override
		public Adapter caseMUpdate(MUpdate object) {
			return createMUpdateAdapter();
		}

		@Override
		public Adapter caseMUpdateCondition(MUpdateCondition object) {
			return createMUpdateConditionAdapter();
		}

		@Override
		public Adapter caseMUpdatePersistenceLocation(
				MUpdatePersistenceLocation object) {
			return createMUpdatePersistenceLocationAdapter();
		}

		@Override
		public Adapter caseMUpdateObject(MUpdateObject object) {
			return createMUpdateObjectAdapter();
		}

		@Override
		public Adapter caseMUpdateValue(MUpdateValue object) {
			return createMUpdateValueAdapter();
		}

		@Override
		public Adapter caseMInitializationOrder(MInitializationOrder object) {
			return createMInitializationOrderAdapter();
		}

		@Override
		public Adapter caseMInitializationValue(MInitializationValue object) {
			return createMInitializationValueAdapter();
		}

		@Override
		public Adapter caseMHighlightAnnotation(MHighlightAnnotation object) {
			return createMHighlightAnnotationAdapter();
		}

		@Override
		public Adapter caseMChoiceConstruction(MChoiceConstruction object) {
			return createMChoiceConstructionAdapter();
		}

		@Override
		public Adapter caseMChoiceConstraint(MChoiceConstraint object) {
			return createMChoiceConstraintAdapter();
		}

		@Override
		public Adapter caseMUpdatePersistence(MUpdatePersistence object) {
			return createMUpdatePersistenceAdapter();
		}

		@Override
		public Adapter caseMUpdateAlternativePersistencePackage(
				MUpdateAlternativePersistencePackage object) {
			return createMUpdateAlternativePersistencePackageAdapter();
		}

		@Override
		public Adapter caseMUpdateAlternativePersistenceRoot(
				MUpdateAlternativePersistenceRoot object) {
			return createMUpdateAlternativePersistenceRootAdapter();
		}

		@Override
		public Adapter caseMUpdateAlternativePersistenceReference(
				MUpdateAlternativePersistenceReference object) {
			return createMUpdateAlternativePersistenceReferenceAdapter();
		}

		@Override
		public Adapter caseMRepositoryElement(MRepositoryElement object) {
			return createMRepositoryElementAdapter();
		}

		@Override
		public Adapter caseMNamed(MNamed object) {
			return createMNamedAdapter();
		}

		@Override
		public Adapter caseMTyped(MTyped object) {
			return createMTypedAdapter();
		}

		@Override
		public Adapter caseMVariable(MVariable object) {
			return createMVariableAdapter();
		}

		@Override
		public Adapter caseMAbstractExpression(MAbstractExpression object) {
			return createMAbstractExpressionAdapter();
		}

		@Override
		public Adapter caseMAbstractExpressionWithBase(
				MAbstractExpressionWithBase object) {
			return createMAbstractExpressionWithBaseAdapter();
		}

		@Override
		public Adapter caseMAbstractChain(MAbstractChain object) {
			return createMAbstractChainAdapter();
		}

		@Override
		public Adapter caseMBaseChain(MBaseChain object) {
			return createMBaseChainAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MPackageAnnotations <em>MPackage Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MPackageAnnotations
	 * @generated
	 */
	public Adapter createMPackageAnnotationsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MClassifierAnnotations <em>MClassifier Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MClassifierAnnotations
	 * @generated
	 */
	public Adapter createMClassifierAnnotationsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MAbstractPropertyAnnotations <em>MAbstract Property Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MAbstractPropertyAnnotations
	 * @generated
	 */
	public Adapter createMAbstractPropertyAnnotationsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MPropertyAnnotations <em>MProperty Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MPropertyAnnotations
	 * @generated
	 */
	public Adapter createMPropertyAnnotationsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MOperationAnnotations <em>MOperation Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MOperationAnnotations
	 * @generated
	 */
	public Adapter createMOperationAnnotationsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MAbstractAnnotion <em>MAbstract Annotion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MAbstractAnnotion
	 * @generated
	 */
	public Adapter createMAbstractAnnotionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MGeneralAnnotation <em>MGeneral Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MGeneralAnnotation
	 * @generated
	 */
	public Adapter createMGeneralAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MGeneralDetail <em>MGeneral Detail</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MGeneralDetail
	 * @generated
	 */
	public Adapter createMGeneralDetailAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MStringAsOclAnnotation <em>MString As Ocl Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MStringAsOclAnnotation
	 * @generated
	 */
	public Adapter createMStringAsOclAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MStringAsOclVariable <em>MString As Ocl Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MStringAsOclVariable
	 * @generated
	 */
	public Adapter createMStringAsOclVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MLayoutAnnotation <em>MLayout Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MLayoutAnnotation
	 * @generated
	 */
	public Adapter createMLayoutAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MAnnotation <em>MAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MAnnotation
	 * @generated
	 */
	public Adapter createMAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MExprAnnotation <em>MExpr Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MExprAnnotation
	 * @generated
	 */
	public Adapter createMExprAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MLabel <em>MLabel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MLabel
	 * @generated
	 */
	public Adapter createMLabelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MTrgAnnotation <em>MTrg Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MTrgAnnotation
	 * @generated
	 */
	public Adapter createMTrgAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MBooleanAnnotation <em>MBoolean Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MBooleanAnnotation
	 * @generated
	 */
	public Adapter createMBooleanAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MInvariantConstraint <em>MInvariant Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MInvariantConstraint
	 * @generated
	 */
	public Adapter createMInvariantConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MResult <em>MResult</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MResult
	 * @generated
	 */
	public Adapter createMResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MAdd <em>MAdd</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MAdd
	 * @generated
	 */
	public Adapter createMAddAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MAddCommand <em>MAdd Command</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MAddCommand
	 * @generated
	 */
	public Adapter createMAddCommandAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdate <em>MUpdate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdate
	 * @generated
	 */
	public Adapter createMUpdateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdateCondition <em>MUpdate Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdateCondition
	 * @generated
	 */
	public Adapter createMUpdateConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdatePersistenceLocation <em>MUpdate Persistence Location</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdatePersistenceLocation
	 * @generated
	 */
	public Adapter createMUpdatePersistenceLocationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdateObject <em>MUpdate Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdateObject
	 * @generated
	 */
	public Adapter createMUpdateObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdateValue <em>MUpdate Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdateValue
	 * @generated
	 */
	public Adapter createMUpdateValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MInitializationOrder <em>MInitialization Order</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MInitializationOrder
	 * @generated
	 */
	public Adapter createMInitializationOrderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MInitializationValue <em>MInitialization Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MInitializationValue
	 * @generated
	 */
	public Adapter createMInitializationValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MHighlightAnnotation <em>MHighlight Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MHighlightAnnotation
	 * @generated
	 */
	public Adapter createMHighlightAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MChoiceConstruction <em>MChoice Construction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MChoiceConstruction
	 * @generated
	 */
	public Adapter createMChoiceConstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MChoiceConstraint <em>MChoice Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MChoiceConstraint
	 * @generated
	 */
	public Adapter createMChoiceConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdatePersistence <em>MUpdate Persistence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdatePersistence
	 * @generated
	 */
	public Adapter createMUpdatePersistenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdateAlternativePersistencePackage <em>MUpdate Alternative Persistence Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdateAlternativePersistencePackage
	 * @generated
	 */
	public Adapter createMUpdateAlternativePersistencePackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdateAlternativePersistenceRoot <em>MUpdate Alternative Persistence Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdateAlternativePersistenceRoot
	 * @generated
	 */
	public Adapter createMUpdateAlternativePersistenceRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.annotations.MUpdateAlternativePersistenceReference <em>MUpdate Alternative Persistence Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.annotations.MUpdateAlternativePersistenceReference
	 * @generated
	 */
	public Adapter createMUpdateAlternativePersistenceReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MRepositoryElement <em>MRepository Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MRepositoryElement
	 * @generated
	 */
	public Adapter createMRepositoryElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MNamed <em>MNamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MNamed
	 * @generated
	 */
	public Adapter createMNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MTyped <em>MTyped</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MTyped
	 * @generated
	 */
	public Adapter createMTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MVariable <em>MVariable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MVariable
	 * @generated
	 */
	public Adapter createMVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractExpression <em>MAbstract Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractExpression
	 * @generated
	 */
	public Adapter createMAbstractExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase <em>MAbstract Expression With Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractExpressionWithBase
	 * @generated
	 */
	public Adapter createMAbstractExpressionWithBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractChain <em>MAbstract Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractChain
	 * @generated
	 */
	public Adapter createMAbstractChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MBaseChain <em>MBase Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MBaseChain
	 * @generated
	 */
	public Adapter createMBaseChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AnnotationsAdapterFactory
