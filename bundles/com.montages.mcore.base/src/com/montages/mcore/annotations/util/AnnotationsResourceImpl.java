/**
 */
package com.montages.mcore.annotations.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.annotations.util.AnnotationsResourceFactoryImpl
 * @generated
 */
public class AnnotationsResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public AnnotationsResourceImpl(URI uri) {
		super(uri);
	}

} //AnnotationsResourceImpl
