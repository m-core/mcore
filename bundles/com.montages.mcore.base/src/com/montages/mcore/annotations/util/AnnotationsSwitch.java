/**
 */
package com.montages.mcore.annotations.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import com.montages.mcore.MNamed;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.annotations.*;
import com.montages.mcore.expressions.MAbstractChain;
import com.montages.mcore.expressions.MAbstractExpression;
import com.montages.mcore.expressions.MAbstractExpressionWithBase;
import com.montages.mcore.expressions.MBaseChain;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractAnnotion;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.annotations.MAdd;
import com.montages.mcore.annotations.MAddCommand;
import com.montages.mcore.annotations.MAnnotation;
import com.montages.mcore.annotations.MBooleanAnnotation;
import com.montages.mcore.annotations.MChoiceConstraint;
import com.montages.mcore.annotations.MChoiceConstruction;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MGeneralDetail;
import com.montages.mcore.annotations.MHighlightAnnotation;
import com.montages.mcore.annotations.MInitializationOrder;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MLabel;
import com.montages.mcore.annotations.MLayoutAnnotation;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPackageAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.annotations.MStringAsOclAnnotation;
import com.montages.mcore.annotations.MStringAsOclVariable;
import com.montages.mcore.annotations.MTrgAnnotation;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.MUpdateObject;
import com.montages.mcore.annotations.MUpdateValue;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.annotations.AnnotationsPackage
 * @generated
 */
public class AnnotationsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AnnotationsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsSwitch() {
		if (modelPackage == null) {
			modelPackage = AnnotationsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case AnnotationsPackage.MPACKAGE_ANNOTATIONS: {
			MPackageAnnotations mPackageAnnotations = (MPackageAnnotations) theEObject;
			T result = caseMPackageAnnotations(mPackageAnnotations);
			if (result == null)
				result = caseMRepositoryElement(mPackageAnnotations);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS: {
			MClassifierAnnotations mClassifierAnnotations = (MClassifierAnnotations) theEObject;
			T result = caseMClassifierAnnotations(mClassifierAnnotations);
			if (result == null)
				result = caseMRepositoryElement(mClassifierAnnotations);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS: {
			MAbstractPropertyAnnotations mAbstractPropertyAnnotations = (MAbstractPropertyAnnotations) theEObject;
			T result = caseMAbstractPropertyAnnotations(
					mAbstractPropertyAnnotations);
			if (result == null)
				result = caseMRepositoryElement(mAbstractPropertyAnnotations);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS: {
			MPropertyAnnotations mPropertyAnnotations = (MPropertyAnnotations) theEObject;
			T result = caseMPropertyAnnotations(mPropertyAnnotations);
			if (result == null)
				result = caseMAbstractPropertyAnnotations(mPropertyAnnotations);
			if (result == null)
				result = caseMRepositoryElement(mPropertyAnnotations);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MOPERATION_ANNOTATIONS: {
			MOperationAnnotations mOperationAnnotations = (MOperationAnnotations) theEObject;
			T result = caseMOperationAnnotations(mOperationAnnotations);
			if (result == null)
				result = caseMAbstractPropertyAnnotations(
						mOperationAnnotations);
			if (result == null)
				result = caseMRepositoryElement(mOperationAnnotations);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MABSTRACT_ANNOTION: {
			MAbstractAnnotion mAbstractAnnotion = (MAbstractAnnotion) theEObject;
			T result = caseMAbstractAnnotion(mAbstractAnnotion);
			if (result == null)
				result = caseMRepositoryElement(mAbstractAnnotion);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MGENERAL_ANNOTATION: {
			MGeneralAnnotation mGeneralAnnotation = (MGeneralAnnotation) theEObject;
			T result = caseMGeneralAnnotation(mGeneralAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mGeneralAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mGeneralAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MGENERAL_DETAIL: {
			MGeneralDetail mGeneralDetail = (MGeneralDetail) theEObject;
			T result = caseMGeneralDetail(mGeneralDetail);
			if (result == null)
				result = caseMRepositoryElement(mGeneralDetail);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION: {
			MStringAsOclAnnotation mStringAsOclAnnotation = (MStringAsOclAnnotation) theEObject;
			T result = caseMStringAsOclAnnotation(mStringAsOclAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mStringAsOclAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MSTRING_AS_OCL_VARIABLE: {
			MStringAsOclVariable mStringAsOclVariable = (MStringAsOclVariable) theEObject;
			T result = caseMStringAsOclVariable(mStringAsOclVariable);
			if (result == null)
				result = caseMVariable(mStringAsOclVariable);
			if (result == null)
				result = caseMNamed(mStringAsOclVariable);
			if (result == null)
				result = caseMTyped(mStringAsOclVariable);
			if (result == null)
				result = caseMRepositoryElement(mStringAsOclVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MLAYOUT_ANNOTATION: {
			MLayoutAnnotation mLayoutAnnotation = (MLayoutAnnotation) theEObject;
			T result = caseMLayoutAnnotation(mLayoutAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mLayoutAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mLayoutAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MANNOTATION: {
			MAnnotation mAnnotation = (MAnnotation) theEObject;
			T result = caseMAnnotation(mAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MEXPR_ANNOTATION: {
			MExprAnnotation mExprAnnotation = (MExprAnnotation) theEObject;
			T result = caseMExprAnnotation(mExprAnnotation);
			if (result == null)
				result = caseMAnnotation(mExprAnnotation);
			if (result == null)
				result = caseMBaseChain(mExprAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mExprAnnotation);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mExprAnnotation);
			if (result == null)
				result = caseMAbstractChain(mExprAnnotation);
			if (result == null)
				result = caseMAbstractExpression(mExprAnnotation);
			if (result == null)
				result = caseMTyped(mExprAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mExprAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MLABEL: {
			MLabel mLabel = (MLabel) theEObject;
			T result = caseMLabel(mLabel);
			if (result == null)
				result = caseMExprAnnotation(mLabel);
			if (result == null)
				result = caseMAnnotation(mLabel);
			if (result == null)
				result = caseMBaseChain(mLabel);
			if (result == null)
				result = caseMAbstractAnnotion(mLabel);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mLabel);
			if (result == null)
				result = caseMAbstractChain(mLabel);
			if (result == null)
				result = caseMAbstractExpression(mLabel);
			if (result == null)
				result = caseMTyped(mLabel);
			if (result == null)
				result = caseMRepositoryElement(mLabel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MTRG_ANNOTATION: {
			MTrgAnnotation mTrgAnnotation = (MTrgAnnotation) theEObject;
			T result = caseMTrgAnnotation(mTrgAnnotation);
			if (result == null)
				result = caseMExprAnnotation(mTrgAnnotation);
			if (result == null)
				result = caseMAnnotation(mTrgAnnotation);
			if (result == null)
				result = caseMBaseChain(mTrgAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mTrgAnnotation);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mTrgAnnotation);
			if (result == null)
				result = caseMAbstractChain(mTrgAnnotation);
			if (result == null)
				result = caseMAbstractExpression(mTrgAnnotation);
			if (result == null)
				result = caseMTyped(mTrgAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mTrgAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MBOOLEAN_ANNOTATION: {
			MBooleanAnnotation mBooleanAnnotation = (MBooleanAnnotation) theEObject;
			T result = caseMBooleanAnnotation(mBooleanAnnotation);
			if (result == null)
				result = caseMExprAnnotation(mBooleanAnnotation);
			if (result == null)
				result = caseMAnnotation(mBooleanAnnotation);
			if (result == null)
				result = caseMBaseChain(mBooleanAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mBooleanAnnotation);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mBooleanAnnotation);
			if (result == null)
				result = caseMAbstractChain(mBooleanAnnotation);
			if (result == null)
				result = caseMAbstractExpression(mBooleanAnnotation);
			if (result == null)
				result = caseMTyped(mBooleanAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mBooleanAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MINVARIANT_CONSTRAINT: {
			MInvariantConstraint mInvariantConstraint = (MInvariantConstraint) theEObject;
			T result = caseMInvariantConstraint(mInvariantConstraint);
			if (result == null)
				result = caseMExprAnnotation(mInvariantConstraint);
			if (result == null)
				result = caseMNamed(mInvariantConstraint);
			if (result == null)
				result = caseMAnnotation(mInvariantConstraint);
			if (result == null)
				result = caseMBaseChain(mInvariantConstraint);
			if (result == null)
				result = caseMAbstractAnnotion(mInvariantConstraint);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mInvariantConstraint);
			if (result == null)
				result = caseMAbstractChain(mInvariantConstraint);
			if (result == null)
				result = caseMAbstractExpression(mInvariantConstraint);
			if (result == null)
				result = caseMTyped(mInvariantConstraint);
			if (result == null)
				result = caseMRepositoryElement(mInvariantConstraint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MRESULT: {
			MResult mResult = (MResult) theEObject;
			T result = caseMResult(mResult);
			if (result == null)
				result = caseMExprAnnotation(mResult);
			if (result == null)
				result = caseMAnnotation(mResult);
			if (result == null)
				result = caseMBaseChain(mResult);
			if (result == null)
				result = caseMAbstractAnnotion(mResult);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mResult);
			if (result == null)
				result = caseMAbstractChain(mResult);
			if (result == null)
				result = caseMAbstractExpression(mResult);
			if (result == null)
				result = caseMTyped(mResult);
			if (result == null)
				result = caseMRepositoryElement(mResult);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MADD: {
			MAdd mAdd = (MAdd) theEObject;
			T result = caseMAdd(mAdd);
			if (result == null)
				result = caseMRepositoryElement(mAdd);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MADD_COMMAND: {
			MAddCommand mAddCommand = (MAddCommand) theEObject;
			T result = caseMAddCommand(mAddCommand);
			if (result == null)
				result = caseMExprAnnotation(mAddCommand);
			if (result == null)
				result = caseMNamed(mAddCommand);
			if (result == null)
				result = caseMAnnotation(mAddCommand);
			if (result == null)
				result = caseMBaseChain(mAddCommand);
			if (result == null)
				result = caseMAbstractAnnotion(mAddCommand);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mAddCommand);
			if (result == null)
				result = caseMAbstractChain(mAddCommand);
			if (result == null)
				result = caseMAbstractExpression(mAddCommand);
			if (result == null)
				result = caseMTyped(mAddCommand);
			if (result == null)
				result = caseMRepositoryElement(mAddCommand);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE: {
			MUpdate mUpdate = (MUpdate) theEObject;
			T result = caseMUpdate(mUpdate);
			if (result == null)
				result = caseMNamed(mUpdate);
			if (result == null)
				result = caseMRepositoryElement(mUpdate);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_CONDITION: {
			MUpdateCondition mUpdateCondition = (MUpdateCondition) theEObject;
			T result = caseMUpdateCondition(mUpdateCondition);
			if (result == null)
				result = caseMUpdateValue(mUpdateCondition);
			if (result == null)
				result = caseMTrgAnnotation(mUpdateCondition);
			if (result == null)
				result = caseMExprAnnotation(mUpdateCondition);
			if (result == null)
				result = caseMAnnotation(mUpdateCondition);
			if (result == null)
				result = caseMBaseChain(mUpdateCondition);
			if (result == null)
				result = caseMAbstractAnnotion(mUpdateCondition);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mUpdateCondition);
			if (result == null)
				result = caseMAbstractChain(mUpdateCondition);
			if (result == null)
				result = caseMAbstractExpression(mUpdateCondition);
			if (result == null)
				result = caseMTyped(mUpdateCondition);
			if (result == null)
				result = caseMRepositoryElement(mUpdateCondition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION: {
			MUpdatePersistenceLocation mUpdatePersistenceLocation = (MUpdatePersistenceLocation) theEObject;
			T result = caseMUpdatePersistenceLocation(
					mUpdatePersistenceLocation);
			if (result == null)
				result = caseMTrgAnnotation(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMExprAnnotation(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMAnnotation(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMBaseChain(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMAbstractAnnotion(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMAbstractExpressionWithBase(
						mUpdatePersistenceLocation);
			if (result == null)
				result = caseMAbstractChain(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMAbstractExpression(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMTyped(mUpdatePersistenceLocation);
			if (result == null)
				result = caseMRepositoryElement(mUpdatePersistenceLocation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_OBJECT: {
			MUpdateObject mUpdateObject = (MUpdateObject) theEObject;
			T result = caseMUpdateObject(mUpdateObject);
			if (result == null)
				result = caseMTrgAnnotation(mUpdateObject);
			if (result == null)
				result = caseMExprAnnotation(mUpdateObject);
			if (result == null)
				result = caseMAnnotation(mUpdateObject);
			if (result == null)
				result = caseMBaseChain(mUpdateObject);
			if (result == null)
				result = caseMAbstractAnnotion(mUpdateObject);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mUpdateObject);
			if (result == null)
				result = caseMAbstractChain(mUpdateObject);
			if (result == null)
				result = caseMAbstractExpression(mUpdateObject);
			if (result == null)
				result = caseMTyped(mUpdateObject);
			if (result == null)
				result = caseMRepositoryElement(mUpdateObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_VALUE: {
			MUpdateValue mUpdateValue = (MUpdateValue) theEObject;
			T result = caseMUpdateValue(mUpdateValue);
			if (result == null)
				result = caseMTrgAnnotation(mUpdateValue);
			if (result == null)
				result = caseMExprAnnotation(mUpdateValue);
			if (result == null)
				result = caseMAnnotation(mUpdateValue);
			if (result == null)
				result = caseMBaseChain(mUpdateValue);
			if (result == null)
				result = caseMAbstractAnnotion(mUpdateValue);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mUpdateValue);
			if (result == null)
				result = caseMAbstractChain(mUpdateValue);
			if (result == null)
				result = caseMAbstractExpression(mUpdateValue);
			if (result == null)
				result = caseMTyped(mUpdateValue);
			if (result == null)
				result = caseMRepositoryElement(mUpdateValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MINITIALIZATION_ORDER: {
			MInitializationOrder mInitializationOrder = (MInitializationOrder) theEObject;
			T result = caseMInitializationOrder(mInitializationOrder);
			if (result == null)
				result = caseMExprAnnotation(mInitializationOrder);
			if (result == null)
				result = caseMAnnotation(mInitializationOrder);
			if (result == null)
				result = caseMBaseChain(mInitializationOrder);
			if (result == null)
				result = caseMAbstractAnnotion(mInitializationOrder);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mInitializationOrder);
			if (result == null)
				result = caseMAbstractChain(mInitializationOrder);
			if (result == null)
				result = caseMAbstractExpression(mInitializationOrder);
			if (result == null)
				result = caseMTyped(mInitializationOrder);
			if (result == null)
				result = caseMRepositoryElement(mInitializationOrder);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MINITIALIZATION_VALUE: {
			MInitializationValue mInitializationValue = (MInitializationValue) theEObject;
			T result = caseMInitializationValue(mInitializationValue);
			if (result == null)
				result = caseMExprAnnotation(mInitializationValue);
			if (result == null)
				result = caseMAnnotation(mInitializationValue);
			if (result == null)
				result = caseMBaseChain(mInitializationValue);
			if (result == null)
				result = caseMAbstractAnnotion(mInitializationValue);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mInitializationValue);
			if (result == null)
				result = caseMAbstractChain(mInitializationValue);
			if (result == null)
				result = caseMAbstractExpression(mInitializationValue);
			if (result == null)
				result = caseMTyped(mInitializationValue);
			if (result == null)
				result = caseMRepositoryElement(mInitializationValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MHIGHLIGHT_ANNOTATION: {
			MHighlightAnnotation mHighlightAnnotation = (MHighlightAnnotation) theEObject;
			T result = caseMHighlightAnnotation(mHighlightAnnotation);
			if (result == null)
				result = caseMExprAnnotation(mHighlightAnnotation);
			if (result == null)
				result = caseMAnnotation(mHighlightAnnotation);
			if (result == null)
				result = caseMBaseChain(mHighlightAnnotation);
			if (result == null)
				result = caseMAbstractAnnotion(mHighlightAnnotation);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mHighlightAnnotation);
			if (result == null)
				result = caseMAbstractChain(mHighlightAnnotation);
			if (result == null)
				result = caseMAbstractExpression(mHighlightAnnotation);
			if (result == null)
				result = caseMTyped(mHighlightAnnotation);
			if (result == null)
				result = caseMRepositoryElement(mHighlightAnnotation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MCHOICE_CONSTRUCTION: {
			MChoiceConstruction mChoiceConstruction = (MChoiceConstruction) theEObject;
			T result = caseMChoiceConstruction(mChoiceConstruction);
			if (result == null)
				result = caseMExprAnnotation(mChoiceConstruction);
			if (result == null)
				result = caseMAnnotation(mChoiceConstruction);
			if (result == null)
				result = caseMBaseChain(mChoiceConstruction);
			if (result == null)
				result = caseMAbstractAnnotion(mChoiceConstruction);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mChoiceConstruction);
			if (result == null)
				result = caseMAbstractChain(mChoiceConstruction);
			if (result == null)
				result = caseMAbstractExpression(mChoiceConstruction);
			if (result == null)
				result = caseMTyped(mChoiceConstruction);
			if (result == null)
				result = caseMRepositoryElement(mChoiceConstruction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MCHOICE_CONSTRAINT: {
			MChoiceConstraint mChoiceConstraint = (MChoiceConstraint) theEObject;
			T result = caseMChoiceConstraint(mChoiceConstraint);
			if (result == null)
				result = caseMBooleanAnnotation(mChoiceConstraint);
			if (result == null)
				result = caseMExprAnnotation(mChoiceConstraint);
			if (result == null)
				result = caseMAnnotation(mChoiceConstraint);
			if (result == null)
				result = caseMBaseChain(mChoiceConstraint);
			if (result == null)
				result = caseMAbstractAnnotion(mChoiceConstraint);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mChoiceConstraint);
			if (result == null)
				result = caseMAbstractChain(mChoiceConstraint);
			if (result == null)
				result = caseMAbstractExpression(mChoiceConstraint);
			if (result == null)
				result = caseMTyped(mChoiceConstraint);
			if (result == null)
				result = caseMRepositoryElement(mChoiceConstraint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_PERSISTENCE: {
			MUpdatePersistence mUpdatePersistence = (MUpdatePersistence) theEObject;
			T result = caseMUpdatePersistence(mUpdatePersistence);
			if (result == null)
				result = caseMTrgAnnotation(mUpdatePersistence);
			if (result == null)
				result = caseMExprAnnotation(mUpdatePersistence);
			if (result == null)
				result = caseMAnnotation(mUpdatePersistence);
			if (result == null)
				result = caseMBaseChain(mUpdatePersistence);
			if (result == null)
				result = caseMAbstractAnnotion(mUpdatePersistence);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mUpdatePersistence);
			if (result == null)
				result = caseMAbstractChain(mUpdatePersistence);
			if (result == null)
				result = caseMAbstractExpression(mUpdatePersistence);
			if (result == null)
				result = caseMTyped(mUpdatePersistence);
			if (result == null)
				result = caseMRepositoryElement(mUpdatePersistence);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_ALTERNATIVE_PERSISTENCE_PACKAGE: {
			MUpdateAlternativePersistencePackage mUpdateAlternativePersistencePackage = (MUpdateAlternativePersistencePackage) theEObject;
			T result = caseMUpdateAlternativePersistencePackage(
					mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMUpdatePersistence(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMTrgAnnotation(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMExprAnnotation(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMAnnotation(mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMBaseChain(mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMAbstractAnnotion(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMAbstractExpressionWithBase(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMAbstractChain(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMAbstractExpression(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMTyped(mUpdateAlternativePersistencePackage);
			if (result == null)
				result = caseMRepositoryElement(
						mUpdateAlternativePersistencePackage);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_ALTERNATIVE_PERSISTENCE_ROOT: {
			MUpdateAlternativePersistenceRoot mUpdateAlternativePersistenceRoot = (MUpdateAlternativePersistenceRoot) theEObject;
			T result = caseMUpdateAlternativePersistenceRoot(
					mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMUpdatePersistence(
						mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMTrgAnnotation(mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMExprAnnotation(mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMAnnotation(mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMBaseChain(mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMAbstractAnnotion(
						mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMAbstractExpressionWithBase(
						mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMAbstractChain(mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMAbstractExpression(
						mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMTyped(mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = caseMRepositoryElement(
						mUpdateAlternativePersistenceRoot);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AnnotationsPackage.MUPDATE_ALTERNATIVE_PERSISTENCE_REFERENCE: {
			MUpdateAlternativePersistenceReference mUpdateAlternativePersistenceReference = (MUpdateAlternativePersistenceReference) theEObject;
			T result = caseMUpdateAlternativePersistenceReference(
					mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMUpdatePersistence(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMTrgAnnotation(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMExprAnnotation(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMAnnotation(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMBaseChain(mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMAbstractAnnotion(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMAbstractExpressionWithBase(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMAbstractChain(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMAbstractExpression(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMTyped(mUpdateAlternativePersistenceReference);
			if (result == null)
				result = caseMRepositoryElement(
						mUpdateAlternativePersistenceReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPackage Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPackage Annotations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPackageAnnotations(MPackageAnnotations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MClassifier Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MClassifier Annotations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMClassifierAnnotations(MClassifierAnnotations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Property Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Property Annotations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractPropertyAnnotations(
			MAbstractPropertyAnnotations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MProperty Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MProperty Annotations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPropertyAnnotations(MPropertyAnnotations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MOperation Annotations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MOperation Annotations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMOperationAnnotations(MOperationAnnotations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Annotion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Annotion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractAnnotion(MAbstractAnnotion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MGeneral Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MGeneral Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMGeneralAnnotation(MGeneralAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MGeneral Detail</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MGeneral Detail</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMGeneralDetail(MGeneralDetail object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MString As Ocl Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MString As Ocl Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMStringAsOclAnnotation(MStringAsOclAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MString As Ocl Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MString As Ocl Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMStringAsOclVariable(MStringAsOclVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLayout Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLayout Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLayoutAnnotation(MLayoutAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAnnotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAnnotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAnnotation(MAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MExpr Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MExpr Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMExprAnnotation(MExprAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLabel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLabel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLabel(MLabel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTrg Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTrg Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTrgAnnotation(MTrgAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MBoolean Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MBoolean Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMBooleanAnnotation(MBooleanAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MInvariant Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MInvariant Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMInvariantConstraint(MInvariantConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MResult</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MResult</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMResult(MResult object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAdd</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAdd</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAdd(MAdd object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAdd Command</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAdd Command</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAddCommand(MAddCommand object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdate(MUpdate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdateCondition(MUpdateCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Persistence Location</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Persistence Location</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdatePersistenceLocation(MUpdatePersistenceLocation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdateObject(MUpdateObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdateValue(MUpdateValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MInitialization Order</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MInitialization Order</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMInitializationOrder(MInitializationOrder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MInitialization Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MInitialization Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMInitializationValue(MInitializationValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MHighlight Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MHighlight Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMHighlightAnnotation(MHighlightAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MChoice Construction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MChoice Construction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMChoiceConstruction(MChoiceConstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MChoice Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MChoice Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMChoiceConstraint(MChoiceConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Persistence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Persistence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdatePersistence(MUpdatePersistence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Alternative Persistence Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Alternative Persistence Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdateAlternativePersistencePackage(
			MUpdateAlternativePersistencePackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Alternative Persistence Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Alternative Persistence Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdateAlternativePersistenceRoot(
			MUpdateAlternativePersistenceRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUpdate Alternative Persistence Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUpdate Alternative Persistence Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUpdateAlternativePersistenceReference(
			MUpdateAlternativePersistenceReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMRepositoryElement(MRepositoryElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamed(MNamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTyped</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTyped</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTyped(MTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MVariable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MVariable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMVariable(MVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractExpression(MAbstractExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Expression With Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Expression With Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractExpressionWithBase(
			MAbstractExpressionWithBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Chain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Chain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractChain(MAbstractChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MBase Chain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MBase Chain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMBaseChain(MBaseChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AnnotationsSwitch
