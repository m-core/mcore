/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MString As Ocl Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MStringAsOclVariable#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclVariable()
 * @model
 * @generated
 */

public interface MStringAsOclVariable extends MVariable, MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #isSetId()
	 * @see #unsetId()
	 * @see #setId(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclVariable_Id()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	String getId();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MStringAsOclVariable#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #isSetId()
	 * @see #unsetId()
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MStringAsOclVariable#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetId()
	 * @see #getId()
	 * @see #setId(String)
	 * @generated
	 */
	void unsetId();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MStringAsOclVariable#getId <em>Id</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id</em>' attribute is set.
	 * @see #unsetId()
	 * @see #getId()
	 * @see #setId(String)
	 * @generated
	 */
	boolean isSetId();

} // MStringAsOclVariable
