/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.SimpleType;
import com.montages.mcore.objects.MObject;
import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAnnotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getDomainObject <em>Domain Object</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getDomainOperation <em>Domain Operation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getSelfObjectTypeOfAnnotation <em>Self Object Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getTargetObjectTypeOfAnnotation <em>Target Object Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getTargetSimpleTypeOfAnnotation <em>Target Simple Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getObjectObjectTypeOfAnnotation <em>Object Object Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getValue <em>Value</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1 <em>Historized OCL1</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1Timestamp <em>Historized OCL1 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2 <em>Historized OCL2</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2Timestamp <em>Historized OCL2 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3 <em>Historized OCL3</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3Timestamp <em>Historized OCL3 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4 <em>Historized OCL4</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4Timestamp <em>Historized OCL4 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5 <em>Historized OCL5</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5Timestamp <em>Historized OCL5 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6 <em>Historized OCL6</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6Timestamp <em>Historized OCL6 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7 <em>Historized OCL7</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7Timestamp <em>Historized OCL7 Timestamp</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation()
 * @model abstract="true"
 * @generated
 */

public interface MAnnotation extends MAbstractAnnotion {
	/**
	 * Returns the value of the '<em><b>Domain Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Object</em>' reference.
	 * @see #isSetDomainObject()
	 * @see #unsetDomainObject()
	 * @see #setDomainObject(MObject)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_DomainObject()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='if self.generalContent->isEmpty() \r\n  then self.generalReference\r\n  else if self.generalReference->isEmpty()\r\n    then self.generalContent\r\n    else self.generalContent->asSequence()\r\n    ->union(self.generalReference->asSequence()) endif endif\r\n'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special Domain Specific Semantics' createColumn='false'"
	 * @generated
	 */
	MObject getDomainObject();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getDomainObject <em>Domain Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Object</em>' reference.
	 * @see #isSetDomainObject()
	 * @see #unsetDomainObject()
	 * @see #getDomainObject()
	 * @generated
	 */
	void setDomainObject(MObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getDomainObject <em>Domain Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDomainObject()
	 * @see #getDomainObject()
	 * @see #setDomainObject(MObject)
	 * @generated
	 */
	void unsetDomainObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getDomainObject <em>Domain Object</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Domain Object</em>' reference is set.
	 * @see #unsetDomainObject()
	 * @see #getDomainObject()
	 * @see #setDomainObject(MObject)
	 * @generated
	 */
	boolean isSetDomainObject();

	/**
	 * Returns the value of the '<em><b>Domain Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Operation</em>' reference.
	 * @see #isSetDomainOperation()
	 * @see #unsetDomainOperation()
	 * @see #setDomainOperation(MOperationSignature)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_DomainOperation()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if domainObject.oclIsUndefined() \r\n  then false\r\n  else if self.selfObjectTypeOfAnnotation.oclIsUndefined()\r\n    then false\r\n    else let c:MClassifier=domainObject.type in\r\n      if c.oclIsUndefined() \r\n        then false\r\n        else let sts:OrderedSet(MClassifier) = \r\n                    self.selfObjectTypeOfAnnotation.allSubTypes()\r\n                       ->append(self.selfObjectTypeOfAnnotation) in\r\n        \r\n        c.allProperties().operationSignature\r\n         ->select(s:MOperationSignature|\r\n            if  s.parameter->size() = 1\r\n              then let p:MParameter = s.parameter->first() in\r\n                if p.type.oclIsUndefined() \r\n                   then false\r\n                   else sts->includes(p.type) endif\r\n              else false endif)->includes(trg)\r\n    \r\n        endif endif endif\r\n              \r\n             '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special Domain Specific Semantics' createColumn='false'"
	 * @generated
	 */
	MOperationSignature getDomainOperation();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getDomainOperation <em>Domain Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Operation</em>' reference.
	 * @see #isSetDomainOperation()
	 * @see #unsetDomainOperation()
	 * @see #getDomainOperation()
	 * @generated
	 */
	void setDomainOperation(MOperationSignature value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getDomainOperation <em>Domain Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDomainOperation()
	 * @see #getDomainOperation()
	 * @see #setDomainOperation(MOperationSignature)
	 * @generated
	 */
	void unsetDomainOperation();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getDomainOperation <em>Domain Operation</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Domain Operation</em>' reference is set.
	 * @see #unsetDomainOperation()
	 * @see #getDomainOperation()
	 * @see #setDomainOperation(MOperationSignature)
	 * @generated
	 */
	boolean isSetDomainOperation();

	/**
	 * Returns the value of the '<em><b>Self Object Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Self Object Type Of Annotation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Self Object Type Of Annotation</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_SelfObjectTypeOfAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(annotations::MClassifierAnnotations) \r\n  then eContainer().oclAsType(annotations::MClassifierAnnotations).classifier \r\n  else if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n    then eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier \r\n    else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier\r\n      else if eContainer().oclIsTypeOf(annotations::MUpdate)\r\n        then eContainer().eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier \r\n        else null \r\nendif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	MClassifier getSelfObjectTypeOfAnnotation();

	/**
	 * Returns the value of the '<em><b>Target Object Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Object Type Of Annotation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Object Type Of Annotation</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_TargetObjectTypeOfAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	MClassifier getTargetObjectTypeOfAnnotation();

	/**
	 * Returns the value of the '<em><b>Target Simple Type Of Annotation</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Simple Type Of Annotation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Simple Type Of Annotation</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_TargetSimpleTypeOfAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::SimpleType::None\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	SimpleType getTargetSimpleTypeOfAnnotation();

	/**
	 * Returns the value of the '<em><b>Object Object Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Object Type Of Annotation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Object Type Of Annotation</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_ObjectObjectTypeOfAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	MClassifier getObjectObjectTypeOfAnnotation();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_Value()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Explicit OCL' createColumn='false'"
	 * @generated
	 */
	String getValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getValue <em>Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' attribute is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	boolean isSetValue();

	/**
	 * Returns the value of the '<em><b>Historized OCL1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL1</em>' attribute.
	 * @see #isSetHistorizedOCL1()
	 * @see #unsetHistorizedOCL1()
	 * @see #setHistorizedOCL1(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL1()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL1();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1 <em>Historized OCL1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL1</em>' attribute.
	 * @see #isSetHistorizedOCL1()
	 * @see #unsetHistorizedOCL1()
	 * @see #getHistorizedOCL1()
	 * @generated
	 */
	void setHistorizedOCL1(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1 <em>Historized OCL1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL1()
	 * @see #getHistorizedOCL1()
	 * @see #setHistorizedOCL1(String)
	 * @generated
	 */
	void unsetHistorizedOCL1();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1 <em>Historized OCL1</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL1</em>' attribute is set.
	 * @see #unsetHistorizedOCL1()
	 * @see #getHistorizedOCL1()
	 * @see #setHistorizedOCL1(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL1();

	/**
	 * Returns the value of the '<em><b>Historized OCL1 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL1 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL1 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL1Timestamp()
	 * @see #unsetHistorizedOCL1Timestamp()
	 * @see #setHistorizedOCL1Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL1Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL1Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1Timestamp <em>Historized OCL1 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL1 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL1Timestamp()
	 * @see #unsetHistorizedOCL1Timestamp()
	 * @see #getHistorizedOCL1Timestamp()
	 * @generated
	 */
	void setHistorizedOCL1Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1Timestamp <em>Historized OCL1 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL1Timestamp()
	 * @see #getHistorizedOCL1Timestamp()
	 * @see #setHistorizedOCL1Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL1Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL1Timestamp <em>Historized OCL1 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL1 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL1Timestamp()
	 * @see #getHistorizedOCL1Timestamp()
	 * @see #setHistorizedOCL1Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL1Timestamp();

	/**
	 * Returns the value of the '<em><b>Historized OCL2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL2</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL2</em>' attribute.
	 * @see #isSetHistorizedOCL2()
	 * @see #unsetHistorizedOCL2()
	 * @see #setHistorizedOCL2(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL2()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL2();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2 <em>Historized OCL2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL2</em>' attribute.
	 * @see #isSetHistorizedOCL2()
	 * @see #unsetHistorizedOCL2()
	 * @see #getHistorizedOCL2()
	 * @generated
	 */
	void setHistorizedOCL2(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2 <em>Historized OCL2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL2()
	 * @see #getHistorizedOCL2()
	 * @see #setHistorizedOCL2(String)
	 * @generated
	 */
	void unsetHistorizedOCL2();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2 <em>Historized OCL2</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL2</em>' attribute is set.
	 * @see #unsetHistorizedOCL2()
	 * @see #getHistorizedOCL2()
	 * @see #setHistorizedOCL2(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL2();

	/**
	 * Returns the value of the '<em><b>Historized OCL2 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL2 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL2 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL2Timestamp()
	 * @see #unsetHistorizedOCL2Timestamp()
	 * @see #setHistorizedOCL2Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL2Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL2Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2Timestamp <em>Historized OCL2 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL2 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL2Timestamp()
	 * @see #unsetHistorizedOCL2Timestamp()
	 * @see #getHistorizedOCL2Timestamp()
	 * @generated
	 */
	void setHistorizedOCL2Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2Timestamp <em>Historized OCL2 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL2Timestamp()
	 * @see #getHistorizedOCL2Timestamp()
	 * @see #setHistorizedOCL2Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL2Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL2Timestamp <em>Historized OCL2 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL2 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL2Timestamp()
	 * @see #getHistorizedOCL2Timestamp()
	 * @see #setHistorizedOCL2Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL2Timestamp();

	/**
	 * Returns the value of the '<em><b>Historized OCL3</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL3</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL3</em>' attribute.
	 * @see #isSetHistorizedOCL3()
	 * @see #unsetHistorizedOCL3()
	 * @see #setHistorizedOCL3(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL3()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL3();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3 <em>Historized OCL3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL3</em>' attribute.
	 * @see #isSetHistorizedOCL3()
	 * @see #unsetHistorizedOCL3()
	 * @see #getHistorizedOCL3()
	 * @generated
	 */
	void setHistorizedOCL3(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3 <em>Historized OCL3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL3()
	 * @see #getHistorizedOCL3()
	 * @see #setHistorizedOCL3(String)
	 * @generated
	 */
	void unsetHistorizedOCL3();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3 <em>Historized OCL3</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL3</em>' attribute is set.
	 * @see #unsetHistorizedOCL3()
	 * @see #getHistorizedOCL3()
	 * @see #setHistorizedOCL3(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL3();

	/**
	 * Returns the value of the '<em><b>Historized OCL3 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL3 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL3 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL3Timestamp()
	 * @see #unsetHistorizedOCL3Timestamp()
	 * @see #setHistorizedOCL3Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL3Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL3Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3Timestamp <em>Historized OCL3 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL3 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL3Timestamp()
	 * @see #unsetHistorizedOCL3Timestamp()
	 * @see #getHistorizedOCL3Timestamp()
	 * @generated
	 */
	void setHistorizedOCL3Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3Timestamp <em>Historized OCL3 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL3Timestamp()
	 * @see #getHistorizedOCL3Timestamp()
	 * @see #setHistorizedOCL3Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL3Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL3Timestamp <em>Historized OCL3 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL3 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL3Timestamp()
	 * @see #getHistorizedOCL3Timestamp()
	 * @see #setHistorizedOCL3Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL3Timestamp();

	/**
	 * Returns the value of the '<em><b>Historized OCL4</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL4</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL4</em>' attribute.
	 * @see #isSetHistorizedOCL4()
	 * @see #unsetHistorizedOCL4()
	 * @see #setHistorizedOCL4(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL4()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL4();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4 <em>Historized OCL4</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL4</em>' attribute.
	 * @see #isSetHistorizedOCL4()
	 * @see #unsetHistorizedOCL4()
	 * @see #getHistorizedOCL4()
	 * @generated
	 */
	void setHistorizedOCL4(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4 <em>Historized OCL4</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL4()
	 * @see #getHistorizedOCL4()
	 * @see #setHistorizedOCL4(String)
	 * @generated
	 */
	void unsetHistorizedOCL4();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4 <em>Historized OCL4</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL4</em>' attribute is set.
	 * @see #unsetHistorizedOCL4()
	 * @see #getHistorizedOCL4()
	 * @see #setHistorizedOCL4(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL4();

	/**
	 * Returns the value of the '<em><b>Historized OCL4 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL4 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL4 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL4Timestamp()
	 * @see #unsetHistorizedOCL4Timestamp()
	 * @see #setHistorizedOCL4Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL4Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL4Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4Timestamp <em>Historized OCL4 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL4 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL4Timestamp()
	 * @see #unsetHistorizedOCL4Timestamp()
	 * @see #getHistorizedOCL4Timestamp()
	 * @generated
	 */
	void setHistorizedOCL4Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4Timestamp <em>Historized OCL4 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL4Timestamp()
	 * @see #getHistorizedOCL4Timestamp()
	 * @see #setHistorizedOCL4Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL4Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL4Timestamp <em>Historized OCL4 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL4 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL4Timestamp()
	 * @see #getHistorizedOCL4Timestamp()
	 * @see #setHistorizedOCL4Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL4Timestamp();

	/**
	 * Returns the value of the '<em><b>Historized OCL5</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL5</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL5</em>' attribute.
	 * @see #isSetHistorizedOCL5()
	 * @see #unsetHistorizedOCL5()
	 * @see #setHistorizedOCL5(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL5()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL5();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5 <em>Historized OCL5</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL5</em>' attribute.
	 * @see #isSetHistorizedOCL5()
	 * @see #unsetHistorizedOCL5()
	 * @see #getHistorizedOCL5()
	 * @generated
	 */
	void setHistorizedOCL5(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5 <em>Historized OCL5</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL5()
	 * @see #getHistorizedOCL5()
	 * @see #setHistorizedOCL5(String)
	 * @generated
	 */
	void unsetHistorizedOCL5();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5 <em>Historized OCL5</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL5</em>' attribute is set.
	 * @see #unsetHistorizedOCL5()
	 * @see #getHistorizedOCL5()
	 * @see #setHistorizedOCL5(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL5();

	/**
	 * Returns the value of the '<em><b>Historized OCL5 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL5 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL5 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL5Timestamp()
	 * @see #unsetHistorizedOCL5Timestamp()
	 * @see #setHistorizedOCL5Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL5Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL5Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5Timestamp <em>Historized OCL5 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL5 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL5Timestamp()
	 * @see #unsetHistorizedOCL5Timestamp()
	 * @see #getHistorizedOCL5Timestamp()
	 * @generated
	 */
	void setHistorizedOCL5Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5Timestamp <em>Historized OCL5 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL5Timestamp()
	 * @see #getHistorizedOCL5Timestamp()
	 * @see #setHistorizedOCL5Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL5Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL5Timestamp <em>Historized OCL5 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL5 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL5Timestamp()
	 * @see #getHistorizedOCL5Timestamp()
	 * @see #setHistorizedOCL5Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL5Timestamp();

	/**
	 * Returns the value of the '<em><b>Historized OCL6</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL6</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL6</em>' attribute.
	 * @see #isSetHistorizedOCL6()
	 * @see #unsetHistorizedOCL6()
	 * @see #setHistorizedOCL6(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL6()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL6();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6 <em>Historized OCL6</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL6</em>' attribute.
	 * @see #isSetHistorizedOCL6()
	 * @see #unsetHistorizedOCL6()
	 * @see #getHistorizedOCL6()
	 * @generated
	 */
	void setHistorizedOCL6(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6 <em>Historized OCL6</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL6()
	 * @see #getHistorizedOCL6()
	 * @see #setHistorizedOCL6(String)
	 * @generated
	 */
	void unsetHistorizedOCL6();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6 <em>Historized OCL6</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL6</em>' attribute is set.
	 * @see #unsetHistorizedOCL6()
	 * @see #getHistorizedOCL6()
	 * @see #setHistorizedOCL6(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL6();

	/**
	 * Returns the value of the '<em><b>Historized OCL6 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL6 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL6 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL6Timestamp()
	 * @see #unsetHistorizedOCL6Timestamp()
	 * @see #setHistorizedOCL6Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL6Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL6Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6Timestamp <em>Historized OCL6 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL6 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL6Timestamp()
	 * @see #unsetHistorizedOCL6Timestamp()
	 * @see #getHistorizedOCL6Timestamp()
	 * @generated
	 */
	void setHistorizedOCL6Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6Timestamp <em>Historized OCL6 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL6Timestamp()
	 * @see #getHistorizedOCL6Timestamp()
	 * @see #setHistorizedOCL6Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL6Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL6Timestamp <em>Historized OCL6 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL6 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL6Timestamp()
	 * @see #getHistorizedOCL6Timestamp()
	 * @see #setHistorizedOCL6Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL6Timestamp();

	/**
	 * Returns the value of the '<em><b>Historized OCL7</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL7</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL7</em>' attribute.
	 * @see #isSetHistorizedOCL7()
	 * @see #unsetHistorizedOCL7()
	 * @see #setHistorizedOCL7(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL7()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	String getHistorizedOCL7();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7 <em>Historized OCL7</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL7</em>' attribute.
	 * @see #isSetHistorizedOCL7()
	 * @see #unsetHistorizedOCL7()
	 * @see #getHistorizedOCL7()
	 * @generated
	 */
	void setHistorizedOCL7(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7 <em>Historized OCL7</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL7()
	 * @see #getHistorizedOCL7()
	 * @see #setHistorizedOCL7(String)
	 * @generated
	 */
	void unsetHistorizedOCL7();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7 <em>Historized OCL7</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL7</em>' attribute is set.
	 * @see #unsetHistorizedOCL7()
	 * @see #getHistorizedOCL7()
	 * @see #setHistorizedOCL7(String)
	 * @generated
	 */
	boolean isSetHistorizedOCL7();

	/**
	 * Returns the value of the '<em><b>Historized OCL7 Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized OCL7 Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized OCL7 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL7Timestamp()
	 * @see #unsetHistorizedOCL7Timestamp()
	 * @see #setHistorizedOCL7Timestamp(Date)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAnnotation_HistorizedOCL7Timestamp()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Historized OCL' createColumn='false'"
	 * @generated
	 */
	Date getHistorizedOCL7Timestamp();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7Timestamp <em>Historized OCL7 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized OCL7 Timestamp</em>' attribute.
	 * @see #isSetHistorizedOCL7Timestamp()
	 * @see #unsetHistorizedOCL7Timestamp()
	 * @see #getHistorizedOCL7Timestamp()
	 * @generated
	 */
	void setHistorizedOCL7Timestamp(Date value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7Timestamp <em>Historized OCL7 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistorizedOCL7Timestamp()
	 * @see #getHistorizedOCL7Timestamp()
	 * @see #setHistorizedOCL7Timestamp(Date)
	 * @generated
	 */
	void unsetHistorizedOCL7Timestamp();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAnnotation#getHistorizedOCL7Timestamp <em>Historized OCL7 Timestamp</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Historized OCL7 Timestamp</em>' attribute is set.
	 * @see #unsetHistorizedOCL7Timestamp()
	 * @see #getHistorizedOCL7Timestamp()
	 * @see #setHistorizedOCL7Timestamp(Date)
	 * @generated
	 */
	boolean isSetHistorizedOCL7Timestamp();

} // MAnnotation
