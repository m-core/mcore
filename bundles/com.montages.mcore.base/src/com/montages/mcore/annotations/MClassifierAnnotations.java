/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MClassifier Annotations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MClassifierAnnotations#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MClassifierAnnotations#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MClassifierAnnotations#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MClassifierAnnotations#getPropertyAnnotations <em>Property Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MClassifierAnnotations#getOperationAnnotations <em>Operation Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMClassifierAnnotations()
 * @model annotation="http://www.xocl.org/OCL label='\'\''"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Semantics\'\n'"
 * @generated
 */

public interface MClassifierAnnotations extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMClassifierAnnotations_Classifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(mcore::MPropertiesContainer).containingClassifier'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getClassifier();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' containment reference.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #setLabel(MLabel)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMClassifierAnnotations_Label()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MLabel getLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getLabel <em>Label</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' containment reference.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(MLabel value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getLabel <em>Label</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLabel()
	 * @see #getLabel()
	 * @see #setLabel(MLabel)
	 * @generated
	 */
	void unsetLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getLabel <em>Label</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Label</em>' containment reference is set.
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @see #setLabel(MLabel)
	 * @generated
	 */
	boolean isSetLabel();

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MInvariantConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' containment reference list.
	 * @see #isSetConstraint()
	 * @see #unsetConstraint()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMClassifierAnnotations_Constraint()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MInvariantConstraint> getConstraint();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getConstraint <em>Constraint</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstraint()
	 * @see #getConstraint()
	 * @generated
	 */
	void unsetConstraint();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getConstraint <em>Constraint</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constraint</em>' containment reference list is set.
	 * @see #unsetConstraint()
	 * @see #getConstraint()
	 * @generated
	 */
	boolean isSetConstraint();

	/**
	 * Returns the value of the '<em><b>Property Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MPropertyAnnotations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Annotations</em>' containment reference list.
	 * @see #isSetPropertyAnnotations()
	 * @see #unsetPropertyAnnotations()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMClassifierAnnotations_PropertyAnnotations()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MPropertyAnnotations> getPropertyAnnotations();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getPropertyAnnotations <em>Property Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPropertyAnnotations()
	 * @see #getPropertyAnnotations()
	 * @generated
	 */
	void unsetPropertyAnnotations();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getPropertyAnnotations <em>Property Annotations</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Property Annotations</em>' containment reference list is set.
	 * @see #unsetPropertyAnnotations()
	 * @see #getPropertyAnnotations()
	 * @generated
	 */
	boolean isSetPropertyAnnotations();

	/**
	 * Returns the value of the '<em><b>Operation Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MOperationAnnotations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Annotations</em>' containment reference list.
	 * @see #isSetOperationAnnotations()
	 * @see #unsetOperationAnnotations()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMClassifierAnnotations_OperationAnnotations()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MOperationAnnotations> getOperationAnnotations();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getOperationAnnotations <em>Operation Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperationAnnotations()
	 * @see #getOperationAnnotations()
	 * @generated
	 */
	void unsetOperationAnnotations();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MClassifierAnnotations#getOperationAnnotations <em>Operation Annotations</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operation Annotations</em>' containment reference list is set.
	 * @see #unsetOperationAnnotations()
	 * @see #getOperationAnnotations()
	 * @generated
	 */
	boolean isSetOperationAnnotations();

} // MClassifierAnnotations
