/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MChoice Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMChoiceConstraint()
 * @model annotation="http://www.xocl.org/OCL label='let prefix: String = \'choice constraint =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL targetObjectTypeOfAnnotationDerive='if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n    then let c: annotations::MPropertyAnnotations = eContainer().oclAsType(annotations::MPropertyAnnotations) in\r\n    \tif c.annotatedProperty.oclIsUndefined()\r\n    \t\tthen null\r\n    \t\telse c.annotatedProperty.calculatedType endif\r\n\telse null \r\nendif' kindLabelDerive='\'Choice Condition\'\n'"
 * @generated
 */

public interface MChoiceConstraint extends MBooleanAnnotation {
} // MChoiceConstraint
