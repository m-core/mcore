/**
 */
package com.montages.mcore.annotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MProperty Annotations Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotationsAction()
 * @model
 * @generated
 */
public enum MPropertyAnnotationsAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Construct Choice</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTRUCT_CHOICE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTRUCT_CHOICE(1, "ConstructChoice", "Construct Choice"),

	/**
	 * The '<em><b>Constrain Choice</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTRAIN_CHOICE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTRAIN_CHOICE(2, "ConstrainChoice", "Constrain Choice"),

	/**
	 * The '<em><b>Init Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INIT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	INIT_VALUE(3, "InitValue", "Init Value"),

	/**
	 * The '<em><b>Init Order</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INIT_ORDER_VALUE
	 * @generated
	 * @ordered
	 */
	INIT_ORDER(4, "InitOrder", "Init Order"),

	/**
	 * The '<em><b>Result</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESULT_VALUE
	 * @generated
	 * @ordered
	 */
	RESULT(5, "Result", "Result"),

	/**
	 * The '<em><b>Highlight</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIGHLIGHT_VALUE
	 * @generated
	 * @ordered
	 */
	HIGHLIGHT(6, "Highlight", "Highlight");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Construct Choice</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Construct Choice</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTRUCT_CHOICE
	 * @model name="ConstructChoice" literal="Construct Choice"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTRUCT_CHOICE_VALUE = 1;

	/**
	 * The '<em><b>Constrain Choice</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constrain Choice</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTRAIN_CHOICE
	 * @model name="ConstrainChoice" literal="Constrain Choice"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTRAIN_CHOICE_VALUE = 2;

	/**
	 * The '<em><b>Init Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Init Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INIT_VALUE
	 * @model name="InitValue" literal="Init Value"
	 * @generated
	 * @ordered
	 */
	public static final int INIT_VALUE_VALUE = 3;

	/**
	 * The '<em><b>Init Order</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Init Order</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INIT_ORDER
	 * @model name="InitOrder" literal="Init Order"
	 * @generated
	 * @ordered
	 */
	public static final int INIT_ORDER_VALUE = 4;

	/**
	 * The '<em><b>Result</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Result</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RESULT
	 * @model name="Result"
	 * @generated
	 * @ordered
	 */
	public static final int RESULT_VALUE = 5;

	/**
	 * The '<em><b>Highlight</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Highlight</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIGHLIGHT
	 * @model name="Highlight"
	 * @generated
	 * @ordered
	 */
	public static final int HIGHLIGHT_VALUE = 6;

	/**
	 * An array of all the '<em><b>MProperty Annotations Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MPropertyAnnotationsAction[] VALUES_ARRAY = new MPropertyAnnotationsAction[] {
			DO, CONSTRUCT_CHOICE, CONSTRAIN_CHOICE, INIT_VALUE, INIT_ORDER,
			RESULT, HIGHLIGHT, };

	/**
	 * A public read-only list of all the '<em><b>MProperty Annotations Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MPropertyAnnotationsAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MProperty Annotations Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyAnnotationsAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertyAnnotationsAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperty Annotations Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyAnnotationsAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertyAnnotationsAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperty Annotations Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyAnnotationsAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case CONSTRUCT_CHOICE_VALUE:
			return CONSTRUCT_CHOICE;
		case CONSTRAIN_CHOICE_VALUE:
			return CONSTRAIN_CHOICE;
		case INIT_VALUE_VALUE:
			return INIT_VALUE;
		case INIT_ORDER_VALUE:
			return INIT_ORDER;
		case RESULT_VALUE:
			return RESULT;
		case HIGHLIGHT_VALUE:
			return HIGHLIGHT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MPropertyAnnotationsAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MPropertyAnnotationsAction
