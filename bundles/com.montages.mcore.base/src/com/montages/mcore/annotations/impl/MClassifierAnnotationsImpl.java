/**
 */
package com.montages.mcore.annotations.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MLabel;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.impl.MRepositoryElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MClassifier Annotations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MClassifierAnnotationsImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MClassifierAnnotationsImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MClassifierAnnotationsImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MClassifierAnnotationsImpl#getPropertyAnnotations <em>Property Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MClassifierAnnotationsImpl#getOperationAnnotations <em>Operation Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MClassifierAnnotationsImpl extends MRepositoryElementImpl
		implements MClassifierAnnotations {
	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected MLabel label;

	/**
	 * This is true if the Label containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean labelESet;

	/**
	 * The cached value of the '{@link #getConstraint() <em>Constraint</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraint()
	 * @generated
	 * @ordered
	 */
	protected EList<MInvariantConstraint> constraint;

	/**
	 * The cached value of the '{@link #getPropertyAnnotations() <em>Property Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<MPropertyAnnotations> propertyAnnotations;

	/**
	 * The cached value of the '{@link #getOperationAnnotations() <em>Operation Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<MOperationAnnotations> operationAnnotations;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getClassifier <em>Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression classifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MClassifierAnnotationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getClassifier() {
		MClassifier classifier = basicGetClassifier();
		return classifier != null && classifier.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) classifier)
				: classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetClassifier() {
		/**
		 * @OCL self.eContainer().oclAsType(mcore::MPropertiesContainer).containingClassifier
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__CLASSIFIER;

		if (classifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				classifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(classifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLabel getLabel() {
		if (label != null && label.eIsProxy()) {
			InternalEObject oldLabel = (InternalEObject) label;
			label = (MLabel) eResolveProxy(oldLabel);
			if (label != oldLabel) {
				InternalEObject newLabel = (InternalEObject) label;
				NotificationChain msgs = oldLabel.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
						null, null);
				if (newLabel.eInternalContainer() == null) {
					msgs = newLabel.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
							oldLabel, label));
			}
		}
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLabel basicGetLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabel(MLabel newLabel,
			NotificationChain msgs) {
		MLabel oldLabel = label;
		label = newLabel;
		boolean oldLabelESet = labelESet;
		labelESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL, oldLabel,
					newLabel, !oldLabelESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(MLabel newLabel) {
		if (newLabel != label) {
			NotificationChain msgs = null;
			if (label != null)
				msgs = ((InternalEObject) label).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
						null, msgs);
			if (newLabel != null)
				msgs = ((InternalEObject) newLabel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
						null, msgs);
			msgs = basicSetLabel(newLabel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldLabelESet = labelESet;
			labelESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
						newLabel, newLabel, !oldLabelESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetLabel(NotificationChain msgs) {
		MLabel oldLabel = label;
		label = null;
		boolean oldLabelESet = labelESet;
		labelESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL, oldLabel,
					null, oldLabelESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLabel() {
		if (label != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) label).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL,
					null, msgs);
			msgs = basicUnsetLabel(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldLabelESet = labelESet;
			labelESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL, null,
						null, oldLabelESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLabel() {
		return labelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MInvariantConstraint> getConstraint() {
		if (constraint == null) {
			constraint = new EObjectContainmentEList.Unsettable.Resolving<MInvariantConstraint>(
					MInvariantConstraint.class, this,
					AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT);
		}
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstraint() {
		if (constraint != null)
			((InternalEList.Unsettable<?>) constraint).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstraint() {
		return constraint != null
				&& ((InternalEList.Unsettable<?>) constraint).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAnnotations> getPropertyAnnotations() {
		if (propertyAnnotations == null) {
			propertyAnnotations = new EObjectContainmentEList.Unsettable.Resolving<MPropertyAnnotations>(
					MPropertyAnnotations.class, this,
					AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS);
		}
		return propertyAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPropertyAnnotations() {
		if (propertyAnnotations != null)
			((InternalEList.Unsettable<?>) propertyAnnotations).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPropertyAnnotations() {
		return propertyAnnotations != null
				&& ((InternalEList.Unsettable<?>) propertyAnnotations).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MOperationAnnotations> getOperationAnnotations() {
		if (operationAnnotations == null) {
			operationAnnotations = new EObjectContainmentEList.Unsettable.Resolving<MOperationAnnotations>(
					MOperationAnnotations.class, this,
					AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS);
		}
		return operationAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperationAnnotations() {
		if (operationAnnotations != null)
			((InternalEList.Unsettable<?>) operationAnnotations).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperationAnnotations() {
		return operationAnnotations != null
				&& ((InternalEList.Unsettable<?>) operationAnnotations).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL:
			return basicUnsetLabel(msgs);
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT:
			return ((InternalEList<?>) getConstraint()).basicRemove(otherEnd,
					msgs);
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS:
			return ((InternalEList<?>) getPropertyAnnotations())
					.basicRemove(otherEnd, msgs);
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS:
			return ((InternalEList<?>) getOperationAnnotations())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL:
			if (resolve)
				return getLabel();
			return basicGetLabel();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT:
			return getConstraint();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS:
			return getPropertyAnnotations();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS:
			return getOperationAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL:
			setLabel((MLabel) newValue);
			return;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT:
			getConstraint().clear();
			getConstraint().addAll(
					(Collection<? extends MInvariantConstraint>) newValue);
			return;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS:
			getPropertyAnnotations().clear();
			getPropertyAnnotations().addAll(
					(Collection<? extends MPropertyAnnotations>) newValue);
			return;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS:
			getOperationAnnotations().clear();
			getOperationAnnotations().addAll(
					(Collection<? extends MOperationAnnotations>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL:
			unsetLabel();
			return;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT:
			unsetConstraint();
			return;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS:
			unsetPropertyAnnotations();
			return;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS:
			unsetOperationAnnotations();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CLASSIFIER:
			return basicGetClassifier() != null;
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL:
			return isSetLabel();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT:
			return isSetConstraint();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS:
			return isSetPropertyAnnotations();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS:
			return isSetOperationAnnotations();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL ''
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Semantics'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MClassifierAnnotationsImpl
