/**
 */
package com.montages.mcore.annotations.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MNamed;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.util.McoreOclUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MInvariant Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getSpecialEName <em>Special EName</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getShortName <em>Short Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getEName <em>EName</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getFullLabel <em>Full Label</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getLocalStructuralName <em>Local Structural Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getCalculatedName <em>Calculated Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getCalculatedShortName <em>Calculated Short Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getCorrectName <em>Correct Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getCorrectShortName <em>Correct Short Name</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MInvariantConstraintImpl#getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MInvariantConstraintImpl extends MExprAnnotationImpl
		implements MInvariantConstraint {
	/**
	 * The default value of the '{@link #getSpecialEName() <em>Special EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialEName()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_ENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialEName() <em>Special EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialEName()
	 * @generated
	 * @ordered
	 */
	protected String specialEName = SPECIAL_ENAME_EDEFAULT;

	/**
	 * This is true if the Special EName attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean specialENameESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The default value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected String shortName = SHORT_NAME_EDEFAULT;

	/**
	 * This is true if the Short Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean shortNameESet;

	/**
	 * The default value of the '{@link #getEName() <em>EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName()
	 * @generated
	 * @ordered
	 */
	protected static final String ENAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getFullLabel() <em>Full Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String FULL_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLocalStructuralName() <em>Local Structural Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalStructuralName()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_STRUCTURAL_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedName() <em>Calculated Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedName()
	 * @generated
	 * @ordered
	 */
	protected static final String CALCULATED_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedShortName() <em>Calculated Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String CALCULATED_SHORT_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCorrectName() <em>Correct Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectName()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECT_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCorrectShortName() <em>Correct Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectShortName()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECT_SHORT_NAME_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameName <em>Same Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameNamemcoreMNamedBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameShortName <em>Same Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameShortName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameShortNamemcoreMNamedBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameString <em>Same String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringEmpty <em>String Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedShortName <em>Calculated Short Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedShortName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectName <em>Correct Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectShortName <em>Correct Short Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectShortName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel <em>Full Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalStructuralName <em>Local Structural Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalStructuralName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localStructuralNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedName <em>Calculated Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifierAnnotations <em>Containing Classifier Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingClassifierAnnotations
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingClassifierAnnotationsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnTypeOfAnnotation <em>Expected Return Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression expectedReturnTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnSimpleTypeOfAnnotation <em>Expected Return Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression expectedReturnSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MInvariantConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCalculatedShortName() {
		/**
		 * @OCL if stringEmpty(name) or stringEmpty(shortName) then calculatedName else shortName endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__CALCULATED_SHORT_NAME;

		if (calculatedShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME, oldName,
					name, !oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		String oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME, oldName,
					NAME_EDEFAULT, oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortName(String newShortName) {
		String oldShortName = shortName;
		shortName = newShortName;
		boolean oldShortNameESet = shortNameESet;
		shortNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME,
					oldShortName, shortName, !oldShortNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetShortName() {
		String oldShortName = shortName;
		boolean oldShortNameESet = shortNameESet;
		shortName = SHORT_NAME_EDEFAULT;
		shortNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME,
					oldShortName, SHORT_NAME_EDEFAULT, oldShortNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetShortName() {
		return shortNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectName() {
		/**
		 * @OCL not stringEmpty(name)
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__CORRECT_NAME;

		if (correctNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				correctNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectShortName() {
		/**
		 * @OCL  stringEmpty(shortName)
		or (not stringEmpty(name))
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__CORRECT_SHORT_NAME;

		if (correctShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				correctShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEName() {
		/**
		 * @OCL if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true
		then self.calculatedShortName.camelCaseLower()
		else self.specialEName.camelCaseLower()
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialEName() {
		return specialEName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialEName(String newSpecialEName) {
		String oldSpecialEName = specialEName;
		specialEName = newSpecialEName;
		boolean oldSpecialENameESet = specialENameESet;
		specialENameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME,
					oldSpecialEName, specialEName, !oldSpecialENameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpecialEName() {
		String oldSpecialEName = specialEName;
		boolean oldSpecialENameESet = specialENameESet;
		specialEName = SPECIAL_ENAME_EDEFAULT;
		specialENameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME,
					oldSpecialEName, SPECIAL_ENAME_EDEFAULT,
					oldSpecialENameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpecialEName() {
		return specialENameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFullLabel() {
		/**
		 * @OCL 'OVERRIDE IN SUBCLASS '.concat(self.calculatedName)
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalStructuralName() {
		/**
		 * @OCL ''
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__LOCAL_STRUCTURAL_NAME;

		if (localStructuralNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localStructuralNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localStructuralNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCalculatedName() {
		/**
		 * @OCL if stringEmpty(name) then ' NAME MISSING' else name endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = McorePackage.Literals.MNAMED__CALCULATED_NAME;

		if (calculatedNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAnnotations getContainingClassifierAnnotations() {
		MClassifierAnnotations containingClassifierAnnotations = basicGetContainingClassifierAnnotations();
		return containingClassifierAnnotations != null
				&& containingClassifierAnnotations.eIsProxy()
						? (MClassifierAnnotations) eResolveProxy(
								(InternalEObject) containingClassifierAnnotations)
						: containingClassifierAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAnnotations basicGetContainingClassifierAnnotations() {
		/**
		 * @OCL self.eContainer().oclAsType(MClassifierAnnotations)
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT__CONTAINING_CLASSIFIER_ANNOTATIONS;

		if (containingClassifierAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingClassifierAnnotationsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingClassifierAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifierAnnotations result = (MClassifierAnnotations) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameName(MNamed n) {

		/**
		 * @OCL sameString(name, n.name)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MNAMED);
		EOperation eOperation = McorePackage.Literals.MNAMED.getEOperations()
				.get(0);
		if (sameNamemcoreMNamedBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				sameNamemcoreMNamedBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(sameNamemcoreMNamedBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("n", n);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameShortName(MNamed n) {

		/**
		 * @OCL if stringEmpty(shortName)  then
		sameString(name, n.shortName)
		else if  stringEmpty(n.shortName) then
		sameString(shortName, n.name)
		else sameString(shortName, n.shortName)
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MNAMED);
		EOperation eOperation = McorePackage.Literals.MNAMED.getEOperations()
				.get(1);
		if (sameShortNamemcoreMNamedBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				sameShortNamemcoreMNamedBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(sameShortNamemcoreMNamedBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("n", n);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameString(String s1, String s2) {

		/**
		 * @OCL s1=s2 
		or 
		(s1.oclIsUndefined() and s2='')
		or
		(s1='' and s2.oclIsUndefined())
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MNAMED);
		EOperation eOperation = McorePackage.Literals.MNAMED.getEOperations()
				.get(2);
		if (sameStringecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				sameStringecoreEStringecoreEStringBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(sameStringecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("s1", s1);

			evalEnv.add("s2", s2);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean stringEmpty(String s) {

		/**
		 * @OCL s.oclIsUndefined() or s=''
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MNAMED);
		EOperation eOperation = McorePackage.Literals.MNAMED.getEOperations()
				.get(3);
		if (stringEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				stringEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME:
			return getSpecialEName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME:
			return getName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME:
			return getShortName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__ENAME:
			return getEName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__FULL_LABEL:
			return getFullLabel();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__LOCAL_STRUCTURAL_NAME:
			return getLocalStructuralName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_NAME:
			return getCalculatedName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_SHORT_NAME:
			return getCalculatedShortName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_NAME:
			return getCorrectName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_SHORT_NAME:
			return getCorrectShortName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CONTAINING_CLASSIFIER_ANNOTATIONS:
			if (resolve)
				return getContainingClassifierAnnotations();
			return basicGetContainingClassifierAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME:
			setSpecialEName((String) newValue);
			return;
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME:
			setName((String) newValue);
			return;
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME:
			setShortName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME:
			unsetSpecialEName();
			return;
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME:
			unsetName();
			return;
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME:
			unsetShortName();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME:
			return isSetSpecialEName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME:
			return isSetName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME:
			return isSetShortName();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__ENAME:
			return ENAME_EDEFAULT == null ? getEName() != null
					: !ENAME_EDEFAULT.equals(getEName());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__FULL_LABEL:
			return FULL_LABEL_EDEFAULT == null ? getFullLabel() != null
					: !FULL_LABEL_EDEFAULT.equals(getFullLabel());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__LOCAL_STRUCTURAL_NAME:
			return LOCAL_STRUCTURAL_NAME_EDEFAULT == null
					? getLocalStructuralName() != null
					: !LOCAL_STRUCTURAL_NAME_EDEFAULT
							.equals(getLocalStructuralName());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_NAME:
			return CALCULATED_NAME_EDEFAULT == null
					? getCalculatedName() != null
					: !CALCULATED_NAME_EDEFAULT.equals(getCalculatedName());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_SHORT_NAME:
			return CALCULATED_SHORT_NAME_EDEFAULT == null
					? getCalculatedShortName() != null
					: !CALCULATED_SHORT_NAME_EDEFAULT
							.equals(getCalculatedShortName());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_NAME:
			return CORRECT_NAME_EDEFAULT == null ? getCorrectName() != null
					: !CORRECT_NAME_EDEFAULT.equals(getCorrectName());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_SHORT_NAME:
			return CORRECT_SHORT_NAME_EDEFAULT == null
					? getCorrectShortName() != null
					: !CORRECT_SHORT_NAME_EDEFAULT
							.equals(getCorrectShortName());
		case AnnotationsPackage.MINVARIANT_CONSTRAINT__CONTAINING_CLASSIFIER_ANNOTATIONS:
			return basicGetContainingClassifierAnnotations() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MNamed.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME:
				return McorePackage.MNAMED__SPECIAL_ENAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME:
				return McorePackage.MNAMED__NAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME:
				return McorePackage.MNAMED__SHORT_NAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__ENAME:
				return McorePackage.MNAMED__ENAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__FULL_LABEL:
				return McorePackage.MNAMED__FULL_LABEL;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__LOCAL_STRUCTURAL_NAME:
				return McorePackage.MNAMED__LOCAL_STRUCTURAL_NAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_NAME:
				return McorePackage.MNAMED__CALCULATED_NAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_SHORT_NAME:
				return McorePackage.MNAMED__CALCULATED_SHORT_NAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_NAME:
				return McorePackage.MNAMED__CORRECT_NAME;
			case AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_SHORT_NAME:
				return McorePackage.MNAMED__CORRECT_SHORT_NAME;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MNamed.class) {
			switch (baseFeatureID) {
			case McorePackage.MNAMED__SPECIAL_ENAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__SPECIAL_ENAME;
			case McorePackage.MNAMED__NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__NAME;
			case McorePackage.MNAMED__SHORT_NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__SHORT_NAME;
			case McorePackage.MNAMED__ENAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__ENAME;
			case McorePackage.MNAMED__FULL_LABEL:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__FULL_LABEL;
			case McorePackage.MNAMED__LOCAL_STRUCTURAL_NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__LOCAL_STRUCTURAL_NAME;
			case McorePackage.MNAMED__CALCULATED_NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_NAME;
			case McorePackage.MNAMED__CALCULATED_SHORT_NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__CALCULATED_SHORT_NAME;
			case McorePackage.MNAMED__CORRECT_NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_NAME;
			case McorePackage.MNAMED__CORRECT_SHORT_NAME:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT__CORRECT_SHORT_NAME;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MNamed.class) {
			switch (baseOperationID) {
			case McorePackage.MNAMED___SAME_NAME__MNAMED:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT___SAME_NAME__MNAMED;
			case McorePackage.MNAMED___SAME_SHORT_NAME__MNAMED:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT___SAME_SHORT_NAME__MNAMED;
			case McorePackage.MNAMED___SAME_STRING__STRING_STRING:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT___SAME_STRING__STRING_STRING;
			case McorePackage.MNAMED___STRING_EMPTY__STRING:
				return AnnotationsPackage.MINVARIANT_CONSTRAINT___STRING_EMPTY__STRING;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case AnnotationsPackage.MINVARIANT_CONSTRAINT___SAME_NAME__MNAMED:
			return sameName((MNamed) arguments.get(0));
		case AnnotationsPackage.MINVARIANT_CONSTRAINT___SAME_SHORT_NAME__MNAMED:
			return sameShortName((MNamed) arguments.get(0));
		case AnnotationsPackage.MINVARIANT_CONSTRAINT___SAME_STRING__STRING_STRING:
			return sameString((String) arguments.get(0),
					(String) arguments.get(1));
		case AnnotationsPackage.MINVARIANT_CONSTRAINT___STRING_EMPTY__STRING:
			return stringEmpty((String) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specialEName: ");
		if (specialENameESet)
			result.append(specialEName);
		else
			result.append("<unset>");
		result.append(", name: ");
		if (nameESet)
			result.append(name);
		else
			result.append("<unset>");
		result.append(", shortName: ");
		if (shortNameESet)
			result.append(shortName);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL (if self.containingClassifierAnnotations.constraint->indexOf(self)=1
	then 'constraint(s): ' else '                    ' endif)
	.concat(self.eName)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Constraint'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL expectedReturnTypeOfAnnotation null
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetExpectedReturnTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE_OF_ANNOTATION;

		if (expectedReturnTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				expectedReturnTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(expectedReturnTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL expectedReturnSimpleTypeOfAnnotation SimpleType::Boolean
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getExpectedReturnSimpleTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

		if (expectedReturnSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				expectedReturnSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(expectedReturnSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isReturnValueOfAnnotationMandatory true
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsReturnValueOfAnnotationMandatory() {
		EClass eClass = (AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY;

		if (isReturnValueOfAnnotationMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isReturnValueOfAnnotationMandatoryDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(isReturnValueOfAnnotationMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isReturnValueOfAnnotationSingular true
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsReturnValueOfAnnotationSingular() {
		EClass eClass = (AnnotationsPackage.Literals.MINVARIANT_CONSTRAINT);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR;

		if (isReturnValueOfAnnotationSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isReturnValueOfAnnotationSingularDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(isReturnValueOfAnnotationSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * Returns the OCL expression context for the '<em><b>Value</b></em>'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag GFI03
	 * @generated NOT
	 */
	public OCLExpressionContext getValueOCLExpressionContext() {
		OCLExpressionContext defaultC = new OCLExpressionContext(
				OCLExpressionContext.getEObjectClass(), null, null);
		MModelElement e = this.getAnnotatedElement();
		if (false == e instanceof MClassifier) {
			return defaultC;
		}

		MClassifier annotatedClassifier = (MClassifier) e;
		if (annotatedClassifier.getKind() != ClassifierKind.CLASS_TYPE) {
			// not supported for now, will return best we can 
			return defaultC;
		}

		EModelElement internalEModelElement;
		String detailKey;
		EAnnotation eAnnotation;

		internalEModelElement = annotatedClassifier.getInternalEClassifier();
		eAnnotation = XoclEmfUtil.findAnnotationForSuffix(internalEModelElement,
				"NAMED_OCL");
		detailKey = this.getEName();

		try {
			return McoreOclUtil.wrapXOCLContext(eAnnotation, detailKey);
		} catch (DiagnosticException e1) {
			return defaultC;
		}
	}

} //MInvariantConstraintImpl
