
package com.montages.mcore.annotations.impl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.annotations.MUpdatePersistence;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUpdate Persistence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public abstract class MUpdatePersistenceImpl extends MTrgAnnotationImpl
		implements MUpdatePersistence {

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetObjectTypeOfAnnotation <em>Target Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetObjectTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression targetObjectTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetSimpleTypeOfAnnotation <em>Target Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression targetSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAbstractPropertyAnnotations <em>Containing Abstract Property Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAbstractPropertyAnnotations
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression containingAbstractPropertyAnnotationsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnSimpleTypeOfAnnotation <em>Expected Return Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression expectedReturnSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectObjectTypeOfAnnotation <em>Object Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectObjectTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression objectObjectTypeOfAnnotationDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUpdatePersistenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MUPDATE_PERSISTENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL targetObjectTypeOfAnnotation let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in
	if chain.oclIsUndefined()
	then null
	else if chain.oclIsKindOf(MPropertyAnnotations)
	then chain.oclAsType(MPropertyAnnotations)
	else null
	endif
	endif in
	if annotations = null
	then null
	else if annotations.annotatedProperty.oclIsUndefined()
	then null
	else annotations.annotatedProperty.calculatedType
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetTargetObjectTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION;

		if (targetObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				targetObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(targetObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL targetSimpleTypeOfAnnotation let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in
	if chain.oclIsUndefined()
	then null
	else if chain.oclIsKindOf(MPropertyAnnotations)
	then chain.oclAsType(MPropertyAnnotations)
	else null
	endif
	endif in
	if annotations = null
	then null
	else if annotations.annotatedProperty.oclIsUndefined()
	then null
	else annotations.annotatedProperty.calculatedSimpleType
	endif endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getTargetSimpleTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION;

		if (targetSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				targetSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(targetSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL containingAbstractPropertyAnnotations self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MAbstractPropertyAnnotations basicGetContainingAbstractPropertyAnnotations() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS;

		if (containingAbstractPropertyAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				containingAbstractPropertyAnnotationsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingAbstractPropertyAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MAbstractPropertyAnnotations) xoclEval
					.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL expectedReturnSimpleTypeOfAnnotation mcore::SimpleType::String
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getExpectedReturnSimpleTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

		if (expectedReturnSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				expectedReturnSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(expectedReturnSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL objectObjectTypeOfAnnotation if eContainer().oclIsKindOf(MUpdate) then
	let update :MUpdate  = eContainer().oclAsType(MUpdate) in let class:MClassifier = update.feature.containingClassifier in if class.oclIsUndefined() then null else class endif
	else 
	null
	endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetObjectObjectTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION;

		if (objectObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				objectObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(objectObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}
} //MUpdatePersistenceImpl
