/**
 */
package com.montages.mcore.annotations.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;
import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MExprAnnotationAction;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractBaseDefinition;
import com.montages.mcore.expressions.MAbstractChain;
import com.montages.mcore.expressions.MAbstractExpression;
import com.montages.mcore.expressions.MAbstractExpressionWithBase;
import com.montages.mcore.expressions.MAbstractNamedTuple;
import com.montages.mcore.expressions.MAccumulatorBaseDefinition;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MBaseChain;
import com.montages.mcore.expressions.MBaseDefinition;
import com.montages.mcore.expressions.MCallArgument;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.MContainerBaseDefinition;
import com.montages.mcore.expressions.MDataValueExpr;
import com.montages.mcore.expressions.MIf;
import com.montages.mcore.expressions.MIteratorBaseDefinition;
import com.montages.mcore.expressions.MLiteralConstantBaseDefinition;
import com.montages.mcore.expressions.MLiteralLet;
import com.montages.mcore.expressions.MLiteralValueExpr;
import com.montages.mcore.expressions.MNamedConstant;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MNumberBaseDefinition;
import com.montages.mcore.expressions.MObjectBaseDefinition;
import com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition;
import com.montages.mcore.expressions.MOperator;
import com.montages.mcore.expressions.MParameterBaseDefinition;
import com.montages.mcore.expressions.MProcessor;
import com.montages.mcore.expressions.MProcessorDefinition;
import com.montages.mcore.expressions.MSelfBaseDefinition;
import com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition;
import com.montages.mcore.expressions.MSimpleTypeConstantLet;
import com.montages.mcore.expressions.MSourceBaseDefinition;
import com.montages.mcore.expressions.MSubChain;
import com.montages.mcore.expressions.MTargetBaseDefinition;
import com.montages.mcore.expressions.MVariableBaseDefinition;
import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MExpr Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getMultiplicityAsString <em>Multiplicity As String</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedType <em>Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedMandatory <em>Calculated Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedSingular <em>Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedTypePackage <em>Calculated Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getVoidTypeAllowed <em>Void Type Allowed</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedSimpleType <em>Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getMultiplicityCase <em>Multiplicity Case</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getAsCode <em>As Code</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getAsBasicCode <em>As Basic Code</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCollector <em>Collector</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getEntireScope <em>Entire Scope</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeBase <em>Scope Base</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeSelf <em>Scope Self</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeTrg <em>Scope Trg</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeObj <em>Scope Obj</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeLiteralConstants <em>Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeVariables <em>Scope Variables</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeIterator <em>Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeAccumulator <em>Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeParameters <em>Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeContainerBase <em>Scope Container Base</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeNumberBase <em>Scope Number Base</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getScopeSource <em>Scope Source</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalEntireScope <em>Local Entire Scope</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeBase <em>Local Scope Base</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeSelf <em>Local Scope Self</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeTrg <em>Local Scope Trg</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeObj <em>Local Scope Obj</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeSource <em>Local Scope Source</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeVariables <em>Local Scope Variables</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeIterator <em>Local Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeAccumulator <em>Local Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeParameters <em>Local Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLocalScopeContainer <em>Local Scope Container</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getContainingAnnotation <em>Containing Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getContainingExpression <em>Containing Expression</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsComplexExpression <em>Is Complex Expression</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsSubExpression <em>Is Sub Expression</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedOwnType <em>Calculated Own Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedOwnSingular <em>Calculated Own Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getSelfObjectType <em>Self Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getSelfObjectPackage <em>Self Object Package</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getTargetObjectType <em>Target Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getTargetSimpleType <em>Target Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getObjectObjectType <em>Object Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getExpectedReturnType <em>Expected Return Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getExpectedReturnSimpleType <em>Expected Return Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsReturnValueSingular <em>Is Return Value Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getSrcObjectType <em>Src Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getSrcObjectSimpleType <em>Src Object Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseAsCode <em>Base As Code</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBase <em>Base</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseDefinition <em>Base Definition</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseVar <em>Base Var</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseExitType <em>Base Exit Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseExitSimpleType <em>Base Exit Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseExitMandatory <em>Base Exit Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getBaseExitSingular <em>Base Exit Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getChainEntryType <em>Chain Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getChainAsCode <em>Chain As Code</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement1 <em>Element1</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement1Correct <em>Element1 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement2EntryType <em>Element2 Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement2 <em>Element2</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement2Correct <em>Element2 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement3EntryType <em>Element3 Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement3 <em>Element3</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getElement3Correct <em>Element3 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCastType <em>Cast Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getLastElement <em>Last Element</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getChainCalculatedType <em>Chain Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getChainCalculatedSimpleType <em>Chain Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getChainCalculatedSingular <em>Chain Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getProcessor <em>Processor</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getProcessorDefinition <em>Processor Definition</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getTypeMismatch <em>Type Mismatch</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getCallArgument <em>Call Argument</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getSubExpression <em>Sub Expression</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getContainedCollector <em>Contained Collector</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getChainCodeforSubchains <em>Chain Codefor Subchains</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsOwnXOCLOp <em>Is Own XOCL Op</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getNamedExpression <em>Named Expression</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getNamedTuple <em>Named Tuple</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getNamedConstant <em>Named Constant</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getExpectedReturnTypeOfAnnotation <em>Expected Return Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getExpectedReturnSimpleTypeOfAnnotation <em>Expected Return Simple Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getUseExplicitOcl <em>Use Explicit Ocl</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getOclChanged <em>Ocl Changed</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getOclCode <em>Ocl Code</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MExprAnnotationImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MExprAnnotationImpl extends MAnnotationImpl
		implements MExprAnnotation, IXoclInitializable {
	/**
	 * The default value of the '{@link #getMultiplicityAsString() <em>Multiplicity As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityAsString()
	 * @generated
	 * @ordered
	 */
	protected static final String MULTIPLICITY_AS_STRING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedMandatory() <em>Calculated Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedSingular() <em>Calculated Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getVoidTypeAllowed() <em>Void Type Allowed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoidTypeAllowed()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean VOID_TYPE_ALLOWED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedSimpleType() <em>Calculated Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CALCULATED_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getMultiplicityCase() <em>Multiplicity Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityCase()
	 * @generated
	 * @ordered
	 */
	protected static final MultiplicityCase MULTIPLICITY_CASE_EDEFAULT = MultiplicityCase.ZERO_ONE;

	/**
	 * The default value of the '{@link #getAsCode() <em>As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAsBasicCode() <em>As Basic Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_BASIC_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsComplexExpression() <em>Is Complex Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_COMPLEX_EXPRESSION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsSubExpression() <em>Is Sub Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_SUB_EXPRESSION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnMandatory() <em>Calculated Own Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_OWN_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnSingular() <em>Calculated Own Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_OWN_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnSimpleType() <em>Calculated Own Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CALCULATED_OWN_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getTargetSimpleType() <em>Target Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType TARGET_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getExpectedReturnSimpleType() <em>Expected Return Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType EXPECTED_RETURN_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getIsReturnValueMandatory() <em>Is Return Value Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsReturnValueSingular() <em>Is Return Value Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSrcObjectSimpleType() <em>Src Object Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcObjectSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SRC_OBJECT_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getBaseAsCode() <em>Base As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_AS_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected static final ExpressionBase BASE_EDEFAULT = ExpressionBase.UNDEFINED;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected ExpressionBase base = BASE_EDEFAULT;

	/**
	 * This is true if the Base attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean baseESet;

	/**
	 * The cached value of the '{@link #getBaseVar() <em>Base Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseVar()
	 * @generated
	 * @ordered
	 */
	protected MVariable baseVar;

	/**
	 * This is true if the Base Var reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean baseVarESet;

	/**
	 * The default value of the '{@link #getBaseExitSimpleType() <em>Base Exit Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType BASE_EXIT_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getBaseExitMandatory() <em>Base Exit Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BASE_EXIT_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBaseExitSingular() <em>Base Exit Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BASE_EXIT_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getChainAsCode() <em>Chain As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CHAIN_AS_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElement1() <em>Element1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1()
	 * @generated
	 * @ordered
	 */
	protected MProperty element1;

	/**
	 * This is true if the Element1 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean element1ESet;

	/**
	 * The default value of the '{@link #getElement1Correct() <em>Element1 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT1_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElement2() <em>Element2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2()
	 * @generated
	 * @ordered
	 */
	protected MProperty element2;

	/**
	 * This is true if the Element2 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean element2ESet;

	/**
	 * The default value of the '{@link #getElement2Correct() <em>Element2 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT2_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElement3() <em>Element3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3()
	 * @generated
	 * @ordered
	 */
	protected MProperty element3;

	/**
	 * This is true if the Element3 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean element3ESet;

	/**
	 * The default value of the '{@link #getElement3Correct() <em>Element3 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT3_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCastType() <em>Cast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCastType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier castType;

	/**
	 * This is true if the Cast Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean castTypeESet;

	/**
	 * The default value of the '{@link #getChainCalculatedSimpleType() <em>Chain Calculated Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CHAIN_CALCULATED_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getChainCalculatedSingular() <em>Chain Calculated Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CHAIN_CALCULATED_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getProcessor() <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected static final MProcessor PROCESSOR_EDEFAULT = MProcessor.NONE;

	/**
	 * The cached value of the '{@link #getProcessor() <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected MProcessor processor = PROCESSOR_EDEFAULT;

	/**
	 * This is true if the Processor attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean processorESet;

	/**
	 * The default value of the '{@link #getTypeMismatch() <em>Type Mismatch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeMismatch()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TYPE_MISMATCH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCallArgument() <em>Call Argument</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallArgument()
	 * @generated
	 * @ordered
	 */
	protected EList<MCallArgument> callArgument;

	/**
	 * The cached value of the '{@link #getSubExpression() <em>Sub Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubExpression()
	 * @generated
	 * @ordered
	 */
	protected EList<MSubChain> subExpression;

	/**
	 * The cached value of the '{@link #getContainedCollector() <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedCollector()
	 * @generated
	 * @ordered
	 */
	protected MCollectionExpression containedCollector;

	/**
	 * This is true if the Contained Collector containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean containedCollectorESet;

	/**
	 * The default value of the '{@link #getChainCodeforSubchains() <em>Chain Codefor Subchains</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCodeforSubchains()
	 * @generated
	 * @ordered
	 */
	protected static final String CHAIN_CODEFOR_SUBCHAINS_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsOwnXOCLOp() <em>Is Own XOCL Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsOwnXOCLOp()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_OWN_XOCL_OP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNamedExpression() <em>Named Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedExpression()
	 * @generated
	 * @ordered
	 */
	protected EList<MNamedExpression> namedExpression;

	/**
	 * The cached value of the '{@link #getNamedTuple() <em>Named Tuple</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedTuple()
	 * @generated
	 * @ordered
	 */
	protected EList<MAbstractNamedTuple> namedTuple;

	/**
	 * The cached value of the '{@link #getNamedConstant() <em>Named Constant</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedConstant()
	 * @generated
	 * @ordered
	 */
	protected EList<MNamedConstant> namedConstant;

	/**
	 * The default value of the '{@link #getExpectedReturnSimpleTypeOfAnnotation() <em>Expected Return Simple Type Of Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleTypeOfAnnotation()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getIsReturnValueOfAnnotationMandatory() <em>Is Return Value Of Annotation Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsReturnValueOfAnnotationSingular() <em>Is Return Value Of Annotation Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getUseExplicitOcl() <em>Use Explicit Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseExplicitOcl()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USE_EXPLICIT_OCL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUseExplicitOcl() <em>Use Explicit Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseExplicitOcl()
	 * @generated
	 * @ordered
	 */
	protected Boolean useExplicitOcl = USE_EXPLICIT_OCL_EDEFAULT;

	/**
	 * This is true if the Use Explicit Ocl attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean useExplicitOclESet;

	/**
	 * The default value of the '{@link #getOclChanged() <em>Ocl Changed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclChanged()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean OCL_CHANGED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getOclCode() <em>Ocl Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclCode()
	 * @generated
	 * @ordered
	 */
	protected static final String OCL_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MExprAnnotationAction DO_ACTION_EDEFAULT = MExprAnnotationAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #oclChanged$Update <em>Ocl Changed$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #oclChanged$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression oclChanged$UpdateecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateannotationsMExprAnnotationActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdateannotationsMExprAnnotationActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #variableFromExpression <em>Variable From Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #variableFromExpression
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression variableFromExpressionexpressionsMNamedExpressionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #autoCastWithProc <em>Auto Cast With Proc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #autoCastWithProc
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression autoCastWithProcBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForVariables <em>As Code For Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForVariables
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForVariablesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorDefinition$Update <em>Processor Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorDefinition$UpdateexpressionsMProcessorDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ownToApplyMismatch <em>Own To Apply Mismatch</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ownToApplyMismatch
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ownToApplyMismatchBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #uniqueChainNumber <em>Unique Chain Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #uniqueChainNumber
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression uniqueChainNumberBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #reuseFromOtherNoMoreUsedChain <em>Reuse From Other No More Used Chain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #reuseFromOtherNoMoreUsedChain
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression reuseFromOtherNoMoreUsedChainexpressionsMBaseChainBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #resetToBase <em>Reset To Base</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #resetToBase
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression resetToBaseexpressionsExpressionBasemcoreMVariableBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #reuseFromOtherNoMoreUsedChainAsUpdate <em>Reuse From Other No More Used Chain As Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #reuseFromOtherNoMoreUsedChainAsUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression reuseFromOtherNoMoreUsedChainAsUpdateexpressionsMBaseChainBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isProcessorCheckEqualOperator <em>Is Processor Check Equal Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProcessorCheckEqualOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isProcessorCheckEqualOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isPrefixProcessor <em>Is Prefix Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrefixProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isPrefixProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isPostfixProcessor <em>Is Postfix Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPostfixProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isPostfixProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForOthers <em>As Code For Others</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForOthers
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForOthersBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeElementAsCode <em>Unsafe Element As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeElementAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeElementAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength1 <em>Code For Length1</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength1
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength1BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength2 <em>Code For Length2</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength2
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength2BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength3 <em>Code For Length3</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength3
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength3BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #length <em>Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #length
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression lengthBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainStepAsCode <em>Unsafe Chain Step As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainStepAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainAsCode <em>Unsafe Chain As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainAsCode <em>Unsafe Chain As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procAsCode <em>Proc As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procAsCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isCustomCodeProcessor <em>Is Custom Code Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCustomCodeProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isCustomCodeProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isProcessorSetOperator <em>Is Processor Set Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProcessorSetOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isProcessorSetOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOwnXOCLOperator <em>Is Own XOCL Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOwnXOCLOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOwnXOCLOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorReturnsSingular <em>Processor Returns Singular</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorReturnsSingular
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorReturnsSingularBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorIsSet <em>Processor Is Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorIsSet
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorIsSetBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #createProcessorDefinition <em>Create Processor Definition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #createProcessorDefinition
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression createProcessorDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForObject <em>Proc Def Choices For Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForObjects <em>Proc Def Choices For Objects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForObjects
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForObjectsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForBoolean <em>Proc Def Choices For Boolean</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForBoolean
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForBooleanBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForBooleans <em>Proc Def Choices For Booleans</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForBooleans
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForBooleansBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForInteger <em>Proc Def Choices For Integer</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForInteger
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForIntegerBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForIntegers <em>Proc Def Choices For Integers</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForIntegers
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForIntegersBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForReal <em>Proc Def Choices For Real</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForReal
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForRealBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForReals <em>Proc Def Choices For Reals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForReals
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForRealsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForString <em>Proc Def Choices For String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForStrings <em>Proc Def Choices For Strings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForStrings
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForStringsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForDate <em>Proc Def Choices For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForDate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForDateBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForDates <em>Proc Def Choices For Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForDates
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForDatesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #baseDefinition$Update <em>Base Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #baseDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression baseDefinition$UpdateexpressionsMAbstractBaseDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForBuiltIn <em>As Code For Built In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForBuiltIn
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForBuiltInBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForConstants <em>As Code For Constants</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForConstants
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForConstantsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #getShortCode <em>Get Short Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getShortCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #getScope <em>Get Scope</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getScopeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #multiplicityCase$Update <em>Multiplicity Case$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #multiplicityCase$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression multiplicityCase$UpdatemcoreMultiplicityCaseBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #typeAsOcl <em>Type As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #typeAsOcl
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMultiplicityAsString <em>Multiplicity As String</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityAsString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression multiplicityAsStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedTypePackage <em>Calculated Type Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedTypePackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedTypePackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getVoidTypeAllowed <em>Void Type Allowed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoidTypeAllowed
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression voidTypeAllowedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMultiplicityCase <em>Multiplicity Case</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityCase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression multiplicityCaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsCode <em>As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollector <em>Collector</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollector
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression collectorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEntireScope <em>Entire Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntireScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression entireScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeBase <em>Scope Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSelf <em>Scope Self</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSelf
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSelfDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeTrg <em>Scope Trg</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeTrg
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeTrgDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeObj <em>Scope Obj</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeObj
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeObjDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSimpleTypeConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSimpleTypeConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeLiteralConstants <em>Scope Literal Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeLiteralConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeLiteralConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeObjectReferenceConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeObjectReferenceConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeVariables <em>Scope Variables</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeVariables
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeVariablesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeIterator <em>Scope Iterator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeIterator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeIteratorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeAccumulator <em>Scope Accumulator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeAccumulator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeAccumulatorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeParameters <em>Scope Parameters</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeParameters
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeParametersDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeContainerBase <em>Scope Container Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeContainerBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeContainerBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeNumberBase <em>Scope Number Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeNumberBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeNumberBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSource <em>Scope Source</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalEntireScope <em>Local Entire Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalEntireScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localEntireScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeBase <em>Local Scope Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSelf <em>Local Scope Self</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSelf
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSelfDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeTrg <em>Local Scope Trg</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeTrg
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeTrgDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeObj <em>Local Scope Obj</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeObj
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeObjDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSource <em>Local Scope Source</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSimpleTypeConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSimpleTypeConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeLiteralConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeLiteralConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeObjectReferenceConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeObjectReferenceConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeVariables <em>Local Scope Variables</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeVariables
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeVariablesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeIterator <em>Local Scope Iterator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeIterator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeIteratorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeAccumulator <em>Local Scope Accumulator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeAccumulator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeAccumulatorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeParameters <em>Local Scope Parameters</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeParameters
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeParametersDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeNumberBaseDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeNumberBaseDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeContainer <em>Local Scope Container</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeContainer
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeContainerDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAnnotation <em>Containing Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingExpression <em>Containing Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsSubExpression <em>Is Sub Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isSubExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnType <em>Calculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSelfObjectType <em>Self Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression selfObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSelfObjectPackage <em>Self Object Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression selfObjectPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetObjectType <em>Target Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression targetObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetSimpleType <em>Target Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression targetSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectObjectType <em>Object Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnType <em>Expected Return Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression expectedReturnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnSimpleType <em>Expected Return Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression expectedReturnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueSingular <em>Is Return Value Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSrcObjectType <em>Src Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression srcObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSrcObjectSimpleType <em>Src Object Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcObjectSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression srcObjectSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseAsCode <em>Base As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseDefinition <em>Base Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getBaseDefinition <em>Base Definition</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDefinition
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression baseDefinitionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitType <em>Base Exit Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitSimpleType <em>Base Exit Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitMandatory <em>Base Exit Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitSingular <em>Base Exit Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainEntryType <em>Chain Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainEntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainEntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainAsCode <em>Chain As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getElement1 <em>Element1</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression element1ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement1Correct <em>Element1 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1Correct
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element1CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement2EntryType <em>Element2 Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2EntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element2EntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getElement2 <em>Element2</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression element2ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement2Correct <em>Element2 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2Correct
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element2CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement3EntryType <em>Element3 Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3EntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element3EntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getElement3 <em>Element3</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression element3ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement3Correct <em>Element3 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3Correct
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element3CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getCastType <em>Cast Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCastType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression castTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLastElement <em>Last Element</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastElement
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression lastElementDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedType <em>Chain Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainCalculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedSimpleType <em>Chain Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainCalculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedSingular <em>Chain Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainCalculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getProcessorDefinition <em>Processor Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression processorDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getProcessorDefinition <em>Processor Definition</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorDefinition
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression processorDefinitionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTypeMismatch <em>Type Mismatch</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeMismatch
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression typeMismatchDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCodeforSubchains <em>Chain Codefor Subchains</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCodeforSubchains
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainCodeforSubchainsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsOwnXOCLOp <em>Is Own XOCL Op</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsOwnXOCLOp
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isOwnXOCLOpDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnTypeOfAnnotation <em>Expected Return Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression expectedReturnTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnSimpleTypeOfAnnotation <em>Expected Return Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression expectedReturnSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOclChanged <em>Ocl Changed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclChanged
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression oclChangedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOclCode <em>Ocl Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression oclCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MExprAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MEXPR_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMultiplicityAsString() {
		/**
		 * @OCL (if self.calculatedMandatory then '[1..' else '[0..' endif)
		.concat(if self.calculatedSingular then '1]' else '*]' endif)
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__MULTIPLICITY_AS_STRING;

		if (multiplicityAsStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				multiplicityAsStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(multiplicityAsStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCalculatedType() {
		MClassifier calculatedType = basicGetCalculatedType();
		return calculatedType != null && calculatedType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) calculatedType)
				: calculatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCalculatedType() {
		/**
		 * @OCL let res:MClassifier=null in res
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedMandatory() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedSingular() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getCalculatedTypePackage() {
		MPackage calculatedTypePackage = basicGetCalculatedTypePackage();
		return calculatedTypePackage != null && calculatedTypePackage.eIsProxy()
				? (MPackage) eResolveProxy(
						(InternalEObject) calculatedTypePackage)
				: calculatedTypePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetCalculatedTypePackage() {
		/**
		 * @OCL if self.calculatedType.oclIsUndefined() then null else self.calculatedType.containingPackage endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE_PACKAGE;

		if (calculatedTypePackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedTypePackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypePackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getVoidTypeAllowed() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__VOID_TYPE_ALLOWED;

		if (voidTypeAllowedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				voidTypeAllowedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(voidTypeAllowedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getCalculatedSimpleType() {
		/**
		 * @OCL mcore::SimpleType::None
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityCase getMultiplicityCase() {
		/**
		 * @OCL if self.calculatedMandatory and self.calculatedSingular
		then MultiplicityCase::OneOne
		else if (not self.calculatedMandatory) and self.calculatedSingular
		then MultiplicityCase::ZeroOne
		else if self.calculatedMandatory and (not self.calculatedSingular)
		then MultiplicityCase::OneMany
		else MultiplicityCase::ZeroMany endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__MULTIPLICITY_CASE;

		if (multiplicityCaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				multiplicityCaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(multiplicityCaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MultiplicityCase result = (MultiplicityCase) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplicityCase(MultiplicityCase newMultiplicityCase) {
		if (newMultiplicityCase == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsCode() {
		/**
		 * @OCL if (let e: Boolean = collector.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then asBasicCode
		else if collector.oclIsUndefined()
		then null
		else collector.asBasicCode
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE;

		if (asCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				asCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsBasicCode() {
		/**
		 * @OCL eClass().name.concat(' : <?>')
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getCollector() {
		MCollectionExpression collector = basicGetCollector();
		return collector != null && collector.eIsProxy()
				? (MCollectionExpression) eResolveProxy(
						(InternalEObject) collector)
				: collector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetCollector() {
		/**
		 * @OCL let nl: mcore::expressions::MCollectionExpression = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR;

		if (collectorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				collectorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MCollectionExpression result = (MCollectionExpression) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getEntireScope() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localEntireScope 
		else /* Note this is necessary for some cases like scopeIterator that otherwise won't work *\/ 
		scopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(
		scopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeNumberBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeContainerBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ENTIRE_SCOPE;

		if (entireScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				entireScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(entireScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAbstractBaseDefinition> result = (EList<MAbstractBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MBaseDefinition> getScopeBase() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeBase 
		else containingExpression.scopeBase
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_BASE;

		if (scopeBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MBaseDefinition> result = (EList<MBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSelfBaseDefinition> getScopeSelf() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeSelf
		else containingExpression.scopeSelf
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SELF;

		if (scopeSelfDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeSelfDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSelfDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSelfBaseDefinition> result = (EList<MSelfBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTargetBaseDefinition> getScopeTrg() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeTrg
		else containingExpression.scopeTrg
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_TRG;

		if (scopeTrgDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeTrgDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeTrgDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTargetBaseDefinition> result = (EList<MTargetBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectBaseDefinition> getScopeObj() {
		/**
		 * @OCL --todo: only for updates...
		if containingExpression.oclIsUndefined() then localScopeObj else containingExpression.scopeObj endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJ;

		if (scopeObjDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeObjDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeObjDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectBaseDefinition> result = (EList<MObjectBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSimpleTypeConstantBaseDefinition> getScopeSimpleTypeConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeSimpleTypeConstants
		else containingExpression.scopeSimpleTypeConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS;

		if (scopeSimpleTypeConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeSimpleTypeConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSimpleTypeConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSimpleTypeConstantBaseDefinition> result = (EList<MSimpleTypeConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralConstantBaseDefinition> getScopeLiteralConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeLiteralConstants
		else containingExpression.scopeLiteralConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS;

		if (scopeLiteralConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeLiteralConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeLiteralConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralConstantBaseDefinition> result = (EList<MLiteralConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReferenceConstantBaseDefinition> getScopeObjectReferenceConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeObjectReferenceConstants
		else containingExpression.scopeObjectReferenceConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS;

		if (scopeObjectReferenceConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeObjectReferenceConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(scopeObjectReferenceConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReferenceConstantBaseDefinition> result = (EList<MObjectReferenceConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MVariableBaseDefinition> getScopeVariables() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeVariables
		else containingExpression.scopeVariables
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_VARIABLES;

		if (scopeVariablesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeVariablesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeVariablesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MVariableBaseDefinition> result = (EList<MVariableBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MIteratorBaseDefinition> getScopeIterator() {
		/**
		 * @OCL /* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr *\/
		
		if containingExpression.oclIsUndefined()
		then localScopeIterator
		else containingExpression.scopeIterator->union(localScopeIterator)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ITERATOR;

		if (scopeIteratorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeIteratorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeIteratorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MIteratorBaseDefinition> result = (EList<MIteratorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAccumulatorBaseDefinition> getScopeAccumulator() {
		/**
		 * @OCL /* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr *\/
		
		if containingExpression.oclIsUndefined()
		then localScopeAccumulator
		else containingExpression.scopeAccumulator->union(localScopeAccumulator)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR;

		if (scopeAccumulatorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeAccumulatorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeAccumulatorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAccumulatorBaseDefinition> result = (EList<MAccumulatorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameterBaseDefinition> getScopeParameters() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeParameters
		else containingExpression.scopeParameters
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS;

		if (scopeParametersDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeParametersDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeParametersDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MParameterBaseDefinition> result = (EList<MParameterBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MContainerBaseDefinition> getScopeContainerBase() {
		/**
		 * @OCL localScopeContainer
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE;

		if (scopeContainerBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeContainerBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeContainerBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MContainerBaseDefinition> result = (EList<MContainerBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNumberBaseDefinition> getScopeNumberBase() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then self.localScopeNumberBaseDefinition
		else containingExpression.scopeNumberBase
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE;

		if (scopeNumberBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeNumberBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeNumberBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MNumberBaseDefinition> result = (EList<MNumberBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSourceBaseDefinition> getScopeSource() {
		/**
		 * @OCL 
		if containingExpression.oclIsUndefined() then localScopeSource else containingExpression.scopeSource endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SOURCE;

		if (scopeSourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeSourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSourceBaseDefinition> result = (EList<MSourceBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getLocalEntireScope() {
		/**
		 * @OCL localScopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(
		localScopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeNumberBaseDefinition->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeContainer->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE;

		if (localEntireScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localEntireScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localEntireScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAbstractBaseDefinition> result = (EList<MAbstractBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MBaseDefinition> getLocalScopeBase() {
		/**
		 * @OCL OrderedSet{
		Tuple{expressionBase = mcore::expressions::ExpressionBase::NullValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::FalseValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::TrueValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyStringValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyCollection}  
		}
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE;

		if (localScopeBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MBaseDefinition> result = (EList<MBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSelfBaseDefinition> getLocalScopeSelf() {
		/**
		 * @OCL OrderedSet{Tuple{debug=''}}
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF;

		if (localScopeSelfDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeSelfDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSelfDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSelfBaseDefinition> result = (EList<MSelfBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTargetBaseDefinition> getLocalScopeTrg() {
		/**
		 * @OCL if targetObjectType.oclIsUndefined() and self.targetSimpleType=mcore::SimpleType::None
		then OrderedSet{}
		else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG;

		if (localScopeTrgDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeTrgDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeTrgDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTargetBaseDefinition> result = (EList<MTargetBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectBaseDefinition> getLocalScopeObj() {
		/**
		 * @OCL if objectObjectType.oclIsUndefined() then OrderedSet{} else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ;

		if (localScopeObjDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeObjDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeObjDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectBaseDefinition> result = (EList<MObjectBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSourceBaseDefinition> getLocalScopeSource() {
		/**
		 * @OCL if srcObjectType.oclIsUndefined() and self.srcObjectSimpleType=mcore::SimpleType::None
		then OrderedSet{}
		else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE;

		if (localScopeSourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeSourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSourceBaseDefinition> result = (EList<MSourceBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSimpleTypeConstantBaseDefinition> getLocalScopeSimpleTypeConstants() {
		/**
		 * @OCL /*if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)
		then let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in*\/
		let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mcore::expressions::MSimpleTypeConstantLet) and (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/* else OrderedSet{} endif
		endif*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;

		if (localScopeSimpleTypeConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeSimpleTypeConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(localScopeSimpleTypeConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSimpleTypeConstantBaseDefinition> result = (EList<MSimpleTypeConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralConstantBaseDefinition> getLocalScopeLiteralConstants() {
		/**
		 * @OCL /* if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)
		then let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in *\/
		let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mcore::expressions::MLiteralLet) and (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/*
		else OrderedSet{} endif
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS;

		if (localScopeLiteralConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeLiteralConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeLiteralConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralConstantBaseDefinition> result = (EList<MLiteralConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReferenceConstantBaseDefinition> getLocalScopeObjectReferenceConstants() {
		/**
		 * @OCL /*if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)
		then let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in *\/
		let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		
		a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mcore::expressions::MObjectReferenceLet) and (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/* else OrderedSet{} endif
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;

		if (localScopeObjectReferenceConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeObjectReferenceConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(localScopeObjectReferenceConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReferenceConstantBaseDefinition> result = (EList<MObjectReferenceConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MVariableBaseDefinition> getLocalScopeVariables() {
		/**
		 * @OCL let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined()
		then  OrderedSet{} 
		else
		a.namedExpression->iterate (i: mcore::expressions::MNamedExpression; r: OrderedSet(Tuple(namedExpression:mcore::expressions::MNamedExpression))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if  (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedExpression=i}) 
		else r 
		endif
		endif
		) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES;

		if (localScopeVariablesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeVariablesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeVariablesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MVariableBaseDefinition> result = (EList<MVariableBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MIteratorBaseDefinition> getLocalScopeIterator() {
		/**
		 * @OCL if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)
		then let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in
		if c.iteratorVar.oclIsUndefined() then OrderedSet{}
		else OrderedSet{Tuple{iterator=c.iteratorVar}} endif
		else OrderedSet{} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR;

		if (localScopeIteratorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeIteratorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeIteratorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MIteratorBaseDefinition> result = (EList<MIteratorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAccumulatorBaseDefinition> getLocalScopeAccumulator() {
		/**
		 * @OCL if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)
		then let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in
		if c.accumulatorVar.oclIsUndefined() then OrderedSet{}
		else OrderedSet{Tuple{accumulator=c.accumulatorVar}} endif
		else OrderedSet{} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR;

		if (localScopeAccumulatorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeAccumulatorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeAccumulatorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAccumulatorBaseDefinition> result = (EList<MAccumulatorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameterBaseDefinition> getLocalScopeParameters() {
		/**
		 * @OCL if self.containingAnnotation=null 
		then OrderedSet{}
		else 
		let e:MModelElement= self.containingAnnotation.annotatedElement in
		if e=null 
		then OrderedSet{}
		else if not e.oclIsKindOf(mcore::MOperationSignature)
		then OrderedSet{}
		else e.oclAsType(mcore::MOperationSignature).parameter
		 ->iterate (i: mcore::MParameter; r: OrderedSet(Tuple(parameter:mcore::MParameter))=OrderedSet{} 
		                |r->append(Tuple{parameter=i}) )
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS;

		if (localScopeParametersDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeParametersDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeParametersDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MParameterBaseDefinition> result = (EList<MParameterBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNumberBaseDefinition> getLocalScopeNumberBaseDefinition() {
		/**
		 * @OCL OrderedSet{
		Tuple{expressionBase = mcore::expressions::ExpressionBase::ZeroValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::OneValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::MinusOneValue}
		}
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;

		if (localScopeNumberBaseDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeNumberBaseDefinitionDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(localScopeNumberBaseDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MNumberBaseDefinition> result = (EList<MNumberBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MContainerBaseDefinition> getLocalScopeContainer() {
		/**
		 * @OCL OrderedSet{Tuple{debug=''}}
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER;

		if (localScopeContainerDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeContainerDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeContainerDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MContainerBaseDefinition> result = (EList<MContainerBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotation getContainingAnnotation() {
		MExprAnnotation containingAnnotation = basicGetContainingAnnotation();
		return containingAnnotation != null && containingAnnotation.eIsProxy()
				? (MExprAnnotation) eResolveProxy(
						(InternalEObject) containingAnnotation)
				: containingAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotation basicGetContainingAnnotation() {
		/**
		 * @OCL -- expressions can now be as well annotations, so the "containing" annotation can be self.
		if self.oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then self.oclAsType(mcore::annotations::MExprAnnotation) 
		else if eContainer().oclIsUndefined() 
		then null 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then eContainer().oclAsType(mcore::annotations::MExprAnnotation) 
		else if eContainer().oclIsTypeOf(mcore::expressions::MNamedExpression) 
		then self.eContainer().oclAsType(mcore::expressions::MNamedExpression).containingAnnotation
		else if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue) 
		then eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue)
		.eContainer().oclAsType(mcore::expressions::MNewObjecct).containingAnnotation
		else if containingExpression.oclIsUndefined() 
		then null 
		else containingExpression.containingAnnotation endif endif endif endif endif	endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION;

		if (containingAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MExprAnnotation result = (MExprAnnotation) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression getContainingExpression() {
		MAbstractExpression containingExpression = basicGetContainingExpression();
		return containingExpression != null && containingExpression.eIsProxy()
				? (MAbstractExpression) eResolveProxy(
						(InternalEObject) containingExpression)
				: containingExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression basicGetContainingExpression() {
		/**
		 * @OCL if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression)
		then eContainer().oclAsType(mcore::expressions::MAbstractExpression)
		
		else null endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION;

		if (containingExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractExpression result = (MAbstractExpression) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsComplexExpression() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsSubExpression() {
		/**
		 * @OCL if containingExpression.oclIsUndefined() then false
		else not (containingExpression.oclIsKindOf(mcore::expressions::MNamedConstant) or containingExpression.oclIsKindOf(mcore::expressions::MNamedExpression)) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION;

		if (isSubExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isSubExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isSubExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCalculatedOwnType() {
		MClassifier calculatedOwnType = basicGetCalculatedOwnType();
		return calculatedOwnType != null && calculatedOwnType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) calculatedOwnType)
				: calculatedOwnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCalculatedOwnType() {
		/**
		 * @OCL let nl: mcore::MClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;

		if (calculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedOwnMandatory() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedOwnSingular() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getCalculatedOwnSimpleType() {
		/**
		 * @OCL mcore::SimpleType::None
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;

		if (calculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getSelfObjectType() {
		MClassifier selfObjectType = basicGetSelfObjectType();
		return selfObjectType != null && selfObjectType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) selfObjectType)
				: selfObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetSelfObjectType() {
		/**
		 * @OCL let a:mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then null
		else a.selfObjectTypeOfAnnotation endif
		
		/*if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then eContainer().oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation
		--else if self.oclsKindOf(mcore::annotations::MExprAnnotation)
		--   then self.oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation
		
		--else  if  eContainer().oclIsTypeOf(mcore::expressions::MAbstractExpression) then
		-- eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType
		else if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue)
		then 
		if eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclIsKindOf(mcore::expressions::MNewObjecct)
		then  eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclAsType(mcore::expressions::MNewObjecct).selfObjectType
		else 
		null
		endif
		else eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType
		endif
		--endif
		endif
		endif 
		--endif
		*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE;

		if (selfObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				selfObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(selfObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getSelfObjectPackage() {
		MPackage selfObjectPackage = basicGetSelfObjectPackage();
		return selfObjectPackage != null && selfObjectPackage.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) selfObjectPackage)
				: selfObjectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetSelfObjectPackage() {
		/**
		 * @OCL if (let e: Boolean = selfObjectType.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if selfObjectType.oclIsUndefined()
		then null
		else selfObjectType.containingPackage
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE;

		if (selfObjectPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				selfObjectPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(selfObjectPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getTargetObjectType() {
		MClassifier targetObjectType = basicGetTargetObjectType();
		return targetObjectType != null && targetObjectType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) targetObjectType)
				: targetObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetTargetObjectType() {
		/**
		 * @OCL if self.containingAnnotation.oclIsUndefined()
		then null
		else self.containingAnnotation.targetObjectTypeOfAnnotation endif
		/*   
		if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then eContainer().oclAsType(mcore::annotations::MExprAnnotation).targetObjectTypeOfAnnotation
		else  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) then
		eContainer().oclAsType(mcore::expressions::MAbstractExpression).targetObjectType else null endif
		endif
		endif *\/
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE;

		if (targetObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				targetObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(targetObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getTargetSimpleType() {
		/**
		 * @OCL if self.containingAnnotation.oclIsUndefined()
		then null
		else self.containingAnnotation.targetSimpleTypeOfAnnotation endif
		/*
		if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.targetSimpleTypeOfAnnotation
		endif
		endif
		*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE;

		if (targetSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				targetSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(targetSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getObjectObjectType() {
		MClassifier objectObjectType = basicGetObjectObjectType();
		return objectObjectType != null && objectObjectType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) objectObjectType)
				: objectObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetObjectObjectType() {
		/**
		 * @OCL if self.oclIsKindOf(mcore::annotations::MUpdateValue)  
		--replaced with self.
		then self.oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation
		else if self.oclIsKindOf(mcore::annotations::MUpdatePersistence)
		then self.oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation
		else if eContainer().oclIsUndefined()  then null 
		else if eContainer().oclIsKindOf(mcore::annotations::MUpdateValue)  
		then eContainer().oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation 
		else if eContainer().oclIsKindOf(mcore::annotations::MUpdatePersistence)  
		then eContainer().oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation   
		else  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) 
		then eContainer().oclAsType(mcore::expressions::MAbstractExpression).objectObjectType 
		else null endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE;

		if (objectObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				objectObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(objectObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getExpectedReturnType() {
		MClassifier expectedReturnType = basicGetExpectedReturnType();
		return expectedReturnType != null && expectedReturnType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) expectedReturnType)
				: expectedReturnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetExpectedReturnType() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.expectedReturnTypeOfAnnotation
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE;

		if (expectedReturnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				expectedReturnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(expectedReturnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getExpectedReturnSimpleType() {
		/**
		 * @OCL if containingAnnotation.oclIsUndefined() 
		then mcore::SimpleType::None
		else containingAnnotation.expectedReturnSimpleTypeOfAnnotation endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE;

		if (expectedReturnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				expectedReturnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(expectedReturnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueMandatory() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.isReturnValueOfAnnotationMandatory
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY;

		if (isReturnValueMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReturnValueMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueSingular() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.isReturnValueOfAnnotationSingular
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR;

		if (isReturnValueSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReturnValueSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getSrcObjectType() {
		MClassifier srcObjectType = basicGetSrcObjectType();
		return srcObjectType != null && srcObjectType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) srcObjectType)
				: srcObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetSrcObjectType() {
		/**
		 * @OCL let srcType : MClassifier =
		let srcClassifier:MProperty = 
		self.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty
		in 
		if srcClassifier.oclIsUndefined() then null
		else if  ((srcClassifier.hasStorage) and (srcClassifier.changeable)) then srcClassifier.type
		else null 
		endif
		endif
		in
		if srcType.oclIsUndefined() then null else srcType endif
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE;

		if (srcObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				srcObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(srcObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSrcObjectSimpleType() {
		/**
		 * @OCL let srcType : SimpleType =
		let srcClassifier:MProperty = 
		self.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty
		in 
		if srcClassifier.oclIsUndefined() then null
		else if  (srcClassifier.hasStorage) and ( srcClassifier.changeable) then srcClassifier.simpleType
		else null
		endif endif
		in
		if srcType.oclIsUndefined() then SimpleType::None else srcType endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE;

		if (srcObjectSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				srcObjectSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(srcObjectSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBaseAsCode() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then ''
		else if baseDefinition.oclIsTypeOf(mcore::expressions::MNumberBaseDefinition)
		then  baseDefinition.oclAsType(mcore::expressions::MNumberBaseDefinition).calculateAsCode(self)
		else baseDefinition.calculatedAsCode
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE;

		if (baseAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionBase getBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(ExpressionBase newBase) {
		ExpressionBase oldBase = base;
		base = newBase == null ? BASE_EDEFAULT : newBase;
		boolean oldBaseESet = baseESet;
		baseESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__BASE, oldBase, base,
					!oldBaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBase() {
		ExpressionBase oldBase = base;
		boolean oldBaseESet = baseESet;
		base = BASE_EDEFAULT;
		baseESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__BASE, oldBase,
					BASE_EDEFAULT, oldBaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBase() {
		return baseESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinition getBaseDefinition() {
		MAbstractBaseDefinition baseDefinition = basicGetBaseDefinition();
		return baseDefinition != null && baseDefinition.eIsProxy()
				? (MAbstractBaseDefinition) eResolveProxy(
						(InternalEObject) baseDefinition)
				: baseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinition basicGetBaseDefinition() {
		/**
		 * @OCL let r: mcore::expressions::MAbstractBaseDefinition =
		
		if base=mcore::expressions::ExpressionBase::Undefined 
		then null
		else if base=mcore::expressions::ExpressionBase::SelfObject
		then scopeSelf->first() 
		else if base=mcore::expressions::ExpressionBase::EContainer
		then scopeContainerBase->first() 
		else if base=mcore::expressions::ExpressionBase::Target
		then scopeTrg->first() 
		else if base=mcore::expressions::ExpressionBase::ObjectObject
		then scopeObj->first() 
		else if base=mcore::expressions::ExpressionBase::Source
		then scopeSource->first() 
		else if base=mcore::expressions::ExpressionBase::ConstantValue 
		then scopeSimpleTypeConstants->select(c:mcore::expressions::MSimpleTypeConstantBaseDefinition | c.namedConstant = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::ConstantLiteralValue
		then scopeLiteralConstants->select(c:mcore::expressions::MLiteralConstantBaseDefinition | c.namedConstant = baseVar)->first() 
		else if base=mcore::expressions::ExpressionBase::ConstantObjectReference
		then scopeObjectReferenceConstants->select(c:mcore::expressions::MObjectReferenceConstantBaseDefinition | c.namedConstant = baseVar)->first()
		else if base=mcore::expressions::ExpressionBase::Variable
		then scopeVariables->select(c:mcore::expressions::MVariableBaseDefinition | c.namedExpression = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::Parameter
		then scopeParameters->select(c:mcore::expressions::MParameterBaseDefinition | c.parameter = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::Iterator
		then scopeIterator->select(c:mcore::expressions::MIteratorBaseDefinition | c.iterator = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::Accumulator
		then scopeAccumulator->select(c:mcore::expressions::MAccumulatorBaseDefinition | c.accumulator = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::OneValue or base=mcore::expressions::ExpressionBase::ZeroValue  or base = mcore::expressions::ExpressionBase::MinusOneValue
		then scopeNumberBase->select(c:mcore::expressions::MNumberBaseDefinition|c.expressionBase = base)->first()
		else scopeBase->select(c:mcore::expressions::MBaseDefinition | c.expressionBase=base)->first() 
		endif endif endif endif endif endif endif endif endif endif endif endif endif endif
		in
		if r.oclIsUndefined() then null else r endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;

		if (baseDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractBaseDefinition result = (MAbstractBaseDefinition) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setBaseDefinition(MAbstractBaseDefinition newBaseDefinition) {
		/* NOTE: After you changed this you must also change basicGetBaseDefinition() in order to retrieve the selected base variable. */

		if (newBaseDefinition == null) {
			unsetBase();
		} else {
			setBase(newBaseDefinition.getCalculatedBase());
			if (newBaseDefinition instanceof MSimpleTypeConstantBaseDefinition) {
				MSimpleTypeConstantBaseDefinition stcbd = (MSimpleTypeConstantBaseDefinition) newBaseDefinition;
				setBaseVar(stcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MLiteralConstantBaseDefinition) {
				MLiteralConstantBaseDefinition lcbd = (MLiteralConstantBaseDefinition) newBaseDefinition;
				setBaseVar(lcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MObjectReferenceConstantBaseDefinition) {
				MObjectReferenceConstantBaseDefinition orcbd = (MObjectReferenceConstantBaseDefinition) newBaseDefinition;
				setBaseVar(orcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MVariableBaseDefinition) {
				MVariableBaseDefinition vbd = (MVariableBaseDefinition) newBaseDefinition;
				setBaseVar(vbd.getNamedExpression());
			} else if (newBaseDefinition instanceof MParameterBaseDefinition) {
				MParameterBaseDefinition pbd = (MParameterBaseDefinition) newBaseDefinition;
				setBaseVar(pbd.getParameter());
			} else if (newBaseDefinition instanceof MIteratorBaseDefinition) {
				MIteratorBaseDefinition ibd = (MIteratorBaseDefinition) newBaseDefinition;
				setBaseVar(ibd.getIterator());
			} else if (newBaseDefinition instanceof MAccumulatorBaseDefinition) {
				MAccumulatorBaseDefinition abd = (MAccumulatorBaseDefinition) newBaseDefinition;
				setBaseVar(abd.getAccumulator());
			}
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Base Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MAbstractBaseDefinition>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.getScope()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MAbstractBaseDefinition> evalBaseDefinitionChoiceConstruction(
			List<MAbstractBaseDefinition> choice) {
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		if (baseDefinitionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				baseDefinitionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"BaseDefinitionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(baseDefinitionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "BaseDefinitionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MAbstractBaseDefinition> result = new ArrayList<MAbstractBaseDefinition>(
					(Collection<MAbstractBaseDefinition>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariable getBaseVar() {
		if (baseVar != null && baseVar.eIsProxy()) {
			InternalEObject oldBaseVar = (InternalEObject) baseVar;
			baseVar = (MVariable) eResolveProxy(oldBaseVar);
			if (baseVar != oldBaseVar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR,
							oldBaseVar, baseVar));
			}
		}
		return baseVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariable basicGetBaseVar() {
		return baseVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseVar(MVariable newBaseVar) {
		MVariable oldBaseVar = baseVar;
		baseVar = newBaseVar;
		boolean oldBaseVarESet = baseVarESet;
		baseVarESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR, oldBaseVar,
					baseVar, !oldBaseVarESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBaseVar() {
		MVariable oldBaseVar = baseVar;
		boolean oldBaseVarESet = baseVarESet;
		baseVar = null;
		baseVarESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR, oldBaseVar,
					null, oldBaseVarESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBaseVar() {
		return baseVarESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getBaseExitType() {
		MClassifier baseExitType = basicGetBaseExitType();
		return baseExitType != null && baseExitType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) baseExitType)
				: baseExitType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetBaseExitType() {
		/**
		 * @OCL if baseDefinition.oclIsUndefined() 
		then null
		else 
		/* Trick b/c we need to reference the container here and baseDefinition is generated on the fly *\/
		if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::SelfObject
		then self.selfObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target
		then self.targetObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::ObjectObject
		then self.objectObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Source
		then self.srcObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::EContainer
		then  if self.selfObjectType.allClassesContainedIn()->size() = 1 then self.selfObjectType.allClassesContainedIn()->first() else null endif
		else baseDefinition.calculatedType
		endif endif endif endif
		endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE;

		if (baseExitTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getBaseExitSimpleType() {
		/**
		 * @OCL if baseDefinition.oclIsUndefined()  or not (self.baseExitType.oclIsUndefined())
		then mcore::SimpleType::None
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target
		then self.targetSimpleType
		else self.baseDefinition.calculatedSimpleType
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE;

		if (baseExitSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBaseExitMandatory() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if baseDefinition.oclIsUndefined()
		then null
		else baseDefinition.calculatedMandatory
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY;

		if (baseExitMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBaseExitSingular() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then true
		else if baseDefinition.oclIsUndefined()
		then null
		else baseDefinition.calculatedSingular
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR;

		if (baseExitSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getChainEntryType() {
		MClassifier chainEntryType = basicGetChainEntryType();
		return chainEntryType != null && chainEntryType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) chainEntryType)
				: chainEntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetChainEntryType() {
		/**
		 * @OCL let c:MClassifier=null in c
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE;

		if (chainEntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainEntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainEntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChainAsCode() {
		/**
		 * @OCL unsafeChainAsCode(1)
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE;

		if (chainAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getElement1() {
		if (element1 != null && element1.eIsProxy()) {
			InternalEObject oldElement1 = (InternalEObject) element1;
			element1 = (MProperty) eResolveProxy(oldElement1);
			if (element1 != oldElement1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1,
							oldElement1, element1));
			}
		}
		return element1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetElement1() {
		return element1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement1(MProperty newElement1) {
		MProperty oldElement1 = element1;
		element1 = newElement1;
		boolean oldElement1ESet = element1ESet;
		element1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1, oldElement1,
					element1, !oldElement1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElement1() {
		MProperty oldElement1 = element1;
		boolean oldElement1ESet = element1ESet;
		element1 = null;
		element1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1, oldElement1,
					null, oldElement1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElement1() {
		return element1ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Element1</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 
	let annotatedProp: mcore::MProperty = 
	self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
	in
	if self.chainEntryType.oclIsUndefined() then 
	OrderedSet{} else
	self.chainEntryType.allProperties()
	endif
	
	--
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalElement1ChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		if (element1ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				element1ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"Element1ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(element1ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "Element1ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement1Correct() {
		/**
		 * @OCL if element1.oclIsUndefined() then true else
		--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else
		if chainEntryType.oclIsUndefined() then false 
		else chainEntryType.allProperties()->includes(element1)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1_CORRECT;

		if (element1CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element1CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element1CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getElement2EntryType() {
		MClassifier element2EntryType = basicGetElement2EntryType();
		return element2EntryType != null && element2EntryType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) element2EntryType)
				: element2EntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetElement2EntryType() {
		/**
		 * @OCL if not self.element1Correct then null
		else 
		if self.element1.oclIsUndefined() then null 
		else self.element1.type endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE;

		if (element2EntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element2EntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element2EntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getElement2() {
		if (element2 != null && element2.eIsProxy()) {
			InternalEObject oldElement2 = (InternalEObject) element2;
			element2 = (MProperty) eResolveProxy(oldElement2);
			if (element2 != oldElement2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2,
							oldElement2, element2));
			}
		}
		return element2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetElement2() {
		return element2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement2(MProperty newElement2) {
		MProperty oldElement2 = element2;
		element2 = newElement2;
		boolean oldElement2ESet = element2ESet;
		element2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2, oldElement2,
					element2, !oldElement2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElement2() {
		MProperty oldElement2 = element2;
		boolean oldElement2ESet = element2ESet;
		element2 = null;
		element2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2, oldElement2,
					null, oldElement2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElement2() {
		return element2ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Element2</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let annotatedProp: MProperty = 
	self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
	in
	
	if element1.oclIsUndefined() 
	then OrderedSet{}
	else if element1.isOperation
	then OrderedSet{} 
	else if element2EntryType.oclIsUndefined() 
	  then OrderedSet{}
	  else element2EntryType.allProperties() endif
	 endif
	endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalElement2ChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		if (element2ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				element2ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"Element2ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(element2ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "Element2ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement2Correct() {
		/**
		 * @OCL if element2.oclIsUndefined() then true else
		--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else
		if element2EntryType.oclIsUndefined() then false 
		else element2EntryType.allProperties()->includes(self.element2)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_CORRECT;

		if (element2CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element2CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element2CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getElement3EntryType() {
		MClassifier element3EntryType = basicGetElement3EntryType();
		return element3EntryType != null && element3EntryType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) element3EntryType)
				: element3EntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetElement3EntryType() {
		/**
		 * @OCL if not self.element2Correct then null
		else 
		if self.element2.oclIsUndefined() then null
		else self.element2.type endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE;

		if (element3EntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element3EntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element3EntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getElement3() {
		if (element3 != null && element3.eIsProxy()) {
			InternalEObject oldElement3 = (InternalEObject) element3;
			element3 = (MProperty) eResolveProxy(oldElement3);
			if (element3 != oldElement3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3,
							oldElement3, element3));
			}
		}
		return element3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetElement3() {
		return element3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement3(MProperty newElement3) {
		MProperty oldElement3 = element3;
		element3 = newElement3;
		boolean oldElement3ESet = element3ESet;
		element3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3, oldElement3,
					element3, !oldElement3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElement3() {
		MProperty oldElement3 = element3;
		boolean oldElement3ESet = element3ESet;
		element3 = null;
		element3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3, oldElement3,
					null, oldElement3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElement3() {
		return element3ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Element3</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let annotatedProp: mcore::MProperty = 
	self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
	in
	
	if element2.oclIsUndefined() 
	then OrderedSet{}
	else if element2.isOperation
	then OrderedSet{} 
	else if element3EntryType.oclIsUndefined() 
	  then OrderedSet{}
	  else element3EntryType.allProperties() endif
	 endif
	endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalElement3ChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		if (element3ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				element3ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"Element3ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(element3ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "Element3ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement3Correct() {
		/**
		 * @OCL if element3.oclIsUndefined() then true else
		--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else
		if element3EntryType.oclIsUndefined() then false
		else element3EntryType.allProperties()->includes(self.element3)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_CORRECT;

		if (element3CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element3CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element3CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCastType() {
		if (castType != null && castType.eIsProxy()) {
			InternalEObject oldCastType = (InternalEObject) castType;
			castType = (MClassifier) eResolveProxy(oldCastType);
			if (castType != oldCastType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE,
							oldCastType, castType));
			}
		}
		return castType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCastType() {
		return castType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCastType(MClassifier newCastType) {
		MClassifier oldCastType = castType;
		castType = newCastType;
		boolean oldCastTypeESet = castTypeESet;
		castTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE, oldCastType,
					castType, !oldCastTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCastType() {
		MClassifier oldCastType = castType;
		boolean oldCastTypeESet = castTypeESet;
		castType = null;
		castTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE, oldCastType,
					null, oldCastTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCastType() {
		return castTypeESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Cast Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.kind = mcore::ClassifierKind::ClassType 
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalCastTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		if (castTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CAST_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				castTypeChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"CastTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(castTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "CastTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getLastElement() {
		MProperty lastElement = basicGetLastElement();
		return lastElement != null && lastElement.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) lastElement)
				: lastElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetLastElement() {
		/**
		 * @OCL if not self.element3.oclIsUndefined() then self.element3 else
		if not self.element2.oclIsUndefined() then self.element2 else
		if not self.element1.oclIsUndefined() then self.element1 else
		null endif endif endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__LAST_ELEMENT;

		if (lastElementDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				lastElementDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(lastElementDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getChainCalculatedType() {
		MClassifier chainCalculatedType = basicGetChainCalculatedType();
		return chainCalculatedType != null && chainCalculatedType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) chainCalculatedType)
				: chainCalculatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetChainCalculatedType() {
		/**
		 * @OCL let nl: mcore::MClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE;

		if (chainCalculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainCalculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getChainCalculatedSimpleType() {
		/**
		 * @OCL let nl: mcore::SimpleType = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE;

		if (chainCalculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainCalculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getChainCalculatedSingular() {
		/**
		 * @OCL let nl: Boolean = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;

		if (chainCalculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainCalculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessor getProcessor() {
		return processor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessor(MProcessor newProcessor) {
		MProcessor oldProcessor = processor;
		processor = newProcessor == null ? PROCESSOR_EDEFAULT : newProcessor;
		boolean oldProcessorESet = processorESet;
		processorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR,
					oldProcessor, processor, !oldProcessorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProcessor() {
		MProcessor oldProcessor = processor;
		boolean oldProcessorESet = processorESet;
		processor = PROCESSOR_EDEFAULT;
		processorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR,
					oldProcessor, PROCESSOR_EDEFAULT, oldProcessorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProcessor() {
		return processorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition getProcessorDefinition() {
		MProcessorDefinition processorDefinition = basicGetProcessorDefinition();
		return processorDefinition != null && processorDefinition.eIsProxy()
				? (MProcessorDefinition) eResolveProxy(
						(InternalEObject) processorDefinition)
				: processorDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition basicGetProcessorDefinition() {
		/**
		 * @OCL if (let e0: Boolean = processor = mcore::expressions::MProcessor::None in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then null
		else createProcessorDefinition()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;

		if (processorDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				processorDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(processorDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProcessorDefinition result = (MProcessorDefinition) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setProcessorDefinition(
			MProcessorDefinition newProcessorDefinition) {
		/* NOTE: Repeated from MAbstractChainImpl
		/* NOTE: After you changed this you may also have to change basicGetProcessorDefinition() as in basicGetBaseDefinition() where you do it in order to retrieve the selected base variable. */
		if (newProcessorDefinition == null) {
			setProcessor(MProcessor.NONE);
		} else {
			setProcessor(newProcessorDefinition.getProcessor());
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Processor Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProcessorDefinition>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 
	let s:SimpleType = self.chainCalculatedSimpleType in
	let t:MClassifier = self.chainCalculatedType in
	let res:OrderedSet(mcore::expressions::MProcessorDefinition) =
	if s = mcore::SimpleType::Boolean
	then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForBoolean()
	             else self.procDefChoicesForBooleans() endif
	else if s = mcore::SimpleType::Integer 
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForInteger()
	             else self.procDefChoicesForIntegers() endif
	else if   s = mcore::SimpleType::Double
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForReal()
	             else self.procDefChoicesForReals() endif
	else if   s = mcore::SimpleType::String
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForString()
	             else self.procDefChoicesForStrings() endif
	else if   s = mcore::SimpleType::Date
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForDate()
	             else self.procDefChoicesForDates() endif
	else if s = mcore::SimpleType::None or 
	      s = mcore::SimpleType::Annotation or 
	      s = mcore::SimpleType::Attribute or 
	      s = mcore::SimpleType::Class or 
	      s = mcore::SimpleType::Classifier or 
	      s = mcore::SimpleType::DataType or 
	      s = mcore::SimpleType::Enumeration or 
	      s = mcore::SimpleType::Feature or 
	      s = mcore::SimpleType::KeyValue or 
	      s = mcore::SimpleType::Literal or 
	      s = mcore::SimpleType::NamedElement or 
	      s = mcore::SimpleType::Object or 
	      s = mcore::SimpleType::Operation or 
	      s = mcore::SimpleType::Package or 
	      s = mcore::SimpleType::Parameter or 
	      s = mcore::SimpleType::Reference or 
	      s = mcore::SimpleType::TypedElement 
	 then if self.chainCalculatedSingular 
	             then self.procDefChoicesForObject()
	             else self.procDefChoicesForObjects()
	                    --OrderedSet{Tuple{processor=MProcessor::Head},
	                    --                   Tuple{processor=MProcessor::Tail}} 
	                    endif
	 else OrderedSet{} endif endif endif endif endif endif
	in res->prepend(null)
	                                       
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProcessorDefinition> evalProcessorDefinitionChoiceConstruction(
			List<MProcessorDefinition> choice) {
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		if (processorDefinitionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				processorDefinitionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"ProcessorDefinitionChoiceConstruction");
			}
		}
		Query query = OCL_ENV
				.createQuery(processorDefinitionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "ProcessorDefinitionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProcessorDefinition> result = new ArrayList<MProcessorDefinition>(
					(Collection<MProcessorDefinition>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getTypeMismatch() {
		/**
		 * @OCL self.ownToApplyMismatch()
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MBASE_CHAIN__TYPE_MISMATCH;

		if (typeMismatchDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				typeMismatchDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(typeMismatchDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MCallArgument> getCallArgument() {
		if (callArgument == null) {
			callArgument = new EObjectContainmentWithInverseEList.Unsettable.Resolving<MCallArgument>(
					MCallArgument.class, this,
					AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT,
					ExpressionsPackage.MCALL_ARGUMENT__CALL);
		}
		return callArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCallArgument() {
		if (callArgument != null)
			((InternalEList.Unsettable<?>) callArgument).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCallArgument() {
		return callArgument != null
				&& ((InternalEList.Unsettable<?>) callArgument).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSubChain> getSubExpression() {
		if (subExpression == null) {
			subExpression = new EObjectContainmentEList.Unsettable.Resolving<MSubChain>(
					MSubChain.class, this,
					AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION);
		}
		return subExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSubExpression() {
		if (subExpression != null)
			((InternalEList.Unsettable<?>) subExpression).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSubExpression() {
		return subExpression != null
				&& ((InternalEList.Unsettable<?>) subExpression).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getContainedCollector() {
		if (containedCollector != null && containedCollector.eIsProxy()) {
			InternalEObject oldContainedCollector = (InternalEObject) containedCollector;
			containedCollector = (MCollectionExpression) eResolveProxy(
					oldContainedCollector);
			if (containedCollector != oldContainedCollector) {
				InternalEObject newContainedCollector = (InternalEObject) containedCollector;
				NotificationChain msgs = oldContainedCollector.eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
						null, null);
				if (newContainedCollector.eInternalContainer() == null) {
					msgs = newContainedCollector.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
							oldContainedCollector, containedCollector));
			}
		}
		return containedCollector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetContainedCollector() {
		return containedCollector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainedCollector(
			MCollectionExpression newContainedCollector,
			NotificationChain msgs) {
		MCollectionExpression oldContainedCollector = containedCollector;
		containedCollector = newContainedCollector;
		boolean oldContainedCollectorESet = containedCollectorESet;
		containedCollectorESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
					oldContainedCollector, newContainedCollector,
					!oldContainedCollectorESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedCollector(
			MCollectionExpression newContainedCollector) {
		if (newContainedCollector != containedCollector) {
			NotificationChain msgs = null;
			if (containedCollector != null)
				msgs = ((InternalEObject) containedCollector).eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
						null, msgs);
			if (newContainedCollector != null)
				msgs = ((InternalEObject) newContainedCollector).eInverseAdd(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
						null, msgs);
			msgs = basicSetContainedCollector(newContainedCollector, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContainedCollectorESet = containedCollectorESet;
			containedCollectorESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
						newContainedCollector, newContainedCollector,
						!oldContainedCollectorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetContainedCollector(
			NotificationChain msgs) {
		MCollectionExpression oldContainedCollector = containedCollector;
		containedCollector = null;
		boolean oldContainedCollectorESet = containedCollectorESet;
		containedCollectorESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
					oldContainedCollector, null, oldContainedCollectorESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContainedCollector() {
		if (containedCollector != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) containedCollector).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
					null, msgs);
			msgs = basicUnsetContainedCollector(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContainedCollectorESet = containedCollectorESet;
			containedCollectorESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR,
						null, null, oldContainedCollectorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContainedCollector() {
		return containedCollectorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChainCodeforSubchains() {
		/**
		 * @OCL let code: String = 
		if baseDefinition.oclIsKindOf(mcore::expressions::MBaseDefinition) 
		then asCodeForBuiltIn()
		else if baseDefinition.oclIsKindOf(mcore::expressions::MContainerBaseDefinition) 
		then asCodeForOthers()
		else if (baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)) 
		then asCodeForConstants()
		else if baseDefinition.oclIsKindOf(mcore::expressions::MVariableBaseDefinition) 
		then asCodeForVariables()
		else asCodeForOthers() endif endif endif endif in
		let res: String = if calculatedSingular then 'null' else 'OrderedSet{}' endif in
		let chainTypeString: String = 
		typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, chainCalculatedSingular) in
		let chainTypeStringSingular: String = 
		typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, true) in
		--if (castType.oclIsUndefined() or chainCalculatedType.oclIsUndefined())
		--then 
		if self.typeMismatch or not(self.castType.oclIsUndefined())-- and self.containedCollector.oclIsUndefined() and
		--(if self.castType.oclIsUndefined() then true else castType.allSubTypes()->excludes(chainCalculatedType)endif)
		then self.autoCastWithProc()
		else if self.processor=mcore::expressions::MProcessor::None 
		then code 
		else if self.isPrefixProcessor() or self.isPostfixProcessor() 
		then if self.processor = mcore::expressions::MProcessor::Not 
		      then let businessLog : String = ' '  in
		              businessLog.concat('if (').concat(code).concat(')= true \n then false \n else if (').concat(code).concat(')= false \n then true \n else null endif endif \n ')
		 else if self.processor = mcore::expressions::MProcessor::OneDividedBy 
		       then let businessLog : String = ' '  in
		               businessLog.concat('(1 / (').concat(code).concat('))')
		 else if self.isPostfixProcessor() 
		        then let businessLog : String = ' '  in
		                businessLog.concat(code).concat(self.procAsCode())
		        else code endif endif endif
		else let variableName : String = 
		 self.uniqueChainNumber() in
		'let '.concat(variableName).concat(': ').concat(chainTypeString).concat(' = ').concat(
		code).concat(' in\n').concat(    -- Accessing feature is not unary:
		'if ').concat(variableName).concat(
		if self.isOwnXOCLOperator() 
		     then '.oclIsUndefined() or '.concat(variableName) else ''endif).concat(
		if self.isProcessorSetOperator() 
		     then '->'
		else if not(self.isProcessorCheckEqualOperator()) 
		     then '.' else '' endif endif).concat(
		if self.isCustomCodeProcessor() 
		     then 'isEmpty()' 
		     else self.procAsCode() endif).concat
		(
		if self.isProcessorCheckEqualOperator() 
		     then ' then true else false ' 
		else           
		        if self.isCustomCodeProcessor()  
		             then '' 
		             else if not(self.processorReturnsSingular())  then '->' else '.' endif.concat(
		                    'oclIsUndefined() \n ') endif.concat( 
		        'then null \n else ').concat(-- todo   processor does not influence calculatedSingular
		        variableName).concat(
		        if self.isProcessorSetOperator() then '->' else '.' endif).concat
		        ( 
		        --  "->" for Set , "." for unary ,  nothing if we check for a value
		        if self.isCustomCodeProcessor() 
		           then let checkPart : String = if  processor= mcore::expressions::MProcessor::And then 'false' else 'true' endif in
		                   let elsePart : String = if processor= mcore::expressions::MProcessor::And then 'true' else 'false' endif in
		                   let iterateBase: String = 'iterate( x:'.concat(chainTypeStringSingular).concat('; s:') in
		                   if self.processor = mcore::expressions::MProcessor::And or processor = mcore::expressions::MProcessor::Or 
		                        then iterateBase.concat(chainTypeStringSingular).concat(
		                                '= ').concat(elsePart).concat(
		                               '|  if ( x) = ').concat(checkPart).concat( ' \n then ').concat(checkPart).concat('\n').concat(
		                                'else if (s)=').concat(checkPart).concat('\n').concat('then ').concat(checkPart).concat('\n').concat(
		                                ' else if x =null then null \n').concat('else if s =null then null').concat(
		                                ' else ').concat(elsePart).concat(' endif endif endif endif)')
		                       else iterateBase.concat(chainTypeString).concat('= ').concat('OrderedSet{} | if x.oclIsUndefined() then s else if x=').concat(
		                               --CHANGE ->first and ->last 
		                               --.concat(code).concat(if processor=MProcessor::Head then '->first() ' else '->last() ' endif).concat('then s else s->including(x)->asOrderedSet() endif endif)'))
		                               code).concat(if processor=mcore::expressions::MProcessor::Head then '->last() ' else '->first() ' endif).concat(
		                               'then s else s->including(x)->asOrderedSet() endif endif)') endif
		          else self.procAsCode() endif
		          ) endif
		  ).concat( 
		 '\n  endif') endif endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS;

		if (chainCodeforSubchainsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainCodeforSubchainsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCodeforSubchainsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsOwnXOCLOp() {
		/**
		 * @OCL processor = expressions::MProcessor::CamelCaseLower or
		processor = expressions::MProcessor::CamelCaseToBusiness or
		processor = mcore::expressions::MProcessor::CamelCaseUpper
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MBASE_CHAIN__IS_OWN_XOCL_OP;

		if (isOwnXOCLOpDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isOwnXOCLOpDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isOwnXOCLOpDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNamedConstant> getNamedConstant() {
		if (namedConstant == null) {
			namedConstant = new EObjectContainmentEList.Unsettable.Resolving<MNamedConstant>(
					MNamedConstant.class, this,
					AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT);
		}
		return namedConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedConstant() {
		if (namedConstant != null)
			((InternalEList.Unsettable<?>) namedConstant).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedConstant() {
		return namedConstant != null
				&& ((InternalEList.Unsettable<?>) namedConstant).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNamedExpression> getNamedExpression() {
		if (namedExpression == null) {
			namedExpression = new EObjectContainmentEList.Unsettable.Resolving<MNamedExpression>(
					MNamedExpression.class, this,
					AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION);
		}
		return namedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedExpression() {
		if (namedExpression != null)
			((InternalEList.Unsettable<?>) namedExpression).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedExpression() {
		return namedExpression != null
				&& ((InternalEList.Unsettable<?>) namedExpression).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractNamedTuple> getNamedTuple() {
		if (namedTuple == null) {
			namedTuple = new EObjectContainmentEList.Unsettable.Resolving<MAbstractNamedTuple>(
					MAbstractNamedTuple.class, this,
					AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE);
		}
		return namedTuple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedTuple() {
		if (namedTuple != null)
			((InternalEList.Unsettable<?>) namedTuple).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedTuple() {
		return namedTuple != null
				&& ((InternalEList.Unsettable<?>) namedTuple).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getExpectedReturnTypeOfAnnotation() {
		MClassifier expectedReturnTypeOfAnnotation = basicGetExpectedReturnTypeOfAnnotation();
		return expectedReturnTypeOfAnnotation != null
				&& expectedReturnTypeOfAnnotation.eIsProxy()
						? (MClassifier) eResolveProxy(
								(InternalEObject) expectedReturnTypeOfAnnotation)
						: expectedReturnTypeOfAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetExpectedReturnTypeOfAnnotation() {
		/**
		 * @OCL let apa:annotations::MAbstractPropertyAnnotations = eContainer().oclAsType(MAbstractPropertyAnnotations)
		in if apa.annotatedProperty.oclIsUndefined()
		then null 
		else apa.annotatedProperty.calculatedType
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE_OF_ANNOTATION;

		if (expectedReturnTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				expectedReturnTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(expectedReturnTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getExpectedReturnSimpleTypeOfAnnotation() {
		/**
		 * @OCL if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else
		
		if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.simpleType
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.simpleType
		else SimpleType::None 
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

		if (expectedReturnSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				expectedReturnSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(expectedReturnSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueOfAnnotationMandatory() {
		/**
		 * @OCL if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else
		
		
		if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.mandatory
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.mandatory
		else true
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY;

		if (isReturnValueOfAnnotationMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReturnValueOfAnnotationMandatoryDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(isReturnValueOfAnnotationMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueOfAnnotationSingular() {
		/**
		 * @OCL if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else
		
		
		
		if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.singular
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.singular
		else true
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR;

		if (isReturnValueOfAnnotationSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReturnValueOfAnnotationSingularDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(isReturnValueOfAnnotationSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getOclChanged() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__OCL_CHANGED;

		if (oclChangedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				oclChangedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(oclChangedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setOclChanged(Boolean newOclChanged) {
		// Refreshes corresponding OCL, based on changes of expressions in
		// tabular MRules syntax
		String ocl = this.getOclCode();
		if (ocl != null) {
			setValue(ocl);
		} else {
			setValue("");
		}

		// NEW as well set the "Use Explicit OCL" to true:
		setUseExplicitOcl(true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setValue(String newValue) {
		super.setValue(newValue);
		if (null == newValue) {
			this.unsetUseExplicitOcl();
		} else if (newValue.equals("")) {
			this.unsetUseExplicitOcl();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getOclCode() {
		/**
		 * @OCL let attentionImplementedinJava: String = 'TODO Replace Java Code' in
		attentionImplementedinJava
		
		 * @templateTag GGFT01
		 */
		StringBuffer ocl = new StringBuffer();
		boolean flag = false;
		for (MNamedConstant e : getNamedConstant())
			ocl.append(e.getAsCode()).append("\n");
		for (MAbstractNamedTuple e : getNamedTuple()) {

			if (getNamedTuple().size() > 1 && flag == false) {
				ocl.append("OrderedSet{");
				flag = true;
			}
			ocl.append(e.getAsCode()).append("\n");
			if (getNamedTuple().size() > 1) {
				if (this.getNamedTuple().indexOf(e) + 1 == this.getNamedTuple()
						.size())
					ocl.append("}");
				else

					ocl.append(",");
			}

		}
		//20150617 check for new toplevel chain.
		if (this.getBase() == null || this.getBase() == ExpressionBase.UNDEFINED
		//it seems in some cases, the base gets initialized as self, even if we did no do it, thus adding as next options, that the last named expression or namedConstant is unnamed.
				|| (!this.getNamedExpression().isEmpty()
						&& (this.getNamedExpression()
								.get(this.getNamedExpression().size() - 1)
								.getEName() == null
								|| this.getNamedExpression()
										.get(this.getNamedExpression().size()
												- 1)
										.getEName().isEmpty()))
				|| (this.getNamedExpression().isEmpty()
						&& !this.getNamedConstant().isEmpty()
						&& (this.getNamedConstant()
								.get(this.getNamedConstant().size() - 1)
								.getEName() == null
								|| this.getNamedConstant()
										.get(getNamedConstant().size() - 1)
										.getEName().isEmpty()))) {
			//20150617 this was the code used up to now.
			for (MNamedExpression e : getNamedExpression())
				ocl.append(e.getAsCode()).append("\n");
			return ocl.toString();
		} else {
			//20150617 the following needs to be done:

			//20150617    1- do the above for-loop, but start from the end, and go to the beginning.
			//20150617    2- append the asCode of this object

			//20150617    1- do the above for-loop, but start from the end, and go to the beginning.

			for (ListIterator<MNamedExpression> it = getNamedExpression()
					.listIterator(getNamedExpression().size()); it
							.hasPrevious();) {
				MNamedExpression e = it.previous();
				ocl.append(e.getAsCode()).append("\n");
			}

			//20150617    2- append the asCode of this object

			ocl.append(this.getAsCode()).append("\n");

			return ocl.toString();

		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotationAction getDoAction() {
		/**
		 * @OCL mcore::annotations::MExprAnnotationAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MEXPR_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MExprAnnotationAction result = (MExprAnnotationAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MExprAnnotationAction newDoAction) {
		MNamedExpression ne = null;
		MNamedConstant nc = null;
		MChain c1 = null;
		MChain c2 = null;
		MChain c3 = null;
		switch (newDoAction.getValue()) {
		case MExprAnnotationAction.INTO_DATA_CONSTANT_VALUE:
			nc = ExpressionsFactory.eINSTANCE.createMNamedConstant();
			this.getNamedConstant().add(nc);
			nc.setName("Const "
					.concat(String.valueOf(this.getNamedConstant().size())));
			MSimpleTypeConstantLet stcl = ExpressionsFactory.eINSTANCE
					.createMSimpleTypeConstantLet();
			nc.setExpression(stcl);
			if (this.getCalculatedSimpleType() == SimpleType.STRING) {
				stcl.setSimpleType(this.getCalculatedSimpleType());
				stcl.setConstant1("ENTER VALUE");
			} else if (this.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
				stcl.setSimpleType(this.getCalculatedSimpleType());
				stcl.setConstant1("true");
			} else if (this.getCalculatedSimpleType() == SimpleType.INTEGER) {
				stcl.setSimpleType(this.getCalculatedSimpleType());
				stcl.setConstant1("7");
			} else if (this.getCalculatedSimpleType() == SimpleType.DOUBLE) {
				stcl.setSimpleType(this.getCalculatedSimpleType());
				stcl.setConstant1("3.5");
			} else if (this.getCalculatedSimpleType() == SimpleType.DATE) {
				stcl.setSimpleType(this.getCalculatedSimpleType());
				stcl.setConstant1("1983-02-14T06:55:19.000+0200");
			} else {
				stcl.setSimpleType(SimpleType.STRING);
				stcl.setConstant1("ENTER VALUE");
			}
			if (this.getBase() == ExpressionBase.SELF_OBJECT
					&& (this.getElement1() != null
							|| this.getProcessor() != null)) {
				ne = ExpressionsFactory.eINSTANCE.createMNamedExpression();
				this.getNamedExpression().add(ne);
				ne.setName("Previous Main Chain ".concat(
						String.valueOf(this.getNamedExpression().size())));
				c1 = ExpressionsFactory.eINSTANCE.createMChain();
				ne.setExpression(c1);
				c1.reuseFromOtherNoMoreUsedChain(this);
			}
			this.resetToBase(ExpressionBase.CONSTANT_VALUE, nc);
			break;
		case MExprAnnotationAction.INTO_LITERAL_CONSTANT_VALUE:
			nc = ExpressionsFactory.eINSTANCE.createMNamedConstant();
			this.getNamedConstant().add(nc);
			nc.setName("Const "
					.concat(String.valueOf(this.getNamedConstant().size())));
			MLiteralLet ll = ExpressionsFactory.eINSTANCE.createMLiteralLet();
			nc.setExpression(ll);
			if (this.getCalculatedType() != null && this.getCalculatedType()
					.getKind() == ClassifierKind.ENUMERATION) {
				ll.setEnumerationType(getCalculatedType());
			} else {
				MClassifier e = null;
				for (MClassifier mclassifier : this
						.getContainingAbstractPropertyAnnotations()
						.getContainingClassifier().getContainingPackage()
						.getClassifier()) {
					if (mclassifier.getKind() == ClassifierKind.ENUMERATION) {
						e = mclassifier;
					}
				}
				ll.setEnumerationType(e);
			}
			if (ll.getEnumerationType() != null) {
				if (!ll.getEnumerationType().getLiteral().isEmpty()) {
					ll.setConstant1(
							ll.getEnumerationType().getLiteral().get(0));
				}
			}

			if (this.getBase() == ExpressionBase.SELF_OBJECT
					&& (this.getElement1() != null
							|| this.getProcessor() != null)) {
				ne = ExpressionsFactory.eINSTANCE.createMNamedExpression();
				this.getNamedExpression().add(ne);
				ne.setName("Previous Main Chain ".concat(
						String.valueOf(this.getNamedExpression().size())));
				c1 = ExpressionsFactory.eINSTANCE.createMChain();
				ne.setExpression(c1);
				c1.reuseFromOtherNoMoreUsedChain(this);
			}
			this.resetToBase(ExpressionBase.CONSTANT_LITERAL_VALUE, nc);
			break;
		case MExprAnnotationAction.INTO_APPLY_VALUE:
			ne = ExpressionsFactory.eINSTANCE.createMNamedExpression();
			this.getNamedExpression().add(0, ne);
			ne.setName("Var "
					.concat(String.valueOf(this.getNamedExpression().size())));
			MApplication a = ExpressionsFactory.eINSTANCE.createMApplication();
			ne.setExpression(a);
			MOperator o = null;
			if (this.getCalculatedSimpleType() == SimpleType.STRING) {
				o = MOperator.CONCAT;
			} else if (this.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
				o = MOperator.AND_OP;
			} else if (this.getCalculatedSimpleType() == SimpleType.INTEGER) {
				o = MOperator.PLUS;
			} else if (this.getCalculatedSimpleType() == SimpleType.DOUBLE) {
				o = MOperator.TIMES;
			} else if (this.getCalculatedSimpleType() == SimpleType.DATE) {
				o = MOperator.DIFF_IN_HRS;
			} else {
				o = MOperator.INTERSECTION;
			}
			a.setOperator(o);
			c1 = ExpressionsFactory.eINSTANCE.createMChain();
			a.getOperands().add(c1);
			c1.reuseFromOtherNoMoreUsedChain(this);
			this.resetToBase(ExpressionBase.VARIABLE, ne);
			c2 = ExpressionsFactory.eINSTANCE.createMChain();
			a.getOperands().add(c2);
			if (this.getCalculatedSimpleType() == SimpleType.STRING) {
				c2.setBase(ExpressionBase.EMPTY_STRING_VALUE);
			} else if (this.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
				c2.setBase(ExpressionBase.FALSE_VALUE);
			} else if (this.getCalculatedSimpleType() == SimpleType.INTEGER) {
				c2.setBase(ExpressionBase.ONE_VALUE);
			} else if (this.getCalculatedSimpleType() == SimpleType.DOUBLE) {
				c2.setBase(ExpressionBase.ONE_VALUE);
			} else if (this.getCalculatedSimpleType() == SimpleType.DATE) {
				c2.setBase(ExpressionBase.NULL_VALUE);
			} else {
				if (this.getCalculatedSingular() == true) {
					c2.setBase(ExpressionBase.NULL_VALUE);
				} else {
					c2.setBase(ExpressionBase.EMPTY_COLLECTION);
				}
				o = MOperator.INTERSECTION;
			}
			break;
		//TODO  Change code to work with new semantics model
		/*
		case MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE_VALUE:
		case MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE:
		case MExprAnnotationAction.INTO_ELSE_OF_IF_THEN_ELSE_VALUE: {
		
		if (this.getNamedExpression().isEmpty()) {
		
		} else {
			XTransition t = SemanticsFactory.eINSTANCE.createXTransition();
		
			//Get lastExpr, which will be our baseVar
			MNamedExpression lastExpr = this.getNamedExpression().get(
					getNamedExpression().size() - 1);
		
			if (!lastExpr.isSetName() || lastExpr.getName().equals("")) {
				//Get the namedExpr and change its name to 'var1' so we can access it as BaseVar
				XUpdatedObject updateNameExpr = t
						.nextStateObjectDefinitionFromObject(lastExpr);
				t.addStringAttributeUpdate(updateNameExpr,
						McorePackage.eINSTANCE.getMNamed_Name(),
						XUpdateMode.REDEFINE, XAddUpdateMode.FIRST,
						"newVariable");
		
			}
		
			//new instance MNamedExpression
			XUpdatedObject newCla = t
					.createNextStateNewObject(ExpressionsPackage.eINSTANCE
							.getMNamedExpression());
			//MResult to be updated
			XUpdatedObject rootCo = t
					.nextStateObjectDefinitionFromObject(this);
			// Adding new MNamedExpr at first position
			t.addReferenceUpdate(rootCo, AnnotationsPackage.eINSTANCE
					.getMExprAnnotation_NamedExpression(), XUpdateMode.ADD,
					XAddUpdateMode.FIRST, newCla, null, null);
		
			//Create MIf and put(redefine) into MNamedExpr
			XUpdatedObject newIf = t
					.createNextStateNewObject(ExpressionsPackage.eINSTANCE
							.getMIf());
			t.addReferenceUpdate(newCla, ExpressionsPackage.eINSTANCE
					.getMNamedExpression_Expression(),
					XUpdateMode.REDEFINE, XAddUpdateMode.LAST, newIf, null,
					null);
		
			//Execute, so we can use the baseVar
			t.setExecuteNow(true);
		
			t = SemanticsFactory.eINSTANCE.createXTransition();
		
			// Get the chain, which we want to set 
			XUpdatedObject updatingChainBecauseOfAction = null;
			if (newDoAction.getValue() == MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE_VALUE)
				updatingChainBecauseOfAction = t
						.nextStateObjectDefinitionFromObject(((MIf) this
								.getNamedExpression().get(0)
								.getExpression()).getCondition());
			else if (newDoAction.getValue() == MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE)
				updatingChainBecauseOfAction = t
						.nextStateObjectDefinitionFromObject(((MIf) this
								.getNamedExpression().get(0)
								.getExpression()).getThenPart()
								.getExpression());
			else
				updatingChainBecauseOfAction = t
						.nextStateObjectDefinitionFromObject(((MIf) this
								.getNamedExpression().get(0)
								.getExpression()).getElsePart()
								.getExpression());
		
		
		
			// Add the lastExpr as baseVar in the chain
			XUpdatedObject varUpdate = t
					.nextStateObjectDefinitionFromObject(lastExpr);
			// To be done
			t.addLiteralAttributeUpdate(
					updatingChainBecauseOfAction,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE,
					XAddUpdateMode.LAST,
					ExpressionsPackage.eINSTANCE.getExpressionBase()
							.getEEnumLiteral(
									this.variableFromExpression(lastExpr)
											.getCalculatedBase().getName()));
			// Sets baseVar
			t.addReferenceUpdate(updatingChainBecauseOfAction,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_BaseVar(),
					XUpdateMode.REDEFINE, XAddUpdateMode.LAST, varUpdate,
					null, null);
		
			t.setExecuteNow(true);
		}
		break;
		}
		 */
		case MExprAnnotationAction.DATA_CONSTANT_AS_ARGUMENT_VALUE:
			break;
		case MExprAnnotationAction.LITERAL_CONSTANT_AS_ARGUMENT_VALUE:
			break;
		case MExprAnnotationAction.WHERE_AS_BASE_VALUE:
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate oclChanged$Update(Boolean trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		java.lang.Boolean triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				AnnotationsPackage.eINSTANCE.getMExprAnnotation_OclChanged(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MExprAnnotationAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.annotations.MExprAnnotationAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				AnnotationsPackage.eINSTANCE.getMExprAnnotation_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MExprAnnotationAction trg) {

		switch (trg.getValue()) {

		case MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE_VALUE:
		case MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE:
		case MExprAnnotationAction.INTO_ELSE_OF_IF_THEN_ELSE_VALUE: {
			if (this.getNamedExpression().isEmpty()) {

				//1. Create transition and set trigger 
				XTransition transition = SemanticsFactory.eINSTANCE
						.createXTransition();

				XUpdate currentTrigger = transition
						.addAttributeUpdate(this,
								McorePackage.Literals.MPROPERTY__DO_ACTION,
								XUpdateMode.REDEFINE, null,
								//:=
								AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
										.getEEnumLiteral(trg.getValue()),
								null, null);

				// 2. create NamedExpression
				EObject namedExpr = transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMNamedExpression());

				XUpdate addedNamedExpr = currentTrigger.addReferenceUpdate(this,
						AnnotationsPackage.eINSTANCE
								.getMExprAnnotation_NamedExpression(),
						XUpdateMode.ADD, XAddUpdateMode.FIRST, namedExpr, null,
						null);

				//3. Update name of namedExpression
				currentTrigger.addAttributeUpdate(namedExpr,
						McorePackage.eINSTANCE.getMNamed_Name(),
						XUpdateMode.REDEFINE, null,
						"Result of " + ((MProperty) this.getAnnotatedElement())
								.getName(),
						null, null);

				// 4. Create MIF 
				EObject newIf = transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMIf());
				currentTrigger.addReferenceUpdate(namedExpr,
						ExpressionsPackage.eINSTANCE
								.getMNamedExpression_Expression(),
						XUpdateMode.REDEFINE, null, newIf, null, null);

				// 5. Create and set conditionPart

				MChain condPart = null;

				condPart = (MChain) transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMChain());

				currentTrigger.addReferenceUpdate(newIf,
						ExpressionsPackage.eINSTANCE.getMAbstractIf_Condition(),
						XUpdateMode.REDEFINE, null, condPart, null, null);

				if (trg.getValue() != MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE_VALUE) {

					currentTrigger.addAttributeUpdate(condPart,
							ExpressionsPackage.eINSTANCE
									.getMAbstractExpressionWithBase_Base(),
							XUpdateMode.REDEFINE, null,
							ExpressionsPackage.Literals.EXPRESSION_BASE
									.getEEnumLiteral(
											ExpressionBase.TRUE_VALUE_VALUE),
							null, null);
				} else {
					// Merge chain and MResult
					currentTrigger.addAndMergeUpdate(condPart
							.reuseFromOtherNoMoreUsedChainAsUpdate(this));

					currentTrigger.getContainingTransition().getFocusObjects()
							.add(currentTrigger
									.nextStateObjectDefinitionFromObject(
											condPart));

				}

				//6. Create and set thenPart
				EObject thenPart = transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMThen());
				currentTrigger.addReferenceUpdate(newIf,
						ExpressionsPackage.eINSTANCE.getMAbstractIf_ThenPart(),
						XUpdateMode.REDEFINE, null, thenPart, null, null);
				MChain thenChain = (MChain) transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMChain());

				currentTrigger.addReferenceUpdate(thenPart,
						ExpressionsPackage.eINSTANCE.getMThen_Expression(),
						XUpdateMode.REDEFINE, null, thenChain, null, null);
				if (trg.getValue() != MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE) {

					//NOP
				} else {
					// Merge chain and MResult
					currentTrigger.addAndMergeUpdate(thenChain
							.reuseFromOtherNoMoreUsedChainAsUpdate(this));

					currentTrigger.getContainingTransition().getFocusObjects()
							.add(currentTrigger
									.nextStateObjectDefinitionFromObject(
											thenChain));

				}

				//7. Create and set elsePart
				EObject elsePart = transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMElse());
				currentTrigger.addReferenceUpdate(newIf,
						ExpressionsPackage.eINSTANCE.getMAbstractIf_ElsePart(),
						XUpdateMode.REDEFINE, null, elsePart, null, null);
				MChain elseChain = (MChain) transition.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMChain());
				currentTrigger.addReferenceUpdate(elsePart,
						ExpressionsPackage.eINSTANCE.getMElse_Expression(),
						XUpdateMode.REDEFINE, null, elseChain, null, null);

				if (trg.getValue() != MExprAnnotationAction.INTO_ELSE_OF_IF_THEN_ELSE_VALUE) {

					//NOP
				} else {
					currentTrigger.addAndMergeUpdate(elseChain
							.reuseFromOtherNoMoreUsedChainAsUpdate(this));

					currentTrigger.getContainingTransition().getFocusObjects()
							.add(currentTrigger
									.nextStateObjectDefinitionFromObject(
											elseChain));

					// Merge chain and MResult
				}

				//

				currentTrigger.addAttributeUpdate(this,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_Base(),
						XUpdateMode.REDEFINE, null,
						ExpressionsPackage.Literals.EXPRESSION_BASE
								.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
						null, null);

				currentTrigger.addReferenceUpdate(this,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_BaseVar(),
						XUpdateMode.REDEFINE, null, namedExpr, null, null);

				return currentTrigger;
			} else {
				XTransition t = SemanticsFactory.eINSTANCE.createXTransition();

				//Get lastExpr, which will be our baseVar
				MNamedExpression lastExpr = this.getNamedExpression()
						.get(getNamedExpression().size() - 1);

				if (!lastExpr.isSetName() || lastExpr.getName().equals("")) {
					//Get the namedExpr and change its name to 'var1' so we can access it as BaseVar
					t.addAttributeUpdate(lastExpr,
							McorePackage.eINSTANCE.getMNamed_Name(),
							XUpdateMode.REDEFINE, XAddUpdateMode.FIRST,
							"newVariable", null, null);

				}

				//new instance MNamedExpression
				EObject newCla = t.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMNamedExpression());
				//MResult to be updated

				// Adding new MNamedExpr at first position
				t.addReferenceUpdate(this,
						AnnotationsPackage.eINSTANCE
								.getMExprAnnotation_NamedExpression(),
						XUpdateMode.ADD, XAddUpdateMode.FIRST, newCla, null,
						null);

				//Create MIf and put(redefine) into MNamedExpr
				EObject newIf = t.createNextStateNewObject(
						ExpressionsPackage.eINSTANCE.getMIf());
				t.addReferenceUpdate(newCla,
						ExpressionsPackage.eINSTANCE
								.getMNamedExpression_Expression(),
						XUpdateMode.REDEFINE, XAddUpdateMode.LAST, newIf, null,
						null);

				//Execute, so we can use the baseVar
				t.setExecuteNow(true);

				t = SemanticsFactory.eINSTANCE.createXTransition();

				// Get the chain, which we want to set 
				EObject updatingChainBecauseOfAction = null;
				if (trg.getValue() == MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE_VALUE)
					updatingChainBecauseOfAction = ((MIf) this
							.getNamedExpression().get(0).getExpression())
									.getCondition();
				else if (trg
						.getValue() == MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE)
					updatingChainBecauseOfAction = (((MIf) this
							.getNamedExpression().get(0).getExpression())
									.getThenPart().getExpression());
				else
					updatingChainBecauseOfAction = (((MIf) this
							.getNamedExpression().get(0).getExpression())
									.getElsePart().getExpression());

				// Add the lastExpr as baseVar in the chain

				XUpdate update = null;
				// To be done
				update = t.addAttributeUpdate(updatingChainBecauseOfAction,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_Base(),
						XUpdateMode.REDEFINE, XAddUpdateMode.LAST,
						ExpressionsPackage.eINSTANCE.getExpressionBase()
								.getEEnumLiteral(
										this.variableFromExpression(lastExpr)
												.getCalculatedBase().getName()),
						null, null);
				// Sets baseVar
				t.addReferenceUpdate(updatingChainBecauseOfAction,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_BaseVar(),
						XUpdateMode.REDEFINE, XAddUpdateMode.LAST, lastExpr,
						null, null);

				//  Focus change examples, use in code and see what happens
				t.getFocusObjects()
						.add(t.nextStateObjectDefinitionFromObject(this));
				//	t.getFocusObjects().add(updatingChainBecauseOfAction);

				return update;
			}

		}
		case MExprAnnotationAction.INTO_APPLY_VALUE: {
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			XUpdate currentTrigger = transition
					.addAttributeUpdate(this,
							McorePackage.Literals.MPROPERTY__DO_ACTION,
							XUpdateMode.REDEFINE, null,
							//:=
							AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
									.getEEnumLiteral(trg.getValue()),
							null, null);

			MNamedExpression ne = (MNamedExpression) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MNAMED_EXPRESSION);

			currentTrigger.addReferenceUpdate(this,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_EXPRESSION,
					XUpdateMode.ADD, XAddUpdateMode.FIRST, ne, null, null);
			currentTrigger.addAttributeUpdate(ne,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null,
					"Var ".concat(
							String.valueOf(this.getNamedExpression().size())),
					null, null);

			MApplication a = (MApplication) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MAPPLICATION);

			MOperator o = null;
			if (this.getCalculatedSimpleType() == SimpleType.STRING) {
				o = MOperator.CONCAT;
			} else if (this.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
				o = MOperator.AND_OP;
			} else if (this.getCalculatedSimpleType() == SimpleType.INTEGER) {
				o = MOperator.PLUS;
			} else if (this.getCalculatedSimpleType() == SimpleType.DOUBLE) {
				o = MOperator.TIMES;
			} else if (this.getCalculatedSimpleType() == SimpleType.DATE) {
				o = MOperator.DIFF_IN_HRS;
			} else {
				o = MOperator.INTERSECTION;
			}

			currentTrigger.addAttributeUpdate(a,
					ExpressionsPackage.Literals.MAPPLICATION__OPERATOR,
					XUpdateMode.REDEFINE, null,
					ExpressionsPackage.Literals.MOPERATOR
							.getEEnumLiteral(o.getValue()),
					null, null);
			currentTrigger.addReferenceUpdate(ne,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
					XUpdateMode.REDEFINE, null, a, null, null);

			MChain chain1 = (MChain) currentTrigger.createNextStateNewObject(
					ExpressionsPackage.Literals.MCHAIN);
			MChain chain2 = (MChain) currentTrigger.createNextStateNewObject(
					ExpressionsPackage.Literals.MCHAIN);

			ArrayList<MChain> c = new ArrayList<MChain>();
			c.add(chain1);
			c.add(chain2);

			currentTrigger.addReferenceUpdate(a,
					ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
					XUpdateMode.REDEFINE, XAddUpdateMode.FIRST, c, null, null);

			currentTrigger.addAndMergeUpdate(
					chain1.reuseFromOtherNoMoreUsedChainAsUpdate(this));

			currentTrigger.addAttributeUpdate(this,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE,
					null, ExpressionsPackage.Literals.EXPRESSION_BASE
							.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
					null, null);

			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_BaseVar(),
					XUpdateMode.REDEFINE, null, ne, null, null);

			ExpressionBase base1 = null;
			if (this.getCalculatedSimpleType() == SimpleType.STRING) {
				base1 = ExpressionBase.EMPTY_STRING_VALUE;
			} else if (this.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
				base1 = ExpressionBase.FALSE_VALUE;
			} else if (this.getCalculatedSimpleType() == SimpleType.INTEGER) {
				base1 = ExpressionBase.ONE_VALUE;
			} else if (this.getCalculatedSimpleType() == SimpleType.DOUBLE) {
				base1 = ExpressionBase.ONE_VALUE;
			} else if (this.getCalculatedSimpleType() == SimpleType.DATE) {
				base1 = ExpressionBase.NULL_VALUE;
			} else {
				if (this.getCalculatedSingular() == true) {
					base1 = ExpressionBase.NULL_VALUE;
				} else {
					base1 = ExpressionBase.EMPTY_COLLECTION;
				}
				o = MOperator.INTERSECTION;
			}

			currentTrigger
					.addAttributeUpdate(chain2,
							ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE,
							XUpdateMode.REDEFINE, null,
							ExpressionsPackage.Literals.EXPRESSION_BASE
									.getEEnumLiteral(base1.getValue()),
							null, null);

			transition.getFocusObjects().add(
					currentTrigger.nextStateObjectDefinitionFromObject(chain2));
			return currentTrigger;
		}
		case MExprAnnotationAction.ARGUMENT_VALUE: {
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			XUpdate currentTrigger = transition
					.addAttributeUpdate(this,
							McorePackage.Literals.MPROPERTY__DO_ACTION,
							XUpdateMode.REDEFINE, null,
							//:=
							AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
									.getEEnumLiteral(trg.getValue()),
							null, null);

			MCallArgument newArg = (MCallArgument) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCALL_ARGUMENT);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE.getMBaseChain_CallArgument(),
					XUpdateMode.ADD, XAddUpdateMode.LAST, newArg, null, null);
			return currentTrigger;
		}
		case MExprAnnotationAction.SUB_CHAIN_VALUE: {
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			XUpdate currentTrigger = transition
					.addAttributeUpdate(this,
							McorePackage.Literals.MPROPERTY__DO_ACTION,
							XUpdateMode.REDEFINE, null,
							//:=
							AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
									.getEEnumLiteral(trg.getValue()),
							null, null);

			MSubChain subChain = (MSubChain) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MSUB_CHAIN);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.Literals.MBASE_CHAIN__SUB_EXPRESSION,
					XUpdateMode.ADD, XAddUpdateMode.LAST, subChain, null, null);
			return currentTrigger;
		}
		case MExprAnnotationAction.COLLECTION_OP_VALUE: {
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			XUpdate currentTrigger = transition
					.addAttributeUpdate(this,
							McorePackage.Literals.MPROPERTY__DO_ACTION,
							XUpdateMode.REDEFINE, null,
							//:=
							AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
									.getEEnumLiteral(trg.getValue()),
							null, null);

			MCollectionExpression collector = (MCollectionExpression) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR,
					XUpdateMode.REDEFINE, null, collector, null, null);
			transition.getFocusObjects()
					.add(currentTrigger.nextStateObjectDefinitionFromObject(
							collector.getExpression()));
			return currentTrigger;
		}

		case MExprAnnotationAction.LITERAL_CONSTANT_AS_ARGUMENT_VALUE: {
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			XUpdate currentTrigger = transition
					.addAttributeUpdate(this,
							McorePackage.Literals.MPROPERTY__DO_ACTION,
							XUpdateMode.REDEFINE, null,
							//:=
							AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
									.getEEnumLiteral(trg.getValue()),
							null, null);

			MLiteralValueExpr newLiteral = (MLiteralValueExpr) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);

			MNamedExpression newWhere = (MNamedExpression) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MNAMED_EXPRESSION);

			currentTrigger.addReferenceUpdate(this,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_EXPRESSION,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newWhere, null, null);
			currentTrigger.addAttributeUpdate(newWhere,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null,
					"Var ".concat(
							String.valueOf(this.getNamedExpression().size())),
					null, null);

			currentTrigger.addReferenceUpdate(newWhere,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
					XUpdateMode.REDEFINE, null, newLiteral, null, null);

			MCallArgument newArg = (MCallArgument) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCALL_ARGUMENT);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE.getMBaseChain_CallArgument(),
					XUpdateMode.ADD, XAddUpdateMode.LAST, newArg, null, null);

			currentTrigger.addAttributeUpdate(newArg,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE,
					null, ExpressionsPackage.Literals.EXPRESSION_BASE
							.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
					null, null);

			currentTrigger.addReferenceUpdate(newArg,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_BaseVar(),
					XUpdateMode.REDEFINE, null, newWhere, null, null);

			return currentTrigger;
		}
		case MExprAnnotationAction.DATA_CONSTANT_AS_ARGUMENT_VALUE: {
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			XUpdate currentTrigger = transition
					.addAttributeUpdate(this,
							McorePackage.Literals.MPROPERTY__DO_ACTION,
							XUpdateMode.REDEFINE, null,
							//:=
							AnnotationsPackage.Literals.MEXPR_ANNOTATION_ACTION
									.getEEnumLiteral(trg.getValue()),
							null, null);

			MDataValueExpr newData = (MDataValueExpr) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MDATA_VALUE_EXPR);

			MNamedExpression newWhere = (MNamedExpression) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MNAMED_EXPRESSION);

			currentTrigger.addReferenceUpdate(this,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_EXPRESSION,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newWhere, null, null);
			currentTrigger.addAttributeUpdate(newWhere,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null,
					"Var ".concat(
							String.valueOf(this.getNamedExpression().size())),
					null, null);

			currentTrigger.addReferenceUpdate(newWhere,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
					XUpdateMode.REDEFINE, null, newData, null, null);

			MCallArgument newArg = (MCallArgument) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCALL_ARGUMENT);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE.getMBaseChain_CallArgument(),
					XUpdateMode.ADD, XAddUpdateMode.LAST, newArg, null, null);

			currentTrigger.addAttributeUpdate(newArg,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE,
					null, ExpressionsPackage.Literals.EXPRESSION_BASE
							.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
					null, null);

			currentTrigger.addReferenceUpdate(newArg,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_BaseVar(),
					XUpdateMode.REDEFINE, null, newWhere, null, null);

			return currentTrigger;
		}

		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariableBaseDefinition variableFromExpression(
			MNamedExpression namedExpression) {

		/**
		 * @OCL if self.localScopeVariables->isEmpty() then null
		else
		self.localScopeVariables->select(x:mcore::expressions::MVariableBaseDefinition| if x.namedExpression<> null  then x.namedExpression = namedExpression else false endif )->first() endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (AnnotationsPackage.Literals.MEXPR_ANNOTATION);
		EOperation eOperation = AnnotationsPackage.Literals.MEXPR_ANNOTATION
				.getEOperations().get(3);
		if (variableFromExpressionexpressionsMNamedExpressionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				variableFromExpressionexpressionsMNamedExpressionBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				variableFromExpressionexpressionsMNamedExpressionBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("namedExpression", namedExpression);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MVariableBaseDefinition) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String autoCastWithProc() {

		/**
		 * @OCL  let code: String = if baseDefinition.oclIsKindOf(mcore::expressions::MBaseDefinition) 
		then asCodeForBuiltIn()
		else if (baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)) then asCodeForConstants()
		else if baseDefinition.oclIsKindOf(mcore::expressions::MVariableBaseDefinition) then asCodeForVariables()
		else asCodeForOthers() endif endif endif in
		let apply: mcore::expressions::MApplication= if self.eContainer().oclIsTypeOf(mcore::expressions::MApplication ) then self.eContainer().oclAsType(mcore::expressions::MApplication) else null endif in
		let chainTypeString: String =typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, chainCalculatedSingular) in
		let chainTypeStringSingular: String = typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, true) in
		
		--let castType: MClassifier =  if apply.oclIsUndefined() then self.expectedReturnType else if self.castType.oclIsUndefined() then apply.operands->first().calculatedOwnType else self.castType endif endif
		let castType: mcore::MClassifier =  if self.castType.oclIsUndefined() then (if apply.oclIsUndefined() then self.expectedReturnType else apply.operands->first().calculatedOwnType  endif) else self.castType endif  -- CastType has to be preferred to autocast
		
		in
		let castTypeString: String = typeAsOcl(selfObjectPackage, castType, SimpleType::None, chainCalculatedSingular) in
		let castTypeStringSingular: String = typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) in
		
		let opChangesReturn : Boolean = apply.operands->first().calculatedSimpleType <> apply.calculatedSimpleType or apply.operands->first().calculatedType <> apply.calculatedType
		in
		    let variableName : String = self.uniqueChainNumber() in  
		    
		    -- chain name
		    let chainName: String = 
		    if self.processor= mcore::expressions::MProcessor::None then '' else
		'let '.concat(variableName).concat(': ').concat(if self.chainCalculatedSingular then castTypeStringSingular else castTypeString endif).concat(' = ') endif
		in
		-- chainname end	
		
		if  
		
		not(self.castType.oclIsUndefined()) or 
		--new
		(if (not(apply.oclIsUndefined()) and (apply.calculatedOwnSimpleType= SimpleType::Boolean and self.calculatedSimpleType <> mcore::SimpleType::Boolean  or (apply.calculatedOwnSimpleType= mcore::SimpleType::Integer and self.calculatedSimpleType <> mcore::SimpleType::Integer)))then 
		if apply.operands->first().calculatedOwnType.oclIsUndefined() then false
		else    apply.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and apply.operands->first().calculatedOwnType<> self.calculatedOwnType
		endif else false endif)
		--new     
		or
		
		( if apply.oclIsUndefined() then self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType               --   check is dont have an apply but chain only    :::::::.changed OwnType for Collection
		else ((apply.calculatedOwnSimpleType = self.calculatedOwnSimpleType) or opChangesReturn) and apply.calculatedOwnSimpleType= mcore::SimpleType::None endif)      --check if Applytype differs from chain and type is a Classifier
		
		then
		chainName.concat('let chain: ').concat(chainTypeString).concat(' = ').concat(code).concat(' in\n').concat(
		  if chainCalculatedSingular then 'if chain.oclIsUndefined()'.concat('\n').concat('  then null\n  else ') else '' endif
		).concat(
		  if chainCalculatedSingular
		    then
		      'if chain.oclIsKindOf('.concat(castTypeStringSingular).concat(')\n    then chain.oclAsType(').concat(castTypeStringSingular).concat(')\n    else null\n  endif')
		    else 
		
		'chain->iterate(i:'.concat(chainTypeStringSingular).concat('; r: OrderedSet(').concat(castTypeStringSingular).concat(')=OrderedSet{} | if i.oclIsKindOf(').concat(castTypeStringSingular).concat(') then r->including(i.oclAsType(').concat(castTypeStringSingular).concat(')').concat(')->asOrderedSet() \n else r endif)')
		     endif
		 .concat(
		  if chainCalculatedSingular then '\n  endif' else '' endif
		).concat(if self.processor= MProcessor::None then '' else ' in\n'.concat(    -- Accessing feature is not unary
		
		'if ').concat(variableName).concat(if self.isOwnXOCLOperator() then '.oclIsUndefined() or '.concat(variableName) else ''endif).concat(if self.isProcessorSetOperator() then '->' else if not(self.isProcessorCheckEqualOperator()) then '.' else '' endif endif).concat(self.procAsCode())
		.concat(if self.isProcessorCheckEqualOperator() then ' then true else false ' 
		
		
		else (if processor=mcore::expressions::MProcessor::AsOrderedSet then '->' else '.' endif).concat('oclIsUndefined() \n then null \n else '  -- todo   processor does not influence calculatedSingular
		.concat(variableName).concat(
		  if self.isProcessorSetOperator() then '->' else '.' endif --  "->" for Set , "." for unary ,  nothing if we check for a value
		  ).concat(self.procAsCode())) endif) 
		  
		  .concat( 
		 '\n  endif' )  
		 endif))
		else
		
		let procString : String =  if self.processor <> mcore::expressions::MProcessor::None then
		--  let a: String =  '->iterate(i:'.concat(chainTypeStringSingular).concat('; r: String = \'\' | r.concat(i.toString()) )')  in
		'let '.concat(variableName).concat(': ').concat(chainTypeString).concat(' = ').concat(code).concat(' in\n').concat(    -- Accessing feature is not unary
		
		'if ').concat(variableName).concat(if self.isOwnXOCLOperator() then '.oclIsUndefined() or '.concat(variableName) else ''endif).concat(if self.isProcessorSetOperator() then '->' else if not(self.isProcessorCheckEqualOperator()) then '.' else '' endif endif).concat(self.procAsCode())
		.concat(if self.isProcessorCheckEqualOperator() then ' then true else false ' 
		
		
		else (if processor=mcore::expressions::MProcessor::AsOrderedSet then '->' else '.' endif).concat('oclIsUndefined() \n then null \n else '  -- todo   processor does not influence calculatedSingular
		.concat(variableName).concat(
		  if self.isProcessorSetOperator() then '->' else '.' endif --  "->" for Set , "." for unary ,  nothing if we check for a value
		  ).concat(self.procAsCode())) endif) 
		  
		  .concat( 
		 '\n  endif'  )  else code endif in
		 procString.concat
		 (if self.chainCalculatedSingular or self.processor <> mcore::expressions::MProcessor::None then '.toString()' else  '->iterate(i:'.concat(chainTypeStringSingular).concat('; r: String = \'\' | r.concat(i.toString()) )')
		  endif)
		
		 
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(0);
		if (autoCastWithProcBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				autoCastWithProcBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(autoCastWithProcBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForConstants() {

		/**
		 * @OCL if baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition)
		then let b: mcore::expressions::MSimpleTypeConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MSimpleTypeConstantBaseDefinition) in
		b.namedConstant.eName
		else if baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)
		then let b: mcore::expressions::MLiteralConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MLiteralConstantBaseDefinition) in
		b.namedConstant.eName
		else null endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE
				.getEOperations().get(2);
		if (asCodeForConstantsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForConstantsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForConstantsBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForVariables() {

		/**
		 * @OCL let v: mcore::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mcore::expressions::MVariableBaseDefinition) in
		let vName: String = v.namedExpression.eName in
		
		if v.calculatedSingular and (length() > 0)
		then 
		'if '.concat(vName).concat(' = null\n  then null\n  else ').concat(asCodeForOthers()).concat(' endif')
		else asCodeForOthers()
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(14);
		if (asCodeForVariablesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForVariablesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForVariablesBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate processorDefinition$Update(MProcessorDefinition trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.expressions.MProcessorDefinition triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE
						.getMAbstractChain_ProcessorDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ownToApplyMismatch() {

		/**
		 * @OCL if self.eContainer().oclIsTypeOf(mcore::expressions::MIf) then false   -- Implement IF typemismatch
		else
		if self.eContainer().oclIsTypeOf(mcore::expressions::MApplication) and self.containedCollector.oclIsUndefined()
		then 
		let app: mcore::expressions::MApplication = self.eContainer().oclAsType(mcore::expressions::MApplication) in
		let opChangesReturn : Boolean = app.operands->first().calculatedSimpleType <> app.calculatedSimpleType or app.operands->first().calculatedType <> app.calculatedType
		--let opChangesReturn : Boolean = app.operands->first().calculatedOwnSimpleType <> app.calculatedOwnSimpleType or app.operands->first().calculatedOwnType <> app.calculatedOwnType
		in
		
		if self.base = mcore::expressions::ExpressionBase::SelfObject or  self.base = mcore::expressions::ExpressionBase::Variable
		then -- builtin been casted  : TODO  add Parameter,Iterator etc...
		
		if (app.calculatedOwnSimpleType= SimpleType::Boolean and self.calculatedSimpleType <> mcore::SimpleType::Boolean) or (app.calculatedOwnSimpleType= mcore::SimpleType::Integer and self.calculatedSimpleType <> mcore::SimpleType::Integer)  then 
		if app.operands->first().calculatedOwnType.oclIsUndefined() then (self.calculatedOwnSimpleType <>  app.operands->first().calculatedOwnSimpleType and app.calculatedSimpleType<> mcore::SimpleType::Double)
		else    app.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and app.operands->first().calculatedOwnType<> self.calculatedOwnType
		endif
		
		else
		
		
		if ((app.calculatedOwnSimpleType = self.calculatedOwnSimpleType) or opChangesReturn) and (app.calculatedOwnSimpleType= SimpleType::None)-- CHanged to App.calcOwnType
		then
		app.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and app.operands->first().calculatedOwnType<> self.calculatedOwnType
		else if  ((app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType) or opChangesReturn) and  (app.calculatedSimpleType <> SimpleType::None and (app.calculatedSimpleType <> SimpleType::Double))
		-- (app.calculatedSimpleType <> SimpleType::None  and app.calculatedSimpleType <> SimpleType::Boolean)
		then true else false endif
		
		endif
		
		endif
		
		else
		
		if self.base = ExpressionBase::SelfObject 
		then
		(app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType) and opChangesReturn
		else 
		false
		endif
		endif
		
		
		else if not(self.eContainer().oclIsTypeOf(MApplication)) and self.containedCollector.oclIsUndefined() 
		and self.base = ExpressionBase::SelfObject then     --TODO  add parameter,iterator etc
		--self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType or     FOR TYPE
		if self.eContainer().oclIsTypeOf(MNamedExpression) and self.eContainer().oclIsTypeOf(MNamedExpression).oclIsTypeOf(mcore::annotations::MResult) and
		self.eContainer().oclAsType(MNamedExpression).eContainer().oclAsType(mcore::annotations::MExprAnnotation).namedExpression->asSequence()->last() = self.eContainer() 
		--   or self.eContainer().oclIsTypeOf(MCollectionExpression)
		then
		if self.expectedReturnType.oclIsUndefined() then self.expectedReturnType = self.calculatedOwnType and self.expectedReturnSimpleType <> self.calculatedOwnSimpleType 
		else
		self.expectedReturnType.allSubTypes()->excludes(self.calculatedType) and  self.expectedReturnType <>self.calculatedOwnType and self.expectedReturnSimpleType = self.calculatedOwnSimpleType 
		endif 
		else false endif
		
		
		else
		
		false endif endif
		endif
		
		--if (app.calculatedOwnSimpleType = SimpleType::Double and self.calculatedOwnSimpleType= SimpleType::Integer) then false
		--else
		--    app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType
		--endif
		--endif
		--else
		--null
		-- endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(1);
		if (ownToApplyMismatchBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ownToApplyMismatchBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ownToApplyMismatchBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String uniqueChainNumber() {

		/**
		 * @OCL 		        let variableName : String = 'chain'.concat(if self.eContainer().oclAsType(mcore::expressions::MApplication).oclIsUndefined() then '' else  self.eContainer().oclAsType(mcore::expressions::MApplication).uniqueApplyNumber().toString() endif).concat(if self.eContainer().oclAsType(mcore::expressions::MApplication).oclIsUndefined() then '' else
		    
		     if (self.eContainer().oclIsTypeOf(mcore::expressions::MApplication))  then 
		      if (self.eContainer().oclAsType(mcore::expressions::MApplication).operands->isEmpty()) and self.oclIsTypeOf(mcore::expressions::MChain) then ' '  else 
		       self.eContainer().oclAsType(mcore::expressions::MApplication).operands->iterate(i:mcore::expressions::MChainOrApplication; r: OrderedSet(MBaseChain)=OrderedSet{} | if i.oclIsKindOf(MBaseChain) then r->including(i.oclAsType(MBaseChain))->asOrderedSet() else r endif )->indexOf(self).toString()
		       endif else '' 
		       endif endif ) in variableName
		       
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(2);
		if (uniqueChainNumberBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				uniqueChainNumberBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(uniqueChainNumberBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean reuseFromOtherNoMoreUsedChain(MBaseChain archetype) {
		//ATTENTION: Duplicated in MExprAnnotation and MBaseChain
		//Add all operations of expr annotation, base, element 1-3, cast, processor
		this.setBaseDefinition(archetype.getBaseDefinition());
		this.setElement1(archetype.getElement1());
		this.setElement2(archetype.getElement2());
		this.setElement3(archetype.getElement3());
		this.setCastType(archetype.getCastType());
		this.setProcessor(archetype.getProcessor());
		this.setContainedCollector(archetype.getContainedCollector());
		if (!(archetype.getSubExpression() == null)
				&& !archetype.getSubExpression().isEmpty()) {
			this.getSubExpression().addAll(archetype.getSubExpression());
		}
		if (!(archetype.getCallArgument() == null)
				&& !archetype.getCallArgument().isEmpty()) {
			this.getCallArgument().addAll(archetype.getCallArgument());
		}
		return true;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean resetToBase(ExpressionBase base, MVariable baseVar) {
		//ATTENTION: Duplicated in MExprAnnotation and MBaseChain

		this.setBase(base);
		this.setBaseVar(baseVar);
		this.setElement1(null);
		this.setElement2(null);
		this.setElement3(null);
		this.setCastType(null);
		this.setProcessor(null);
		this.setContainedCollector(null);
		if (!(getCallArgument() == null) && !getCallArgument().isEmpty()) {
			this.getCallArgument().clear();
		}
		if (!(getSubExpression() == null) && !getSubExpression().isEmpty()) {
			this.getSubExpression().clear();
		}
		return true;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate reuseFromOtherNoMoreUsedChainAsUpdate(MBaseChain archetype) {

		/**
		 * @OCL null
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(5);
		if (reuseFromOtherNoMoreUsedChainAsUpdateexpressionsMBaseChainBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				reuseFromOtherNoMoreUsedChainAsUpdateexpressionsMBaseChainBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				reuseFromOtherNoMoreUsedChainAsUpdateexpressionsMBaseChainBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("archetype", archetype);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (XUpdate) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isProcessorCheckEqualOperator() {

		/**
		 * @OCL if processor.oclIsUndefined() then false 
		else
		processor=expressions::MProcessor::IsOne or
		processor=expressions::MProcessor::IsZero or
		processor = expressions::MProcessor::IsFalse or
		processor = expressions::MProcessor::IsTrue or
		processor = expressions::MProcessor::NotNull 
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(6);
		if (isProcessorCheckEqualOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isProcessorCheckEqualOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isProcessorCheckEqualOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isPrefixProcessor() {

		/**
		 * @OCL if processor.oclIsUndefined() then false 
		else
		processor=expressions::MProcessor::Not or
		processor = expressions::MProcessor::OneDividedBy
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(7);
		if (isPrefixProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isPrefixProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isPrefixProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isPostfixProcessor() {

		/**
		 * @OCL if processor.oclIsUndefined() then false 
		else
		processor=expressions::MProcessor::PlusOne or
		processor=expressions::MProcessor::MinusOne or
		processor=expressions::MProcessor::TimesMinusOne or
		processor=expressions::MProcessor::IsNull or
		processor=expressions::MProcessor::IsInvalid
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(8);
		if (isPostfixProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isPostfixProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isPostfixProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForOthers() {

		/**
		 * @OCL if length()=0 then baseAsCode
		else if length()=1 then codeForLength1() 
		else if length()=2 then codeForLength2() 
		else if length()=3 then codeForLength3() 
		else 'ERROR' endif endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(9);
		if (asCodeForOthersBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForOthersBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForOthersBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeElementAsCode(Integer step) {

		/**
		 * @OCL let element: mcore::MProperty = if step=1 then element1
		else if step=2 then element2 
		else if step=3 then element3
		else null endif endif endif in
		
		if element.oclIsUndefined() then 'ERROR' else 
		if element.isOperation
		then if step=length()
		then
		let p: String = let pp: String = callArgument->iterate(
		x: mcore::expressions::MCallArgument; s: String = '' | 
		s.concat(', ').concat(x.asCode)
		) in if pp.size()>2 then pp.substring(3,pp.size()) else pp endif in
		element.eName.concat('(').concat(p).concat(')') 
		else		
		element.eName.concat('()') endif		
		else 
		element.eName 
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(10);
		if (unsafeElementAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeElementAsCodeecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(unsafeElementAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("step", step);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength1() {

		/**
		 * @OCL let chain:mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in
		
		let b: String = if (chain.base = mcore::expressions::ExpressionBase::SelfObject ) then '' else chain.baseAsCode.concat('.') endif in 
		b.concat(unsafeChainStepAsCode(1)).concat(if chain.chainCalculatedSingular then '' else '->asOrderedSet()' endif)
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(11);
		if (codeForLength1BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				codeForLength1BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength1BodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength2() {

		/**
		 * @OCL 
		let chain: mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in
		let abstract:  mcore::expressions::MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in
		let b0: String = if (abstract.base = mcore::expressions::ExpressionBase::SelfObject ) then '' else abstract.baseAsCode.concat('.') endif in 
		let b:String = if b0.oclIsUndefined() then 'PROBLEM WITH BASE' else b0 endif in
		if element1.calculatedSingular and element2.calculatedSingular then
		let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then null\n  else ').concat(b).concat(unsafeChainAsCode(1,2)).concat('\nendif')
		
		else if element1.calculatedSingular and (not element2.calculatedSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,2).concat('\nendif'))
		
		else  if (not element1.calculatedSingular) and element2.calculatedSingular then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else  if (not element1.calculatedSingular) and (not element2.calculatedSingular) then
		b.concat(unsafeChainAsCode(1,2)).concat('->asOrderedSet()')
		
		else null
		
		endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(12);
		if (codeForLength2BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				codeForLength2BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength2BodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength3() {

		/**
		 * @OCL let chain: mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in
		let abstract:  mcore::expressions::MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in
		
		
		let b: String = if (abstract.base = mcore::expressions::ExpressionBase::SelfObject ) then '' else abstract.baseAsCode.concat('.') endif in 
		
		if element1.calculatedSingular and element2.calculatedSingular and element3.calculatedSingular then
		'if '.concat(b).concat(unsafeChainAsCode(1,2)).concat('.oclIsUndefined()\n  then null\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('\nendif')
		
		else if element1.calculatedSingular and element2.calculatedSingular and (not element3.calculatedSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,2)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('\nendif')
		
		else if element1.calculatedSingular and (not element2.calculatedSingular) and element3.calculatedSingular then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('->reject(oclIsUndefined())->asOrderedSet()\nendif')
		
		else if element1.calculatedSingular and (not element2.calculatedSingular) and (not element3.calculatedSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('->asOrderedSet()\nendif')
		
		else if (not element1.calculatedSingular) and element2.calculatedSingular and element3.calculatedSingular then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else if (not element1.calculatedSingular) and element2.calculatedSingular and (not element3.calculatedSingular) then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat(if chain.subExpression->isEmpty() and not(self.processorIsSet()) then '' else '->asOrderedSet()' endif)
		
		else if (not element1.calculatedSingular) and (not element2.calculatedSingular) and element3.calculatedSingular then
		b.concat(unsafeChainAsCode(1,3)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else if (not element1.calculatedSingular) and (not element2.calculatedSingular) and (not element3.calculatedSingular) then
		b.concat(unsafeChainAsCode(1,3)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else null
		endif endif endif endif endif endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(13);
		if (codeForLength3BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				codeForLength3BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength3BodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer length() {

		/**
		 * @OCL if not element3.oclIsUndefined() then 3
		else if not element2.oclIsUndefined() then 2
		else if not element1.oclIsUndefined() then 1
		else 0 endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(1);
		if (lengthBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				lengthBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(lengthBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainStepAsCode(Integer step) {

		/**
		 * @OCL if step=1 then
		if element1.oclIsUndefined() then 'MISSING ELEMENT 1'
		else unsafeElementAsCode(1) endif
		else if step=2 then
		if element2.oclIsUndefined() then 'MISSING ELEMENT 2'
		else unsafeElementAsCode(2) endif
		else if step=3 then
		if element3.oclIsUndefined() then 'MISSING ELEMENT 3'
		else unsafeElementAsCode(3) endif
		else 'ERROR'
		endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(3);
		if (unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("step", step);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainAsCode(Integer fromStep) {

		/**
		 * @OCL unsafeChainAsCode(fromStep, length())
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(4);
		if (unsafeChainAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeChainAsCodeecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(unsafeChainAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("fromStep", fromStep);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainAsCode(Integer fromStep, Integer toStep) {

		/**
		 * @OCL let end: Integer = if (length() > toStep) then toStep else length() endif in
		
		if fromStep=1 then
		if end=3 then
		unsafeChainStepAsCode(1).concat('.').concat(unsafeChainStepAsCode(2)).concat('.').concat(unsafeChainStepAsCode(3)) 
		else if end=2 then
		unsafeChainStepAsCode(1).concat('.').concat(unsafeChainStepAsCode(2))
		else if end=1 then unsafeChainStepAsCode(1) else '' endif
		endif endif
		else if fromStep=2 then
		if end=3 then
		unsafeChainStepAsCode(2).concat('.').concat(unsafeChainStepAsCode(3)) 
		else if end=2 then unsafeChainStepAsCode(2) else '' endif
		endif
		else if fromStep=3 then
		if end=3 then unsafeChainStepAsCode(3) else '' endif
		else 'ERROR'
		endif endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(5);
		if (unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("fromStep", fromStep);

			evalEnv.add("toStep", toStep);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String procAsCode() {

		/**
		 * @OCL if self.processor= mcore::expressions::MProcessor::IsNull then '.oclIsUndefined()' 
		else if self.processor = mcore::expressions::MProcessor::AllUpperCase then 'toUpperCase()'
		else if self.processor = mcore::expressions::MProcessor::IsInvalid then '.oclIsInvalid()'
		else if self.processor = mcore::expressions::MProcessor::Container then 'eContainer()'
		else  self.processor.toString()
		endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(10);
		if (procAsCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procAsCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procAsCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isCustomCodeProcessor() {

		/**
		 * @OCL if self.processorIsSet().oclIsUndefined() then null
		else 
		processor = mcore::expressions::MProcessor::Head or
		processor = mcore::expressions::MProcessor::Tail or
		processor = mcore::expressions::MProcessor::And or
		processor = mcore::expressions::MProcessor::Or  
		endif
		
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(11);
		if (isCustomCodeProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isCustomCodeProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isCustomCodeProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isProcessorSetOperator() {

		/**
		 * @OCL if processor = mcore::expressions::MProcessor::None then false 
		else
		processor=mcore::expressions::MProcessor::AsOrderedSet or
		processor=mcore::expressions::MProcessor::First or
		processor=mcore::expressions::MProcessor::IsEmpty or
		processor=mcore::expressions::MProcessor::Last or
		processor=mcore::expressions::MProcessor::NotEmpty or
		processor=mcore::expressions::MProcessor::Size or
		processor=mcore::expressions::MProcessor::Sum or
		processor=mcore::expressions::MProcessor::Head or
		processor=mcore::expressions::MProcessor::Tail or
		processor=mcore::expressions::MProcessor::And or
		processor=mcore::expressions::MProcessor::Or
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(12);
		if (isProcessorSetOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isProcessorSetOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isProcessorSetOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOwnXOCLOperator() {

		/**
		 * @OCL processor =mcore::expressions::MProcessor::CamelCaseLower or
		processor =mcore::expressions::MProcessor::CamelCaseToBusiness or
		processor =mcore::expressions::MProcessor::CamelCaseUpper 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(13);
		if (isOwnXOCLOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isOwnXOCLOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOwnXOCLOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean processorReturnsSingular() {

		/**
		 * @OCL if self.processor = mcore::expressions::MProcessor::None then null
		else if
		self.processor = mcore::expressions::MProcessor::AsOrderedSet or
		processor = mcore::expressions::MProcessor::Head or
		processor= mcore::expressions::MProcessor::Tail
		
		then 
		false
		else true
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(14);
		if (processorReturnsSingularBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				processorReturnsSingularBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(processorReturnsSingularBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean processorIsSet() {

		/**
		 * @OCL self.processor <> mcore::expressions::MProcessor::None
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(15);
		if (processorIsSetBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				processorIsSetBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(processorIsSetBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition createProcessorDefinition() {

		/**
		 * @OCL Tuple{processor=processor}
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(16);
		if (createProcessorDefinitionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				createProcessorDefinitionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(createProcessorDefinitionBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProcessorDefinition) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForObject() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::Container},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(17);
		if (procDefChoicesForObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForObjects() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail},
		Tuple{processor=mcore::expressions::MProcessor::Container}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(18);
		if (procDefChoicesForObjectsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForObjectsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForObjectsBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForBoolean() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsFalse},
		Tuple{processor=mcore::expressions::MProcessor::IsTrue},
		Tuple{processor=mcore::expressions::MProcessor::Not},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(19);
		if (procDefChoicesForBooleanBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForBooleanBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForBooleanBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForBooleans() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::And},
		Tuple{processor=mcore::expressions::MProcessor::Or},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(20);
		if (procDefChoicesForBooleansBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForBooleansBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForBooleansBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForInteger() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsZero},
		Tuple{processor=mcore::expressions::MProcessor::IsOne},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(21);
		if (procDefChoicesForIntegerBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForIntegerBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForIntegerBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForIntegers() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Sum},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(22);
		if (procDefChoicesForIntegersBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForIntegersBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForIntegersBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForReal() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Round},
		Tuple{processor=mcore::expressions::MProcessor::Floor},
		Tuple{processor=mcore::expressions::MProcessor::IsZero},
		Tuple{processor=mcore::expressions::MProcessor::IsOne},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(23);
		if (procDefChoicesForRealBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForRealBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForRealBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForReals() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Round},
		Tuple{processor=mcore::expressions::MProcessor::Floor},
		Tuple{processor=mcore::expressions::MProcessor::Sum},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(24);
		if (procDefChoicesForRealsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForRealsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForRealsBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForString() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Trim},
		Tuple{processor=mcore::expressions::MProcessor::AllLowerCase},
		Tuple{processor=mcore::expressions::MProcessor::AllUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::FirstUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseLower},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},
		Tuple{processor=mcore::expressions::MProcessor::ToBoolean},
		Tuple{processor=mcore::expressions::MProcessor::ToInteger},
		Tuple{processor=mcore::expressions::MProcessor::ToReal},
		Tuple{processor=mcore::expressions::MProcessor::ToDate},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(25);
		if (procDefChoicesForStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForStrings() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Trim},
		Tuple{processor=mcore::expressions::MProcessor::AllLowerCase},
		Tuple{processor=mcore::expressions::MProcessor::AllUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::FirstUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseLower},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},
		Tuple{processor=mcore::expressions::MProcessor::ToBoolean},
		Tuple{processor=mcore::expressions::MProcessor::ToInteger},
		Tuple{processor=mcore::expressions::MProcessor::ToReal},
		Tuple{processor=mcore::expressions::MProcessor::ToDate},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(26);
		if (procDefChoicesForStringsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForStringsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForStringsBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForDate() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Year},
		Tuple{processor=mcore::expressions::MProcessor::Month},
		Tuple{processor=mcore::expressions::MProcessor::Day},
		Tuple{processor=mcore::expressions::MProcessor::Hour},
		Tuple{processor=mcore::expressions::MProcessor::Minute},
		Tuple{processor=mcore::expressions::MProcessor::Second},
		Tuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},
		Tuple{processor=mcore::expressions::MProcessor::ToHhMm},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(27);
		if (procDefChoicesForDateBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForDateBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForDateBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForDates() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Year},
		Tuple{processor=mcore::expressions::MProcessor::Month},
		Tuple{processor=mcore::expressions::MProcessor::Day},
		Tuple{processor=mcore::expressions::MProcessor::Hour},
		Tuple{processor=mcore::expressions::MProcessor::Minute},
		Tuple{processor=mcore::expressions::MProcessor::Second},
		Tuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},
		Tuple{processor=mcore::expressions::MProcessor::ToHhMm},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(28);
		if (procDefChoicesForDatesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForDatesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForDatesBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate baseDefinition$Update(MAbstractBaseDefinition trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.expressions.MAbstractBaseDefinition triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE
						.getMAbstractExpressionWithBase_BaseDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForBuiltIn() {

		/**
		 * @OCL baseAsCode
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE
				.getEOperations().get(1);
		if (asCodeForBuiltInBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForBuiltInBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForBuiltInBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortCode() {

		/**
		 * @OCL if asCode.oclIsUndefined() then ''
		else let s: String = asCode in
		if s.size() > 30 then s.substring(1, 27).concat('...')
		else s endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION
				.getEOperations().get(0);
		if (getShortCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				getShortCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getShortCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getScope() {

		/**
		 * @OCL entireScope
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION
				.getEOperations().get(1);
		if (getScopeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				getScopeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getScopeBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MAbstractBaseDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate multiplicityCase$Update(MultiplicityCase trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MultiplicityCase triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMTyped_MultiplicityCase(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String typeAsOcl(MPackage oclPackage, MClassifier mClassifier,
			SimpleType simpleType, Boolean singular) {

		/**
		 * @OCL if  mClassifier.oclIsUndefined() and (simpleType = SimpleType::None)  
		then 'MISSING TYPE' 
		else let t: String = 
		if  simpleType<>SimpleType::None
		then 
		if simpleType=SimpleType::Double  then 'Real' 
		else if simpleType=SimpleType::Object then 'ecore::EObject'
		else if simpleType=SimpleType::Annotation then 'ecore::EAnnotation'
		else if simpleType=SimpleType::Attribute then 'ecore::EAttribute'
		else if simpleType=SimpleType::Class then 'ecore::EClass'
		else if simpleType=SimpleType::Classifier then 'ecore::EClassifier'
		else if simpleType=SimpleType::DataType then 'ecore::EDataType'
		else if simpleType=SimpleType::Enumeration then 'ecore::Enumeration'
		else if simpleType=SimpleType::Feature then 'ecore::EStructuralFeature'
		else if simpleType=SimpleType::Literal then 'ecore::EEnumLiteral'
		else if simpleType=SimpleType::KeyValue then 'ecore::EStringToStringMapEntry'
		else if simpleType=SimpleType::NamedElement then 'ecore::NamedElement'
		else if simpleType=SimpleType::Operation then 'ecore::EOperation'
		else if simpleType=SimpleType::Package then 'ecore::EPackage'
		else if simpleType=SimpleType::Parameter then 'ecore::EParameter'
		else if simpleType=SimpleType::Reference then 'ecore::EReference'
		else if simpleType=SimpleType::TypedElement then 'ecore::ETypedElement'
		else if simpleType = SimpleType::Date then 'ecore::EDate' 
		else simpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif
		else 
		/* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified 
		let pkg: String = 
		if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat('::')
		  else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then '' else mClassifier.containingPackage.eName.allLowerCase().concat('::') endif 
		endif
		in pkg.concat(mClassifier.eName) 
		NEW Create string with all package qualifiers *\/
		let pkg:String=
		if mClassifier.containingClassifier   =null then ''
		else mClassifier.containingPackage.qualifiedName.concat('::' ) endif                            in
		pkg.concat(mClassifier.eName)
		endif
		in 
		
		if  singular.oclIsUndefined() then 'MISSING ARITY INFO' else 
		if singular=true then t else 'OrderedSet('.concat(t).concat(') ') endif
		endif
		endif
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MTYPED);
		EOperation eOperation = McorePackage.Literals.MTYPED.getEOperations()
				.get(1);
		if (typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("oclPackage", oclPackage);

			evalEnv.add("mClassifier", mClassifier);

			evalEnv.add("simpleType", simpleType);

			evalEnv.add("singular", singular);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCallArgument())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUseExplicitOcl() {
		return useExplicitOcl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseExplicitOcl(Boolean newUseExplicitOcl) {
		Boolean oldUseExplicitOcl = useExplicitOcl;
		useExplicitOcl = newUseExplicitOcl;
		boolean oldUseExplicitOclESet = useExplicitOclESet;
		useExplicitOclESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL,
					oldUseExplicitOcl, useExplicitOcl, !oldUseExplicitOclESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUseExplicitOcl() {
		Boolean oldUseExplicitOcl = useExplicitOcl;
		boolean oldUseExplicitOclESet = useExplicitOclESet;
		useExplicitOcl = USE_EXPLICIT_OCL_EDEFAULT;
		useExplicitOclESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL,
					oldUseExplicitOcl, USE_EXPLICIT_OCL_EDEFAULT,
					oldUseExplicitOclESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUseExplicitOcl() {
		return useExplicitOclESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
			return ((InternalEList<?>) getCallArgument()).basicRemove(otherEnd,
					msgs);
		case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
			return ((InternalEList<?>) getSubExpression()).basicRemove(otherEnd,
					msgs);
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
			return basicUnsetContainedCollector(msgs);
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION:
			return ((InternalEList<?>) getNamedExpression())
					.basicRemove(otherEnd, msgs);
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE:
			return ((InternalEList<?>) getNamedTuple()).basicRemove(otherEnd,
					msgs);
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT:
			return ((InternalEList<?>) getNamedConstant()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_AS_STRING:
			return getMultiplicityAsString();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE:
			if (resolve)
				return getCalculatedType();
			return basicGetCalculatedType();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_MANDATORY:
			return getCalculatedMandatory();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SINGULAR:
			return getCalculatedSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE_PACKAGE:
			if (resolve)
				return getCalculatedTypePackage();
			return basicGetCalculatedTypePackage();
		case AnnotationsPackage.MEXPR_ANNOTATION__VOID_TYPE_ALLOWED:
			return getVoidTypeAllowed();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SIMPLE_TYPE:
			return getCalculatedSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE:
			return getMultiplicityCase();
		case AnnotationsPackage.MEXPR_ANNOTATION__AS_CODE:
			return getAsCode();
		case AnnotationsPackage.MEXPR_ANNOTATION__AS_BASIC_CODE:
			return getAsBasicCode();
		case AnnotationsPackage.MEXPR_ANNOTATION__COLLECTOR:
			if (resolve)
				return getCollector();
			return basicGetCollector();
		case AnnotationsPackage.MEXPR_ANNOTATION__ENTIRE_SCOPE:
			return getEntireScope();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_BASE:
			return getScopeBase();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SELF:
			return getScopeSelf();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_TRG:
			return getScopeTrg();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJ:
			return getScopeObj();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS:
			return getScopeSimpleTypeConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_LITERAL_CONSTANTS:
			return getScopeLiteralConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return getScopeObjectReferenceConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_VARIABLES:
			return getScopeVariables();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ITERATOR:
			return getScopeIterator();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ACCUMULATOR:
			return getScopeAccumulator();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_PARAMETERS:
			return getScopeParameters();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_CONTAINER_BASE:
			return getScopeContainerBase();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_NUMBER_BASE:
			return getScopeNumberBase();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SOURCE:
			return getScopeSource();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_ENTIRE_SCOPE:
			return getLocalEntireScope();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_BASE:
			return getLocalScopeBase();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SELF:
			return getLocalScopeSelf();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_TRG:
			return getLocalScopeTrg();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJ:
			return getLocalScopeObj();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SOURCE:
			return getLocalScopeSource();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
			return getLocalScopeSimpleTypeConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS:
			return getLocalScopeLiteralConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return getLocalScopeObjectReferenceConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_VARIABLES:
			return getLocalScopeVariables();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ITERATOR:
			return getLocalScopeIterator();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR:
			return getLocalScopeAccumulator();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_PARAMETERS:
			return getLocalScopeParameters();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
			return getLocalScopeNumberBaseDefinition();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_CONTAINER:
			return getLocalScopeContainer();
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_ANNOTATION:
			if (resolve)
				return getContainingAnnotation();
			return basicGetContainingAnnotation();
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_EXPRESSION:
			if (resolve)
				return getContainingExpression();
			return basicGetContainingExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_COMPLEX_EXPRESSION:
			return getIsComplexExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_SUB_EXPRESSION:
			return getIsSubExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_TYPE:
			if (resolve)
				return getCalculatedOwnType();
			return basicGetCalculatedOwnType();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_MANDATORY:
			return getCalculatedOwnMandatory();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SINGULAR:
			return getCalculatedOwnSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SIMPLE_TYPE:
			return getCalculatedOwnSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_TYPE:
			if (resolve)
				return getSelfObjectType();
			return basicGetSelfObjectType();
		case AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_PACKAGE:
			if (resolve)
				return getSelfObjectPackage();
			return basicGetSelfObjectPackage();
		case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_OBJECT_TYPE:
			if (resolve)
				return getTargetObjectType();
			return basicGetTargetObjectType();
		case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_SIMPLE_TYPE:
			return getTargetSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__OBJECT_OBJECT_TYPE:
			if (resolve)
				return getObjectObjectType();
			return basicGetObjectObjectType();
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE:
			if (resolve)
				return getExpectedReturnType();
			return basicGetExpectedReturnType();
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE:
			return getExpectedReturnSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_MANDATORY:
			return getIsReturnValueMandatory();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_SINGULAR:
			return getIsReturnValueSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_TYPE:
			if (resolve)
				return getSrcObjectType();
			return basicGetSrcObjectType();
		case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_SIMPLE_TYPE:
			return getSrcObjectSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_AS_CODE:
			return getBaseAsCode();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE:
			return getBase();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_DEFINITION:
			if (resolve)
				return getBaseDefinition();
			return basicGetBaseDefinition();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR:
			if (resolve)
				return getBaseVar();
			return basicGetBaseVar();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_TYPE:
			if (resolve)
				return getBaseExitType();
			return basicGetBaseExitType();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SIMPLE_TYPE:
			return getBaseExitSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_MANDATORY:
			return getBaseExitMandatory();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SINGULAR:
			return getBaseExitSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_ENTRY_TYPE:
			if (resolve)
				return getChainEntryType();
			return basicGetChainEntryType();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_AS_CODE:
			return getChainAsCode();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1:
			if (resolve)
				return getElement1();
			return basicGetElement1();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1_CORRECT:
			return getElement1Correct();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_ENTRY_TYPE:
			if (resolve)
				return getElement2EntryType();
			return basicGetElement2EntryType();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2:
			if (resolve)
				return getElement2();
			return basicGetElement2();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_CORRECT:
			return getElement2Correct();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_ENTRY_TYPE:
			if (resolve)
				return getElement3EntryType();
			return basicGetElement3EntryType();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3:
			if (resolve)
				return getElement3();
			return basicGetElement3();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_CORRECT:
			return getElement3Correct();
		case AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE:
			if (resolve)
				return getCastType();
			return basicGetCastType();
		case AnnotationsPackage.MEXPR_ANNOTATION__LAST_ELEMENT:
			if (resolve)
				return getLastElement();
			return basicGetLastElement();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_TYPE:
			if (resolve)
				return getChainCalculatedType();
			return basicGetChainCalculatedType();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SIMPLE_TYPE:
			return getChainCalculatedSimpleType();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SINGULAR:
			return getChainCalculatedSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR:
			return getProcessor();
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR_DEFINITION:
			if (resolve)
				return getProcessorDefinition();
			return basicGetProcessorDefinition();
		case AnnotationsPackage.MEXPR_ANNOTATION__TYPE_MISMATCH:
			return getTypeMismatch();
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
			return getCallArgument();
		case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
			return getSubExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
			if (resolve)
				return getContainedCollector();
			return basicGetContainedCollector();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS:
			return getChainCodeforSubchains();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_OWN_XOCL_OP:
			return getIsOwnXOCLOp();
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION:
			return getNamedExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE:
			return getNamedTuple();
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT:
			return getNamedConstant();
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE_OF_ANNOTATION:
			if (resolve)
				return getExpectedReturnTypeOfAnnotation();
			return basicGetExpectedReturnTypeOfAnnotation();
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION:
			return getExpectedReturnSimpleTypeOfAnnotation();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY:
			return getIsReturnValueOfAnnotationMandatory();
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR:
			return getIsReturnValueOfAnnotationSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL:
			return getUseExplicitOcl();
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CHANGED:
			return getOclChanged();
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CODE:
			return getOclCode();
		case AnnotationsPackage.MEXPR_ANNOTATION__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE:
			setMultiplicityCase((MultiplicityCase) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE:
			setBase((ExpressionBase) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_DEFINITION:
			setBaseDefinition((MAbstractBaseDefinition) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR:
			setBaseVar((MVariable) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1:
			setElement1((MProperty) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2:
			setElement2((MProperty) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3:
			setElement3((MProperty) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE:
			setCastType((MClassifier) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR:
			setProcessor((MProcessor) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR_DEFINITION:
			setProcessorDefinition((MProcessorDefinition) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
			getCallArgument().clear();
			getCallArgument()
					.addAll((Collection<? extends MCallArgument>) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
			getSubExpression().clear();
			getSubExpression()
					.addAll((Collection<? extends MSubChain>) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
			setContainedCollector((MCollectionExpression) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION:
			getNamedExpression().clear();
			getNamedExpression()
					.addAll((Collection<? extends MNamedExpression>) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE:
			getNamedTuple().clear();
			getNamedTuple().addAll(
					(Collection<? extends MAbstractNamedTuple>) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT:
			getNamedConstant().clear();
			getNamedConstant()
					.addAll((Collection<? extends MNamedConstant>) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL:
			setUseExplicitOcl((Boolean) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CHANGED:
			setOclChanged((Boolean) newValue);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__DO_ACTION:
			setDoAction((MExprAnnotationAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE:
			setMultiplicityCase(MULTIPLICITY_CASE_EDEFAULT);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE:
			unsetBase();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_DEFINITION:
			setBaseDefinition((MAbstractBaseDefinition) null);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR:
			unsetBaseVar();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1:
			unsetElement1();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2:
			unsetElement2();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3:
			unsetElement3();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE:
			unsetCastType();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR:
			unsetProcessor();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR_DEFINITION:
			setProcessorDefinition((MProcessorDefinition) null);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
			unsetCallArgument();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
			unsetSubExpression();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
			unsetContainedCollector();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION:
			unsetNamedExpression();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE:
			unsetNamedTuple();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT:
			unsetNamedConstant();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL:
			unsetUseExplicitOcl();
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CHANGED:
			setOclChanged(OCL_CHANGED_EDEFAULT);
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_AS_STRING:
			return MULTIPLICITY_AS_STRING_EDEFAULT == null
					? getMultiplicityAsString() != null
					: !MULTIPLICITY_AS_STRING_EDEFAULT
							.equals(getMultiplicityAsString());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE:
			return basicGetCalculatedType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_MANDATORY:
			return CALCULATED_MANDATORY_EDEFAULT == null
					? getCalculatedMandatory() != null
					: !CALCULATED_MANDATORY_EDEFAULT
							.equals(getCalculatedMandatory());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SINGULAR:
			return CALCULATED_SINGULAR_EDEFAULT == null
					? getCalculatedSingular() != null
					: !CALCULATED_SINGULAR_EDEFAULT
							.equals(getCalculatedSingular());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE_PACKAGE:
			return basicGetCalculatedTypePackage() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__VOID_TYPE_ALLOWED:
			return VOID_TYPE_ALLOWED_EDEFAULT == null
					? getVoidTypeAllowed() != null
					: !VOID_TYPE_ALLOWED_EDEFAULT.equals(getVoidTypeAllowed());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SIMPLE_TYPE:
			return getCalculatedSimpleType() != CALCULATED_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE:
			return getMultiplicityCase() != MULTIPLICITY_CASE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__AS_CODE:
			return AS_CODE_EDEFAULT == null ? getAsCode() != null
					: !AS_CODE_EDEFAULT.equals(getAsCode());
		case AnnotationsPackage.MEXPR_ANNOTATION__AS_BASIC_CODE:
			return AS_BASIC_CODE_EDEFAULT == null ? getAsBasicCode() != null
					: !AS_BASIC_CODE_EDEFAULT.equals(getAsBasicCode());
		case AnnotationsPackage.MEXPR_ANNOTATION__COLLECTOR:
			return basicGetCollector() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__ENTIRE_SCOPE:
			return !getEntireScope().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_BASE:
			return !getScopeBase().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SELF:
			return !getScopeSelf().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_TRG:
			return !getScopeTrg().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJ:
			return !getScopeObj().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS:
			return !getScopeSimpleTypeConstants().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_LITERAL_CONSTANTS:
			return !getScopeLiteralConstants().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return !getScopeObjectReferenceConstants().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_VARIABLES:
			return !getScopeVariables().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ITERATOR:
			return !getScopeIterator().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ACCUMULATOR:
			return !getScopeAccumulator().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_PARAMETERS:
			return !getScopeParameters().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_CONTAINER_BASE:
			return !getScopeContainerBase().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_NUMBER_BASE:
			return !getScopeNumberBase().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SOURCE:
			return !getScopeSource().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_ENTIRE_SCOPE:
			return !getLocalEntireScope().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_BASE:
			return !getLocalScopeBase().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SELF:
			return !getLocalScopeSelf().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_TRG:
			return !getLocalScopeTrg().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJ:
			return !getLocalScopeObj().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SOURCE:
			return !getLocalScopeSource().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
			return !getLocalScopeSimpleTypeConstants().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS:
			return !getLocalScopeLiteralConstants().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return !getLocalScopeObjectReferenceConstants().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_VARIABLES:
			return !getLocalScopeVariables().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ITERATOR:
			return !getLocalScopeIterator().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR:
			return !getLocalScopeAccumulator().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_PARAMETERS:
			return !getLocalScopeParameters().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
			return !getLocalScopeNumberBaseDefinition().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_CONTAINER:
			return !getLocalScopeContainer().isEmpty();
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_ANNOTATION:
			return basicGetContainingAnnotation() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_EXPRESSION:
			return basicGetContainingExpression() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_COMPLEX_EXPRESSION:
			return IS_COMPLEX_EXPRESSION_EDEFAULT == null
					? getIsComplexExpression() != null
					: !IS_COMPLEX_EXPRESSION_EDEFAULT
							.equals(getIsComplexExpression());
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_SUB_EXPRESSION:
			return IS_SUB_EXPRESSION_EDEFAULT == null
					? getIsSubExpression() != null
					: !IS_SUB_EXPRESSION_EDEFAULT.equals(getIsSubExpression());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_TYPE:
			return basicGetCalculatedOwnType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_MANDATORY:
			return CALCULATED_OWN_MANDATORY_EDEFAULT == null
					? getCalculatedOwnMandatory() != null
					: !CALCULATED_OWN_MANDATORY_EDEFAULT
							.equals(getCalculatedOwnMandatory());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SINGULAR:
			return CALCULATED_OWN_SINGULAR_EDEFAULT == null
					? getCalculatedOwnSingular() != null
					: !CALCULATED_OWN_SINGULAR_EDEFAULT
							.equals(getCalculatedOwnSingular());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SIMPLE_TYPE:
			return getCalculatedOwnSimpleType() != CALCULATED_OWN_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_TYPE:
			return basicGetSelfObjectType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_PACKAGE:
			return basicGetSelfObjectPackage() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_OBJECT_TYPE:
			return basicGetTargetObjectType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_SIMPLE_TYPE:
			return getTargetSimpleType() != TARGET_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__OBJECT_OBJECT_TYPE:
			return basicGetObjectObjectType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE:
			return basicGetExpectedReturnType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE:
			return getExpectedReturnSimpleType() != EXPECTED_RETURN_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_MANDATORY:
			return IS_RETURN_VALUE_MANDATORY_EDEFAULT == null
					? getIsReturnValueMandatory() != null
					: !IS_RETURN_VALUE_MANDATORY_EDEFAULT
							.equals(getIsReturnValueMandatory());
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_SINGULAR:
			return IS_RETURN_VALUE_SINGULAR_EDEFAULT == null
					? getIsReturnValueSingular() != null
					: !IS_RETURN_VALUE_SINGULAR_EDEFAULT
							.equals(getIsReturnValueSingular());
		case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_TYPE:
			return basicGetSrcObjectType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_SIMPLE_TYPE:
			return getSrcObjectSimpleType() != SRC_OBJECT_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_AS_CODE:
			return BASE_AS_CODE_EDEFAULT == null ? getBaseAsCode() != null
					: !BASE_AS_CODE_EDEFAULT.equals(getBaseAsCode());
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE:
			return isSetBase();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_DEFINITION:
			return basicGetBaseDefinition() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR:
			return isSetBaseVar();
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_TYPE:
			return basicGetBaseExitType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SIMPLE_TYPE:
			return getBaseExitSimpleType() != BASE_EXIT_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_MANDATORY:
			return BASE_EXIT_MANDATORY_EDEFAULT == null
					? getBaseExitMandatory() != null
					: !BASE_EXIT_MANDATORY_EDEFAULT
							.equals(getBaseExitMandatory());
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SINGULAR:
			return BASE_EXIT_SINGULAR_EDEFAULT == null
					? getBaseExitSingular() != null
					: !BASE_EXIT_SINGULAR_EDEFAULT
							.equals(getBaseExitSingular());
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_ENTRY_TYPE:
			return basicGetChainEntryType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_AS_CODE:
			return CHAIN_AS_CODE_EDEFAULT == null ? getChainAsCode() != null
					: !CHAIN_AS_CODE_EDEFAULT.equals(getChainAsCode());
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1:
			return isSetElement1();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1_CORRECT:
			return ELEMENT1_CORRECT_EDEFAULT == null
					? getElement1Correct() != null
					: !ELEMENT1_CORRECT_EDEFAULT.equals(getElement1Correct());
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_ENTRY_TYPE:
			return basicGetElement2EntryType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2:
			return isSetElement2();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_CORRECT:
			return ELEMENT2_CORRECT_EDEFAULT == null
					? getElement2Correct() != null
					: !ELEMENT2_CORRECT_EDEFAULT.equals(getElement2Correct());
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_ENTRY_TYPE:
			return basicGetElement3EntryType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3:
			return isSetElement3();
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_CORRECT:
			return ELEMENT3_CORRECT_EDEFAULT == null
					? getElement3Correct() != null
					: !ELEMENT3_CORRECT_EDEFAULT.equals(getElement3Correct());
		case AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE:
			return isSetCastType();
		case AnnotationsPackage.MEXPR_ANNOTATION__LAST_ELEMENT:
			return basicGetLastElement() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_TYPE:
			return basicGetChainCalculatedType() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SIMPLE_TYPE:
			return getChainCalculatedSimpleType() != CHAIN_CALCULATED_SIMPLE_TYPE_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SINGULAR:
			return CHAIN_CALCULATED_SINGULAR_EDEFAULT == null
					? getChainCalculatedSingular() != null
					: !CHAIN_CALCULATED_SINGULAR_EDEFAULT
							.equals(getChainCalculatedSingular());
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR:
			return isSetProcessor();
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR_DEFINITION:
			return basicGetProcessorDefinition() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__TYPE_MISMATCH:
			return TYPE_MISMATCH_EDEFAULT == null ? getTypeMismatch() != null
					: !TYPE_MISMATCH_EDEFAULT.equals(getTypeMismatch());
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
			return isSetCallArgument();
		case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
			return isSetSubExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
			return isSetContainedCollector();
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS:
			return CHAIN_CODEFOR_SUBCHAINS_EDEFAULT == null
					? getChainCodeforSubchains() != null
					: !CHAIN_CODEFOR_SUBCHAINS_EDEFAULT
							.equals(getChainCodeforSubchains());
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_OWN_XOCL_OP:
			return IS_OWN_XOCL_OP_EDEFAULT == null ? getIsOwnXOCLOp() != null
					: !IS_OWN_XOCL_OP_EDEFAULT.equals(getIsOwnXOCLOp());
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION:
			return isSetNamedExpression();
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE:
			return isSetNamedTuple();
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT:
			return isSetNamedConstant();
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE_OF_ANNOTATION:
			return basicGetExpectedReturnTypeOfAnnotation() != null;
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION:
			return getExpectedReturnSimpleTypeOfAnnotation() != EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION_EDEFAULT;
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY:
			return IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY_EDEFAULT == null
					? getIsReturnValueOfAnnotationMandatory() != null
					: !IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY_EDEFAULT
							.equals(getIsReturnValueOfAnnotationMandatory());
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR:
			return IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR_EDEFAULT == null
					? getIsReturnValueOfAnnotationSingular() != null
					: !IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR_EDEFAULT
							.equals(getIsReturnValueOfAnnotationSingular());
		case AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL:
			return isSetUseExplicitOcl();
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CHANGED:
			return OCL_CHANGED_EDEFAULT == null ? getOclChanged() != null
					: !OCL_CHANGED_EDEFAULT.equals(getOclChanged());
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CODE:
			return OCL_CODE_EDEFAULT == null ? getOclCode() != null
					: !OCL_CODE_EDEFAULT.equals(getOclCode());
		case AnnotationsPackage.MEXPR_ANNOTATION__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_AS_STRING:
				return McorePackage.MTYPED__MULTIPLICITY_AS_STRING;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE:
				return McorePackage.MTYPED__CALCULATED_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_MANDATORY:
				return McorePackage.MTYPED__CALCULATED_MANDATORY;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SINGULAR:
				return McorePackage.MTYPED__CALCULATED_SINGULAR;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE_PACKAGE:
				return McorePackage.MTYPED__CALCULATED_TYPE_PACKAGE;
			case AnnotationsPackage.MEXPR_ANNOTATION__VOID_TYPE_ALLOWED:
				return McorePackage.MTYPED__VOID_TYPE_ALLOWED;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SIMPLE_TYPE:
				return McorePackage.MTYPED__CALCULATED_SIMPLE_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE:
				return McorePackage.MTYPED__MULTIPLICITY_CASE;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractExpression.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.MEXPR_ANNOTATION__AS_CODE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE;
			case AnnotationsPackage.MEXPR_ANNOTATION__AS_BASIC_CODE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE;
			case AnnotationsPackage.MEXPR_ANNOTATION__COLLECTOR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__COLLECTOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__ENTIRE_SCOPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ENTIRE_SCOPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_BASE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_BASE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SELF:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SELF;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_TRG:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_TRG;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJ:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJ;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_LITERAL_CONSTANTS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_VARIABLES:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_VARIABLES;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ITERATOR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ITERATOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ACCUMULATOR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_PARAMETERS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_CONTAINER_BASE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_NUMBER_BASE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SOURCE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SOURCE;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_ENTIRE_SCOPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_BASE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SELF:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_TRG:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJ:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SOURCE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_VARIABLES:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ITERATOR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_PARAMETERS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;
			case AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_CONTAINER:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER;
			case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_ANNOTATION:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION;
			case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_EXPRESSION:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION;
			case AnnotationsPackage.MEXPR_ANNOTATION__IS_COMPLEX_EXPRESSION:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;
			case AnnotationsPackage.MEXPR_ANNOTATION__IS_SUB_EXPRESSION:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_MANDATORY:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SINGULAR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_PACKAGE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE;
			case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_OBJECT_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__OBJECT_OBJECT_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_MANDATORY:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY;
			case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_SINGULAR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR;
			case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractExpressionWithBase.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_AS_CODE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_DEFINITION:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_MANDATORY:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY;
			case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SINGULAR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractChain.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_AS_CODE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_AS_CODE;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1_CORRECT;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_CORRECT;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3;
			case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_CORRECT;
			case AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CAST_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__LAST_ELEMENT:
				return ExpressionsPackage.MABSTRACT_CHAIN__LAST_ELEMENT;
			case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE;
			case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SINGULAR:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;
			case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR:
				return ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR_DEFINITION:
				return ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;
			default:
				return -1;
			}
		}
		if (baseClass == MBaseChain.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.MEXPR_ANNOTATION__TYPE_MISMATCH:
				return ExpressionsPackage.MBASE_CHAIN__TYPE_MISMATCH;
			case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
				return ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT;
			case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
				return ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION;
			case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
				return ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR;
			case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS:
				return ExpressionsPackage.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS;
			case AnnotationsPackage.MEXPR_ANNOTATION__IS_OWN_XOCL_OP:
				return ExpressionsPackage.MBASE_CHAIN__IS_OWN_XOCL_OP;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (baseFeatureID) {
			case McorePackage.MTYPED__MULTIPLICITY_AS_STRING:
				return AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_AS_STRING;
			case McorePackage.MTYPED__CALCULATED_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE;
			case McorePackage.MTYPED__CALCULATED_MANDATORY:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_MANDATORY;
			case McorePackage.MTYPED__CALCULATED_SINGULAR:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SINGULAR;
			case McorePackage.MTYPED__CALCULATED_TYPE_PACKAGE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_TYPE_PACKAGE;
			case McorePackage.MTYPED__VOID_TYPE_ALLOWED:
				return AnnotationsPackage.MEXPR_ANNOTATION__VOID_TYPE_ALLOWED;
			case McorePackage.MTYPED__CALCULATED_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SIMPLE_TYPE;
			case McorePackage.MTYPED__MULTIPLICITY_CASE:
				return AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractExpression.class) {
			switch (baseFeatureID) {
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
				return AnnotationsPackage.MEXPR_ANNOTATION__AS_CODE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
				return AnnotationsPackage.MEXPR_ANNOTATION__AS_BASIC_CODE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__COLLECTOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__COLLECTOR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ENTIRE_SCOPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__ENTIRE_SCOPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_BASE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_BASE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SELF:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SELF;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_TRG:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_TRG;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJ:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJ;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_LITERAL_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_VARIABLES:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_VARIABLES;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ITERATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ITERATOR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_ACCUMULATOR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_PARAMETERS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_CONTAINER_BASE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_NUMBER_BASE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SOURCE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SCOPE_SOURCE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_ENTIRE_SCOPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_BASE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SELF;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_TRG;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJ;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SOURCE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_VARIABLES;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ITERATOR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_PARAMETERS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER:
				return AnnotationsPackage.MEXPR_ANNOTATION__LOCAL_SCOPE_CONTAINER;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION:
				return AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_ANNOTATION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION:
				return AnnotationsPackage.MEXPR_ANNOTATION__CONTAINING_EXPRESSION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
				return AnnotationsPackage.MEXPR_ANNOTATION__IS_COMPLEX_EXPRESSION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
				return AnnotationsPackage.MEXPR_ANNOTATION__IS_SUB_EXPRESSION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_MANDATORY;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SINGULAR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SELF_OBJECT_PACKAGE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__TARGET_OBJECT_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__TARGET_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__OBJECT_OBJECT_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
				return AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_MANDATORY;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
				return AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_SINGULAR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_SIMPLE_TYPE;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractExpressionWithBase.class) {
			switch (baseFeatureID) {
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_AS_CODE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_DEFINITION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_VAR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_MANDATORY;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR:
				return AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SINGULAR;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractChain.class) {
			switch (baseFeatureID) {
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_AS_CODE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_AS_CODE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1_CORRECT:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_CORRECT:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_CORRECT:
				return AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__CAST_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CAST_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__LAST_ELEMENT:
				return AnnotationsPackage.MEXPR_ANNOTATION__LAST_ELEMENT;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE:
				return AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR:
				return AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SINGULAR;
			case ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR;
			case ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR_DEFINITION:
				return AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR_DEFINITION;
			default:
				return -1;
			}
		}
		if (baseClass == MBaseChain.class) {
			switch (baseFeatureID) {
			case ExpressionsPackage.MBASE_CHAIN__TYPE_MISMATCH:
				return AnnotationsPackage.MEXPR_ANNOTATION__TYPE_MISMATCH;
			case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
				return AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT;
			case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
				return AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION;
			case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
				return AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR;
			case ExpressionsPackage.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS:
				return AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS;
			case ExpressionsPackage.MBASE_CHAIN__IS_OWN_XOCL_OP:
				return AnnotationsPackage.MEXPR_ANNOTATION__IS_OWN_XOCL_OP;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (baseOperationID) {
			case McorePackage.MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE:
				return AnnotationsPackage.MEXPR_ANNOTATION___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;
			case McorePackage.MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN:
				return AnnotationsPackage.MEXPR_ANNOTATION___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractExpression.class) {
			switch (baseOperationID) {
			case ExpressionsPackage.MABSTRACT_EXPRESSION___GET_SHORT_CODE:
				return AnnotationsPackage.MEXPR_ANNOTATION___GET_SHORT_CODE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION___GET_SCOPE:
				return AnnotationsPackage.MEXPR_ANNOTATION___GET_SCOPE;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractExpressionWithBase.class) {
			switch (baseOperationID) {
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION:
				return AnnotationsPackage.MEXPR_ANNOTATION___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_BUILT_IN:
				return AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_BUILT_IN;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_CONSTANTS:
				return AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_CONSTANTS;
			case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_VARIABLES:
				return AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_VARIABLES;
			default:
				return -1;
			}
		}
		if (baseClass == MAbstractChain.class) {
			switch (baseOperationID) {
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION;
			case ExpressionsPackage.MABSTRACT_CHAIN___LENGTH:
				return AnnotationsPackage.MEXPR_ANNOTATION___LENGTH;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER:
				return AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_ELEMENT_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER:
				return AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER:
				return AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER:
				return AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___AS_CODE_FOR_OTHERS:
				return AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_OTHERS;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH1:
				return AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH1;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH2:
				return AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH2;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH3:
				return AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH3;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_AS_CODE:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_AS_CODE;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_CUSTOM_CODE_PROCESSOR:
				return AnnotationsPackage.MEXPR_ANNOTATION___IS_CUSTOM_CODE_PROCESSOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_PROCESSOR_SET_OPERATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION___IS_PROCESSOR_SET_OPERATOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_OWN_XOCL_OPERATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION___IS_OWN_XOCL_OPERATOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_RETURNS_SINGULAR:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROCESSOR_RETURNS_SINGULAR;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_IS_SET:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROCESSOR_IS_SET;
			case ExpressionsPackage.MABSTRACT_CHAIN___CREATE_PROCESSOR_DEFINITION:
				return AnnotationsPackage.MEXPR_ANNOTATION___CREATE_PROCESSOR_DEFINITION;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECT;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECTS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEAN;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEANS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGERS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REAL:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_REAL;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REALS:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_REALS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRING:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_STRING;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_STRINGS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATE:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_DATE;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATES:
				return AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_DATES;
			default:
				return -1;
			}
		}
		if (baseClass == MBaseChain.class) {
			switch (baseOperationID) {
			case ExpressionsPackage.MBASE_CHAIN___AUTO_CAST_WITH_PROC:
				return AnnotationsPackage.MEXPR_ANNOTATION___AUTO_CAST_WITH_PROC;
			case ExpressionsPackage.MBASE_CHAIN___OWN_TO_APPLY_MISMATCH:
				return AnnotationsPackage.MEXPR_ANNOTATION___OWN_TO_APPLY_MISMATCH;
			case ExpressionsPackage.MBASE_CHAIN___UNIQUE_CHAIN_NUMBER:
				return AnnotationsPackage.MEXPR_ANNOTATION___UNIQUE_CHAIN_NUMBER;
			case ExpressionsPackage.MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN:
				return AnnotationsPackage.MEXPR_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN;
			case ExpressionsPackage.MBASE_CHAIN___RESET_TO_BASE__EXPRESSIONBASE_MVARIABLE:
				return AnnotationsPackage.MEXPR_ANNOTATION___RESET_TO_BASE__EXPRESSIONBASE_MVARIABLE;
			case ExpressionsPackage.MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN:
				return AnnotationsPackage.MEXPR_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN;
			case ExpressionsPackage.MBASE_CHAIN___IS_PROCESSOR_CHECK_EQUAL_OPERATOR:
				return AnnotationsPackage.MEXPR_ANNOTATION___IS_PROCESSOR_CHECK_EQUAL_OPERATOR;
			case ExpressionsPackage.MBASE_CHAIN___IS_PREFIX_PROCESSOR:
				return AnnotationsPackage.MEXPR_ANNOTATION___IS_PREFIX_PROCESSOR;
			case ExpressionsPackage.MBASE_CHAIN___IS_POSTFIX_PROCESSOR:
				return AnnotationsPackage.MEXPR_ANNOTATION___IS_POSTFIX_PROCESSOR;
			case ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_OTHERS:
				return AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_OTHERS;
			case ExpressionsPackage.MBASE_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER:
				return AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_ELEMENT_AS_CODE__INTEGER;
			case ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH1:
				return AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH1;
			case ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH2:
				return AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH2;
			case ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH3:
				return AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH3;
			case ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_VARIABLES:
				return AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_VARIABLES;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case AnnotationsPackage.MEXPR_ANNOTATION___OCL_CHANGED$_UPDATE__BOOLEAN:
			return oclChanged$Update((Boolean) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___DO_ACTION$_UPDATE__MEXPRANNOTATIONACTION:
			return doAction$Update((MExprAnnotationAction) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___DO_ACTION_UPDATE__MEXPRANNOTATIONACTION:
			return doActionUpdate((MExprAnnotationAction) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION:
			return variableFromExpression((MNamedExpression) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___AUTO_CAST_WITH_PROC:
			return autoCastWithProc();
		case AnnotationsPackage.MEXPR_ANNOTATION___OWN_TO_APPLY_MISMATCH:
			return ownToApplyMismatch();
		case AnnotationsPackage.MEXPR_ANNOTATION___UNIQUE_CHAIN_NUMBER:
			return uniqueChainNumber();
		case AnnotationsPackage.MEXPR_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN:
			return reuseFromOtherNoMoreUsedChain((MBaseChain) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___RESET_TO_BASE__EXPRESSIONBASE_MVARIABLE:
			return resetToBase((ExpressionBase) arguments.get(0),
					(MVariable) arguments.get(1));
		case AnnotationsPackage.MEXPR_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN:
			return reuseFromOtherNoMoreUsedChainAsUpdate(
					(MBaseChain) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___IS_PROCESSOR_CHECK_EQUAL_OPERATOR:
			return isProcessorCheckEqualOperator();
		case AnnotationsPackage.MEXPR_ANNOTATION___IS_PREFIX_PROCESSOR:
			return isPrefixProcessor();
		case AnnotationsPackage.MEXPR_ANNOTATION___IS_POSTFIX_PROCESSOR:
			return isPostfixProcessor();
		case AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_OTHERS:
			return asCodeForOthers();
		case AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_ELEMENT_AS_CODE__INTEGER:
			return unsafeElementAsCode((Integer) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH1:
			return codeForLength1();
		case AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH2:
			return codeForLength2();
		case AnnotationsPackage.MEXPR_ANNOTATION___CODE_FOR_LENGTH3:
			return codeForLength3();
		case AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_VARIABLES:
			return asCodeForVariables();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION:
			return processorDefinition$Update(
					(MProcessorDefinition) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___LENGTH:
			return length();
		case AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER:
			return unsafeChainStepAsCode((Integer) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER:
			return unsafeChainAsCode((Integer) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER:
			return unsafeChainAsCode((Integer) arguments.get(0),
					(Integer) arguments.get(1));
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_AS_CODE:
			return procAsCode();
		case AnnotationsPackage.MEXPR_ANNOTATION___IS_CUSTOM_CODE_PROCESSOR:
			return isCustomCodeProcessor();
		case AnnotationsPackage.MEXPR_ANNOTATION___IS_PROCESSOR_SET_OPERATOR:
			return isProcessorSetOperator();
		case AnnotationsPackage.MEXPR_ANNOTATION___IS_OWN_XOCL_OPERATOR:
			return isOwnXOCLOperator();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROCESSOR_RETURNS_SINGULAR:
			return processorReturnsSingular();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROCESSOR_IS_SET:
			return processorIsSet();
		case AnnotationsPackage.MEXPR_ANNOTATION___CREATE_PROCESSOR_DEFINITION:
			return createProcessorDefinition();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECT:
			return procDefChoicesForObject();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECTS:
			return procDefChoicesForObjects();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEAN:
			return procDefChoicesForBoolean();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEANS:
			return procDefChoicesForBooleans();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGER:
			return procDefChoicesForInteger();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGERS:
			return procDefChoicesForIntegers();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_REAL:
			return procDefChoicesForReal();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_REALS:
			return procDefChoicesForReals();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_STRING:
			return procDefChoicesForString();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_STRINGS:
			return procDefChoicesForStrings();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_DATE:
			return procDefChoicesForDate();
		case AnnotationsPackage.MEXPR_ANNOTATION___PROC_DEF_CHOICES_FOR_DATES:
			return procDefChoicesForDates();
		case AnnotationsPackage.MEXPR_ANNOTATION___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION:
			return baseDefinition$Update(
					(MAbstractBaseDefinition) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_BUILT_IN:
			return asCodeForBuiltIn();
		case AnnotationsPackage.MEXPR_ANNOTATION___AS_CODE_FOR_CONSTANTS:
			return asCodeForConstants();
		case AnnotationsPackage.MEXPR_ANNOTATION___GET_SHORT_CODE:
			return getShortCode();
		case AnnotationsPackage.MEXPR_ANNOTATION___GET_SCOPE:
			return getScope();
		case AnnotationsPackage.MEXPR_ANNOTATION___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE:
			return multiplicityCase$Update((MultiplicityCase) arguments.get(0));
		case AnnotationsPackage.MEXPR_ANNOTATION___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN:
			return typeAsOcl((MPackage) arguments.get(0),
					(MClassifier) arguments.get(1),
					(SimpleType) arguments.get(2), (Boolean) arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (base: ");
		if (baseESet)
			result.append(base);
		else
			result.append("<unset>");
		result.append(", processor: ");
		if (processorESet)
			result.append(processor);
		else
			result.append("<unset>");
		result.append(", useExplicitOcl: ");
		if (useExplicitOclESet)
			result.append(useExplicitOcl);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel let kind: String = 'CHAIN' in
	kind
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MEXPR_ANNOTATION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR,
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__USE_EXPLICIT_OCL,
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MEXPR_ANNOTATION,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, oclText,
						helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MExprAnnotationImpl
