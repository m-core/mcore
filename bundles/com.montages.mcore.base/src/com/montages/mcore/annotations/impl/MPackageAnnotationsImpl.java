/**
 */
package com.montages.mcore.annotations.impl;

import org.eclipse.emf.ecore.EClass;

import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MPackageAnnotations;
import com.montages.mcore.impl.MRepositoryElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MPackage Annotations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public class MPackageAnnotationsImpl extends MRepositoryElementImpl
		implements MPackageAnnotations {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPackageAnnotationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MPACKAGE_ANNOTATIONS;
	}

} //MPackageAnnotationsImpl
