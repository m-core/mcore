/**
 */
package com.montages.mcore.annotations.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MProperty;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.impl.MRepositoryElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Property Annotations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractPropertyAnnotationsImpl#getContainingPropertiesContainer <em>Containing Properties Container</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractPropertyAnnotationsImpl#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractPropertyAnnotationsImpl#getAnnotatedProperty <em>Annotated Property</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAbstractPropertyAnnotationsImpl
		extends MRepositoryElementImpl implements MAbstractPropertyAnnotations {
	/**
	 * The parsed OCL expression for the derivation of '{@link #getAnnotatedProperty <em>Annotated Property</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatedProperty
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression annotatedPropertyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifier <em>Containing Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingPropertiesContainer <em>Containing Properties Container</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingPropertiesContainer
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPropertiesContainerDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractPropertyAnnotationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getAnnotatedProperty() {
		MProperty annotatedProperty = basicGetAnnotatedProperty();
		return annotatedProperty != null && annotatedProperty.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) annotatedProperty)
				: annotatedProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetAnnotatedProperty() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY;

		if (annotatedPropertyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				annotatedPropertyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(annotatedPropertyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getContainingClassifier() {
		MClassifier containingClassifier = basicGetContainingClassifier();
		return containingClassifier != null && containingClassifier.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) containingClassifier)
				: containingClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetContainingClassifier() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() 
		then null
		else let pc:MPropertiesContainer
		= self.eContainer().oclAsType(annotations::MClassifierAnnotations).eContainer().oclAsType(MPropertiesContainer) in
		pc.containingClassifier  endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_CLASSIFIER;

		if (containingClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesContainer getContainingPropertiesContainer() {
		MPropertiesContainer containingPropertiesContainer = basicGetContainingPropertiesContainer();
		return containingPropertiesContainer != null
				&& containingPropertiesContainer.eIsProxy()
						? (MPropertiesContainer) eResolveProxy(
								(InternalEObject) containingPropertiesContainer)
						: containingPropertiesContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesContainer basicGetContainingPropertiesContainer() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() 
		then null
		else let pc:MPropertiesContainer
		= self.eContainer().oclAsType(annotations::MClassifierAnnotations).eContainer().oclAsType(MPropertiesContainer) in
		pc  endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_PROPERTIES_CONTAINER;

		if (containingPropertiesContainerDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPropertiesContainerDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingPropertiesContainerDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertiesContainer result = (MPropertiesContainer) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_PROPERTIES_CONTAINER:
			if (resolve)
				return getContainingPropertiesContainer();
			return basicGetContainingPropertiesContainer();
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_CLASSIFIER:
			if (resolve)
				return getContainingClassifier();
			return basicGetContainingClassifier();
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY:
			if (resolve)
				return getAnnotatedProperty();
			return basicGetAnnotatedProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_PROPERTIES_CONTAINER:
			return basicGetContainingPropertiesContainer() != null;
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_CLASSIFIER:
			return basicGetContainingClassifier() != null;
		case AnnotationsPackage.MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY:
			return basicGetAnnotatedProperty() != null;
		}
		return super.eIsSet(featureID);
	}

} //MAbstractPropertyAnnotationsImpl
