/**
 */
package com.montages.mcore.annotations.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.langlets.acore.AcorePackage;
import org.xocl.semantics.SemanticsPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.DefaultCommandMode;
import com.montages.mcore.annotations.GeneralAnnotationAction;
import com.montages.mcore.annotations.MAbstractAnnotion;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.annotations.MAdd;
import com.montages.mcore.annotations.MAddCommand;
import com.montages.mcore.annotations.MAnnotation;
import com.montages.mcore.annotations.MBooleanAnnotation;
import com.montages.mcore.annotations.MChoiceConstraint;
import com.montages.mcore.annotations.MChoiceConstruction;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MExprAnnotationAction;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MGeneralDetail;
import com.montages.mcore.annotations.MHighlightAnnotation;
import com.montages.mcore.annotations.MInitializationOrder;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MLabel;
import com.montages.mcore.annotations.MLayoutAnnotation;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPackageAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotationsAction;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.annotations.MStringAsOclAnnotation;
import com.montages.mcore.annotations.MStringAsOclVariable;
import com.montages.mcore.annotations.MTrgAnnotation;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.MUpdateAlternativePersistencePackage;
import com.montages.mcore.annotations.MUpdateAlternativePersistenceReference;
import com.montages.mcore.annotations.MUpdateAlternativePersistenceRoot;
import com.montages.mcore.annotations.MUpdateCondition;
import com.montages.mcore.annotations.MUpdateObject;
import com.montages.mcore.annotations.MUpdatePersistence;
import com.montages.mcore.annotations.MUpdatePersistenceLocation;
import com.montages.mcore.annotations.MUpdateValue;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.ExpressionsPackageImpl;
import com.montages.mcore.impl.McorePackageImpl;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.ObjectsPackageImpl;
import com.montages.mcore.tables.TablesPackage;
import com.montages.mcore.tables.impl.TablesPackageImpl;
import com.montages.mcore.updates.UpdatesPackage;
import com.montages.mcore.updates.impl.UpdatesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationsPackageImpl extends EPackageImpl
		implements AnnotationsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPackageAnnotationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mClassifierAnnotationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractPropertyAnnotationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPropertyAnnotationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mOperationAnnotationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractAnnotionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mGeneralAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mGeneralDetailEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mStringAsOclAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mStringAsOclVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLayoutAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mExprAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLabelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTrgAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mBooleanAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mInvariantConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAddEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAddCommandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdatePersistenceLocationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mInitializationOrderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mInitializationValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mHighlightAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mChoiceConstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mChoiceConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdatePersistenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateAlternativePersistencePackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateAlternativePersistenceRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mUpdateAlternativePersistenceReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mPropertyAnnotationsActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mExprAnnotationActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum defaultCommandModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum generalAnnotationActionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mcore.annotations.AnnotationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AnnotationsPackageImpl() {
		super(eNS_URI, AnnotationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AnnotationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AnnotationsPackage init() {
		if (isInited)
			return (AnnotationsPackage) EPackage.Registry.INSTANCE
					.getEPackage(AnnotationsPackage.eNS_URI);

		// Obtain or create and register package
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof AnnotationsPackageImpl
						? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new AnnotationsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();
		SemanticsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		McorePackageImpl theMcorePackage = (McorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI) instanceof McorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(
								McorePackage.eNS_URI)
						: McorePackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ExpressionsPackage.eNS_URI)
								: ExpressionsPackage.eINSTANCE);
		ObjectsPackageImpl theObjectsPackage = (ObjectsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ObjectsPackage.eNS_URI) instanceof ObjectsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ObjectsPackage.eNS_URI)
								: ObjectsPackage.eINSTANCE);
		TablesPackageImpl theTablesPackage = (TablesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI) instanceof TablesPackageImpl
						? EPackage.Registry.INSTANCE
								.getEPackage(TablesPackage.eNS_URI)
						: TablesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(UpdatesPackage.eNS_URI)
								: UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theAnnotationsPackage.createPackageContents();
		theMcorePackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theObjectsPackage.createPackageContents();
		theTablesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theAnnotationsPackage.initializePackageContents();
		theMcorePackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theObjectsPackage.initializePackageContents();
		theTablesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAnnotationsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AnnotationsPackage.eNS_URI,
				theAnnotationsPackage);
		return theAnnotationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMPackageAnnotations() {
		return mPackageAnnotationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMClassifierAnnotations() {
		return mClassifierAnnotationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifierAnnotations_Classifier() {
		return (EReference) mClassifierAnnotationsEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifierAnnotations_Label() {
		return (EReference) mClassifierAnnotationsEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifierAnnotations_Constraint() {
		return (EReference) mClassifierAnnotationsEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifierAnnotations_PropertyAnnotations() {
		return (EReference) mClassifierAnnotationsEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifierAnnotations_OperationAnnotations() {
		return (EReference) mClassifierAnnotationsEClass
				.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractPropertyAnnotations() {
		return mAbstractPropertyAnnotationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractPropertyAnnotations_AnnotatedProperty() {
		return (EReference) mAbstractPropertyAnnotationsEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractPropertyAnnotations_ContainingClassifier() {
		return (EReference) mAbstractPropertyAnnotationsEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractPropertyAnnotations_ContainingPropertiesContainer() {
		return (EReference) mAbstractPropertyAnnotationsEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMPropertyAnnotations() {
		return mPropertyAnnotationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Property() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyAnnotations_Overriding() {
		return (EAttribute) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Overrides() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_OverriddenIn() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Result() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_InitOrder() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_InitValue() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_ChoiceConstruction() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_ChoiceConstraint() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Add() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Update() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_StringAsOcl() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Highlight() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyAnnotations_Layout() {
		return (EReference) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyAnnotations_DoAction() {
		return (EAttribute) mPropertyAnnotationsEClass.getEStructuralFeatures()
				.get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyAnnotations__DoAction$Update__MPropertyAnnotationsAction() {
		return mPropertyAnnotationsEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMOperationAnnotations() {
		return mOperationAnnotationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationAnnotations_OperationSignature() {
		return (EReference) mOperationAnnotationsEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationAnnotations_Result() {
		return (EReference) mOperationAnnotationsEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMOperationAnnotations_Overriding() {
		return (EAttribute) mOperationAnnotationsEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationAnnotations_Overrides() {
		return (EReference) mOperationAnnotationsEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationAnnotations_OverriddenIn() {
		return (EReference) mOperationAnnotationsEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractAnnotion() {
		return mAbstractAnnotionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractAnnotion_GeneralReference() {
		return (EReference) mAbstractAnnotionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractAnnotion_GeneralContent() {
		return (EReference) mAbstractAnnotionEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractAnnotion_AnnotatedElement() {
		return (EReference) mAbstractAnnotionEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractAnnotion_ContainingAbstractPropertyAnnotations() {
		return (EReference) mAbstractAnnotionEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMGeneralAnnotation() {
		return mGeneralAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMGeneralAnnotation_Source() {
		return (EAttribute) mGeneralAnnotationEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMGeneralAnnotation_MGeneralDetail() {
		return (EReference) mGeneralAnnotationEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMGeneralAnnotation_DoAction() {
		return (EAttribute) mGeneralAnnotationEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMGeneralAnnotation__DoAction$Update__GeneralAnnotationAction() {
		return mGeneralAnnotationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMGeneralDetail() {
		return mGeneralDetailEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMGeneralDetail_Key() {
		return (EAttribute) mGeneralDetailEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMGeneralDetail_Value() {
		return (EAttribute) mGeneralDetailEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMStringAsOclAnnotation() {
		return mStringAsOclAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMStringAsOclAnnotation_SelfObjectType() {
		return (EReference) mStringAsOclAnnotationEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMStringAsOclAnnotation_SelfObjectMandatory() {
		return (EAttribute) mStringAsOclAnnotationEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMStringAsOclAnnotation_SelfObjectSingular() {
		return (EAttribute) mStringAsOclAnnotationEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMStringAsOclAnnotation_MStringAsOclVariable() {
		return (EReference) mStringAsOclAnnotationEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMStringAsOclVariable() {
		return mStringAsOclVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMStringAsOclVariable_Id() {
		return (EAttribute) mStringAsOclVariableEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLayoutAnnotation() {
		return mLayoutAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMLayoutAnnotation_HideInTable() {
		return (EAttribute) mLayoutAnnotationEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMLayoutAnnotation_HideInPropertyView() {
		return (EAttribute) mLayoutAnnotationEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAnnotation() {
		return mAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAnnotation_DomainObject() {
		return (EReference) mAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAnnotation_DomainOperation() {
		return (EReference) mAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAnnotation_SelfObjectTypeOfAnnotation() {
		return (EReference) mAnnotationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAnnotation_TargetObjectTypeOfAnnotation() {
		return (EReference) mAnnotationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_TargetSimpleTypeOfAnnotation() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAnnotation_ObjectObjectTypeOfAnnotation() {
		return (EReference) mAnnotationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_Value() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL1() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL1Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL2() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL2Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL3() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL3Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL4() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL4Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL5() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL5Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL6() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL6Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL7() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAnnotation_HistorizedOCL7Timestamp() {
		return (EAttribute) mAnnotationEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMExprAnnotation() {
		return mExprAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMExprAnnotation_NamedConstant() {
		return (EReference) mExprAnnotationEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMExprAnnotation_NamedExpression() {
		return (EReference) mExprAnnotationEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMExprAnnotation_NamedTuple() {
		return (EReference) mExprAnnotationEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMExprAnnotation_ExpectedReturnTypeOfAnnotation() {
		return (EReference) mExprAnnotationEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_ExpectedReturnSimpleTypeOfAnnotation() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_IsReturnValueOfAnnotationMandatory() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_IsReturnValueOfAnnotationSingular() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_OclChanged() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_OclCode() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_DoAction() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMExprAnnotation__OclChanged$Update__Boolean() {
		return mExprAnnotationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMExprAnnotation__DoAction$Update__MExprAnnotationAction() {
		return mExprAnnotationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMExprAnnotation__DoActionUpdate__MExprAnnotationAction() {
		return mExprAnnotationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMExprAnnotation__VariableFromExpression__MNamedExpression() {
		return mExprAnnotationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExprAnnotation_UseExplicitOcl() {
		return (EAttribute) mExprAnnotationEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLabel() {
		return mLabelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLabel_ContainingClassifierAnnotations() {
		return (EReference) mLabelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTrgAnnotation() {
		return mTrgAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMBooleanAnnotation() {
		return mBooleanAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMInvariantConstraint() {
		return mInvariantConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMInvariantConstraint_ContainingClassifierAnnotations() {
		return (EReference) mInvariantConstraintEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMResult() {
		return mResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAdd() {
		return mAddEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAdd_DefaultCommandMode() {
		return (EAttribute) mAddEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAdd_AddCommand() {
		return (EReference) mAddEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAddCommand() {
		return mAddCommandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAddCommand_Type() {
		return (EReference) mAddCommandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAddCommand_Label() {
		return (EAttribute) mAddCommandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdate() {
		return mUpdateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_Object() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_Value() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_Location() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_AlternativePersistenceRoot() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_AlternativePersistenceReference() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_MUpdateCondition() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdateCondition() {
		return mUpdateConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdatePersistenceLocation() {
		return mUpdatePersistenceLocationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdatePersistenceLocation_ContainingUpdateAnnotation() {
		return (EReference) mUpdatePersistenceLocationEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMUpdate_Mode() {
		return (EAttribute) mUpdateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_FeaturePackageFilter() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_FeatureClassFilter() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_Feature() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_ContainingPropertyAnnotations() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdate_AlternativePersistencePackage() {
		return (EReference) mUpdateEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdateObject() {
		return mUpdateObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdateObject_ContainingUpdateAnnotation() {
		return (EReference) mUpdateObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdateValue() {
		return mUpdateValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMUpdateValue_ContainingUpdateAnnotation() {
		return (EReference) mUpdateValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMInitializationOrder() {
		return mInitializationOrderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMInitializationValue() {
		return mInitializationValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMHighlightAnnotation() {
		return mHighlightAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMChoiceConstruction() {
		return mChoiceConstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMChoiceConstraint() {
		return mChoiceConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdatePersistence() {
		return mUpdatePersistenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdateAlternativePersistencePackage() {
		return mUpdateAlternativePersistencePackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdateAlternativePersistenceRoot() {
		return mUpdateAlternativePersistenceRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMUpdateAlternativePersistenceReference() {
		return mUpdateAlternativePersistenceReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMPropertyAnnotationsAction() {
		return mPropertyAnnotationsActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMExprAnnotationAction() {
		return mExprAnnotationActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDefaultCommandMode() {
		return defaultCommandModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGeneralAnnotationAction() {
		return generalAnnotationActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsFactory getAnnotationsFactory() {
		return (AnnotationsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mPackageAnnotationsEClass = createEClass(MPACKAGE_ANNOTATIONS);

		mClassifierAnnotationsEClass = createEClass(MCLASSIFIER_ANNOTATIONS);
		createEReference(mClassifierAnnotationsEClass,
				MCLASSIFIER_ANNOTATIONS__CLASSIFIER);
		createEReference(mClassifierAnnotationsEClass,
				MCLASSIFIER_ANNOTATIONS__LABEL);
		createEReference(mClassifierAnnotationsEClass,
				MCLASSIFIER_ANNOTATIONS__CONSTRAINT);
		createEReference(mClassifierAnnotationsEClass,
				MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS);
		createEReference(mClassifierAnnotationsEClass,
				MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS);

		mAbstractPropertyAnnotationsEClass = createEClass(
				MABSTRACT_PROPERTY_ANNOTATIONS);
		createEReference(mAbstractPropertyAnnotationsEClass,
				MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_PROPERTIES_CONTAINER);
		createEReference(mAbstractPropertyAnnotationsEClass,
				MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_CLASSIFIER);
		createEReference(mAbstractPropertyAnnotationsEClass,
				MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY);

		mPropertyAnnotationsEClass = createEClass(MPROPERTY_ANNOTATIONS);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__PROPERTY);
		createEAttribute(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__OVERRIDING);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__OVERRIDES);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__RESULT);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__INIT_ORDER);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__INIT_VALUE);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__ADD);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__UPDATE);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__STRING_AS_OCL);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__HIGHLIGHT);
		createEReference(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__LAYOUT);
		createEAttribute(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS__DO_ACTION);
		createEOperation(mPropertyAnnotationsEClass,
				MPROPERTY_ANNOTATIONS___DO_ACTION$_UPDATE__MPROPERTYANNOTATIONSACTION);

		mOperationAnnotationsEClass = createEClass(MOPERATION_ANNOTATIONS);
		createEReference(mOperationAnnotationsEClass,
				MOPERATION_ANNOTATIONS__RESULT);
		createEReference(mOperationAnnotationsEClass,
				MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE);
		createEAttribute(mOperationAnnotationsEClass,
				MOPERATION_ANNOTATIONS__OVERRIDING);
		createEReference(mOperationAnnotationsEClass,
				MOPERATION_ANNOTATIONS__OVERRIDES);
		createEReference(mOperationAnnotationsEClass,
				MOPERATION_ANNOTATIONS__OVERRIDDEN_IN);

		mAbstractAnnotionEClass = createEClass(MABSTRACT_ANNOTION);
		createEReference(mAbstractAnnotionEClass,
				MABSTRACT_ANNOTION__GENERAL_REFERENCE);
		createEReference(mAbstractAnnotionEClass,
				MABSTRACT_ANNOTION__GENERAL_CONTENT);
		createEReference(mAbstractAnnotionEClass,
				MABSTRACT_ANNOTION__ANNOTATED_ELEMENT);
		createEReference(mAbstractAnnotionEClass,
				MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS);

		mGeneralAnnotationEClass = createEClass(MGENERAL_ANNOTATION);
		createEAttribute(mGeneralAnnotationEClass, MGENERAL_ANNOTATION__SOURCE);
		createEReference(mGeneralAnnotationEClass,
				MGENERAL_ANNOTATION__MGENERAL_DETAIL);
		createEAttribute(mGeneralAnnotationEClass,
				MGENERAL_ANNOTATION__DO_ACTION);
		createEOperation(mGeneralAnnotationEClass,
				MGENERAL_ANNOTATION___DO_ACTION$_UPDATE__GENERALANNOTATIONACTION);

		mGeneralDetailEClass = createEClass(MGENERAL_DETAIL);
		createEAttribute(mGeneralDetailEClass, MGENERAL_DETAIL__KEY);
		createEAttribute(mGeneralDetailEClass, MGENERAL_DETAIL__VALUE);

		mStringAsOclAnnotationEClass = createEClass(MSTRING_AS_OCL_ANNOTATION);
		createEReference(mStringAsOclAnnotationEClass,
				MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE);
		createEAttribute(mStringAsOclAnnotationEClass,
				MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY);
		createEAttribute(mStringAsOclAnnotationEClass,
				MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR);
		createEReference(mStringAsOclAnnotationEClass,
				MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE);

		mStringAsOclVariableEClass = createEClass(MSTRING_AS_OCL_VARIABLE);
		createEAttribute(mStringAsOclVariableEClass,
				MSTRING_AS_OCL_VARIABLE__ID);

		mLayoutAnnotationEClass = createEClass(MLAYOUT_ANNOTATION);
		createEAttribute(mLayoutAnnotationEClass,
				MLAYOUT_ANNOTATION__HIDE_IN_TABLE);
		createEAttribute(mLayoutAnnotationEClass,
				MLAYOUT_ANNOTATION__HIDE_IN_PROPERTY_VIEW);

		mAnnotationEClass = createEClass(MANNOTATION);
		createEReference(mAnnotationEClass, MANNOTATION__DOMAIN_OBJECT);
		createEReference(mAnnotationEClass, MANNOTATION__DOMAIN_OPERATION);
		createEReference(mAnnotationEClass,
				MANNOTATION__SELF_OBJECT_TYPE_OF_ANNOTATION);
		createEReference(mAnnotationEClass,
				MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION);
		createEReference(mAnnotationEClass,
				MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION);
		createEAttribute(mAnnotationEClass, MANNOTATION__VALUE);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL1);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL1_TIMESTAMP);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL2);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL2_TIMESTAMP);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL3);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL3_TIMESTAMP);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL4);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL4_TIMESTAMP);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL5);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL5_TIMESTAMP);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL6);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL6_TIMESTAMP);
		createEAttribute(mAnnotationEClass, MANNOTATION__HISTORIZED_OCL7);
		createEAttribute(mAnnotationEClass,
				MANNOTATION__HISTORIZED_OCL7_TIMESTAMP);

		mExprAnnotationEClass = createEClass(MEXPR_ANNOTATION);
		createEReference(mExprAnnotationEClass,
				MEXPR_ANNOTATION__NAMED_EXPRESSION);
		createEReference(mExprAnnotationEClass, MEXPR_ANNOTATION__NAMED_TUPLE);
		createEReference(mExprAnnotationEClass,
				MEXPR_ANNOTATION__NAMED_CONSTANT);
		createEReference(mExprAnnotationEClass,
				MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE_OF_ANNOTATION);
		createEAttribute(mExprAnnotationEClass,
				MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION);
		createEAttribute(mExprAnnotationEClass,
				MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY);
		createEAttribute(mExprAnnotationEClass,
				MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR);
		createEAttribute(mExprAnnotationEClass,
				MEXPR_ANNOTATION__USE_EXPLICIT_OCL);
		createEAttribute(mExprAnnotationEClass, MEXPR_ANNOTATION__OCL_CHANGED);
		createEAttribute(mExprAnnotationEClass, MEXPR_ANNOTATION__OCL_CODE);
		createEAttribute(mExprAnnotationEClass, MEXPR_ANNOTATION__DO_ACTION);
		createEOperation(mExprAnnotationEClass,
				MEXPR_ANNOTATION___OCL_CHANGED$_UPDATE__BOOLEAN);
		createEOperation(mExprAnnotationEClass,
				MEXPR_ANNOTATION___DO_ACTION$_UPDATE__MEXPRANNOTATIONACTION);
		createEOperation(mExprAnnotationEClass,
				MEXPR_ANNOTATION___DO_ACTION_UPDATE__MEXPRANNOTATIONACTION);
		createEOperation(mExprAnnotationEClass,
				MEXPR_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION);

		mLabelEClass = createEClass(MLABEL);
		createEReference(mLabelEClass,
				MLABEL__CONTAINING_CLASSIFIER_ANNOTATIONS);

		mTrgAnnotationEClass = createEClass(MTRG_ANNOTATION);

		mBooleanAnnotationEClass = createEClass(MBOOLEAN_ANNOTATION);

		mInvariantConstraintEClass = createEClass(MINVARIANT_CONSTRAINT);
		createEReference(mInvariantConstraintEClass,
				MINVARIANT_CONSTRAINT__CONTAINING_CLASSIFIER_ANNOTATIONS);

		mResultEClass = createEClass(MRESULT);

		mAddEClass = createEClass(MADD);
		createEAttribute(mAddEClass, MADD__DEFAULT_COMMAND_MODE);
		createEReference(mAddEClass, MADD__ADD_COMMAND);

		mAddCommandEClass = createEClass(MADD_COMMAND);
		createEReference(mAddCommandEClass, MADD_COMMAND__TYPE);
		createEAttribute(mAddCommandEClass, MADD_COMMAND__LABEL);

		mUpdateEClass = createEClass(MUPDATE);
		createEReference(mUpdateEClass, MUPDATE__OBJECT);
		createEReference(mUpdateEClass, MUPDATE__VALUE);
		createEAttribute(mUpdateEClass, MUPDATE__MODE);
		createEReference(mUpdateEClass, MUPDATE__FEATURE_PACKAGE_FILTER);
		createEReference(mUpdateEClass, MUPDATE__FEATURE_CLASS_FILTER);
		createEReference(mUpdateEClass, MUPDATE__FEATURE);
		createEReference(mUpdateEClass,
				MUPDATE__CONTAINING_PROPERTY_ANNOTATIONS);
		createEReference(mUpdateEClass,
				MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE);
		createEReference(mUpdateEClass, MUPDATE__LOCATION);
		createEReference(mUpdateEClass, MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT);
		createEReference(mUpdateEClass,
				MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE);
		createEReference(mUpdateEClass, MUPDATE__MUPDATE_CONDITION);

		mUpdateConditionEClass = createEClass(MUPDATE_CONDITION);

		mUpdatePersistenceLocationEClass = createEClass(
				MUPDATE_PERSISTENCE_LOCATION);
		createEReference(mUpdatePersistenceLocationEClass,
				MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION);

		mUpdateObjectEClass = createEClass(MUPDATE_OBJECT);
		createEReference(mUpdateObjectEClass,
				MUPDATE_OBJECT__CONTAINING_UPDATE_ANNOTATION);

		mUpdateValueEClass = createEClass(MUPDATE_VALUE);
		createEReference(mUpdateValueEClass,
				MUPDATE_VALUE__CONTAINING_UPDATE_ANNOTATION);

		mInitializationOrderEClass = createEClass(MINITIALIZATION_ORDER);

		mInitializationValueEClass = createEClass(MINITIALIZATION_VALUE);

		mHighlightAnnotationEClass = createEClass(MHIGHLIGHT_ANNOTATION);

		mChoiceConstructionEClass = createEClass(MCHOICE_CONSTRUCTION);

		mChoiceConstraintEClass = createEClass(MCHOICE_CONSTRAINT);

		mUpdatePersistenceEClass = createEClass(MUPDATE_PERSISTENCE);

		mUpdateAlternativePersistencePackageEClass = createEClass(
				MUPDATE_ALTERNATIVE_PERSISTENCE_PACKAGE);

		mUpdateAlternativePersistenceRootEClass = createEClass(
				MUPDATE_ALTERNATIVE_PERSISTENCE_ROOT);

		mUpdateAlternativePersistenceReferenceEClass = createEClass(
				MUPDATE_ALTERNATIVE_PERSISTENCE_REFERENCE);

		// Create enums
		mPropertyAnnotationsActionEEnum = createEEnum(
				MPROPERTY_ANNOTATIONS_ACTION);
		mExprAnnotationActionEEnum = createEEnum(MEXPR_ANNOTATION_ACTION);
		defaultCommandModeEEnum = createEEnum(DEFAULT_COMMAND_MODE);
		generalAnnotationActionEEnum = createEEnum(GENERAL_ANNOTATION_ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		McorePackage theMcorePackage = (McorePackage) EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI);
		SemanticsPackage theSemanticsPackage = (SemanticsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SemanticsPackage.eNS_URI);
		ObjectsPackage theObjectsPackage = (ObjectsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ObjectsPackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);
		UpdatesPackage theUpdatesPackage = (UpdatesPackage) EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mPackageAnnotationsEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mClassifierAnnotationsEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mAbstractPropertyAnnotationsEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mPropertyAnnotationsEClass.getESuperTypes()
				.add(this.getMAbstractPropertyAnnotations());
		mOperationAnnotationsEClass.getESuperTypes()
				.add(this.getMAbstractPropertyAnnotations());
		mAbstractAnnotionEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mGeneralAnnotationEClass.getESuperTypes()
				.add(this.getMAbstractAnnotion());
		mGeneralDetailEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mStringAsOclAnnotationEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mStringAsOclVariableEClass.getESuperTypes()
				.add(theMcorePackage.getMVariable());
		mStringAsOclVariableEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mLayoutAnnotationEClass.getESuperTypes()
				.add(this.getMAbstractAnnotion());
		mAnnotationEClass.getESuperTypes().add(this.getMAbstractAnnotion());
		mExprAnnotationEClass.getESuperTypes().add(this.getMAnnotation());
		mExprAnnotationEClass.getESuperTypes()
				.add(theExpressionsPackage.getMBaseChain());
		mLabelEClass.getESuperTypes().add(this.getMExprAnnotation());
		mTrgAnnotationEClass.getESuperTypes().add(this.getMExprAnnotation());
		mBooleanAnnotationEClass.getESuperTypes()
				.add(this.getMExprAnnotation());
		mInvariantConstraintEClass.getESuperTypes()
				.add(this.getMExprAnnotation());
		mInvariantConstraintEClass.getESuperTypes()
				.add(theMcorePackage.getMNamed());
		mResultEClass.getESuperTypes().add(this.getMExprAnnotation());
		mAddEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mAddCommandEClass.getESuperTypes().add(this.getMExprAnnotation());
		mAddCommandEClass.getESuperTypes().add(theMcorePackage.getMNamed());
		mUpdateEClass.getESuperTypes().add(theMcorePackage.getMNamed());
		mUpdateEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mUpdateConditionEClass.getESuperTypes().add(this.getMUpdateValue());
		mUpdatePersistenceLocationEClass.getESuperTypes()
				.add(this.getMTrgAnnotation());
		mUpdateObjectEClass.getESuperTypes().add(this.getMTrgAnnotation());
		mUpdateValueEClass.getESuperTypes().add(this.getMTrgAnnotation());
		mInitializationOrderEClass.getESuperTypes()
				.add(this.getMExprAnnotation());
		mInitializationValueEClass.getESuperTypes()
				.add(this.getMExprAnnotation());
		mHighlightAnnotationEClass.getESuperTypes()
				.add(this.getMExprAnnotation());
		mChoiceConstructionEClass.getESuperTypes()
				.add(this.getMExprAnnotation());
		mChoiceConstraintEClass.getESuperTypes()
				.add(this.getMBooleanAnnotation());
		mUpdatePersistenceEClass.getESuperTypes().add(this.getMTrgAnnotation());
		mUpdateAlternativePersistencePackageEClass.getESuperTypes()
				.add(this.getMUpdatePersistence());
		mUpdateAlternativePersistenceRootEClass.getESuperTypes()
				.add(this.getMUpdatePersistence());
		mUpdateAlternativePersistenceReferenceEClass.getESuperTypes()
				.add(this.getMUpdatePersistence());

		// Initialize classes, features, and operations; add parameters
		initEClass(mPackageAnnotationsEClass, MPackageAnnotations.class,
				"MPackageAnnotations", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mClassifierAnnotationsEClass, MClassifierAnnotations.class,
				"MClassifierAnnotations", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMClassifierAnnotations_Classifier(),
				theMcorePackage.getMClassifier(), null, "classifier", null, 0,
				1, MClassifierAnnotations.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifierAnnotations_Label(), this.getMLabel(),
				null, "label", null, 0, 1, MClassifierAnnotations.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifierAnnotations_Constraint(),
				this.getMInvariantConstraint(), null, "constraint", null, 0, -1,
				MClassifierAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifierAnnotations_PropertyAnnotations(),
				this.getMPropertyAnnotations(), null, "propertyAnnotations",
				null, 0, -1, MClassifierAnnotations.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifierAnnotations_OperationAnnotations(),
				this.getMOperationAnnotations(), null, "operationAnnotations",
				null, 0, -1, MClassifierAnnotations.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mAbstractPropertyAnnotationsEClass,
				MAbstractPropertyAnnotations.class,
				"MAbstractPropertyAnnotations", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(
				getMAbstractPropertyAnnotations_ContainingPropertiesContainer(),
				theMcorePackage.getMPropertiesContainer(), null,
				"containingPropertiesContainer", null, 0, 1,
				MAbstractPropertyAnnotations.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractPropertyAnnotations_ContainingClassifier(),
				theMcorePackage.getMClassifier(), null, "containingClassifier",
				null, 0, 1, MAbstractPropertyAnnotations.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractPropertyAnnotations_AnnotatedProperty(),
				theMcorePackage.getMProperty(), null, "annotatedProperty", null,
				0, 1, MAbstractPropertyAnnotations.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mPropertyAnnotationsEClass, MPropertyAnnotations.class,
				"MPropertyAnnotations", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMPropertyAnnotations_Property(),
				theMcorePackage.getMProperty(), null, "property", null, 1, 1,
				MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertyAnnotations_Overriding(),
				ecorePackage.getEBooleanObject(), "overriding", null, 0, 1,
				MPropertyAnnotations.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertyAnnotations_Overrides(),
				this.getMPropertyAnnotations(), null, "overrides", null, 0, 1,
				MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_OverriddenIn(),
				this.getMPropertyAnnotations(), null, "overriddenIn", null, 0,
				-1, MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_Result(), this.getMResult(),
				null, "result", null, 0, 1, MPropertyAnnotations.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertyAnnotations_InitOrder(),
				this.getMInitializationOrder(), null, "initOrder", null, 0, 1,
				MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_InitValue(),
				this.getMInitializationValue(), null, "initValue", null, 0, 1,
				MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_ChoiceConstruction(),
				this.getMChoiceConstruction(), null, "choiceConstruction", null,
				0, 1, MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_ChoiceConstraint(),
				this.getMChoiceConstraint(), null, "choiceConstraint", null, 0,
				1, MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_Add(), this.getMAdd(), null,
				"add", null, 0, 1, MPropertyAnnotations.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_Update(), this.getMUpdate(),
				null, "update", null, 0, -1, MPropertyAnnotations.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertyAnnotations_StringAsOcl(),
				this.getMStringAsOclAnnotation(), null, "stringAsOcl", null, 0,
				1, MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_Highlight(),
				this.getMHighlightAnnotation(), null, "highlight", null, 0, 1,
				MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyAnnotations_Layout(),
				this.getMLayoutAnnotation(), null, "layout", null, 0, 1,
				MPropertyAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertyAnnotations_DoAction(),
				this.getMPropertyAnnotationsAction(), "doAction", null, 0, 1,
				MPropertyAnnotations.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		EOperation op = initEOperation(
				getMPropertyAnnotations__DoAction$Update__MPropertyAnnotationsAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertyAnnotationsAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mOperationAnnotationsEClass, MOperationAnnotations.class,
				"MOperationAnnotations", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMOperationAnnotations_Result(), this.getMResult(),
				null, "result", null, 0, 1, MOperationAnnotations.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMOperationAnnotations_OperationSignature(),
				theMcorePackage.getMOperationSignature(), null,
				"operationSignature", null, 1, 1, MOperationAnnotations.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMOperationAnnotations_Overriding(),
				ecorePackage.getEBooleanObject(), "overriding", null, 0, 1,
				MOperationAnnotations.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMOperationAnnotations_Overrides(),
				this.getMOperationAnnotations(), null, "overrides", null, 0, 1,
				MOperationAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMOperationAnnotations_OverriddenIn(),
				this.getMOperationAnnotations(), null, "overriddenIn", null, 0,
				-1, MOperationAnnotations.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mAbstractAnnotionEClass, MAbstractAnnotion.class,
				"MAbstractAnnotion", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAbstractAnnotion_GeneralReference(),
				theObjectsPackage.getMObject(), null, "generalReference", null,
				0, -1, MAbstractAnnotion.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractAnnotion_GeneralContent(),
				theObjectsPackage.getMObject(), null, "generalContent", null, 0,
				-1, MAbstractAnnotion.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractAnnotion_AnnotatedElement(),
				theMcorePackage.getMModelElement(), null, "annotatedElement",
				null, 0, 1, MAbstractAnnotion.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(
				getMAbstractAnnotion_ContainingAbstractPropertyAnnotations(),
				this.getMAbstractPropertyAnnotations(), null,
				"containingAbstractPropertyAnnotations", null, 0, 1,
				MAbstractAnnotion.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mGeneralAnnotationEClass, MGeneralAnnotation.class,
				"MGeneralAnnotation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMGeneralAnnotation_Source(),
				ecorePackage.getEString(), "source", null, 1, 1,
				MGeneralAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMGeneralAnnotation_MGeneralDetail(),
				this.getMGeneralDetail(), null, "mGeneralDetail", null, 0, -1,
				MGeneralAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMGeneralAnnotation_DoAction(),
				this.getGeneralAnnotationAction(), "doAction", null, 0, 1,
				MGeneralAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(
				getMGeneralAnnotation__DoAction$Update__GeneralAnnotationAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getGeneralAnnotationAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mGeneralDetailEClass, MGeneralDetail.class, "MGeneralDetail",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMGeneralDetail_Key(), ecorePackage.getEString(),
				"key", null, 1, 1, MGeneralDetail.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMGeneralDetail_Value(), ecorePackage.getEString(),
				"value", null, 0, 1, MGeneralDetail.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mStringAsOclAnnotationEClass, MStringAsOclAnnotation.class,
				"MStringAsOclAnnotation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMStringAsOclAnnotation_SelfObjectType(),
				theMcorePackage.getMClassifier(), null, "selfObjectType", null,
				1, 1, MStringAsOclAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMStringAsOclAnnotation_SelfObjectMandatory(),
				ecorePackage.getEBooleanObject(), "selfObjectMandatory",
				"false", 0, 1, MStringAsOclAnnotation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMStringAsOclAnnotation_SelfObjectSingular(),
				ecorePackage.getEBooleanObject(), "selfObjectSingular", "false",
				0, 1, MStringAsOclAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMStringAsOclAnnotation_MStringAsOclVariable(),
				this.getMStringAsOclVariable(), null, "mStringAsOclVariable",
				null, 0, -1, MStringAsOclAnnotation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mStringAsOclVariableEClass, MStringAsOclVariable.class,
				"MStringAsOclVariable", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMStringAsOclVariable_Id(), ecorePackage.getEString(),
				"id", null, 1, 1, MStringAsOclVariable.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mLayoutAnnotationEClass, MLayoutAnnotation.class,
				"MLayoutAnnotation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMLayoutAnnotation_HideInTable(),
				ecorePackage.getEBooleanObject(), "hideInTable", "false", 0, 1,
				MLayoutAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMLayoutAnnotation_HideInPropertyView(),
				ecorePackage.getEBooleanObject(), "hideInPropertyView", null, 0,
				1, MLayoutAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mAnnotationEClass, MAnnotation.class, "MAnnotation",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAnnotation_DomainObject(),
				theObjectsPackage.getMObject(), null, "domainObject", null, 0,
				1, MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAnnotation_DomainOperation(),
				theMcorePackage.getMOperationSignature(), null,
				"domainOperation", null, 0, 1, MAnnotation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAnnotation_SelfObjectTypeOfAnnotation(),
				theMcorePackage.getMClassifier(), null,
				"selfObjectTypeOfAnnotation", null, 0, 1, MAnnotation.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAnnotation_TargetObjectTypeOfAnnotation(),
				theMcorePackage.getMClassifier(), null,
				"targetObjectTypeOfAnnotation", null, 0, 1, MAnnotation.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAnnotation_TargetSimpleTypeOfAnnotation(),
				theMcorePackage.getSimpleType(), "targetSimpleTypeOfAnnotation",
				null, 0, 1, MAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAnnotation_ObjectObjectTypeOfAnnotation(),
				theMcorePackage.getMClassifier(), null,
				"objectObjectTypeOfAnnotation", null, 0, 1, MAnnotation.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAnnotation_Value(), ecorePackage.getEString(),
				"value", null, 0, 1, MAnnotation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL1(),
				ecorePackage.getEString(), "historizedOCL1", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL1Timestamp(),
				ecorePackage.getEDate(), "historizedOCL1Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL2(),
				ecorePackage.getEString(), "historizedOCL2", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL2Timestamp(),
				ecorePackage.getEDate(), "historizedOCL2Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL3(),
				ecorePackage.getEString(), "historizedOCL3", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL3Timestamp(),
				ecorePackage.getEDate(), "historizedOCL3Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL4(),
				ecorePackage.getEString(), "historizedOCL4", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL4Timestamp(),
				ecorePackage.getEDate(), "historizedOCL4Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL5(),
				ecorePackage.getEString(), "historizedOCL5", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL5Timestamp(),
				ecorePackage.getEDate(), "historizedOCL5Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL6(),
				ecorePackage.getEString(), "historizedOCL6", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL6Timestamp(),
				ecorePackage.getEDate(), "historizedOCL6Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL7(),
				ecorePackage.getEString(), "historizedOCL7", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAnnotation_HistorizedOCL7Timestamp(),
				ecorePackage.getEDate(), "historizedOCL7Timestamp", null, 0, 1,
				MAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mExprAnnotationEClass, MExprAnnotation.class,
				"MExprAnnotation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMExprAnnotation_NamedExpression(),
				theExpressionsPackage.getMNamedExpression(), null,
				"namedExpression", null, 0, -1, MExprAnnotation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMExprAnnotation_NamedTuple(),
				theExpressionsPackage.getMAbstractNamedTuple(), null,
				"namedTuple", null, 0, -1, MExprAnnotation.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMExprAnnotation_NamedConstant(),
				theExpressionsPackage.getMNamedConstant(), null,
				"namedConstant", null, 0, -1, MExprAnnotation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMExprAnnotation_ExpectedReturnTypeOfAnnotation(),
				theMcorePackage.getMClassifier(), null,
				"expectedReturnTypeOfAnnotation", null, 0, 1,
				MExprAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(
				getMExprAnnotation_ExpectedReturnSimpleTypeOfAnnotation(),
				theMcorePackage.getSimpleType(),
				"expectedReturnSimpleTypeOfAnnotation", null, 0, 1,
				MExprAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExprAnnotation_IsReturnValueOfAnnotationMandatory(),
				ecorePackage.getEBooleanObject(),
				"isReturnValueOfAnnotationMandatory", null, 1, 1,
				MExprAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExprAnnotation_IsReturnValueOfAnnotationSingular(),
				ecorePackage.getEBooleanObject(),
				"isReturnValueOfAnnotationSingular", null, 1, 1,
				MExprAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExprAnnotation_UseExplicitOcl(),
				ecorePackage.getEBooleanObject(), "useExplicitOcl", null, 0, 1,
				MExprAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExprAnnotation_OclChanged(),
				ecorePackage.getEBooleanObject(), "oclChanged", null, 1, 1,
				MExprAnnotation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMExprAnnotation_OclCode(), ecorePackage.getEString(),
				"oclCode", null, 0, 1, MExprAnnotation.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMExprAnnotation_DoAction(),
				this.getMExprAnnotationAction(), "doAction", null, 0, 1,
				MExprAnnotation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMExprAnnotation__OclChanged$Update__Boolean(),
				theSemanticsPackage.getXUpdate(), "oclChanged$Update", 1, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "trg", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMExprAnnotation__DoAction$Update__MExprAnnotationAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMExprAnnotationAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMExprAnnotation__DoActionUpdate__MExprAnnotationAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMExprAnnotationAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMExprAnnotation__VariableFromExpression__MNamedExpression(),
				theExpressionsPackage.getMVariableBaseDefinition(),
				"variableFromExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theExpressionsPackage.getMNamedExpression(),
				"namedExpression", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mLabelEClass, MLabel.class, "MLabel", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMLabel_ContainingClassifierAnnotations(),
				this.getMClassifierAnnotations(), null,
				"containingClassifierAnnotations", null, 0, 1, MLabel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mTrgAnnotationEClass, MTrgAnnotation.class, "MTrgAnnotation",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mBooleanAnnotationEClass, MBooleanAnnotation.class,
				"MBooleanAnnotation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mInvariantConstraintEClass, MInvariantConstraint.class,
				"MInvariantConstraint", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(
				getMInvariantConstraint_ContainingClassifierAnnotations(),
				this.getMClassifierAnnotations(), null,
				"containingClassifierAnnotations", null, 0, 1,
				MInvariantConstraint.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mResultEClass, MResult.class, "MResult", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mAddEClass, MAdd.class, "MAdd", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMAdd_DefaultCommandMode(),
				this.getDefaultCommandMode(), "defaultCommandMode", null, 0, 1,
				MAdd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAdd_AddCommand(), this.getMAddCommand(), null,
				"addCommand", null, 0, -1, MAdd.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mAddCommandEClass, MAddCommand.class, "MAddCommand",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAddCommand_Type(), theMcorePackage.getMClassifier(),
				null, "type", null, 0, 1, MAddCommand.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAddCommand_Label(), ecorePackage.getEString(),
				"label", null, 0, 1, MAddCommand.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mUpdateEClass, MUpdate.class, "MUpdate", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMUpdate_Object(), this.getMUpdateObject(),
				this.getMUpdateObject_ContainingUpdateAnnotation(), "object",
				null, 0, 1, MUpdate.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMUpdate_Value(), this.getMUpdateValue(),
				this.getMUpdateValue_ContainingUpdateAnnotation(), "value",
				null, 0, 1, MUpdate.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMUpdate_Mode(), theUpdatesPackage.getUpdateMode(),
				"mode", null, 1, 1, MUpdate.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMUpdate_FeaturePackageFilter(),
				theMcorePackage.getMPackage(), null, "featurePackageFilter",
				null, 0, 1, MUpdate.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMUpdate_FeatureClassFilter(),
				theMcorePackage.getMClassifier(), null, "featureClassFilter",
				null, 0, 1, MUpdate.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMUpdate_Feature(), theMcorePackage.getMProperty(),
				null, "feature", null, 0, 1, MUpdate.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMUpdate_ContainingPropertyAnnotations(),
				this.getMPropertyAnnotations(), null,
				"containingPropertyAnnotations", null, 0, 1, MUpdate.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMUpdate_AlternativePersistencePackage(),
				this.getMUpdateAlternativePersistencePackage(), null,
				"alternativePersistencePackage", null, 0, 1, MUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMUpdate_Location(),
				this.getMUpdatePersistenceLocation(),
				this.getMUpdatePersistenceLocation_ContainingUpdateAnnotation(),
				"location", null, 0, 1, MUpdate.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMUpdate_AlternativePersistenceRoot(),
				this.getMUpdateAlternativePersistenceRoot(), null,
				"alternativePersistenceRoot", null, 0, 1, MUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMUpdate_AlternativePersistenceReference(),
				this.getMUpdateAlternativePersistenceReference(), null,
				"alternativePersistenceReference", null, 0, 1, MUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMUpdate_MUpdateCondition(),
				this.getMUpdateCondition(), null, "mUpdateCondition", null, 0,
				1, MUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mUpdateConditionEClass, MUpdateCondition.class,
				"MUpdateCondition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mUpdatePersistenceLocationEClass,
				MUpdatePersistenceLocation.class, "MUpdatePersistenceLocation",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(
				getMUpdatePersistenceLocation_ContainingUpdateAnnotation(),
				this.getMUpdate(), this.getMUpdate_Location(),
				"containingUpdateAnnotation", null, 0, 1,
				MUpdatePersistenceLocation.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mUpdateObjectEClass, MUpdateObject.class, "MUpdateObject",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMUpdateObject_ContainingUpdateAnnotation(),
				this.getMUpdate(), this.getMUpdate_Object(),
				"containingUpdateAnnotation", null, 0, 1, MUpdateObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mUpdateValueEClass, MUpdateValue.class, "MUpdateValue",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMUpdateValue_ContainingUpdateAnnotation(),
				this.getMUpdate(), this.getMUpdate_Value(),
				"containingUpdateAnnotation", null, 0, 1, MUpdateValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mInitializationOrderEClass, MInitializationOrder.class,
				"MInitializationOrder", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mInitializationValueEClass, MInitializationValue.class,
				"MInitializationValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mHighlightAnnotationEClass, MHighlightAnnotation.class,
				"MHighlightAnnotation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mChoiceConstructionEClass, MChoiceConstruction.class,
				"MChoiceConstruction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mChoiceConstraintEClass, MChoiceConstraint.class,
				"MChoiceConstraint", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mUpdatePersistenceEClass, MUpdatePersistence.class,
				"MUpdatePersistence", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mUpdateAlternativePersistencePackageEClass,
				MUpdateAlternativePersistencePackage.class,
				"MUpdateAlternativePersistencePackage", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mUpdateAlternativePersistenceRootEClass,
				MUpdateAlternativePersistenceRoot.class,
				"MUpdateAlternativePersistenceRoot", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mUpdateAlternativePersistenceReferenceEClass,
				MUpdateAlternativePersistenceReference.class,
				"MUpdateAlternativePersistenceReference", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.class, "MPropertyAnnotationsAction");
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.DO);
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.CONSTRUCT_CHOICE);
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.CONSTRAIN_CHOICE);
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.INIT_VALUE);
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.INIT_ORDER);
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.RESULT);
		addEEnumLiteral(mPropertyAnnotationsActionEEnum,
				MPropertyAnnotationsAction.HIGHLIGHT);

		initEEnum(mExprAnnotationActionEEnum, MExprAnnotationAction.class,
				"MExprAnnotationAction");
		addEEnumLiteral(mExprAnnotationActionEEnum, MExprAnnotationAction.DO);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.INTO_DATA_CONSTANT);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.INTO_LITERAL_CONSTANT);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.INTO_APPLY);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.INTO_ELSE_OF_IF_THEN_ELSE);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.ARGUMENT);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.DATA_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.LITERAL_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.SUB_CHAIN);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.COLLECTION_OP);
		addEEnumLiteral(mExprAnnotationActionEEnum,
				MExprAnnotationAction.WHERE_AS_BASE);

		initEEnum(defaultCommandModeEEnum, DefaultCommandMode.class,
				"DefaultCommandMode");
		addEEnumLiteral(defaultCommandModeEEnum,
				DefaultCommandMode.HIDE_DEFAULTFOR_EXPLICIT_ADD);
		addEEnumLiteral(defaultCommandModeEEnum,
				DefaultCommandMode.HIDE_EVERY_DEFAULT_ADD);
		addEEnumLiteral(defaultCommandModeEEnum,
				DefaultCommandMode.SHOW_EVERY_DEFAULT_ADD);
		addEEnumLiteral(defaultCommandModeEEnum,
				DefaultCommandMode.SHOW_DEFAULTFOR_EXPLICIT_ADD);

		initEEnum(generalAnnotationActionEEnum, GeneralAnnotationAction.class,
				"GeneralAnnotationAction");
		addEEnumLiteral(generalAnnotationActionEEnum,
				GeneralAnnotationAction.DO);
		addEEnumLiteral(generalAnnotationActionEEnum,
				GeneralAnnotationAction.INTO_DESCRIPTION);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/EXPRESSION_OCL
		createEXPRESSION_OCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(mClassifierAnnotationsEClass, source,
				new String[] { "label", "\'\'" });
		addAnnotation(getMClassifierAnnotations_Classifier(), source,
				new String[] { "derive",
						"self.eContainer().oclAsType(mcore::MPropertiesContainer).containingClassifier" });
		addAnnotation(
				getMAbstractPropertyAnnotations_ContainingPropertiesContainer(),
				source, new String[] { "derive",
						"if self.eContainer().oclIsUndefined() \r\n  then null\r\n  else let pc:MPropertiesContainer\r\n   = self.eContainer().oclAsType(annotations::MClassifierAnnotations).eContainer().oclAsType(MPropertiesContainer) in\r\n  pc  endif" });
		addAnnotation(getMAbstractPropertyAnnotations_ContainingClassifier(),
				source, new String[] { "derive",
						"if self.eContainer().oclIsUndefined() \r\n  then null\r\n  else let pc:MPropertiesContainer\r\n   = self.eContainer().oclAsType(annotations::MClassifierAnnotations).eContainer().oclAsType(MPropertiesContainer) in\r\n  pc.containingClassifier  endif" });
		addAnnotation(getMAbstractPropertyAnnotations_AnnotatedProperty(),
				source, new String[] { "derive", "null" });
		addAnnotation(mPropertyAnnotationsEClass, source, new String[] {
				"label",
				"(if self.property.oclIsUndefined() then \'PROPERTY MISSING\' else self.property.eLabel endif)" });
		addAnnotation(
				getMPropertyAnnotations__DoAction$Update__MPropertyAnnotationsAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMPropertyAnnotations_Property(), source, new String[] {
				"choiceConstruction",
				"\r\nself.containingClassifier.allFeatures()",
				"choiceConstraint",
				"trg=self.property or self.containingClassifier.allPropertyAnnotations().property\r\n->excludes(trg)  " });
		addAnnotation(getMPropertyAnnotations_Overriding(), source,
				new String[] { "derive",
						"if self.property.oclIsUndefined() then false else not(self.property.containingClassifier = self.containingClassifier) endif" });
		addAnnotation(getMPropertyAnnotations_DoAction(), source, new String[] {
				"derive",
				"mcore::annotations::MPropertyAnnotationsAction::Do\n" });
		addAnnotation(mOperationAnnotationsEClass, source, new String[] {
				"label",
				"if self.operationSignature.oclIsUndefined() then \'OP SIGNATURE MISSING\' else self.operationSignature.eLabel endif" });
		addAnnotation(getMOperationAnnotations_OperationSignature(), source,
				new String[] { "choiceConstruction",
						"self.containingClassifier.allOperationSignatures()",
						"choiceConstraint",
						"trg=self.operationSignature or \r\nself.containingClassifier.allOperationAnnotations().operationSignature->excludes(trg)" });
		addAnnotation(getMOperationAnnotations_Overriding(), source,
				new String[] { "derive",
						"if self.operationSignature.oclIsUndefined() then false else \r\nnot(self.operationSignature.containingClassifier = self.containingClassifier) endif" });
		addAnnotation(getMAbstractAnnotion_AnnotatedElement(), source,
				new String[] { "derive",
						"if not self.containingAbstractPropertyAnnotations.oclIsUndefined() and self.containingAbstractPropertyAnnotations.oclIsKindOf(MPropertyAnnotations) \n\tthen self.containingAbstractPropertyAnnotations.oclAsType(MPropertyAnnotations).property\n\telse \n\t\tif not self.containingAbstractPropertyAnnotations.oclIsUndefined() and self.containingAbstractPropertyAnnotations.oclIsKindOf(MOperationAnnotations)\n\t\t\tthen self.containingAbstractPropertyAnnotations.oclAsType(MOperationAnnotations).operationSignature\n\t\t\telse \n\t\t\t\tif self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(MClassifierAnnotations) \n\t\t\t\t\tthen  self.oclAsType(ecore::EObject).eContainer().oclAsType(MClassifierAnnotations).classifier\n\t\t\t\t\telse \n\t\t\t\t\t\tif self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(MModelElement) \n\t\t\t\t\t\t\tthen self.oclAsType(ecore::EObject).eContainer().oclAsType(MModelElement) \n\t\t\t\t\t\t\telse null \n\t\t\t\t\t\tendif\n\t\t\t\tendif\n\t\tendif\nendif" });
		addAnnotation(
				getMAbstractAnnotion_ContainingAbstractPropertyAnnotations(),
				source, new String[] { "derive",
						"if self.eContainer().oclIsKindOf(MAbstractPropertyAnnotations) then\r\n self.eContainer().oclAsType(MAbstractPropertyAnnotations)\r\n else null endif" });
		addAnnotation(
				getMGeneralAnnotation__DoAction$Update__GeneralAnnotationAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMGeneralAnnotation_DoAction(), source, new String[] {
				"derive",
				"let const1: mcore::annotations::GeneralAnnotationAction = mcore::annotations::GeneralAnnotationAction::Do in\nconst1\n",
				"choiceConstruction",
				"let mySet : Set(GeneralAnnotationAction) = Set{GeneralAnnotationAction::Do}\r\nin\r\nmySet->including(if self.mGeneralDetail.key->count(\'documentation\') >=1 then GeneralAnnotationAction::IntoDescription else null endif)" });
		addAnnotation(mGeneralDetailEClass, source, new String[] { "label",
				"let k: String = if key.oclIsUndefined() then \'<?>\' else key endif in\r\nlet v: String = if value.oclIsUndefined() then \'\' else value endif in\r\n\r\nk.concat(\' -> \').concat(v)" });
		addAnnotation(mStringAsOclAnnotationEClass, source,
				new String[] { "label", "\'<stringAsOcl>\'" });
		addAnnotation(mLayoutAnnotationEClass, source,
				new String[] { "label", "\'layout = ...\'\n" });
		addAnnotation(getMLayoutAnnotation_HideInTable(), source,
				new String[] { "initValue", "true\n" });
		addAnnotation(getMLayoutAnnotation_HideInPropertyView(), source,
				new String[] { "initValue", "true\n" });
		addAnnotation(getMAnnotation_DomainObject(), source, new String[] {
				"choiceConstruction",
				"if self.generalContent->isEmpty() \r\n  then self.generalReference\r\n  else if self.generalReference->isEmpty()\r\n    then self.generalContent\r\n    else self.generalContent->asSequence()\r\n    ->union(self.generalReference->asSequence()) endif endif\r\n" });
		addAnnotation(getMAnnotation_DomainOperation(), source, new String[] {
				"choiceConstraint",
				"if domainObject.oclIsUndefined() \r\n  then false\r\n  else if self.selfObjectTypeOfAnnotation.oclIsUndefined()\r\n    then false\r\n    else let c:MClassifier=domainObject.type in\r\n      if c.oclIsUndefined() \r\n        then false\r\n        else let sts:OrderedSet(MClassifier) = \r\n                    self.selfObjectTypeOfAnnotation.allSubTypes()\r\n                       ->append(self.selfObjectTypeOfAnnotation) in\r\n        \r\n        c.allProperties().operationSignature\r\n         ->select(s:MOperationSignature|\r\n            if  s.parameter->size() = 1\r\n              then let p:MParameter = s.parameter->first() in\r\n                if p.type.oclIsUndefined() \r\n                   then false\r\n                   else sts->includes(p.type) endif\r\n              else false endif)->includes(trg)\r\n    \r\n        endif endif endif\r\n              \r\n             " });
		addAnnotation(getMAnnotation_SelfObjectTypeOfAnnotation(), source,
				new String[] { "derive",
						"if eContainer().oclIsKindOf(annotations::MClassifierAnnotations) \r\n  then eContainer().oclAsType(annotations::MClassifierAnnotations).classifier \r\n  else if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n    then eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier \r\n    else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier\r\n      else if eContainer().oclIsTypeOf(annotations::MUpdate)\r\n        then eContainer().eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier \r\n        else null \r\nendif endif endif endif" });
		addAnnotation(getMAnnotation_TargetObjectTypeOfAnnotation(), source,
				new String[] { "derive", "null" });
		addAnnotation(getMAnnotation_TargetSimpleTypeOfAnnotation(), source,
				new String[] { "derive", "mcore::SimpleType::None\n" });
		addAnnotation(getMAnnotation_ObjectObjectTypeOfAnnotation(), source,
				new String[] { "derive", "null" });
		addAnnotation(getMExprAnnotation__OclChanged$Update__Boolean(), source,
				new String[] { "body", "null" });
		addAnnotation(
				getMExprAnnotation__DoAction$Update__MExprAnnotationAction(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getMExprAnnotation__DoActionUpdate__MExprAnnotationAction(),
				source, new String[] { "body", "null\n" });
		addAnnotation(
				getMExprAnnotation__VariableFromExpression__MNamedExpression(),
				source, new String[] { "body",
						"if self.localScopeVariables->isEmpty() then null\r\nelse\r\nself.localScopeVariables->select(x:mcore::expressions::MVariableBaseDefinition| if x.namedExpression<> null  then x.namedExpression = namedExpression else false endif )->first() endif" });
		addAnnotation(getMExprAnnotation_ExpectedReturnTypeOfAnnotation(),
				source, new String[] { "derive",
						"let apa:annotations::MAbstractPropertyAnnotations = eContainer().oclAsType(MAbstractPropertyAnnotations)\r\nin if apa.annotatedProperty.oclIsUndefined()\r\n  then null \r\n  else apa.annotatedProperty.calculatedType\r\nendif" });
		addAnnotation(getMExprAnnotation_ExpectedReturnSimpleTypeOfAnnotation(),
				source, new String[] { "derive",
						"if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.simpleType\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.simpleType\r\n      else SimpleType::None \r\nendif endif endif" });
		addAnnotation(getMExprAnnotation_IsReturnValueOfAnnotationMandatory(),
				source, new String[] { "derive",
						"if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.mandatory\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.mandatory\r\n      else true\r\nendif endif endif" });
		addAnnotation(getMExprAnnotation_IsReturnValueOfAnnotationSingular(),
				source, new String[] { "derive",
						"if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\n\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.singular\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.singular\r\n      else true\r\nendif endif endif" });
		addAnnotation(getMExprAnnotation_UseExplicitOcl(), source,
				new String[] { "initValue", "false\n" });
		addAnnotation(getMExprAnnotation_OclChanged(), source,
				new String[] { "derive", "false" });
		addAnnotation(getMExprAnnotation_OclCode(), source, new String[] {
				"derive",
				"let attentionImplementedinJava: String = \'TODO Replace Java Code\' in\nattentionImplementedinJava\n" });
		addAnnotation(getMExprAnnotation_DoAction(), source, new String[] {
				"derive", "mcore::annotations::MExprAnnotationAction::Do\n" });
		addAnnotation(mLabelEClass, source, new String[] { "label",
				"let prefix: String = \'label =  \' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mInvariantConstraintEClass, source, new String[] {
				"label",
				"(if self.containingClassifierAnnotations.constraint->indexOf(self)=1\r\n  then \'constraint(s): \' else \'                    \' endif)\r\n  .concat(self.eName)" });
		addAnnotation(getMInvariantConstraint_ContainingClassifierAnnotations(),
				source, new String[] { "derive",
						"self.eContainer().oclAsType(MClassifierAnnotations)" });
		addAnnotation(mResultEClass, source, new String[] { "label",
				"let prefix: String = \'result =  \' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mUpdateEClass, source, new String[] { "label",
				"\'update LHS. \'.concat(\r\nif self.feature.oclIsUndefined() then \'FEATURE MISSING\' else self.feature.eName endif).concat(\r\n  if self.mode=updates::UpdateMode::Redefine then \' := RHS\'\r\n  else if self.mode=updates::UpdateMode::Add then \' += RHS\'\r\n  else if self.mode=updates::UpdateMode::Remove then \' -= RHS\' else \' ERROR UPDATE MODE \'\r\n  endif endif endif\r\n  )" });
		addAnnotation(getMUpdate_Mode(), source, new String[] { "initValue",
				"let p:MProperty= containingPropertyAnnotations.annotatedProperty in\r\nif p.oclIsUndefined() then updates::UpdateMode::Redefine else \r\nif p.singular then updates::UpdateMode::Redefine else updates::UpdateMode::Add endif endif" });
		addAnnotation(getMUpdate_FeatureClassFilter(), source, new String[] {
				"choiceConstraint",
				"trg.kind=ClassifierKind::ClassType and\r\nif self.featurePackageFilter.oclIsUndefined() \r\nthen true else\r\ntrg.containingPackage = self.featurePackageFilter endif" });
		addAnnotation(getMUpdate_Feature(), source,
				new String[] { "choiceConstraint",
						"trg.kind<>PropertyKind::Operation\r\nand\r\nif self.featureClassFilter.oclIsUndefined()\r\n  then \r\n    if self.featurePackageFilter.oclIsUndefined() \r\n      then true\r\n      else trg.containingClassifier.containingPackage=\r\n        self.featurePackageFilter endif\r\n  else featureClassFilter.allFeatures()->includes(trg) endif",
						"color", " feature.oclIsUndefined()\n" });
		addAnnotation(getMUpdate_ContainingPropertyAnnotations(), source,
				new String[] { "derive",
						"self.eContainer().oclAsType(MPropertyAnnotations)" });
		addAnnotation(mUpdatePersistenceLocationEClass, source,
				new String[] { "label", "\'\'\n" });
		addAnnotation(mUpdateObjectEClass, source,
				new String[] { "label", "\'\'\n" });
		addAnnotation(mUpdateValueEClass, source,
				new String[] { "label", "\'\'\n" });
		addAnnotation(mInitializationOrderEClass, source, new String[] {
				"label",
				"let prefix: String = \'init order =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mInitializationValueEClass, source, new String[] {
				"label",
				"let prefix: String = \'init value =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mHighlightAnnotationEClass, source, new String[] {
				"label",
				"let prefix: String = \'highlight if \' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mChoiceConstructionEClass, source, new String[] { "label",
				"let prefix: String = \'choice construction =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mChoiceConstraintEClass, source, new String[] { "label",
				"let prefix: String = \'choice constraint =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(mUpdateAlternativePersistencePackageEClass, source,
				new String[] { "label", "\'\'\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mClassifierAnnotationsEClass, source,
				new String[] { "kindLabelDerive", "\'Semantics\'\n" });
		addAnnotation(mPropertyAnnotationsEClass, source,
				new String[] { "kindLabelDerive",
						"if self.annotatedProperty.oclIsUndefined() \n     then \'PROPERTY of ?\' \n     else  annotatedProperty.kind.toString().toUpperCase().concat(\' of \')\n              .concat(annotatedProperty.containingClassifier.eName)\n--self.annotatedProperty.containingClassifier.eName.concat(\'-\').concat(\n--self.annotatedProperty.kindLabel.toUpperCase())\nendif \n  \n--if self.property.oclIsUndefined() then \'Select Property\' else self.property.eName endif .concat(\'-Semantics\')",
						"annotatedPropertyDerive", "self.property" });
		addAnnotation(mOperationAnnotationsEClass, source, new String[] {
				"annotatedPropertyDerive",
				"if self.operationSignature.oclIsUndefined() then null\r\nelse self.operationSignature.operation endif",
				"kindLabelDerive",
				"if self.operationSignature.oclIsUndefined() then \n\'OPERATION of ?\'\nelse \'OPERATION of \'.concat(annotatedProperty.containingClassifier.eName) endif\n\n--self.annotatedProperty.containingClassifier.eName.concat(\'-OPERATION\') endif\n--if self.operationSignature.oclIsUndefined() then \'Select Signature\' else self.operationSignature.eLabel endif .concat(\'-Semantics\')" });
		addAnnotation(mGeneralAnnotationEClass, source,
				new String[] { "kindLabelDerive", "\'Source\'\n" });
		addAnnotation(mGeneralDetailEClass, source,
				new String[] { "kindLabelDerive", "\'Key\'\n" });
		addAnnotation(mLayoutAnnotationEClass, source,
				new String[] { "kindLabelDerive", "\'Layout\'\n" });
		addAnnotation(mLabelEClass, source,
				new String[] { "kindLabelDerive", "\'Label\'\n" });
		addAnnotation(mInvariantConstraintEClass, source,
				new String[] { "kindLabelDerive", "\'Constraint\'\n",
						"expectedReturnTypeOfAnnotationDerive", "null",
						"expectedReturnSimpleTypeOfAnnotationDerive",
						"SimpleType::Boolean",
						"isReturnValueOfAnnotationMandatoryDerive", "true",
						"isReturnValueOfAnnotationSingularDerive", "true" });
		addAnnotation(mResultEClass, source,
				new String[] { "kindLabelDerive", "\'Result\'\n" });
		addAnnotation(mUpdateEClass, source,
				new String[] { "kindLabelDerive", "\'Update\'\n",
						"nameInitValue",
						"let pos:Integer=\r\n  self.containingPropertyAnnotations.update->indexOf(self)  \r\nin\r\n  \'Update \'.concat(\r\n    if pos<10 then \'0\'.concat(pos.toString()) else pos.toString() endif)",
						"shortNameInitValue", "null" });
		addAnnotation(mUpdateConditionEClass, source,
				new String[] { "kindLabelDerive", "\'CONDITION\'\n",
						"objectObjectTypeDerive",
						"if (not eContainer().oclIsUndefined()) then\r\nlet class: MClassifier = \r\nself.eContainer().oclAsType(MUpdate).feature.containingClassifier \r\nin  if class.oclIsUndefined() then null else class endif\r\n\r\nelse null endif",
						"expectedReturnSimpleTypeDerive",
						"mcore::SimpleType::Boolean\n" });
		addAnnotation(mUpdatePersistenceLocationEClass, source,
				new String[] { "kindLabelDerive", "\'Persistence Location\'\n",
						"targetObjectTypeOfAnnotationDerive",
						"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedType\nendif endif\n",
						"targetSimpleTypeOfAnnotationDerive",
						"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedSimpleType\nendif endif\n",
						"containingAbstractPropertyAnnotationsDerive",
						"self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)",
						"objectObjectTypeOfAnnotationDerive",
						"if containingUpdateAnnotation.feature.oclIsUndefined()\n  then null\n  else containingUpdateAnnotation.feature.containingClassifier\nendif\n",
						"expectedReturnSimpleTypeOfAnnotationDerive",
						"mcore::SimpleType::String\n", "objectObjectTypeDerive",
						"objectObjectTypeOfAnnotation\n" });
		addAnnotation(mUpdateObjectEClass, source, new String[] {
				"kindLabelDerive", "\'LHS Object\'\n",
				"containingAbstractPropertyAnnotationsDerive",
				"self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)",
				"targetObjectTypeOfAnnotationDerive",
				"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedType\nendif endif\n",
				"targetSimpleTypeOfAnnotationDerive",
				"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedSimpleType\nendif endif\n",
				"objectObjectTypeOfAnnotationDerive",
				"if containingUpdateAnnotation.feature.oclIsUndefined()\n  then null\n  else containingUpdateAnnotation.feature.containingClassifier\nendif\n" });
		addAnnotation(mUpdateValueEClass, source, new String[] {
				"kindLabelDerive", "\'RHS\'\n",
				"targetObjectTypeOfAnnotationDerive",
				"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedType\nendif endif\n",
				"targetSimpleTypeOfAnnotationDerive",
				"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedSimpleType\nendif endif\n",
				"containingAbstractPropertyAnnotationsDerive",
				"self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)",
				"objectObjectTypeOfAnnotationDerive",
				"if containingUpdateAnnotation.feature.oclIsUndefined()\n  then null\n  else containingUpdateAnnotation.feature.containingClassifier\nendif\n" });
		addAnnotation(mInitializationOrderEClass, source,
				new String[] { "kindLabelDerive", "\'Init Order\'\n" });
		addAnnotation(mInitializationValueEClass, source,
				new String[] { "kindLabelDerive", "\'Init Value\'\n" });
		addAnnotation(mHighlightAnnotationEClass, source,
				new String[] { "kindLabelDerive", "\'Highlight\'\n" });
		addAnnotation(mChoiceConstructionEClass, source,
				new String[] { "kindLabelDerive", "\'Choice Values\'\n" });
		addAnnotation(mChoiceConstraintEClass, source,
				new String[] { "targetObjectTypeOfAnnotationDerive",
						"if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n    then let c: annotations::MPropertyAnnotations = eContainer().oclAsType(annotations::MPropertyAnnotations) in\r\n    \tif c.annotatedProperty.oclIsUndefined()\r\n    \t\tthen null\r\n    \t\telse c.annotatedProperty.calculatedType endif\r\n\telse null \r\nendif",
						"kindLabelDerive", "\'Choice Condition\'\n" });
		addAnnotation(mUpdatePersistenceEClass, source, new String[] {
				"targetObjectTypeOfAnnotationDerive",
				"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedType\nendif endif\n",
				"targetSimpleTypeOfAnnotationDerive",
				"let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedSimpleType\nendif endif\n",
				"containingAbstractPropertyAnnotationsDerive",
				"self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)",
				"expectedReturnSimpleTypeOfAnnotationDerive",
				"mcore::SimpleType::String\n",
				"objectObjectTypeOfAnnotationDerive",
				"if eContainer().oclIsKindOf(MUpdate) then\r\nlet update :MUpdate  = eContainer().oclAsType(MUpdate) in let class:MClassifier = update.feature.containingClassifier in if class.oclIsUndefined() then null else class endif\r\nelse \r\nnull\r\nendif" });
		addAnnotation(mUpdateAlternativePersistencePackageEClass, source,
				new String[] { "kindLabelDerive",
						"\'Alternative Package\'\n" });
		addAnnotation(mUpdateAlternativePersistenceRootEClass, source,
				new String[] { "kindLabelDerive",
						"\'Alternative Package Root\'\n" });
		addAnnotation(mUpdateAlternativePersistenceReferenceEClass, source,
				new String[] { "kindLabelDerive",
						"\'Alternative Persistence Reference\'\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMClassifierAnnotations_Classifier(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(
				getMAbstractPropertyAnnotations_ContainingPropertiesContainer(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractPropertyAnnotations_AnnotatedProperty(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertyAnnotations_Property(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMPropertyAnnotations_Overrides(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertyAnnotations_OverriddenIn(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertyAnnotations_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationAnnotations_OperationSignature(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMOperationAnnotations_Overrides(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationAnnotations_OverriddenIn(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractAnnotion_AnnotatedElement(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(
				getMAbstractAnnotion_ContainingAbstractPropertyAnnotations(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMGeneralAnnotation_DoAction(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMAnnotation_DomainObject(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMExprAnnotation_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getMClassifierAnnotations_Classifier(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(
				getMAbstractPropertyAnnotations_ContainingPropertiesContainer(),
				source, new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractPropertyAnnotations_ContainingClassifier(),
				source, new String[] { "createColumn", "false",
						"propertyCategory", "Annotated" });
		addAnnotation(getMAbstractPropertyAnnotations_AnnotatedProperty(),
				source, new String[] { "createColumn", "false",
						"propertyCategory", "Annotated" });
		addAnnotation(getMPropertyAnnotations_Property(), source, new String[] {
				"propertyCategory", "Annotated", "createColumn", "false" });
		addAnnotation(getMPropertyAnnotations_Overriding(), source,
				new String[] { "propertyCategory", "Overriding", "createColumn",
						"false" });
		addAnnotation(getMPropertyAnnotations_Overrides(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Overriding" });
		addAnnotation(getMPropertyAnnotations_OverriddenIn(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Overriding" });
		addAnnotation(getMPropertyAnnotations_Result(), source, new String[] {
				"propertyCategory", "Annotations", "createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_InitOrder(), source,
				new String[] { "propertyCategory", "Annotations",
						"createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_InitValue(), source,
				new String[] { "propertyCategory", "Annotations",
						"createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_ChoiceConstruction(), source,
				new String[] { "propertyCategory", "Annotations",
						"createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_ChoiceConstraint(), source,
				new String[] { "propertyCategory", "Annotations",
						"createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_Add(), source, new String[] {
				"propertyCategory", "Annotations", "createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_Update(), source, new String[] {
				"propertyCategory", "Annotations", "createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_StringAsOcl(), source,
				new String[] { "propertyCategory", "Annotations",
						"createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_Highlight(), source,
				new String[] { "propertyCategory", "Annotations",
						"createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_Layout(), source, new String[] {
				"propertyCategory", "Annotations", "createColumn", "true" });
		addAnnotation(getMPropertyAnnotations_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMOperationAnnotations_OperationSignature(), source,
				new String[] { "propertyCategory", "Annotated", "createColumn",
						"false" });
		addAnnotation(getMOperationAnnotations_Overriding(), source,
				new String[] { "propertyCategory", "Overriding", "createColumn",
						"false" });
		addAnnotation(getMOperationAnnotations_Overrides(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Overriding" });
		addAnnotation(getMOperationAnnotations_OverriddenIn(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Overriding" });
		addAnnotation(getMAbstractAnnotion_GeneralReference(), source,
				new String[] { "propertyCategory", "Special General Annotation",
						"createColumn", "false" });
		addAnnotation(getMAbstractAnnotion_GeneralContent(), source,
				new String[] { "propertyCategory", "Special General Annotation",
						"createColumn", "true" });
		addAnnotation(getMAbstractAnnotion_AnnotatedElement(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Structural" });
		addAnnotation(
				getMAbstractAnnotion_ContainingAbstractPropertyAnnotations(),
				source, new String[] { "createColumn", "true",
						"propertyCategory", "Structural" });
		addAnnotation(getMGeneralAnnotation_Source(), source,
				new String[] { "propertyCategory", "Special General Annotation",
						"createColumn", "false" });
		addAnnotation(getMGeneralAnnotation_MGeneralDetail(), source,
				new String[] { "propertyCategory", "Special General Annotation",
						"createColumn", "true" });
		addAnnotation(getMGeneralAnnotation_DoAction(), source, new String[] {
				"propertyCategory", "Actions", "createColumn", "false" });
		addAnnotation(getMAnnotation_DomainObject(), source,
				new String[] { "propertyCategory",
						"Special Domain Specific Semantics", "createColumn",
						"false" });
		addAnnotation(getMAnnotation_DomainOperation(), source,
				new String[] { "propertyCategory",
						"Special Domain Specific Semantics", "createColumn",
						"false" });
		addAnnotation(getMAnnotation_SelfObjectTypeOfAnnotation(), source,
				new String[] { "propertyCategory", "Typing", "createColumn",
						"false" });
		addAnnotation(getMAnnotation_TargetObjectTypeOfAnnotation(), source,
				new String[] { "propertyCategory", "Typing", "createColumn",
						"false" });
		addAnnotation(getMAnnotation_TargetSimpleTypeOfAnnotation(), source,
				new String[] { "propertyCategory", "Typing", "createColumn",
						"false" });
		addAnnotation(getMAnnotation_ObjectObjectTypeOfAnnotation(), source,
				new String[] { "propertyCategory", "Typing", "createColumn",
						"false" });
		addAnnotation(getMAnnotation_Value(), source, new String[] {
				"propertyCategory", "Explicit OCL", "createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL1(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL1Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL2(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL2Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL3(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL3Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL4(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL4Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL5(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL5Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL6(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL6Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL7(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(getMAnnotation_HistorizedOCL7Timestamp(), source,
				new String[] { "propertyCategory", "Historized OCL",
						"createColumn", "false" });
		addAnnotation(
				getMExprAnnotation__DoActionUpdate__MExprAnnotationAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(
				getMExprAnnotation__VariableFromExpression__MNamedExpression(),
				source, new String[] { "propertyCategory", "Helper",
						"createColumn", "true" });
		addAnnotation(getMExprAnnotation_ExpectedReturnTypeOfAnnotation(),
				source, new String[] { "propertyCategory", "Typing",
						"createColumn", "false" });
		addAnnotation(getMExprAnnotation_ExpectedReturnSimpleTypeOfAnnotation(),
				source, new String[] { "propertyCategory", "Typing",
						"createColumn", "false" });
		addAnnotation(getMExprAnnotation_IsReturnValueOfAnnotationMandatory(),
				source, new String[] { "propertyCategory", "Typing",
						"createColumn", "false" });
		addAnnotation(getMExprAnnotation_IsReturnValueOfAnnotationSingular(),
				source, new String[] { "propertyCategory", "Typing",
						"createColumn", "false" });
		addAnnotation(getMExprAnnotation_UseExplicitOcl(), source,
				new String[] { "propertyCategory", "Explicit OCL",
						"createColumn", "false" });
		addAnnotation(getMExprAnnotation_OclChanged(), source, new String[] {
				"propertyCategory", "Explicit OCL", "createColumn", "false" });
		addAnnotation(getMExprAnnotation_OclCode(), source, new String[] {
				"propertyCategory", "Explicit OCL", "createColumn", "false" });
		addAnnotation(getMExprAnnotation_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMUpdate_AlternativePersistencePackage(), source,
				new String[] { "propertyCategory", "Persistence",
						"createColumn", "true" });
		addAnnotation(getMUpdate_Location(), source, new String[] {
				"propertyCategory", "Persistence", "createColumn", "true" });
		addAnnotation(getMUpdate_AlternativePersistenceRoot(), source,
				new String[] { "propertyCategory", "Persistence",
						"createColumn", "true" });
		addAnnotation(getMUpdate_AlternativePersistenceReference(), source,
				new String[] { "propertyCategory", "Persistence",
						"createColumn", "true" });
		addAnnotation(getMUpdate_MUpdateCondition(), source, new String[] {
				"propertyCategory", "Condition", "createColumn", "true" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EXPRESSION_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEXPRESSION_OCLAnnotations() {
		String source = "http://www.xocl.org/EXPRESSION_OCL";
		addAnnotation(getMAnnotation_Value(), source, new String[] {});
		addAnnotation(getMAnnotation_Value(), source, new String[] {});
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(getMGeneralAnnotation_DoAction(), source,
				new String[] { "mName", "doAction" });
		addAnnotation(
				(getMExprAnnotation__DoActionUpdate__MExprAnnotationAction())
						.getEParameters().get(0),
				source, new String[] { "mName", "trg" });
		addAnnotation(
				getMExprAnnotation__VariableFromExpression__MNamedExpression(),
				source, new String[] { "mName", "VariableFromExpression" });
		addAnnotation(
				(getMExprAnnotation__VariableFromExpression__MNamedExpression())
						.getEParameters().get(0),
				source, new String[] { "mName", "NamedExpression" });
		addAnnotation(defaultCommandModeEEnum.getELiterals().get(0), source,
				new String[] { "mName", "Hide Default for Explicit Add" });
		addAnnotation(defaultCommandModeEEnum.getELiterals().get(3), source,
				new String[] { "mName", "Show Default for Explicit Add" });
		addAnnotation(getMUpdate_Location(), source,
				new String[] { "mName", "location" });
		addAnnotation(getMUpdateObject_ContainingUpdateAnnotation(), source,
				new String[] { "mName", "containingUpdateAnnotation" });
		addAnnotation(generalAnnotationActionEEnum, source,
				new String[] { "mName", "General Annotation Action " });
		addAnnotation(mUpdatePersistenceEClass, source,
				new String[] { "mName", "MUpdatePersistence" });
	}

} //AnnotationsPackageImpl
