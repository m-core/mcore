/**
 */
package com.montages.mcore.annotations.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.MUpdateAlternativePersistencePackage;
import com.montages.mcore.annotations.MUpdateAlternativePersistenceReference;
import com.montages.mcore.annotations.MUpdateAlternativePersistenceRoot;
import com.montages.mcore.annotations.MUpdateCondition;
import com.montages.mcore.annotations.MUpdateObject;
import com.montages.mcore.annotations.MUpdatePersistenceLocation;
import com.montages.mcore.annotations.MUpdateValue;
import com.montages.mcore.impl.MNamedImpl;
import com.montages.mcore.updates.UpdateMode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUpdate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getObject <em>Object</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getFeaturePackageFilter <em>Feature Package Filter</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getFeatureClassFilter <em>Feature Class Filter</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getContainingPropertyAnnotations <em>Containing Property Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdateImpl#getMUpdateCondition <em>MUpdate Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MUpdateImpl extends MNamedImpl
		implements MUpdate, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected MUpdateObject object;

	/**
	 * This is true if the Object containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean objectESet;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected MUpdateValue value;

	/**
	 * This is true if the Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean valueESet;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final UpdateMode MODE_EDEFAULT = UpdateMode.REDEFINE;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected UpdateMode mode = MODE_EDEFAULT;

	/**
	 * This is true if the Mode attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean modeESet;

	/**
	 * The cached value of the '{@link #getFeaturePackageFilter() <em>Feature Package Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturePackageFilter()
	 * @generated
	 * @ordered
	 */
	protected MPackage featurePackageFilter;

	/**
	 * This is true if the Feature Package Filter reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean featurePackageFilterESet;

	/**
	 * The cached value of the '{@link #getFeatureClassFilter() <em>Feature Class Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureClassFilter()
	 * @generated
	 * @ordered
	 */
	protected MClassifier featureClassFilter;

	/**
	 * This is true if the Feature Class Filter reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean featureClassFilterESet;

	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected MProperty feature;

	/**
	 * This is true if the Feature reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean featureESet;

	/**
	 * The cached value of the '{@link #getAlternativePersistencePackage() <em>Alternative Persistence Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistencePackage()
	 * @generated
	 * @ordered
	 */
	protected MUpdateAlternativePersistencePackage alternativePersistencePackage;

	/**
	 * This is true if the Alternative Persistence Package containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean alternativePersistencePackageESet;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected MUpdatePersistenceLocation location;

	/**
	 * This is true if the Location containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean locationESet;

	/**
	 * The cached value of the '{@link #getAlternativePersistenceRoot() <em>Alternative Persistence Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistenceRoot()
	 * @generated
	 * @ordered
	 */
	protected MUpdateAlternativePersistenceRoot alternativePersistenceRoot;

	/**
	 * This is true if the Alternative Persistence Root containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean alternativePersistenceRootESet;

	/**
	 * The cached value of the '{@link #getAlternativePersistenceReference() <em>Alternative Persistence Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistenceReference()
	 * @generated
	 * @ordered
	 */
	protected MUpdateAlternativePersistenceReference alternativePersistenceReference;

	/**
	 * This is true if the Alternative Persistence Reference containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean alternativePersistenceReferenceESet;

	/**
	 * The cached value of the '{@link #getMUpdateCondition() <em>MUpdate Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMUpdateCondition()
	 * @generated
	 * @ordered
	 */
	protected MUpdateCondition mUpdateCondition;

	/**
	 * This is true if the MUpdate Condition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mUpdateConditionESet;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getFeatureClassFilter <em>Feature Class Filter</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureClassFilter
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression featureClassFilterChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getFeature <em>Feature</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression featureChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingPropertyAnnotations <em>Containing Property Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingPropertyAnnotations
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPropertyAnnotationsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUpdateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MUPDATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateObject getObject() {
		if (object != null && object.eIsProxy()) {
			InternalEObject oldObject = (InternalEObject) object;
			object = (MUpdateObject) eResolveProxy(oldObject);
			if (object != oldObject) {
				InternalEObject newObject = (InternalEObject) object;
				NotificationChain msgs = oldObject.eInverseRemove(this,
						AnnotationsPackage.MUPDATE_OBJECT__CONTAINING_UPDATE_ANNOTATION,
						MUpdateObject.class, null);
				if (newObject.eInternalContainer() == null) {
					msgs = newObject.eInverseAdd(this,
							AnnotationsPackage.MUPDATE_OBJECT__CONTAINING_UPDATE_ANNOTATION,
							MUpdateObject.class, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__OBJECT, oldObject,
							object));
			}
		}
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateObject basicGetObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObject(MUpdateObject newObject,
			NotificationChain msgs) {
		MUpdateObject oldObject = object;
		object = newObject;
		boolean oldObjectESet = objectESet;
		objectESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, AnnotationsPackage.MUPDATE__OBJECT,
					oldObject, newObject, !oldObjectESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(MUpdateObject newObject) {
		if (newObject != object) {
			NotificationChain msgs = null;
			if (object != null)
				msgs = ((InternalEObject) object).eInverseRemove(this,
						AnnotationsPackage.MUPDATE_OBJECT__CONTAINING_UPDATE_ANNOTATION,
						MUpdateObject.class, msgs);
			if (newObject != null)
				msgs = ((InternalEObject) newObject).eInverseAdd(this,
						AnnotationsPackage.MUPDATE_OBJECT__CONTAINING_UPDATE_ANNOTATION,
						MUpdateObject.class, msgs);
			msgs = basicSetObject(newObject, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldObjectESet = objectESet;
			objectESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__OBJECT, newObject,
						newObject, !oldObjectESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetObject(NotificationChain msgs) {
		MUpdateObject oldObject = object;
		object = null;
		boolean oldObjectESet = objectESet;
		objectESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET, AnnotationsPackage.MUPDATE__OBJECT,
					oldObject, null, oldObjectESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObject() {
		if (object != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) object).eInverseRemove(this,
					AnnotationsPackage.MUPDATE_OBJECT__CONTAINING_UPDATE_ANNOTATION,
					MUpdateObject.class, msgs);
			msgs = basicUnsetObject(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldObjectESet = objectESet;
			objectESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__OBJECT, null, null,
						oldObjectESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObject() {
		return objectESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateValue getValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject) value;
			value = (MUpdateValue) eResolveProxy(oldValue);
			if (value != oldValue) {
				InternalEObject newValue = (InternalEObject) value;
				NotificationChain msgs = oldValue.eInverseRemove(this,
						AnnotationsPackage.MUPDATE_VALUE__CONTAINING_UPDATE_ANNOTATION,
						MUpdateValue.class, null);
				if (newValue.eInternalContainer() == null) {
					msgs = newValue.eInverseAdd(this,
							AnnotationsPackage.MUPDATE_VALUE__CONTAINING_UPDATE_ANNOTATION,
							MUpdateValue.class, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__VALUE, oldValue,
							value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateValue basicGetValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(MUpdateValue newValue,
			NotificationChain msgs) {
		MUpdateValue oldValue = value;
		value = newValue;
		boolean oldValueESet = valueESet;
		valueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, AnnotationsPackage.MUPDATE__VALUE,
					oldValue, newValue, !oldValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(MUpdateValue newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject) value).eInverseRemove(this,
						AnnotationsPackage.MUPDATE_VALUE__CONTAINING_UPDATE_ANNOTATION,
						MUpdateValue.class, msgs);
			if (newValue != null)
				msgs = ((InternalEObject) newValue).eInverseAdd(this,
						AnnotationsPackage.MUPDATE_VALUE__CONTAINING_UPDATE_ANNOTATION,
						MUpdateValue.class, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldValueESet = valueESet;
			valueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__VALUE, newValue, newValue,
						!oldValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetValue(NotificationChain msgs) {
		MUpdateValue oldValue = value;
		value = null;
		boolean oldValueESet = valueESet;
		valueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET, AnnotationsPackage.MUPDATE__VALUE,
					oldValue, null, oldValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetValue() {
		if (value != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) value).eInverseRemove(this,
					AnnotationsPackage.MUPDATE_VALUE__CONTAINING_UPDATE_ANNOTATION,
					MUpdateValue.class, msgs);
			msgs = basicUnsetValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldValueESet = valueESet;
			valueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__VALUE, null, null,
						oldValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetValue() {
		return valueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdatePersistenceLocation getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject) location;
			location = (MUpdatePersistenceLocation) eResolveProxy(oldLocation);
			if (location != oldLocation) {
				InternalEObject newLocation = (InternalEObject) location;
				NotificationChain msgs = oldLocation.eInverseRemove(this,
						AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
						MUpdatePersistenceLocation.class, null);
				if (newLocation.eInternalContainer() == null) {
					msgs = newLocation.eInverseAdd(this,
							AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
							MUpdatePersistenceLocation.class, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__LOCATION, oldLocation,
							location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdatePersistenceLocation basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(
			MUpdatePersistenceLocation newLocation, NotificationChain msgs) {
		MUpdatePersistenceLocation oldLocation = location;
		location = newLocation;
		boolean oldLocationESet = locationESet;
		locationESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, AnnotationsPackage.MUPDATE__LOCATION,
					oldLocation, newLocation, !oldLocationESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(MUpdatePersistenceLocation newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject) location).eInverseRemove(this,
						AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
						MUpdatePersistenceLocation.class, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject) newLocation).eInverseAdd(this,
						AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
						MUpdatePersistenceLocation.class, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldLocationESet = locationESet;
			locationESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__LOCATION, newLocation,
						newLocation, !oldLocationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetLocation(NotificationChain msgs) {
		MUpdatePersistenceLocation oldLocation = location;
		location = null;
		boolean oldLocationESet = locationESet;
		locationESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET, AnnotationsPackage.MUPDATE__LOCATION,
					oldLocation, null, oldLocationESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLocation() {
		if (location != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) location).eInverseRemove(this,
					AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
					MUpdatePersistenceLocation.class, msgs);
			msgs = basicUnsetLocation(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldLocationESet = locationESet;
			locationESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__LOCATION, null, null,
						oldLocationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLocation() {
		return locationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistenceRoot getAlternativePersistenceRoot() {
		if (alternativePersistenceRoot != null
				&& alternativePersistenceRoot.eIsProxy()) {
			InternalEObject oldAlternativePersistenceRoot = (InternalEObject) alternativePersistenceRoot;
			alternativePersistenceRoot = (MUpdateAlternativePersistenceRoot) eResolveProxy(
					oldAlternativePersistenceRoot);
			if (alternativePersistenceRoot != oldAlternativePersistenceRoot) {
				InternalEObject newAlternativePersistenceRoot = (InternalEObject) alternativePersistenceRoot;
				NotificationChain msgs = oldAlternativePersistenceRoot
						.eInverseRemove(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
								null, null);
				if (newAlternativePersistenceRoot
						.eInternalContainer() == null) {
					msgs = newAlternativePersistenceRoot.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
							oldAlternativePersistenceRoot,
							alternativePersistenceRoot));
			}
		}
		return alternativePersistenceRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistenceRoot basicGetAlternativePersistenceRoot() {
		return alternativePersistenceRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlternativePersistenceRoot(
			MUpdateAlternativePersistenceRoot newAlternativePersistenceRoot,
			NotificationChain msgs) {
		MUpdateAlternativePersistenceRoot oldAlternativePersistenceRoot = alternativePersistenceRoot;
		alternativePersistenceRoot = newAlternativePersistenceRoot;
		boolean oldAlternativePersistenceRootESet = alternativePersistenceRootESet;
		alternativePersistenceRootESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
					oldAlternativePersistenceRoot,
					newAlternativePersistenceRoot,
					!oldAlternativePersistenceRootESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlternativePersistenceRoot(
			MUpdateAlternativePersistenceRoot newAlternativePersistenceRoot) {
		if (newAlternativePersistenceRoot != alternativePersistenceRoot) {
			NotificationChain msgs = null;
			if (alternativePersistenceRoot != null)
				msgs = ((InternalEObject) alternativePersistenceRoot)
						.eInverseRemove(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
								null, msgs);
			if (newAlternativePersistenceRoot != null)
				msgs = ((InternalEObject) newAlternativePersistenceRoot)
						.eInverseAdd(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
								null, msgs);
			msgs = basicSetAlternativePersistenceRoot(
					newAlternativePersistenceRoot, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAlternativePersistenceRootESet = alternativePersistenceRootESet;
			alternativePersistenceRootESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
						newAlternativePersistenceRoot,
						newAlternativePersistenceRoot,
						!oldAlternativePersistenceRootESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAlternativePersistenceRoot(
			NotificationChain msgs) {
		MUpdateAlternativePersistenceRoot oldAlternativePersistenceRoot = alternativePersistenceRoot;
		alternativePersistenceRoot = null;
		boolean oldAlternativePersistenceRootESet = alternativePersistenceRootESet;
		alternativePersistenceRootESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
					oldAlternativePersistenceRoot, null,
					oldAlternativePersistenceRootESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAlternativePersistenceRoot() {
		if (alternativePersistenceRoot != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) alternativePersistenceRoot)
					.eInverseRemove(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
							null, msgs);
			msgs = basicUnsetAlternativePersistenceRoot(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAlternativePersistenceRootESet = alternativePersistenceRootESet;
			alternativePersistenceRootESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
						null, null, oldAlternativePersistenceRootESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAlternativePersistenceRoot() {
		return alternativePersistenceRootESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistenceReference getAlternativePersistenceReference() {
		if (alternativePersistenceReference != null
				&& alternativePersistenceReference.eIsProxy()) {
			InternalEObject oldAlternativePersistenceReference = (InternalEObject) alternativePersistenceReference;
			alternativePersistenceReference = (MUpdateAlternativePersistenceReference) eResolveProxy(
					oldAlternativePersistenceReference);
			if (alternativePersistenceReference != oldAlternativePersistenceReference) {
				InternalEObject newAlternativePersistenceReference = (InternalEObject) alternativePersistenceReference;
				NotificationChain msgs = oldAlternativePersistenceReference
						.eInverseRemove(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
								null, null);
				if (newAlternativePersistenceReference
						.eInternalContainer() == null) {
					msgs = newAlternativePersistenceReference.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
							oldAlternativePersistenceReference,
							alternativePersistenceReference));
			}
		}
		return alternativePersistenceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistenceReference basicGetAlternativePersistenceReference() {
		return alternativePersistenceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlternativePersistenceReference(
			MUpdateAlternativePersistenceReference newAlternativePersistenceReference,
			NotificationChain msgs) {
		MUpdateAlternativePersistenceReference oldAlternativePersistenceReference = alternativePersistenceReference;
		alternativePersistenceReference = newAlternativePersistenceReference;
		boolean oldAlternativePersistenceReferenceESet = alternativePersistenceReferenceESet;
		alternativePersistenceReferenceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
					oldAlternativePersistenceReference,
					newAlternativePersistenceReference,
					!oldAlternativePersistenceReferenceESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlternativePersistenceReference(
			MUpdateAlternativePersistenceReference newAlternativePersistenceReference) {
		if (newAlternativePersistenceReference != alternativePersistenceReference) {
			NotificationChain msgs = null;
			if (alternativePersistenceReference != null)
				msgs = ((InternalEObject) alternativePersistenceReference)
						.eInverseRemove(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
								null, msgs);
			if (newAlternativePersistenceReference != null)
				msgs = ((InternalEObject) newAlternativePersistenceReference)
						.eInverseAdd(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
								null, msgs);
			msgs = basicSetAlternativePersistenceReference(
					newAlternativePersistenceReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAlternativePersistenceReferenceESet = alternativePersistenceReferenceESet;
			alternativePersistenceReferenceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
						newAlternativePersistenceReference,
						newAlternativePersistenceReference,
						!oldAlternativePersistenceReferenceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAlternativePersistenceReference(
			NotificationChain msgs) {
		MUpdateAlternativePersistenceReference oldAlternativePersistenceReference = alternativePersistenceReference;
		alternativePersistenceReference = null;
		boolean oldAlternativePersistenceReferenceESet = alternativePersistenceReferenceESet;
		alternativePersistenceReferenceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
					oldAlternativePersistenceReference, null,
					oldAlternativePersistenceReferenceESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAlternativePersistenceReference() {
		if (alternativePersistenceReference != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) alternativePersistenceReference)
					.eInverseRemove(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
							null, msgs);
			msgs = basicUnsetAlternativePersistenceReference(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAlternativePersistenceReferenceESet = alternativePersistenceReferenceESet;
			alternativePersistenceReferenceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
						null, null, oldAlternativePersistenceReferenceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAlternativePersistenceReference() {
		return alternativePersistenceReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateCondition getMUpdateCondition() {
		if (mUpdateCondition != null && mUpdateCondition.eIsProxy()) {
			InternalEObject oldMUpdateCondition = (InternalEObject) mUpdateCondition;
			mUpdateCondition = (MUpdateCondition) eResolveProxy(
					oldMUpdateCondition);
			if (mUpdateCondition != oldMUpdateCondition) {
				InternalEObject newMUpdateCondition = (InternalEObject) mUpdateCondition;
				NotificationChain msgs = oldMUpdateCondition.eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
						null, null);
				if (newMUpdateCondition.eInternalContainer() == null) {
					msgs = newMUpdateCondition.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
							oldMUpdateCondition, mUpdateCondition));
			}
		}
		return mUpdateCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateCondition basicGetMUpdateCondition() {
		return mUpdateCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMUpdateCondition(
			MUpdateCondition newMUpdateCondition, NotificationChain msgs) {
		MUpdateCondition oldMUpdateCondition = mUpdateCondition;
		mUpdateCondition = newMUpdateCondition;
		boolean oldMUpdateConditionESet = mUpdateConditionESet;
		mUpdateConditionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
					oldMUpdateCondition, newMUpdateCondition,
					!oldMUpdateConditionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMUpdateCondition(MUpdateCondition newMUpdateCondition) {
		if (newMUpdateCondition != mUpdateCondition) {
			NotificationChain msgs = null;
			if (mUpdateCondition != null)
				msgs = ((InternalEObject) mUpdateCondition).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
						null, msgs);
			if (newMUpdateCondition != null)
				msgs = ((InternalEObject) newMUpdateCondition).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
						null, msgs);
			msgs = basicSetMUpdateCondition(newMUpdateCondition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldMUpdateConditionESet = mUpdateConditionESet;
			mUpdateConditionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
						newMUpdateCondition, newMUpdateCondition,
						!oldMUpdateConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetMUpdateCondition(
			NotificationChain msgs) {
		MUpdateCondition oldMUpdateCondition = mUpdateCondition;
		mUpdateCondition = null;
		boolean oldMUpdateConditionESet = mUpdateConditionESet;
		mUpdateConditionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
					oldMUpdateCondition, null, oldMUpdateConditionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMUpdateCondition() {
		if (mUpdateCondition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) mUpdateCondition).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MUPDATE__MUPDATE_CONDITION,
					null, msgs);
			msgs = basicUnsetMUpdateCondition(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldMUpdateConditionESet = mUpdateConditionESet;
			mUpdateConditionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__MUPDATE_CONDITION, null,
						null, oldMUpdateConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMUpdateCondition() {
		return mUpdateConditionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UpdateMode getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(UpdateMode newMode) {
		UpdateMode oldMode = mode;
		mode = newMode == null ? MODE_EDEFAULT : newMode;
		boolean oldModeESet = modeESet;
		modeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MUPDATE__MODE, oldMode, mode,
					!oldModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMode() {
		UpdateMode oldMode = mode;
		boolean oldModeESet = modeESet;
		mode = MODE_EDEFAULT;
		modeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MUPDATE__MODE, oldMode, MODE_EDEFAULT,
					oldModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMode() {
		return modeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getFeaturePackageFilter() {
		if (featurePackageFilter != null && featurePackageFilter.eIsProxy()) {
			InternalEObject oldFeaturePackageFilter = (InternalEObject) featurePackageFilter;
			featurePackageFilter = (MPackage) eResolveProxy(
					oldFeaturePackageFilter);
			if (featurePackageFilter != oldFeaturePackageFilter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER,
							oldFeaturePackageFilter, featurePackageFilter));
			}
		}
		return featurePackageFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetFeaturePackageFilter() {
		return featurePackageFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeaturePackageFilter(MPackage newFeaturePackageFilter) {
		MPackage oldFeaturePackageFilter = featurePackageFilter;
		featurePackageFilter = newFeaturePackageFilter;
		boolean oldFeaturePackageFilterESet = featurePackageFilterESet;
		featurePackageFilterESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER,
					oldFeaturePackageFilter, featurePackageFilter,
					!oldFeaturePackageFilterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFeaturePackageFilter() {
		MPackage oldFeaturePackageFilter = featurePackageFilter;
		boolean oldFeaturePackageFilterESet = featurePackageFilterESet;
		featurePackageFilter = null;
		featurePackageFilterESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER,
					oldFeaturePackageFilter, null,
					oldFeaturePackageFilterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFeaturePackageFilter() {
		return featurePackageFilterESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getFeatureClassFilter() {
		if (featureClassFilter != null && featureClassFilter.eIsProxy()) {
			InternalEObject oldFeatureClassFilter = (InternalEObject) featureClassFilter;
			featureClassFilter = (MClassifier) eResolveProxy(
					oldFeatureClassFilter);
			if (featureClassFilter != oldFeatureClassFilter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER,
							oldFeatureClassFilter, featureClassFilter));
			}
		}
		return featureClassFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetFeatureClassFilter() {
		return featureClassFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureClassFilter(MClassifier newFeatureClassFilter) {
		MClassifier oldFeatureClassFilter = featureClassFilter;
		featureClassFilter = newFeatureClassFilter;
		boolean oldFeatureClassFilterESet = featureClassFilterESet;
		featureClassFilterESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER,
					oldFeatureClassFilter, featureClassFilter,
					!oldFeatureClassFilterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFeatureClassFilter() {
		MClassifier oldFeatureClassFilter = featureClassFilter;
		boolean oldFeatureClassFilterESet = featureClassFilterESet;
		featureClassFilter = null;
		featureClassFilterESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER,
					oldFeatureClassFilter, null, oldFeatureClassFilterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFeatureClassFilter() {
		return featureClassFilterESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Feature Class Filter</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.kind=ClassifierKind::ClassType and
	if self.featurePackageFilter.oclIsUndefined() 
	then true else
	trg.containingPackage = self.featurePackageFilter endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalFeatureClassFilterChoiceConstraint(MClassifier trg) {
		EClass eClass = AnnotationsPackage.Literals.MUPDATE;
		if (featureClassFilterChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = AnnotationsPackage.Literals.MUPDATE__FEATURE_CLASS_FILTER;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				featureClassFilterChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"FeatureClassFilterChoiceConstraint");
			}
		}
		Query query = OCL_ENV
				.createQuery(featureClassFilterChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "FeatureClassFilterChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getFeature() {
		if (feature != null && feature.eIsProxy()) {
			InternalEObject oldFeature = (InternalEObject) feature;
			feature = (MProperty) eResolveProxy(oldFeature);
			if (feature != oldFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__FEATURE, oldFeature,
							feature));
			}
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeature(MProperty newFeature) {
		MProperty oldFeature = feature;
		feature = newFeature;
		boolean oldFeatureESet = featureESet;
		featureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MUPDATE__FEATURE, oldFeature, feature,
					!oldFeatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFeature() {
		MProperty oldFeature = feature;
		boolean oldFeatureESet = featureESet;
		feature = null;
		featureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MUPDATE__FEATURE, oldFeature, null,
					oldFeatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFeature() {
		return featureESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Feature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MProperty
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.kind<>PropertyKind::Operation
	and
	if self.featureClassFilter.oclIsUndefined()
	then 
	if self.featurePackageFilter.oclIsUndefined() 
	  then true
	  else trg.containingClassifier.containingPackage=
	    self.featurePackageFilter endif
	else featureClassFilter.allFeatures()->includes(trg) endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalFeatureChoiceConstraint(MProperty trg) {
		EClass eClass = AnnotationsPackage.Literals.MUPDATE;
		if (featureChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = AnnotationsPackage.Literals.MUPDATE__FEATURE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				featureChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"FeatureChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(featureChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "FeatureChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations getContainingPropertyAnnotations() {
		MPropertyAnnotations containingPropertyAnnotations = basicGetContainingPropertyAnnotations();
		return containingPropertyAnnotations != null
				&& containingPropertyAnnotations.eIsProxy()
						? (MPropertyAnnotations) eResolveProxy(
								(InternalEObject) containingPropertyAnnotations)
						: containingPropertyAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations basicGetContainingPropertyAnnotations() {
		/**
		 * @OCL self.eContainer().oclAsType(MPropertyAnnotations)
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MUPDATE;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MUPDATE__CONTAINING_PROPERTY_ANNOTATIONS;

		if (containingPropertyAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPropertyAnnotationsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MUPDATE, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingPropertyAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MUPDATE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyAnnotations result = (MPropertyAnnotations) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistencePackage getAlternativePersistencePackage() {
		if (alternativePersistencePackage != null
				&& alternativePersistencePackage.eIsProxy()) {
			InternalEObject oldAlternativePersistencePackage = (InternalEObject) alternativePersistencePackage;
			alternativePersistencePackage = (MUpdateAlternativePersistencePackage) eResolveProxy(
					oldAlternativePersistencePackage);
			if (alternativePersistencePackage != oldAlternativePersistencePackage) {
				InternalEObject newAlternativePersistencePackage = (InternalEObject) alternativePersistencePackage;
				NotificationChain msgs = oldAlternativePersistencePackage
						.eInverseRemove(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
								null, null);
				if (newAlternativePersistencePackage
						.eInternalContainer() == null) {
					msgs = newAlternativePersistencePackage.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
							oldAlternativePersistencePackage,
							alternativePersistencePackage));
			}
		}
		return alternativePersistencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistencePackage basicGetAlternativePersistencePackage() {
		return alternativePersistencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlternativePersistencePackage(
			MUpdateAlternativePersistencePackage newAlternativePersistencePackage,
			NotificationChain msgs) {
		MUpdateAlternativePersistencePackage oldAlternativePersistencePackage = alternativePersistencePackage;
		alternativePersistencePackage = newAlternativePersistencePackage;
		boolean oldAlternativePersistencePackageESet = alternativePersistencePackageESet;
		alternativePersistencePackageESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
					oldAlternativePersistencePackage,
					newAlternativePersistencePackage,
					!oldAlternativePersistencePackageESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlternativePersistencePackage(
			MUpdateAlternativePersistencePackage newAlternativePersistencePackage) {
		if (newAlternativePersistencePackage != alternativePersistencePackage) {
			NotificationChain msgs = null;
			if (alternativePersistencePackage != null)
				msgs = ((InternalEObject) alternativePersistencePackage)
						.eInverseRemove(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
								null, msgs);
			if (newAlternativePersistencePackage != null)
				msgs = ((InternalEObject) newAlternativePersistencePackage)
						.eInverseAdd(this,
								EOPPOSITE_FEATURE_BASE
										- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
								null, msgs);
			msgs = basicSetAlternativePersistencePackage(
					newAlternativePersistencePackage, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAlternativePersistencePackageESet = alternativePersistencePackageESet;
			alternativePersistencePackageESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
						newAlternativePersistencePackage,
						newAlternativePersistencePackage,
						!oldAlternativePersistencePackageESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAlternativePersistencePackage(
			NotificationChain msgs) {
		MUpdateAlternativePersistencePackage oldAlternativePersistencePackage = alternativePersistencePackage;
		alternativePersistencePackage = null;
		boolean oldAlternativePersistencePackageESet = alternativePersistencePackageESet;
		alternativePersistencePackageESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
					oldAlternativePersistencePackage, null,
					oldAlternativePersistencePackageESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAlternativePersistencePackage() {
		if (alternativePersistencePackage != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) alternativePersistencePackage)
					.eInverseRemove(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
							null, msgs);
			msgs = basicUnsetAlternativePersistencePackage(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAlternativePersistencePackageESet = alternativePersistencePackageESet;
			alternativePersistencePackageESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
						null, null, oldAlternativePersistencePackageESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAlternativePersistencePackage() {
		return alternativePersistencePackageESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE__OBJECT:
			if (object != null)
				msgs = ((InternalEObject) object).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MUPDATE__OBJECT,
						null, msgs);
			return basicSetObject((MUpdateObject) otherEnd, msgs);
		case AnnotationsPackage.MUPDATE__VALUE:
			if (value != null)
				msgs = ((InternalEObject) value).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MUPDATE__VALUE,
						null, msgs);
			return basicSetValue((MUpdateValue) otherEnd, msgs);
		case AnnotationsPackage.MUPDATE__LOCATION:
			if (location != null)
				msgs = ((InternalEObject) location).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MUPDATE__LOCATION,
						null, msgs);
			return basicSetLocation((MUpdatePersistenceLocation) otherEnd,
					msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE__OBJECT:
			return basicUnsetObject(msgs);
		case AnnotationsPackage.MUPDATE__VALUE:
			return basicUnsetValue(msgs);
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE:
			return basicUnsetAlternativePersistencePackage(msgs);
		case AnnotationsPackage.MUPDATE__LOCATION:
			return basicUnsetLocation(msgs);
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT:
			return basicUnsetAlternativePersistenceRoot(msgs);
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE:
			return basicUnsetAlternativePersistenceReference(msgs);
		case AnnotationsPackage.MUPDATE__MUPDATE_CONDITION:
			return basicUnsetMUpdateCondition(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE__OBJECT:
			if (resolve)
				return getObject();
			return basicGetObject();
		case AnnotationsPackage.MUPDATE__VALUE:
			if (resolve)
				return getValue();
			return basicGetValue();
		case AnnotationsPackage.MUPDATE__MODE:
			return getMode();
		case AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER:
			if (resolve)
				return getFeaturePackageFilter();
			return basicGetFeaturePackageFilter();
		case AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER:
			if (resolve)
				return getFeatureClassFilter();
			return basicGetFeatureClassFilter();
		case AnnotationsPackage.MUPDATE__FEATURE:
			if (resolve)
				return getFeature();
			return basicGetFeature();
		case AnnotationsPackage.MUPDATE__CONTAINING_PROPERTY_ANNOTATIONS:
			if (resolve)
				return getContainingPropertyAnnotations();
			return basicGetContainingPropertyAnnotations();
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE:
			if (resolve)
				return getAlternativePersistencePackage();
			return basicGetAlternativePersistencePackage();
		case AnnotationsPackage.MUPDATE__LOCATION:
			if (resolve)
				return getLocation();
			return basicGetLocation();
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT:
			if (resolve)
				return getAlternativePersistenceRoot();
			return basicGetAlternativePersistenceRoot();
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE:
			if (resolve)
				return getAlternativePersistenceReference();
			return basicGetAlternativePersistenceReference();
		case AnnotationsPackage.MUPDATE__MUPDATE_CONDITION:
			if (resolve)
				return getMUpdateCondition();
			return basicGetMUpdateCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE__OBJECT:
			setObject((MUpdateObject) newValue);
			return;
		case AnnotationsPackage.MUPDATE__VALUE:
			setValue((MUpdateValue) newValue);
			return;
		case AnnotationsPackage.MUPDATE__MODE:
			setMode((UpdateMode) newValue);
			return;
		case AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER:
			setFeaturePackageFilter((MPackage) newValue);
			return;
		case AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER:
			setFeatureClassFilter((MClassifier) newValue);
			return;
		case AnnotationsPackage.MUPDATE__FEATURE:
			setFeature((MProperty) newValue);
			return;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE:
			setAlternativePersistencePackage(
					(MUpdateAlternativePersistencePackage) newValue);
			return;
		case AnnotationsPackage.MUPDATE__LOCATION:
			setLocation((MUpdatePersistenceLocation) newValue);
			return;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT:
			setAlternativePersistenceRoot(
					(MUpdateAlternativePersistenceRoot) newValue);
			return;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE:
			setAlternativePersistenceReference(
					(MUpdateAlternativePersistenceReference) newValue);
			return;
		case AnnotationsPackage.MUPDATE__MUPDATE_CONDITION:
			setMUpdateCondition((MUpdateCondition) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE__OBJECT:
			unsetObject();
			return;
		case AnnotationsPackage.MUPDATE__VALUE:
			unsetValue();
			return;
		case AnnotationsPackage.MUPDATE__MODE:
			unsetMode();
			return;
		case AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER:
			unsetFeaturePackageFilter();
			return;
		case AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER:
			unsetFeatureClassFilter();
			return;
		case AnnotationsPackage.MUPDATE__FEATURE:
			unsetFeature();
			return;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE:
			unsetAlternativePersistencePackage();
			return;
		case AnnotationsPackage.MUPDATE__LOCATION:
			unsetLocation();
			return;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT:
			unsetAlternativePersistenceRoot();
			return;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE:
			unsetAlternativePersistenceReference();
			return;
		case AnnotationsPackage.MUPDATE__MUPDATE_CONDITION:
			unsetMUpdateCondition();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE__OBJECT:
			return isSetObject();
		case AnnotationsPackage.MUPDATE__VALUE:
			return isSetValue();
		case AnnotationsPackage.MUPDATE__MODE:
			return isSetMode();
		case AnnotationsPackage.MUPDATE__FEATURE_PACKAGE_FILTER:
			return isSetFeaturePackageFilter();
		case AnnotationsPackage.MUPDATE__FEATURE_CLASS_FILTER:
			return isSetFeatureClassFilter();
		case AnnotationsPackage.MUPDATE__FEATURE:
			return isSetFeature();
		case AnnotationsPackage.MUPDATE__CONTAINING_PROPERTY_ANNOTATIONS:
			return basicGetContainingPropertyAnnotations() != null;
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE:
			return isSetAlternativePersistencePackage();
		case AnnotationsPackage.MUPDATE__LOCATION:
			return isSetLocation();
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT:
			return isSetAlternativePersistenceRoot();
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE:
			return isSetAlternativePersistenceReference();
		case AnnotationsPackage.MUPDATE__MUPDATE_CONDITION:
			return isSetMUpdateCondition();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mode: ");
		if (modeESet)
			result.append(mode);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 'update LHS. '.concat(
	if self.feature.oclIsUndefined() then 'FEATURE MISSING' else self.feature.eName endif).concat(
	if self.mode=updates::UpdateMode::Redefine then ' := RHS'
	else if self.mode=updates::UpdateMode::Add then ' += RHS'
	else if self.mode=updates::UpdateMode::Remove then ' -= RHS' else ' ERROR UPDATE MODE '
	endif endif endif
	)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MUPDATE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Update'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				AnnotationsPackage.Literals.MUPDATE__MODE,
				McorePackage.Literals.MNAMED__NAME,
				McorePackage.Literals.MNAMED__SHORT_NAME };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MUPDATE,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, oclText,
						helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MUpdateImpl
