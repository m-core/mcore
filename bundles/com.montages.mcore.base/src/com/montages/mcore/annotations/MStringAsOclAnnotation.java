/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MString As Ocl Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectType <em>Self Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectMandatory <em>Self Object Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectSingular <em>Self Object Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getMStringAsOclVariable <em>MString As Ocl Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclAnnotation()
 * @model annotation="http://www.xocl.org/OCL label='\'<stringAsOcl>\''"
 * @generated
 */

public interface MStringAsOclAnnotation extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Self Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Self Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Self Object Type</em>' reference.
	 * @see #isSetSelfObjectType()
	 * @see #unsetSelfObjectType()
	 * @see #setSelfObjectType(MClassifier)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclAnnotation_SelfObjectType()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	MClassifier getSelfObjectType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectType <em>Self Object Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Self Object Type</em>' reference.
	 * @see #isSetSelfObjectType()
	 * @see #unsetSelfObjectType()
	 * @see #getSelfObjectType()
	 * @generated
	 */
	void setSelfObjectType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectType <em>Self Object Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSelfObjectType()
	 * @see #getSelfObjectType()
	 * @see #setSelfObjectType(MClassifier)
	 * @generated
	 */
	void unsetSelfObjectType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectType <em>Self Object Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Self Object Type</em>' reference is set.
	 * @see #unsetSelfObjectType()
	 * @see #getSelfObjectType()
	 * @see #setSelfObjectType(MClassifier)
	 * @generated
	 */
	boolean isSetSelfObjectType();

	/**
	 * Returns the value of the '<em><b>Self Object Mandatory</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Self Object Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Self Object Mandatory</em>' attribute.
	 * @see #isSetSelfObjectMandatory()
	 * @see #unsetSelfObjectMandatory()
	 * @see #setSelfObjectMandatory(Boolean)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclAnnotation_SelfObjectMandatory()
	 * @model default="false" unsettable="true"
	 * @generated
	 */
	Boolean getSelfObjectMandatory();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectMandatory <em>Self Object Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Self Object Mandatory</em>' attribute.
	 * @see #isSetSelfObjectMandatory()
	 * @see #unsetSelfObjectMandatory()
	 * @see #getSelfObjectMandatory()
	 * @generated
	 */
	void setSelfObjectMandatory(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectMandatory <em>Self Object Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSelfObjectMandatory()
	 * @see #getSelfObjectMandatory()
	 * @see #setSelfObjectMandatory(Boolean)
	 * @generated
	 */
	void unsetSelfObjectMandatory();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectMandatory <em>Self Object Mandatory</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Self Object Mandatory</em>' attribute is set.
	 * @see #unsetSelfObjectMandatory()
	 * @see #getSelfObjectMandatory()
	 * @see #setSelfObjectMandatory(Boolean)
	 * @generated
	 */
	boolean isSetSelfObjectMandatory();

	/**
	 * Returns the value of the '<em><b>Self Object Singular</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Self Object Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Self Object Singular</em>' attribute.
	 * @see #isSetSelfObjectSingular()
	 * @see #unsetSelfObjectSingular()
	 * @see #setSelfObjectSingular(Boolean)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclAnnotation_SelfObjectSingular()
	 * @model default="false" unsettable="true"
	 * @generated
	 */
	Boolean getSelfObjectSingular();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectSingular <em>Self Object Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Self Object Singular</em>' attribute.
	 * @see #isSetSelfObjectSingular()
	 * @see #unsetSelfObjectSingular()
	 * @see #getSelfObjectSingular()
	 * @generated
	 */
	void setSelfObjectSingular(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectSingular <em>Self Object Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSelfObjectSingular()
	 * @see #getSelfObjectSingular()
	 * @see #setSelfObjectSingular(Boolean)
	 * @generated
	 */
	void unsetSelfObjectSingular();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getSelfObjectSingular <em>Self Object Singular</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Self Object Singular</em>' attribute is set.
	 * @see #unsetSelfObjectSingular()
	 * @see #getSelfObjectSingular()
	 * @see #setSelfObjectSingular(Boolean)
	 * @generated
	 */
	boolean isSetSelfObjectSingular();

	/**
	 * Returns the value of the '<em><b>MString As Ocl Variable</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MStringAsOclVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MString As Ocl Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MString As Ocl Variable</em>' containment reference list.
	 * @see #isSetMStringAsOclVariable()
	 * @see #unsetMStringAsOclVariable()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMStringAsOclAnnotation_MStringAsOclVariable()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MStringAsOclVariable> getMStringAsOclVariable();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getMStringAsOclVariable <em>MString As Ocl Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMStringAsOclVariable()
	 * @see #getMStringAsOclVariable()
	 * @generated
	 */
	void unsetMStringAsOclVariable();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MStringAsOclAnnotation#getMStringAsOclVariable <em>MString As Ocl Variable</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MString As Ocl Variable</em>' containment reference list is set.
	 * @see #unsetMStringAsOclVariable()
	 * @see #getMStringAsOclVariable()
	 * @generated
	 */
	boolean isSetMStringAsOclVariable();

} // MStringAsOclAnnotation
