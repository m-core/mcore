/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLayout Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInTable <em>Hide In Table</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInPropertyView <em>Hide In Property View</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMLayoutAnnotation()
 * @model annotation="http://www.xocl.org/OCL label='\'layout = ...\'\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Layout\'\n'"
 * @generated
 */

public interface MLayoutAnnotation extends MAbstractAnnotion {
	/**
	 * Returns the value of the '<em><b>Hide In Table</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide In Table</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide In Table</em>' attribute.
	 * @see #isSetHideInTable()
	 * @see #unsetHideInTable()
	 * @see #setHideInTable(Boolean)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMLayoutAnnotation_HideInTable()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true\n'"
	 * @generated
	 */
	Boolean getHideInTable();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInTable <em>Hide In Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide In Table</em>' attribute.
	 * @see #isSetHideInTable()
	 * @see #unsetHideInTable()
	 * @see #getHideInTable()
	 * @generated
	 */
	void setHideInTable(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInTable <em>Hide In Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHideInTable()
	 * @see #getHideInTable()
	 * @see #setHideInTable(Boolean)
	 * @generated
	 */
	void unsetHideInTable();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInTable <em>Hide In Table</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Hide In Table</em>' attribute is set.
	 * @see #unsetHideInTable()
	 * @see #getHideInTable()
	 * @see #setHideInTable(Boolean)
	 * @generated
	 */
	boolean isSetHideInTable();

	/**
	 * Returns the value of the '<em><b>Hide In Property View</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide In Property View</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide In Property View</em>' attribute.
	 * @see #isSetHideInPropertyView()
	 * @see #unsetHideInPropertyView()
	 * @see #setHideInPropertyView(Boolean)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMLayoutAnnotation_HideInPropertyView()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true\n'"
	 * @generated
	 */
	Boolean getHideInPropertyView();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInPropertyView <em>Hide In Property View</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide In Property View</em>' attribute.
	 * @see #isSetHideInPropertyView()
	 * @see #unsetHideInPropertyView()
	 * @see #getHideInPropertyView()
	 * @generated
	 */
	void setHideInPropertyView(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInPropertyView <em>Hide In Property View</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHideInPropertyView()
	 * @see #getHideInPropertyView()
	 * @see #setHideInPropertyView(Boolean)
	 * @generated
	 */
	void unsetHideInPropertyView();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MLayoutAnnotation#getHideInPropertyView <em>Hide In Property View</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Hide In Property View</em>' attribute is set.
	 * @see #unsetHideInPropertyView()
	 * @see #getHideInPropertyView()
	 * @see #setHideInPropertyView(Boolean)
	 * @generated
	 */
	boolean isSetHideInPropertyView();

} // MLayoutAnnotation
