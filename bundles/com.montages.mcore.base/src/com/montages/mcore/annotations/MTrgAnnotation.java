/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTrg Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMTrgAnnotation()
 * @model
 * @generated
 */

public interface MTrgAnnotation extends MExprAnnotation {
} // MTrgAnnotation
