
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdateCondition()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'CONDITION\'\n' objectObjectTypeDerive='if (not eContainer().oclIsUndefined()) then\r\nlet class: MClassifier = \r\nself.eContainer().oclAsType(MUpdate).feature.containingClassifier \r\nin  if class.oclIsUndefined() then null else class endif\r\n\r\nelse null endif' expectedReturnSimpleTypeDerive='mcore::SimpleType::Boolean\n'"
 * @generated
 */

public interface MUpdateCondition extends MUpdateValue {
} // MUpdateCondition
