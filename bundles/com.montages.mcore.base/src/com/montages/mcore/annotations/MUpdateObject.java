/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MUpdateObject#getContainingUpdateAnnotation <em>Containing Update Annotation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdateObject()
 * @model annotation="http://www.xocl.org/OCL label='\'\'\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'LHS Object\'\n' containingAbstractPropertyAnnotationsDerive='self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)' targetObjectTypeOfAnnotationDerive='let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedType\nendif endif\n' targetSimpleTypeOfAnnotationDerive='let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedSimpleType\nendif endif\n' objectObjectTypeOfAnnotationDerive='if containingUpdateAnnotation.feature.oclIsUndefined()\n  then null\n  else containingUpdateAnnotation.feature.containingClassifier\nendif\n'"
 * @generated
 */

public interface MUpdateObject extends MTrgAnnotation {

	/**
	 * Returns the value of the '<em><b>Containing Update Annotation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mcore.annotations.MUpdate#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Update Annotation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Update Annotation</em>' container reference.
	 * @see #setContainingUpdateAnnotation(MUpdate)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdateObject_ContainingUpdateAnnotation()
	 * @see com.montages.mcore.annotations.MUpdate#getObject
	 * @model opposite="object" unsettable="true" transient="false"
	 *        annotation="http://www.montages.com/mCore/MCore mName='containingUpdateAnnotation'"
	 * @generated
	 */
	MUpdate getContainingUpdateAnnotation();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MUpdateObject#getContainingUpdateAnnotation <em>Containing Update Annotation</em>}' container reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing Update Annotation</em>' container reference.
	 * @see #getContainingUpdateAnnotation()
	 * @generated
	 */

	void setContainingUpdateAnnotation(MUpdate value);
} // MUpdateObject
