/**
 */
package com.montages.mcore;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MExplicitly Typed And Named</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MExplicitlyTypedAndNamed#getAppendTypeNameToName <em>Append Type Name To Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMExplicitlyTypedAndNamed()
 * @model abstract="true"
 * @generated
 */

public interface MExplicitlyTypedAndNamed extends MNamed, MExplicitlyTyped {
	/**
	 * Returns the value of the '<em><b>Append Type Name To Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Append Type Name To Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Append Type Name To Name</em>' attribute.
	 * @see #isSetAppendTypeNameToName()
	 * @see #unsetAppendTypeNameToName()
	 * @see #setAppendTypeNameToName(Boolean)
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTypedAndNamed_AppendTypeNameToName()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	Boolean getAppendTypeNameToName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MExplicitlyTypedAndNamed#getAppendTypeNameToName <em>Append Type Name To Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Append Type Name To Name</em>' attribute.
	 * @see #isSetAppendTypeNameToName()
	 * @see #unsetAppendTypeNameToName()
	 * @see #getAppendTypeNameToName()
	 * @generated
	 */

	void setAppendTypeNameToName(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MExplicitlyTypedAndNamed#getAppendTypeNameToName <em>Append Type Name To Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAppendTypeNameToName()
	 * @see #getAppendTypeNameToName()
	 * @see #setAppendTypeNameToName(Boolean)
	 * @generated
	 */
	void unsetAppendTypeNameToName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MExplicitlyTypedAndNamed#getAppendTypeNameToName <em>Append Type Name To Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Append Type Name To Name</em>' attribute is set.
	 * @see #unsetAppendTypeNameToName()
	 * @see #getAppendTypeNameToName()
	 * @see #setAppendTypeNameToName(Boolean)
	 * @generated
	 */
	boolean isSetAppendTypeNameToName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if not type.oclIsUndefined() \r\n  then type.calculatedName\r\n  else if  self.simpleType <> SimpleType::None\r\n      then self.simpleType.toString().concat(\' Property\')\r\n      else \' ERROR UNNAMED UNTYPED\' endif endif'"
	 * @generated
	 */
	String nameFromType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if not type.oclIsUndefined() \r\n  then type.calculatedShortName\r\n  else if  self.simpleType <> SimpleType::None\r\n      then self.simpleType.toString().concat(\' Property\')\r\n      else \' ERROR UNNAMED UNTYPED\' endif endif'"
	 * @generated
	 */
	String shortNameFromType();

} // MExplicitlyTypedAndNamed
