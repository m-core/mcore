/**
 */
package com.montages.mcore;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import com.montages.mcore.annotations.MGeneralAnnotation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MModel Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MModelElement#getGeneralAnnotation <em>General Annotation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMModelElement()
 * @model abstract="true"
 * @generated
 */

public interface MModelElement extends EObject {
	/**
	 * Returns the value of the '<em><b>General Annotation</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MGeneralAnnotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>General Annotation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>General Annotation</em>' containment reference list.
	 * @see #isSetGeneralAnnotation()
	 * @see #unsetGeneralAnnotation()
	 * @see com.montages.mcore.McorePackage#getMModelElement_GeneralAnnotation()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MGeneralAnnotation> getGeneralAnnotation();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MModelElement#getGeneralAnnotation <em>General Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGeneralAnnotation()
	 * @see #getGeneralAnnotation()
	 * @generated
	 */
	void unsetGeneralAnnotation();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MModelElement#getGeneralAnnotation <em>General Annotation</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>General Annotation</em>' containment reference list is set.
	 * @see #unsetGeneralAnnotation()
	 * @see #getGeneralAnnotation()
	 * @generated
	 */
	boolean isSetGeneralAnnotation();

} // MModelElement
