/**
 */
package com.montages.mcore;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MClassifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MClassifier#getAbstractClass <em>Abstract Class</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getEnforcedClass <em>Enforced Class</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getSuperTypePackage <em>Super Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getInstance <em>Instance</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getKind <em>Kind</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getInternalEClassifier <em>Internal EClassifier</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getContainingPackage <em>Containing Package</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getRepresentsCoreType <em>Represents Core Type</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getAllPropertyGroups <em>All Property Groups</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getLiteral <em>Literal</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getEInterface <em>EInterface</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getOperationAsIdPropertyError <em>Operation As Id Property Error</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIsClassByStructure <em>Is Class By Structure</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getToggleSuperType <em>Toggle Super Type</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getOrderingStrategy <em>Ordering Strategy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getTestAllProperties <em>Test All Properties</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIdProperty <em>Id Property</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getResolveContainerAction <em>Resolve Container Action</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getAbstractDoAction <em>Abstract Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getObjectReferences <em>Object References</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getNearestInstance <em>Nearest Instance</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getStrictNearestInstance <em>Strict Nearest Instance</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getStrictInstance <em>Strict Instance</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIntelligentNearestInstance <em>Intelligent Nearest Instance</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getObjectUnreferenced <em>Object Unreferenced</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getOutstandingToCopyObjects <em>Outstanding To Copy Objects</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getPropertyInstances <em>Property Instances</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getObjectReferencePropertyInstances <em>Object Reference Property Instances</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIntelligentInstance <em>Intelligent Instance</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getContainmentHierarchy <em>Containment Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getStrictAllClassesContainedIn <em>Strict All Classes Contained In</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getStrictContainmentHierarchy <em>Strict Containment Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIntelligentAllClassesContainedIn <em>Intelligent All Classes Contained In</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIntelligentContainmentHierarchy <em>Intelligent Containment Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getInstantiationHierarchy <em>Instantiation Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getInstantiationPropertyHierarchy <em>Instantiation Property Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getIntelligentInstantiationPropertyHierarchy <em>Intelligent Instantiation Property Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getClassescontainedin <em>Classescontainedin</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getCanApplyDefaultContainment <em>Can Apply Default Containment</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getPropertiesAsText <em>Properties As Text</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getSemanticsAsText <em>Semantics As Text</em>}</li>
 *   <li>{@link com.montages.mcore.MClassifier#getDerivedJsonSchema <em>Derived Json Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMClassifier()
 * @model annotation="http://www.xocl.org/OCL label='eName.concat(\r\n\tif self.superType->isEmpty() \r\n    then \'\'\r\n    else \'->\'.concat(\r\n    \tif superType->size() = 1 \r\n    \tthen superTypesAsString()\r\n    \telse \'(\'.concat(superTypesAsString()).concat(\')\')\r\n    \tendif\r\n    ) endif\r\n)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL fullLabelDerive='(if kind=ClassifierKind::ClassType then \'[class] \' else\r\nif kind=ClassifierKind::DataType then  \'[data] \' else \r\nif kind=ClassifierKind::Enumeration then \'[enum]\' else\r\n\'[IMPOSSIBLE]\'endif endif endif)\r\n.concat(\'localstructuralname\')' kindLabelDerive='if self.kind = mcore::ClassifierKind::ClassType then\r\n  if self.eInterface=true then \'Interface\' else\r\n  if self.abstractClass = true then \'Abstract Class\' else\r\n       \'Class\' endif endif else\r\nif self.kind = mcore::ClassifierKind::DataType then \'Data Type\' else\r\nif self.kind = mcore::ClassifierKind::Enumeration then \'Enumeration\' else\r\n\'ERROR\' endif endif endif' correctNameDerive='(not ambiguousClassifierName()) and\r\nif self.hasSimpleModelingType then self.name=self.simpleTypeAsString(simpleType) else  not self.stringEmpty(self.name) endif' containingClassifierDerive='self' simpleTypeIsCorrectDerive='if self.kind = ClassifierKind::DataType then \r\n  self.hasSimpleDataType\r\nelse if self.kind = ClassifierKind::Enumeration then self.simpleType=SimpleType::None \r\nelse self.simpleType=SimpleType::None or \r\nself.hasSimpleModelingType endif endif' eNameDerive='if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen if hasSimpleModelingType then simpleTypeString else\r\n\tif stringEmpty(containingPackage.derivedNamePrefix)\r\n\tthen calculatedShortName.camelCaseUpper() \r\n\telse containingPackage.derivedNamePrefix.concat(calculatedShortName).camelCaseUpper()\r\n\tendif endif\r\nelse specialEName.camelCaseUpper()\r\nendif' localStructuralNameDerive='containingPackage.localStructuralName\r\n.concat(\'/\')\r\n.concat(self.eName)' calculatedNameDerive='name.concat(if ambiguousClassifierName() then \'  (AMBIGUOUS)\' else \'\' endif)' simpleTypeInitValue='let stringType: SimpleType = SimpleType::None in  stringType' propertyInitValue='OrderedSet{ defaultPropertyValue() }'"
 * @generated
 */

public interface MClassifier extends MPropertiesContainer, MHasSimpleType {
	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MClassifierAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MClassifierAction
	 * @see #setDoAction(MClassifierAction)
	 * @see com.montages.mcore.McorePackage#getMClassifier_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::MClassifierAction::Do\n' choiceConstruction='let classActions:OrderedSet(MClassifierAction) =\n  if self.literal->isEmpty() \n      then OrderedSet{MClassifierAction::CreateInstance, if abstractDoAction then MClassifierAction::Abstract else null endif, MClassifierAction::Specialize,\n      MClassifierAction::StringAttribute, MClassifierAction::BooleanAttribute, MClassifierAction::IntegerAttribute, if not self.containingPackage.classifier->select( not literal->isEmpty())->isEmpty() then MClassifierAction::LiteralAttribute else null endif, MClassifierAction::RealAttribute, MClassifierAction::DateAttribute, MClassifierAction::UnaryReference, MClassifierAction::NaryReference,MClassifierAction::UnaryContainment, MClassifierAction::NaryContainment,MClassifierAction::OperationOneParameter, MClassifierAction::OperationTwoParameters, MClassifierAction::CompleteSemantics} \n      else OrderedSet{} endif in\n let withLabelAndConstraintAction:OrderedSet(MClassifierAction) =\n    if self.literal->isEmpty() then\n       if self.annotations.oclIsUndefined() \n          then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)\n        else if self.annotations.label.oclIsUndefined()\n          then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)\n          else  classActions->prepend(MClassifierAction::Constraint) endif endif \n    else classActions endif in\n      let intoAbstractOrConcrete: OrderedSet(MClassifierAction) =\n if self.literal->isEmpty() and (self.ownedProperty->notEmpty() or self.enforcedClass=true)\n   then withLabelAndConstraintAction->prepend(MClassifierAction::GroupOfProperties)\n   else  withLabelAndConstraintAction->append(MClassifierAction::Literal) endif in\n\tlet resolveContainer:OrderedSet(MClassifierAction) =\n   if self.literal->isEmpty()\n    then if self.abstractClass = true\n \t then intoAbstractOrConcrete->prepend(MClassifierAction::IntoConcreteClass)->prepend(MClassifierAction::Do) \n\t else intoAbstractOrConcrete->prepend(MClassifierAction::IntoAbstractClass)->prepend(MClassifierAction::Do) \n endif\n else intoAbstractOrConcrete->prepend(MClassifierAction::Do)\n endif in\n     if self.resolveContainerAction = true\n\tthen if resolveContainer->includes(MClassifierAction::Label)\n\t\tthen resolveContainer->insertAt(8, MClassifierAction::ResolveIntoContainer)\n\t\telse resolveContainer->insertAt(7, MClassifierAction::ResolveIntoContainer)\n\t\tendif\n\telse resolveContainer\n\tendif\n\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MClassifierAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MClassifierAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MClassifierAction value);

	/**
	 * Returns the value of the '<em><b>Resolve Container Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolve Container Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolve Container Action</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_ResolveContainerAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let var1: OrderedSet(mcore::MProperty)  = let chain: OrderedSet(mcore::MProperty)  = classescontainedin.allFeaturesWithStorage()->asOrderedSet() in\nchain->iterate(i:mcore::MProperty; r: OrderedSet(mcore::MProperty)=OrderedSet{} | if i.oclIsKindOf(mcore::MProperty) then r->including(i.oclAsType(mcore::MProperty))->asOrderedSet() \n else r endif)->select(it: mcore::MProperty | let e0: Boolean = (let e0: Boolean = it.containment = true in \n if e0.oclIsInvalid() then null else e0 endif) and (let e0: Boolean = it.calculatedSingular = true in \n if e0.oclIsInvalid() then null else e0 endif) and (let e0: Boolean = it.type = containingClassifier in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet resultofnull: Boolean = if (let e0: Boolean = let e0: Boolean = allFeaturesWithStorage()->asOrderedSet()->includesAll(allProperties()->asOrderedSet())   in \n if e0.oclIsInvalid() then null else e0 endif and let chain01: OrderedSet(mcore::MClassifier)  = classescontainedin->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif and let chain02: OrderedSet(mcore::MClassifier)  = allDirectSubTypes()->asOrderedSet() in\nif chain02->isEmpty().oclIsUndefined() \n then null \n else chain02->isEmpty()\n  endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: OrderedSet(mcore::MProperty)  = var1 in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif\n  else false\nendif in\nresultofnull\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getResolveContainerAction();

	/**
	 * Returns the value of the '<em><b>Abstract Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Do Action</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_AbstractDoAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let var1: OrderedSet(mcore::MClassifier)  = allSuperTypes()->asOrderedSet()->select(it: mcore::MClassifier | let e0: Boolean = let chain01: Boolean = it.abstractClass in\nif chain01 = true then true else false \n  endif and (let e0: Boolean = it.name = (let e0: String = \'Abstract \'.concat(name) in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet resultofAbstractDoAction: Boolean = if (abstractClass) \n  =true \nthen false\n  else let chain: OrderedSet(mcore::MClassifier)  = var1 in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif\nendif in\nresultofAbstractDoAction\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAbstractDoAction();

	/**
	 * Returns the value of the '<em><b>Object References</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object References</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object References</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_ObjectReferences()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='object References'"
	 *        annotation="http://www.xocl.org/OCL derive='let var1: OrderedSet(mcore::objects::MObject)  = intelligentInstance->asOrderedSet()->select(it: mcore::objects::MObject | let chain: OrderedSet(mcore::objects::MObjectReference)  = it.referencedBy->asOrderedSet() in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (canApplyDefaultContainment) \n  =true \nthen var1\n  else OrderedSet{}\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getObjectReferences();

	/**
	 * Returns the value of the '<em><b>Nearest Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nearest Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nearest Instance</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_NearestInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='nearest Instance'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{}\r\nelse\r\nif allClassesContainedIn()->isEmpty() then\r\nMClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).instance\r\nelse\r\n if allClassesContainedIn().instance->isEmpty() then\r\nallClassesContainedIn()->first().nearestInstance\r\nelse\r\nallClassesContainedIn().instance\r\nendif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getNearestInstance();

	/**
	 * Returns the value of the '<em><b>Strict Nearest Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict Nearest Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict Nearest Instance</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_StrictNearestInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='strict Nearest Instance'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\nif strictAllClassesContainedIn->isEmpty() then\r\nMClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).strictInstance\r\n-- subpackages dont have resourceRootClass set, change that, saves runtime\r\nelse\r\n if strictAllClassesContainedIn.instance->isEmpty() then\r\nstrictAllClassesContainedIn->first().strictNearestInstance\r\nelse\r\nstrictAllClassesContainedIn.strictInstance\r\nendif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getStrictNearestInstance();

	/**
	 * Returns the value of the '<em><b>Strict Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict Instance</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_StrictInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='strict Instance'"
	 *        annotation="http://www.xocl.org/OCL derive='let var1: OrderedSet(mcore::objects::MObject)  = instance->asOrderedSet()->select(it: mcore::objects::MObject | let e0: Boolean = it.type = self in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (canApplyDefaultContainment) \n  =true \nthen var1\n  else OrderedSet{}\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getStrictInstance();

	/**
	 * Returns the value of the '<em><b>Intelligent Nearest Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intelligent Nearest Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intelligent Nearest Instance</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_IntelligentNearestInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='intelligent Nearest Instance'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment= false then OrderedSet{}\r\nelse\r\nif intelligentAllClassesContainedIn->isEmpty()  then\r\nMClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).intelligentInstance \r\n--subpackages don\'t have resourceRootClass set ,  change that  => saves runtime here\r\nelse\r\nif strictNearestInstance->isEmpty() then\r\n  if nearestInstance->isEmpty() then\r\n  intelligentAllClassesContainedIn->first().intelligentNearestInstance\r\n  else \r\n  nearestInstance\r\n  endif \r\n  \r\n else strictNearestInstance\r\n endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getIntelligentNearestInstance();

	/**
	 * Returns the value of the '<em><b>Object Unreferenced</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Unreferenced</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Unreferenced</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_ObjectUnreferenced()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='object Unreferenced'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\nif  intelligentInstance->isEmpty() then OrderedSet{}\r\nelse if objectReferences->isEmpty() then intelligentInstance\r\nelse intelligentInstance->symmetricDifference(objectReferences) endif endif\r\n--add symmetric diff to MRules\r\nendif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getObjectUnreferenced();

	/**
	 * Returns the value of the '<em><b>Outstanding To Copy Objects</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outstanding To Copy Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outstanding To Copy Objects</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_OutstandingToCopyObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='outstanding To Copy Objects'"
	 *        annotation="http://www.xocl.org/OCL derive='objectUnreferenced->asOrderedSet()->select(it: mcore::objects::MObject | let e0: Boolean = intelligentNearestInstance->asOrderedSet()->excludes(it.containingObject)   in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() \n\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getOutstandingToCopyObjects();

	/**
	 * Returns the value of the '<em><b>Property Instances</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MPropertyInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Instances</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_PropertyInstances()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='property Instances'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\nlet os:OrderedSet(objects::MPropertyInstance) = \r\n\tobjects::MPropertyInstance.allInstances()->asOrderedSet()\r\n\t--try to avoid allInstances\r\nin\r\nif os->isEmpty() then\r\n\tOrderedSet{} \r\nelse\r\n\tos->select(x:objects::MPropertyInstance| not( x.property.oclIsUndefined()) and x.property.simpleType = SimpleType::None and x.property.type = self)->asOrderedSet()\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyInstance> getPropertyInstances();

	/**
	 * Returns the value of the '<em><b>Object Reference Property Instances</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MPropertyInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Reference Property Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Reference Property Instances</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_ObjectReferencePropertyInstances()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='object Reference Property Instances'"
	 *        annotation="http://www.xocl.org/OCL derive='if propertyInstances->isEmpty() then OrderedSet{} else\npropertyInstances->asOrderedSet()->select(it: mcore::objects::MPropertyInstance | let chain: OrderedSet(mcore::objects::MObjectReference)  = it.internalReferencedObject->asOrderedSet() in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif)->asOrderedSet()->excluding(null)->asOrderedSet() \n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyInstance> getObjectReferencePropertyInstances();

	/**
	 * Returns the value of the '<em><b>Intelligent Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intelligent Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intelligent Instance</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_IntelligentInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='intelligent Instance'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\nif self.strictInstance->isEmpty() and self.instance->isEmpty() then OrderedSet{}\r\nelse\r\nif self.strictInstance->isEmpty() then self.instance else\r\nlet s: OrderedSet(MClassifier) = self.strictInstance->first().type.intelligentContainmentHierarchy in \r\nif self.instance->forAll(type.intelligentContainmentHierarchy = s ) then  instance\r\nelse \r\nself.strictInstance\r\nendif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Object Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getIntelligentInstance();

	/**
	 * Returns the value of the '<em><b>Containment Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containment Hierarchy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment Hierarchy</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_ContainmentHierarchy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='containmentHierarchy'"
	 *        annotation="http://www.xocl.org/OCL derive=' if allClassesContainedIn()->isEmpty() then OrderedSet{}  --isRoot\r\nelse\r\nif allClassesContainedIn()->first().allClassesContainedIn()->excludes(self) then\r\nallClassesContainedIn()->first()->asOrderedSet()->union(allClassesContainedIn()->first().containmentHierarchy)\r\nelse\r\nOrderedSet{} -- Circle\r\nendif\r\nendif \r\n\r\n  '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getContainmentHierarchy();

	/**
	 * Returns the value of the '<em><b>Strict All Classes Contained In</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict All Classes Contained In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict All Classes Contained In</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_StrictAllClassesContainedIn()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='strict All Classes Contained In'"
	 *        annotation="http://www.xocl.org/OCL derive='\r\nallClassesContainedIn().property\r\n     ->select(p:MProperty| p.type=self.containingClassifier).containingClassifier->asSet()\r\n\r\n     --Need custom OCL because of Set'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getStrictAllClassesContainedIn();

	/**
	 * Returns the value of the '<em><b>Strict Containment Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict Containment Hierarchy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict Containment Hierarchy</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_StrictContainmentHierarchy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='strict Containment Hierarchy'"
	 *        annotation="http://www.xocl.org/OCL derive='if self.canApplyDefaultContainment then\r\nif strictAllClassesContainedIn->isEmpty() then OrderedSet{null}  --isRoot or non existing\r\nelse\r\nstrictAllClassesContainedIn->first()->asSequence()->union(strictAllClassesContainedIn->first().strictContainmentHierarchy->asSequence())\r\nendif\r\n--need custom OCL because of asSequence\r\nelse\r\nOrderedSet{}endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getStrictContainmentHierarchy();

	/**
	 * Returns the value of the '<em><b>Intelligent All Classes Contained In</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intelligent All Classes Contained In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intelligent All Classes Contained In</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_IntelligentAllClassesContainedIn()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='intelligent All Classes Contained In'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let chain: OrderedSet(mcore::MClassifier)  = strictAllClassesContainedIn->asOrderedSet() in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen allClassesContainedIn()->asOrderedSet()\n  else strictAllClassesContainedIn->asOrderedSet()\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getIntelligentAllClassesContainedIn();

	/**
	 * Returns the value of the '<em><b>Intelligent Containment Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intelligent Containment Hierarchy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intelligent Containment Hierarchy</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_IntelligentContainmentHierarchy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='intelligent Containment Hierarchy'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment then\r\nif strictAllClassesContainedIn->isEmpty() and allClassesContainedIn()->isEmpty()\r\nthen OrderedSet{null}  --isRoot or non existing\r\nelse\r\nif  strictAllClassesContainedIn->isEmpty() then\r\nallClassesContainedIn()->first()->asSequence()->union(allClassesContainedIn()->first().intelligentContainmentHierarchy->asSequence())\r\nelse \r\nstrictAllClassesContainedIn->first()->asSequence()->union(strictAllClassesContainedIn->first().intelligentContainmentHierarchy->asSequence())\r\nendif endif\r\nelse\r\nOrderedSet{} endif\r\n\r\n-- Need sequence here again => Custom OCL'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getIntelligentContainmentHierarchy();

	/**
	 * Returns the value of the '<em><b>Instantiation Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instantiation Hierarchy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instantiation Hierarchy</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_InstantiationHierarchy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='instantiation Hierarchy'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\n\r\nlet s:Set(MClassifier) =\r\nintelligentContainmentHierarchy->asSet()->symmetricDifference(\r\nself.intelligentNearestInstance.type.intelligentContainmentHierarchy->asSet())\r\nin\r\nif s->isEmpty() then null else s\r\nendif endif\r\n--Custom OCL bc  symmetricDiff and Set'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getInstantiationHierarchy();

	/**
	 * Returns the value of the '<em><b>Instantiation Property Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instantiation Property Hierarchy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instantiation Property Hierarchy</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_InstantiationPropertyHierarchy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='instantiation Property Hierarchy'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\nif intelligentAllClassesContainedIn->isEmpty() then\r\nSequence{}\r\nelse\r\nlet s: Sequence(MProperty) = \r\nintelligentAllClassesContainedIn->first().property->asSequence() in\r\nif s->select(type = self)->isEmpty() then s->select(self.superType->includes(type)) else s->select(type = self) endif\r\n->asSequence()->union(intelligentAllClassesContainedIn->first().instantiationPropertyHierarchy->asSequence())\r\nendif endif\r\n\r\n--custom ocl bc  sequence '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MProperty> getInstantiationPropertyHierarchy();

	/**
	 * Returns the value of the '<em><b>Intelligent Instantiation Property Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intelligent Instantiation Property Hierarchy</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intelligent Instantiation Property Hierarchy</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_IntelligentInstantiationPropertyHierarchy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='intelligent Instantiation Property Hierarchy'"
	 *        annotation="http://www.xocl.org/OCL derive='if canApplyDefaultContainment = false then OrderedSet{} else\r\n\r\nintelligentNearestInstance.type.instantiationPropertyHierarchy->asOrderedSet()->iterate(it: mcore::MProperty;  acc: Sequence(MProperty) = self.instantiationPropertyHierarchy->asSequence()  |  if acc->includes(it)  then acc->excluding(it) else acc endif)->asSequence() endif\r\n\r\n--custom ocl bc sequence'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MProperty> getIntelligentInstantiationPropertyHierarchy();

	/**
	 * Returns the value of the '<em><b>Classescontainedin</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classescontainedin</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classescontainedin</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_Classescontainedin()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='classes contained in'"
	 *        annotation="http://www.xocl.org/OCL derive='allClassesContainedIn()->asOrderedSet()\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Classifier Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MClassifier> getClassescontainedin();

	/**
	 * Returns the value of the '<em><b>Can Apply Default Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Can Apply Default Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Can Apply Default Containment</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_CanApplyDefaultContainment()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='can Apply Default Containment'"
	 *        annotation="http://www.xocl.org/OCL derive='self.containmentHierarchy->notEmpty() and\r\nlet s: OrderedSet(MClassifier) = \r\n\r\nif strictAllClassesContainedIn->isEmpty() then allClassesContainedIn()\r\nelse strictAllClassesContainedIn\r\nendif  in \r\n\r\ns->size()=1\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Default Containment/Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getCanApplyDefaultContainment();

	/**
	 * Returns the value of the '<em><b>Properties As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties As Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties As Text</em>' attribute.
	 * @see #isSetPropertiesAsText()
	 * @see #unsetPropertiesAsText()
	 * @see #setPropertiesAsText(String)
	 * @see com.montages.mcore.McorePackage#getMClassifier_PropertiesAsText()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Summary As Text' createColumn='false'"
	 * @generated
	 */
	String getPropertiesAsText();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getPropertiesAsText <em>Properties As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Properties As Text</em>' attribute.
	 * @see #isSetPropertiesAsText()
	 * @see #unsetPropertiesAsText()
	 * @see #getPropertiesAsText()
	 * @generated
	 */
	void setPropertiesAsText(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getPropertiesAsText <em>Properties As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPropertiesAsText()
	 * @see #getPropertiesAsText()
	 * @see #setPropertiesAsText(String)
	 * @generated
	 */
	void unsetPropertiesAsText();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getPropertiesAsText <em>Properties As Text</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Properties As Text</em>' attribute is set.
	 * @see #unsetPropertiesAsText()
	 * @see #getPropertiesAsText()
	 * @see #setPropertiesAsText(String)
	 * @generated
	 */
	boolean isSetPropertiesAsText();

	/**
	 * Returns the value of the '<em><b>Semantics As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semantics As Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantics As Text</em>' attribute.
	 * @see #isSetSemanticsAsText()
	 * @see #unsetSemanticsAsText()
	 * @see #setSemanticsAsText(String)
	 * @see com.montages.mcore.McorePackage#getMClassifier_SemanticsAsText()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Summary As Text' createColumn='false'"
	 * @generated
	 */
	String getSemanticsAsText();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getSemanticsAsText <em>Semantics As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semantics As Text</em>' attribute.
	 * @see #isSetSemanticsAsText()
	 * @see #unsetSemanticsAsText()
	 * @see #getSemanticsAsText()
	 * @generated
	 */
	void setSemanticsAsText(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getSemanticsAsText <em>Semantics As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSemanticsAsText()
	 * @see #getSemanticsAsText()
	 * @see #setSemanticsAsText(String)
	 * @generated
	 */
	void unsetSemanticsAsText();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getSemanticsAsText <em>Semantics As Text</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Semantics As Text</em>' attribute is set.
	 * @see #unsetSemanticsAsText()
	 * @see #getSemanticsAsText()
	 * @see #setSemanticsAsText(String)
	 * @generated
	 */
	boolean isSetSemanticsAsText();

	/**
	 * Returns the value of the '<em><b>Derived Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Json Schema</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Json Schema</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_DerivedJsonSchema()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let lineBreak : String = \'\\n\' in\nlet space : String = \' \' in\nlet branch : String = \'{\' in\nlet definition : String = \'#/definitions/\' in\nlet closedBranch : String = \'}\' in\nlet mandatoryProperties : OrderedSet(MProperty)= self.property->asOrderedSet()->reject(e : MProperty| not(e.mandatory))in\nlet comma : String = \',\' in\n-- Classname\n\'\"\'.concat(self.eName).concat(\'\"\').concat(\':\').concat(branch)\n.concat(lineBreak)\n--Description\n.concat(\'\"description\": \').concat(\'\"\').concat(if self.description.oclIsUndefined() then \'no description\' else self.description endif).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n--Supertypes\n.concat(if self.superType->isEmpty() then \'\' else \'\"allOf\": [\'.concat(self.superType->iterate(classifier: MClassifier; s : String = \'\' | s.concat(\'{ \"$ref\": \').concat(\'\"\').concat(definition).concat(classifier.eName).concat(\'\"\').concat(\'}\').concat(if superType->indexOf(classifier) = self.superType->size() then \'\' else lineBreak.concat(\',\') endif))).concat(\']\').concat(comma).concat(lineBreak)endif)\n--Properties\n.concat(\'\"properties\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(self.property->iterate(property : MProperty;s : String= \'\' | s.concat(property.derivedJsonSchema).concat(if self.property->indexOf(property) = self.property->size() then \'\'.concat(lineBreak) else \',\'.concat(lineBreak) endif)))\n.concat(lineBreak)\n.concat(closedBranch)\n.concat(comma)\n.concat(lineBreak)\n-- Required fields\n.concat(\'\"required\": [\').concat(mandatoryProperties->iterate(property: MProperty;s : String = \'\'| if (property.mandatory) then s.concat(\'\"\').concat(property.eName).concat(\'\"\').concat(if mandatoryProperties->indexOf(property) = mandatoryProperties->size() then \'\' else \',\' endif) else s endif)).concat(\']\')\n.concat(closedBranch)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='JSON Schema' createColumn='false'"
	 * @generated
	 */
	String getDerivedJsonSchema();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate toggleSuperType$Update(MClassifier trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" trgRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate orderingStrategy$Update(OrderingStrategy trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MClassifierAction trg);

	/**
	 * Returns the value of the '<em><b>Super Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type Package</em>' reference.
	 * @see #isSetSuperTypePackage()
	 * @see #unsetSuperTypePackage()
	 * @see #setSuperTypePackage(MPackage)
	 * @see com.montages.mcore.McorePackage#getMClassifier_SuperTypePackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	MPackage getSuperTypePackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getSuperTypePackage <em>Super Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type Package</em>' reference.
	 * @see #isSetSuperTypePackage()
	 * @see #unsetSuperTypePackage()
	 * @see #getSuperTypePackage()
	 * @generated
	 */
	void setSuperTypePackage(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getSuperTypePackage <em>Super Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSuperTypePackage()
	 * @see #getSuperTypePackage()
	 * @see #setSuperTypePackage(MPackage)
	 * @generated
	 */
	void unsetSuperTypePackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getSuperTypePackage <em>Super Type Package</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Super Type Package</em>' reference is set.
	 * @see #unsetSuperTypePackage()
	 * @see #getSuperTypePackage()
	 * @see #setSuperTypePackage(MPackage)
	 * @generated
	 */
	boolean isSetSuperTypePackage();

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type</em>' reference list.
	 * @see #isSetSuperType()
	 * @see #unsetSuperType()
	 * @see com.montages.mcore.McorePackage#getMClassifier_SuperType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='  not (trg = self) \r\nand\r\n  trg.kind = ClassifierKind::ClassType\r\nand\r\n  trg.selectableClassifier(superType, superTypePackage)\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	EList<MClassifier> getSuperType();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getSuperType <em>Super Type</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSuperType()
	 * @see #getSuperType()
	 * @generated
	 */
	void unsetSuperType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getSuperType <em>Super Type</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Super Type</em>' reference list is set.
	 * @see #unsetSuperType()
	 * @see #getSuperType()
	 * @generated
	 */
	boolean isSetSuperType();

	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_Instance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let os:OrderedSet(objects::MObject) = \r\n\tobjects::MObject.allInstances()->asOrderedSet()\r\nin\r\nif os->isEmpty() then\r\n\tOrderedSet{} \r\nelse\r\n\tos->select(x:objects::MObject| x.isInstanceOf(self) or x.type.allSuperTypes()->includes(self))->asOrderedSet()\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObject> getInstance();

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.ClassifierKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see com.montages.mcore.ClassifierKind
	 * @see com.montages.mcore.McorePackage#getMClassifier_Kind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isClassByStructure \r\n  then if self.literal->isEmpty() \r\n    then ClassifierKind::ClassType \r\n    else ClassifierKind::WrongClassWithLiteral endif\r\nelse  \r\nif self.literal->notEmpty() then \r\n  ClassifierKind::Enumeration else \r\n  ClassifierKind::DataType\r\nendif endif \r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ClassifierKind getKind();

	/**
	 * Returns the value of the '<em><b>Enforced Class</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enforced Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enforced Class</em>' attribute.
	 * @see #isSetEnforcedClass()
	 * @see #unsetEnforcedClass()
	 * @see #setEnforcedClass(Boolean)
	 * @see com.montages.mcore.McorePackage#getMClassifier_EnforcedClass()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	Boolean getEnforcedClass();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getEnforcedClass <em>Enforced Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enforced Class</em>' attribute.
	 * @see #isSetEnforcedClass()
	 * @see #unsetEnforcedClass()
	 * @see #getEnforcedClass()
	 * @generated
	 */
	void setEnforcedClass(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getEnforcedClass <em>Enforced Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEnforcedClass()
	 * @see #getEnforcedClass()
	 * @see #setEnforcedClass(Boolean)
	 * @generated
	 */
	void unsetEnforcedClass();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getEnforcedClass <em>Enforced Class</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Enforced Class</em>' attribute is set.
	 * @see #unsetEnforcedClass()
	 * @see #getEnforcedClass()
	 * @see #setEnforcedClass(Boolean)
	 * @generated
	 */
	boolean isSetEnforcedClass();

	/**
	 * Returns the value of the '<em><b>Abstract Class</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Class</em>' attribute.
	 * @see #isSetAbstractClass()
	 * @see #unsetAbstractClass()
	 * @see #setAbstractClass(Boolean)
	 * @see com.montages.mcore.McorePackage#getMClassifier_AbstractClass()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	Boolean getAbstractClass();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getAbstractClass <em>Abstract Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Class</em>' attribute.
	 * @see #isSetAbstractClass()
	 * @see #unsetAbstractClass()
	 * @see #getAbstractClass()
	 * @generated
	 */
	void setAbstractClass(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getAbstractClass <em>Abstract Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAbstractClass()
	 * @see #getAbstractClass()
	 * @see #setAbstractClass(Boolean)
	 * @generated
	 */
	void unsetAbstractClass();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getAbstractClass <em>Abstract Class</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Abstract Class</em>' attribute is set.
	 * @see #unsetAbstractClass()
	 * @see #getAbstractClass()
	 * @see #setAbstractClass(Boolean)
	 * @generated
	 */
	boolean isSetAbstractClass();

	/**
	 * Returns the value of the '<em><b>Internal EClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal EClassifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal EClassifier</em>' reference.
	 * @see #isSetInternalEClassifier()
	 * @see #unsetInternalEClassifier()
	 * @see #setInternalEClassifier(EClassifier)
	 * @see com.montages.mcore.McorePackage#getMClassifier_InternalEClassifier()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Relation to ECore'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EClassifier getInternalEClassifier();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getInternalEClassifier <em>Internal EClassifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal EClassifier</em>' reference.
	 * @see #isSetInternalEClassifier()
	 * @see #unsetInternalEClassifier()
	 * @see #getInternalEClassifier()
	 * @generated
	 */
	void setInternalEClassifier(EClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getInternalEClassifier <em>Internal EClassifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalEClassifier()
	 * @see #getInternalEClassifier()
	 * @see #setInternalEClassifier(EClassifier)
	 * @generated
	 */
	void unsetInternalEClassifier();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getInternalEClassifier <em>Internal EClassifier</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal EClassifier</em>' reference is set.
	 * @see #unsetInternalEClassifier()
	 * @see #getInternalEClassifier()
	 * @see #setInternalEClassifier(EClassifier)
	 * @generated
	 */
	boolean isSetInternalEClassifier();

	/**
	 * Returns the value of the '<em><b>Containing Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Package</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMClassifier_ContainingPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() then null else self.eContainer().oclAsType(MPackage) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getContainingPackage();

	/**
	 * Returns the value of the '<em><b>Represents Core Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Represents Core Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Represents Core Type</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_RepresentsCoreType()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingPackage.oclIsUndefined() \r\n  then false\r\n  else containingPackage.representsCorePackage endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getRepresentsCoreType();

	/**
	 * Returns the value of the '<em><b>All Property Groups</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MPropertiesGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Property Groups</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Property Groups</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_AllPropertyGroups()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='propertyGroup->union(propertyGroup->closure(propertyGroup))->sortedBy(indentLevel)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertiesGroup> getAllPropertyGroups();

	/**
	 * Returns the value of the '<em><b>Literal</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' containment reference list.
	 * @see #isSetLiteral()
	 * @see #unsetLiteral()
	 * @see com.montages.mcore.McorePackage#getMClassifier_Literal()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Enumerations' createColumn='true'"
	 * @generated
	 */
	EList<MLiteral> getLiteral();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getLiteral <em>Literal</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLiteral()
	 * @see #getLiteral()
	 * @generated
	 */
	void unsetLiteral();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getLiteral <em>Literal</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Literal</em>' containment reference list is set.
	 * @see #unsetLiteral()
	 * @see #getLiteral()
	 * @generated
	 */
	boolean isSetLiteral();

	/**
	 * Returns the value of the '<em><b>EInterface</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EInterface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EInterface</em>' attribute.
	 * @see #isSetEInterface()
	 * @see #unsetEInterface()
	 * @see #setEInterface(Boolean)
	 * @see com.montages.mcore.McorePackage#getMClassifier_EInterface()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	Boolean getEInterface();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getEInterface <em>EInterface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EInterface</em>' attribute.
	 * @see #isSetEInterface()
	 * @see #unsetEInterface()
	 * @see #getEInterface()
	 * @generated
	 */
	void setEInterface(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getEInterface <em>EInterface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEInterface()
	 * @see #getEInterface()
	 * @see #setEInterface(Boolean)
	 * @generated
	 */
	void unsetEInterface();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getEInterface <em>EInterface</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>EInterface</em>' attribute is set.
	 * @see #unsetEInterface()
	 * @see #getEInterface()
	 * @see #setEInterface(Boolean)
	 * @generated
	 */
	boolean isSetEInterface();

	/**
	 * Returns the value of the '<em><b>Operation As Id Property Error</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation As Id Property Error</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation As Id Property Error</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_OperationAsIdPropertyError()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.idProperty->exists(p:MProperty|p.kind=PropertyKind::Operation)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getOperationAsIdPropertyError();

	/**
	 * Returns the value of the '<em><b>Is Class By Structure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Class By Structure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Class By Structure</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMClassifier_IsClassByStructure()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.enforcedClass or \r\n  self.superType->notEmpty() or \r\n  self.ownedProperty->notEmpty() or \r\n   self.abstractClass or\r\n   self.hasSimpleModelingType '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='For Classes'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getIsClassByStructure();

	/**
	 * Returns the value of the '<em><b>Toggle Super Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Toggle Super Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Toggle Super Type</em>' reference.
	 * @see #setToggleSuperType(MClassifier)
	 * @see com.montages.mcore.McorePackage#getMClassifier_ToggleSuperType()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null' choiceConstraint='  not (trg = self) \r\nand\r\n  trg.kind = ClassifierKind::ClassType\r\nand\r\n  trg.selectableClassifier(superType, superTypePackage)\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='For Classes/Supertype Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getToggleSuperType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getToggleSuperType <em>Toggle Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Toggle Super Type</em>' reference.
	 * @see #getToggleSuperType()
	 * @generated
	 */
	void setToggleSuperType(MClassifier value);

	/**
	 * Returns the value of the '<em><b>Ordering Strategy</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.OrderingStrategy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordering Strategy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordering Strategy</em>' attribute.
	 * @see com.montages.mcore.OrderingStrategy
	 * @see #setOrderingStrategy(OrderingStrategy)
	 * @see com.montages.mcore.McorePackage#getMClassifier_OrderingStrategy()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderingStrategy::None'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='For Classes/Properties Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	OrderingStrategy getOrderingStrategy();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MClassifier#getOrderingStrategy <em>Ordering Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordering Strategy</em>' attribute.
	 * @see com.montages.mcore.OrderingStrategy
	 * @see #getOrderingStrategy()
	 * @generated
	 */
	void setOrderingStrategy(OrderingStrategy value);

	/**
	 * Returns the value of the '<em><b>Test All Properties</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test All Properties</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test All Properties</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMClassifier_TestAllProperties()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.allProperties()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='For Classes/Properties Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MProperty> getTestAllProperties();

	/**
	 * Returns the value of the '<em><b>Id Property</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Property</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Property</em>' reference list.
	 * @see #isSetIdProperty()
	 * @see #unsetIdProperty()
	 * @see com.montages.mcore.McorePackage#getMClassifier_IdProperty()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='self.allFeatures()->includes(trg)\r\nand ( trg.kind<>PropertyKind::Operation)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='For Classes/Id Properties Related'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MProperty> getIdProperty();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MClassifier#getIdProperty <em>Id Property</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIdProperty()
	 * @see #getIdProperty()
	 * @generated
	 */
	void unsetIdProperty();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MClassifier#getIdProperty <em>Id Property</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id Property</em>' reference list is set.
	 * @see #unsetIdProperty()
	 * @see #getIdProperty()
	 * @generated
	 */
	boolean isSetIdProperty();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='Tuple {simpleType=SimpleType::String, name=defaultPropertyDescription()}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related/Defaulting' createColumn='true'"
	 * @generated
	 */
	MProperty defaultPropertyValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='\'Text 1\''"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related/Defaulting' createColumn='true'"
	 * @generated
	 */
	String defaultPropertyDescription();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='literal'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Enumerations' createColumn='true'"
	 * @generated
	 */
	EList<MLiteral> allLiterals();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" currentClassifierMany="true"
	 *        annotation="http://www.xocl.org/OCL body='let mClassifier:MClassifier = self in \r\n\r\nif currentClassifier->includes(mClassifier) \r\n  then true\r\nelse if mClassifier.containingPackage.oclIsUndefined()\r\n  then false\r\nelse if filterPackage.oclIsUndefined()\r\n  then  if mClassifier.containingPackage.usePackageContentOnlyWithExplicitFilter\r\n          then if currentClassifier->isEmpty()\r\n            then false\r\n            else currentClassifier.containingPackage->includes(mClassifier.containingPackage) endif          \r\n   else true endif\r\nelse\r\n  filterPackage=mClassifier.containingPackage\r\nendif endif \r\n endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes' createColumn='true'"
	 * @generated
	 */
	Boolean selectableClassifier(EList<MClassifier> currentClassifier,
			MPackage filterPackage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='MClassifier.allInstances()\r\n->select(c:MClassifier|c.kind=ClassifierKind::ClassType and c.superType->includes(self))'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Supertype Related' createColumn='true'"
	 * @generated
	 */
	EList<MClassifier> allDirectSubTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let dS:Set(MClassifier)=self.allDirectSubTypes() in\r\nlet iDS:Set(MClassifier)=dS->iterate(c:MClassifier;i:Set(MClassifier)=Set{}|i->union(c.allSubTypes())) in\r\ndS->union(iDS)->excluding(null)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Supertype Related' createColumn='true'"
	 * @generated
	 */
	EList<MClassifier> allSubTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let sts:OrderedSet(MClassifier) = self.superType in\r\nif sts->isEmpty()then\'\' else\r\nsts->iterate(c:MClassifier;res:String=\'\' |\r\n  (if res=\'\' then \'\' else res.concat(\', \') endif)\r\n     .concat(c.eName))\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Supertype Related' createColumn='true'"
	 * @generated
	 */
	String superTypesAsString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='allSuperTypes'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Supertype Related' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	EList<MClassifier> allSuperTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='ownedProperty\r\n->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=self.allSuperProperties()| ps->append(p))\r\n\r\n/*self.superType.allProperties()->asSet()\r\n->union(self.ownedProperty)->asOrderedSet()\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='superType.allProperties()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allSuperProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.allProperties()->isEmpty() then OrderedSet{} else\r\nself.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Attribute or p.kind=mcore::PropertyKind::Reference\r\n)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if allProperties()->isEmpty() then OrderedSet{} else\r\nallProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Attribute or (p.kind=mcore::PropertyKind::Reference and not (p.containment))\r\n)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allNonContainmentFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if allProperties()->isEmpty() \r\n  then OrderedSet{} \r\n  else allProperties()\r\n    ->select(p:mcore::MProperty|\r\n      p.kind=mcore::PropertyKind::Attribute \r\n        or \r\n     (p.kind=mcore::PropertyKind::Reference \r\n        and p.hasStorage \r\n        and not (p.containment))) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allStoredNonContainmentFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let ncs:OrderedSet(mcore::MProperty)=self.allNonContainmentFeatures()->select(p:mcore::MProperty|p.hasStorage) in\r\nlet cs:OrderedSet(mcore::MProperty)=self.allContainmentReferences()->select(p:mcore::MProperty|p.hasStorage) in\r\ncs->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=ncs| ps->append(p))'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allFeaturesWithStorage();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.allProperties()->isEmpty() then OrderedSet{} else\r\nself.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Reference and p.containment\r\n)->asOrderedSet()\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allContainmentReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.allProperties()->isEmpty() then OrderedSet{} else\r\nself.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Operation).operationSignature->asOrderedSet()\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MOperationSignature> allOperationSignatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='\r\nMClassifier.allInstances().ownedProperty->select(containment)\r\n     ->select(p:MProperty| (p.type=self.containingClassifier  or self.superType.containingClassifier->includes(p.type )and p<>self)).containingClassifier\r\n   ->excluding(self)\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MClassifier> allClassesContainedIn();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.idProperty->isEmpty() then self.allSuperIdProperties() else\r\nself.idProperty->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=self.allSuperIdProperties()| ps->append(p))\r\nendif\r\n/*self.superType.allProperties()->asSet()\r\n->union(self.ownedProperty)->asOrderedSet()\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Id Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allIdProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='self.superType.allIdProperties()->asOrderedSet()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='For Classes/Id Properties Related' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> allSuperIdProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	XUpdate doActionUpdate(MClassifierAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='invoke Set Do Action'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	Object invokeSetDoAction(MClassifierAction mClassifierAction);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='invoke Toggle Super Type'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	Object invokeToggleSuperType(MClassifier mClassifier);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Do SuperType Update'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	XUpdate doSuperTypeUpdate(MClassifier trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='ambiguous Classifier Name'"
	 *        annotation="http://www.xocl.org/OCL body='let allClasses: OrderedSet(mcore::MClassifier)  = let chain: OrderedSet(mcore::MClassifier)  = if containingPackage.oclIsUndefined()\n  then OrderedSet{}\n  else containingPackage.classifier\nendif in\nchain->iterate(i:mcore::MClassifier; r: OrderedSet(mcore::MClassifier)=OrderedSet{} | if i.oclIsKindOf(mcore::MClassifier) then r->including(i.oclAsType(mcore::MClassifier))->asOrderedSet() \n else r endif)->select(it: mcore::MClassifier | let e0: Boolean = it <> self in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet sameName: OrderedSet(mcore::MClassifier)  = let chain: OrderedSet(mcore::MClassifier)  = allClasses in\nchain->iterate(i:mcore::MClassifier; r: OrderedSet(mcore::MClassifier)=OrderedSet{} | if i.oclIsKindOf(mcore::MClassifier) then r->including(i.oclAsType(mcore::MClassifier))->asOrderedSet() \n else r endif)->select(it: mcore::MClassifier | let e0: Boolean = it.name = name in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet var0: Boolean = let chain: OrderedSet(mcore::MClassifier)  = sameName in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif in\nvar0\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Naming and Label' createColumn='true'"
	 * @generated
	 */
	Boolean ambiguousClassifierName();

} // MClassifier
