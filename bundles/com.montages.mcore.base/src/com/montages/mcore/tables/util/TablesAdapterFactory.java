/**
 */
package com.montages.mcore.tables.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.MHeader;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.MTable;
import com.montages.mcore.tables.TablesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.tables.TablesPackage
 * @generated
 */
public class TablesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TablesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TablesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TablesSwitch<Adapter> modelSwitch = new TablesSwitch<Adapter>() {
		@Override
		public Adapter caseMAbstractHeaderRow(MAbstractHeaderRow object) {
			return createMAbstractHeaderRowAdapter();
		}

		@Override
		public Adapter caseMTable(MTable object) {
			return createMTableAdapter();
		}

		@Override
		public Adapter caseMObjectRow(MObjectRow object) {
			return createMObjectRowAdapter();
		}

		@Override
		public Adapter caseMHeader(MHeader object) {
			return createMHeaderAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.tables.MAbstractHeaderRow <em>MAbstract Header Row</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow
	 * @generated
	 */
	public Adapter createMAbstractHeaderRowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.tables.MTable <em>MTable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.tables.MTable
	 * @generated
	 */
	public Adapter createMTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.tables.MObjectRow <em>MObject Row</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.tables.MObjectRow
	 * @generated
	 */
	public Adapter createMObjectRowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.tables.MHeader <em>MHeader</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.tables.MHeader
	 * @generated
	 */
	public Adapter createMHeaderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TablesAdapterFactory
