/**
 */
package com.montages.mcore.tables.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import com.montages.mcore.tables.TablesPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TablesXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablesXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		TablesPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the TablesResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new TablesResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new TablesResourceFactoryImpl());
		}
		return registrations;
	}

} //TablesXMLProcessor
