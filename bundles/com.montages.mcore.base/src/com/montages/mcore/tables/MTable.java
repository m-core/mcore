/**
 */
package com.montages.mcore.tables;

import com.montages.mcore.objects.MObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.MTable#getObject <em>Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.tables.TablesPackage#getMTable()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL classifierDerive='if (let e: Boolean = object.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if object.oclIsUndefined()\n  then null\n  else object.type\nendif\nendif\n' objectRowInitValue='/*let r:OrderedSet(tables::MObjectRow)=OrderedSet{} in r\052/\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nOrderedSet{Tuple{object=self.object, containingHeaderRow=self}} endif'"
 * @generated
 */

public interface MTable extends MAbstractHeaderRow {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMTable_Object()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(objects::MObject)'"
	 * @generated
	 */
	MObject getObject();

} // MTable
