/**
 */
package com.montages.mcore.tables;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import com.montages.mcore.MLiteral;
import com.montages.mcore.MProperty;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getContainingHeaderRow <em>Containing Header Row</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getObject <em>Object</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getContainmentPropertyHeader <em>Containment Property Header</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn1Values <em>Column1 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn1ReferencedObjects <em>Column1 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn2ReferencedObjects <em>Column2 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn3ReferencedObjects <em>Column3 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn4ReferencedObjects <em>Column4 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn5ReferencedObjects <em>Column5 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn1DataValues <em>Column1 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn1LiteralValue <em>Column1 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn2DataValues <em>Column2 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn2LiteralValue <em>Column2 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn3DataValues <em>Column3 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn3LiteralValue <em>Column3 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn4DataValues <em>Column4 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn4LiteralValue <em>Column4 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn5DataValues <em>Column5 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn5LiteralValue <em>Column5 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn2Values <em>Column2 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn3Values <em>Column3 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn4Values <em>Column4 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MObjectRow#getColumn5Values <em>Column5 Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow()
 * @model
 * @generated
 */

public interface MObjectRow extends EObject {
	/**
	 * Returns the value of the '<em><b>Containing Header Row</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Header Row</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Header Row</em>' reference.
	 * @see #isSetContainingHeaderRow()
	 * @see #unsetContainingHeaderRow()
	 * @see #setContainingHeaderRow(MAbstractHeaderRow)
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_ContainingHeaderRow()
	 * @model unsettable="true"
	 * @generated
	 */
	MAbstractHeaderRow getContainingHeaderRow();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.tables.MObjectRow#getContainingHeaderRow <em>Containing Header Row</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing Header Row</em>' reference.
	 * @see #isSetContainingHeaderRow()
	 * @see #unsetContainingHeaderRow()
	 * @see #getContainingHeaderRow()
	 * @generated
	 */
	void setContainingHeaderRow(MAbstractHeaderRow value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.tables.MObjectRow#getContainingHeaderRow <em>Containing Header Row</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainingHeaderRow()
	 * @see #getContainingHeaderRow()
	 * @see #setContainingHeaderRow(MAbstractHeaderRow)
	 * @generated
	 */
	void unsetContainingHeaderRow();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.tables.MObjectRow#getContainingHeaderRow <em>Containing Header Row</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Containing Header Row</em>' reference is set.
	 * @see #unsetContainingHeaderRow()
	 * @see #getContainingHeaderRow()
	 * @see #setContainingHeaderRow(MAbstractHeaderRow)
	 * @generated
	 */
	boolean isSetContainingHeaderRow();

	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see #setObject(MObject)
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Object()
	 * @model unsettable="true"
	 * @generated
	 */
	MObject getObject();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.tables.MObjectRow#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see #getObject()
	 * @generated
	 */
	void setObject(MObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.tables.MObjectRow#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObject()
	 * @see #getObject()
	 * @see #setObject(MObject)
	 * @generated
	 */
	void unsetObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.tables.MObjectRow#getObject <em>Object</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object</em>' reference is set.
	 * @see #unsetObject()
	 * @see #getObject()
	 * @see #setObject(MObject)
	 * @generated
	 */
	boolean isSetObject();

	/**
	 * Returns the value of the '<em><b>Containment Property Header</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.tables.MHeader}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containment Property Header</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment Property Header</em>' containment reference list.
	 * @see #isSetContainmentPropertyHeader()
	 * @see #unsetContainmentPropertyHeader()
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_ContainmentPropertyHeader()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='let r:OrderedSet(tables::MHeader) =  OrderedSet{} in \r\n\r\nif self.object.oclIsUndefined() then r else\r\nif self.object.propertyInstance->isEmpty() then r else\r\nlet containmentPis:OrderedSet(objects::MPropertyInstance) \r\n = self.object.propertyInstance\r\n      ->select(pi:objects::MPropertyInstance| if  pi.property.oclIsUndefined() then false else  pi.property.containment endif) in\r\nif containmentPis->isEmpty() then r else\r\nself.containmentPropertyHeadersFromContainmentProperties(containmentPis.property->asOrderedSet())\r\n/*containmentPis.property->collect(p:mcore::MProperty|Tuple{reference=p})->asOrderedSet()\052/\r\nendif\r\nendif\r\nendif\r\n'"
	 * @generated
	 */
	EList<MHeader> getContainmentPropertyHeader();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.tables.MObjectRow#getContainmentPropertyHeader <em>Containment Property Header</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainmentPropertyHeader()
	 * @see #getContainmentPropertyHeader()
	 * @generated
	 */
	void unsetContainmentPropertyHeader();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.tables.MObjectRow#getContainmentPropertyHeader <em>Containment Property Header</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Containment Property Header</em>' containment reference list is set.
	 * @see #unsetContainmentPropertyHeader()
	 * @see #getContainmentPropertyHeader()
	 * @generated
	 */
	boolean isSetContainmentPropertyHeader();

	/**
	 * Returns the value of the '<em><b>Column1 Values</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column1 Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column1 Values</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column1Values()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MValue> getColumn1Values();

	/**
	 * Returns the value of the '<em><b>Column1 Referenced Objects</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column1 Referenced Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column1 Referenced Objects</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column1ReferencedObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MObject> getColumn1ReferencedObjects();

	/**
	 * Returns the value of the '<em><b>Column2 Referenced Objects</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column2 Referenced Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column2 Referenced Objects</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column2ReferencedObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MObject> getColumn2ReferencedObjects();

	/**
	 * Returns the value of the '<em><b>Column3 Referenced Objects</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column3 Referenced Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column3 Referenced Objects</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column3ReferencedObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MObject> getColumn3ReferencedObjects();

	/**
	 * Returns the value of the '<em><b>Column4 Referenced Objects</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column4 Referenced Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column4 Referenced Objects</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column4ReferencedObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MObject> getColumn4ReferencedObjects();

	/**
	 * Returns the value of the '<em><b>Column5 Referenced Objects</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column5 Referenced Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column5 Referenced Objects</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column5ReferencedObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MObject> getColumn5ReferencedObjects();

	/**
	 * Returns the value of the '<em><b>Column1 Data Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column1 Data Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column1 Data Values</em>' attribute list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column1DataValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<String> getColumn1DataValues();

	/**
	 * Returns the value of the '<em><b>Column1 Literal Value</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column1 Literal Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column1 Literal Value</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column1LiteralValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MLiteral> getColumn1LiteralValue();

	/**
	 * Returns the value of the '<em><b>Column2 Data Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column2 Data Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column2 Data Values</em>' attribute list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column2DataValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<String> getColumn2DataValues();

	/**
	 * Returns the value of the '<em><b>Column2 Literal Value</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column2 Literal Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column2 Literal Value</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column2LiteralValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MLiteral> getColumn2LiteralValue();

	/**
	 * Returns the value of the '<em><b>Column3 Data Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column3 Data Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column3 Data Values</em>' attribute list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column3DataValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<String> getColumn3DataValues();

	/**
	 * Returns the value of the '<em><b>Column3 Literal Value</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column3 Literal Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column3 Literal Value</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column3LiteralValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MLiteral> getColumn3LiteralValue();

	/**
	 * Returns the value of the '<em><b>Column4 Data Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column4 Data Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column4 Data Values</em>' attribute list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column4DataValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<String> getColumn4DataValues();

	/**
	 * Returns the value of the '<em><b>Column4 Literal Value</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column4 Literal Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column4 Literal Value</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column4LiteralValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MLiteral> getColumn4LiteralValue();

	/**
	 * Returns the value of the '<em><b>Column5 Data Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column5 Data Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column5 Data Values</em>' attribute list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column5DataValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<String> getColumn5DataValues();

	/**
	 * Returns the value of the '<em><b>Column5 Literal Value</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column5 Literal Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column5 Literal Value</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column5LiteralValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MLiteral> getColumn5LiteralValue();

	/**
	 * Returns the value of the '<em><b>Column2 Values</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column2 Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column2 Values</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column2Values()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MValue> getColumn2Values();

	/**
	 * Returns the value of the '<em><b>Column3 Values</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column3 Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column3 Values</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column3Values()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MValue> getColumn3Values();

	/**
	 * Returns the value of the '<em><b>Column4 Values</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column4 Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column4 Values</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column4Values()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MValue> getColumn4Values();

	/**
	 * Returns the value of the '<em><b>Column5 Values</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column5 Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column5 Values</em>' reference list.
	 * @see com.montages.mcore.tables.TablesPackage#getMObjectRow_Column5Values()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif'"
	 * @generated
	 */
	EList<MValue> getColumn5Values();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model containmentReferenceMany="true"
	 *        annotation="http://www.xocl.org/OCL body='containmentReference->collect(p:mcore::MProperty|Tuple{reference=p})->asOrderedSet()'"
	 * @generated
	 */
	EList<MHeader> containmentPropertyHeadersFromContainmentProperties(
			EList<MProperty> containmentReference);

} // MObjectRow
