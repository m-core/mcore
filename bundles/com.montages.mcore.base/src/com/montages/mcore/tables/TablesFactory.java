/**
 */
package com.montages.mcore.tables;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.tables.TablesPackage
 * @generated
 */
public interface TablesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TablesFactory eINSTANCE = com.montages.mcore.tables.impl.TablesFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>MTable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MTable</em>'.
	 * @generated
	 */
	MTable createMTable();

	/**
	 * Returns a new object of class '<em>MObject Row</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MObject Row</em>'.
	 * @generated
	 */
	MObjectRow createMObjectRow();

	/**
	 * Returns a new object of class '<em>MHeader</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MHeader</em>'.
	 * @generated
	 */
	MHeader createMHeader();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TablesPackage getTablesPackage();

} //TablesFactory
