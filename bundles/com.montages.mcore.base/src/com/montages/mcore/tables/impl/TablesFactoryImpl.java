/**
 */
package com.montages.mcore.tables.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import com.montages.mcore.tables.MHeader;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.MTable;
import com.montages.mcore.tables.TablesFactory;
import com.montages.mcore.tables.TablesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TablesFactoryImpl extends EFactoryImpl implements TablesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TablesFactory init() {
		try {
			TablesFactory theTablesFactory = (TablesFactory) EPackage.Registry.INSTANCE
					.getEFactory(TablesPackage.eNS_URI);
			if (theTablesFactory != null) {
				return theTablesFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TablesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case TablesPackage.MTABLE:
			return createMTable();
		case TablesPackage.MOBJECT_ROW:
			return createMObjectRow();
		case TablesPackage.MHEADER:
			return createMHeader();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MTable createMTable() {
		MTableImpl mTable = new MTableImpl();
		return mTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectRow createMObjectRow() {
		MObjectRowImpl mObjectRow = new MObjectRowImpl();
		return mObjectRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MHeader createMHeader() {
		MHeaderImpl mHeader = new MHeaderImpl();
		return mHeader;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablesPackage getTablesPackage() {
		return (TablesPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TablesPackage getPackage() {
		return TablesPackage.eINSTANCE;
	}

} //TablesFactoryImpl
