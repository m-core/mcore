/**
 */
package com.montages.mcore.tables.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

import com.montages.mcore.MLiteral;
import com.montages.mcore.MProperty;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MValue;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.MHeader;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.TablesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MObject Row</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getContainingHeaderRow <em>Containing Header Row</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getObject <em>Object</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getContainmentPropertyHeader <em>Containment Property Header</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn1Values <em>Column1 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn1ReferencedObjects <em>Column1 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn2ReferencedObjects <em>Column2 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn3ReferencedObjects <em>Column3 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn4ReferencedObjects <em>Column4 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn5ReferencedObjects <em>Column5 Referenced Objects</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn1DataValues <em>Column1 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn1LiteralValue <em>Column1 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn2DataValues <em>Column2 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn2LiteralValue <em>Column2 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn3DataValues <em>Column3 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn3LiteralValue <em>Column3 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn4DataValues <em>Column4 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn4LiteralValue <em>Column4 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn5DataValues <em>Column5 Data Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn5LiteralValue <em>Column5 Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn2Values <em>Column2 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn3Values <em>Column3 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn4Values <em>Column4 Values</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MObjectRowImpl#getColumn5Values <em>Column5 Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MObjectRowImpl extends MinimalEObjectImpl.Container
		implements MObjectRow, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getContainingHeaderRow() <em>Containing Header Row</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingHeaderRow()
	 * @generated
	 * @ordered
	 */
	protected MAbstractHeaderRow containingHeaderRow;

	/**
	 * This is true if the Containing Header Row reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean containingHeaderRowESet;

	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected MObject object;

	/**
	 * This is true if the Object reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean objectESet;

	/**
	 * The cached value of the '{@link #getContainmentPropertyHeader() <em>Containment Property Header</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainmentPropertyHeader()
	 * @generated
	 * @ordered
	 */
	protected EList<MHeader> containmentPropertyHeader;

	/**
	 * The parsed OCL expression for the body of the '{@link #containmentPropertyHeadersFromContainmentProperties <em>Containment Property Headers From Containment Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #containmentPropertyHeadersFromContainmentProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression containmentPropertyHeadersFromContainmentPropertiesmcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn1Values <em>Column1 Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn1Values
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column1ValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn1ReferencedObjects <em>Column1 Referenced Objects</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn1ReferencedObjects
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column1ReferencedObjectsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn2ReferencedObjects <em>Column2 Referenced Objects</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn2ReferencedObjects
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column2ReferencedObjectsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn3ReferencedObjects <em>Column3 Referenced Objects</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn3ReferencedObjects
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column3ReferencedObjectsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn4ReferencedObjects <em>Column4 Referenced Objects</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn4ReferencedObjects
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column4ReferencedObjectsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn5ReferencedObjects <em>Column5 Referenced Objects</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn5ReferencedObjects
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column5ReferencedObjectsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn1DataValues <em>Column1 Data Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn1DataValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column1DataValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn1LiteralValue <em>Column1 Literal Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn1LiteralValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column1LiteralValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn2DataValues <em>Column2 Data Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn2DataValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column2DataValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn2LiteralValue <em>Column2 Literal Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn2LiteralValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column2LiteralValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn3DataValues <em>Column3 Data Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn3DataValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column3DataValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn3LiteralValue <em>Column3 Literal Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn3LiteralValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column3LiteralValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn4DataValues <em>Column4 Data Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn4DataValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column4DataValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn4LiteralValue <em>Column4 Literal Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn4LiteralValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column4LiteralValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn5DataValues <em>Column5 Data Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn5DataValues
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column5DataValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn5LiteralValue <em>Column5 Literal Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn5LiteralValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column5LiteralValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn2Values <em>Column2 Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn2Values
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column2ValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn3Values <em>Column3 Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn3Values
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column3ValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn4Values <em>Column4 Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn4Values
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column4ValuesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn5Values <em>Column5 Values</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn5Values
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column5ValuesDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectRowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TablesPackage.Literals.MOBJECT_ROW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractHeaderRow getContainingHeaderRow() {
		if (containingHeaderRow != null && containingHeaderRow.eIsProxy()) {
			InternalEObject oldContainingHeaderRow = (InternalEObject) containingHeaderRow;
			containingHeaderRow = (MAbstractHeaderRow) eResolveProxy(
					oldContainingHeaderRow);
			if (containingHeaderRow != oldContainingHeaderRow) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW,
							oldContainingHeaderRow, containingHeaderRow));
			}
		}
		return containingHeaderRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractHeaderRow basicGetContainingHeaderRow() {
		return containingHeaderRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingHeaderRow(
			MAbstractHeaderRow newContainingHeaderRow) {
		MAbstractHeaderRow oldContainingHeaderRow = containingHeaderRow;
		containingHeaderRow = newContainingHeaderRow;
		boolean oldContainingHeaderRowESet = containingHeaderRowESet;
		containingHeaderRowESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW,
					oldContainingHeaderRow, containingHeaderRow,
					!oldContainingHeaderRowESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContainingHeaderRow() {
		MAbstractHeaderRow oldContainingHeaderRow = containingHeaderRow;
		boolean oldContainingHeaderRowESet = containingHeaderRowESet;
		containingHeaderRow = null;
		containingHeaderRowESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW,
					oldContainingHeaderRow, null, oldContainingHeaderRowESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContainingHeaderRow() {
		return containingHeaderRowESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getObject() {
		if (object != null && object.eIsProxy()) {
			InternalEObject oldObject = (InternalEObject) object;
			object = (MObject) eResolveProxy(oldObject);
			if (object != oldObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TablesPackage.MOBJECT_ROW__OBJECT, oldObject,
							object));
			}
		}
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(MObject newObject) {
		MObject oldObject = object;
		object = newObject;
		boolean oldObjectESet = objectESet;
		objectESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TablesPackage.MOBJECT_ROW__OBJECT, oldObject, object,
					!oldObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObject() {
		MObject oldObject = object;
		boolean oldObjectESet = objectESet;
		object = null;
		objectESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					TablesPackage.MOBJECT_ROW__OBJECT, oldObject, null,
					oldObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObject() {
		return objectESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MHeader> getContainmentPropertyHeader() {
		if (containmentPropertyHeader == null) {
			containmentPropertyHeader = new EObjectContainmentEList.Unsettable.Resolving<MHeader>(
					MHeader.class, this,
					TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER);
		}
		return containmentPropertyHeader;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContainmentPropertyHeader() {
		if (containmentPropertyHeader != null)
			((InternalEList.Unsettable<?>) containmentPropertyHeader).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContainmentPropertyHeader() {
		return containmentPropertyHeader != null
				&& ((InternalEList.Unsettable<?>) containmentPropertyHeader)
						.isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MValue> getColumn1Values() {
		/**
		 * @OCL let colNr:Integer=1 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->notEmpty() then 
		pis->first().internalReferencedObject->asOrderedSet() 
		else if pis->first().internalLiteralValue->notEmpty() then
		pis->first().internalLiteralValue->asOrderedSet()
		else if pis->first().internalDataValue->notEmpty() then
		pis->first().internalDataValue->asOrderedSet()
		else OrderedSet{}
		endif endif endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN1_VALUES;

		if (column1ValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column1ValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column1ValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MValue> result = (EList<MValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getColumn1ReferencedObjects() {
		/**
		 * @OCL let colNr:Integer=1 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->isEmpty() then OrderedSet{}
		else pis->first().internalReferencedObject
		.referencedObject->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS;

		if (column1ReferencedObjectsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column1ReferencedObjectsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column1ReferencedObjectsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getColumn2ReferencedObjects() {
		/**
		 * @OCL let colNr:Integer=2 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->isEmpty() then OrderedSet{}
		else pis->first().internalReferencedObject
		.referencedObject->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS;

		if (column2ReferencedObjectsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column2ReferencedObjectsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column2ReferencedObjectsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getColumn3ReferencedObjects() {
		/**
		 * @OCL let colNr:Integer=3 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->isEmpty() then OrderedSet{}
		else pis->first().internalReferencedObject
		.referencedObject->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS;

		if (column3ReferencedObjectsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column3ReferencedObjectsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column3ReferencedObjectsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getColumn4ReferencedObjects() {
		/**
		 * @OCL let colNr:Integer=4 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->isEmpty() then OrderedSet{}
		else pis->first().internalReferencedObject
		.referencedObject->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS;

		if (column4ReferencedObjectsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column4ReferencedObjectsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column4ReferencedObjectsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getColumn5ReferencedObjects() {
		/**
		 * @OCL let colNr:Integer=5 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->isEmpty() then OrderedSet{}
		else pis->first().internalReferencedObject
		.referencedObject->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS;

		if (column5ReferencedObjectsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column5ReferencedObjectsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column5ReferencedObjectsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumn1DataValues() {
		/**
		 * @OCL let colNr:Integer=1 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalDataValue->isEmpty() then OrderedSet{}
		else pis->first().internalDataValue.dataValue
		->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN1_DATA_VALUES;

		if (column1DataValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column1DataValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column1DataValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<String> result = (EList<String>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> getColumn1LiteralValue() {
		/**
		 * @OCL let colNr:Integer=1 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalLiteralValue->isEmpty() then OrderedSet{}
		else pis->first().internalLiteralValue.literalValue->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN1_LITERAL_VALUE;

		if (column1LiteralValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column1LiteralValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column1LiteralValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteral> result = (EList<MLiteral>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumn2DataValues() {
		/**
		 * @OCL let colNr:Integer=2 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalDataValue->isEmpty() then OrderedSet{}
		else pis->first().internalDataValue.dataValue
		->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN2_DATA_VALUES;

		if (column2DataValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column2DataValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column2DataValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<String> result = (EList<String>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> getColumn2LiteralValue() {
		/**
		 * @OCL let colNr:Integer=2 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalLiteralValue->isEmpty() then OrderedSet{}
		else pis->first().internalLiteralValue.literalValue->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN2_LITERAL_VALUE;

		if (column2LiteralValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column2LiteralValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column2LiteralValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteral> result = (EList<MLiteral>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumn3DataValues() {
		/**
		 * @OCL let colNr:Integer=3 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalDataValue->isEmpty() then OrderedSet{}
		else pis->first().internalDataValue.dataValue
		->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN3_DATA_VALUES;

		if (column3DataValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column3DataValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column3DataValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<String> result = (EList<String>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> getColumn3LiteralValue() {
		/**
		 * @OCL let colNr:Integer=3 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalLiteralValue->isEmpty() then OrderedSet{}
		else pis->first().internalLiteralValue.literalValue->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN3_LITERAL_VALUE;

		if (column3LiteralValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column3LiteralValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column3LiteralValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteral> result = (EList<MLiteral>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumn4DataValues() {
		/**
		 * @OCL let colNr:Integer=4 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalDataValue->isEmpty() then OrderedSet{}
		else pis->first().internalDataValue.dataValue
		->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN4_DATA_VALUES;

		if (column4DataValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column4DataValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column4DataValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<String> result = (EList<String>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> getColumn4LiteralValue() {
		/**
		 * @OCL let colNr:Integer=4 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalLiteralValue->isEmpty() then OrderedSet{}
		else pis->first().internalLiteralValue.literalValue->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN4_LITERAL_VALUE;

		if (column4LiteralValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column4LiteralValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column4LiteralValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteral> result = (EList<MLiteral>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumn5DataValues() {
		/**
		 * @OCL let colNr:Integer=5 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalDataValue->isEmpty() then OrderedSet{}
		else pis->first().internalDataValue.dataValue
		->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN5_DATA_VALUES;

		if (column5DataValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column5DataValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column5DataValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<String> result = (EList<String>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> getColumn5LiteralValue() {
		/**
		 * @OCL let colNr:Integer=5 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalLiteralValue->isEmpty() then OrderedSet{}
		else pis->first().internalLiteralValue.literalValue->asOrderedSet() 
		endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN5_LITERAL_VALUE;

		if (column5LiteralValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column5LiteralValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column5LiteralValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteral> result = (EList<MLiteral>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MValue> getColumn2Values() {
		/**
		 * @OCL let colNr:Integer=2 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->notEmpty() then 
		pis->first().internalReferencedObject->asOrderedSet() 
		else if pis->first().internalLiteralValue->notEmpty() then
		pis->first().internalLiteralValue->asOrderedSet()
		else if pis->first().internalDataValue->notEmpty() then
		pis->first().internalDataValue->asOrderedSet()
		else OrderedSet{}
		endif endif endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN2_VALUES;

		if (column2ValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column2ValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column2ValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MValue> result = (EList<MValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MValue> getColumn3Values() {
		/**
		 * @OCL let colNr:Integer=3 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->notEmpty() then 
		pis->first().internalReferencedObject->asOrderedSet() 
		else if pis->first().internalLiteralValue->notEmpty() then
		pis->first().internalLiteralValue->asOrderedSet()
		else if pis->first().internalDataValue->notEmpty() then
		pis->first().internalDataValue->asOrderedSet()
		else OrderedSet{}
		endif endif endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN3_VALUES;

		if (column3ValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column3ValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column3ValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MValue> result = (EList<MValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MValue> getColumn4Values() {
		/**
		 * @OCL let colNr:Integer=4 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->notEmpty() then 
		pis->first().internalReferencedObject->asOrderedSet() 
		else if pis->first().internalLiteralValue->notEmpty() then
		pis->first().internalLiteralValue->asOrderedSet()
		else if pis->first().internalDataValue->notEmpty() then
		pis->first().internalDataValue->asOrderedSet()
		else OrderedSet{}
		endif endif endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN4_VALUES;

		if (column4ValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column4ValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column4ValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MValue> result = (EList<MValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MValue> getColumn5Values() {
		/**
		 * @OCL let colNr:Integer=5 in
		if self.object.oclIsUndefined() then OrderedSet{} else
		if self.object.propertyInstance->isEmpty() then OrderedSet{} else
		let c:mcore::MClassifier = self.containingHeaderRow.classifier in
		if c.oclIsUndefined() then OrderedSet{} else
		if c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}
		else 
		let p:mcore::MProperty = c.allProperties()->at(colNr) in
		let pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance
		->select(pi2:objects::MPropertyInstance|pi2.property = p) in
		if pis->isEmpty() then OrderedSet{} else
		if pis->first().internalReferencedObject->notEmpty() then 
		pis->first().internalReferencedObject->asOrderedSet() 
		else if pis->first().internalLiteralValue->notEmpty() then
		pis->first().internalLiteralValue->asOrderedSet()
		else if pis->first().internalDataValue->notEmpty() then
		pis->first().internalDataValue->asOrderedSet()
		else OrderedSet{}
		endif endif endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MOBJECT_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MOBJECT_ROW__COLUMN5_VALUES;

		if (column5ValuesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column5ValuesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column5ValuesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MValue> result = (EList<MValue>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MHeader> containmentPropertyHeadersFromContainmentProperties(
			EList<MProperty> containmentReference) {

		/**
		 * @OCL containmentReference->collect(p:mcore::MProperty|Tuple{reference=p})->asOrderedSet()
		 * @templateTag IGOT01
		 */
		EClass eClass = (TablesPackage.Literals.MOBJECT_ROW);
		EOperation eOperation = TablesPackage.Literals.MOBJECT_ROW
				.getEOperations().get(0);
		if (containmentPropertyHeadersFromContainmentPropertiesmcoreMPropertyBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				containmentPropertyHeadersFromContainmentPropertiesmcoreMPropertyBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						body, helper.getProblems(),
						TablesPackage.Literals.MOBJECT_ROW, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				containmentPropertyHeadersFromContainmentPropertiesmcoreMPropertyBodyOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("containmentReference", containmentReference);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MHeader>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER:
			return ((InternalEList<?>) getContainmentPropertyHeader())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW:
			if (resolve)
				return getContainingHeaderRow();
			return basicGetContainingHeaderRow();
		case TablesPackage.MOBJECT_ROW__OBJECT:
			if (resolve)
				return getObject();
			return basicGetObject();
		case TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER:
			return getContainmentPropertyHeader();
		case TablesPackage.MOBJECT_ROW__COLUMN1_VALUES:
			return getColumn1Values();
		case TablesPackage.MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS:
			return getColumn1ReferencedObjects();
		case TablesPackage.MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS:
			return getColumn2ReferencedObjects();
		case TablesPackage.MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS:
			return getColumn3ReferencedObjects();
		case TablesPackage.MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS:
			return getColumn4ReferencedObjects();
		case TablesPackage.MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS:
			return getColumn5ReferencedObjects();
		case TablesPackage.MOBJECT_ROW__COLUMN1_DATA_VALUES:
			return getColumn1DataValues();
		case TablesPackage.MOBJECT_ROW__COLUMN1_LITERAL_VALUE:
			return getColumn1LiteralValue();
		case TablesPackage.MOBJECT_ROW__COLUMN2_DATA_VALUES:
			return getColumn2DataValues();
		case TablesPackage.MOBJECT_ROW__COLUMN2_LITERAL_VALUE:
			return getColumn2LiteralValue();
		case TablesPackage.MOBJECT_ROW__COLUMN3_DATA_VALUES:
			return getColumn3DataValues();
		case TablesPackage.MOBJECT_ROW__COLUMN3_LITERAL_VALUE:
			return getColumn3LiteralValue();
		case TablesPackage.MOBJECT_ROW__COLUMN4_DATA_VALUES:
			return getColumn4DataValues();
		case TablesPackage.MOBJECT_ROW__COLUMN4_LITERAL_VALUE:
			return getColumn4LiteralValue();
		case TablesPackage.MOBJECT_ROW__COLUMN5_DATA_VALUES:
			return getColumn5DataValues();
		case TablesPackage.MOBJECT_ROW__COLUMN5_LITERAL_VALUE:
			return getColumn5LiteralValue();
		case TablesPackage.MOBJECT_ROW__COLUMN2_VALUES:
			return getColumn2Values();
		case TablesPackage.MOBJECT_ROW__COLUMN3_VALUES:
			return getColumn3Values();
		case TablesPackage.MOBJECT_ROW__COLUMN4_VALUES:
			return getColumn4Values();
		case TablesPackage.MOBJECT_ROW__COLUMN5_VALUES:
			return getColumn5Values();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW:
			setContainingHeaderRow((MAbstractHeaderRow) newValue);
			return;
		case TablesPackage.MOBJECT_ROW__OBJECT:
			setObject((MObject) newValue);
			return;
		case TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER:
			getContainmentPropertyHeader().clear();
			getContainmentPropertyHeader()
					.addAll((Collection<? extends MHeader>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW:
			unsetContainingHeaderRow();
			return;
		case TablesPackage.MOBJECT_ROW__OBJECT:
			unsetObject();
			return;
		case TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER:
			unsetContainmentPropertyHeader();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TablesPackage.MOBJECT_ROW__CONTAINING_HEADER_ROW:
			return isSetContainingHeaderRow();
		case TablesPackage.MOBJECT_ROW__OBJECT:
			return isSetObject();
		case TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER:
			return isSetContainmentPropertyHeader();
		case TablesPackage.MOBJECT_ROW__COLUMN1_VALUES:
			return !getColumn1Values().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS:
			return !getColumn1ReferencedObjects().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS:
			return !getColumn2ReferencedObjects().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS:
			return !getColumn3ReferencedObjects().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS:
			return !getColumn4ReferencedObjects().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS:
			return !getColumn5ReferencedObjects().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN1_DATA_VALUES:
			return !getColumn1DataValues().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN1_LITERAL_VALUE:
			return !getColumn1LiteralValue().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN2_DATA_VALUES:
			return !getColumn2DataValues().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN2_LITERAL_VALUE:
			return !getColumn2LiteralValue().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN3_DATA_VALUES:
			return !getColumn3DataValues().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN3_LITERAL_VALUE:
			return !getColumn3LiteralValue().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN4_DATA_VALUES:
			return !getColumn4DataValues().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN4_LITERAL_VALUE:
			return !getColumn4LiteralValue().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN5_DATA_VALUES:
			return !getColumn5DataValues().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN5_LITERAL_VALUE:
			return !getColumn5LiteralValue().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN2_VALUES:
			return !getColumn2Values().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN3_VALUES:
			return !getColumn3Values().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN4_VALUES:
			return !getColumn4Values().isEmpty();
		case TablesPackage.MOBJECT_ROW__COLUMN5_VALUES:
			return !getColumn5Values().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case TablesPackage.MOBJECT_ROW___CONTAINMENT_PROPERTY_HEADERS_FROM_CONTAINMENT_PROPERTIES__ELIST:
			return containmentPropertyHeadersFromContainmentProperties(
					(EList<MProperty>) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				TablesPackage.Literals.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MOBJECT_ROW,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MObjectRowImpl
