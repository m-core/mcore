/**
 */
package com.montages.mcore.tables.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.MHeader;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.TablesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MHeader</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.impl.MHeaderImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MHeaderImpl#getContainingObjectRow <em>Containing Object Row</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MHeaderImpl#getPreviousHeader <em>Previous Header</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MHeaderImpl extends MAbstractHeaderRowImpl
		implements MHeader, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected MProperty reference;

	/**
	 * This is true if the Reference reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean referenceESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingObjectRow <em>Containing Object Row</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingObjectRow
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingObjectRowDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getPreviousHeader <em>Previous Header</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousHeader
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression previousHeaderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getClassifier <em>Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression classifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn2 <em>Displayed Column2</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn2
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression displayedColumn2DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn1 <em>Displayed Column1</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn1
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression displayedColumn1DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn4 <em>Displayed Column4</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn4
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression displayedColumn4DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn3 <em>Displayed Column3</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn3
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression displayedColumn3DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn5 <em>Displayed Column5</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn5
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression displayedColumn5DeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MHeaderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TablesPackage.Literals.MHEADER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getReference() {
		if (reference != null && reference.eIsProxy()) {
			InternalEObject oldReference = (InternalEObject) reference;
			reference = (MProperty) eResolveProxy(oldReference);
			if (reference != oldReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							TablesPackage.MHEADER__REFERENCE, oldReference,
							reference));
			}
		}
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetReference() {
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(MProperty newReference) {
		MProperty oldReference = reference;
		reference = newReference;
		boolean oldReferenceESet = referenceESet;
		referenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					TablesPackage.MHEADER__REFERENCE, oldReference, reference,
					!oldReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReference() {
		MProperty oldReference = reference;
		boolean oldReferenceESet = referenceESet;
		reference = null;
		referenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					TablesPackage.MHEADER__REFERENCE, oldReference, null,
					oldReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReference() {
		return referenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectRow getContainingObjectRow() {
		MObjectRow containingObjectRow = basicGetContainingObjectRow();
		return containingObjectRow != null && containingObjectRow.eIsProxy()
				? (MObjectRow) eResolveProxy(
						(InternalEObject) containingObjectRow)
				: containingObjectRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectRow basicGetContainingObjectRow() {
		/**
		 * @OCL self.eContainer().oclAsType(tables::MObjectRow)
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MHEADER;
		EStructuralFeature eFeature = TablesPackage.Literals.MHEADER__CONTAINING_OBJECT_ROW;

		if (containingObjectRowDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingObjectRowDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MHEADER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingObjectRowDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MHEADER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MObjectRow result = (MObjectRow) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractHeaderRow getPreviousHeader() {
		MAbstractHeaderRow previousHeader = basicGetPreviousHeader();
		return previousHeader != null && previousHeader.eIsProxy()
				? (MAbstractHeaderRow) eResolveProxy(
						(InternalEObject) previousHeader)
				: previousHeader;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractHeaderRow basicGetPreviousHeader() {
		/**
		 * @OCL if self.containingObjectRow.oclIsUndefined() then null else
		if self.containingObjectRow.containmentPropertyHeader->isEmpty() then null else
		let pos:Integer=self.containingObjectRow.containmentPropertyHeader->indexOf(self) in 
		if pos=1 then self.containingObjectRow.containingHeaderRow else self.containingObjectRow.containmentPropertyHeader->at(pos-1) endif
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MHEADER;
		EStructuralFeature eFeature = TablesPackage.Literals.MHEADER__PREVIOUS_HEADER;

		if (previousHeaderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				previousHeaderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MHEADER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(previousHeaderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MHEADER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractHeaderRow result = (MAbstractHeaderRow) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TablesPackage.MHEADER__REFERENCE:
			if (resolve)
				return getReference();
			return basicGetReference();
		case TablesPackage.MHEADER__CONTAINING_OBJECT_ROW:
			if (resolve)
				return getContainingObjectRow();
			return basicGetContainingObjectRow();
		case TablesPackage.MHEADER__PREVIOUS_HEADER:
			if (resolve)
				return getPreviousHeader();
			return basicGetPreviousHeader();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TablesPackage.MHEADER__REFERENCE:
			setReference((MProperty) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TablesPackage.MHEADER__REFERENCE:
			unsetReference();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TablesPackage.MHEADER__REFERENCE:
			return isSetReference();
		case TablesPackage.MHEADER__CONTAINING_OBJECT_ROW:
			return basicGetContainingObjectRow() != null;
		case TablesPackage.MHEADER__PREVIOUS_HEADER:
			return basicGetPreviousHeader() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL classifier if (let e: Boolean = reference.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then null
	else if reference.oclIsUndefined()
	then null
	else reference.type
	endif
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetClassifier() {
		EClass eClass = (TablesPackage.Literals.MHEADER);
		EStructuralFeature eOverrideFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__CLASSIFIER;

		if (classifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				classifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(classifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL displayedColumn2 let p: MProperty = column2 in
	if (let e: Boolean = previousHeader.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then p else if (let e: Boolean = if previousHeader.oclIsUndefined()
	then null
	else previousHeader.column2
	endif = p in 
	if e.oclIsInvalid() then null else e endif) then null
	else p
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetDisplayedColumn2() {
		EClass eClass = (TablesPackage.Literals.MHEADER);
		EStructuralFeature eOverrideFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2;

		if (displayedColumn2DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				displayedColumn2DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn2DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL displayedColumn1 let p: MProperty = column1 in
	if (let e: Boolean = previousHeader.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then p else if (let e: Boolean = if previousHeader.oclIsUndefined()
	then null
	else previousHeader.column1
	endif = p in 
	if e.oclIsInvalid() then null else e endif) then null
	else p
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetDisplayedColumn1() {
		EClass eClass = (TablesPackage.Literals.MHEADER);
		EStructuralFeature eOverrideFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1;

		if (displayedColumn1DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				displayedColumn1DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn1DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL displayedColumn4 let p: MProperty = column4 in
	if (let e: Boolean = previousHeader.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then p else if (let e: Boolean = if previousHeader.oclIsUndefined()
	then null
	else previousHeader.column4
	endif = p in 
	if e.oclIsInvalid() then null else e endif) then null
	else p
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetDisplayedColumn4() {
		EClass eClass = (TablesPackage.Literals.MHEADER);
		EStructuralFeature eOverrideFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4;

		if (displayedColumn4DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				displayedColumn4DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn4DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL displayedColumn3 let p: MProperty = column3 in
	if (let e: Boolean = previousHeader.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then p else if (let e: Boolean = if previousHeader.oclIsUndefined()
	then null
	else previousHeader.column3
	endif = p in 
	if e.oclIsInvalid() then null else e endif) then null
	else p
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetDisplayedColumn3() {
		EClass eClass = (TablesPackage.Literals.MHEADER);
		EStructuralFeature eOverrideFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3;

		if (displayedColumn3DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				displayedColumn3DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn3DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL displayedColumn5 let p: MProperty = column5 in
	if (let e: Boolean = previousHeader.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then p else if (let e: Boolean = if previousHeader.oclIsUndefined()
	then null
	else previousHeader.column5
	endif = p in 
	if e.oclIsInvalid() then null else e endif) then null
	else p
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetDisplayedColumn5() {
		EClass eClass = (TablesPackage.Literals.MHEADER);
		EStructuralFeature eOverrideFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5;

		if (displayedColumn5DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				displayedColumn5DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn5DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__OBJECT_ROW };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MHEADER,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MHeaderImpl
