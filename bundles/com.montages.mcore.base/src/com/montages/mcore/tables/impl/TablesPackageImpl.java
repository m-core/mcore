/**
 */
package com.montages.mcore.tables.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.langlets.acore.AcorePackage;
import org.xocl.semantics.SemanticsPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.impl.AnnotationsPackageImpl;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.ExpressionsPackageImpl;
import com.montages.mcore.impl.McorePackageImpl;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.ObjectsPackageImpl;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.MHeader;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.MTable;
import com.montages.mcore.tables.TablesFactory;
import com.montages.mcore.tables.TablesPackage;
import com.montages.mcore.updates.UpdatesPackage;
import com.montages.mcore.updates.impl.UpdatesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TablesPackageImpl extends EPackageImpl implements TablesPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractHeaderRowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mObjectRowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mHeaderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mcore.tables.TablesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TablesPackageImpl() {
		super(eNS_URI, TablesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TablesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TablesPackage init() {
		if (isInited)
			return (TablesPackage) EPackage.Registry.INSTANCE
					.getEPackage(TablesPackage.eNS_URI);

		// Obtain or create and register package
		TablesPackageImpl theTablesPackage = (TablesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof TablesPackageImpl
						? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new TablesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();
		SemanticsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		McorePackageImpl theMcorePackage = (McorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI) instanceof McorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(
								McorePackage.eNS_URI)
						: McorePackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(AnnotationsPackage.eNS_URI)
								: AnnotationsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ExpressionsPackage.eNS_URI)
								: ExpressionsPackage.eINSTANCE);
		ObjectsPackageImpl theObjectsPackage = (ObjectsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ObjectsPackage.eNS_URI) instanceof ObjectsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ObjectsPackage.eNS_URI)
								: ObjectsPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(UpdatesPackage.eNS_URI)
								: UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theTablesPackage.createPackageContents();
		theMcorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theObjectsPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theTablesPackage.initializePackageContents();
		theMcorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theObjectsPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTablesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TablesPackage.eNS_URI, theTablesPackage);
		return theTablesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractHeaderRow() {
		return mAbstractHeaderRowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_Classifier() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_Column1() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_Column2() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_ObjectRow() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_Column3() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_Column4() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_Column5() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_DisplayedColumn1() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_DisplayedColumn2() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_DisplayedColumn3() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_DisplayedColumn4() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractHeaderRow_DisplayedColumn5() {
		return (EReference) mAbstractHeaderRowEClass.getEStructuralFeatures()
				.get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTable() {
		return mTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTable_Object() {
		return (EReference) mTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMObjectRow() {
		return mObjectRowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_ContainingHeaderRow() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Object() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_ContainmentPropertyHeader() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column1Values() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column1ReferencedObjects() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column2ReferencedObjects() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column3ReferencedObjects() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column4ReferencedObjects() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column5ReferencedObjects() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObjectRow_Column1DataValues() {
		return (EAttribute) mObjectRowEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column1LiteralValue() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObjectRow_Column2DataValues() {
		return (EAttribute) mObjectRowEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column2LiteralValue() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObjectRow_Column3DataValues() {
		return (EAttribute) mObjectRowEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column3LiteralValue() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObjectRow_Column4DataValues() {
		return (EAttribute) mObjectRowEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column4LiteralValue() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObjectRow_Column5DataValues() {
		return (EAttribute) mObjectRowEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column5LiteralValue() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column2Values() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column3Values() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column4Values() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectRow_Column5Values() {
		return (EReference) mObjectRowEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObjectRow__ContainmentPropertyHeadersFromContainmentProperties__EList() {
		return mObjectRowEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMHeader() {
		return mHeaderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMHeader_Reference() {
		return (EReference) mHeaderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMHeader_ContainingObjectRow() {
		return (EReference) mHeaderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMHeader_PreviousHeader() {
		return (EReference) mHeaderEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablesFactory getTablesFactory() {
		return (TablesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mAbstractHeaderRowEClass = createEClass(MABSTRACT_HEADER_ROW);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__CLASSIFIER);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__COLUMN1);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__COLUMN2);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__OBJECT_ROW);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__COLUMN3);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__COLUMN4);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__COLUMN5);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4);
		createEReference(mAbstractHeaderRowEClass,
				MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5);

		mTableEClass = createEClass(MTABLE);
		createEReference(mTableEClass, MTABLE__OBJECT);

		mObjectRowEClass = createEClass(MOBJECT_ROW);
		createEReference(mObjectRowEClass, MOBJECT_ROW__CONTAINING_HEADER_ROW);
		createEReference(mObjectRowEClass, MOBJECT_ROW__OBJECT);
		createEReference(mObjectRowEClass,
				MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN1_VALUES);
		createEReference(mObjectRowEClass,
				MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS);
		createEReference(mObjectRowEClass,
				MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS);
		createEReference(mObjectRowEClass,
				MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS);
		createEReference(mObjectRowEClass,
				MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS);
		createEReference(mObjectRowEClass,
				MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS);
		createEAttribute(mObjectRowEClass, MOBJECT_ROW__COLUMN1_DATA_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN1_LITERAL_VALUE);
		createEAttribute(mObjectRowEClass, MOBJECT_ROW__COLUMN2_DATA_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN2_LITERAL_VALUE);
		createEAttribute(mObjectRowEClass, MOBJECT_ROW__COLUMN3_DATA_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN3_LITERAL_VALUE);
		createEAttribute(mObjectRowEClass, MOBJECT_ROW__COLUMN4_DATA_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN4_LITERAL_VALUE);
		createEAttribute(mObjectRowEClass, MOBJECT_ROW__COLUMN5_DATA_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN5_LITERAL_VALUE);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN2_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN3_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN4_VALUES);
		createEReference(mObjectRowEClass, MOBJECT_ROW__COLUMN5_VALUES);
		createEOperation(mObjectRowEClass,
				MOBJECT_ROW___CONTAINMENT_PROPERTY_HEADERS_FROM_CONTAINMENT_PROPERTIES__ELIST);

		mHeaderEClass = createEClass(MHEADER);
		createEReference(mHeaderEClass, MHEADER__REFERENCE);
		createEReference(mHeaderEClass, MHEADER__CONTAINING_OBJECT_ROW);
		createEReference(mHeaderEClass, MHEADER__PREVIOUS_HEADER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		McorePackage theMcorePackage = (McorePackage) EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI);
		ObjectsPackage theObjectsPackage = (ObjectsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ObjectsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mTableEClass.getESuperTypes().add(this.getMAbstractHeaderRow());
		mHeaderEClass.getESuperTypes().add(this.getMAbstractHeaderRow());

		// Initialize classes, features, and operations; add parameters
		initEClass(mAbstractHeaderRowEClass, MAbstractHeaderRow.class,
				"MAbstractHeaderRow", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAbstractHeaderRow_Classifier(),
				theMcorePackage.getMClassifier(), null, "classifier", null, 0,
				1, MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_Column1(),
				theMcorePackage.getMProperty(), null, "column1", null, 0, 1,
				MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_Column2(),
				theMcorePackage.getMProperty(), null, "column2", null, 0, 1,
				MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_ObjectRow(), this.getMObjectRow(),
				null, "objectRow", null, 0, -1, MAbstractHeaderRow.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractHeaderRow_Column3(),
				theMcorePackage.getMProperty(), null, "column3", null, 0, 1,
				MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_Column4(),
				theMcorePackage.getMProperty(), null, "column4", null, 0, 1,
				MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_Column5(),
				theMcorePackage.getMProperty(), null, "column5", null, 0, 1,
				MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_DisplayedColumn1(),
				theMcorePackage.getMProperty(), null, "displayedColumn1", null,
				0, 1, MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_DisplayedColumn2(),
				theMcorePackage.getMProperty(), null, "displayedColumn2", null,
				0, 1, MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_DisplayedColumn3(),
				theMcorePackage.getMProperty(), null, "displayedColumn3", null,
				0, 1, MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_DisplayedColumn4(),
				theMcorePackage.getMProperty(), null, "displayedColumn4", null,
				0, 1, MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractHeaderRow_DisplayedColumn5(),
				theMcorePackage.getMProperty(), null, "displayedColumn5", null,
				0, 1, MAbstractHeaderRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mTableEClass, MTable.class, "MTable", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMTable_Object(), theObjectsPackage.getMObject(), null,
				"object", null, 0, 1, MTable.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mObjectRowEClass, MObjectRow.class, "MObjectRow",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMObjectRow_ContainingHeaderRow(),
				this.getMAbstractHeaderRow(), null, "containingHeaderRow", null,
				0, 1, MObjectRow.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Object(), theObjectsPackage.getMObject(),
				null, "object", null, 0, 1, MObjectRow.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_ContainmentPropertyHeader(),
				this.getMHeader(), null, "containmentPropertyHeader", null, 0,
				-1, MObjectRow.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column1Values(),
				theObjectsPackage.getMValue(), null, "column1Values", null, 0,
				-1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column1ReferencedObjects(),
				theObjectsPackage.getMObject(), null,
				"column1ReferencedObjects", null, 0, -1, MObjectRow.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMObjectRow_Column2ReferencedObjects(),
				theObjectsPackage.getMObject(), null,
				"column2ReferencedObjects", null, 0, -1, MObjectRow.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMObjectRow_Column3ReferencedObjects(),
				theObjectsPackage.getMObject(), null,
				"column3ReferencedObjects", null, 0, -1, MObjectRow.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMObjectRow_Column4ReferencedObjects(),
				theObjectsPackage.getMObject(), null,
				"column4ReferencedObjects", null, 0, -1, MObjectRow.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMObjectRow_Column5ReferencedObjects(),
				theObjectsPackage.getMObject(), null,
				"column5ReferencedObjects", null, 0, -1, MObjectRow.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMObjectRow_Column1DataValues(),
				ecorePackage.getEString(), "column1DataValues", null, 0, -1,
				MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column1LiteralValue(),
				theMcorePackage.getMLiteral(), null, "column1LiteralValue",
				null, 0, -1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObjectRow_Column2DataValues(),
				ecorePackage.getEString(), "column2DataValues", null, 0, -1,
				MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column2LiteralValue(),
				theMcorePackage.getMLiteral(), null, "column2LiteralValue",
				null, 0, -1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObjectRow_Column3DataValues(),
				ecorePackage.getEString(), "column3DataValues", null, 0, -1,
				MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column3LiteralValue(),
				theMcorePackage.getMLiteral(), null, "column3LiteralValue",
				null, 0, -1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObjectRow_Column4DataValues(),
				ecorePackage.getEString(), "column4DataValues", null, 0, -1,
				MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column4LiteralValue(),
				theMcorePackage.getMLiteral(), null, "column4LiteralValue",
				null, 0, -1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObjectRow_Column5DataValues(),
				ecorePackage.getEString(), "column5DataValues", null, 0, -1,
				MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column5LiteralValue(),
				theMcorePackage.getMLiteral(), null, "column5LiteralValue",
				null, 0, -1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column2Values(),
				theObjectsPackage.getMValue(), null, "column2Values", null, 0,
				-1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column3Values(),
				theObjectsPackage.getMValue(), null, "column3Values", null, 0,
				-1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column4Values(),
				theObjectsPackage.getMValue(), null, "column4Values", null, 0,
				-1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectRow_Column5Values(),
				theObjectsPackage.getMValue(), null, "column5Values", null, 0,
				-1, MObjectRow.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(
				getMObjectRow__ContainmentPropertyHeadersFromContainmentProperties__EList(),
				this.getMHeader(),
				"containmentPropertyHeadersFromContainmentProperties", 0, -1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(),
				"containmentReference", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(mHeaderEClass, MHeader.class, "MHeader", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMHeader_Reference(), theMcorePackage.getMProperty(),
				null, "reference", null, 0, 1, MHeader.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMHeader_ContainingObjectRow(), this.getMObjectRow(),
				null, "containingObjectRow", null, 0, 1, MHeader.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMHeader_PreviousHeader(),
				this.getMAbstractHeaderRow(), null, "previousHeader", null, 0,
				1, MHeader.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getMAbstractHeaderRow_Classifier(), source, new String[] {
				"derive", "let nl: MClassifier = null in nl\n" });
		addAnnotation(getMAbstractHeaderRow_Column1(), source, new String[] {
				"derive",
				"let colNr: Integer = 1 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n" });
		addAnnotation(getMAbstractHeaderRow_Column2(), source, new String[] {
				"derive",
				"let colNr: Integer = 2 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n" });
		addAnnotation(getMAbstractHeaderRow_Column3(), source, new String[] {
				"derive",
				"let colNr: Integer = 3 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n" });
		addAnnotation(getMAbstractHeaderRow_Column4(), source, new String[] {
				"derive",
				"let colNr: Integer = 4 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n" });
		addAnnotation(getMAbstractHeaderRow_Column5(), source, new String[] {
				"derive",
				"let colNr: Integer = 5 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n" });
		addAnnotation(getMAbstractHeaderRow_DisplayedColumn1(), source,
				new String[] { "derive", "column1\n" });
		addAnnotation(getMAbstractHeaderRow_DisplayedColumn2(), source,
				new String[] { "derive", "column2\n" });
		addAnnotation(getMAbstractHeaderRow_DisplayedColumn3(), source,
				new String[] { "derive", "column3\n" });
		addAnnotation(getMAbstractHeaderRow_DisplayedColumn4(), source,
				new String[] { "derive", "column4\n" });
		addAnnotation(getMAbstractHeaderRow_DisplayedColumn5(), source,
				new String[] { "derive", "column5\n" });
		addAnnotation(getMTable_Object(), source, new String[] { "derive",
				"self.eContainer().oclAsType(objects::MObject)" });
		addAnnotation(
				getMObjectRow__ContainmentPropertyHeadersFromContainmentProperties__EList(),
				source, new String[] { "body",
						"containmentReference->collect(p:mcore::MProperty|Tuple{reference=p})->asOrderedSet()" });
		addAnnotation(getMObjectRow_ContainmentPropertyHeader(), source,
				new String[] { "initValue",
						"let r:OrderedSet(tables::MHeader) =  OrderedSet{} in \r\n\r\nif self.object.oclIsUndefined() then r else\r\nif self.object.propertyInstance->isEmpty() then r else\r\nlet containmentPis:OrderedSet(objects::MPropertyInstance) \r\n = self.object.propertyInstance\r\n      ->select(pi:objects::MPropertyInstance| if  pi.property.oclIsUndefined() then false else  pi.property.containment endif) in\r\nif containmentPis->isEmpty() then r else\r\nself.containmentPropertyHeadersFromContainmentProperties(containmentPis.property->asOrderedSet())\r\n/*containmentPis.property->collect(p:mcore::MProperty|Tuple{reference=p})->asOrderedSet()*/\r\nendif\r\nendif\r\nendif\r\n" });
		addAnnotation(getMObjectRow_Column1Values(), source, new String[] {
				"derive",
				"let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column1ReferencedObjects(), source,
				new String[] { "derive",
						"let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column2ReferencedObjects(), source,
				new String[] { "derive",
						"let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column3ReferencedObjects(), source,
				new String[] { "derive",
						"let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column4ReferencedObjects(), source,
				new String[] { "derive",
						"let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column5ReferencedObjects(), source,
				new String[] { "derive",
						"let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->isEmpty() then OrderedSet{}\r\nelse pis->first().internalReferencedObject\r\n.referencedObject->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column1DataValues(), source, new String[] {
				"derive",
				"let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column1LiteralValue(), source,
				new String[] { "derive",
						"let colNr:Integer=1 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column2DataValues(), source, new String[] {
				"derive",
				"let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column2LiteralValue(), source,
				new String[] { "derive",
						"let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column3DataValues(), source, new String[] {
				"derive",
				"let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column3LiteralValue(), source,
				new String[] { "derive",
						"let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column4DataValues(), source, new String[] {
				"derive",
				"let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column4LiteralValue(), source,
				new String[] { "derive",
						"let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column5DataValues(), source, new String[] {
				"derive",
				"let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalDataValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalDataValue.dataValue\r\n->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column5LiteralValue(), source,
				new String[] { "derive",
						"let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalLiteralValue->isEmpty() then OrderedSet{}\r\nelse pis->first().internalLiteralValue.literalValue->asOrderedSet() \r\nendif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column2Values(), source, new String[] {
				"derive",
				"let colNr:Integer=2 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column3Values(), source, new String[] {
				"derive",
				"let colNr:Integer=3 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column4Values(), source, new String[] {
				"derive",
				"let colNr:Integer=4 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif" });
		addAnnotation(getMObjectRow_Column5Values(), source, new String[] {
				"derive",
				"let colNr:Integer=5 in\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nif self.object.propertyInstance->isEmpty() then OrderedSet{} else\r\nlet c:mcore::MClassifier = self.containingHeaderRow.classifier in\r\nif c.oclIsUndefined() then OrderedSet{} else\r\nif c.allStoredNonContainmentFeatures()->size()<colNr then OrderedSet{}\r\nelse \r\nlet p:mcore::MProperty = c.allProperties()->at(colNr) in\r\nlet pis:OrderedSet(objects::MPropertyInstance)=self.object.propertyInstance\r\n->select(pi2:objects::MPropertyInstance|pi2.property = p) in\r\nif pis->isEmpty() then OrderedSet{} else\r\nif pis->first().internalReferencedObject->notEmpty() then \r\n  pis->first().internalReferencedObject->asOrderedSet() \r\nelse if pis->first().internalLiteralValue->notEmpty() then\r\n  pis->first().internalLiteralValue->asOrderedSet()\r\nelse if pis->first().internalDataValue->notEmpty() then\r\n  pis->first().internalDataValue->asOrderedSet()\r\nelse OrderedSet{}\r\nendif endif endif endif endif endif endif endif" });
		addAnnotation(getMHeader_ContainingObjectRow(), source, new String[] {
				"derive", "self.eContainer().oclAsType(tables::MObjectRow)" });
		addAnnotation(getMHeader_PreviousHeader(), source, new String[] {
				"derive",
				"if self.containingObjectRow.oclIsUndefined() then null else\r\nif self.containingObjectRow.containmentPropertyHeader->isEmpty() then null else\r\nlet pos:Integer=self.containingObjectRow.containmentPropertyHeader->indexOf(self) in \r\nif pos=1 then self.containingObjectRow.containingHeaderRow else self.containingObjectRow.containmentPropertyHeader->at(pos-1) endif\r\nendif endif" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mTableEClass, source, new String[] { "classifierDerive",
				"if (let e: Boolean = object.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if object.oclIsUndefined()\n  then null\n  else object.type\nendif\nendif\n",
				"objectRowInitValue",
				"/*let r:OrderedSet(tables::MObjectRow)=OrderedSet{} in r*/\r\nif self.object.oclIsUndefined() then OrderedSet{} else\r\nOrderedSet{Tuple{object=self.object, containingHeaderRow=self}} endif" });
		addAnnotation(mHeaderEClass, source, new String[] { "classifierDerive",
				"if (let e: Boolean = reference.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if reference.oclIsUndefined()\n  then null\n  else reference.type\nendif\nendif\n",
				"displayedColumn2Derive",
				"let p: MProperty = column2 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column2\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n",
				"displayedColumn1Derive",
				"let p: MProperty = column1 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column1\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n",
				"displayedColumn4Derive",
				"let p: MProperty = column4 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column4\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n",
				"displayedColumn3Derive",
				"let p: MProperty = column3 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column3\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n",
				"displayedColumn5Derive",
				"let p: MProperty = column5 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column5\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n",
				"objectRowInitValue",
				"/*let r:OrderedSet(tables::MObjectRow)=OrderedSet{} in r*/\r\nlet o:objects::MObject=self.containingObjectRow.object in\r\nif o.oclIsUndefined() then OrderedSet{} else\r\nlet r:mcore::MProperty=self.reference in\r\nif r.oclIsUndefined() then OrderedSet{} else\r\nlet prIns:OrderedSet(objects::MPropertyInstance) = o.propertyInstance->select(pi2:objects::MPropertyInstance|pi2.property=r) in\r\nif prIns->isEmpty() then OrderedSet{} else\r\nprIns->first().internalContainedObject->collect(o2:objects::MObject|Tuple{object=o2, containingHeaderRow=self})\r\n->asOrderedSet()\r\nendif endif endif" });
	}

} //TablesPackageImpl
