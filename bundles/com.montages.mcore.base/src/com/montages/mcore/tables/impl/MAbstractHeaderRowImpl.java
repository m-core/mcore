/**
 */
package com.montages.mcore.tables.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.TablesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Header Row</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getColumn1 <em>Column1</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getColumn2 <em>Column2</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getObjectRow <em>Object Row</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getColumn3 <em>Column3</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getColumn4 <em>Column4</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getColumn5 <em>Column5</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getDisplayedColumn1 <em>Displayed Column1</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getDisplayedColumn2 <em>Displayed Column2</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getDisplayedColumn3 <em>Displayed Column3</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getDisplayedColumn4 <em>Displayed Column4</em>}</li>
 *   <li>{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl#getDisplayedColumn5 <em>Displayed Column5</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAbstractHeaderRowImpl
		extends MinimalEObjectImpl.Container implements MAbstractHeaderRow {
	/**
	 * The cached value of the '{@link #getObjectRow() <em>Object Row</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectRow()
	 * @generated
	 * @ordered
	 */
	protected EList<MObjectRow> objectRow;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getClassifier <em>Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression classifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn1 <em>Column1</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn1
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column1DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn2 <em>Column2</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn2
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column2DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn3 <em>Column3</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn3
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column3DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn4 <em>Column4</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn4
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column4DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getColumn5 <em>Column5</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn5
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression column5DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn1 <em>Displayed Column1</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn1
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression displayedColumn1DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn2 <em>Displayed Column2</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn2
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression displayedColumn2DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn3 <em>Displayed Column3</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn3
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression displayedColumn3DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn4 <em>Displayed Column4</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn4
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression displayedColumn4DeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDisplayedColumn5 <em>Displayed Column5</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayedColumn5
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression displayedColumn5DeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractHeaderRowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TablesPackage.Literals.MABSTRACT_HEADER_ROW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getClassifier() {
		MClassifier classifier = basicGetClassifier();
		return classifier != null && classifier.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) classifier)
				: classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetClassifier() {
		/**
		 * @OCL let nl: MClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__CLASSIFIER;

		if (classifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				classifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(classifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getColumn1() {
		MProperty column1 = basicGetColumn1();
		return column1 != null && column1.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) column1)
				: column1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetColumn1() {
		/**
		 * @OCL let colNr: Integer = 1 in
		if (let e: Boolean = classifier.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->size() in 
		if e.oclIsInvalid() then null else e endif < colNr in 
		if e.oclIsInvalid() then null else e endif) then null
		else let e: MProperty = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->at(colNr) in 
		if e.oclIsInvalid() then null else e endif
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN1;

		if (column1DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column1DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column1DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getColumn2() {
		MProperty column2 = basicGetColumn2();
		return column2 != null && column2.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) column2)
				: column2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetColumn2() {
		/**
		 * @OCL let colNr: Integer = 2 in
		if (let e: Boolean = classifier.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->size() in 
		if e.oclIsInvalid() then null else e endif < colNr in 
		if e.oclIsInvalid() then null else e endif) then null
		else let e: MProperty = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->at(colNr) in 
		if e.oclIsInvalid() then null else e endif
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN2;

		if (column2DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column2DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column2DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectRow> getObjectRow() {
		if (objectRow == null) {
			objectRow = new EObjectContainmentEList.Unsettable.Resolving<MObjectRow>(
					MObjectRow.class, this,
					TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW);
		}
		return objectRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObjectRow() {
		if (objectRow != null)
			((InternalEList.Unsettable<?>) objectRow).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObjectRow() {
		return objectRow != null
				&& ((InternalEList.Unsettable<?>) objectRow).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getColumn3() {
		MProperty column3 = basicGetColumn3();
		return column3 != null && column3.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) column3)
				: column3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetColumn3() {
		/**
		 * @OCL let colNr: Integer = 3 in
		if (let e: Boolean = classifier.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->size() in 
		if e.oclIsInvalid() then null else e endif < colNr in 
		if e.oclIsInvalid() then null else e endif) then null
		else let e: MProperty = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->at(colNr) in 
		if e.oclIsInvalid() then null else e endif
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN3;

		if (column3DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column3DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column3DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getColumn4() {
		MProperty column4 = basicGetColumn4();
		return column4 != null && column4.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) column4)
				: column4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetColumn4() {
		/**
		 * @OCL let colNr: Integer = 4 in
		if (let e: Boolean = classifier.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->size() in 
		if e.oclIsInvalid() then null else e endif < colNr in 
		if e.oclIsInvalid() then null else e endif) then null
		else let e: MProperty = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->at(colNr) in 
		if e.oclIsInvalid() then null else e endif
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN4;

		if (column4DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column4DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column4DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getColumn5() {
		MProperty column5 = basicGetColumn5();
		return column5 != null && column5.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) column5)
				: column5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetColumn5() {
		/**
		 * @OCL let colNr: Integer = 5 in
		if (let e: Boolean = classifier.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->size() in 
		if e.oclIsInvalid() then null else e endif < colNr in 
		if e.oclIsInvalid() then null else e endif) then null
		else let e: MProperty = if classifier.oclIsUndefined()
		then OrderedSet{}
		else classifier.allStoredNonContainmentFeatures()
		endif->at(colNr) in 
		if e.oclIsInvalid() then null else e endif
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN5;

		if (column5DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				column5DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(column5DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getDisplayedColumn1() {
		MProperty displayedColumn1 = basicGetDisplayedColumn1();
		return displayedColumn1 != null && displayedColumn1.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) displayedColumn1)
				: displayedColumn1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetDisplayedColumn1() {
		/**
		 * @OCL column1
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1;

		if (displayedColumn1DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				displayedColumn1DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn1DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getDisplayedColumn2() {
		MProperty displayedColumn2 = basicGetDisplayedColumn2();
		return displayedColumn2 != null && displayedColumn2.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) displayedColumn2)
				: displayedColumn2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetDisplayedColumn2() {
		/**
		 * @OCL column2
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2;

		if (displayedColumn2DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				displayedColumn2DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn2DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getDisplayedColumn3() {
		MProperty displayedColumn3 = basicGetDisplayedColumn3();
		return displayedColumn3 != null && displayedColumn3.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) displayedColumn3)
				: displayedColumn3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetDisplayedColumn3() {
		/**
		 * @OCL column3
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3;

		if (displayedColumn3DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				displayedColumn3DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn3DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getDisplayedColumn4() {
		MProperty displayedColumn4 = basicGetDisplayedColumn4();
		return displayedColumn4 != null && displayedColumn4.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) displayedColumn4)
				: displayedColumn4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetDisplayedColumn4() {
		/**
		 * @OCL column4
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4;

		if (displayedColumn4DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				displayedColumn4DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn4DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getDisplayedColumn5() {
		MProperty displayedColumn5 = basicGetDisplayedColumn5();
		return displayedColumn5 != null && displayedColumn5.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) displayedColumn5)
				: displayedColumn5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetDisplayedColumn5() {
		/**
		 * @OCL column5
		
		 * @templateTag GGFT01
		 */
		EClass eClass = TablesPackage.Literals.MABSTRACT_HEADER_ROW;
		EStructuralFeature eFeature = TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5;

		if (displayedColumn5DeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				displayedColumn5DeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(TablesPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(displayedColumn5DeriveOCL);
		try {
			XoclErrorHandler.enterContext(TablesPackage.PLUGIN_ID, query,
					TablesPackage.Literals.MABSTRACT_HEADER_ROW, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW:
			return ((InternalEList<?>) getObjectRow()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TablesPackage.MABSTRACT_HEADER_ROW__CLASSIFIER:
			if (resolve)
				return getClassifier();
			return basicGetClassifier();
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN1:
			if (resolve)
				return getColumn1();
			return basicGetColumn1();
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN2:
			if (resolve)
				return getColumn2();
			return basicGetColumn2();
		case TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW:
			return getObjectRow();
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN3:
			if (resolve)
				return getColumn3();
			return basicGetColumn3();
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN4:
			if (resolve)
				return getColumn4();
			return basicGetColumn4();
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN5:
			if (resolve)
				return getColumn5();
			return basicGetColumn5();
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1:
			if (resolve)
				return getDisplayedColumn1();
			return basicGetDisplayedColumn1();
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2:
			if (resolve)
				return getDisplayedColumn2();
			return basicGetDisplayedColumn2();
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3:
			if (resolve)
				return getDisplayedColumn3();
			return basicGetDisplayedColumn3();
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4:
			if (resolve)
				return getDisplayedColumn4();
			return basicGetDisplayedColumn4();
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5:
			if (resolve)
				return getDisplayedColumn5();
			return basicGetDisplayedColumn5();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW:
			getObjectRow().clear();
			getObjectRow().addAll((Collection<? extends MObjectRow>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW:
			unsetObjectRow();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TablesPackage.MABSTRACT_HEADER_ROW__CLASSIFIER:
			return basicGetClassifier() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN1:
			return basicGetColumn1() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN2:
			return basicGetColumn2() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW:
			return isSetObjectRow();
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN3:
			return basicGetColumn3() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN4:
			return basicGetColumn4() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__COLUMN5:
			return basicGetColumn5() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1:
			return basicGetDisplayedColumn1() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2:
			return basicGetDisplayedColumn2() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3:
			return basicGetDisplayedColumn3() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4:
			return basicGetDisplayedColumn4() != null;
		case TablesPackage.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5:
			return basicGetDisplayedColumn5() != null;
		}
		return super.eIsSet(featureID);
	}

} //MAbstractHeaderRowImpl
