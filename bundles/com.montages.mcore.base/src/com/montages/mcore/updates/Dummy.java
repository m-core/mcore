/**
 */
package com.montages.mcore.updates;

import org.eclipse.emf.ecore.EObject;
import org.langlets.acore.classifiers.AAttribute;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dummy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.updates.Dummy#getId23 <em>Id23</em>}</li>
 *   <li>{@link com.montages.mcore.updates.Dummy#getXUpdate <em>XUpdate</em>}</li>
 *   <li>{@link com.montages.mcore.updates.Dummy#getAAttribute <em>AAttribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.updates.UpdatesPackage#getDummy()
 * @model
 * @generated
 */

public interface Dummy extends EObject {
	/**
	 * Returns the value of the '<em><b>Id23</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id23</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id23</em>' attribute.
	 * @see #isSetId23()
	 * @see #unsetId23()
	 * @see #setId23(String)
	 * @see com.montages.mcore.updates.UpdatesPackage#getDummy_Id23()
	 * @model unsettable="true"
	 * @generated
	 */
	String getId23();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.updates.Dummy#getId23 <em>Id23</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id23</em>' attribute.
	 * @see #isSetId23()
	 * @see #unsetId23()
	 * @see #getId23()
	 * @generated
	 */

	void setId23(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.updates.Dummy#getId23 <em>Id23</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetId23()
	 * @see #getId23()
	 * @see #setId23(String)
	 * @generated
	 */
	void unsetId23();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.updates.Dummy#getId23 <em>Id23</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id23</em>' attribute is set.
	 * @see #unsetId23()
	 * @see #getId23()
	 * @see #setId23(String)
	 * @generated
	 */
	boolean isSetId23();

	/**
	 * Returns the value of the '<em><b>XUpdate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XUpdate</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XUpdate</em>' reference.
	 * @see #isSetXUpdate()
	 * @see #unsetXUpdate()
	 * @see #setXUpdate(XUpdate)
	 * @see com.montages.mcore.updates.UpdatesPackage#getDummy_XUpdate()
	 * @model unsettable="true"
	 * @generated
	 */
	XUpdate getXUpdate();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.updates.Dummy#getXUpdate <em>XUpdate</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>XUpdate</em>' reference.
	 * @see #isSetXUpdate()
	 * @see #unsetXUpdate()
	 * @see #getXUpdate()
	 * @generated
	 */

	void setXUpdate(XUpdate value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.updates.Dummy#getXUpdate <em>XUpdate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetXUpdate()
	 * @see #getXUpdate()
	 * @see #setXUpdate(XUpdate)
	 * @generated
	 */
	void unsetXUpdate();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.updates.Dummy#getXUpdate <em>XUpdate</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>XUpdate</em>' reference is set.
	 * @see #unsetXUpdate()
	 * @see #getXUpdate()
	 * @see #setXUpdate(XUpdate)
	 * @generated
	 */
	boolean isSetXUpdate();

	/**
	 * Returns the value of the '<em><b>AAttribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAttribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAttribute</em>' reference.
	 * @see #isSetAAttribute()
	 * @see #unsetAAttribute()
	 * @see #setAAttribute(AAttribute)
	 * @see com.montages.mcore.updates.UpdatesPackage#getDummy_AAttribute()
	 * @model unsettable="true"
	 * @generated
	 */
	AAttribute getAAttribute();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.updates.Dummy#getAAttribute <em>AAttribute</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AAttribute</em>' reference.
	 * @see #isSetAAttribute()
	 * @see #unsetAAttribute()
	 * @see #getAAttribute()
	 * @generated
	 */

	void setAAttribute(AAttribute value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.updates.Dummy#getAAttribute <em>AAttribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAAttribute()
	 * @see #getAAttribute()
	 * @see #setAAttribute(AAttribute)
	 * @generated
	 */
	void unsetAAttribute();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.updates.Dummy#getAAttribute <em>AAttribute</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AAttribute</em>' reference is set.
	 * @see #unsetAAttribute()
	 * @see #getAAttribute()
	 * @see #setAAttribute(AAttribute)
	 * @generated
	 */
	boolean isSetAAttribute();

} // Dummy
