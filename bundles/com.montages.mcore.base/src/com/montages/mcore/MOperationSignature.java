/**
 */
package com.montages.mcore;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EOperation;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MOperation Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MOperationSignature#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.montages.mcore.MOperationSignature#getOperation <em>Operation</em>}</li>
 *   <li>{@link com.montages.mcore.MOperationSignature#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.MOperationSignature#getELabel <em>ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.MOperationSignature#getInternalEOperation <em>Internal EOperation</em>}</li>
 *   <li>{@link com.montages.mcore.MOperationSignature#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMOperationSignature()
 * @model annotation="http://www.xocl.org/OCL label='eLabel'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Signature\'\n' calculatedMandatoryDerive='operation.calculatedMandatory' calculatedSingularDerive='operation.calculatedSingular' calculatedTypeDerive='operation.calculatedType'"
 * @generated
 */

public interface MOperationSignature extends MTyped, MModelElement {
	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MOperationSignatureAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MOperationSignatureAction
	 * @see #setDoAction(MOperationSignatureAction)
	 * @see com.montages.mcore.McorePackage#getMOperationSignature_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::MOperationSignatureAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MOperationSignatureAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MOperationSignature#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MOperationSignatureAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MOperationSignatureAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MOperationSignatureAction trg);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see #isSetParameter()
	 * @see #unsetParameter()
	 * @see com.montages.mcore.McorePackage#getMOperationSignature_Parameter()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MParameter> getParameter();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MOperationSignature#getParameter <em>Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParameter()
	 * @see #getParameter()
	 * @generated
	 */
	void unsetParameter();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MOperationSignature#getParameter <em>Parameter</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parameter</em>' containment reference list is set.
	 * @see #unsetParameter()
	 * @see #getParameter()
	 * @generated
	 */
	boolean isSetParameter();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMOperationSignature_Operation()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsTypeOf(MProperty) then\r\nself.eContainer().oclAsType(MProperty) else null\r\n endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MProperty getOperation();

	/**
	 * Returns the value of the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Classifier</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMOperationSignature_ContainingClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() then null else\r\nif self.eContainer().oclIsKindOf(MProperty) then \r\nself.eContainer().oclAsType(MProperty).containingClassifier else null endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getContainingClassifier();

	/**
	 * Returns the value of the '<em><b>ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ELabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ELabel</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMOperationSignature_ELabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='(if self.operation.oclIsUndefined() then \r\n    \'OPERATION MISSING\' else \r\n     self.operation.eName endif)\r\n.concat(self.signatureAsString())\r\n.concat(\':\')\r\n.concat(self.operation.eTypeLabel)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getELabel();

	/**
	 * Returns the value of the '<em><b>Internal EOperation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal EOperation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal EOperation</em>' reference.
	 * @see #isSetInternalEOperation()
	 * @see #unsetInternalEOperation()
	 * @see #setInternalEOperation(EOperation)
	 * @see com.montages.mcore.McorePackage#getMOperationSignature_InternalEOperation()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Relation to ECore'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EOperation getInternalEOperation();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MOperationSignature#getInternalEOperation <em>Internal EOperation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal EOperation</em>' reference.
	 * @see #isSetInternalEOperation()
	 * @see #unsetInternalEOperation()
	 * @see #getInternalEOperation()
	 * @generated
	 */
	void setInternalEOperation(EOperation value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MOperationSignature#getInternalEOperation <em>Internal EOperation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalEOperation()
	 * @see #getInternalEOperation()
	 * @see #setInternalEOperation(EOperation)
	 * @generated
	 */
	void unsetInternalEOperation();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MOperationSignature#getInternalEOperation <em>Internal EOperation</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal EOperation</em>' reference is set.
	 * @see #unsetInternalEOperation()
	 * @see #getInternalEOperation()
	 * @see #setInternalEOperation(EOperation)
	 * @generated
	 */
	boolean isSetInternalEOperation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.operation <> s.operation then false else\r\nself.parameter->size() = s.parameter->size() and\r\nself.parameter->forAll(p:MParameter|p.type=s.parameter->at(self.parameter->indexOf(p)).type) \r\nendif'"
	 * @generated
	 */
	Boolean sameSignature(MOperationSignature s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='\'(\'.concat(\r\nlet ps:OrderedSet(MParameter) = self.parameter in\r\nif ps->isEmpty()then\'\' else\r\nps->iterate(p:MParameter;res:String=\'\' |\r\n  (if res=\'\' then \'\' else res.concat(\', \') endif)\r\n     .concat(p.eTypeName).concat(if p.singular then \'\' else \'*\' endif))\r\nendif).concat(\')\')'"
	 * @generated
	 */
	String signatureAsString();

} // MOperationSignature
