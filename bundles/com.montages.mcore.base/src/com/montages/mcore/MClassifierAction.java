/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MClassifier Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMClassifierAction()
 * @model
 * @generated
 */
public enum MClassifierAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Create Instance</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CREATE_INSTANCE_VALUE
	 * @generated
	 * @ordered
	 */
	CREATE_INSTANCE(1, "CreateInstance", "Instantiate"),
	/**
	 * The '<em><b>Abstract</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABSTRACT_VALUE
	 * @generated
	 * @ordered
	 */
	ABSTRACT(2, "Abstract", "Abstract"),
	/**
	 * The '<em><b>Specialize</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_VALUE
	 * @generated
	 * @ordered
	 */
	SPECIALIZE(3, "Specialize", "Specialize"),
	/**
	 * The '<em><b>Label</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LABEL_VALUE
	 * @generated
	 * @ordered
	 */
	LABEL(4, "Label", "+ Label"),

	/**
	 * The '<em><b>Constraint</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTRAINT_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTRAINT(5, "Constraint", "+ Constraint"),

	/**
	 * The '<em><b>String Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRING_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	STRING_ATTRIBUTE(6, "StringAttribute", "+ String Attribute"),
	/**
	 * The '<em><b>Boolean Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN_ATTRIBUTE(7, "BooleanAttribute", "+ Boolean Attribute"),
	/**
	 * The '<em><b>Literal Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LITERAL_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	LITERAL_ATTRIBUTE(8, "LiteralAttribute", "+ Literal Attribute"),
	/**
	 * The '<em><b>Integer Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #INTEGER_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGER_ATTRIBUTE(9, "IntegerAttribute", "+ Integer Attribute"),
	/**
	 * The '<em><b>Real Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REAL_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	REAL_ATTRIBUTE(10, "RealAttribute", "+ Real Atrribute"),
	/**
	 * The '<em><b>Date Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_ATTRIBUTE(11, "DateAttribute", "+ Date Attribute"),
	/**
	 * The '<em><b>Unary Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_REFERENCE(12, "UnaryReference", "+ [0..1]Reference"),

	/**
	 * The '<em><b>Nary Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NARY_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	NARY_REFERENCE(13, "NaryReference", "+ [0..*] Reference"),

	/**
	 * The '<em><b>Unary Containment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_CONTAINMENT_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_CONTAINMENT(14, "UnaryContainment", "+ [0..1] Containment"),
	/**
	 * The '<em><b>Nary Containment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NARY_CONTAINMENT_VALUE
	 * @generated
	 * @ordered
	 */
	NARY_CONTAINMENT(15, "NaryContainment", "+ [0..*] Containment"),
	/**
	 * The '<em><b>Operation One Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_ONE_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATION_ONE_PARAMETER(16, "OperationOneParameter", "+ Op, 1 Parameter"),

	/**
	 * The '<em><b>Operation Two Parameters</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_TWO_PARAMETERS_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATION_TWO_PARAMETERS(17, "OperationTwoParameters", "+ Op, 2 Parameters"),

	/**
	 * The '<em><b>Literal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LITERAL_VALUE
	 * @generated
	 * @ordered
	 */
	LITERAL(18, "Literal", "+ Literal"),

	/**
	 * The '<em><b>Group Of Properties</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_OF_PROPERTIES_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_OF_PROPERTIES(19, "GroupOfProperties", "+ Group of Properties"),
	/**
	 * The '<em><b>Resolve Into Container</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESOLVE_INTO_CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	RESOLVE_INTO_CONTAINER(20, "ResolveIntoContainer", "Resolve into Container"),
	/**
	 * The '<em><b>Into Enumeration</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #INTO_ENUMERATION_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_ENUMERATION(21, "IntoEnumeration", "-> Enumeration"),
	/**
	 * The '<em><b>Into Datatype</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_DATATYPE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_DATATYPE(22, "IntoDatatype", "-> Datatype"),
	/**
	 * The '<em><b>Into Abstract Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #INTO_ABSTRACT_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_ABSTRACT_CLASS(23, "IntoAbstractClass", "-> Abstract Class"),
	/**
	 * The '<em><b>Into Concrete Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #INTO_CONCRETE_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_CONCRETE_CLASS(24, "IntoConcreteClass", "-> Concrete Class"),
	/**
	 * The '<em><b>Complete Semantics</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #COMPLETE_SEMANTICS_VALUE
	 * @generated
	 * @ordered
	 */
	COMPLETE_SEMANTICS(25, "CompleteSemantics", "Complete Semantics");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Create Instance</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Create Instance</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CREATE_INSTANCE
	 * @model name="CreateInstance" literal="Instantiate"
	 * @generated
	 * @ordered
	 */
	public static final int CREATE_INSTANCE_VALUE = 1;

	/**
	 * The '<em><b>Abstract</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Abstract</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ABSTRACT
	 * @model name="Abstract"
	 * @generated
	 * @ordered
	 */
	public static final int ABSTRACT_VALUE = 2;

	/**
	 * The '<em><b>Specialize</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Specialize</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE
	 * @model name="Specialize"
	 * @generated
	 * @ordered
	 */
	public static final int SPECIALIZE_VALUE = 3;

	/**
	 * The '<em><b>Label</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Label</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LABEL
	 * @model name="Label" literal="+ Label"
	 * @generated
	 * @ordered
	 */
	public static final int LABEL_VALUE = 4;

	/**
	 * The '<em><b>Constraint</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constraint</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTRAINT
	 * @model name="Constraint" literal="+ Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTRAINT_VALUE = 5;

	/**
	 * The '<em><b>String Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>String Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRING_ATTRIBUTE
	 * @model name="StringAttribute" literal="+ String Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int STRING_ATTRIBUTE_VALUE = 6;

	/**
	 * The '<em><b>Boolean Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Boolean Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_ATTRIBUTE
	 * @model name="BooleanAttribute" literal="+ Boolean Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_ATTRIBUTE_VALUE = 7;

	/**
	 * The '<em><b>Literal Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Literal Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LITERAL_ATTRIBUTE
	 * @model name="LiteralAttribute" literal="+ Literal Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int LITERAL_ATTRIBUTE_VALUE = 8;

	/**
	 * The '<em><b>Integer Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Integer Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTEGER_ATTRIBUTE
	 * @model name="IntegerAttribute" literal="+ Integer Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGER_ATTRIBUTE_VALUE = 9;

	/**
	 * The '<em><b>Real Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Real Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REAL_ATTRIBUTE
	 * @model name="RealAttribute" literal="+ Real Atrribute"
	 * @generated
	 * @ordered
	 */
	public static final int REAL_ATTRIBUTE_VALUE = 10;

	/**
	 * The '<em><b>Date Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE_ATTRIBUTE
	 * @model name="DateAttribute" literal="+ Date Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_ATTRIBUTE_VALUE = 11;

	/**
	 * The '<em><b>Unary Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unary Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_REFERENCE
	 * @model name="UnaryReference" literal="+ [0..1]Reference"
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_REFERENCE_VALUE = 12;

	/**
	 * The '<em><b>Nary Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nary Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NARY_REFERENCE
	 * @model name="NaryReference" literal="+ [0..*] Reference"
	 * @generated
	 * @ordered
	 */
	public static final int NARY_REFERENCE_VALUE = 13;

	/**
	 * The '<em><b>Unary Containment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unary Containment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_CONTAINMENT
	 * @model name="UnaryContainment" literal="+ [0..1] Containment"
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_CONTAINMENT_VALUE = 14;

	/**
	 * The '<em><b>Nary Containment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nary Containment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NARY_CONTAINMENT
	 * @model name="NaryContainment" literal="+ [0..*] Containment"
	 * @generated
	 * @ordered
	 */
	public static final int NARY_CONTAINMENT_VALUE = 15;

	/**
	 * The '<em><b>Operation One Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operation One Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERATION_ONE_PARAMETER
	 * @model name="OperationOneParameter" literal="+ Op, 1 Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int OPERATION_ONE_PARAMETER_VALUE = 16;

	/**
	 * The '<em><b>Operation Two Parameters</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operation Two Parameters</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERATION_TWO_PARAMETERS
	 * @model name="OperationTwoParameters" literal="+ Op, 2 Parameters"
	 * @generated
	 * @ordered
	 */
	public static final int OPERATION_TWO_PARAMETERS_VALUE = 17;

	/**
	 * The '<em><b>Literal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Literal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LITERAL
	 * @model name="Literal" literal="+ Literal"
	 * @generated
	 * @ordered
	 */
	public static final int LITERAL_VALUE = 18;

	/**
	 * The '<em><b>Group Of Properties</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Group Of Properties</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_OF_PROPERTIES
	 * @model name="GroupOfProperties" literal="+ Group of Properties"
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_OF_PROPERTIES_VALUE = 19;

	/**
	 * The '<em><b>Resolve Into Container</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Resolve Into Container</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RESOLVE_INTO_CONTAINER
	 * @model name="ResolveIntoContainer" literal="Resolve into Container"
	 * @generated
	 * @ordered
	 */
	public static final int RESOLVE_INTO_CONTAINER_VALUE = 20;

	/**
	 * The '<em><b>Into Enumeration</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Enumeration</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_ENUMERATION
	 * @model name="IntoEnumeration" literal="-> Enumeration"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_ENUMERATION_VALUE = 21;

	/**
	 * The '<em><b>Into Datatype</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Datatype</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_DATATYPE
	 * @model name="IntoDatatype" literal="-> Datatype"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_DATATYPE_VALUE = 22;

	/**
	 * The '<em><b>Into Abstract Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Abstract Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_ABSTRACT_CLASS
	 * @model name="IntoAbstractClass" literal="-> Abstract Class"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_ABSTRACT_CLASS_VALUE = 23;

	/**
	 * The '<em><b>Into Concrete Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Concrete Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_CONCRETE_CLASS
	 * @model name="IntoConcreteClass" literal="-> Concrete Class"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_CONCRETE_CLASS_VALUE = 24;

	/**
	 * The '<em><b>Complete Semantics</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Complete Semantics</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPLETE_SEMANTICS
	 * @model name="CompleteSemantics" literal="Complete Semantics"
	 * @generated
	 * @ordered
	 */
	public static final int COMPLETE_SEMANTICS_VALUE = 25;

	/**
	 * An array of all the '<em><b>MClassifier Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MClassifierAction[] VALUES_ARRAY = new MClassifierAction[] {
			DO, CREATE_INSTANCE, ABSTRACT, SPECIALIZE, LABEL, CONSTRAINT,
			STRING_ATTRIBUTE, BOOLEAN_ATTRIBUTE, LITERAL_ATTRIBUTE,
			INTEGER_ATTRIBUTE, REAL_ATTRIBUTE, DATE_ATTRIBUTE, UNARY_REFERENCE,
			NARY_REFERENCE, UNARY_CONTAINMENT, NARY_CONTAINMENT,
			OPERATION_ONE_PARAMETER, OPERATION_TWO_PARAMETERS, LITERAL,
			GROUP_OF_PROPERTIES, RESOLVE_INTO_CONTAINER, INTO_ENUMERATION,
			INTO_DATATYPE, INTO_ABSTRACT_CLASS, INTO_CONCRETE_CLASS,
			COMPLETE_SEMANTICS, };

	/**
	 * A public read-only list of all the '<em><b>MClassifier Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MClassifierAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MClassifier Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MClassifierAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MClassifierAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MClassifier Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MClassifierAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MClassifierAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MClassifier Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MClassifierAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case CREATE_INSTANCE_VALUE:
			return CREATE_INSTANCE;
		case ABSTRACT_VALUE:
			return ABSTRACT;
		case SPECIALIZE_VALUE:
			return SPECIALIZE;
		case LABEL_VALUE:
			return LABEL;
		case CONSTRAINT_VALUE:
			return CONSTRAINT;
		case STRING_ATTRIBUTE_VALUE:
			return STRING_ATTRIBUTE;
		case BOOLEAN_ATTRIBUTE_VALUE:
			return BOOLEAN_ATTRIBUTE;
		case LITERAL_ATTRIBUTE_VALUE:
			return LITERAL_ATTRIBUTE;
		case INTEGER_ATTRIBUTE_VALUE:
			return INTEGER_ATTRIBUTE;
		case REAL_ATTRIBUTE_VALUE:
			return REAL_ATTRIBUTE;
		case DATE_ATTRIBUTE_VALUE:
			return DATE_ATTRIBUTE;
		case UNARY_REFERENCE_VALUE:
			return UNARY_REFERENCE;
		case NARY_REFERENCE_VALUE:
			return NARY_REFERENCE;
		case UNARY_CONTAINMENT_VALUE:
			return UNARY_CONTAINMENT;
		case NARY_CONTAINMENT_VALUE:
			return NARY_CONTAINMENT;
		case OPERATION_ONE_PARAMETER_VALUE:
			return OPERATION_ONE_PARAMETER;
		case OPERATION_TWO_PARAMETERS_VALUE:
			return OPERATION_TWO_PARAMETERS;
		case LITERAL_VALUE:
			return LITERAL;
		case GROUP_OF_PROPERTIES_VALUE:
			return GROUP_OF_PROPERTIES;
		case RESOLVE_INTO_CONTAINER_VALUE:
			return RESOLVE_INTO_CONTAINER;
		case INTO_ENUMERATION_VALUE:
			return INTO_ENUMERATION;
		case INTO_DATATYPE_VALUE:
			return INTO_DATATYPE;
		case INTO_ABSTRACT_CLASS_VALUE:
			return INTO_ABSTRACT_CLASS;
		case INTO_CONCRETE_CLASS_VALUE:
			return INTO_CONCRETE_CLASS;
		case COMPLETE_SEMANTICS_VALUE:
			return COMPLETE_SEMANTICS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MClassifierAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MClassifierAction
