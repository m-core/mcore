/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MOperation Signature Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMOperationSignatureAction()
 * @model
 * @generated
 */
public enum MOperationSignatureAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Data Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	DATA_PARAMETER(1, "DataParameter", "+ Data Parameter"),
	/**
	 * The '<em><b>Unary Object Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_OBJECT_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_OBJECT_PARAMETER(2, "UnaryObjectParameter", "+ [0..1] Object Parameter"),
	/**
	 * The '<em><b>Nary Object Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NARY_OBJECT_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	NARY_OBJECT_PARAMETER(3, "NaryObjectParameter", "+ [0..*] Object Parameter");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Data Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Data Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATA_PARAMETER
	 * @model name="DataParameter" literal="+ Data Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int DATA_PARAMETER_VALUE = 1;

	/**
	 * The '<em><b>Unary Object Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unary Object Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_OBJECT_PARAMETER
	 * @model name="UnaryObjectParameter" literal="+ [0..1] Object Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_OBJECT_PARAMETER_VALUE = 2;

	/**
	 * The '<em><b>Nary Object Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nary Object Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NARY_OBJECT_PARAMETER
	 * @model name="NaryObjectParameter" literal="+ [0..*] Object Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int NARY_OBJECT_PARAMETER_VALUE = 3;

	/**
	 * An array of all the '<em><b>MOperation Signature Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MOperationSignatureAction[] VALUES_ARRAY = new MOperationSignatureAction[] {
			DO, DATA_PARAMETER, UNARY_OBJECT_PARAMETER,
			NARY_OBJECT_PARAMETER, };

	/**
	 * A public read-only list of all the '<em><b>MOperation Signature Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MOperationSignatureAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MOperation Signature Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MOperationSignatureAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MOperationSignatureAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MOperation Signature Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MOperationSignatureAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MOperationSignatureAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MOperation Signature Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MOperationSignatureAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case DATA_PARAMETER_VALUE:
			return DATA_PARAMETER;
		case UNARY_OBJECT_PARAMETER_VALUE:
			return UNARY_OBJECT_PARAMETER;
		case NARY_OBJECT_PARAMETER_VALUE:
			return NARY_OBJECT_PARAMETER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MOperationSignatureAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MOperationSignatureAction
