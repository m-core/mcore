package com.montages.mcore.util;

import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.ecore.EAnnotation;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.expr.XOCLElementContext;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.annotations.MAnnotation;

public class McoreOclUtil {

	public static boolean isPropertyAnnotationOverridden(MAnnotation annotation) {
		if (false == annotation.getAnnotatedElement() instanceof MProperty) {
			return false;
		}

		MProperty annotatedProperty = (MProperty) annotation.getAnnotatedElement();
		MClassifier contaniningClassifier = annotation
				.getContainingAbstractPropertyAnnotations()
				.getContainingClassifier();

		return !contaniningClassifier.getOwnedProperty().contains(annotatedProperty);
	}

	public static OCLExpressionContext wrapXOCLContext(EAnnotation eAnnotation, String detailKey) throws DiagnosticException {
		XOCLElementContext context = XOCLElementContext.getElementContext(eAnnotation, detailKey);
		return new OCLExpressionContext(context.getContextClassifier(), 
				context.getVariables(), 
				context.getResultTypes());
	}

}
