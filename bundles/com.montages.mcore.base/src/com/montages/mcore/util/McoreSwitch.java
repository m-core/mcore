/**
 */
package com.montages.mcore.util;

import com.montages.mcore.*;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;
import org.langlets.acore.classifiers.AClassifier;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MExplicitlyTyped;
import com.montages.mcore.MExplicitlyTypedAndNamed;
import com.montages.mcore.MHasSimpleType;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MNamed;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage
 * @generated
 */
public class McoreSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static McorePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public McoreSwitch() {
		if (modelPackage == null) {
			modelPackage = McorePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case McorePackage.MREPOSITORY_ELEMENT: {
			MRepositoryElement mRepositoryElement = (MRepositoryElement) theEObject;
			T result = caseMRepositoryElement(mRepositoryElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MREPOSITORY: {
			MRepository mRepository = (MRepository) theEObject;
			T result = caseMRepository(mRepository);
			if (result == null)
				result = caseMRepositoryElement(mRepository);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MNAMED: {
			MNamed mNamed = (MNamed) theEObject;
			T result = caseMNamed(mNamed);
			if (result == null)
				result = caseMRepositoryElement(mNamed);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MCOMPONENT: {
			MComponent mComponent = (MComponent) theEObject;
			T result = caseMComponent(mComponent);
			if (result == null)
				result = caseMNamed(mComponent);
			if (result == null)
				result = caseMRepositoryElement(mComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MMODEL_ELEMENT: {
			MModelElement mModelElement = (MModelElement) theEObject;
			T result = caseMModelElement(mModelElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MPACKAGE: {
			MPackage mPackage = (MPackage) theEObject;
			T result = caseMPackage(mPackage);
			if (result == null)
				result = caseMNamed(mPackage);
			if (result == null)
				result = caseMModelElement(mPackage);
			if (result == null)
				result = caseMRepositoryElement(mPackage);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MHAS_SIMPLE_TYPE: {
			MHasSimpleType mHasSimpleType = (MHasSimpleType) theEObject;
			T result = caseMHasSimpleType(mHasSimpleType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MCLASSIFIER: {
			MClassifier mClassifier = (MClassifier) theEObject;
			T result = caseMClassifier(mClassifier);
			if (result == null)
				result = caseMPropertiesContainer(mClassifier);
			if (result == null)
				result = caseMHasSimpleType(mClassifier);
			if (result == null)
				result = caseMNamed(mClassifier);
			if (result == null)
				result = caseMModelElement(mClassifier);
			if (result == null)
				result = caseMRepositoryElement(mClassifier);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MLITERAL: {
			MLiteral mLiteral = (MLiteral) theEObject;
			T result = caseMLiteral(mLiteral);
			if (result == null)
				result = caseMNamed(mLiteral);
			if (result == null)
				result = caseMModelElement(mLiteral);
			if (result == null)
				result = caseMRepositoryElement(mLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MPROPERTIES_CONTAINER: {
			MPropertiesContainer mPropertiesContainer = (MPropertiesContainer) theEObject;
			T result = caseMPropertiesContainer(mPropertiesContainer);
			if (result == null)
				result = caseMNamed(mPropertiesContainer);
			if (result == null)
				result = caseMModelElement(mPropertiesContainer);
			if (result == null)
				result = caseMRepositoryElement(mPropertiesContainer);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MPROPERTIES_GROUP: {
			MPropertiesGroup mPropertiesGroup = (MPropertiesGroup) theEObject;
			T result = caseMPropertiesGroup(mPropertiesGroup);
			if (result == null)
				result = caseMPropertiesContainer(mPropertiesGroup);
			if (result == null)
				result = caseMNamed(mPropertiesGroup);
			if (result == null)
				result = caseMModelElement(mPropertiesGroup);
			if (result == null)
				result = caseMRepositoryElement(mPropertiesGroup);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MPROPERTY: {
			MProperty mProperty = (MProperty) theEObject;
			T result = caseMProperty(mProperty);
			if (result == null)
				result = caseMExplicitlyTypedAndNamed(mProperty);
			if (result == null)
				result = caseMModelElement(mProperty);
			if (result == null)
				result = caseMNamed(mProperty);
			if (result == null)
				result = caseMExplicitlyTyped(mProperty);
			if (result == null)
				result = caseMTyped(mProperty);
			if (result == null)
				result = caseMHasSimpleType(mProperty);
			if (result == null)
				result = caseMRepositoryElement(mProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MOPERATION_SIGNATURE: {
			MOperationSignature mOperationSignature = (MOperationSignature) theEObject;
			T result = caseMOperationSignature(mOperationSignature);
			if (result == null)
				result = caseMTyped(mOperationSignature);
			if (result == null)
				result = caseMModelElement(mOperationSignature);
			if (result == null)
				result = caseMRepositoryElement(mOperationSignature);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MTYPED: {
			MTyped mTyped = (MTyped) theEObject;
			T result = caseMTyped(mTyped);
			if (result == null)
				result = caseMRepositoryElement(mTyped);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MEXPLICITLY_TYPED: {
			MExplicitlyTyped mExplicitlyTyped = (MExplicitlyTyped) theEObject;
			T result = caseMExplicitlyTyped(mExplicitlyTyped);
			if (result == null)
				result = caseMTyped(mExplicitlyTyped);
			if (result == null)
				result = caseMHasSimpleType(mExplicitlyTyped);
			if (result == null)
				result = caseMRepositoryElement(mExplicitlyTyped);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MVARIABLE: {
			MVariable mVariable = (MVariable) theEObject;
			T result = caseMVariable(mVariable);
			if (result == null)
				result = caseMNamed(mVariable);
			if (result == null)
				result = caseMTyped(mVariable);
			if (result == null)
				result = caseMRepositoryElement(mVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED: {
			MExplicitlyTypedAndNamed mExplicitlyTypedAndNamed = (MExplicitlyTypedAndNamed) theEObject;
			T result = caseMExplicitlyTypedAndNamed(mExplicitlyTypedAndNamed);
			if (result == null)
				result = caseMNamed(mExplicitlyTypedAndNamed);
			if (result == null)
				result = caseMExplicitlyTyped(mExplicitlyTypedAndNamed);
			if (result == null)
				result = caseMTyped(mExplicitlyTypedAndNamed);
			if (result == null)
				result = caseMHasSimpleType(mExplicitlyTypedAndNamed);
			if (result == null)
				result = caseMRepositoryElement(mExplicitlyTypedAndNamed);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MPARAMETER: {
			MParameter mParameter = (MParameter) theEObject;
			T result = caseMParameter(mParameter);
			if (result == null)
				result = caseMExplicitlyTypedAndNamed(mParameter);
			if (result == null)
				result = caseMVariable(mParameter);
			if (result == null)
				result = caseMModelElement(mParameter);
			if (result == null)
				result = caseAClassifier(mParameter);
			if (result == null)
				result = caseMNamed(mParameter);
			if (result == null)
				result = caseMExplicitlyTyped(mParameter);
			if (result == null)
				result = caseANamed(mParameter);
			if (result == null)
				result = caseMTyped(mParameter);
			if (result == null)
				result = caseMHasSimpleType(mParameter);
			if (result == null)
				result = caseAElement(mParameter);
			if (result == null)
				result = caseMRepositoryElement(mParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MEDITOR: {
			MEditor mEditor = (MEditor) theEObject;
			T result = caseMEditor(mEditor);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case McorePackage.MNAMED_EDITOR: {
			MNamedEditor mNamedEditor = (MNamedEditor) theEObject;
			T result = caseMNamedEditor(mNamedEditor);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMRepositoryElement(MRepositoryElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MRepository</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MRepository</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMRepository(MRepository object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamed(MNamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MComponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MComponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMComponent(MComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MModel Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MModel Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMModelElement(MModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPackage(MPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MHas Simple Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MHas Simple Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMHasSimpleType(MHasSimpleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MClassifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MClassifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMClassifier(MClassifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLiteral</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLiteral</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLiteral(MLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MProperties Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MProperties Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPropertiesContainer(MPropertiesContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MProperties Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MProperties Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPropertiesGroup(MPropertiesGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MProperty</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MProperty</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMProperty(MProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MOperation Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MOperation Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMOperationSignature(MOperationSignature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTyped</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTyped</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTyped(MTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MExplicitly Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MExplicitly Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMExplicitlyTyped(MExplicitlyTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MVariable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MVariable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMVariable(MVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MExplicitly Typed And Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MExplicitly Typed And Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMExplicitlyTypedAndNamed(MExplicitlyTypedAndNamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MParameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MParameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMParameter(MParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MEditor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MEditor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMEditor(MEditor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed Editor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed Editor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamedEditor(MNamedEditor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAElement(AElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ANamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ANamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseANamed(ANamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AClassifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AClassifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAClassifier(AClassifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //McoreSwitch
