package com.montages.mcore.util;

import java.util.Map;

import org.eclipse.emf.common.util.URI;

import com.montages.mcore.MComponent;

public class URIService {

	public URI createComponentURI(MComponent component) {
		if (component == null) return null;

		return URI.createURI(component.getDerivedURI().replace("http:", "mcore:"));
	}

	public URI createPlatformResource(MComponent component) {
		if (component == null) return null;

		final String folder = component.getDerivedBundleName() + "/model/";

		return URI.createPlatformResourceURI(folder + componentFileName(component), true);
	}

	public URI createComponentLocationURI(MComponent component) {
		URI componentURI = createComponentURI(component);
		URI locationURI = createPlatformResource(component);

		return createComponentLocationURI(componentURI, locationURI);
	}

	public URI createComponentLocationURI(URI componentURI, URI locationURI) {
		if (componentURI == null || locationURI == null) { 
			return null;
		}

		return componentURI.appendSegment(locationURI.lastSegment());
	}

	public URI addUriMapping(URI componentURI, URI locationURI, Map<URI, URI> map) {
		if (componentURI == null || locationURI == null) return null;

		final URI componentFolderURI = componentURI.appendSegment("");
		final URI locationFolderURI = locationURI.trimSegments(1).appendSegment("");

		componentURI = createComponentLocationURI(componentURI, locationURI);
		map.put(componentURI, locationURI);
		map.put(componentFolderURI, locationFolderURI);

		return componentURI;
	}

	public String componentFileName(MComponent component) {
		return component.getName().trim().replaceAll("\\s", "").toLowerCase() + ".mcore";
	}
}
