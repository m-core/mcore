/**
 */
package com.montages.mcore.util;

import com.montages.mcore.*;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;
import org.langlets.acore.classifiers.AClassifier;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MExplicitlyTyped;
import com.montages.mcore.MExplicitlyTypedAndNamed;
import com.montages.mcore.MHasSimpleType;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MNamed;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage
 * @generated
 */
public class McoreAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static McorePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public McoreAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = McorePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected McoreSwitch<Adapter> modelSwitch = new McoreSwitch<Adapter>() {
		@Override
		public Adapter caseMRepositoryElement(MRepositoryElement object) {
			return createMRepositoryElementAdapter();
		}

		@Override
		public Adapter caseMRepository(MRepository object) {
			return createMRepositoryAdapter();
		}

		@Override
		public Adapter caseMNamed(MNamed object) {
			return createMNamedAdapter();
		}

		@Override
		public Adapter caseMComponent(MComponent object) {
			return createMComponentAdapter();
		}

		@Override
		public Adapter caseMModelElement(MModelElement object) {
			return createMModelElementAdapter();
		}

		@Override
		public Adapter caseMPackage(MPackage object) {
			return createMPackageAdapter();
		}

		@Override
		public Adapter caseMHasSimpleType(MHasSimpleType object) {
			return createMHasSimpleTypeAdapter();
		}

		@Override
		public Adapter caseMClassifier(MClassifier object) {
			return createMClassifierAdapter();
		}

		@Override
		public Adapter caseMLiteral(MLiteral object) {
			return createMLiteralAdapter();
		}

		@Override
		public Adapter caseMPropertiesContainer(MPropertiesContainer object) {
			return createMPropertiesContainerAdapter();
		}

		@Override
		public Adapter caseMPropertiesGroup(MPropertiesGroup object) {
			return createMPropertiesGroupAdapter();
		}

		@Override
		public Adapter caseMProperty(MProperty object) {
			return createMPropertyAdapter();
		}

		@Override
		public Adapter caseMOperationSignature(MOperationSignature object) {
			return createMOperationSignatureAdapter();
		}

		@Override
		public Adapter caseMTyped(MTyped object) {
			return createMTypedAdapter();
		}

		@Override
		public Adapter caseMExplicitlyTyped(MExplicitlyTyped object) {
			return createMExplicitlyTypedAdapter();
		}

		@Override
		public Adapter caseMVariable(MVariable object) {
			return createMVariableAdapter();
		}

		@Override
		public Adapter caseMExplicitlyTypedAndNamed(
				MExplicitlyTypedAndNamed object) {
			return createMExplicitlyTypedAndNamedAdapter();
		}

		@Override
		public Adapter caseMParameter(MParameter object) {
			return createMParameterAdapter();
		}

		@Override
		public Adapter caseMEditor(MEditor object) {
			return createMEditorAdapter();
		}

		@Override
		public Adapter caseMNamedEditor(MNamedEditor object) {
			return createMNamedEditorAdapter();
		}

		@Override
		public Adapter caseAElement(AElement object) {
			return createAElementAdapter();
		}

		@Override
		public Adapter caseANamed(ANamed object) {
			return createANamedAdapter();
		}

		@Override
		public Adapter caseAClassifier(AClassifier object) {
			return createAClassifierAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MRepositoryElement <em>MRepository Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MRepositoryElement
	 * @generated
	 */
	public Adapter createMRepositoryElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MRepository <em>MRepository</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MRepository
	 * @generated
	 */
	public Adapter createMRepositoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MNamed <em>MNamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MNamed
	 * @generated
	 */
	public Adapter createMNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MComponent <em>MComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MComponent
	 * @generated
	 */
	public Adapter createMComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MModelElement <em>MModel Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MModelElement
	 * @generated
	 */
	public Adapter createMModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MPackage <em>MPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MPackage
	 * @generated
	 */
	public Adapter createMPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MHasSimpleType <em>MHas Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MHasSimpleType
	 * @generated
	 */
	public Adapter createMHasSimpleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MClassifier <em>MClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MClassifier
	 * @generated
	 */
	public Adapter createMClassifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MLiteral <em>MLiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MLiteral
	 * @generated
	 */
	public Adapter createMLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MPropertiesContainer <em>MProperties Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MPropertiesContainer
	 * @generated
	 */
	public Adapter createMPropertiesContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MPropertiesGroup <em>MProperties Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MPropertiesGroup
	 * @generated
	 */
	public Adapter createMPropertiesGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MProperty <em>MProperty</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MProperty
	 * @generated
	 */
	public Adapter createMPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MOperationSignature <em>MOperation Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MOperationSignature
	 * @generated
	 */
	public Adapter createMOperationSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MTyped <em>MTyped</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MTyped
	 * @generated
	 */
	public Adapter createMTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MExplicitlyTyped <em>MExplicitly Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MExplicitlyTyped
	 * @generated
	 */
	public Adapter createMExplicitlyTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MVariable <em>MVariable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MVariable
	 * @generated
	 */
	public Adapter createMVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MExplicitlyTypedAndNamed <em>MExplicitly Typed And Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MExplicitlyTypedAndNamed
	 * @generated
	 */
	public Adapter createMExplicitlyTypedAndNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MParameter <em>MParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MParameter
	 * @generated
	 */
	public Adapter createMParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MEditor <em>MEditor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MEditor
	 * @generated
	 */
	public Adapter createMEditorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MNamedEditor <em>MNamed Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MNamedEditor
	 * @generated
	 */
	public Adapter createMNamedEditorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.abstractions.AElement
	 * @generated
	 */
	public Adapter createAElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.abstractions.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.abstractions.ANamed
	 * @generated
	 */
	public Adapter createANamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.classifiers.AClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.classifiers.AClassifier
	 * @generated
	 */
	public Adapter createAClassifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //McoreAdapterFactory
