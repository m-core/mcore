package com.montages.mcore.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLLoad;
import org.eclipse.emf.ecore.xmi.XMLParserPool;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.XMLResource.URIHandler;
import org.eclipse.emf.ecore.xmi.impl.SAXXMIHandler;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMILoadImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLParserPoolImpl;
import org.xml.sax.helpers.DefaultHandler;
import org.xocl.editorconfig.util.EditorConfigURIHandler;

import com.montages.common.CommonPlugin;

public class ResourceService {

	public static ResourceSet createResourceSet() {
		ResourceSetImpl result = new ResourceSetImpl();
		result.getURIConverter().getURIMap().putAll(CommonPlugin.computeURIMap());
		return result;
	}

	public static ResourceSet getResourceSet(EObject object) {
		return get(object.eResource() != null ? object.eResource().getResourceSet() : null);
	}

	public static ResourceSet get(ResourceSet resourceSet) {
		if (resourceSet == null) {
			resourceSet = createResourceSet();
		}
		return resourceSet;
	}

	public static URI transformResourceToPlugin(URI uri) {
		if (uri == null || !uri.isPlatformResource()) {
			return uri;
		}
		return URI.createURI(uri.toString().replace("platform:/resource/", "platform:/plugin/"));
	}

	public static URI normalize(ResourceSet rs, URI uri) {
		if (rs == null || uri == null) {
			return null;
		}
		return rs.getURIConverter().normalize(uri);
	}

	public static URI normalizeResourceURI(ResourceSet rs, EObject eObject) {
		Resource eResource = eObject.eResource();
		if (eResource == null) {
			return null;
		}
		return rs.getURIConverter().normalize(eResource.getURI());
	}

	public static URI normalizeResourceURI(EObject eObject) {
		Resource eResource = eObject.eResource();
		if (eResource == null) {
			return null;
		}
		return normalize(eResource.getResourceSet(), eResource.getURI());
	}

	public static ResourceSet addPluginMap(ResourceSet resourceSet) {
		if (resourceSet == null) {
			resourceSet = createResourceSet();
		}

		Map<URI, URI> map = new HashMap<URI, URI>();
		CommonPlugin.computePluginModels(map);
		CommonPlugin.computeWorkspaceModels(map);
		resourceSet.getURIConverter().getURIMap().putAll(map);

		return resourceSet;
	}

	public static Map<String, Object> getSaveOptions() {
		Map<String, Object> options = new HashMap<String, Object>();
		options.put(XMLResource.OPTION_ENCODING, "UTF-8");
		options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);

		return options;
	}

	public static Map<String, Object> getMcoreOptions(final ResourceSet resourceSet) {
		return getMcoreOptions(resourceSet, null);
	}

	public static Map<String, Object> getMcoreOptions(final ResourceSet resourceSet, final RenameService renameService) {
		Map<String, Object> options = getSaveOptions();
		options.put(XMLResource.OPTION_URI_HANDLER, getMcoreURIHandler(resourceSet, renameService));

		return options;
	}

	private static URIHandler getMcoreURIHandler(final ResourceSet resourceSet, final RenameService renameService) {
		return new URIHandlerImpl() {

			@Override
			public URI deresolve(URI uri) {
				URI deresolvedURI = null;

				if (resolve && !uri.isRelative()) {
					
					if (uri.fileExtension() != null && uri.fileExtension().equals("ecore")) {
						URI base = resourceSet.getURIConverter().normalize(baseURI);
						URI normalized = resourceSet.getURIConverter().normalize(uri);
						deresolvedURI = normalized.deresolve(base, true, true, false);
					}
					if (deresolvedURI == null && "mcore".equals(uri.scheme())) {
						deresolvedURI = uri;
					}
					if (deresolvedURI == null && uri.fileExtension() != null && uri.fileExtension().equals("mcore")) {
						deresolvedURI = uri;
						if (deresolvedURI.isPlatform()) {
							String uriString = deresolvedURI.toString();
							int length = uriString.length();
							for (URI mcoreURI: resourceSet.getURIConverter().getURIMap().keySet()) {
								URI normURI = resourceSet.getURIConverter().getURIMap().get(mcoreURI);
								String resolvedURIString = normURI.toString();
								if (length > resolvedURIString.length() && uriString.startsWith(resolvedURIString)) {
									uri = URI.createURI(uriString.replace(resolvedURIString, mcoreURI.toString()));
									break;
								}
							}
						}
					}
					if (deresolvedURI == null) {
						deresolvedURI = uri.deresolve(baseURI, true, true, false);
					}
				}

				if (deresolvedURI != null && deresolvedURI.hasRelativePath()) {
					uri = deresolvedURI;
				}

				return uri;
			}

			@Override
			public URI resolve(URI uri) {
				if (resolve && uri.isRelative() && uri.hasRelativePath()) {
					if ("mcore".equals(baseURI.scheme()) && "ecore".equals(uri.fileExtension())) {
						uri = uri.resolve(resourceSet.getURIConverter().normalize(baseURI));
					} else {
						uri = uri.resolve(baseURI);
					}
				}
				return uri;
			}
		};
	}

	public Map<String, Object> getEditorConfigSaveOptions(ResourceSet resourceSet) {
		return getEditorConfigSaveOptions(resourceSet, null);
	}

	public Map<String, Object> getEditorConfigSaveOptions(final ResourceSet resourceSet, final RenameService renameService) {
		Map<String, Object> options = new HashMap<String, Object>();
		options.put(XMLResource.OPTION_ENCODING, "UTF-8");
		options.put(XMLResource.OPTION_URI_HANDLER, new EditorConfigURIHandler(resourceSet) {

			@Override
			public URI deresolve(URI uri) {
				if (resolve) {
					if (renameService != null) {
						if ("http".equals(uri.scheme())) {
							uri = uriDeresolve(renameService, uri);
						}
						if (uri.isPlatformResource()) {
							uri = resourceDeresolve(renameService, uri);
						}
					}
					uri = super.deresolve(uri);
				}
				return uri;
			}
		});

		return options;
	}

	protected URI resourceDeresolve(final RenameService renameService, URI uri) {
		return uri;
	}

	protected URI uriDeresolve(final RenameService renameService, URI uri) {
		String scheme = uri.scheme() + "://";
		String uriString = uri.trimQuery().toString().replaceFirst(scheme, "");
		uriString = renameService.updateRef(uriString);
		uri = URI.createURI(scheme+uriString).appendQuery(uri.query());
		return uri;
	}

	public static XMIResource createXMLResourceWithUUID(URI uri) {

		XMIResource result = new XMIResourceImpl(uri) {

			@Override
			protected boolean useUUIDs() {
				return true;
			}

			@Override
			protected XMLLoad createXMLLoad() {
				return new XMILoadImpl(createXMLHelper()) {

					@Override
					protected DefaultHandler makeDefaultHandler() {
						return new SAXXMIHandler(resource, helper, options) {

							@Override
							protected void handleObjectAttribs(EObject obj) {
								if (attribs != null) {
									InternalEObject internalEObject = (InternalEObject) obj;
									for (int i = 0, size = attribs.getLength(); i < size; ++i) {
										String name = attribs.getQName(i);

										// one string diff from SAXXMLHandler#handleObjectAttribs
										// the original string
										// if (name.equals(ID_ATTRIB))
										if (name.equals(idAttribute)) {
											xmlResource.setID(internalEObject, attribs.getValue(i));
										} else if (name.equals(hrefAttribute) && (!recordUnknownFeature || types.peek() != UNKNOWN_FEATURE_TYPE || obj.eClass() != anyType)) {
											handleProxy(internalEObject, attribs.getValue(i));
										} else if (isNamespaceAware) {
											String namespace = attribs.getURI(i);
											if (!ExtendedMetaData.XSI_URI.equals(namespace) && !notFeatures.contains(name)) {
												setAttribValue(obj, name, attribs.getValue(i));
											}
										} else if (!name.startsWith(XMLResource.XML_NS) && !notFeatures.contains(name)) {
											setAttribValue(obj, name, attribs.getValue(i));
										}
									}
								}
							}
						};
					}
				};
			}
		};

		final List<Object> lookupTable = new ArrayList<Object>();
		final XMLParserPool parserPool = new XMLParserPoolImpl();

		XMLResource.XMLMap xmlMap = new XMLMapImpl();
		xmlMap.setIDAttributeName("_uuid");
		result.getDefaultSaveOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		result.getDefaultLoadOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);

		Map<Object, Object> saveOptions = result.getDefaultSaveOptions();
		saveOptions.put(XMLResource.OPTION_CONFIGURATION_CACHE, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_USE_CACHED_LOOKUP_TABLE, lookupTable);
		saveOptions.put(XMLResource.OPTION_ENCODING, "UTF-8");

		Map<Object, Object> loadOptions = result.getDefaultLoadOptions();
		loadOptions.put(XMLResource.OPTION_DEFER_ATTACHMENT, Boolean.TRUE);
		loadOptions.put(XMLResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
		loadOptions.put(XMLResource.OPTION_USE_DEPRECATED_METHODS, Boolean.FALSE);
		loadOptions.put(XMLResource.OPTION_USE_PARSER_POOL, parserPool);

		return result;
	}

	public static ExtensibleURIConverterImpl createURIConverter() {
		ExtensibleURIConverterImpl uriConverter = new ExtensibleURIConverterImpl();
		uriConverter.getURIMap().putAll(CommonPlugin.computeURIMap());
		return uriConverter;
	}
}
