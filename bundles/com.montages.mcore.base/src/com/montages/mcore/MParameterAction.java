/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MParameter Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMParameterAction()
 * @model
 * @generated
 */
public enum MParameterAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Into Unary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_UNARY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_UNARY(1, "IntoUnary", "-> [0..1]"),

	/**
	 * The '<em><b>Into Nary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_NARY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_NARY(2, "IntoNary", "-> [0..*]"),

	/**
	 * The '<em><b>Into String Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_STRING_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_STRING_ATTRIBUTE(3, "IntoStringAttribute", "-> String Parameter"),

	/**
	 * The '<em><b>Into Boolean Flag</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_BOOLEAN_FLAG_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_BOOLEAN_FLAG(4, "IntoBooleanFlag", "-> Boolean Parameter"),

	/**
	 * The '<em><b>Into Integer Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_INTEGER_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_INTEGER_ATTRIBUTE(5, "IntoIntegerAttribute", "-> Integer Parameter"),

	/**
	 * The '<em><b>Into Real Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_REAL_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_REAL_ATTRIBUTE(6, "IntoRealAttribute", "-> Real Parameter"),

	/**
	 * The '<em><b>Into Date Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_DATE_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_DATE_ATTRIBUTE(7, "IntoDateAttribute", "-> Date Parameter");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Into Unary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Unary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_UNARY
	 * @model name="IntoUnary" literal="-> [0..1]"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_UNARY_VALUE = 1;

	/**
	 * The '<em><b>Into Nary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Nary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_NARY
	 * @model name="IntoNary" literal="-> [0..*]"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_NARY_VALUE = 2;

	/**
	 * The '<em><b>Into String Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into String Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_STRING_ATTRIBUTE
	 * @model name="IntoStringAttribute" literal="-> String Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_STRING_ATTRIBUTE_VALUE = 3;

	/**
	 * The '<em><b>Into Boolean Flag</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Boolean Flag</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_BOOLEAN_FLAG
	 * @model name="IntoBooleanFlag" literal="-> Boolean Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_BOOLEAN_FLAG_VALUE = 4;

	/**
	 * The '<em><b>Into Integer Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Integer Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_INTEGER_ATTRIBUTE
	 * @model name="IntoIntegerAttribute" literal="-> Integer Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_INTEGER_ATTRIBUTE_VALUE = 5;

	/**
	 * The '<em><b>Into Real Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Real Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_REAL_ATTRIBUTE
	 * @model name="IntoRealAttribute" literal="-> Real Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_REAL_ATTRIBUTE_VALUE = 6;

	/**
	 * The '<em><b>Into Date Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Date Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_DATE_ATTRIBUTE
	 * @model name="IntoDateAttribute" literal="-> Date Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_DATE_ATTRIBUTE_VALUE = 7;

	/**
	 * An array of all the '<em><b>MParameter Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MParameterAction[] VALUES_ARRAY = new MParameterAction[] {
			DO, INTO_UNARY, INTO_NARY, INTO_STRING_ATTRIBUTE, INTO_BOOLEAN_FLAG,
			INTO_INTEGER_ATTRIBUTE, INTO_REAL_ATTRIBUTE, INTO_DATE_ATTRIBUTE, };

	/**
	 * A public read-only list of all the '<em><b>MParameter Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MParameterAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MParameter Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MParameterAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MParameterAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MParameter Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MParameterAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MParameterAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MParameter Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MParameterAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case INTO_UNARY_VALUE:
			return INTO_UNARY;
		case INTO_NARY_VALUE:
			return INTO_NARY;
		case INTO_STRING_ATTRIBUTE_VALUE:
			return INTO_STRING_ATTRIBUTE;
		case INTO_BOOLEAN_FLAG_VALUE:
			return INTO_BOOLEAN_FLAG;
		case INTO_INTEGER_ATTRIBUTE_VALUE:
			return INTO_INTEGER_ATTRIBUTE;
		case INTO_REAL_ATTRIBUTE_VALUE:
			return INTO_REAL_ATTRIBUTE;
		case INTO_DATE_ATTRIBUTE_VALUE:
			return INTO_DATE_ATTRIBUTE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MParameterAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MParameterAction
