/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage
 * @generated
 */
public interface McoreFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	McoreFactory eINSTANCE = com.montages.mcore.impl.McoreFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MRepository</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MRepository</em>'.
	 * @generated
	 */
	MRepository createMRepository();

	/**
	 * Returns a new object of class '<em>MComponent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MComponent</em>'.
	 * @generated
	 */
	MComponent createMComponent();

	/**
	 * Returns a new object of class '<em>MPackage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPackage</em>'.
	 * @generated
	 */
	MPackage createMPackage();

	/**
	 * Returns a new object of class '<em>MClassifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MClassifier</em>'.
	 * @generated
	 */
	MClassifier createMClassifier();

	/**
	 * Returns a new object of class '<em>MLiteral</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLiteral</em>'.
	 * @generated
	 */
	MLiteral createMLiteral();

	/**
	 * Returns a new object of class '<em>MProperties Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MProperties Group</em>'.
	 * @generated
	 */
	MPropertiesGroup createMPropertiesGroup();

	/**
	 * Returns a new object of class '<em>MProperty</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MProperty</em>'.
	 * @generated
	 */
	MProperty createMProperty();

	/**
	 * Returns a new object of class '<em>MOperation Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MOperation Signature</em>'.
	 * @generated
	 */
	MOperationSignature createMOperationSignature();

	/**
	 * Returns a new object of class '<em>MParameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MParameter</em>'.
	 * @generated
	 */
	MParameter createMParameter();

	/**
	 * Returns a new object of class '<em>MNamed Editor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MNamed Editor</em>'.
	 * @generated
	 */
	MNamedEditor createMNamedEditor();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	McorePackage getMcorePackage();

} //McoreFactory
