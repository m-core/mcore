
/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNamed Editor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MNamedEditor#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamedEditor#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mcore.MNamedEditor#getUserVisible <em>User Visible</em>}</li>
 *   <li>{@link com.montages.mcore.MNamedEditor#getEditor <em>Editor</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMNamedEditor()
 * @model
 * @generated
 */

public interface MNamedEditor extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(String)
	 * @see com.montages.mcore.McorePackage#getMNamedEditor_Name()
	 * @model unsettable="true"
	 * @generated
	 */
	String getName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamedEditor#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamedEditor#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamedEditor#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #setLabel(String)
	 * @see com.montages.mcore.McorePackage#getMNamedEditor_Label()
	 * @model unsettable="true"
	 * @generated
	 */
	String getLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamedEditor#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamedEditor#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	void unsetLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamedEditor#getLabel <em>Label</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Label</em>' attribute is set.
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	boolean isSetLabel();

	/**
	 * Returns the value of the '<em><b>User Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Visible</em>' attribute.
	 * @see #isSetUserVisible()
	 * @see #unsetUserVisible()
	 * @see #setUserVisible(Boolean)
	 * @see com.montages.mcore.McorePackage#getMNamedEditor_UserVisible()
	 * @model unsettable="true"
	 * @generated
	 */
	Boolean getUserVisible();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamedEditor#getUserVisible <em>User Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Visible</em>' attribute.
	 * @see #isSetUserVisible()
	 * @see #unsetUserVisible()
	 * @see #getUserVisible()
	 * @generated
	 */
	void setUserVisible(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamedEditor#getUserVisible <em>User Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUserVisible()
	 * @see #getUserVisible()
	 * @see #setUserVisible(Boolean)
	 * @generated
	 */
	void unsetUserVisible();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamedEditor#getUserVisible <em>User Visible</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>User Visible</em>' attribute is set.
	 * @see #unsetUserVisible()
	 * @see #getUserVisible()
	 * @see #setUserVisible(Boolean)
	 * @generated
	 */
	boolean isSetUserVisible();

	/**
	 * Returns the value of the '<em><b>Editor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor</em>' reference.
	 * @see #isSetEditor()
	 * @see #unsetEditor()
	 * @see #setEditor(MEditor)
	 * @see com.montages.mcore.McorePackage#getMNamedEditor_Editor()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	MEditor getEditor();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamedEditor#getEditor <em>Editor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor</em>' reference.
	 * @see #isSetEditor()
	 * @see #unsetEditor()
	 * @see #getEditor()
	 * @generated
	 */
	void setEditor(MEditor value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamedEditor#getEditor <em>Editor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEditor()
	 * @see #getEditor()
	 * @see #setEditor(MEditor)
	 * @generated
	 */
	void unsetEditor();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamedEditor#getEditor <em>Editor</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Editor</em>' reference is set.
	 * @see #unsetEditor()
	 * @see #getEditor()
	 * @see #setEditor(MEditor)
	 * @generated
	 */
	boolean isSetEditor();

} // MNamedEditor
