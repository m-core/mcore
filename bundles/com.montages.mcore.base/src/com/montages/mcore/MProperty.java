/**
 */
package com.montages.mcore;

import com.montages.mcore.annotations.MOperationAnnotations;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.annotations.MPropertyAnnotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MProperty</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MProperty#getOperationSignature <em>Operation Signature</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getOppositeDefinition <em>Opposite Definition</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getContainment <em>Containment</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getHasStorage <em>Has Storage</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getChangeable <em>Changeable</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getPersisted <em>Persisted</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getPropertyBehavior <em>Property Behavior</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getIsOperation <em>Is Operation</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getIsAttribute <em>Is Attribute</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getIsReference <em>Is Reference</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getKind <em>Kind</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getTypeDefinition <em>Type Definition</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getUseCoreTypes <em>Use Core Types</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getELabel <em>ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getENameForELabel <em>EName For ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getOrderWithinGroup <em>Order Within Group</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getId <em>Id</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getInternalEStructuralFeature <em>Internal EStructural Feature</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getDirectSemantics <em>Direct Semantics</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getSemanticsSpecialization <em>Semantics Specialization</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getAllSignatures <em>All Signatures</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getDirectOperationSemantics <em>Direct Operation Semantics</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getContainingPropertiesContainer <em>Containing Properties Container</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getEKeyForPath <em>EKey For Path</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getNotUnsettable <em>Not Unsettable</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getAttributeForEId <em>Attribute For EId</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getEDefaultValueLiteral <em>EDefault Value Literal</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getBehaviorActions <em>Behavior Actions</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getLayoutActions <em>Layout Actions</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getArityActions <em>Arity Actions</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getSimpleTypeActions <em>Simple Type Actions</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getAnnotationActions <em>Annotation Actions</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getMoveActions <em>Move Actions</em>}</li>
 *   <li>{@link com.montages.mcore.MProperty#getDerivedJsonSchema <em>Derived Json Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMProperty()
 * @model annotation="http://www.xocl.org/OCL label='eLabel '"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL fullLabelDerive='(if kind=PropertyKind::Attribute then \'[attribute] \' else\r\nif kind=PropertyKind::Reference then \'[reference] \' else\r\nif kind=PropertyKind::Operation then \'[operation] \' else\r\nif kind=PropertyKind::Ambiguous then \'[AMBIGUOUS]\' else\r\nif kind=PropertyKind::TypeMissing then \'[TYPE MISSING] \' else\r\nif kind=PropertyKind::Undefined then \'[UNDEFINED] \' else\r\n\'[IMPOSSIBLE] \' endif endif endif endif endif endif)\r\n.concat(self.localStructuralName)\r\n.concat(\r\nif kind = PropertyKind::Operation then \'(...)\' else \'\' endif) \r\n' kindLabelDerive='if self.propertyBehavior = mcore::PropertyBehavior::Contained then\r\nif (not(self.opposite.oclIsUndefined()) and (self.opposite.multiplicityCase = MultiplicityCase::OneOne))\r\nthen \'Composition\'\r\nelse\r\n\'Containment\'\r\nendif\r\n else\r\nif self.propertyBehavior = mcore::PropertyBehavior::Derived then\r\nlet changeableString: String = if (self.changeable) then \' Change. \' else \' \'  endif in\r\n   if self.kind = mcore::PropertyKind::Attribute then \'Derived\'.concat(changeableString).concat(\'Attr.\') else\r\n   if self.kind = mcore::PropertyKind::Reference then \'Derived\'.concat(changeableString).concat(\'Ref.\') else \'Derived WRONG\' endif endif \r\nelse\r\nif self.kind = mcore::PropertyKind::Attribute then \'Attribute\' else\r\nif self.kind = mcore::PropertyKind::Reference then\r\n\'Reference\'\r\nelse\r\nif self.kind = mcore::PropertyKind::Operation then \'Operation\' else\r\n\'ERROR\' endif endif endif endif endif' correctNameDerive='(not ambiguousPropertyName()) and\r\nif self.hasSimpleDataType then not self.stringEmpty(name) else true endif' correctShortNameDerive='not ambiguousPropertyShortName()' calculatedMandatoryDerive='self.mandatory' calculatedTypeDerive='self.type' calculatedSingularDerive='self.singular' simpleTypeIsCorrectDerive='if  not type.oclIsUndefined() then simpleType=SimpleType::None else\r\nif voidTypeAllowed then true else simpleType<>SimpleType::None\r\nendif endif\r\n /* repeated from explicitly typedk because of bug, is repeated in MProperty and MParameter... \052/' voidTypeAllowedDerive='self.kind=PropertyKind::Operation' calculatedSimpleTypeDerive='simpleType\n' localStructuralNameDerive='if self.containingClassifier.oclIsUndefined() then \'\' else\r\nself.containingClassifier.localStructuralName\r\n.concat(\'/\')\r\n.concat(self.calculatedShortName.camelCaseUpper())\r\nendif' calculatedNameDerive='(if not stringEmpty(name) then name else  \r\nif hasSimpleDataType then \'NAMEMISSING\' else nameFromType() endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                                   \' \'.concat(nameFromType())\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if ambiguousPropertyName() then \'  (AMBIGUOUS)\' else \'\' endif)' calculatedShortNameDerive='(if stringEmpty(name) then shortNameFromType()\r\nelse\r\n  if stringEmpty(shortName) then name\r\n  else shortName\r\n endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                       if self.hasSimpleDataType or self.hasSimpleModelingType then \'\'\r\n                                else   \' \'.concat(shortNameFromType())\r\n                       endif\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if self.ambiguousPropertyShortName() then \'( AMBIGUOUS)\' else \'\' endif)' eNameDerive='if stringEmpty(self.specialEName) = true \r\nthen if self.isReference = true\r\n then if self.type.oclIsUndefined()\r\n \t\tthen if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n  \t\t \telse self.calculatedShortName.camelCaseLower()\r\n  \t\t \tendif\r\n \t\telse if stringEmpty(self.calculatedShortName) and ( not (stringEmpty(self.type.specialEName) or stringEmpty(self.type.specialEName.trim()))) and true\r\n  \t\t\t\tthen self.type.specialEName.camelCaseLower()\r\n  \t\t\t\telse if self.calculatedTypePackage.containingComponent.namePrefixForFeatures = true and not (stringEmpty(self.calculatedTypePackage.derivedNamePrefix) or \t\t\t\t\t\t\tself.calculatedTypePackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\t\t\t\tthen (self.calculatedTypePackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n \t\t\t\t\t \telse self.calculatedShortName.camelCaseLower()\r\n   \t\t\t\t\t\tendif\r\n   \t\t\t\tendif\r\n   \t\tendif\r\n   \telse if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n   \t\t\telse self.calculatedShortName.camelCaseLower()\r\n   \t\t\tendif\r\n\tendif \r\nelse self.specialEName.camelCaseLower()\r\nendif' typeChoiceConstraint='true'"
 * @generated
 */

public interface MProperty extends MExplicitlyTypedAndNamed, MModelElement {
	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MPropertyAction
	 * @see #setDoAction(MPropertyAction)
	 * @see com.montages.mcore.McorePackage#getMProperty_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Do Action '"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::MPropertyAction::Do\n' choiceConstruction='    let arityActionsInput: OrderedSet(MPropertyAction) = behaviorActions->prepend(MPropertyAction::Do) in\n    let simpleTypeActionsInput: OrderedSet(MPropertyAction) = \narityActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = arityActionsInput | ac->append(it)) in\n    let annotationActionsInput: OrderedSet(MPropertyAction) = \nsimpleTypeActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = simpleTypeActionsInput | ac->append(it)) in\n    let layoutActionsInput: OrderedSet(MPropertyAction) = \nannotationActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = annotationActionsInput | ac->append(it))\n->append(MPropertyAction::Highlight) in\n\n\tlet moveActionsInput: OrderedSet(MPropertyAction) = \nlayoutActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = layoutActionsInput | ac->append(it)) in \nmoveActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = moveActionsInput | ac->append(it))->append(MPropertyAction::AddUpdateAnnotation)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MPropertyAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MPropertyAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MPropertyAction value);

	/**
	 * Returns the value of the '<em><b>Behavior Actions</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mcore.MPropertyAction}.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavior Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior Actions</em>' attribute list.
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.McorePackage#getMProperty_BehaviorActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let a:OrderedSet(MPropertyAction) =\nif (propertyBehavior = PropertyBehavior::Stored) \n   then  if self.kind= PropertyKind::Reference\n                then OrderedSet{MPropertyAction::IntoContained, MPropertyAction::IntoDerived, MPropertyAction::IntoOperation}\n                else OrderedSet{MPropertyAction::IntoDerived, MPropertyAction::IntoOperation} endif\nelse if (propertyBehavior = PropertyBehavior::Contained)\n   then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived, MPropertyAction::IntoOperation}\nelse if propertyBehavior = PropertyBehavior::Derived\n   then if self.kind= PropertyKind::Reference\n                then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoOperation}\n                else OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoOperation} endif\nelse if propertyBehavior = PropertyBehavior::IsOperation\n   then if self.hasSimpleModelingType \n                 then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoDerived}\n           else if self.type.oclIsUndefined()\n                 then  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived} \n           else if self.type.kind = ClassifierKind::ClassType\n                 then  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoDerived}\n                 else  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived} endif endif endif\n    else OrderedSet{} endif endif endif endif\n in\n let b:OrderedSet(MPropertyAction) =\n   let sts:OrderedSet(MClassifier) = self.containingClassifier.allSubTypes() in\n   if sts->isEmpty()\n        then a\n   else if propertyBehavior = PropertyBehavior::Derived\n         then a->prepend(MPropertyAction::SpecializeResult)\n   else if propertyBehavior = PropertyBehavior::Stored or propertyBehavior = PropertyBehavior::Contained\n         then a->prepend(MPropertyAction::SpecializeInitOrder)->prepend(MPropertyAction::SpecializeInitValue)\n         else a\n   endif endif endif in\nb\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAction> getBehaviorActions();

	/**
	 * Returns the value of the '<em><b>Layout Actions</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mcore.MPropertyAction}.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout Actions</em>' attribute list.
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.McorePackage#getMProperty_LayoutActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (kind = mcore::PropertyKind::Attribute) or \n   (kind = mcore::PropertyKind::Reference)\n   then\n      let a:mcore::annotations::MPropertyAnnotations= directSemantics in \n       if a.oclIsUndefined()\n         then if containingPropertiesContainer.oclIsTypeOf(mcore::MClassifier)\n            then OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::HideInTableEditor}\n            else /* In property group \052/\n                    OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::ShowInTableEditor} endif\n       else if a.layout.oclIsUndefined()\n          then if containingPropertiesContainer.oclIsTypeOf(mcore::MClassifier)\n            then OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::HideInTableEditor}\n            else /* In property group \052/\n                    OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::ShowInTableEditor} endif\n       else OrderedSet{\n                                   if a.layout.hideInPropertyView\n                                       then MPropertyAction::ShowInPropertiesView\n                                       else MPropertyAction::HideInPropertiesView endif,\n                                   if a.layout.hideInTable\n                                       then MPropertyAction::ShowInTableEditor\n                                       else MPropertyAction::HideInTableEditor endif\n                                   } endif endif\n    else OrderedSet{} endif      '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAction> getLayoutActions();

	/**
	 * Returns the value of the '<em><b>Arity Actions</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mcore.MPropertyAction}.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arity Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arity Actions</em>' attribute list.
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.McorePackage#getMProperty_ArityActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.singular = true \n  then OrderedSet{MPropertyAction::IntoNary}\n  else OrderedSet{MPropertyAction::IntoUnary} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAction> getArityActions();

	/**
	 * Returns the value of the '<em><b>Simple Type Actions</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mcore.MPropertyAction}.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type Actions</em>' attribute list.
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.McorePackage#getMProperty_SimpleTypeActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.calculatedSimpleType = SimpleType::String \n   then OrderedSet{MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Boolean\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Integer\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Double\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Date\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute}  \n  else OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute} endif endif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAction> getSimpleTypeActions();

	/**
	 * Returns the value of the '<em><b>Annotation Actions</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mcore.MPropertyAction}.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Actions</em>' attribute list.
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.McorePackage#getMProperty_AnnotationActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let a:OrderedSet(MPropertyAction) =\nif propertyBehavior = PropertyBehavior::Derived \n    or propertyBehavior = PropertyBehavior::IsOperation\n  then OrderedSet{MPropertyAction::Result}\n  else OrderedSet{MPropertyAction::InitValue, MPropertyAction::InitOrder} endif \nin\nif changeable\n  then a->append(MPropertyAction::ConstructChoice)\n            ->append(MPropertyAction::ConstrainChoice)\n  else a endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAction> getAnnotationActions();

	/**
	 * Returns the value of the '<em><b>Move Actions</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mcore.MPropertyAction}.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertyAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Move Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Move Actions</em>' attribute list.
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.McorePackage#getMProperty_MoveActions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e0: OrderedSet(MPropertyAction) = if self.containingClassifier.indentLevel = (self.indentLevel-1)\r\nthen if self.hasStorage = true and self.simpleType = SimpleType::String\r\n   \t\t\tthen if self.containingClassifier.propertyGroup->isEmpty()\r\n   \t\t\t\t\t\tthen OrderedSet{MPropertyAction::MoveToNewGroup, MPropertyAction::ClassWithStringProperty}\r\n   \t\t\t\t\t\telse OrderedSet{MPropertyAction::MoveToLastGroup, MPropertyAction::MoveToNewGroup, MPropertyAction::ClassWithStringProperty}\r\n   \t\t\t\t\t\tendif   \t\t\t\r\n   \t\t\telse if self.containingClassifier.propertyGroup->isEmpty()\r\n   \t\t\t\t\t\tthen OrderedSet{MPropertyAction::MoveToNewGroup}\r\n   \t\t\t\t\t\telse OrderedSet{MPropertyAction::MoveToLastGroup, MPropertyAction::MoveToNewGroup}\r\n   \t\t\t\t\t\tendif  endif\r\nelse if self.hasStorage = true and self.simpleType = SimpleType::String\r\n   \t\t\tthen OrderedSet{MPropertyAction::ClassWithStringProperty}\r\n   \t\t\telse OrderedSet{} endif\r\nendif\r\nin if self.propertyBehavior = PropertyBehavior::Derived and not self.directSemantics.oclIsUndefined()\r\n\tthen if self.containingClassifier.allDirectSubTypes()->isEmpty()\r\n\t\t\t\tthen if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1\r\n\t\t\t\t\t\t\tthen e0->prepend(MPropertyAction::Abstract)\r\n\t\t\t\t\t\t\telse e0\r\n\t\t\t\t\t\t\tendif\r\n\t\t\t\telse \r\n\t\t\t\tlet subTypes: OrderedSet(mcore::MClassifier)  =\r\n\t\t\t\t\tif containingClassifier.oclIsUndefined()\r\n  \t\t\t\t\t\tthen OrderedSet{}\r\n  \t\t\t\t\t\telse containingClassifier.allDirectSubTypes()\r\n\t\t\t\t\tendif in let subTypesAnno: OrderedSet(mcore::annotations::MClassifierAnnotations)  = subTypes.annotations->asOrderedSet() in\r\n\t\t\t\t\tlet alreadySpecialized: OrderedSet(mcore::annotations::MPropertyAnnotations)  = subTypesAnno.propertyAnnotations->asOrderedSet()->select(it: mcore::annotations::MPropertyAnnotations | let e1: Boolean = it.property = self in \r\n\t\t\t\t\tif e1.oclIsInvalid()\r\n\t\t\t\t\t\tthen null\r\n\t\t\t\t\t\telse e1\r\n\t\t\t\t\tendif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\r\n\t\t\t\t\tlet leftToSpecialize: Boolean = \r\n\t\t\t\t\tif (let e1: Boolean = let chain01: OrderedSet(mcore::MClassifier)  = subTypes in\r\n\t\t\t\t\t\t\tif chain01->size().oclIsUndefined() \r\n \t\t\t\t\t\t\t\tthen null \r\n \t\t\t\t\t\t\t\telse chain01->size()\r\n  \t\t\t\t\t\t\tendif = let chain02: OrderedSet(mcore::annotations::MPropertyAnnotations)  = alreadySpecialized in\r\n\t\t\t\t\t\t\tif chain02->size().oclIsUndefined() \r\n \t\t\t\t\t\t\t\tthen null \r\n \t\t\t\t\t\t\t\telse chain02->size()\r\n  \t\t\t\t\t\t\tendif in \r\n \t\t\t\t\t\t\tif e1.oclIsInvalid()\r\n \t\t\t\t\t\t\t\tthen null\r\n \t\t\t\t\t\t\t\telse e1\r\n \t\t\t\t\t\t\tendif) \r\n  \t\t\t\t\t=true \r\n\t\t\t\t\t\tthen false\r\n  \t\t\t\t\t\telse true\r\n\t\t\t\t\tendif in\r\n\t\t\t\t\tif leftToSpecialize\r\n\t\t\t\t\t\tthen if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1\r\n\t\t\t\t\t\t\t\tthen e0->prepend(MPropertyAction::Abstract)->prepend(MPropertyAction::Specialize)\r\n\t\t\t\t\t\t\t\telse e0->prepend(MPropertyAction::Specialize)\r\n\t\t\t\t\t\t\t\tendif\r\n\t\t\t\t\t\telse if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\tthen e0->prepend(MPropertyAction::Abstract)\r\n\t\t\t\t\t\t\t\t\telse e0\r\n\t\t\t\t\t\t\t\t\tendif\r\n\t\t\t\t\t\tendif\r\n\t\t\tendif\r\nelse e0\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAction> getMoveActions();

	/**
	 * Returns the value of the '<em><b>Derived Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Json Schema</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Json Schema</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_DerivedJsonSchema()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let space : String = \' \' in\nlet branch : String = \'{\' in\nlet lineBreak : String = \'\\n\' in\nlet closedBranch : String = \'}\' in\nlet comma : String = \',\' in\nlet definitions : String = \'#/definitions/\' in\nif (self.hasStorage = false) then \'\'\nelse\n\'\"\'.concat(self.eName).concat(\'\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(\nif(self.calculatedSingular = false) then\n\'\"type\":\'.concat(space).concat(\'\"array\"\').concat(comma).concat(lineBreak).concat(\nif(self.mandatory) then lineBreak.concat(\'\"minItems\":\').concat(space).concat(\'\"1\"\').concat(comma).concat(lineBreak) else \'\' endif)\n.concat(\'\"items\":\').concat(branch).concat(lineBreak)  else \'\' endif)\n.concat(\'\"description\": \').concat(\'\"\').concat(if self.description.oclIsUndefined() then \'no description\' else self.description endif).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\nif(self.hasSimpleDataType) then \n\'\"type\":\'.concat(space).concat(\'\"string\"\')\nelse\n\'\"$ref\":\'.concat(space).concat(\'\"\').concat(definitions).concat(self.type.eName).concat(\'\"\')\nendif\n)\nendif\n.concat(\nif(self.calculatedSingular = false) then\nlineBreak.concat(closedBranch)\nelse \'\'\nendif\n.concat(lineBreak).concat(closedBranch)\n)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='JSON Schema' createColumn='false'"
	 * @generated
	 */
	String getDerivedJsonSchema();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate oppositeDefinition$Update(MProperty trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" trgRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate propertyBehavior$Update(PropertyBehavior trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate typeDefinition$Update(MClassifier trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MPropertyAction trg);

	/**
	 * Returns the value of the '<em><b>Operation Signature</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MOperationSignature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Signature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Signature</em>' containment reference list.
	 * @see #isSetOperationSignature()
	 * @see #unsetOperationSignature()
	 * @see com.montages.mcore.McorePackage#getMProperty_OperationSignature()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MOperationSignature> getOperationSignature();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getOperationSignature <em>Operation Signature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperationSignature()
	 * @see #getOperationSignature()
	 * @generated
	 */
	void unsetOperationSignature();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getOperationSignature <em>Operation Signature</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operation Signature</em>' containment reference list is set.
	 * @see #unsetOperationSignature()
	 * @see #getOperationSignature()
	 * @generated
	 */
	boolean isSetOperationSignature();

	/**
	 * Returns the value of the '<em><b>Opposite Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opposite Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite Definition</em>' reference.
	 * @see #setOppositeDefinition(MProperty)
	 * @see com.montages.mcore.McorePackage#getMProperty_OppositeDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='opposite\n' choiceConstruction='--Manually changed in MPropertyItemProvider\r\nif self.kind <> PropertyKind::Reference \r\n  then OrderedSet{}\r\n  else if self.type.oclIsUndefined() \r\n    then OrderedSet{}\r\n   else self.type.allProperties()\r\n     ->select(p:MProperty|p.type=self.containingClassifier and p<>self and \r\n                      (if self.containment then not p.containment else true endif) \r\n                      )\r\n   endif \r\n endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MProperty getOppositeDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getOppositeDefinition <em>Opposite Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite Definition</em>' reference.
	 * @see #getOppositeDefinition()
	 * @generated
	 */
	void setOppositeDefinition(MProperty value);

	/**
	 * Returns the value of the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opposite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite</em>' reference.
	 * @see #isSetOpposite()
	 * @see #unsetOpposite()
	 * @see #setOpposite(MProperty)
	 * @see com.montages.mcore.McorePackage#getMProperty_Opposite()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='if self.kind <> PropertyKind::Reference \r\n  then OrderedSet{}\r\n  else if self.type.oclIsUndefined() \r\n    then OrderedSet{}\r\n   else self.type.allProperties()\r\n     ->select(p:MProperty|p.type=self.containingClassifier and p<>self and \r\n                      (if self.containment then not p.containment else true endif) \r\n                      )\r\n   endif \r\n endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MProperty getOpposite();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getOpposite <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite</em>' reference.
	 * @see #isSetOpposite()
	 * @see #unsetOpposite()
	 * @see #getOpposite()
	 * @generated
	 */
	void setOpposite(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getOpposite <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOpposite()
	 * @see #getOpposite()
	 * @see #setOpposite(MProperty)
	 * @generated
	 */
	void unsetOpposite();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getOpposite <em>Opposite</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Opposite</em>' reference is set.
	 * @see #unsetOpposite()
	 * @see #getOpposite()
	 * @see #setOpposite(MProperty)
	 * @generated
	 */
	boolean isSetOpposite();

	/**
	 * Returns the value of the '<em><b>Is Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Operation</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_IsOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.kind=PropertyKind::Operation'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior/Derived Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getIsOperation();

	/**
	 * Returns the value of the '<em><b>Is Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Attribute</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_IsAttribute()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.kind=PropertyKind::Attribute'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior/Derived Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getIsAttribute();

	/**
	 * Returns the value of the '<em><b>Is Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Reference</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_IsReference()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.kind=PropertyKind::Reference'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior/Derived Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getIsReference();

	/**
	 * Returns the value of the '<em><b>Property Behavior</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.PropertyBehavior}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Behavior</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Behavior</em>' attribute.
	 * @see com.montages.mcore.PropertyBehavior
	 * @see #setPropertyBehavior(PropertyBehavior)
	 * @see com.montages.mcore.McorePackage#getMProperty_PropertyBehavior()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.kind=PropertyKind::Operation\r\n  then PropertyBehavior::IsOperation\r\nelse if self.hasStorage \r\n  then if self.containment \r\n  \t\tthen PropertyBehavior::Contained\r\n  \t\telse  PropertyBehavior::Stored endif\r\n  else PropertyBehavior::Derived endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Behavior' createColumn='false'"
	 * @generated
	 */
	PropertyBehavior getPropertyBehavior();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getPropertyBehavior <em>Property Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Behavior</em>' attribute.
	 * @see com.montages.mcore.PropertyBehavior
	 * @see #getPropertyBehavior()
	 * @generated
	 */
	void setPropertyBehavior(PropertyBehavior value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.PropertyKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see com.montages.mcore.PropertyKind
	 * @see com.montages.mcore.McorePackage#getMProperty_Kind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if  not self.operationSignature->isEmpty() then PropertyKind::Operation else\r\nif type.oclIsUndefined() then \r\n  if self.simpleType=SimpleType::None then PropertyKind::TypeMissing else \r\n  if self.hasSimpleDataType then PropertyKind::Attribute \r\n  else if self.hasSimpleModelingType then PropertyKind::Reference else PropertyKind::TypeMissing endif endif endif\r\nelse\r\nif type.kind = ClassifierKind::ClassType then PropertyKind::Reference else\r\nif type.kind =ClassifierKind::DataType or type.kind = ClassifierKind::Enumeration then PropertyKind::Attribute else\r\nPropertyKind::Undefined\r\nendif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior/Derived Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	PropertyKind getKind();

	/**
	 * Returns the value of the '<em><b>Containment</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment</em>' attribute.
	 * @see #isSetContainment()
	 * @see #unsetContainment()
	 * @see #setContainment(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_Containment()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getContainment();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getContainment <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containment</em>' attribute.
	 * @see #isSetContainment()
	 * @see #unsetContainment()
	 * @see #getContainment()
	 * @generated
	 */
	void setContainment(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getContainment <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainment()
	 * @see #getContainment()
	 * @see #setContainment(Boolean)
	 * @generated
	 */
	void unsetContainment();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getContainment <em>Containment</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Containment</em>' attribute is set.
	 * @see #unsetContainment()
	 * @see #getContainment()
	 * @see #setContainment(Boolean)
	 * @generated
	 */
	boolean isSetContainment();

	/**
	 * Returns the value of the '<em><b>Has Storage</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Storage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Storage</em>' attribute.
	 * @see #isSetHasStorage()
	 * @see #unsetHasStorage()
	 * @see #setHasStorage(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_HasStorage()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getHasStorage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getHasStorage <em>Has Storage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Storage</em>' attribute.
	 * @see #isSetHasStorage()
	 * @see #unsetHasStorage()
	 * @see #getHasStorage()
	 * @generated
	 */
	void setHasStorage(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getHasStorage <em>Has Storage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHasStorage()
	 * @see #getHasStorage()
	 * @see #setHasStorage(Boolean)
	 * @generated
	 */
	void unsetHasStorage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getHasStorage <em>Has Storage</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Has Storage</em>' attribute is set.
	 * @see #unsetHasStorage()
	 * @see #getHasStorage()
	 * @see #setHasStorage(Boolean)
	 * @generated
	 */
	boolean isSetHasStorage();

	/**
	 * Returns the value of the '<em><b>Changeable</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Changeable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Changeable</em>' attribute.
	 * @see #isSetChangeable()
	 * @see #unsetChangeable()
	 * @see #setChangeable(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_Changeable()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Behavior' createColumn='false'"
	 * @generated
	 */
	Boolean getChangeable();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getChangeable <em>Changeable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Changeable</em>' attribute.
	 * @see #isSetChangeable()
	 * @see #unsetChangeable()
	 * @see #getChangeable()
	 * @generated
	 */
	void setChangeable(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getChangeable <em>Changeable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetChangeable()
	 * @see #getChangeable()
	 * @see #setChangeable(Boolean)
	 * @generated
	 */
	void unsetChangeable();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getChangeable <em>Changeable</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Changeable</em>' attribute is set.
	 * @see #unsetChangeable()
	 * @see #getChangeable()
	 * @see #setChangeable(Boolean)
	 * @generated
	 */
	boolean isSetChangeable();

	/**
	 * Returns the value of the '<em><b>Persisted</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persisted</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persisted</em>' attribute.
	 * @see #isSetPersisted()
	 * @see #unsetPersisted()
	 * @see #setPersisted(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_Persisted()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Behavior'"
	 * @generated
	 */
	Boolean getPersisted();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getPersisted <em>Persisted</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persisted</em>' attribute.
	 * @see #isSetPersisted()
	 * @see #unsetPersisted()
	 * @see #getPersisted()
	 * @generated
	 */
	void setPersisted(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getPersisted <em>Persisted</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPersisted()
	 * @see #getPersisted()
	 * @see #setPersisted(Boolean)
	 * @generated
	 */
	void unsetPersisted();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getPersisted <em>Persisted</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Persisted</em>' attribute is set.
	 * @see #unsetPersisted()
	 * @see #getPersisted()
	 * @see #setPersisted(Boolean)
	 * @generated
	 */
	boolean isSetPersisted();

	/**
	 * Returns the value of the '<em><b>Type Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Definition</em>' reference.
	 * @see #setTypeDefinition(MClassifier)
	 * @see com.montages.mcore.McorePackage#getMProperty_TypeDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.type' choiceConstraint='self.selectableType(trg)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getTypeDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getTypeDefinition <em>Type Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Definition</em>' reference.
	 * @see #getTypeDefinition()
	 * @generated
	 */
	void setTypeDefinition(MClassifier value);

	/**
	 * Returns the value of the '<em><b>Use Core Types</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Core Types</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Core Types</em>' attribute.
	 * @see #isSetUseCoreTypes()
	 * @see #unsetUseCoreTypes()
	 * @see #setUseCoreTypes(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_UseCoreTypes()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getUseCoreTypes();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getUseCoreTypes <em>Use Core Types</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Core Types</em>' attribute.
	 * @see #isSetUseCoreTypes()
	 * @see #unsetUseCoreTypes()
	 * @see #getUseCoreTypes()
	 * @generated
	 */
	void setUseCoreTypes(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getUseCoreTypes <em>Use Core Types</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUseCoreTypes()
	 * @see #getUseCoreTypes()
	 * @see #setUseCoreTypes(Boolean)
	 * @generated
	 */
	void unsetUseCoreTypes();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getUseCoreTypes <em>Use Core Types</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Use Core Types</em>' attribute is set.
	 * @see #unsetUseCoreTypes()
	 * @see #getUseCoreTypes()
	 * @see #setUseCoreTypes(Boolean)
	 * @generated
	 */
	boolean isSetUseCoreTypes();

	/**
	 * Returns the value of the '<em><b>ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ELabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ELabel</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_ELabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if stringEmpty(name)\r\n  then eTypeLabel\r\n  else eNameForELabel\r\n           .concat(if self.kind=PropertyKind::Operation \r\n                             then \'(...)\' else \'\' endif)\r\n            .concat(if self.eTypeLabel=\'\'\r\n                             then \'\' else \':\'.concat(self.eTypeLabel) endif) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getELabel();

	/**
	 * Returns the value of the '<em><b>EName For ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EName For ELabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EName For ELabel</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_ENameForELabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let calculatedShortNameWithoutTypeName:String = \r\nif stringEmpty(shortName) then name else shortName endif in \r\nif self.isReference = true\r\n then if self.type.oclIsUndefined()\r\n \t\tthen if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(calculatedShortNameWithoutTypeName)).camelCaseLower()\r\n  \t\t \telse calculatedShortNameWithoutTypeName.camelCaseLower()\r\n  \t\t \tendif\r\n \t\telse if self.calculatedTypePackage.containingComponent.namePrefixForFeatures = true and not (stringEmpty(self.calculatedTypePackage.derivedNamePrefix) or \t\t\t\t\t\t\tself.calculatedTypePackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\t\t\t\tthen (self.calculatedTypePackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n \t\t\t\t\t \telse calculatedShortNameWithoutTypeName.camelCaseLower()\r\n   \t\t\t     endif\r\n   \t\tendif\r\n   \telse if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(calculatedShortNameWithoutTypeName)).camelCaseLower()\r\n   \t\t\telse calculatedShortNameWithoutTypeName.camelCaseLower()\r\n   \t\t\tendif\r\n\tendif '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getENameForELabel();

	/**
	 * Returns the value of the '<em><b>Order Within Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order Within Group</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order Within Group</em>' attribute.
	 * @see #isSetOrderWithinGroup()
	 * @see #unsetOrderWithinGroup()
	 * @see #setOrderWithinGroup(Integer)
	 * @see com.montages.mcore.McorePackage#getMProperty_OrderWithinGroup()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Order' createColumn='false'"
	 * @generated
	 */
	Integer getOrderWithinGroup();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getOrderWithinGroup <em>Order Within Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order Within Group</em>' attribute.
	 * @see #isSetOrderWithinGroup()
	 * @see #unsetOrderWithinGroup()
	 * @see #getOrderWithinGroup()
	 * @generated
	 */
	void setOrderWithinGroup(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getOrderWithinGroup <em>Order Within Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOrderWithinGroup()
	 * @see #getOrderWithinGroup()
	 * @see #setOrderWithinGroup(Integer)
	 * @generated
	 */
	void unsetOrderWithinGroup();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getOrderWithinGroup <em>Order Within Group</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Order Within Group</em>' attribute is set.
	 * @see #unsetOrderWithinGroup()
	 * @see #getOrderWithinGroup()
	 * @see #setOrderWithinGroup(Integer)
	 * @generated
	 */
	boolean isSetOrderWithinGroup();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMProperty_Id()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingClassifier.oclIsUndefined() then \'CONTAINER MISSING\' else \r\nlet pos:Integer=self.containingClassifier.ownedProperty->indexOf(self) in\r\nif pos<10 then \'0\'.concat(pos.toString()) else pos.toString() endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Numeric ID'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getId();

	/**
	 * Returns the value of the '<em><b>Internal EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal EStructural Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal EStructural Feature</em>' reference.
	 * @see #isSetInternalEStructuralFeature()
	 * @see #unsetInternalEStructuralFeature()
	 * @see #setInternalEStructuralFeature(EStructuralFeature)
	 * @see com.montages.mcore.McorePackage#getMProperty_InternalEStructuralFeature()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Relationship to ECore'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EStructuralFeature getInternalEStructuralFeature();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getInternalEStructuralFeature <em>Internal EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal EStructural Feature</em>' reference.
	 * @see #isSetInternalEStructuralFeature()
	 * @see #unsetInternalEStructuralFeature()
	 * @see #getInternalEStructuralFeature()
	 * @generated
	 */
	void setInternalEStructuralFeature(EStructuralFeature value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getInternalEStructuralFeature <em>Internal EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalEStructuralFeature()
	 * @see #getInternalEStructuralFeature()
	 * @see #setInternalEStructuralFeature(EStructuralFeature)
	 * @generated
	 */
	void unsetInternalEStructuralFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getInternalEStructuralFeature <em>Internal EStructural Feature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal EStructural Feature</em>' reference is set.
	 * @see #unsetInternalEStructuralFeature()
	 * @see #getInternalEStructuralFeature()
	 * @see #setInternalEStructuralFeature(EStructuralFeature)
	 * @generated
	 */
	boolean isSetInternalEStructuralFeature();

	/**
	 * Returns the value of the '<em><b>Direct Semantics</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Semantics</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Semantics</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMProperty_DirectSemantics()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingClassifier.oclIsUndefined() \r\n  then null\r\n  else let pAs:OrderedSet(annotations::MPropertyAnnotations)\r\n            =  containingClassifier\r\n                   .allPropertyAnnotations()\r\n                        ->select(p:annotations::MPropertyAnnotations|\r\n                                   p.annotatedProperty=self) in \r\n                  if pAs->isEmpty() \r\n                    then null \r\n                    else pAs->first() endif endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Semantics'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPropertyAnnotations getDirectSemantics();

	/**
	 * Returns the value of the '<em><b>Semantics Specialization</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MPropertyAnnotations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semantics Specialization</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantics Specialization</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMProperty_SemanticsSpecialization()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingClassifier.oclIsUndefined()\r\n  then OrderedSet{}\r\n  else containingClassifier\r\n            .allSubTypes()\r\n              .allPropertyAnnotations()\r\n                ->select(p:annotations::MPropertyAnnotations\r\n                                |p.annotatedProperty=self) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Semantics'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAnnotations> getSemanticsSpecialization();

	/**
	 * Returns the value of the '<em><b>All Signatures</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MOperationSignature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Signatures</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Signatures</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMProperty_AllSignatures()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingClassifier.oclIsUndefined()\r\n  then Sequence{}\r\n  else self.containingClassifier.allProperties()\r\n  ->select(p:MProperty|p.kind=PropertyKind::Operation and p.calculatedShortName = self.calculatedShortName).operationSignature endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Semantics'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MOperationSignature> getAllSignatures();

	/**
	 * Returns the value of the '<em><b>Direct Operation Semantics</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Operation Semantics</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Operation Semantics</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMProperty_DirectOperationSemantics()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingClassifier.oclIsUndefined() \r\n  then null\r\n  else let pAs:OrderedSet(annotations::MOperationAnnotations)\r\n            =  containingClassifier\r\n                   .allOperationAnnotations()\r\n                        ->select(p:annotations::MOperationAnnotations|\r\n                                   p.annotatedProperty=self) in \r\n                  if pAs->isEmpty() \r\n                    then null \r\n                    else pAs->first() endif endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Semantics'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MOperationAnnotations getDirectOperationSemantics();

	/**
	 * Returns the value of the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Classifier</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMProperty_ContainingClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() then null else\r\nself.eContainer().oclAsType(MPropertiesContainer).containingClassifier\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getContainingClassifier();

	/**
	 * Returns the value of the '<em><b>Containing Properties Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Properties Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Properties Container</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMProperty_ContainingPropertiesContainer()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() then null else\r\nself.eContainer().oclAsType(MPropertiesContainer)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPropertiesContainer getContainingPropertiesContainer();

	/**
	 * Returns the value of the '<em><b>EKey For Path</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EKey For Path</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EKey For Path</em>' reference list.
	 * @see #isSetEKeyForPath()
	 * @see #unsetEKeyForPath()
	 * @see com.montages.mcore.McorePackage#getMProperty_EKeyForPath()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='if self.type.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else self.type.allFeaturesWithStorage()->select(p:MProperty|p.isAttribute) endif'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	EList<MProperty> getEKeyForPath();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getEKeyForPath <em>EKey For Path</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEKeyForPath()
	 * @see #getEKeyForPath()
	 * @generated
	 */
	void unsetEKeyForPath();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getEKeyForPath <em>EKey For Path</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>EKey For Path</em>' reference list is set.
	 * @see #unsetEKeyForPath()
	 * @see #getEKeyForPath()
	 * @generated
	 */
	boolean isSetEKeyForPath();

	/**
	 * Returns the value of the '<em><b>Not Unsettable</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Unsettable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Unsettable</em>' attribute.
	 * @see #isSetNotUnsettable()
	 * @see #unsetNotUnsettable()
	 * @see #setNotUnsettable(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_NotUnsettable()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	Boolean getNotUnsettable();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getNotUnsettable <em>Not Unsettable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Unsettable</em>' attribute.
	 * @see #isSetNotUnsettable()
	 * @see #unsetNotUnsettable()
	 * @see #getNotUnsettable()
	 * @generated
	 */
	void setNotUnsettable(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getNotUnsettable <em>Not Unsettable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNotUnsettable()
	 * @see #getNotUnsettable()
	 * @see #setNotUnsettable(Boolean)
	 * @generated
	 */
	void unsetNotUnsettable();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getNotUnsettable <em>Not Unsettable</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Not Unsettable</em>' attribute is set.
	 * @see #unsetNotUnsettable()
	 * @see #getNotUnsettable()
	 * @see #setNotUnsettable(Boolean)
	 * @generated
	 */
	boolean isSetNotUnsettable();

	/**
	 * Returns the value of the '<em><b>Attribute For EId</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute For EId</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute For EId</em>' attribute.
	 * @see #isSetAttributeForEId()
	 * @see #unsetAttributeForEId()
	 * @see #setAttributeForEId(Boolean)
	 * @see com.montages.mcore.McorePackage#getMProperty_AttributeForEId()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	Boolean getAttributeForEId();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getAttributeForEId <em>Attribute For EId</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute For EId</em>' attribute.
	 * @see #isSetAttributeForEId()
	 * @see #unsetAttributeForEId()
	 * @see #getAttributeForEId()
	 * @generated
	 */
	void setAttributeForEId(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getAttributeForEId <em>Attribute For EId</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAttributeForEId()
	 * @see #getAttributeForEId()
	 * @see #setAttributeForEId(Boolean)
	 * @generated
	 */
	void unsetAttributeForEId();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getAttributeForEId <em>Attribute For EId</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Attribute For EId</em>' attribute is set.
	 * @see #unsetAttributeForEId()
	 * @see #getAttributeForEId()
	 * @see #setAttributeForEId(Boolean)
	 * @generated
	 */
	boolean isSetAttributeForEId();

	/**
	 * Returns the value of the '<em><b>EDefault Value Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EDefault Value Literal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EDefault Value Literal</em>' attribute.
	 * @see #isSetEDefaultValueLiteral()
	 * @see #unsetEDefaultValueLiteral()
	 * @see #setEDefaultValueLiteral(String)
	 * @see com.montages.mcore.McorePackage#getMProperty_EDefaultValueLiteral()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	String getEDefaultValueLiteral();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MProperty#getEDefaultValueLiteral <em>EDefault Value Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EDefault Value Literal</em>' attribute.
	 * @see #isSetEDefaultValueLiteral()
	 * @see #unsetEDefaultValueLiteral()
	 * @see #getEDefaultValueLiteral()
	 * @generated
	 */
	void setEDefaultValueLiteral(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MProperty#getEDefaultValueLiteral <em>EDefault Value Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEDefaultValueLiteral()
	 * @see #getEDefaultValueLiteral()
	 * @see #setEDefaultValueLiteral(String)
	 * @generated
	 */
	void unsetEDefaultValueLiteral();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MProperty#getEDefaultValueLiteral <em>EDefault Value Literal</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>EDefault Value Literal</em>' attribute is set.
	 * @see #unsetEDefaultValueLiteral()
	 * @see #getEDefaultValueLiteral()
	 * @see #setEDefaultValueLiteral(String)
	 * @generated
	 */
	boolean isSetEDefaultValueLiteral();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" mClassifierRequired="true"
	 *        annotation="http://www.xocl.org/OCL body=' mClassifier.selectableClassifier(\r\n  if type=null then OrderedSet{} else OrderedSet{type} endif, \r\n  typePackage)\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='true'"
	 * @generated
	 */
	Boolean selectableType(MClassifier mClassifier);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if true then false else\r\nif self.containingClassifier.oclIsUndefined() then false else\r\nif self.isOperation then false else\r\nif self.containingClassifier.allProperties()->isEmpty() then false else\r\nself.containingClassifier.allProperties()\r\n  ->exists(p:MProperty|not (p=self) \r\n       and (not p.isOperation)\r\n       and p.sameName(self)\r\n       and if p.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not stringEmpty(p.containingClassifier.containingPackage.derivedNamePrefix)\r\n       \t\t\tthen if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix)\r\n       \t\t\t\t\tthen if self.containingClassifier.containingPackage.derivedNamePrefix = p.containingClassifier.containingPackage.derivedNamePrefix\r\n       \t\t\t\t\t\t\tthen true\r\n       \t\t\t\t\t\t\telse false\r\n       \t\t\t\t\t\t\tendif\r\n       \t\t\t\t\telse false\r\n       \t\t\t\t\tendif\r\n\t\t       \telse true\r\n\t\t       \tendif\r\n       \tand if stringEmpty(p.name) then \r\n                    p.type = self.type else true endif)\r\nendif endif endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Naming and Labels' createColumn='true'"
	 * @generated
	 */
	Boolean ambiguousPropertyName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if true then false else\r\nif self.ambiguousPropertyName() then true else\r\nif self.stringEmpty(self.shortName) then false else\r\nif self.containingClassifier.oclIsUndefined() then false else\r\nif self.containingClassifier.allProperties()->isEmpty() then false else\r\nif self.isOperation\r\n  then\r\n    self.containingClassifier.allProperties()\r\n      ->exists(p:MProperty|\r\n         (not (p=self)) \r\n         and p.isOperation \r\n         and (\r\n              (p.sameShortName(self)\r\n                and (not p.sameName(self))\r\n                and (not p.stringEmpty(p.name)) \r\n                and (not  p.stringEmpty(self.name)))\r\n             or\r\n              ((not p.sameShortName(self))\r\n               and p.sameName(self)\r\n               and (not p.stringEmpty(p.name))\r\n               and (not  p.stringEmpty(self.name)))\r\n             )\r\n        )\r\n  else\r\n    self.containingClassifier.allProperties()\r\n      ->exists(p:MProperty|\r\n         (not (p=self)) \r\n         and (not p.isOperation) \r\n         and p.sameShortName(self)\r\n         and not p.stringEmpty(p.name) \r\n         and not  p.stringEmpty(self.name))\r\nendif endif endif endif endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Naming and Labels' createColumn='true'"
	 * @generated
	 */
	Boolean ambiguousPropertyShortName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='doActionUpdate'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	XUpdate doActionUpdate(MPropertyAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='invoke Set Property Behaviour'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	String invokeSetPropertyBehaviour(PropertyBehavior propertyBehavior);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='invoke Set Do Action'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	Object invokeSetDoAction(MPropertyAction mPropertyAction);

} // MProperty
