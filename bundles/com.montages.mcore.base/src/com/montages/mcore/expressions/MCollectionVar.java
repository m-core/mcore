/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MCollection Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MCollectionVar#getContainingCollection <em>Containing Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMCollectionVar()
 * @model abstract="true"
 * @generated
 */

public interface MCollectionVar extends MAbstractExpression, MVariable {
	/**
	 * Returns the value of the '<em><b>Containing Collection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Collection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Collection</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMCollectionVar_ContainingCollection()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(MCollectionExpression)\r\nthen eContainer().oclAsType(MCollectionExpression)\r\nelse null endif'"
	 * @generated
	 */
	MCollectionExpression getContainingCollection();

} // MCollectionVar
