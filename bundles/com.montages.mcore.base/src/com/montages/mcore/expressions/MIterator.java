/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MIterator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMIterator()
 * @model annotation="http://www.xocl.org/OCL label='eName'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Iterator\'\n' calculatedOwnMandatoryDerive='true' calculatedOwnSingularDerive='true' calculatedOwnSimpleTypeDerive='if containingCollection.collection.calculatedOwnSimpleType.oclIsInvalid()\r\nthen SimpleType::None\r\nelse containingCollection.collection.calculatedOwnSimpleType endif' calculatedOwnTypeDerive='if containingCollection.collection.calculatedOwnType.oclIsUndefined() then null\r\nelse containingCollection.collection.calculatedOwnType endif'"
 * @generated
 */

public interface MIterator extends MCollectionVar {
} // MIterator
