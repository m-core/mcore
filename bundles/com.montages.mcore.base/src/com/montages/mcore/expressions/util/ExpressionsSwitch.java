/**
 */
package com.montages.mcore.expressions.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import com.montages.mcore.MNamed;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.expressions.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.expressions.ExpressionsPackage
 * @generated
 */
public class ExpressionsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ExpressionsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsSwitch() {
		if (modelPackage == null) {
			modelPackage = ExpressionsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION: {
			MAbstractExpression mAbstractExpression = (MAbstractExpression) theEObject;
			T result = caseMAbstractExpression(mAbstractExpression);
			if (result == null)
				result = caseMTyped(mAbstractExpression);
			if (result == null)
				result = caseMRepositoryElement(mAbstractExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE: {
			MAbstractExpressionWithBase mAbstractExpressionWithBase = (MAbstractExpressionWithBase) theEObject;
			T result = caseMAbstractExpressionWithBase(
					mAbstractExpressionWithBase);
			if (result == null)
				result = caseMAbstractExpression(mAbstractExpressionWithBase);
			if (result == null)
				result = caseMTyped(mAbstractExpressionWithBase);
			if (result == null)
				result = caseMRepositoryElement(mAbstractExpressionWithBase);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_BASE_DEFINITION: {
			MAbstractBaseDefinition mAbstractBaseDefinition = (MAbstractBaseDefinition) theEObject;
			T result = caseMAbstractBaseDefinition(mAbstractBaseDefinition);
			if (result == null)
				result = caseMTyped(mAbstractBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mAbstractBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCONTAINER_BASE_DEFINITION: {
			MContainerBaseDefinition mContainerBaseDefinition = (MContainerBaseDefinition) theEObject;
			T result = caseMContainerBaseDefinition(mContainerBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mContainerBaseDefinition);
			if (result == null)
				result = caseMTyped(mContainerBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mContainerBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MBASE_DEFINITION: {
			MBaseDefinition mBaseDefinition = (MBaseDefinition) theEObject;
			T result = caseMBaseDefinition(mBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mBaseDefinition);
			if (result == null)
				result = caseMTyped(mBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MNUMBER_BASE_DEFINITION: {
			MNumberBaseDefinition mNumberBaseDefinition = (MNumberBaseDefinition) theEObject;
			T result = caseMNumberBaseDefinition(mNumberBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mNumberBaseDefinition);
			if (result == null)
				result = caseMTyped(mNumberBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mNumberBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MSELF_BASE_DEFINITION: {
			MSelfBaseDefinition mSelfBaseDefinition = (MSelfBaseDefinition) theEObject;
			T result = caseMSelfBaseDefinition(mSelfBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mSelfBaseDefinition);
			if (result == null)
				result = caseMTyped(mSelfBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mSelfBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MTARGET_BASE_DEFINITION: {
			MTargetBaseDefinition mTargetBaseDefinition = (MTargetBaseDefinition) theEObject;
			T result = caseMTargetBaseDefinition(mTargetBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mTargetBaseDefinition);
			if (result == null)
				result = caseMTyped(mTargetBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mTargetBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MSOURCE_BASE_DEFINITION: {
			MSourceBaseDefinition mSourceBaseDefinition = (MSourceBaseDefinition) theEObject;
			T result = caseMSourceBaseDefinition(mSourceBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mSourceBaseDefinition);
			if (result == null)
				result = caseMTyped(mSourceBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mSourceBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MOBJECT_BASE_DEFINITION: {
			MObjectBaseDefinition mObjectBaseDefinition = (MObjectBaseDefinition) theEObject;
			T result = caseMObjectBaseDefinition(mObjectBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mObjectBaseDefinition);
			if (result == null)
				result = caseMTyped(mObjectBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mObjectBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_BASE_DEFINITION: {
			MSimpleTypeConstantBaseDefinition mSimpleTypeConstantBaseDefinition = (MSimpleTypeConstantBaseDefinition) theEObject;
			T result = caseMSimpleTypeConstantBaseDefinition(
					mSimpleTypeConstantBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(
						mSimpleTypeConstantBaseDefinition);
			if (result == null)
				result = caseMTyped(mSimpleTypeConstantBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(
						mSimpleTypeConstantBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MLITERAL_CONSTANT_BASE_DEFINITION: {
			MLiteralConstantBaseDefinition mLiteralConstantBaseDefinition = (MLiteralConstantBaseDefinition) theEObject;
			T result = caseMLiteralConstantBaseDefinition(
					mLiteralConstantBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(
						mLiteralConstantBaseDefinition);
			if (result == null)
				result = caseMTyped(mLiteralConstantBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mLiteralConstantBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MOBJECT_REFERENCE_CONSTANT_BASE_DEFINITION: {
			MObjectReferenceConstantBaseDefinition mObjectReferenceConstantBaseDefinition = (MObjectReferenceConstantBaseDefinition) theEObject;
			T result = caseMObjectReferenceConstantBaseDefinition(
					mObjectReferenceConstantBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(
						mObjectReferenceConstantBaseDefinition);
			if (result == null)
				result = caseMTyped(mObjectReferenceConstantBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(
						mObjectReferenceConstantBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION: {
			MVariableBaseDefinition mVariableBaseDefinition = (MVariableBaseDefinition) theEObject;
			T result = caseMVariableBaseDefinition(mVariableBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mVariableBaseDefinition);
			if (result == null)
				result = caseMTyped(mVariableBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mVariableBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MITERATOR_BASE_DEFINITION: {
			MIteratorBaseDefinition mIteratorBaseDefinition = (MIteratorBaseDefinition) theEObject;
			T result = caseMIteratorBaseDefinition(mIteratorBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mIteratorBaseDefinition);
			if (result == null)
				result = caseMTyped(mIteratorBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mIteratorBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MPARAMETER_BASE_DEFINITION: {
			MParameterBaseDefinition mParameterBaseDefinition = (MParameterBaseDefinition) theEObject;
			T result = caseMParameterBaseDefinition(mParameterBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(mParameterBaseDefinition);
			if (result == null)
				result = caseMTyped(mParameterBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mParameterBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION: {
			MAccumulatorBaseDefinition mAccumulatorBaseDefinition = (MAccumulatorBaseDefinition) theEObject;
			T result = caseMAccumulatorBaseDefinition(
					mAccumulatorBaseDefinition);
			if (result == null)
				result = caseMAbstractBaseDefinition(
						mAccumulatorBaseDefinition);
			if (result == null)
				result = caseMTyped(mAccumulatorBaseDefinition);
			if (result == null)
				result = caseMRepositoryElement(mAccumulatorBaseDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_LET: {
			MAbstractLet mAbstractLet = (MAbstractLet) theEObject;
			T result = caseMAbstractLet(mAbstractLet);
			if (result == null)
				result = caseMVariable(mAbstractLet);
			if (result == null)
				result = caseMAbstractExpression(mAbstractLet);
			if (result == null)
				result = caseMNamed(mAbstractLet);
			if (result == null)
				result = caseMTyped(mAbstractLet);
			if (result == null)
				result = caseMRepositoryElement(mAbstractLet);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_NAMED_TUPLE: {
			MAbstractNamedTuple mAbstractNamedTuple = (MAbstractNamedTuple) theEObject;
			T result = caseMAbstractNamedTuple(mAbstractNamedTuple);
			if (result == null)
				result = caseMAbstractLet(mAbstractNamedTuple);
			if (result == null)
				result = caseMVariable(mAbstractNamedTuple);
			if (result == null)
				result = caseMAbstractExpression(mAbstractNamedTuple);
			if (result == null)
				result = caseMNamed(mAbstractNamedTuple);
			if (result == null)
				result = caseMTyped(mAbstractNamedTuple);
			if (result == null)
				result = caseMRepositoryElement(mAbstractNamedTuple);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_TUPLE_ENTRY: {
			MAbstractTupleEntry mAbstractTupleEntry = (MAbstractTupleEntry) theEObject;
			T result = caseMAbstractTupleEntry(mAbstractTupleEntry);
			if (result == null)
				result = caseMRepositoryElement(mAbstractTupleEntry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MTUPLE: {
			MTuple mTuple = (MTuple) theEObject;
			T result = caseMTuple(mTuple);
			if (result == null)
				result = caseMAbstractNamedTuple(mTuple);
			if (result == null)
				result = caseMAbstractLet(mTuple);
			if (result == null)
				result = caseMVariable(mTuple);
			if (result == null)
				result = caseMAbstractExpression(mTuple);
			if (result == null)
				result = caseMNamed(mTuple);
			if (result == null)
				result = caseMTyped(mTuple);
			if (result == null)
				result = caseMRepositoryElement(mTuple);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MTUPLE_ENTRY: {
			MTupleEntry mTupleEntry = (MTupleEntry) theEObject;
			T result = caseMTupleEntry(mTupleEntry);
			if (result == null)
				result = caseMNamed(mTupleEntry);
			if (result == null)
				result = caseMAbstractTupleEntry(mTupleEntry);
			if (result == null)
				result = caseMRepositoryElement(mTupleEntry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MNEW_OBJECCT: {
			MNewObjecct mNewObjecct = (MNewObjecct) theEObject;
			T result = caseMNewObjecct(mNewObjecct);
			if (result == null)
				result = caseMAbstractNamedTuple(mNewObjecct);
			if (result == null)
				result = caseMAbstractLet(mNewObjecct);
			if (result == null)
				result = caseMVariable(mNewObjecct);
			if (result == null)
				result = caseMAbstractExpression(mNewObjecct);
			if (result == null)
				result = caseMNamed(mNewObjecct);
			if (result == null)
				result = caseMTyped(mNewObjecct);
			if (result == null)
				result = caseMRepositoryElement(mNewObjecct);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE: {
			MNewObjectFeatureValue mNewObjectFeatureValue = (MNewObjectFeatureValue) theEObject;
			T result = caseMNewObjectFeatureValue(mNewObjectFeatureValue);
			if (result == null)
				result = caseMAbstractTupleEntry(mNewObjectFeatureValue);
			if (result == null)
				result = caseMRepositoryElement(mNewObjectFeatureValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MNAMED_EXPRESSION: {
			MNamedExpression mNamedExpression = (MNamedExpression) theEObject;
			T result = caseMNamedExpression(mNamedExpression);
			if (result == null)
				result = caseMAbstractLet(mNamedExpression);
			if (result == null)
				result = caseMVariable(mNamedExpression);
			if (result == null)
				result = caseMAbstractExpression(mNamedExpression);
			if (result == null)
				result = caseMNamed(mNamedExpression);
			if (result == null)
				result = caseMTyped(mNamedExpression);
			if (result == null)
				result = caseMRepositoryElement(mNamedExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MTOPLEVEL_EXPRESSION: {
			MToplevelExpression mToplevelExpression = (MToplevelExpression) theEObject;
			T result = caseMToplevelExpression(mToplevelExpression);
			if (result == null)
				result = caseMAbstractExpression(mToplevelExpression);
			if (result == null)
				result = caseMTyped(mToplevelExpression);
			if (result == null)
				result = caseMRepositoryElement(mToplevelExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_CHAIN: {
			MAbstractChain mAbstractChain = (MAbstractChain) theEObject;
			T result = caseMAbstractChain(mAbstractChain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MBASE_CHAIN: {
			MBaseChain mBaseChain = (MBaseChain) theEObject;
			T result = caseMBaseChain(mBaseChain);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mBaseChain);
			if (result == null)
				result = caseMAbstractChain(mBaseChain);
			if (result == null)
				result = caseMAbstractExpression(mBaseChain);
			if (result == null)
				result = caseMTyped(mBaseChain);
			if (result == null)
				result = caseMRepositoryElement(mBaseChain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MPROCESSOR_DEFINITION: {
			MProcessorDefinition mProcessorDefinition = (MProcessorDefinition) theEObject;
			T result = caseMProcessorDefinition(mProcessorDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCHAIN: {
			MChain mChain = (MChain) theEObject;
			T result = caseMChain(mChain);
			if (result == null)
				result = caseMBaseChain(mChain);
			if (result == null)
				result = caseMChainOrApplication(mChain);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mChain);
			if (result == null)
				result = caseMAbstractChain(mChain);
			if (result == null)
				result = caseMToplevelExpression(mChain);
			if (result == null)
				result = caseMAbstractExpression(mChain);
			if (result == null)
				result = caseMTyped(mChain);
			if (result == null)
				result = caseMRepositoryElement(mChain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCALL_ARGUMENT: {
			MCallArgument mCallArgument = (MCallArgument) theEObject;
			T result = caseMCallArgument(mCallArgument);
			if (result == null)
				result = caseMAbstractExpressionWithBase(mCallArgument);
			if (result == null)
				result = caseMAbstractExpression(mCallArgument);
			if (result == null)
				result = caseMTyped(mCallArgument);
			if (result == null)
				result = caseMRepositoryElement(mCallArgument);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MSUB_CHAIN: {
			MSubChain mSubChain = (MSubChain) theEObject;
			T result = caseMSubChain(mSubChain);
			if (result == null)
				result = caseMAbstractExpression(mSubChain);
			if (result == null)
				result = caseMAbstractChain(mSubChain);
			if (result == null)
				result = caseMTyped(mSubChain);
			if (result == null)
				result = caseMRepositoryElement(mSubChain);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCHAIN_OR_APPLICATION: {
			MChainOrApplication mChainOrApplication = (MChainOrApplication) theEObject;
			T result = caseMChainOrApplication(mChainOrApplication);
			if (result == null)
				result = caseMToplevelExpression(mChainOrApplication);
			if (result == null)
				result = caseMAbstractExpression(mChainOrApplication);
			if (result == null)
				result = caseMTyped(mChainOrApplication);
			if (result == null)
				result = caseMRepositoryElement(mChainOrApplication);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MDATA_VALUE_EXPR: {
			MDataValueExpr mDataValueExpr = (MDataValueExpr) theEObject;
			T result = caseMDataValueExpr(mDataValueExpr);
			if (result == null)
				result = caseMChainOrApplication(mDataValueExpr);
			if (result == null)
				result = caseMToplevelExpression(mDataValueExpr);
			if (result == null)
				result = caseMAbstractExpression(mDataValueExpr);
			if (result == null)
				result = caseMTyped(mDataValueExpr);
			if (result == null)
				result = caseMRepositoryElement(mDataValueExpr);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MLITERAL_VALUE_EXPR: {
			MLiteralValueExpr mLiteralValueExpr = (MLiteralValueExpr) theEObject;
			T result = caseMLiteralValueExpr(mLiteralValueExpr);
			if (result == null)
				result = caseMChainOrApplication(mLiteralValueExpr);
			if (result == null)
				result = caseMToplevelExpression(mLiteralValueExpr);
			if (result == null)
				result = caseMAbstractExpression(mLiteralValueExpr);
			if (result == null)
				result = caseMTyped(mLiteralValueExpr);
			if (result == null)
				result = caseMRepositoryElement(mLiteralValueExpr);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MABSTRACT_IF: {
			MAbstractIf mAbstractIf = (MAbstractIf) theEObject;
			T result = caseMAbstractIf(mAbstractIf);
			if (result == null)
				result = caseMAbstractExpression(mAbstractIf);
			if (result == null)
				result = caseMTyped(mAbstractIf);
			if (result == null)
				result = caseMRepositoryElement(mAbstractIf);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MIF: {
			MIf mIf = (MIf) theEObject;
			T result = caseMIf(mIf);
			if (result == null)
				result = caseMAbstractIf(mIf);
			if (result == null)
				result = caseMToplevelExpression(mIf);
			if (result == null)
				result = caseMAbstractExpression(mIf);
			if (result == null)
				result = caseMTyped(mIf);
			if (result == null)
				result = caseMRepositoryElement(mIf);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MTHEN: {
			MThen mThen = (MThen) theEObject;
			T result = caseMThen(mThen);
			if (result == null)
				result = caseMAbstractExpression(mThen);
			if (result == null)
				result = caseMTyped(mThen);
			if (result == null)
				result = caseMRepositoryElement(mThen);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MELSE_IF: {
			MElseIf mElseIf = (MElseIf) theEObject;
			T result = caseMElseIf(mElseIf);
			if (result == null)
				result = caseMAbstractExpression(mElseIf);
			if (result == null)
				result = caseMTyped(mElseIf);
			if (result == null)
				result = caseMRepositoryElement(mElseIf);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MELSE: {
			MElse mElse = (MElse) theEObject;
			T result = caseMElse(mElse);
			if (result == null)
				result = caseMAbstractExpression(mElse);
			if (result == null)
				result = caseMTyped(mElse);
			if (result == null)
				result = caseMRepositoryElement(mElse);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCOLLECTION_EXPRESSION: {
			MCollectionExpression mCollectionExpression = (MCollectionExpression) theEObject;
			T result = caseMCollectionExpression(mCollectionExpression);
			if (result == null)
				result = caseMAbstractExpression(mCollectionExpression);
			if (result == null)
				result = caseMTyped(mCollectionExpression);
			if (result == null)
				result = caseMRepositoryElement(mCollectionExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCOLLECTION_VAR: {
			MCollectionVar mCollectionVar = (MCollectionVar) theEObject;
			T result = caseMCollectionVar(mCollectionVar);
			if (result == null)
				result = caseMAbstractExpression(mCollectionVar);
			if (result == null)
				result = caseMVariable(mCollectionVar);
			if (result == null)
				result = caseMTyped(mCollectionVar);
			if (result == null)
				result = caseMNamed(mCollectionVar);
			if (result == null)
				result = caseMRepositoryElement(mCollectionVar);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MITERATOR: {
			MIterator mIterator = (MIterator) theEObject;
			T result = caseMIterator(mIterator);
			if (result == null)
				result = caseMCollectionVar(mIterator);
			if (result == null)
				result = caseMAbstractExpression(mIterator);
			if (result == null)
				result = caseMVariable(mIterator);
			if (result == null)
				result = caseMTyped(mIterator);
			if (result == null)
				result = caseMNamed(mIterator);
			if (result == null)
				result = caseMRepositoryElement(mIterator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MACCUMULATOR: {
			MAccumulator mAccumulator = (MAccumulator) theEObject;
			T result = caseMAccumulator(mAccumulator);
			if (result == null)
				result = caseMCollectionVar(mAccumulator);
			if (result == null)
				result = caseMAbstractExpression(mAccumulator);
			if (result == null)
				result = caseMVariable(mAccumulator);
			if (result == null)
				result = caseMTyped(mAccumulator);
			if (result == null)
				result = caseMNamed(mAccumulator);
			if (result == null)
				result = caseMRepositoryElement(mAccumulator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MNAMED_CONSTANT: {
			MNamedConstant mNamedConstant = (MNamedConstant) theEObject;
			T result = caseMNamedConstant(mNamedConstant);
			if (result == null)
				result = caseMAbstractLet(mNamedConstant);
			if (result == null)
				result = caseMVariable(mNamedConstant);
			if (result == null)
				result = caseMAbstractExpression(mNamedConstant);
			if (result == null)
				result = caseMNamed(mNamedConstant);
			if (result == null)
				result = caseMTyped(mNamedConstant);
			if (result == null)
				result = caseMRepositoryElement(mNamedConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MCONSTANT_LET: {
			MConstantLet mConstantLet = (MConstantLet) theEObject;
			T result = caseMConstantLet(mConstantLet);
			if (result == null)
				result = caseMAbstractExpression(mConstantLet);
			if (result == null)
				result = caseMTyped(mConstantLet);
			if (result == null)
				result = caseMRepositoryElement(mConstantLet);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET: {
			MSimpleTypeConstantLet mSimpleTypeConstantLet = (MSimpleTypeConstantLet) theEObject;
			T result = caseMSimpleTypeConstantLet(mSimpleTypeConstantLet);
			if (result == null)
				result = caseMConstantLet(mSimpleTypeConstantLet);
			if (result == null)
				result = caseMAbstractExpression(mSimpleTypeConstantLet);
			if (result == null)
				result = caseMTyped(mSimpleTypeConstantLet);
			if (result == null)
				result = caseMRepositoryElement(mSimpleTypeConstantLet);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MLITERAL_LET: {
			MLiteralLet mLiteralLet = (MLiteralLet) theEObject;
			T result = caseMLiteralLet(mLiteralLet);
			if (result == null)
				result = caseMConstantLet(mLiteralLet);
			if (result == null)
				result = caseMAbstractExpression(mLiteralLet);
			if (result == null)
				result = caseMTyped(mLiteralLet);
			if (result == null)
				result = caseMRepositoryElement(mLiteralLet);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MOBJECT_REFERENCE_LET: {
			MObjectReferenceLet mObjectReferenceLet = (MObjectReferenceLet) theEObject;
			T result = caseMObjectReferenceLet(mObjectReferenceLet);
			if (result == null)
				result = caseMConstantLet(mObjectReferenceLet);
			if (result == null)
				result = caseMAbstractExpression(mObjectReferenceLet);
			if (result == null)
				result = caseMTyped(mObjectReferenceLet);
			if (result == null)
				result = caseMRepositoryElement(mObjectReferenceLet);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MAPPLICATION: {
			MApplication mApplication = (MApplication) theEObject;
			T result = caseMApplication(mApplication);
			if (result == null)
				result = caseMChainOrApplication(mApplication);
			if (result == null)
				result = caseMToplevelExpression(mApplication);
			if (result == null)
				result = caseMAbstractExpression(mApplication);
			if (result == null)
				result = caseMTyped(mApplication);
			if (result == null)
				result = caseMRepositoryElement(mApplication);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ExpressionsPackage.MOPERATOR_DEFINITION: {
			MOperatorDefinition mOperatorDefinition = (MOperatorDefinition) theEObject;
			T result = caseMOperatorDefinition(mOperatorDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractExpression(MAbstractExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Expression With Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Expression With Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractExpressionWithBase(
			MAbstractExpressionWithBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractBaseDefinition(MAbstractBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MContainer Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MContainer Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMContainerBaseDefinition(MContainerBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MBase Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MBase Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMBaseDefinition(MBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNumber Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNumber Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNumberBaseDefinition(MNumberBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSelf Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSelf Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSelfBaseDefinition(MSelfBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTarget Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTarget Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTargetBaseDefinition(MTargetBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSource Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSource Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSourceBaseDefinition(MSourceBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MObject Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MObject Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMObjectBaseDefinition(MObjectBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSimple Type Constant Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSimple Type Constant Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSimpleTypeConstantBaseDefinition(
			MSimpleTypeConstantBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLiteral Constant Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLiteral Constant Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLiteralConstantBaseDefinition(
			MLiteralConstantBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MObject Reference Constant Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MObject Reference Constant Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMObjectReferenceConstantBaseDefinition(
			MObjectReferenceConstantBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MVariable Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MVariable Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMVariableBaseDefinition(MVariableBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MParameter Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MParameter Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMParameterBaseDefinition(MParameterBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MIterator Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MIterator Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMIteratorBaseDefinition(MIteratorBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAccumulator Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAccumulator Base Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAccumulatorBaseDefinition(MAccumulatorBaseDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Let</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Let</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractLet(MAbstractLet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Named Tuple</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Named Tuple</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractNamedTuple(MAbstractNamedTuple object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Tuple Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Tuple Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractTupleEntry(MAbstractTupleEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTuple</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTuple</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTuple(MTuple object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTuple Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTuple Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTupleEntry(MTupleEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNew Objecct</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNew Objecct</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNewObjecct(MNewObjecct object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNew Object Feature Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNew Object Feature Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNewObjectFeatureValue(MNewObjectFeatureValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamedExpression(MNamedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MToplevel Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MToplevel Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMToplevelExpression(MToplevelExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Chain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Chain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractChain(MAbstractChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MBase Chain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MBase Chain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMBaseChain(MBaseChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MProcessor Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MProcessor Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMProcessorDefinition(MProcessorDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MChain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MChain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMChain(MChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MCall Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MCall Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMCallArgument(MCallArgument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MChain Or Application</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MChain Or Application</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMChainOrApplication(MChainOrApplication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MData Value Expr</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MData Value Expr</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMDataValueExpr(MDataValueExpr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLiteral Value Expr</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLiteral Value Expr</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLiteralValueExpr(MLiteralValueExpr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract If</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract If</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractIf(MAbstractIf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MIf</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MIf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMIf(MIf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MThen</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MThen</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMThen(MThen object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MElse If</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MElse If</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMElseIf(MElseIf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MElse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MElse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMElse(MElse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSub Chain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSub Chain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSubChain(MSubChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MCollection Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MCollection Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMCollectionExpression(MCollectionExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MCollection Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MCollection Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMCollectionVar(MCollectionVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MIterator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MIterator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMIterator(MIterator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAccumulator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAccumulator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAccumulator(MAccumulator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamedConstant(MNamedConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MConstant Let</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MConstant Let</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMConstantLet(MConstantLet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MSimple Type Constant Let</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MSimple Type Constant Let</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMSimpleTypeConstantLet(MSimpleTypeConstantLet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLiteral Let</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLiteral Let</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLiteralLet(MLiteralLet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MObject Reference Let</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MObject Reference Let</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMObjectReferenceLet(MObjectReferenceLet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MApplication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MApplication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMApplication(MApplication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MOperator Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MOperator Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMOperatorDefinition(MOperatorDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMRepositoryElement(MRepositoryElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTyped</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTyped</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTyped(MTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamed(MNamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MVariable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MVariable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMVariable(MVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ExpressionsSwitch
