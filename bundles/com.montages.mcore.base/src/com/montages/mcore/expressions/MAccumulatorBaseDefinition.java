/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAccumulator Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulatorBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<accumulator> \'.concat(if accumulator.oclIsUndefined() then \'\' else accumulator.eName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='ExpressionBase::Accumulator' calculatedAsCodeDerive='if accumulator.oclIsUndefined() then \'MISSING ACCUMULATOR\' else accumulator.eName endif' calculatedMandatoryDerive='if accumulator.oclIsUndefined() then false\r\nelse accumulator.calculatedMandatory endif' calculatedSingularDerive='if accumulator.oclIsUndefined() then true\r\nelse accumulator.calculatedSingular endif' calculatedSimpleTypeDerive='if accumulator.oclIsUndefined() then SimpleType::None\r\nelse accumulator.calculatedSimpleType\r\nendif' calculatedTypeDerive='if accumulator.oclIsUndefined() then null\r\nelse accumulator.calculatedType endif'"
 * @generated
 */

public interface MAccumulatorBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Accumulator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accumulator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accumulator</em>' reference.
	 * @see #isSetAccumulator()
	 * @see #unsetAccumulator()
	 * @see #setAccumulator(MAccumulator)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulatorBaseDefinition_Accumulator()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='accumulator'"
	 * @generated
	 */
	MAccumulator getAccumulator();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accumulator</em>' reference.
	 * @see #isSetAccumulator()
	 * @see #unsetAccumulator()
	 * @see #getAccumulator()
	 * @generated
	 */
	void setAccumulator(MAccumulator value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAccumulator()
	 * @see #getAccumulator()
	 * @see #setAccumulator(MAccumulator)
	 * @generated
	 */
	void unsetAccumulator();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAccumulatorBaseDefinition#getAccumulator <em>Accumulator</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Accumulator</em>' reference is set.
	 * @see #unsetAccumulator()
	 * @see #getAccumulator()
	 * @see #setAccumulator(MAccumulator)
	 * @generated
	 */
	boolean isSetAccumulator();

} // MAccumulatorBaseDefinition
