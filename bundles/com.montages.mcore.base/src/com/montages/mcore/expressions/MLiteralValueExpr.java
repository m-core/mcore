/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral Value Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MLiteralValueExpr#getEnumerationType <em>Enumeration Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MLiteralValueExpr#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralValueExpr()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Literal\'\n' isComplexExpressionDerive='false' calculatedOwnMandatoryDerive='true' calculatedOwnSingularDerive='true' calculatedOwnTypeDerive='enumerationType' calculatedOwnSimpleTypeDerive='SimpleType::None' asBasicCodeDerive='if value.oclIsUndefined() then \'null\'\r\nelse value.qualifiedName endif'"
 * @generated
 */

public interface MLiteralValueExpr extends MChainOrApplication {
	/**
	 * Returns the value of the '<em><b>Enumeration Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type</em>' reference.
	 * @see #isSetEnumerationType()
	 * @see #unsetEnumerationType()
	 * @see #setEnumerationType(MClassifier)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralValueExpr_EnumerationType()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.allLiterals()->size() > 0'"
	 * @generated
	 */
	MClassifier getEnumerationType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MLiteralValueExpr#getEnumerationType <em>Enumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Type</em>' reference.
	 * @see #isSetEnumerationType()
	 * @see #unsetEnumerationType()
	 * @see #getEnumerationType()
	 * @generated
	 */
	void setEnumerationType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MLiteralValueExpr#getEnumerationType <em>Enumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEnumerationType()
	 * @see #getEnumerationType()
	 * @see #setEnumerationType(MClassifier)
	 * @generated
	 */
	void unsetEnumerationType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MLiteralValueExpr#getEnumerationType <em>Enumeration Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Enumeration Type</em>' reference is set.
	 * @see #unsetEnumerationType()
	 * @see #getEnumerationType()
	 * @see #setEnumerationType(MClassifier)
	 * @generated
	 */
	boolean isSetEnumerationType();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(MLiteral)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralValueExpr_Value()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MLiteral getValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MLiteralValueExpr#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(MLiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MLiteralValueExpr#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(MLiteral)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MLiteralValueExpr#getValue <em>Value</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' reference is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(MLiteral)
	 * @generated
	 */
	boolean isSetValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if self.enumerationType.oclIsUndefined() then OrderedSet{}\r\nelse if self.enumerationType.kind = ClassifierKind::Enumeration then enumerationType.allLiterals() else OrderedSet{} endif endif'"
	 * @generated
	 */
	EList<MLiteral> getAllowedLiterals();

} // MLiteralValueExpr
