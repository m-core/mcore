/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MIf</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MIf#getContainedCollector <em>Contained Collector</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMIf()
 * @model annotation="http://www.xocl.org/OCL label='let c: String = if condition.oclIsUndefined() then \'MISSING CONDITION\' \r\n  else condition.getShortCode() endif in\r\n    \r\n\'if \'.concat(c)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let kind: String = \'IF\' in\nkind\n' calculatedOwnMandatoryDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedMandatory endif' calculatedOwnSingularDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedSingular endif' calculatedOwnTypeDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then null\r\nelse thenPart.calculatedType endif' calculatedOwnSimpleTypeDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then SimpleType::None\r\nelse thenPart.calculatedSimpleType endif' asBasicCodeDerive='let c: String = if condition.oclIsUndefined() then \'MISSING CONDITION\' \r\n  else condition.asCode endif in\r\nlet t: String = if thenPart.oclIsUndefined() then \'MISSING THEN\' \r\n  else thenPart.asCode endif in\r\nlet e: String = if elsePart.oclIsUndefined() then \'MISSING ELSE\' \r\n  else elsePart.asCode endif in\r\n\'if (\'.concat(c).concat(\') \\n  =true \\n\').concat(\r\nt).concat(\r\n\telseifPart->iterate(it: MElseIf; s: String=\'\' | s.concat(\' \').concat(it.asCode))\r\n).concat(\'\\n  \').concat(e).concat(\'\\nendif\').concat(\r\n\telseifPart->iterate(it: MElseIf; s: String=\'\' | s.concat(\' endif\'))\r\n)' collectorDerive='containedCollector' elseifPartInitValue='OrderedSet{}'"
 * @generated
 */

public interface MIf extends MAbstractIf, MToplevelExpression {
	/**
	 * Returns the value of the '<em><b>Contained Collector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Collector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Collector</em>' containment reference.
	 * @see #isSetContainedCollector()
	 * @see #unsetContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMIf_ContainedCollector()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='null'"
	 * @generated
	 */
	MCollectionExpression getContainedCollector();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MIf#getContainedCollector <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Collector</em>' containment reference.
	 * @see #isSetContainedCollector()
	 * @see #unsetContainedCollector()
	 * @see #getContainedCollector()
	 * @generated
	 */
	void setContainedCollector(MCollectionExpression value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MIf#getContainedCollector <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainedCollector()
	 * @see #getContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @generated
	 */
	void unsetContainedCollector();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MIf#getContainedCollector <em>Contained Collector</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Contained Collector</em>' containment reference is set.
	 * @see #unsetContainedCollector()
	 * @see #getContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @generated
	 */
	boolean isSetContainedCollector();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='Tuple{base=ExpressionBase::SelfObject}'"
	 * @generated
	 */
	MChain defaultValue();

} // MIf
