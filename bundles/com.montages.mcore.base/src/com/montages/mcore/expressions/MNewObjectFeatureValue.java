/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNew Object Feature Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MNewObjectFeatureValue#getFeature <em>Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNewObjectFeatureValue()
 * @model
 * @generated
 */

public interface MNewObjectFeatureValue extends MAbstractTupleEntry {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #setFeature(MProperty)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNewObjectFeatureValue_Feature()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='if self.eContainer().oclAsType(expressions::MNewObjecct).newType.oclIsUndefined() then \r\nOrderedSet{} else\r\nlet type : MClassifier = \r\nself.eContainer().oclAsType(expressions::MNewObjecct).newType in\r\ntype.allFeaturesWithStorage()->union(type.allContainmentReferences())  endif'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MProperty getFeature();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MNewObjectFeatureValue#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MNewObjectFeatureValue#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFeature()
	 * @see #getFeature()
	 * @see #setFeature(MProperty)
	 * @generated
	 */
	void unsetFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MNewObjectFeatureValue#getFeature <em>Feature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Feature</em>' reference is set.
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @see #setFeature(MProperty)
	 * @generated
	 */
	boolean isSetFeature();

} // MNewObjectFeatureValue
