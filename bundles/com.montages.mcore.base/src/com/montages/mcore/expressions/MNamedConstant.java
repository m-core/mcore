/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNamed Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MNamedConstant#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNamedConstant()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Where\'\r\n/*\r\nlet c: annotations::MExprAnnotation = self.eContainer().oclAsType(annotations::MExprAnnotation) in\r\nif c.oclIsUndefined() then \'Let\'\r\n\telse if c.namedExpression->notEmpty()\r\n\t\tthen \'Let\'\r\n\t\telse if c.namedConstant->last()=self\r\n\t\t    then \'Definition\'\r\n\t\t    else \'Let\' endif\r\n\t\tendif\r\nendif\r\n\052/\r\n' asBasicCodeDerive='if expression.oclIsUndefined() then \'MISSING EXPRESSION\' else\r\n\r\nlet c: String = expression.asCode in\r\nlet t: String = if  expression.calculatedType.oclIsUndefined()\r\n\tthen expression.calculatedSimpleType.toString()\r\n\telse expression.calculatedType.eName endif in\r\n\t\r\n if name.oclIsUndefined() or name=\'\' \r\n then c\r\n else \'let \'.concat(eName).concat(\': \').concat( typeAsOcl(selfObjectPackage, expression.calculatedType, expression.calculatedSimpleType, expression.calculatedSingular) ).concat(\' = \').concat(c).concat(\' in\')\r\nendif\r\n\r\nendif' calculatedOwnMandatoryDerive='if expression.oclIsUndefined() then false\r\nelse expression.calculatedMandatory endif' calculatedOwnSingularDerive='if expression.oclIsUndefined() then true\r\nelse expression.calculatedSingular endif' calculatedOwnSimpleTypeDerive='if expression.oclIsUndefined() then SimpleType::None\r\nelse expression.calculatedSimpleType endif' calculatedOwnTypeDerive='if expression.oclIsUndefined() then null\r\nelse expression.calculatedType endif'"
 * @generated
 */

public interface MNamedConstant extends MAbstractLet {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #setExpression(MConstantLet)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNamedConstant_Expression()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MConstantLet getExpression();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MNamedConstant#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(MConstantLet value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MNamedConstant#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MConstantLet)
	 * @generated
	 */
	void unsetExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MNamedConstant#getExpression <em>Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression</em>' containment reference is set.
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MConstantLet)
	 * @generated
	 */
	boolean isSetExpression();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='Tuple{base=ExpressionBase::SelfObject}'"
	 * @generated
	 */
	MChain defaultValue();

} // MNamedConstant
