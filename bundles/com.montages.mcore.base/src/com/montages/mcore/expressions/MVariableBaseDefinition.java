/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MVariable Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MVariableBaseDefinition#getNamedExpression <em>Named Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MVariableBaseDefinition#getVariableLet <em>Variable Let</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMVariableBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<var> \'.concat(if namedExpression.oclIsUndefined() then \'\' else namedExpression.eName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='ExpressionBase::Variable' calculatedAsCodeDerive='if (let e: Boolean = namedExpression.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'MISSING EXPRESSION\'\n  else if namedExpression.oclIsUndefined()\n  then null\n  else namedExpression.eName\nendif\nendif\n' calculatedMandatoryDerive='if (let e: Boolean = variableLet.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen true\n  else if variableLet.oclIsUndefined()\n  then null\n  else variableLet.calculatedMandatory\nendif\nendif\n' calculatedSingularDerive='if namedExpression.oclIsUndefined() then null\nelse namedExpression.calculatedSingular\nendif\n' calculatedSimpleTypeDerive='if variableLet.oclIsUndefined() then SimpleType::None\r\nelse variableLet.calculatedSimpleType\r\nendif' calculatedTypeDerive='if (let e: Boolean = variableLet.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if variableLet.oclIsUndefined()\n  then null\n  else variableLet.calculatedType\nendif\nendif\n'"
 * @generated
 */

public interface MVariableBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Named Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Expression</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Expression</em>' reference.
	 * @see #isSetNamedExpression()
	 * @see #unsetNamedExpression()
	 * @see #setNamedExpression(MNamedExpression)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMVariableBaseDefinition_NamedExpression()
	 * @model unsettable="true"
	 * @generated
	 */
	MNamedExpression getNamedExpression();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MVariableBaseDefinition#getNamedExpression <em>Named Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Expression</em>' reference.
	 * @see #isSetNamedExpression()
	 * @see #unsetNamedExpression()
	 * @see #getNamedExpression()
	 * @generated
	 */
	void setNamedExpression(MNamedExpression value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MVariableBaseDefinition#getNamedExpression <em>Named Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedExpression()
	 * @see #getNamedExpression()
	 * @see #setNamedExpression(MNamedExpression)
	 * @generated
	 */
	void unsetNamedExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MVariableBaseDefinition#getNamedExpression <em>Named Expression</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Expression</em>' reference is set.
	 * @see #unsetNamedExpression()
	 * @see #getNamedExpression()
	 * @see #setNamedExpression(MNamedExpression)
	 * @generated
	 */
	boolean isSetNamedExpression();

	/**
	 * Returns the value of the '<em><b>Variable Let</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Let</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Let</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMVariableBaseDefinition_VariableLet()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if namedExpression.expression.oclIsUndefined() then  null\r\nelse if namedExpression.expression.oclIsKindOf(MToplevelExpression) \r\n\tthen namedExpression.expression.oclAsType(MToplevelExpression)\r\n\telse null endif \r\nendif'"
	 * @generated
	 */
	MToplevelExpression getVariableLet();

} // MVariableBaseDefinition
