/**
 */
package com.montages.mcore.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MProcessor</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMProcessor()
 * @model
 * @generated
 */
public enum MProcessor implements Enumerator {
	/**
	 * The '<em><b>None</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE_VALUE
	 * @generated
	 * @ordered
	 */
	NONE(0, "None", "n/a"),

	/**
	 * The '<em><b>Is Null</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_NULL_VALUE
	 * @generated
	 * @ordered
	 */
	IS_NULL(1, "IsNull", " = null"),

	/**
	 * The '<em><b>Not Null</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_NULL_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_NULL(2, "NotNull", " <> null"),

	/**
	 * The '<em><b>To Boolean</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_BOOLEAN_VALUE
	 * @generated
	 * @ordered
	 */
	TO_BOOLEAN(3, "ToBoolean", "toBoolean()"),

	/**
	 * The '<em><b>Is False</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_FALSE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_FALSE(4, "IsFalse", " = false"),
	/**
	 * The '<em><b>Is True</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_TRUE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_TRUE(5, "IsTrue", " = true"),
	/**
	 * The '<em><b>Not</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_VALUE
	 * @generated
	 * @ordered
	 */
	NOT(6, "Not", "not"),
	/**
	 * The '<em><b>To Integer</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_INTEGER_VALUE
	 * @generated
	 * @ordered
	 */
	TO_INTEGER(7, "ToInteger", "toInteger()"),
	/**
	 * The '<em><b>Is Zero</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ZERO(8, "IsZero", " = 0"),
	/**
	 * The '<em><b>Is One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ONE(9, "IsOne", " = 1"),
	/**
	 * The '<em><b>Plus One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLUS_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	PLUS_ONE(10, "PlusOne", " + 1"),
	/**
	 * The '<em><b>Minus One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUS_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	MINUS_ONE(11, "MinusOne", " - 1"),
	/**
	 * The '<em><b>Times Minus One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TIMES_MINUS_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	TIMES_MINUS_ONE(12, "TimesMinusOne", " * -1"),
	/**
	 * The '<em><b>Absolute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABSOLUTE_VALUE
	 * @generated
	 * @ordered
	 */
	ABSOLUTE(13, "Absolute", "abs()"),
	/**
	 * The '<em><b>Round</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROUND_VALUE
	 * @generated
	 * @ordered
	 */
	ROUND(14, "Round", "round()"),
	/**
	 * The '<em><b>Floor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FLOOR_VALUE
	 * @generated
	 * @ordered
	 */
	FLOOR(15, "Floor", "floor()"),
	/**
	 * The '<em><b>To Real</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_REAL_VALUE
	 * @generated
	 * @ordered
	 */
	TO_REAL(16, "ToReal", "toReal()"),
	/**
	 * The '<em><b>One Divided By</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ONE_DIVIDED_BY_VALUE
	 * @generated
	 * @ordered
	 */
	ONE_DIVIDED_BY(17, "OneDividedBy", "1/x"),
	/**
	 * The '<em><b>To String</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_STRING_VALUE
	 * @generated
	 * @ordered
	 */
	TO_STRING(18, "ToString", "toString()"),
	/**
	 * The '<em><b>Trim</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRIM_VALUE
	 * @generated
	 * @ordered
	 */
	TRIM(19, "Trim", "trim()"),
	/**
	 * The '<em><b>All Lower Case</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL_LOWER_CASE_VALUE
	 * @generated
	 * @ordered
	 */
	ALL_LOWER_CASE(20, "AllLowerCase", "allLowerCase()"),
	/**
	 * The '<em><b>All Upper Case</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL_UPPER_CASE_VALUE
	 * @generated
	 * @ordered
	 */
	ALL_UPPER_CASE(21, "AllUpperCase", "allUpperCase()"),
	/**
	 * The '<em><b>First Upper Case</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIRST_UPPER_CASE_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_UPPER_CASE(22, "FirstUpperCase", "firstUpperCase()"),
	/**
	 * The '<em><b>Camel Case Lower</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAMEL_CASE_LOWER_VALUE
	 * @generated
	 * @ordered
	 */
	CAMEL_CASE_LOWER(23, "CamelCaseLower", "camelCaseLower()"),
	/**
	 * The '<em><b>Camel Case Upper</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAMEL_CASE_UPPER_VALUE
	 * @generated
	 * @ordered
	 */
	CAMEL_CASE_UPPER(24, "CamelCaseUpper", "camelCaseUpper()"),
	/**
	 * The '<em><b>Camel Case To Business</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAMEL_CASE_TO_BUSINESS_VALUE
	 * @generated
	 * @ordered
	 */
	CAMEL_CASE_TO_BUSINESS(25, "CamelCaseToBusiness", "camelCaseToBusiness()"),
	/**
	 * The '<em><b>To Date</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_DATE_VALUE
	 * @generated
	 * @ordered
	 */
	TO_DATE(26, "ToDate", "toDate()"),
	/**
	 * The '<em><b>Year</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #YEAR_VALUE
	 * @generated
	 * @ordered
	 */
	YEAR(27, "Year", "year()"),
	/**
	 * The '<em><b>Month</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MONTH_VALUE
	 * @generated
	 * @ordered
	 */
	MONTH(28, "Month", "month()"),
	/**
	 * The '<em><b>Day</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DAY_VALUE
	 * @generated
	 * @ordered
	 */
	DAY(29, "Day", "day()"),
	/**
	 * The '<em><b>Hour</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HOUR_VALUE
	 * @generated
	 * @ordered
	 */
	HOUR(30, "Hour", "hour()"),
	/**
	 * The '<em><b>Minute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUTE_VALUE
	 * @generated
	 * @ordered
	 */
	MINUTE(31, "Minute", "minute()"),
	/**
	 * The '<em><b>Second</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SECOND_VALUE
	 * @generated
	 * @ordered
	 */
	SECOND(32, "Second", "second()"),
	/**
	 * The '<em><b>To Yyyy Mm Dd</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_YYYY_MM_DD_VALUE
	 * @generated
	 * @ordered
	 */
	TO_YYYY_MM_DD(33, "ToYyyyMmDd", "toYyyyMmDd()"),
	/**
	 * The '<em><b>To Hh Mm</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_HH_MM_VALUE
	 * @generated
	 * @ordered
	 */
	TO_HH_MM(34, "ToHhMm", "toHhMm()"),
	/**
	 * The '<em><b>As Ordered Set</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_ORDERED_SET_VALUE
	 * @generated
	 * @ordered
	 */
	AS_ORDERED_SET(35, "AsOrderedSet", "asOrderedSet()"),
	/**
	 * The '<em><b>Is Empty</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	IS_EMPTY(36, "IsEmpty", "isEmpty()"),
	/**
	 * The '<em><b>Not Empty</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_EMPTY(37, "NotEmpty", "notEmpty()"),
	/**
	 * The '<em><b>Size</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIZE_VALUE
	 * @generated
	 * @ordered
	 */
	SIZE(38, "Size", "size()"),
	/**
	 * The '<em><b>And</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AND_VALUE
	 * @generated
	 * @ordered
	 */
	AND(39, "And", "and()"),
	/**
	 * The '<em><b>Or</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OR_VALUE
	 * @generated
	 * @ordered
	 */
	OR(40, "Or", "or()"),
	/**
	 * The '<em><b>Sum</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUM_VALUE
	 * @generated
	 * @ordered
	 */
	SUM(41, "Sum", "sum()"),
	/**
	 * The '<em><b>First</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIRST_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST(42, "First", "first()"),
	/**
	 * The '<em><b>Last</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LAST_VALUE
	 * @generated
	 * @ordered
	 */
	LAST(43, "Last", "last()"),
	/**
	 * The '<em><b>Head</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HEAD_VALUE
	 * @generated
	 * @ordered
	 */
	HEAD(44, "Head", "head()"),
	/**
	 * The '<em><b>Tail</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TAIL_VALUE
	 * @generated
	 * @ordered
	 */
	TAIL(45, "Tail", "tail()"),
	/**
	 * The '<em><b>Is Invalid</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_INVALID_VALUE
	 * @generated
	 * @ordered
	 */
	IS_INVALID(46, "IsInvalid", "isInvalid()"),
	/**
	 * The '<em><b>Container</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	CONTAINER(47, "Container", "container()");

	/**
	 * The '<em><b>None</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>None</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NONE
	 * @model name="None" literal="n/a"
	 * @generated
	 * @ordered
	 */
	public static final int NONE_VALUE = 0;

	/**
	 * The '<em><b>Is Null</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Null</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_NULL
	 * @model name="IsNull" literal=" = null"
	 * @generated
	 * @ordered
	 */
	public static final int IS_NULL_VALUE = 1;

	/**
	 * The '<em><b>Not Null</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not Null</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_NULL
	 * @model name="NotNull" literal=" <> null"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_NULL_VALUE = 2;

	/**
	 * The '<em><b>To Boolean</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To Boolean</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_BOOLEAN
	 * @model name="ToBoolean" literal="toBoolean()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_BOOLEAN_VALUE = 3;

	/**
	 * The '<em><b>Is False</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is False</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_FALSE
	 * @model name="IsFalse" literal=" = false"
	 * @generated
	 * @ordered
	 */
	public static final int IS_FALSE_VALUE = 4;

	/**
	 * The '<em><b>Is True</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is True</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_TRUE
	 * @model name="IsTrue" literal=" = true"
	 * @generated
	 * @ordered
	 */
	public static final int IS_TRUE_VALUE = 5;

	/**
	 * The '<em><b>Not</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT
	 * @model name="Not" literal="not"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_VALUE = 6;

	/**
	 * The '<em><b>To Integer</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To Integer</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_INTEGER
	 * @model name="ToInteger" literal="toInteger()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_INTEGER_VALUE = 7;

	/**
	 * The '<em><b>Is Zero</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Zero</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_ZERO
	 * @model name="IsZero" literal=" = 0"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ZERO_VALUE = 8;

	/**
	 * The '<em><b>Is One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_ONE
	 * @model name="IsOne" literal=" = 1"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ONE_VALUE = 9;

	/**
	 * The '<em><b>Plus One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Plus One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLUS_ONE
	 * @model name="PlusOne" literal=" + 1"
	 * @generated
	 * @ordered
	 */
	public static final int PLUS_ONE_VALUE = 10;

	/**
	 * The '<em><b>Minus One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minus One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUS_ONE
	 * @model name="MinusOne" literal=" - 1"
	 * @generated
	 * @ordered
	 */
	public static final int MINUS_ONE_VALUE = 11;

	/**
	 * The '<em><b>Times Minus One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Times Minus One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIMES_MINUS_ONE
	 * @model name="TimesMinusOne" literal=" * -1"
	 * @generated
	 * @ordered
	 */
	public static final int TIMES_MINUS_ONE_VALUE = 12;

	/**
	 * The '<em><b>Absolute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Absolute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ABSOLUTE
	 * @model name="Absolute" literal="abs()"
	 * @generated
	 * @ordered
	 */
	public static final int ABSOLUTE_VALUE = 13;

	/**
	 * The '<em><b>Round</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Round</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROUND
	 * @model name="Round" literal="round()"
	 * @generated
	 * @ordered
	 */
	public static final int ROUND_VALUE = 14;

	/**
	 * The '<em><b>Floor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Floor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FLOOR
	 * @model name="Floor" literal="floor()"
	 * @generated
	 * @ordered
	 */
	public static final int FLOOR_VALUE = 15;

	/**
	 * The '<em><b>To Real</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To Real</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_REAL
	 * @model name="ToReal" literal="toReal()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_REAL_VALUE = 16;

	/**
	 * The '<em><b>One Divided By</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>One Divided By</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ONE_DIVIDED_BY
	 * @model name="OneDividedBy" literal="1/x"
	 * @generated
	 * @ordered
	 */
	public static final int ONE_DIVIDED_BY_VALUE = 17;

	/**
	 * The '<em><b>To String</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To String</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_STRING
	 * @model name="ToString" literal="toString()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_STRING_VALUE = 18;

	/**
	 * The '<em><b>Trim</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Trim</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRIM
	 * @model name="Trim" literal="trim()"
	 * @generated
	 * @ordered
	 */
	public static final int TRIM_VALUE = 19;

	/**
	 * The '<em><b>All Lower Case</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>All Lower Case</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ALL_LOWER_CASE
	 * @model name="AllLowerCase" literal="allLowerCase()"
	 * @generated
	 * @ordered
	 */
	public static final int ALL_LOWER_CASE_VALUE = 20;

	/**
	 * The '<em><b>All Upper Case</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>All Upper Case</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ALL_UPPER_CASE
	 * @model name="AllUpperCase" literal="allUpperCase()"
	 * @generated
	 * @ordered
	 */
	public static final int ALL_UPPER_CASE_VALUE = 21;

	/**
	 * The '<em><b>First Upper Case</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>First Upper Case</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_UPPER_CASE
	 * @model name="FirstUpperCase" literal="firstUpperCase()"
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_UPPER_CASE_VALUE = 22;

	/**
	 * The '<em><b>Camel Case Lower</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Camel Case Lower</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAMEL_CASE_LOWER
	 * @model name="CamelCaseLower" literal="camelCaseLower()"
	 * @generated
	 * @ordered
	 */
	public static final int CAMEL_CASE_LOWER_VALUE = 23;

	/**
	 * The '<em><b>Camel Case Upper</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Camel Case Upper</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAMEL_CASE_UPPER
	 * @model name="CamelCaseUpper" literal="camelCaseUpper()"
	 * @generated
	 * @ordered
	 */
	public static final int CAMEL_CASE_UPPER_VALUE = 24;

	/**
	 * The '<em><b>Camel Case To Business</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Camel Case To Business</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAMEL_CASE_TO_BUSINESS
	 * @model name="CamelCaseToBusiness" literal="camelCaseToBusiness()"
	 * @generated
	 * @ordered
	 */
	public static final int CAMEL_CASE_TO_BUSINESS_VALUE = 25;

	/**
	 * The '<em><b>To Date</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To Date</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_DATE
	 * @model name="ToDate" literal="toDate()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_DATE_VALUE = 26;

	/**
	 * The '<em><b>Year</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Year</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #YEAR
	 * @model name="Year" literal="year()"
	 * @generated
	 * @ordered
	 */
	public static final int YEAR_VALUE = 27;

	/**
	 * The '<em><b>Month</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Month</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MONTH
	 * @model name="Month" literal="month()"
	 * @generated
	 * @ordered
	 */
	public static final int MONTH_VALUE = 28;

	/**
	 * The '<em><b>Day</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Day</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DAY
	 * @model name="Day" literal="day()"
	 * @generated
	 * @ordered
	 */
	public static final int DAY_VALUE = 29;

	/**
	 * The '<em><b>Hour</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hour</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HOUR
	 * @model name="Hour" literal="hour()"
	 * @generated
	 * @ordered
	 */
	public static final int HOUR_VALUE = 30;

	/**
	 * The '<em><b>Minute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUTE
	 * @model name="Minute" literal="minute()"
	 * @generated
	 * @ordered
	 */
	public static final int MINUTE_VALUE = 31;

	/**
	 * The '<em><b>Second</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Second</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SECOND
	 * @model name="Second" literal="second()"
	 * @generated
	 * @ordered
	 */
	public static final int SECOND_VALUE = 32;

	/**
	 * The '<em><b>To Yyyy Mm Dd</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To Yyyy Mm Dd</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_YYYY_MM_DD
	 * @model name="ToYyyyMmDd" literal="toYyyyMmDd()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_YYYY_MM_DD_VALUE = 33;

	/**
	 * The '<em><b>To Hh Mm</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To Hh Mm</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_HH_MM
	 * @model name="ToHhMm" literal="toHhMm()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_HH_MM_VALUE = 34;

	/**
	 * The '<em><b>As Ordered Set</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Ordered Set</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_ORDERED_SET
	 * @model name="AsOrderedSet" literal="asOrderedSet()"
	 * @generated
	 * @ordered
	 */
	public static final int AS_ORDERED_SET_VALUE = 35;

	/**
	 * The '<em><b>Is Empty</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Empty</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_EMPTY
	 * @model name="IsEmpty" literal="isEmpty()"
	 * @generated
	 * @ordered
	 */
	public static final int IS_EMPTY_VALUE = 36;

	/**
	 * The '<em><b>Not Empty</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not Empty</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_EMPTY
	 * @model name="NotEmpty" literal="notEmpty()"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_EMPTY_VALUE = 37;

	/**
	 * The '<em><b>Size</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Size</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIZE
	 * @model name="Size" literal="size()"
	 * @generated
	 * @ordered
	 */
	public static final int SIZE_VALUE = 38;

	/**
	 * The '<em><b>And</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>And</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AND
	 * @model name="And" literal="and()"
	 * @generated
	 * @ordered
	 */
	public static final int AND_VALUE = 39;

	/**
	 * The '<em><b>Or</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Or</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OR
	 * @model name="Or" literal="or()"
	 * @generated
	 * @ordered
	 */
	public static final int OR_VALUE = 40;

	/**
	 * The '<em><b>Sum</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sum</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUM
	 * @model name="Sum" literal="sum()"
	 * @generated
	 * @ordered
	 */
	public static final int SUM_VALUE = 41;

	/**
	 * The '<em><b>First</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>First</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST
	 * @model name="First" literal="first()"
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_VALUE = 42;

	/**
	 * The '<em><b>Last</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Last</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LAST
	 * @model name="Last" literal="last()"
	 * @generated
	 * @ordered
	 */
	public static final int LAST_VALUE = 43;

	/**
	 * The '<em><b>Head</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Head</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HEAD
	 * @model name="Head" literal="head()"
	 * @generated
	 * @ordered
	 */
	public static final int HEAD_VALUE = 44;

	/**
	 * The '<em><b>Tail</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Tail</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TAIL
	 * @model name="Tail" literal="tail()"
	 * @generated
	 * @ordered
	 */
	public static final int TAIL_VALUE = 45;

	/**
	 * The '<em><b>Is Invalid</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Invalid</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_INVALID
	 * @model name="IsInvalid" literal="isInvalid()"
	 * @generated
	 * @ordered
	 */
	public static final int IS_INVALID_VALUE = 46;

	/**
	 * The '<em><b>Container</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Container</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTAINER
	 * @model name="Container" literal="container()"
	 * @generated
	 * @ordered
	 */
	public static final int CONTAINER_VALUE = 47;

	/**
	 * An array of all the '<em><b>MProcessor</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MProcessor[] VALUES_ARRAY = new MProcessor[] { NONE,
			IS_NULL, NOT_NULL, TO_BOOLEAN, IS_FALSE, IS_TRUE, NOT, TO_INTEGER,
			IS_ZERO, IS_ONE, PLUS_ONE, MINUS_ONE, TIMES_MINUS_ONE, ABSOLUTE,
			ROUND, FLOOR, TO_REAL, ONE_DIVIDED_BY, TO_STRING, TRIM,
			ALL_LOWER_CASE, ALL_UPPER_CASE, FIRST_UPPER_CASE, CAMEL_CASE_LOWER,
			CAMEL_CASE_UPPER, CAMEL_CASE_TO_BUSINESS, TO_DATE, YEAR, MONTH, DAY,
			HOUR, MINUTE, SECOND, TO_YYYY_MM_DD, TO_HH_MM, AS_ORDERED_SET,
			IS_EMPTY, NOT_EMPTY, SIZE, AND, OR, SUM, FIRST, LAST, HEAD, TAIL,
			IS_INVALID, CONTAINER, };

	/**
	 * A public read-only list of all the '<em><b>MProcessor</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MProcessor> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MProcessor</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MProcessor get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MProcessor result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProcessor</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MProcessor getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MProcessor result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProcessor</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MProcessor get(int value) {
		switch (value) {
		case NONE_VALUE:
			return NONE;
		case IS_NULL_VALUE:
			return IS_NULL;
		case NOT_NULL_VALUE:
			return NOT_NULL;
		case TO_BOOLEAN_VALUE:
			return TO_BOOLEAN;
		case IS_FALSE_VALUE:
			return IS_FALSE;
		case IS_TRUE_VALUE:
			return IS_TRUE;
		case NOT_VALUE:
			return NOT;
		case TO_INTEGER_VALUE:
			return TO_INTEGER;
		case IS_ZERO_VALUE:
			return IS_ZERO;
		case IS_ONE_VALUE:
			return IS_ONE;
		case PLUS_ONE_VALUE:
			return PLUS_ONE;
		case MINUS_ONE_VALUE:
			return MINUS_ONE;
		case TIMES_MINUS_ONE_VALUE:
			return TIMES_MINUS_ONE;
		case ABSOLUTE_VALUE:
			return ABSOLUTE;
		case ROUND_VALUE:
			return ROUND;
		case FLOOR_VALUE:
			return FLOOR;
		case TO_REAL_VALUE:
			return TO_REAL;
		case ONE_DIVIDED_BY_VALUE:
			return ONE_DIVIDED_BY;
		case TO_STRING_VALUE:
			return TO_STRING;
		case TRIM_VALUE:
			return TRIM;
		case ALL_LOWER_CASE_VALUE:
			return ALL_LOWER_CASE;
		case ALL_UPPER_CASE_VALUE:
			return ALL_UPPER_CASE;
		case FIRST_UPPER_CASE_VALUE:
			return FIRST_UPPER_CASE;
		case CAMEL_CASE_LOWER_VALUE:
			return CAMEL_CASE_LOWER;
		case CAMEL_CASE_UPPER_VALUE:
			return CAMEL_CASE_UPPER;
		case CAMEL_CASE_TO_BUSINESS_VALUE:
			return CAMEL_CASE_TO_BUSINESS;
		case TO_DATE_VALUE:
			return TO_DATE;
		case YEAR_VALUE:
			return YEAR;
		case MONTH_VALUE:
			return MONTH;
		case DAY_VALUE:
			return DAY;
		case HOUR_VALUE:
			return HOUR;
		case MINUTE_VALUE:
			return MINUTE;
		case SECOND_VALUE:
			return SECOND;
		case TO_YYYY_MM_DD_VALUE:
			return TO_YYYY_MM_DD;
		case TO_HH_MM_VALUE:
			return TO_HH_MM;
		case AS_ORDERED_SET_VALUE:
			return AS_ORDERED_SET;
		case IS_EMPTY_VALUE:
			return IS_EMPTY;
		case NOT_EMPTY_VALUE:
			return NOT_EMPTY;
		case SIZE_VALUE:
			return SIZE;
		case AND_VALUE:
			return AND;
		case OR_VALUE:
			return OR;
		case SUM_VALUE:
			return SUM;
		case FIRST_VALUE:
			return FIRST;
		case LAST_VALUE:
			return LAST;
		case HEAD_VALUE:
			return HEAD;
		case TAIL_VALUE:
			return TAIL;
		case IS_INVALID_VALUE:
			return IS_INVALID;
		case CONTAINER_VALUE:
			return CONTAINER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MProcessor(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MProcessor
