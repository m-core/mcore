/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MTyped;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.MExprAnnotation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getAsCode <em>As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getAsBasicCode <em>As Basic Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getCollector <em>Collector</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getEntireScope <em>Entire Scope</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeBase <em>Scope Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeSelf <em>Scope Self</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeTrg <em>Scope Trg</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeObj <em>Scope Obj</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeLiteralConstants <em>Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeVariables <em>Scope Variables</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeIterator <em>Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeAccumulator <em>Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeParameters <em>Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeContainerBase <em>Scope Container Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeNumberBase <em>Scope Number Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getScopeSource <em>Scope Source</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalEntireScope <em>Local Entire Scope</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeBase <em>Local Scope Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeSelf <em>Local Scope Self</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeTrg <em>Local Scope Trg</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeObj <em>Local Scope Obj</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeSource <em>Local Scope Source</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeVariables <em>Local Scope Variables</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeIterator <em>Local Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeAccumulator <em>Local Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeParameters <em>Local Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getLocalScopeContainer <em>Local Scope Container</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getContainingAnnotation <em>Containing Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getContainingExpression <em>Containing Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getIsComplexExpression <em>Is Complex Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getIsSubExpression <em>Is Sub Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getCalculatedOwnType <em>Calculated Own Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getCalculatedOwnSingular <em>Calculated Own Singular</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getSelfObjectType <em>Self Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getSelfObjectPackage <em>Self Object Package</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getTargetObjectType <em>Target Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getTargetSimpleType <em>Target Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getObjectObjectType <em>Object Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getExpectedReturnType <em>Expected Return Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getExpectedReturnSimpleType <em>Expected Return Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getIsReturnValueSingular <em>Is Return Value Singular</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getSrcObjectType <em>Src Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpression#getSrcObjectSimpleType <em>Src Object Simple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'OVERRIDE IN SUBCLASS\'\n' calculatedTypeDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnType\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedType\nendif\nendif\n' calculatedMandatoryDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnMandatory\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedMandatory\nendif\nendif\n' calculatedSingularDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnSingular\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedSingular\nendif\nendif\n' calculatedSimpleTypeDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnSimpleType\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedSimpleType\nendif\nendif\n'"
 * @generated
 */

public interface MAbstractExpression extends MTyped {
	/**
	 * Returns the value of the '<em><b>As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The OCL for this expression. After all transformations have been applied (e.g. conversion of arity, casting, etc.)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>As Code</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_AsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen asBasicCode\n  else if collector.oclIsUndefined()\n  then null\n  else collector.asBasicCode\nendif\nendif\n'"
	 * @generated
	 */
	String getAsCode();

	/**
	 * Returns the value of the '<em><b>As Basic Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The most basic code for the expression, sometimes this code needs to be augmented (e.g. to convert expression arity). Sub-classes should overwrite this method and not asCode...usually.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>As Basic Code</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_AsBasicCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='eClass().name.concat(\' : <?>\')'"
	 * @generated
	 */
	String getAsBasicCode();

	/**
	 * Returns the value of the '<em><b>Collector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collector</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collector</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_Collector()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: mcore::expressions::MCollectionExpression = null in nl\n'"
	 * @generated
	 */
	MCollectionExpression getCollector();

	/**
	 * Returns the value of the '<em><b>Entire Scope</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MAbstractBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entire Scope</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entire Scope</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_EntireScope()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localEntireScope \r\nelse /* Note this is necessary for some cases like scopeIterator that otherwise won\'t work \052/ \r\nscopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(\r\nscopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeNumberBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeContainerBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAbstractBaseDefinition> getEntireScope();

	/**
	 * Returns the value of the '<em><b>Scope Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Base</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeBase \r\nelse containingExpression.scopeBase\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MBaseDefinition> getScopeBase();

	/**
	 * Returns the value of the '<em><b>Scope Self</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MSelfBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Self</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Self</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeSelf()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeSelf\r\nelse containingExpression.scopeSelf\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSelfBaseDefinition> getScopeSelf();

	/**
	 * Returns the value of the '<em><b>Scope Trg</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MTargetBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Trg</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Trg</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeTrg()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeTrg\r\nelse containingExpression.scopeTrg\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MTargetBaseDefinition> getScopeTrg();

	/**
	 * Returns the value of the '<em><b>Scope Obj</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MObjectBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Obj</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Obj</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeObj()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='--todo: only for updates...\r\nif containingExpression.oclIsUndefined() then localScopeObj else containingExpression.scopeObj endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectBaseDefinition> getScopeObj();

	/**
	 * Returns the value of the '<em><b>Scope Simple Type Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Simple Type Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Simple Type Constants</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeSimpleTypeConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeSimpleTypeConstants\r\nelse containingExpression.scopeSimpleTypeConstants\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSimpleTypeConstantBaseDefinition> getScopeSimpleTypeConstants();

	/**
	 * Returns the value of the '<em><b>Scope Literal Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MLiteralConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Literal Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Literal Constants</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeLiteralConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeLiteralConstants\r\nelse containingExpression.scopeLiteralConstants\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MLiteralConstantBaseDefinition> getScopeLiteralConstants();

	/**
	 * Returns the value of the '<em><b>Scope Object Reference Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Object Reference Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Object Reference Constants</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeObjectReferenceConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeObjectReferenceConstants\r\nelse containingExpression.scopeObjectReferenceConstants\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectReferenceConstantBaseDefinition> getScopeObjectReferenceConstants();

	/**
	 * Returns the value of the '<em><b>Scope Variables</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MVariableBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Variables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Variables</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeVariables()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeVariables\r\nelse containingExpression.scopeVariables\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MVariableBaseDefinition> getScopeVariables();

	/**
	 * Returns the value of the '<em><b>Scope Iterator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MIteratorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Iterator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Iterator</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeIterator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr \052/\r\n\r\nif containingExpression.oclIsUndefined()\r\nthen localScopeIterator\r\nelse containingExpression.scopeIterator->union(localScopeIterator)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MIteratorBaseDefinition> getScopeIterator();

	/**
	 * Returns the value of the '<em><b>Scope Accumulator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MAccumulatorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Accumulator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Accumulator</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeAccumulator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr \052/\r\n\r\nif containingExpression.oclIsUndefined()\r\nthen localScopeAccumulator\r\nelse containingExpression.scopeAccumulator->union(localScopeAccumulator)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAccumulatorBaseDefinition> getScopeAccumulator();

	/**
	 * Returns the value of the '<em><b>Scope Parameters</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MParameterBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Parameters</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeParameters()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeParameters\r\nelse containingExpression.scopeParameters\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MParameterBaseDefinition> getScopeParameters();

	/**
	 * Returns the value of the '<em><b>Scope Container Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MContainerBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Container Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Container Base</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeContainerBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='scopeContainerBase'"
	 *        annotation="http://www.xocl.org/OCL derive='localScopeContainer'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MContainerBaseDefinition> getScopeContainerBase();

	/**
	 * Returns the value of the '<em><b>Scope Number Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MNumberBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Number Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Number Base</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeNumberBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='scopeNumberBase'"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen self.localScopeNumberBaseDefinition\r\nelse containingExpression.scopeNumberBase\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MNumberBaseDefinition> getScopeNumberBase();

	/**
	 * Returns the value of the '<em><b>Scope Source</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MSourceBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Source</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ScopeSource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='scopeSource'"
	 *        annotation="http://www.xocl.org/OCL derive='\r\nif containingExpression.oclIsUndefined() then localScopeSource else containingExpression.scopeSource endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSourceBaseDefinition> getScopeSource();

	/**
	 * Returns the value of the '<em><b>Local Entire Scope</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MAbstractBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Entire Scope</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Entire Scope</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalEntireScope()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='localScopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(\r\nlocalScopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeNumberBaseDefinition->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeContainer->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAbstractBaseDefinition> getLocalEntireScope();

	/**
	 * Returns the value of the '<em><b>Local Scope Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Base</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::NullValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::FalseValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::TrueValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyStringValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyCollection}  \r\n}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MBaseDefinition> getLocalScopeBase();

	/**
	 * Returns the value of the '<em><b>Local Scope Self</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MSelfBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Self</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Self</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeSelf()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{Tuple{debug=\'\'}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSelfBaseDefinition> getLocalScopeSelf();

	/**
	 * Returns the value of the '<em><b>Local Scope Trg</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MTargetBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Trg</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Trg</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeTrg()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if targetObjectType.oclIsUndefined() and self.targetSimpleType=mcore::SimpleType::None\r\n\tthen OrderedSet{}\r\n\telse OrderedSet{Tuple{debug=\'\'}} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MTargetBaseDefinition> getLocalScopeTrg();

	/**
	 * Returns the value of the '<em><b>Local Scope Obj</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MObjectBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Obj</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Obj</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeObj()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if objectObjectType.oclIsUndefined() then OrderedSet{} else OrderedSet{Tuple{debug=\'\'}} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectBaseDefinition> getLocalScopeObj();

	/**
	 * Returns the value of the '<em><b>Local Scope Source</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MSourceBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Source</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeSource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if srcObjectType.oclIsUndefined() and self.srcObjectSimpleType=mcore::SimpleType::None\r\n\tthen OrderedSet{}\r\n\telse OrderedSet{Tuple{debug=\'\'}} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSourceBaseDefinition> getLocalScopeSource();

	/**
	 * Returns the value of the '<em><b>Local Scope Simple Type Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Simple Type Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Simple Type Constants</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeSimpleTypeConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)\r\nthen let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in\052/\r\nlet a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n  a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mcore::expressions::MSimpleTypeConstantLet) and (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\nendif\r\n/* else OrderedSet{} endif\r\nendif\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSimpleTypeConstantBaseDefinition> getLocalScopeSimpleTypeConstants();

	/**
	 * Returns the value of the '<em><b>Local Scope Literal Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MLiteralConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Literal Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Literal Constants</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeLiteralConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/* if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)\r\nthen let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in \052/\r\nlet a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n  a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mcore::expressions::MLiteralLet) and (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\n  endif\r\n/*\r\nelse OrderedSet{} endif\r\nendif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MLiteralConstantBaseDefinition> getLocalScopeLiteralConstants();

	/**
	 * Returns the value of the '<em><b>Local Scope Object Reference Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Object Reference Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Object Reference Constants</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeObjectReferenceConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)\r\nthen let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in \052/\r\nlet a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n\r\n  a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mcore::expressions::MObjectReferenceLet) and (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\nendif\r\n/* else OrderedSet{} endif\r\nendif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectReferenceConstantBaseDefinition> getLocalScopeObjectReferenceConstants();

	/**
	 * Returns the value of the '<em><b>Local Scope Variables</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MVariableBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Variables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Variables</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeVariables()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined()\r\n  then  OrderedSet{} \r\n  else\r\n  a.namedExpression->iterate (i: mcore::expressions::MNamedExpression; r: OrderedSet(Tuple(namedExpression:mcore::expressions::MNamedExpression))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if  (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedExpression=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  ) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MVariableBaseDefinition> getLocalScopeVariables();

	/**
	 * Returns the value of the '<em><b>Local Scope Iterator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MIteratorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Iterator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Iterator</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeIterator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)\r\nthen let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in\r\n  if c.iteratorVar.oclIsUndefined() then OrderedSet{}\r\n  \telse OrderedSet{Tuple{iterator=c.iteratorVar}} endif\r\nelse OrderedSet{} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MIteratorBaseDefinition> getLocalScopeIterator();

	/**
	 * Returns the value of the '<em><b>Local Scope Accumulator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MAccumulatorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Accumulator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Accumulator</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeAccumulator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)\r\nthen let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in\r\n  if c.accumulatorVar.oclIsUndefined() then OrderedSet{}\r\n  \telse OrderedSet{Tuple{accumulator=c.accumulatorVar}} endif\r\nelse OrderedSet{} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAccumulatorBaseDefinition> getLocalScopeAccumulator();

	/**
	 * Returns the value of the '<em><b>Local Scope Parameters</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MParameterBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Parameters</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeParameters()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingAnnotation=null \r\n  then OrderedSet{}\r\n  else \r\n  let e:MModelElement= self.containingAnnotation.annotatedElement in\r\n    if e=null \r\n      then OrderedSet{}\r\n      else if not e.oclIsKindOf(mcore::MOperationSignature)\r\n        then OrderedSet{}\r\n        else e.oclAsType(mcore::MOperationSignature).parameter\r\n             ->iterate (i: mcore::MParameter; r: OrderedSet(Tuple(parameter:mcore::MParameter))=OrderedSet{} \r\n                            |r->append(Tuple{parameter=i}) )\r\n    \tendif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MParameterBaseDefinition> getLocalScopeParameters();

	/**
	 * Returns the value of the '<em><b>Local Scope Number Base Definition</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MNumberBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Number Base Definition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Number Base Definition</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeNumberBaseDefinition()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Local Scope NumberBase Definition'"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::ZeroValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::OneValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::MinusOneValue}\r\n}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MNumberBaseDefinition> getLocalScopeNumberBaseDefinition();

	/**
	 * Returns the value of the '<em><b>Local Scope Container</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MContainerBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Container</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Container</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeContainer()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{Tuple{debug=\'\'}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MContainerBaseDefinition> getLocalScopeContainer();

	/**
	 * Returns the value of the '<em><b>Containing Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The annotation that contains, directly or indirectly, this expression.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Annotation</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ContainingAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='-- expressions can now be as well annotations, so the \"containing\" annotation can be self.\r\nif self.oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n\tthen self.oclAsType(mcore::annotations::MExprAnnotation) \r\nelse if eContainer().oclIsUndefined() \r\n\tthen null \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n\tthen eContainer().oclAsType(mcore::annotations::MExprAnnotation) \r\nelse if eContainer().oclIsTypeOf(mcore::expressions::MNamedExpression) \r\n   then self.eContainer().oclAsType(mcore::expressions::MNamedExpression).containingAnnotation\r\nelse if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue) \r\n   then eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue)\r\n   \t\t\t.eContainer().oclAsType(mcore::expressions::MNewObjecct).containingAnnotation\r\nelse if containingExpression.oclIsUndefined() \r\n\tthen null \r\n\telse containingExpression.containingAnnotation endif endif endif endif endif\tendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	MExprAnnotation getContainingAnnotation();

	/**
	 * Returns the value of the '<em><b>Containing Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The expression, if any, that contains this one.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Expression</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ContainingExpression()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression)\r\nthen eContainer().oclAsType(mcore::expressions::MAbstractExpression)\r\n\r\n else null endif endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	MAbstractExpression getContainingExpression();

	/**
	 * Returns the value of the '<em><b>Is Complex Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * It is true when the resulting OCL is complex enought that it needs to be encloed in parenthesis when it's a sub-expression.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Complex Expression</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_IsComplexExpression()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	Boolean getIsComplexExpression();

	/**
	 * Returns the value of the '<em><b>Is Sub Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Sub Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Sub Expression</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_IsSubExpression()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined() then false\r\nelse not (containingExpression.oclIsKindOf(mcore::expressions::MNamedConstant) or containingExpression.oclIsKindOf(mcore::expressions::MNamedExpression)) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	Boolean getIsSubExpression();

	/**
	 * Returns the value of the '<em><b>Self Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Self Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Self Object Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_SelfObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let a:mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then null\r\n  else a.selfObjectTypeOfAnnotation endif\r\n\r\n/*if eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n  then eContainer().oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation\r\n--else if self.oclsKindOf(mcore::annotations::MExprAnnotation)\r\n--   then self.oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation\r\n\r\n    --else  if  eContainer().oclIsTypeOf(mcore::expressions::MAbstractExpression) then\r\n  -- eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType\r\n   else if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue)\r\n      then \r\n       if eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclIsKindOf(mcore::expressions::MNewObjecct)\r\n       then  eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclAsType(mcore::expressions::MNewObjecct).selfObjectType\r\n       else \r\n       null\r\n     endif\r\n       else eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType\r\n       endif\r\n  --endif\r\nendif\r\nendif \r\n--endif\r\n\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	MClassifier getSelfObjectType();

	/**
	 * Returns the value of the '<em><b>Self Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The package of "self" object. This is needed because, when writing types in OCL expressions, types not in same package than "self" need to be explicitely named.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Self Object Package</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_SelfObjectPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = selfObjectType.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if selfObjectType.oclIsUndefined()\n  then null\n  else selfObjectType.containingPackage\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	MPackage getSelfObjectPackage();

	/**
	 * Returns the value of the '<em><b>Target Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Object Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_TargetObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingAnnotation.oclIsUndefined()\r\n   then null\r\n   else self.containingAnnotation.targetObjectTypeOfAnnotation endif\r\n/*   \r\nif eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n  then eContainer().oclAsType(mcore::annotations::MExprAnnotation).targetObjectTypeOfAnnotation\r\n  else  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) then\r\n   eContainer().oclAsType(mcore::expressions::MAbstractExpression).targetObjectType else null endif\r\nendif\r\nendif \052/\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	MClassifier getTargetObjectType();

	/**
	 * Returns the value of the '<em><b>Target Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_TargetSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingAnnotation.oclIsUndefined()\n   then null\n   else self.containingAnnotation.targetSimpleTypeOfAnnotation endif\n/*\nif (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.targetSimpleTypeOfAnnotation\nendif\nendif\n\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	SimpleType getTargetSimpleType();

	/**
	 * Returns the value of the '<em><b>Object Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Object Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ObjectObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.oclIsKindOf(mcore::annotations::MUpdateValue)  \r\n  --replaced with self.\r\n     then self.oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation\r\nelse if self.oclIsKindOf(mcore::annotations::MUpdatePersistence)\r\n    then self.oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation\r\nelse if eContainer().oclIsUndefined()  then null \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MUpdateValue)  \r\n     then eContainer().oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation \r\n  else if eContainer().oclIsKindOf(mcore::annotations::MUpdatePersistence)  \r\n     then eContainer().oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation   \r\nelse  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) \r\n     then eContainer().oclAsType(mcore::expressions::MAbstractExpression).objectObjectType \r\n     else null endif endif endif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	MClassifier getObjectObjectType();

	/**
	 * Returns the value of the '<em><b>Expected Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The object type that the expression should return, based on where the containing annotation is attached.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Expected Return Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ExpectedReturnType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Expected ReturnType'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.expectedReturnTypeOfAnnotation\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	MClassifier getExpectedReturnType();

	/**
	 * Returns the value of the '<em><b>Expected Return Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected Return Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected Return Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_ExpectedReturnSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingAnnotation.oclIsUndefined() \r\n     then mcore::SimpleType::None\r\n     else containingAnnotation.expectedReturnSimpleTypeOfAnnotation endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	SimpleType getExpectedReturnSimpleType();

	/**
	 * Returns the value of the '<em><b>Is Return Value Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Return Value Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Return Value Mandatory</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_IsReturnValueMandatory()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.isReturnValueOfAnnotationMandatory\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getIsReturnValueMandatory();

	/**
	 * Returns the value of the '<em><b>Is Return Value Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Return Value Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Return Value Singular</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_IsReturnValueSingular()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.isReturnValueOfAnnotationSingular\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getIsReturnValueSingular();

	/**
	 * Returns the value of the '<em><b>Src Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Object Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_SrcObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let srcType : MClassifier =\r\nlet srcClassifier:MProperty = \r\nself.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty\r\nin \r\nif srcClassifier.oclIsUndefined() then null\r\nelse if  ((srcClassifier.hasStorage) and (srcClassifier.changeable)) then srcClassifier.type\r\nelse null \r\nendif\r\nendif\r\nin\r\nif srcType.oclIsUndefined() then null else srcType endif\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getSrcObjectType();

	/**
	 * Returns the value of the '<em><b>Src Object Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src Object Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Object Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_SrcObjectSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='src Object SimpleType'"
	 *        annotation="http://www.xocl.org/OCL derive='let srcType : SimpleType =\r\nlet srcClassifier:MProperty = \r\nself.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty\r\nin \r\nif srcClassifier.oclIsUndefined() then null\r\nelse if  (srcClassifier.hasStorage) and ( srcClassifier.changeable) then srcClassifier.simpleType\r\nelse null\r\nendif endif\r\nin\r\nif srcType.oclIsUndefined() then SimpleType::None else srcType endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	SimpleType getSrcObjectSimpleType();

	/**
	 * Returns the value of the '<em><b>Calculated Own Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Own Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Own Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_CalculatedOwnType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: mcore::MClassifier = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	MClassifier getCalculatedOwnType();

	/**
	 * Returns the value of the '<em><b>Calculated Own Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Own Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Own Mandatory</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_CalculatedOwnMandatory()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getCalculatedOwnMandatory();

	/**
	 * Returns the value of the '<em><b>Calculated Own Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Own Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Own Singular</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_CalculatedOwnSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getCalculatedOwnSingular();

	/**
	 * Returns the value of the '<em><b>Calculated Own Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Own Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Own Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpression_CalculatedOwnSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::SimpleType::None'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	SimpleType getCalculatedOwnSimpleType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if asCode.oclIsUndefined() then \'\'\r\nelse let s: String = asCode in\r\n\tif s.size() > 30 then s.substring(1, 27).concat(\'...\')\r\n\telse s endif\r\nendif'"
	 * @generated
	 */
	String getShortCode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='entireScope'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Base Scope' createColumn='true'"
	 * @generated
	 */
	EList<MAbstractBaseDefinition> getScope();

} // MAbstractExpression
