/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MLiteralLet#getEnumerationType <em>Enumeration Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MLiteralLet#getConstant1 <em>Constant1</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MLiteralLet#getConstant2 <em>Constant2</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MLiteralLet#getConstant3 <em>Constant3</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralLet()
 * @model annotation="http://www.xocl.org/OCL label='asCode'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Literal Constant\'\n' asBasicCodeDerive='let a: String = if constant1.oclIsUndefined() then \'\' else constant1.qualifiedName endif in\r\nlet b: String = if constant2.oclIsUndefined() then \'\' else constant2.qualifiedName endif in\r\nlet c: String = if constant3.oclIsUndefined() then \'\' else constant3.qualifiedName endif in\r\n\r\n if calculatedSingular\r\n  then a.concat(b).concat(c)\r\n  else asSetString(a, b, c)\r\nendif\r\n' calculatedOwnSingularDerive='let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) <= 1' calculatedOwnMandatoryDerive='true' calculatedOwnTypeDerive='enumerationType'"
 * @generated
 */

public interface MLiteralLet extends MConstantLet {
	/**
	 * Returns the value of the '<em><b>Enumeration Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type</em>' reference.
	 * @see #isSetEnumerationType()
	 * @see #unsetEnumerationType()
	 * @see #setEnumerationType(MClassifier)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralLet_EnumerationType()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.allLiterals()->size() > 0'"
	 * @generated
	 */
	MClassifier getEnumerationType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getEnumerationType <em>Enumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Type</em>' reference.
	 * @see #isSetEnumerationType()
	 * @see #unsetEnumerationType()
	 * @see #getEnumerationType()
	 * @generated
	 */
	void setEnumerationType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getEnumerationType <em>Enumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEnumerationType()
	 * @see #getEnumerationType()
	 * @see #setEnumerationType(MClassifier)
	 * @generated
	 */
	void unsetEnumerationType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getEnumerationType <em>Enumeration Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Enumeration Type</em>' reference is set.
	 * @see #unsetEnumerationType()
	 * @see #getEnumerationType()
	 * @see #setEnumerationType(MClassifier)
	 * @generated
	 */
	boolean isSetEnumerationType();

	/**
	 * Returns the value of the '<em><b>Constant1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant1</em>' reference.
	 * @see #isSetConstant1()
	 * @see #unsetConstant1()
	 * @see #setConstant1(MLiteral)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralLet_Constant1()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MLiteral getConstant1();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant1 <em>Constant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant1</em>' reference.
	 * @see #isSetConstant1()
	 * @see #unsetConstant1()
	 * @see #getConstant1()
	 * @generated
	 */
	void setConstant1(MLiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant1 <em>Constant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant1()
	 * @see #getConstant1()
	 * @see #setConstant1(MLiteral)
	 * @generated
	 */
	void unsetConstant1();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant1 <em>Constant1</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant1</em>' reference is set.
	 * @see #unsetConstant1()
	 * @see #getConstant1()
	 * @see #setConstant1(MLiteral)
	 * @generated
	 */
	boolean isSetConstant1();

	/**
	 * Returns the value of the '<em><b>Constant2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant2</em>' reference.
	 * @see #isSetConstant2()
	 * @see #unsetConstant2()
	 * @see #setConstant2(MLiteral)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralLet_Constant2()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MLiteral getConstant2();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant2 <em>Constant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant2</em>' reference.
	 * @see #isSetConstant2()
	 * @see #unsetConstant2()
	 * @see #getConstant2()
	 * @generated
	 */
	void setConstant2(MLiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant2 <em>Constant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant2()
	 * @see #getConstant2()
	 * @see #setConstant2(MLiteral)
	 * @generated
	 */
	void unsetConstant2();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant2 <em>Constant2</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant2</em>' reference is set.
	 * @see #unsetConstant2()
	 * @see #getConstant2()
	 * @see #setConstant2(MLiteral)
	 * @generated
	 */
	boolean isSetConstant2();

	/**
	 * Returns the value of the '<em><b>Constant3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant3</em>' reference.
	 * @see #isSetConstant3()
	 * @see #unsetConstant3()
	 * @see #setConstant3(MLiteral)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMLiteralLet_Constant3()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='getAllowedLiterals()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MLiteral getConstant3();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant3 <em>Constant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant3</em>' reference.
	 * @see #isSetConstant3()
	 * @see #unsetConstant3()
	 * @see #getConstant3()
	 * @generated
	 */
	void setConstant3(MLiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant3 <em>Constant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant3()
	 * @see #getConstant3()
	 * @see #setConstant3(MLiteral)
	 * @generated
	 */
	void unsetConstant3();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MLiteralLet#getConstant3 <em>Constant3</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant3</em>' reference is set.
	 * @see #unsetConstant3()
	 * @see #getConstant3()
	 * @see #setConstant3(MLiteral)
	 * @generated
	 */
	boolean isSetConstant3();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if self.enumerationType.oclIsUndefined() then OrderedSet{}\r\nelse if self.enumerationType.kind = ClassifierKind::Enumeration then enumerationType.allLiterals() else OrderedSet{} endif endif'"
	 * @generated
	 */
	EList<MLiteral> getAllowedLiterals();

} // MLiteralLet
