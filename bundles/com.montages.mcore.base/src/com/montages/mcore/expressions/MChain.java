/**
 */
package com.montages.mcore.expressions;

import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MChain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MChain#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMChain()
 * @model
 * @generated
 */

public interface MChain extends MBaseChain, MChainOrApplication {

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.expressions.MChainAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.expressions.MChainAction
	 * @see #setDoAction(MChainAction)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMChain_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::expressions::MChainAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MChainAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MChain#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.expressions.MChainAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MChainAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MChainAction trg);

} // MChain
