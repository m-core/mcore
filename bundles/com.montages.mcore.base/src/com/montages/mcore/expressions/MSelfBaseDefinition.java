/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSelf Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMSelfBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'self\'\n' calculatedBaseDerive='ExpressionBase::SelfObject' calculatedMandatoryDerive='true\n' calculatedTypeDerive='if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).selfObjectType\r\nelse null\r\nendif\r\n' calculatedSingularDerive='true\n'"
 * @generated
 */

public interface MSelfBaseDefinition extends MAbstractBaseDefinition {
} // MSelfBaseDefinition
