/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.MClassifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNew Objecct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MNewObjecct#getNewType <em>New Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MNewObjecct#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNewObjecct()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL abstractEntryDerive='entry->asOrderedSet()\n' asBasicCodeDerive='let c:String= \'Tuple{\' in\r\n c.concat(\r\n    if entry->isEmpty() then \'}\' else  self.entry->iterate(\r\n\t\t  temp1 : MNewObjectFeatureValue; s: String = \'\' | \r\n\t\t  s.concat(if temp1.feature.eName.oclIsUndefined() then \'\' else temp1.feature.eName endif).concat(\'=\').concat(if temp1.value.asCode.oclIsUndefined() then \'\' else temp1.value.asCode endif).concat(if entry->at(entry->indexOf(temp1)+1).oclIsUndefined() then \'\' else \',\' endif )).concat(\'}\') endif)\r\n' kindLabelDerive='\'New Object\'\n'"
 * @generated
 */

public interface MNewObjecct extends MAbstractNamedTuple {
	/**
	 * Returns the value of the '<em><b>New Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>New Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Type</em>' reference.
	 * @see #isSetNewType()
	 * @see #unsetNewType()
	 * @see #setNewType(MClassifier)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNewObjecct_NewType()
	 * @model unsettable="true"
	 * @generated
	 */
	MClassifier getNewType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MNewObjecct#getNewType <em>New Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Type</em>' reference.
	 * @see #isSetNewType()
	 * @see #unsetNewType()
	 * @see #getNewType()
	 * @generated
	 */
	void setNewType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MNewObjecct#getNewType <em>New Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNewType()
	 * @see #getNewType()
	 * @see #setNewType(MClassifier)
	 * @generated
	 */
	void unsetNewType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MNewObjecct#getNewType <em>New Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>New Type</em>' reference is set.
	 * @see #unsetNewType()
	 * @see #getNewType()
	 * @see #setNewType(MClassifier)
	 * @generated
	 */
	boolean isSetNewType();

	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MNewObjectFeatureValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference list.
	 * @see #isSetEntry()
	 * @see #unsetEntry()
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMNewObjecct_Entry()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MNewObjectFeatureValue> getEntry();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MNewObjecct#getEntry <em>Entry</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEntry()
	 * @see #getEntry()
	 * @generated
	 */
	void unsetEntry();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MNewObjecct#getEntry <em>Entry</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Entry</em>' containment reference list is set.
	 * @see #unsetEntry()
	 * @see #getEntry()
	 * @generated
	 */
	boolean isSetEntry();

} // MNewObjecct
