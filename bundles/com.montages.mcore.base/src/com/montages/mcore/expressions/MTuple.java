/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MTuple#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMTuple()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL abstractEntryDerive='entry->asOrderedSet()\n' kindLabelDerive='\'Tuple\'\n' asBasicCodeDerive='let c:String= \'Tuple{TODOENTRIES)\' in\r\n\tif name.oclIsUndefined() or name=\'\' \r\nthen /* The expression shoudl be returned here, as this is the final result \052/\r\n c\r\nelse \'let \'.concat(eName).concat(\': \').concat(\'TYPE TODO\').concat(\' = \').concat(c).concat(\' in\')\r\nendif\r\n'"
 * @generated
 */

public interface MTuple extends MAbstractNamedTuple {
	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MTupleEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference list.
	 * @see #isSetEntry()
	 * @see #unsetEntry()
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMTuple_Entry()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MTupleEntry> getEntry();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MTuple#getEntry <em>Entry</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEntry()
	 * @see #getEntry()
	 * @generated
	 */
	void unsetEntry();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MTuple#getEntry <em>Entry</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Entry</em>' containment reference list is set.
	 * @see #unsetEntry()
	 * @see #getEntry()
	 * @generated
	 */
	boolean isSetEntry();

} // MTuple
