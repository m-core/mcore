/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.ecore.EObject;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.SimpleType;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getChainEntryType <em>Chain Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getChainAsCode <em>Chain As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement1 <em>Element1</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement1Correct <em>Element1 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement2EntryType <em>Element2 Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement2 <em>Element2</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement2Correct <em>Element2 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement3EntryType <em>Element3 Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement3 <em>Element3</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getElement3Correct <em>Element3 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getCastType <em>Cast Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getLastElement <em>Last Element</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getChainCalculatedType <em>Chain Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getChainCalculatedSimpleType <em>Chain Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getChainCalculatedSingular <em>Chain Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getProcessor <em>Processor</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractChain#getProcessorDefinition <em>Processor Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractChain extends EObject {
	/**
	 * Returns the value of the '<em><b>Chain Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain Entry Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain Entry Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_ChainEntryType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let c:MClassifier=null in c'"
	 * @generated
	 */
	MClassifier getChainEntryType();

	/**
	 * Returns the value of the '<em><b>Chain As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain As Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain As Code</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_ChainAsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='unsafeChainAsCode(1)'"
	 * @generated
	 */
	String getChainAsCode();

	/**
	 * Returns the value of the '<em><b>Element1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element1</em>' reference.
	 * @see #isSetElement1()
	 * @see #unsetElement1()
	 * @see #setElement1(MProperty)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element1()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='\r\nlet annotatedProp: mcore::MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\nif self.chainEntryType.oclIsUndefined() then \r\nOrderedSet{} else\r\nself.chainEntryType.allProperties()\r\nendif\r\n\r\n--'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MProperty getElement1();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement1 <em>Element1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element1</em>' reference.
	 * @see #isSetElement1()
	 * @see #unsetElement1()
	 * @see #getElement1()
	 * @generated
	 */
	void setElement1(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement1 <em>Element1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElement1()
	 * @see #getElement1()
	 * @see #setElement1(MProperty)
	 * @generated
	 */
	void unsetElement1();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement1 <em>Element1</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Element1</em>' reference is set.
	 * @see #unsetElement1()
	 * @see #getElement1()
	 * @see #setElement1(MProperty)
	 * @generated
	 */
	boolean isSetElement1();

	/**
	 * Returns the value of the '<em><b>Element1 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element1 Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element1 Correct</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element1Correct()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if element1.oclIsUndefined() then true else\r\n--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else\r\nif chainEntryType.oclIsUndefined() then false \r\n  else chainEntryType.allProperties()->includes(element1)\r\nendif endif \r\n--endif'"
	 * @generated
	 */
	Boolean getElement1Correct();

	/**
	 * Returns the value of the '<em><b>Element2 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element2 Entry Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element2 Entry Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element2EntryType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.element1Correct then null\r\nelse \r\nif self.element1.oclIsUndefined() then null \r\nelse self.element1.type endif endif'"
	 * @generated
	 */
	MClassifier getElement2EntryType();

	/**
	 * Returns the value of the '<em><b>Element2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element2</em>' reference.
	 * @see #isSetElement2()
	 * @see #unsetElement2()
	 * @see #setElement2(MProperty)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element2()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='let annotatedProp: MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\n\r\nif element1.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if element1.isOperation\r\n    then OrderedSet{} \r\n    else if element2EntryType.oclIsUndefined() \r\n\t  then OrderedSet{}\r\n  \t  else element2EntryType.allProperties() endif\r\n  \t endif\r\n  endif\r\n'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MProperty getElement2();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement2 <em>Element2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element2</em>' reference.
	 * @see #isSetElement2()
	 * @see #unsetElement2()
	 * @see #getElement2()
	 * @generated
	 */
	void setElement2(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement2 <em>Element2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElement2()
	 * @see #getElement2()
	 * @see #setElement2(MProperty)
	 * @generated
	 */
	void unsetElement2();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement2 <em>Element2</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Element2</em>' reference is set.
	 * @see #unsetElement2()
	 * @see #getElement2()
	 * @see #setElement2(MProperty)
	 * @generated
	 */
	boolean isSetElement2();

	/**
	 * Returns the value of the '<em><b>Element2 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element2 Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element2 Correct</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element2Correct()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if element2.oclIsUndefined() then true else\r\n--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else\r\nif element2EntryType.oclIsUndefined() then false \r\n  else element2EntryType.allProperties()->includes(self.element2)\r\nendif endif \r\n--endif'"
	 * @generated
	 */
	Boolean getElement2Correct();

	/**
	 * Returns the value of the '<em><b>Element3 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element3 Entry Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element3 Entry Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element3EntryType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.element2Correct then null\r\nelse \r\nif self.element2.oclIsUndefined() then null\r\nelse self.element2.type endif endif'"
	 * @generated
	 */
	MClassifier getElement3EntryType();

	/**
	 * Returns the value of the '<em><b>Element3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element3</em>' reference.
	 * @see #isSetElement3()
	 * @see #unsetElement3()
	 * @see #setElement3(MProperty)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element3()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='let annotatedProp: mcore::MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\n\r\nif element2.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if element2.isOperation\r\n    then OrderedSet{} \r\n    else if element3EntryType.oclIsUndefined() \r\n\t  then OrderedSet{}\r\n  \t  else element3EntryType.allProperties() endif\r\n  \t endif\r\n  endif\r\n'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	MProperty getElement3();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement3 <em>Element3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element3</em>' reference.
	 * @see #isSetElement3()
	 * @see #unsetElement3()
	 * @see #getElement3()
	 * @generated
	 */
	void setElement3(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement3 <em>Element3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElement3()
	 * @see #getElement3()
	 * @see #setElement3(MProperty)
	 * @generated
	 */
	void unsetElement3();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getElement3 <em>Element3</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Element3</em>' reference is set.
	 * @see #unsetElement3()
	 * @see #getElement3()
	 * @see #setElement3(MProperty)
	 * @generated
	 */
	boolean isSetElement3();

	/**
	 * Returns the value of the '<em><b>Element3 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element3 Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element3 Correct</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Element3Correct()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if element3.oclIsUndefined() then true else\r\n--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else\r\nif element3EntryType.oclIsUndefined() then false\r\n  else element3EntryType.allProperties()->includes(self.element3)\r\nendif endif \r\n--endif'"
	 * @generated
	 */
	Boolean getElement3Correct();

	/**
	 * Returns the value of the '<em><b>Cast Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cast Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cast Type</em>' reference.
	 * @see #isSetCastType()
	 * @see #unsetCastType()
	 * @see #setCastType(MClassifier)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_CastType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.kind = mcore::ClassifierKind::ClassType '"
	 * @generated
	 */
	MClassifier getCastType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getCastType <em>Cast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cast Type</em>' reference.
	 * @see #isSetCastType()
	 * @see #unsetCastType()
	 * @see #getCastType()
	 * @generated
	 */
	void setCastType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getCastType <em>Cast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCastType()
	 * @see #getCastType()
	 * @see #setCastType(MClassifier)
	 * @generated
	 */
	void unsetCastType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getCastType <em>Cast Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cast Type</em>' reference is set.
	 * @see #unsetCastType()
	 * @see #getCastType()
	 * @see #setCastType(MClassifier)
	 * @generated
	 */
	boolean isSetCastType();

	/**
	 * Returns the value of the '<em><b>Last Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Element</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_LastElement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.element3.oclIsUndefined() then self.element3 else\r\nif not self.element2.oclIsUndefined() then self.element2 else\r\nif not self.element1.oclIsUndefined() then self.element1 else\r\nnull endif endif endif '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true'"
	 * @generated
	 */
	MProperty getLastElement();

	/**
	 * Returns the value of the '<em><b>Processor</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.expressions.MProcessor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor</em>' attribute.
	 * @see com.montages.mcore.expressions.MProcessor
	 * @see #isSetProcessor()
	 * @see #unsetProcessor()
	 * @see #setProcessor(MProcessor)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_Processor()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='false'"
	 * @generated
	 */
	MProcessor getProcessor();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getProcessor <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processor</em>' attribute.
	 * @see com.montages.mcore.expressions.MProcessor
	 * @see #isSetProcessor()
	 * @see #unsetProcessor()
	 * @see #getProcessor()
	 * @generated
	 */
	void setProcessor(MProcessor value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getProcessor <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProcessor()
	 * @see #getProcessor()
	 * @see #setProcessor(MProcessor)
	 * @generated
	 */
	void unsetProcessor();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getProcessor <em>Processor</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Processor</em>' attribute is set.
	 * @see #unsetProcessor()
	 * @see #getProcessor()
	 * @see #setProcessor(MProcessor)
	 * @generated
	 */
	boolean isSetProcessor();

	/**
	 * Returns the value of the '<em><b>Processor Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor Definition</em>' reference.
	 * @see #setProcessorDefinition(MProcessorDefinition)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_ProcessorDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = processor = mcore::expressions::MProcessor::None in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen null\n  else createProcessorDefinition()\nendif\n' choiceConstruction='\nlet s:SimpleType = self.chainCalculatedSimpleType in\nlet t:MClassifier = self.chainCalculatedType in\nlet res:OrderedSet(mcore::expressions::MProcessorDefinition) =\nif s = mcore::SimpleType::Boolean\n    then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForBoolean()\n                 else self.procDefChoicesForBooleans() endif\nelse if s = mcore::SimpleType::Integer \n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForInteger()\n                 else self.procDefChoicesForIntegers() endif\nelse if   s = mcore::SimpleType::Double\n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForReal()\n                 else self.procDefChoicesForReals() endif\nelse if   s = mcore::SimpleType::String\n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForString()\n                 else self.procDefChoicesForStrings() endif\nelse if   s = mcore::SimpleType::Date\n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForDate()\n                 else self.procDefChoicesForDates() endif\nelse if s = mcore::SimpleType::None or \n          s = mcore::SimpleType::Annotation or \n          s = mcore::SimpleType::Attribute or \n          s = mcore::SimpleType::Class or \n          s = mcore::SimpleType::Classifier or \n          s = mcore::SimpleType::DataType or \n          s = mcore::SimpleType::Enumeration or \n          s = mcore::SimpleType::Feature or \n          s = mcore::SimpleType::KeyValue or \n          s = mcore::SimpleType::Literal or \n          s = mcore::SimpleType::NamedElement or \n          s = mcore::SimpleType::Object or \n          s = mcore::SimpleType::Operation or \n          s = mcore::SimpleType::Package or \n          s = mcore::SimpleType::Parameter or \n          s = mcore::SimpleType::Reference or \n          s = mcore::SimpleType::TypedElement \n     then if self.chainCalculatedSingular \n                 then self.procDefChoicesForObject()\n                 else self.procDefChoicesForObjects()\n                        --OrderedSet{Tuple{processor=MProcessor::Head},\n                        --                   Tuple{processor=MProcessor::Tail}} \n                        endif\n     else OrderedSet{} endif endif endif endif endif endif\nin res->prepend(null)\n                                           \n  '"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='false'"
	 * @generated
	 */
	MProcessorDefinition getProcessorDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractChain#getProcessorDefinition <em>Processor Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processor Definition</em>' reference.
	 * @see #getProcessorDefinition()
	 * @generated
	 */
	void setProcessorDefinition(MProcessorDefinition value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate processorDefinition$Update(MProcessorDefinition trg);

	/**
	 * Returns the value of the '<em><b>Chain Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain Calculated Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain Calculated Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_ChainCalculatedType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: mcore::MClassifier = null in nl\n'"
	 * @generated
	 */
	MClassifier getChainCalculatedType();

	/**
	 * Returns the value of the '<em><b>Chain Calculated Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain Calculated Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain Calculated Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_ChainCalculatedSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Chain Calculated SimpleType'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: mcore::SimpleType = null in nl\n'"
	 * @generated
	 */
	SimpleType getChainCalculatedSimpleType();

	/**
	 * Returns the value of the '<em><b>Chain Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain Calculated Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain Calculated Singular</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractChain_ChainCalculatedSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: Boolean = null in nl\n'"
	 * @generated
	 */
	Boolean getChainCalculatedSingular();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='if not element3.oclIsUndefined() then 3\r\nelse if not element2.oclIsUndefined() then 2\r\nelse if not element1.oclIsUndefined() then 1\r\nelse 0 endif endif endif'"
	 * @generated
	 */
	Integer length();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stepRequired="true"
	 *        stepAnnotation="http://www.montages.com/mCore/MCore mName='step'"
	 *        annotation="http://www.xocl.org/OCL body='let element: MProperty = if step=1 then element1\r\n\telse if step=2 then element2 \r\n\t\telse if step=3 then element3\r\n\t\t\telse null endif endif endif in\r\n\t\r\nif element.oclIsUndefined() then \'ERROR\' else element.eName endif'"
	 * @generated
	 */
	String unsafeElementAsCode(Integer step);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stepRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='if step=1 then\r\n\tif element1.oclIsUndefined() then \'MISSING ELEMENT 1\'\r\n\telse unsafeElementAsCode(1) endif\r\nelse if step=2 then\r\n\tif element2.oclIsUndefined() then \'MISSING ELEMENT 2\'\r\n\telse unsafeElementAsCode(2) endif\r\nelse if step=3 then\r\n\tif element3.oclIsUndefined() then \'MISSING ELEMENT 3\'\r\n\telse unsafeElementAsCode(3) endif\r\nelse \'ERROR\'\r\nendif endif endif'"
	 * @generated
	 */
	String unsafeChainStepAsCode(Integer step);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fromStepRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='unsafeChainAsCode(fromStep, length())'"
	 * @generated
	 */
	String unsafeChainAsCode(Integer fromStep);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fromStepRequired="true" toStepRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='let end: Integer = if (length() > toStep) then toStep else length() endif in\r\n\r\nif fromStep=1 then\r\n\tif end=3 then\r\n\t\tunsafeChainStepAsCode(1).concat(\'.\').concat(unsafeChainStepAsCode(2)).concat(\'.\').concat(unsafeChainStepAsCode(3)) \r\n\telse if end=2 then\r\n  \t\tunsafeChainStepAsCode(1).concat(\'.\').concat(unsafeChainStepAsCode(2))\r\n\telse if end=1 then unsafeChainStepAsCode(1) else \'\' endif\r\n\tendif endif\r\nelse if fromStep=2 then\r\n\tif end=3 then\r\n\t\tunsafeChainStepAsCode(2).concat(\'.\').concat(unsafeChainStepAsCode(3)) \r\n\telse if end=2 then unsafeChainStepAsCode(2) else \'\' endif\r\n\tendif\r\nelse if fromStep=3 then\r\n\tif end=3 then unsafeChainStepAsCode(3) else \'\' endif\r\n\telse \'ERROR\'\r\nendif endif endif\r\n'"
	 * @generated
	 */
	String unsafeChainAsCode(Integer fromStep, Integer toStep);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String asCodeForOthers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length1'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String codeForLength1();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length2'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String codeForLength2();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length3'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String codeForLength3();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='procAsCode'"
	 *        annotation="http://www.xocl.org/OCL body='if self.processor= mcore::expressions::MProcessor::IsNull then \'.oclIsUndefined()\' \r\nelse if self.processor = mcore::expressions::MProcessor::AllUpperCase then \'toUpperCase()\'\r\nelse if self.processor = mcore::expressions::MProcessor::IsInvalid then \'.oclIsInvalid()\'\r\nelse if self.processor = mcore::expressions::MProcessor::Container then \'eContainer()\'\r\nelse  self.processor.toString()\r\nendif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	String procAsCode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if processor = mcore::expressions::MProcessor::None then false \r\nelse\r\nprocessor=mcore::expressions::MProcessor::AsOrderedSet or\r\nprocessor=mcore::expressions::MProcessor::First or\r\nprocessor=mcore::expressions::MProcessor::IsEmpty or\r\nprocessor=mcore::expressions::MProcessor::Last or\r\nprocessor=mcore::expressions::MProcessor::NotEmpty or\r\nprocessor=mcore::expressions::MProcessor::Size or\r\nprocessor=mcore::expressions::MProcessor::Sum or\r\nprocessor=mcore::expressions::MProcessor::Head or\r\nprocessor=mcore::expressions::MProcessor::Tail or\r\nprocessor=mcore::expressions::MProcessor::And or\r\nprocessor=mcore::expressions::MProcessor::Or\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isProcessorSetOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='IsOwnXOCLOperator'"
	 *        annotation="http://www.xocl.org/OCL body='processor =mcore::expressions::MProcessor::CamelCaseLower or\r\nprocessor =mcore::expressions::MProcessor::CamelCaseToBusiness or\r\nprocessor =mcore::expressions::MProcessor::CamelCaseUpper '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isOwnXOCLOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.processor = mcore::expressions::MProcessor::None then null\r\nelse if\r\n self.processor = mcore::expressions::MProcessor::AsOrderedSet or\r\n processor = mcore::expressions::MProcessor::Head or\r\n processor= mcore::expressions::MProcessor::Tail\r\n\r\nthen \r\nfalse\r\nelse true\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean processorReturnsSingular();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='self.processor <> mcore::expressions::MProcessor::None'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean processorIsSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='Tuple{processor=processor}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	MProcessorDefinition createProcessorDefinition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Object' mName='Processor Definition Choices For Singular Object'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::Container},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForObject();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Objects' mName='Processor Definition Choices For Objects'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail},\nTuple{processor=mcore::expressions::MProcessor::Container}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Boolean' mName='Processor Definition Choices For Boolean'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsFalse},\nTuple{processor=mcore::expressions::MProcessor::IsTrue},\nTuple{processor=mcore::expressions::MProcessor::Not},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForBoolean();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Booleans' mName='Processor Definition Choices For Booleans'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::And},\nTuple{processor=mcore::expressions::MProcessor::Or},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForBooleans();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Integer' mName='Processor Definition Choices For Integer'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsZero},\nTuple{processor=mcore::expressions::MProcessor::IsOne},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForInteger();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Integers' mName='Processor Definition Choices For Integers'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Sum},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForIntegers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Real' mName='Processor Definition Choices For Real'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Round},\nTuple{processor=mcore::expressions::MProcessor::Floor},\nTuple{processor=mcore::expressions::MProcessor::IsZero},\nTuple{processor=mcore::expressions::MProcessor::IsOne},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForReal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Reals' mName='Processor Definition Choices For Reals'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Round},\nTuple{processor=mcore::expressions::MProcessor::Floor},\nTuple{processor=mcore::expressions::MProcessor::Sum},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForReals();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For String' mName='Processor Definition Choices For String'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Trim},\nTuple{processor=mcore::expressions::MProcessor::AllLowerCase},\nTuple{processor=mcore::expressions::MProcessor::AllUpperCase},\nTuple{processor=mcore::expressions::MProcessor::FirstUpperCase},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseLower},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},\nTuple{processor=mcore::expressions::MProcessor::ToBoolean},\nTuple{processor=mcore::expressions::MProcessor::ToInteger},\nTuple{processor=mcore::expressions::MProcessor::ToReal},\nTuple{processor=mcore::expressions::MProcessor::ToDate},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Strings' mName='Processor Definition Choices For Strings'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Trim},\nTuple{processor=mcore::expressions::MProcessor::AllLowerCase},\nTuple{processor=mcore::expressions::MProcessor::AllUpperCase},\nTuple{processor=mcore::expressions::MProcessor::FirstUpperCase},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseLower},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},\nTuple{processor=mcore::expressions::MProcessor::ToBoolean},\nTuple{processor=mcore::expressions::MProcessor::ToInteger},\nTuple{processor=mcore::expressions::MProcessor::ToReal},\nTuple{processor=mcore::expressions::MProcessor::ToDate},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForStrings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Date' mName='Processor Definition Choices For Date'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Year},\nTuple{processor=mcore::expressions::MProcessor::Month},\nTuple{processor=mcore::expressions::MProcessor::Day},\nTuple{processor=mcore::expressions::MProcessor::Hour},\nTuple{processor=mcore::expressions::MProcessor::Minute},\nTuple{processor=mcore::expressions::MProcessor::Second},\nTuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},\nTuple{processor=mcore::expressions::MProcessor::ToHhMm},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForDate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Dates' mName='Processor Definition Choices For Dates'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Year},\nTuple{processor=mcore::expressions::MProcessor::Month},\nTuple{processor=mcore::expressions::MProcessor::Day},\nTuple{processor=mcore::expressions::MProcessor::Hour},\nTuple{processor=mcore::expressions::MProcessor::Minute},\nTuple{processor=mcore::expressions::MProcessor::Second},\nTuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},\nTuple{processor=mcore::expressions::MProcessor::ToHhMm},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForDates();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Is CustomCode Processor'"
	 *        annotation="http://www.xocl.org/OCL body='if self.processorIsSet().oclIsUndefined() then null\r\nelse \r\nprocessor = mcore::expressions::MProcessor::Head or\r\nprocessor = mcore::expressions::MProcessor::Tail or\r\nprocessor = mcore::expressions::MProcessor::And or\r\nprocessor = mcore::expressions::MProcessor::Or  \r\nendif\r\n\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isCustomCodeProcessor();

} // MAbstractChain
