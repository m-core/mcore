/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MClassifier;
import com.montages.mcore.SimpleType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAccumulator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAccumulator#getAccDefinition <em>Acc Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAccumulator#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAccumulator#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAccumulator#getMandatory <em>Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAccumulator#getSingular <em>Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulator()
 * @model annotation="http://www.xocl.org/OCL label='eName'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Accumulator\'\n' calculatedOwnMandatoryDerive='if mandatory.oclIsUndefined() then false\r\nelse mandatory endif' calculatedOwnSingularDerive='if singular.oclIsUndefined() then true\r\nelse singular endif' calculatedOwnSimpleTypeDerive='simpleType' calculatedOwnTypeDerive='type'"
 * @generated
 */

public interface MAccumulator extends MCollectionVar {
	/**
	 * Returns the value of the '<em><b>Acc Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acc Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acc Definition</em>' containment reference.
	 * @see #isSetAccDefinition()
	 * @see #unsetAccDefinition()
	 * @see #setAccDefinition(MChainOrApplication)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulator_AccDefinition()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MChainOrApplication getAccDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getAccDefinition <em>Acc Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acc Definition</em>' containment reference.
	 * @see #isSetAccDefinition()
	 * @see #unsetAccDefinition()
	 * @see #getAccDefinition()
	 * @generated
	 */
	void setAccDefinition(MChainOrApplication value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getAccDefinition <em>Acc Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAccDefinition()
	 * @see #getAccDefinition()
	 * @see #setAccDefinition(MChainOrApplication)
	 * @generated
	 */
	void unsetAccDefinition();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAccumulator#getAccDefinition <em>Acc Definition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Acc Definition</em>' containment reference is set.
	 * @see #unsetAccDefinition()
	 * @see #getAccDefinition()
	 * @see #setAccDefinition(MChainOrApplication)
	 * @generated
	 */
	boolean isSetAccDefinition();

	/**
	 * Returns the value of the '<em><b>Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulator_SimpleType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='SimpleType::None'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	SimpleType getSimpleType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @generated
	 */
	void setSimpleType(SimpleType value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @generated
	 */
	void unsetSimpleType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAccumulator#getSimpleType <em>Simple Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Simple Type</em>' attribute is set.
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @generated
	 */
	boolean isSetSimpleType();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(MClassifier)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulator_Type()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	MClassifier getType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAccumulator#getType <em>Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' reference is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Mandatory</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory</em>' attribute.
	 * @see #isSetMandatory()
	 * @see #unsetMandatory()
	 * @see #setMandatory(Boolean)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulator_Mandatory()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	Boolean getMandatory();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getMandatory <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mandatory</em>' attribute.
	 * @see #isSetMandatory()
	 * @see #unsetMandatory()
	 * @see #getMandatory()
	 * @generated
	 */
	void setMandatory(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getMandatory <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMandatory()
	 * @see #getMandatory()
	 * @see #setMandatory(Boolean)
	 * @generated
	 */
	void unsetMandatory();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAccumulator#getMandatory <em>Mandatory</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mandatory</em>' attribute is set.
	 * @see #unsetMandatory()
	 * @see #getMandatory()
	 * @see #setMandatory(Boolean)
	 * @generated
	 */
	boolean isSetMandatory();

	/**
	 * Returns the value of the '<em><b>Singular</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Singular</em>' attribute.
	 * @see #isSetSingular()
	 * @see #unsetSingular()
	 * @see #setSingular(Boolean)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAccumulator_Singular()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	Boolean getSingular();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getSingular <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Singular</em>' attribute.
	 * @see #isSetSingular()
	 * @see #unsetSingular()
	 * @see #getSingular()
	 * @generated
	 */
	void setSingular(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAccumulator#getSingular <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSingular()
	 * @see #getSingular()
	 * @see #setSingular(Boolean)
	 * @generated
	 */
	void unsetSingular();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAccumulator#getSingular <em>Singular</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Singular</em>' attribute is set.
	 * @see #unsetSingular()
	 * @see #getSingular()
	 * @see #setSingular(Boolean)
	 * @generated
	 */
	boolean isSetSingular();

} // MAccumulator
