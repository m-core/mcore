/**
 */
package com.montages.mcore.expressions.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MVariable;
import com.montages.mcore.SimpleType;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractBaseDefinition;
import com.montages.mcore.expressions.MAbstractExpressionWithBase;
import com.montages.mcore.expressions.MAccumulatorBaseDefinition;
import com.montages.mcore.expressions.MIteratorBaseDefinition;
import com.montages.mcore.expressions.MLiteralConstantBaseDefinition;
import com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition;
import com.montages.mcore.expressions.MParameterBaseDefinition;
import com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition;
import com.montages.mcore.expressions.MVariableBaseDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Expression With Base</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseAsCode <em>Base As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBase <em>Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseDefinition <em>Base Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseVar <em>Base Var</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseExitType <em>Base Exit Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseExitSimpleType <em>Base Exit Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseExitMandatory <em>Base Exit Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionWithBaseImpl#getBaseExitSingular <em>Base Exit Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MAbstractExpressionWithBaseImpl extends MAbstractExpressionImpl
		implements MAbstractExpressionWithBase {
	/**
	 * The default value of the '{@link #getBaseAsCode() <em>Base As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_AS_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected static final ExpressionBase BASE_EDEFAULT = ExpressionBase.UNDEFINED;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected ExpressionBase base = BASE_EDEFAULT;

	/**
	 * This is true if the Base attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean baseESet;

	/**
	 * The cached value of the '{@link #getBaseVar() <em>Base Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseVar()
	 * @generated
	 * @ordered
	 */
	protected MVariable baseVar;

	/**
	 * This is true if the Base Var reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean baseVarESet;

	/**
	 * The default value of the '{@link #getBaseExitSimpleType() <em>Base Exit Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType BASE_EXIT_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getBaseExitMandatory() <em>Base Exit Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BASE_EXIT_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBaseExitSingular() <em>Base Exit Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BASE_EXIT_SINGULAR_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #baseDefinition$Update <em>Base Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #baseDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression baseDefinition$UpdateexpressionsMAbstractBaseDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForBuiltIn <em>As Code For Built In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForBuiltIn
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForBuiltInBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForConstants <em>As Code For Constants</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForConstants
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForConstantsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForVariables <em>As Code For Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForVariables
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForVariablesBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseAsCode <em>Base As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseDefinition <em>Base Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getBaseDefinition <em>Base Definition</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDefinition
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression baseDefinitionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitType <em>Base Exit Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitSimpleType <em>Base Exit Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitMandatory <em>Base Exit Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseExitSingular <em>Base Exit Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseExitSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression baseExitSingularDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractExpressionWithBaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBaseAsCode() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then ''
		else if baseDefinition.oclIsTypeOf(mcore::expressions::MNumberBaseDefinition)
		then  baseDefinition.oclAsType(mcore::expressions::MNumberBaseDefinition).calculateAsCode(self)
		else baseDefinition.calculatedAsCode
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE;

		if (baseAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionBase getBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(ExpressionBase newBase) {
		ExpressionBase oldBase = base;
		base = newBase == null ? BASE_EDEFAULT : newBase;
		boolean oldBaseESet = baseESet;
		baseESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE,
					oldBase, base, !oldBaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBase() {
		ExpressionBase oldBase = base;
		boolean oldBaseESet = baseESet;
		base = BASE_EDEFAULT;
		baseESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE,
					oldBase, BASE_EDEFAULT, oldBaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBase() {
		return baseESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinition getBaseDefinition() {
		MAbstractBaseDefinition baseDefinition = basicGetBaseDefinition();
		return baseDefinition != null && baseDefinition.eIsProxy()
				? (MAbstractBaseDefinition) eResolveProxy(
						(InternalEObject) baseDefinition)
				: baseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinition basicGetBaseDefinition() {
		/**
		 * @OCL let r: mcore::expressions::MAbstractBaseDefinition =
		
		if base=mcore::expressions::ExpressionBase::Undefined 
		then null
		else if base=mcore::expressions::ExpressionBase::SelfObject
		then scopeSelf->first() 
		else if base=mcore::expressions::ExpressionBase::EContainer
		then scopeContainerBase->first() 
		else if base=mcore::expressions::ExpressionBase::Target
		then scopeTrg->first() 
		else if base=mcore::expressions::ExpressionBase::ObjectObject
		then scopeObj->first() 
		else if base=mcore::expressions::ExpressionBase::Source
		then scopeSource->first() 
		else if base=mcore::expressions::ExpressionBase::ConstantValue 
		then scopeSimpleTypeConstants->select(c:mcore::expressions::MSimpleTypeConstantBaseDefinition | c.namedConstant = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::ConstantLiteralValue
		then scopeLiteralConstants->select(c:mcore::expressions::MLiteralConstantBaseDefinition | c.namedConstant = baseVar)->first() 
		else if base=mcore::expressions::ExpressionBase::ConstantObjectReference
		then scopeObjectReferenceConstants->select(c:mcore::expressions::MObjectReferenceConstantBaseDefinition | c.namedConstant = baseVar)->first()
		else if base=mcore::expressions::ExpressionBase::Variable
		then scopeVariables->select(c:mcore::expressions::MVariableBaseDefinition | c.namedExpression = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::Parameter
		then scopeParameters->select(c:mcore::expressions::MParameterBaseDefinition | c.parameter = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::Iterator
		then scopeIterator->select(c:mcore::expressions::MIteratorBaseDefinition | c.iterator = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::Accumulator
		then scopeAccumulator->select(c:mcore::expressions::MAccumulatorBaseDefinition | c.accumulator = baseVar)->first()  
		else if base=mcore::expressions::ExpressionBase::OneValue or base=mcore::expressions::ExpressionBase::ZeroValue  or base = mcore::expressions::ExpressionBase::MinusOneValue
		then scopeNumberBase->select(c:mcore::expressions::MNumberBaseDefinition|c.expressionBase = base)->first()
		else scopeBase->select(c:mcore::expressions::MBaseDefinition | c.expressionBase=base)->first() 
		endif endif endif endif endif endif endif endif endif endif endif endif endif endif
		in
		if r.oclIsUndefined() then null else r endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;

		if (baseDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractBaseDefinition result = (MAbstractBaseDefinition) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setBaseDefinition(MAbstractBaseDefinition newBaseDefinition) {
		/* NOTE: After you changed this you must also change basicGetBaseDefinition() in order to retrieve the selected base variable. */

		if (newBaseDefinition == null) {
			unsetBase();
		} else {
			setBase(newBaseDefinition.getCalculatedBase());
			if (newBaseDefinition instanceof MSimpleTypeConstantBaseDefinition) {
				MSimpleTypeConstantBaseDefinition stcbd = (MSimpleTypeConstantBaseDefinition) newBaseDefinition;
				setBaseVar(stcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MLiteralConstantBaseDefinition) {
				MLiteralConstantBaseDefinition lcbd = (MLiteralConstantBaseDefinition) newBaseDefinition;
				setBaseVar(lcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MObjectReferenceConstantBaseDefinition) {
				MObjectReferenceConstantBaseDefinition orcbd = (MObjectReferenceConstantBaseDefinition) newBaseDefinition;
				setBaseVar(orcbd.getNamedConstant());
			} else if (newBaseDefinition instanceof MVariableBaseDefinition) {
				MVariableBaseDefinition vbd = (MVariableBaseDefinition) newBaseDefinition;
				setBaseVar(vbd.getNamedExpression());
			} else if (newBaseDefinition instanceof MParameterBaseDefinition) {
				MParameterBaseDefinition pbd = (MParameterBaseDefinition) newBaseDefinition;
				setBaseVar(pbd.getParameter());
			} else if (newBaseDefinition instanceof MIteratorBaseDefinition) {
				MIteratorBaseDefinition ibd = (MIteratorBaseDefinition) newBaseDefinition;
				setBaseVar(ibd.getIterator());
			} else if (newBaseDefinition instanceof MAccumulatorBaseDefinition) {
				MAccumulatorBaseDefinition abd = (MAccumulatorBaseDefinition) newBaseDefinition;
				setBaseVar(abd.getAccumulator());
			}
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Base Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MAbstractBaseDefinition>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.getScope()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MAbstractBaseDefinition> evalBaseDefinitionChoiceConstruction(
			List<MAbstractBaseDefinition> choice) {
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		if (baseDefinitionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				baseDefinitionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"BaseDefinitionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(baseDefinitionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "BaseDefinitionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MAbstractBaseDefinition> result = new ArrayList<MAbstractBaseDefinition>(
					(Collection<MAbstractBaseDefinition>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariable getBaseVar() {
		if (baseVar != null && baseVar.eIsProxy()) {
			InternalEObject oldBaseVar = (InternalEObject) baseVar;
			baseVar = (MVariable) eResolveProxy(oldBaseVar);
			if (baseVar != oldBaseVar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR,
							oldBaseVar, baseVar));
			}
		}
		return baseVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariable basicGetBaseVar() {
		return baseVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseVar(MVariable newBaseVar) {
		MVariable oldBaseVar = baseVar;
		baseVar = newBaseVar;
		boolean oldBaseVarESet = baseVarESet;
		baseVarESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR,
					oldBaseVar, baseVar, !oldBaseVarESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBaseVar() {
		MVariable oldBaseVar = baseVar;
		boolean oldBaseVarESet = baseVarESet;
		baseVar = null;
		baseVarESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR,
					oldBaseVar, null, oldBaseVarESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBaseVar() {
		return baseVarESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getBaseExitType() {
		MClassifier baseExitType = basicGetBaseExitType();
		return baseExitType != null && baseExitType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) baseExitType)
				: baseExitType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetBaseExitType() {
		/**
		 * @OCL if baseDefinition.oclIsUndefined() 
		then null
		else 
		/* Trick b/c we need to reference the container here and baseDefinition is generated on the fly *\/
		if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::SelfObject
		then self.selfObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target
		then self.targetObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::ObjectObject
		then self.objectObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Source
		then self.srcObjectType
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::EContainer
		then  if self.selfObjectType.allClassesContainedIn()->size() = 1 then self.selfObjectType.allClassesContainedIn()->first() else null endif
		else baseDefinition.calculatedType
		endif endif endif endif
		endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE;

		if (baseExitTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getBaseExitSimpleType() {
		/**
		 * @OCL if baseDefinition.oclIsUndefined()  or not (self.baseExitType.oclIsUndefined())
		then mcore::SimpleType::None
		else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target
		then self.targetSimpleType
		else self.baseDefinition.calculatedSimpleType
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE;

		if (baseExitSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBaseExitMandatory() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if baseDefinition.oclIsUndefined()
		then null
		else baseDefinition.calculatedMandatory
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY;

		if (baseExitMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBaseExitSingular() {
		/**
		 * @OCL if (let e: Boolean = baseDefinition.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then true
		else if baseDefinition.oclIsUndefined()
		then null
		else baseDefinition.calculatedSingular
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR;

		if (baseExitSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				baseExitSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseExitSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate baseDefinition$Update(MAbstractBaseDefinition trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.expressions.MAbstractBaseDefinition triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE
						.getMAbstractExpressionWithBase_BaseDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForBuiltIn() {

		/**
		 * @OCL baseAsCode
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE
				.getEOperations().get(1);
		if (asCodeForBuiltInBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForBuiltInBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForBuiltInBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForConstants() {

		/**
		 * @OCL if baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition)
		then let b: mcore::expressions::MSimpleTypeConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MSimpleTypeConstantBaseDefinition) in
		b.namedConstant.eName
		else if baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)
		then let b: mcore::expressions::MLiteralConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MLiteralConstantBaseDefinition) in
		b.namedConstant.eName
		else null endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE
				.getEOperations().get(2);
		if (asCodeForConstantsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForConstantsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForConstantsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForVariables() {

		/**
		 * @OCL let v: mcore::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mcore::expressions::MVariableBaseDefinition) in
		let vName: String = v.namedExpression.eName in
		baseAsCode
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE
				.getEOperations().get(3);
		if (asCodeForVariablesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForVariablesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForVariablesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE:
			return getBaseAsCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			return getBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			if (resolve)
				return getBaseDefinition();
			return basicGetBaseDefinition();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR:
			if (resolve)
				return getBaseVar();
			return basicGetBaseVar();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE:
			if (resolve)
				return getBaseExitType();
			return basicGetBaseExitType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE:
			return getBaseExitSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY:
			return getBaseExitMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR:
			return getBaseExitSingular();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			setBase((ExpressionBase) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			setBaseDefinition((MAbstractBaseDefinition) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR:
			setBaseVar((MVariable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			unsetBase();
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			setBaseDefinition((MAbstractBaseDefinition) null);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR:
			unsetBaseVar();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE:
			return BASE_AS_CODE_EDEFAULT == null ? getBaseAsCode() != null
					: !BASE_AS_CODE_EDEFAULT.equals(getBaseAsCode());
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
			return isSetBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION:
			return basicGetBaseDefinition() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR:
			return isSetBaseVar();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE:
			return basicGetBaseExitType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE:
			return getBaseExitSimpleType() != BASE_EXIT_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY:
			return BASE_EXIT_MANDATORY_EDEFAULT == null
					? getBaseExitMandatory() != null
					: !BASE_EXIT_MANDATORY_EDEFAULT
							.equals(getBaseExitMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR:
			return BASE_EXIT_SINGULAR_EDEFAULT == null
					? getBaseExitSingular() != null
					: !BASE_EXIT_SINGULAR_EDEFAULT
							.equals(getBaseExitSingular());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION:
			return baseDefinition$Update(
					(MAbstractBaseDefinition) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_BUILT_IN:
			return asCodeForBuiltIn();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_CONSTANTS:
			return asCodeForConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_VARIABLES:
			return asCodeForVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (base: ");
		if (baseESet)
			result.append(base);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}
} //MAbstractExpressionWithBaseImpl
