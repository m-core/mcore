/**
 */
package com.montages.mcore.expressions.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAccumulator;
import com.montages.mcore.expressions.MChainOrApplication;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAccumulator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MAccumulatorImpl#getAccDefinition <em>Acc Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAccumulatorImpl#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAccumulatorImpl#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAccumulatorImpl#getMandatory <em>Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAccumulatorImpl#getSingular <em>Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MAccumulatorImpl extends MCollectionVarImpl
		implements MAccumulator, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getAccDefinition() <em>Acc Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccDefinition()
	 * @generated
	 * @ordered
	 */
	protected MChainOrApplication accDefinition;

	/**
	 * This is true if the Acc Definition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean accDefinitionESet;

	/**
	 * The default value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The cached value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected SimpleType simpleType = SIMPLE_TYPE_EDEFAULT;

	/**
	 * This is true if the Simple Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean simpleTypeESet;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier type;

	/**
	 * This is true if the Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The default value of the '{@link #getMandatory() <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean MANDATORY_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getMandatory() <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatory()
	 * @generated
	 * @ordered
	 */
	protected Boolean mandatory = MANDATORY_EDEFAULT;

	/**
	 * This is true if the Mandatory attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mandatoryESet;

	/**
	 * The default value of the '{@link #getSingular() <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SINGULAR_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getSingular() <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingular()
	 * @generated
	 * @ordered
	 */
	protected Boolean singular = SINGULAR_EDEFAULT;

	/**
	 * This is true if the Singular attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean singularESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnType <em>Calculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAccumulatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MACCUMULATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication getAccDefinition() {
		if (accDefinition != null && accDefinition.eIsProxy()) {
			InternalEObject oldAccDefinition = (InternalEObject) accDefinition;
			accDefinition = (MChainOrApplication) eResolveProxy(
					oldAccDefinition);
			if (accDefinition != oldAccDefinition) {
				InternalEObject newAccDefinition = (InternalEObject) accDefinition;
				NotificationChain msgs = oldAccDefinition.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
						null, null);
				if (newAccDefinition.eInternalContainer() == null) {
					msgs = newAccDefinition.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
							oldAccDefinition, accDefinition));
			}
		}
		return accDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication basicGetAccDefinition() {
		return accDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccDefinition(
			MChainOrApplication newAccDefinition, NotificationChain msgs) {
		MChainOrApplication oldAccDefinition = accDefinition;
		accDefinition = newAccDefinition;
		boolean oldAccDefinitionESet = accDefinitionESet;
		accDefinitionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
					oldAccDefinition, newAccDefinition, !oldAccDefinitionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccDefinition(MChainOrApplication newAccDefinition) {
		if (newAccDefinition != accDefinition) {
			NotificationChain msgs = null;
			if (accDefinition != null)
				msgs = ((InternalEObject) accDefinition).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
						null, msgs);
			if (newAccDefinition != null)
				msgs = ((InternalEObject) newAccDefinition).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
						null, msgs);
			msgs = basicSetAccDefinition(newAccDefinition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAccDefinitionESet = accDefinitionESet;
			accDefinitionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
						newAccDefinition, newAccDefinition,
						!oldAccDefinitionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAccDefinition(NotificationChain msgs) {
		MChainOrApplication oldAccDefinition = accDefinition;
		accDefinition = null;
		boolean oldAccDefinitionESet = accDefinitionESet;
		accDefinitionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
					oldAccDefinition, null, oldAccDefinitionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAccDefinition() {
		if (accDefinition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) accDefinition).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION,
					null, msgs);
			msgs = basicUnsetAccDefinition(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAccDefinitionESet = accDefinitionESet;
			accDefinitionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION, null,
						null, oldAccDefinitionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAccDefinition() {
		return accDefinitionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSimpleType() {
		return simpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimpleType(SimpleType newSimpleType) {
		SimpleType oldSimpleType = simpleType;
		simpleType = newSimpleType == null ? SIMPLE_TYPE_EDEFAULT
				: newSimpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE, oldSimpleType,
					simpleType, !oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSimpleType() {
		SimpleType oldSimpleType = simpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleType = SIMPLE_TYPE_EDEFAULT;
		simpleTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE, oldSimpleType,
					SIMPLE_TYPE_EDEFAULT, oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSimpleType() {
		return simpleTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject) type;
			type = (MClassifier) eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MACCUMULATOR__TYPE, oldType,
							type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MClassifier newType) {
		MClassifier oldType = type;
		type = newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MACCUMULATOR__TYPE, oldType, type,
					!oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetType() {
		MClassifier oldType = type;
		boolean oldTypeESet = typeESet;
		type = null;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MACCUMULATOR__TYPE, oldType, null,
					oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMandatory(Boolean newMandatory) {
		Boolean oldMandatory = mandatory;
		mandatory = newMandatory;
		boolean oldMandatoryESet = mandatoryESet;
		mandatoryESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MACCUMULATOR__MANDATORY, oldMandatory,
					mandatory, !oldMandatoryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMandatory() {
		Boolean oldMandatory = mandatory;
		boolean oldMandatoryESet = mandatoryESet;
		mandatory = MANDATORY_EDEFAULT;
		mandatoryESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MACCUMULATOR__MANDATORY, oldMandatory,
					MANDATORY_EDEFAULT, oldMandatoryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMandatory() {
		return mandatoryESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSingular() {
		return singular;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingular(Boolean newSingular) {
		Boolean oldSingular = singular;
		singular = newSingular;
		boolean oldSingularESet = singularESet;
		singularESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MACCUMULATOR__SINGULAR, oldSingular,
					singular, !oldSingularESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSingular() {
		Boolean oldSingular = singular;
		boolean oldSingularESet = singularESet;
		singular = SINGULAR_EDEFAULT;
		singularESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MACCUMULATOR__SINGULAR, oldSingular,
					SINGULAR_EDEFAULT, oldSingularESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSingular() {
		return singularESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION:
			return basicUnsetAccDefinition(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION:
			if (resolve)
				return getAccDefinition();
			return basicGetAccDefinition();
		case ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE:
			return getSimpleType();
		case ExpressionsPackage.MACCUMULATOR__TYPE:
			if (resolve)
				return getType();
			return basicGetType();
		case ExpressionsPackage.MACCUMULATOR__MANDATORY:
			return getMandatory();
		case ExpressionsPackage.MACCUMULATOR__SINGULAR:
			return getSingular();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION:
			setAccDefinition((MChainOrApplication) newValue);
			return;
		case ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE:
			setSimpleType((SimpleType) newValue);
			return;
		case ExpressionsPackage.MACCUMULATOR__TYPE:
			setType((MClassifier) newValue);
			return;
		case ExpressionsPackage.MACCUMULATOR__MANDATORY:
			setMandatory((Boolean) newValue);
			return;
		case ExpressionsPackage.MACCUMULATOR__SINGULAR:
			setSingular((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION:
			unsetAccDefinition();
			return;
		case ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE:
			unsetSimpleType();
			return;
		case ExpressionsPackage.MACCUMULATOR__TYPE:
			unsetType();
			return;
		case ExpressionsPackage.MACCUMULATOR__MANDATORY:
			unsetMandatory();
			return;
		case ExpressionsPackage.MACCUMULATOR__SINGULAR:
			unsetSingular();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION:
			return isSetAccDefinition();
		case ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE:
			return isSetSimpleType();
		case ExpressionsPackage.MACCUMULATOR__TYPE:
			return isSetType();
		case ExpressionsPackage.MACCUMULATOR__MANDATORY:
			return isSetMandatory();
		case ExpressionsPackage.MACCUMULATOR__SINGULAR:
			return isSetSingular();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (simpleType: ");
		if (simpleTypeESet)
			result.append(simpleType);
		else
			result.append("<unset>");
		result.append(", mandatory: ");
		if (mandatoryESet)
			result.append(mandatory);
		else
			result.append("<unset>");
		result.append(", singular: ");
		if (singularESet)
			result.append(singular);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL eName
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MACCUMULATOR;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Accumulator'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory if mandatory.oclIsUndefined() then false
	else mandatory endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular if singular.oclIsUndefined() then true
	else singular endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSimpleType simpleType
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;

		if (calculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnType type
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MACCUMULATOR);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;

		if (calculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MACCUMULATOR__SIMPLE_TYPE,
				ExpressionsPackage.Literals.MACCUMULATOR__MANDATORY,
				ExpressionsPackage.Literals.MACCUMULATOR__SINGULAR };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MACCUMULATOR,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, oclText,
						helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MAccumulatorImpl
