/**
 */
package com.montages.mcore.expressions.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MObjectReferenceLet;
import com.montages.mcore.objects.MObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MObject Reference Let</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MObjectReferenceLetImpl#getObjectType <em>Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MObjectReferenceLetImpl#getConstant1 <em>Constant1</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MObjectReferenceLetImpl#getConstant2 <em>Constant2</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MObjectReferenceLetImpl#getConstant3 <em>Constant3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MObjectReferenceLetImpl extends MConstantLetImpl
		implements MObjectReferenceLet {
	/**
	 * The cached value of the '{@link #getObjectType() <em>Object Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier objectType;

	/**
	 * This is true if the Object Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean objectTypeESet;

	/**
	 * The cached value of the '{@link #getConstant1() <em>Constant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant1()
	 * @generated
	 * @ordered
	 */
	protected MObject constant1;

	/**
	 * This is true if the Constant1 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean constant1ESet;

	/**
	 * The cached value of the '{@link #getConstant2() <em>Constant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant2()
	 * @generated
	 * @ordered
	 */
	protected MObject constant2;

	/**
	 * This is true if the Constant2 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean constant2ESet;

	/**
	 * The cached value of the '{@link #getConstant3() <em>Constant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant3()
	 * @generated
	 * @ordered
	 */
	protected MObject constant3;

	/**
	 * This is true if the Constant3 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean constant3ESet;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getConstant1 <em>Constant1</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant1
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression constant1ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getConstant2 <em>Constant2</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant2
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression constant2ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getConstant3 <em>Constant3</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant3
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression constant3ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnType <em>Calculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectReferenceLetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getObjectType() {
		if (objectType != null && objectType.eIsProxy()) {
			InternalEObject oldObjectType = (InternalEObject) objectType;
			objectType = (MClassifier) eResolveProxy(oldObjectType);
			if (objectType != oldObjectType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE,
							oldObjectType, objectType));
			}
		}
		return objectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetObjectType() {
		return objectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectType(MClassifier newObjectType) {
		MClassifier oldObjectType = objectType;
		objectType = newObjectType;
		boolean oldObjectTypeESet = objectTypeESet;
		objectTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE,
					oldObjectType, objectType, !oldObjectTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObjectType() {
		MClassifier oldObjectType = objectType;
		boolean oldObjectTypeESet = objectTypeESet;
		objectType = null;
		objectTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE,
					oldObjectType, null, oldObjectTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObjectType() {
		return objectTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getConstant1() {
		if (constant1 != null && constant1.eIsProxy()) {
			InternalEObject oldConstant1 = (InternalEObject) constant1;
			constant1 = (MObject) eResolveProxy(oldConstant1);
			if (constant1 != oldConstant1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1,
							oldConstant1, constant1));
			}
		}
		return constant1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetConstant1() {
		return constant1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant1(MObject newConstant1) {
		MObject oldConstant1 = constant1;
		constant1 = newConstant1;
		boolean oldConstant1ESet = constant1ESet;
		constant1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1,
					oldConstant1, constant1, !oldConstant1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstant1() {
		MObject oldConstant1 = constant1;
		boolean oldConstant1ESet = constant1ESet;
		constant1 = null;
		constant1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1,
					oldConstant1, null, oldConstant1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstant1() {
		return constant1ESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Constant1</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if objectType.oclIsUndefined() then false
	else trg.type = objectType endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalConstant1ChoiceConstraint(MObject trg) {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (constant1ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__CONSTANT1;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				constant1ChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"Constant1ChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(constant1ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "Constant1ChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getConstant2() {
		if (constant2 != null && constant2.eIsProxy()) {
			InternalEObject oldConstant2 = (InternalEObject) constant2;
			constant2 = (MObject) eResolveProxy(oldConstant2);
			if (constant2 != oldConstant2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2,
							oldConstant2, constant2));
			}
		}
		return constant2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetConstant2() {
		return constant2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant2(MObject newConstant2) {
		MObject oldConstant2 = constant2;
		constant2 = newConstant2;
		boolean oldConstant2ESet = constant2ESet;
		constant2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2,
					oldConstant2, constant2, !oldConstant2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstant2() {
		MObject oldConstant2 = constant2;
		boolean oldConstant2ESet = constant2ESet;
		constant2 = null;
		constant2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2,
					oldConstant2, null, oldConstant2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstant2() {
		return constant2ESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Constant2</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if objectType.oclIsUndefined() then false
	else trg.type = objectType endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalConstant2ChoiceConstraint(MObject trg) {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (constant2ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__CONSTANT2;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				constant2ChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"Constant2ChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(constant2ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "Constant2ChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getConstant3() {
		if (constant3 != null && constant3.eIsProxy()) {
			InternalEObject oldConstant3 = (InternalEObject) constant3;
			constant3 = (MObject) eResolveProxy(oldConstant3);
			if (constant3 != oldConstant3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3,
							oldConstant3, constant3));
			}
		}
		return constant3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetConstant3() {
		return constant3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant3(MObject newConstant3) {
		MObject oldConstant3 = constant3;
		constant3 = newConstant3;
		boolean oldConstant3ESet = constant3ESet;
		constant3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3,
					oldConstant3, constant3, !oldConstant3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstant3() {
		MObject oldConstant3 = constant3;
		boolean oldConstant3ESet = constant3ESet;
		constant3 = null;
		constant3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3,
					oldConstant3, null, oldConstant3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstant3() {
		return constant3ESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Constant3</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if objectType.oclIsUndefined() then false
	else trg.type = objectType endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalConstant3ChoiceConstraint(MObject trg) {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (constant3ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__CONSTANT3;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				constant3ChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"Constant3ChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(constant3ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "Constant3ChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE:
			if (resolve)
				return getObjectType();
			return basicGetObjectType();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1:
			if (resolve)
				return getConstant1();
			return basicGetConstant1();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2:
			if (resolve)
				return getConstant2();
			return basicGetConstant2();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3:
			if (resolve)
				return getConstant3();
			return basicGetConstant3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE:
			setObjectType((MClassifier) newValue);
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1:
			setConstant1((MObject) newValue);
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2:
			setConstant2((MObject) newValue);
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3:
			setConstant3((MObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE:
			unsetObjectType();
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1:
			unsetConstant1();
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2:
			unsetConstant2();
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3:
			unsetConstant3();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__OBJECT_TYPE:
			return isSetObjectType();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT1:
			return isSetConstant1();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT2:
			return isSetConstant2();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__CONSTANT3:
			return isSetConstant3();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL asCode
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Object Reference'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode let t: String =  if objectType.oclIsUndefined() then 'MISSING TYPE' else
	objectType.calculatedShortName endif in
	let a: String = if constant1.oclIsUndefined() then '' else '<obj '.concat(constant1.id).concat('>') endif in
	let b: String = if constant2.oclIsUndefined() then '' else '<obj '.concat(constant2.id).concat('>') endif in
	let c: String = if constant3.oclIsUndefined() then '' else '<obj '.concat(constant3.id).concat('>')  endif in
	
	if calculatedSingular
	then a.concat(b).concat(c)
	else asSetString(
	'<obj '.concat(constant1.id).concat('>'),
	'<obj '.concat(constant2.id).concat('>'),
	'<obj '.concat(constant3.id).concat('>')
	)
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in
	let b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in
	let c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in
	
	(a+b+c) > 0
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in
	let b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in
	let c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in
	
	(a+b+c) <= 1
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnType objectType
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;

		if (calculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MObjectReferenceLetImpl
