/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An abstract "let" expression in OCL. Note that for these expression asCode returns the corresponding OCL variable name, that canthen be used to build more complex expressions.  Should you need to get the complete let...in statement, then use calculatedLetExpression.
 * <!-- end-model-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractLet()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractLet extends MVariable, MAbstractExpression {
} // MAbstractLet
