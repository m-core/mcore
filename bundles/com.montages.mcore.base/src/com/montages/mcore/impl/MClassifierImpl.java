/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MComponent;
import com.montages.mcore.MComponentAction;
import com.montages.mcore.MHasSimpleType;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.OrderingStrategy;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MLabel;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MNamedConstant;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MOperator;
import com.montages.mcore.expressions.MProcessor;
import com.montages.mcore.expressions.MSimpleTypeConstantLet;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MPropertyInstanceAction;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MClassifier</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getSimpleTypeString <em>Simple Type String</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getHasSimpleDataType <em>Has Simple Data Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getHasSimpleModelingType <em>Has Simple Modeling Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getAbstractClass <em>Abstract Class</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getEnforcedClass <em>Enforced Class</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getSuperTypePackage <em>Super Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getInstance <em>Instance</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getInternalEClassifier <em>Internal EClassifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getContainingPackage <em>Containing Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getRepresentsCoreType <em>Represents Core Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getAllPropertyGroups <em>All Property Groups</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getLiteral <em>Literal</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getEInterface <em>EInterface</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getOperationAsIdPropertyError <em>Operation As Id Property Error</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIsClassByStructure <em>Is Class By Structure</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getToggleSuperType <em>Toggle Super Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getOrderingStrategy <em>Ordering Strategy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getTestAllProperties <em>Test All Properties</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIdProperty <em>Id Property</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getResolveContainerAction <em>Resolve Container Action</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getAbstractDoAction <em>Abstract Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getObjectReferences <em>Object References</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getNearestInstance <em>Nearest Instance</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getStrictNearestInstance <em>Strict Nearest Instance</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getStrictInstance <em>Strict Instance</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIntelligentNearestInstance <em>Intelligent Nearest Instance</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getObjectUnreferenced <em>Object Unreferenced</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getOutstandingToCopyObjects <em>Outstanding To Copy Objects</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getPropertyInstances <em>Property Instances</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getObjectReferencePropertyInstances <em>Object Reference Property Instances</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIntelligentInstance <em>Intelligent Instance</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getContainmentHierarchy <em>Containment Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getStrictAllClassesContainedIn <em>Strict All Classes Contained In</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getStrictContainmentHierarchy <em>Strict Containment Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIntelligentAllClassesContainedIn <em>Intelligent All Classes Contained In</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIntelligentContainmentHierarchy <em>Intelligent Containment Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getInstantiationHierarchy <em>Instantiation Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getInstantiationPropertyHierarchy <em>Instantiation Property Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getIntelligentInstantiationPropertyHierarchy <em>Intelligent Instantiation Property Hierarchy</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getClassescontainedin <em>Classescontainedin</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getCanApplyDefaultContainment <em>Can Apply Default Containment</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getPropertiesAsText <em>Properties As Text</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getSemanticsAsText <em>Semantics As Text</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MClassifierImpl#getDerivedJsonSchema <em>Derived Json Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MClassifierImpl extends MPropertiesContainerImpl
		implements MClassifier, IXoclInitializable {
	/**
	 * The default value of the '{@link #getSimpleTypeString() <em>Simple Type String</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSimpleTypeString()
	 * @generated
	 * @ordered
	 */
	protected static final String SIMPLE_TYPE_STRING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getHasSimpleDataType() <em>Has Simple Data Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHasSimpleDataType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HAS_SIMPLE_DATA_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getHasSimpleModelingType() <em>Has Simple Modeling Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getHasSimpleModelingType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HAS_SIMPLE_MODELING_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSimpleTypeIsCorrect() <em>Simple Type Is Correct</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSimpleTypeIsCorrect()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SIMPLE_TYPE_IS_CORRECT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The cached value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected SimpleType simpleType = SIMPLE_TYPE_EDEFAULT;

	/**
	 * This is true if the Simple Type attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean simpleTypeESet;

	/**
	 * The default value of the '{@link #getAbstractClass() <em>Abstract Class</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAbstractClass()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ABSTRACT_CLASS_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getAbstractClass() <em>Abstract Class</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAbstractClass()
	 * @generated
	 * @ordered
	 */
	protected Boolean abstractClass = ABSTRACT_CLASS_EDEFAULT;

	/**
	 * This is true if the Abstract Class attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean abstractClassESet;

	/**
	 * The default value of the '{@link #getEnforcedClass() <em>Enforced Class</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEnforcedClass()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ENFORCED_CLASS_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getEnforcedClass() <em>Enforced Class</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEnforcedClass()
	 * @generated
	 * @ordered
	 */
	protected Boolean enforcedClass = ENFORCED_CLASS_EDEFAULT;

	/**
	 * This is true if the Enforced Class attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean enforcedClassESet;

	/**
	 * The cached value of the '{@link #getSuperTypePackage() <em>Super Type Package</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSuperTypePackage()
	 * @generated
	 * @ordered
	 */
	protected MPackage superTypePackage;

	/**
	 * This is true if the Super Type Package reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean superTypePackageESet;

	/**
	 * The cached value of the '{@link #getSuperType() <em>Super Type</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSuperType()
	 * @generated
	 * @ordered
	 */
	protected EList<MClassifier> superType;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final ClassifierKind KIND_EDEFAULT = ClassifierKind.CLASS_TYPE;

	/**
	 * The cached value of the '{@link #getInternalEClassifier() <em>Internal
	 * EClassifier</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getInternalEClassifier()
	 * @generated
	 * @ordered
	 */
	protected EClassifier internalEClassifier;

	/**
	 * This is true if the Internal EClassifier reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean internalEClassifierESet;

	/**
	 * The default value of the '{@link #getRepresentsCoreType() <em>Represents Core Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRepresentsCoreType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean REPRESENTS_CORE_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLiteral() <em>Literal</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLiteral()
	 * @generated
	 * @ordered
	 */
	protected EList<MLiteral> literal;

	/**
	 * The default value of the '{@link #getEInterface() <em>EInterface</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEInterface()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean EINTERFACE_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getEInterface() <em>EInterface</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEInterface()
	 * @generated
	 * @ordered
	 */
	protected Boolean eInterface = EINTERFACE_EDEFAULT;

	/**
	 * This is true if the EInterface attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean eInterfaceESet;

	/**
	 * The default value of the '{@link #getOperationAsIdPropertyError() <em>Operation As Id Property Error</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getOperationAsIdPropertyError()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean OPERATION_AS_ID_PROPERTY_ERROR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsClassByStructure() <em>Is Class By Structure</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIsClassByStructure()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_CLASS_BY_STRUCTURE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getOrderingStrategy() <em>Ordering Strategy</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOrderingStrategy()
	 * @generated
	 * @ordered
	 */
	protected static final OrderingStrategy ORDERING_STRATEGY_EDEFAULT = OrderingStrategy.NONE;

	/**
	 * The cached value of the '{@link #getIdProperty() <em>Id Property</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIdProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<MProperty> idProperty;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MClassifierAction DO_ACTION_EDEFAULT = MClassifierAction.DO;

	/**
	 * The default value of the '{@link #getResolveContainerAction() <em>Resolve Container Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getResolveContainerAction()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean RESOLVE_CONTAINER_ACTION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAbstractDoAction() <em>Abstract Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAbstractDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ABSTRACT_DO_ACTION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCanApplyDefaultContainment() <em>Can Apply Default Containment</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCanApplyDefaultContainment()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CAN_APPLY_DEFAULT_CONTAINMENT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getPropertiesAsText() <em>Properties As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPropertiesAsText()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTIES_AS_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPropertiesAsText() <em>Properties As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPropertiesAsText()
	 * @generated
	 * @ordered
	 */
	protected String propertiesAsText = PROPERTIES_AS_TEXT_EDEFAULT;

	/**
	 * This is true if the Properties As Text attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean propertiesAsTextESet;

	/**
	 * The default value of the '{@link #getSemanticsAsText() <em>Semantics As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSemanticsAsText()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMANTICS_AS_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSemanticsAsText() <em>Semantics As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSemanticsAsText()
	 * @generated
	 * @ordered
	 */
	protected String semanticsAsText = SEMANTICS_AS_TEXT_EDEFAULT;

	/**
	 * This is true if the Semantics As Text attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean semanticsAsTextESet;

	/**
	 * The default value of the '{@link #getDerivedJsonSchema() <em>Derived Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJsonSchema()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_JSON_SCHEMA_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #toggleSuperType$Update <em>Toggle Super Type$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #toggleSuperType$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression toggleSuperType$UpdatemcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #orderingStrategy$Update <em>Ordering Strategy$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #orderingStrategy$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression orderingStrategy$UpdatemcoreOrderingStrategyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMClassifierActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #defaultPropertyValue <em>Default Property Value</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #defaultPropertyValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultPropertyValueBodyOCL;

	/**
	 * The parsed OCL expression for the body of the
	 * '{@link #defaultPropertyDescription <em>Default Property
	 * Description</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #defaultPropertyDescription
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultPropertyDescriptionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allLiterals <em>All Literals</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allLiterals
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allLiteralsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #selectableClassifier <em>Selectable Classifier</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #selectableClassifier
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression selectableClassifiermcoreMClassifiermcoreMPackageBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allDirectSubTypes <em>All Direct Sub Types</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allDirectSubTypes
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allDirectSubTypesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allSubTypes <em>All Sub Types</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allSubTypes
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allSubTypesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #superTypesAsString <em>Super Types As String</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #superTypesAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression superTypesAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allSuperTypes <em>All Super Types</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allSuperTypes
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allSuperTypesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allProperties <em>All Properties</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allPropertiesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allSuperProperties <em>All Super Properties</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allSuperProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allSuperPropertiesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allFeatures <em>All Features</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allFeatures
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allFeaturesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allNonContainmentFeatures <em>All Non Containment Features</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allNonContainmentFeatures
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allNonContainmentFeaturesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allStoredNonContainmentFeatures <em>All Stored Non Containment Features</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allStoredNonContainmentFeatures
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allStoredNonContainmentFeaturesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allFeaturesWithStorage <em>All Features With Storage</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allFeaturesWithStorage
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allFeaturesWithStorageBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allContainmentReferences <em>All Containment References</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allContainmentReferences
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allContainmentReferencesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allOperationSignatures <em>All Operation Signatures</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allOperationSignatures
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allOperationSignaturesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allClassesContainedIn <em>All Classes Contained In</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allClassesContainedIn
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allClassesContainedInBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allIdProperties <em>All Id Properties</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #allIdProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allIdPropertiesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allSuperIdProperties <em>All Super Id Properties</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #allSuperIdProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allSuperIdPropertiesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemcoreMClassifierActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeSetDoAction <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #invokeSetDoAction
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeSetDoActionmcoreMClassifierActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeToggleSuperType <em>Invoke Toggle Super Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #invokeToggleSuperType
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeToggleSuperTypemcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doSuperTypeUpdate <em>Do Super Type Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doSuperTypeUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doSuperTypeUpdatemcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ambiguousClassifierName <em>Ambiguous Classifier Name</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #ambiguousClassifierName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ambiguousClassifierNameBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #simpleTypeAsString <em>Simple Type As String</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #simpleTypeAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression simpleTypeAsStringmcoreSimpleTypeBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getSimpleTypeString <em>Simple Type String</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSimpleTypeString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression simpleTypeStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getHasSimpleDataType <em>Has Simple Data Type</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHasSimpleDataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression hasSimpleDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getHasSimpleModelingType <em>Has Simple Modeling Type</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHasSimpleModelingType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression hasSimpleModelingTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of
	 * '{@link #getDoAction <em>Do Action</em>}' property. Is combined with the
	 * choice constraint definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getResolveContainerAction <em>Resolve Container Action</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResolveContainerAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression resolveContainerActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getAbstractDoAction <em>Abstract Do Action</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAbstractDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression abstractDoActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getObjectReferences <em>Object References</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getObjectReferences
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectReferencesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getNearestInstance <em>Nearest Instance</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNearestInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression nearestInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getStrictNearestInstance <em>Strict Nearest Instance</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStrictNearestInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression strictNearestInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getStrictInstance <em>Strict Instance</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getStrictInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression strictInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIntelligentNearestInstance <em>Intelligent Nearest Instance</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIntelligentNearestInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression intelligentNearestInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectUnreferenced <em>Object Unreferenced</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getObjectUnreferenced
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectUnreferencedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOutstandingToCopyObjects <em>Outstanding To Copy Objects</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOutstandingToCopyObjects
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression outstandingToCopyObjectsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getPropertyInstances <em>Property Instances</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPropertyInstances
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression propertyInstancesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectReferencePropertyInstances <em>Object Reference Property Instances</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getObjectReferencePropertyInstances
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectReferencePropertyInstancesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIntelligentInstance <em>Intelligent Instance</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIntelligentInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression intelligentInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainmentHierarchy <em>Containment Hierarchy</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainmentHierarchy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containmentHierarchyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getStrictAllClassesContainedIn <em>Strict All Classes Contained In</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStrictAllClassesContainedIn
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression strictAllClassesContainedInDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getStrictContainmentHierarchy <em>Strict Containment Hierarchy</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStrictContainmentHierarchy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression strictContainmentHierarchyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getIntelligentAllClassesContainedIn <em>Intelligent All Classes
	 * Contained In</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getIntelligentAllClassesContainedIn
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression intelligentAllClassesContainedInDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIntelligentContainmentHierarchy <em>Intelligent Containment Hierarchy</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIntelligentContainmentHierarchy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression intelligentContainmentHierarchyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getInstantiationHierarchy <em>Instantiation Hierarchy</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInstantiationHierarchy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression instantiationHierarchyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getInstantiationPropertyHierarchy <em>Instantiation Property Hierarchy</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInstantiationPropertyHierarchy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression instantiationPropertyHierarchyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIntelligentInstantiationPropertyHierarchy <em>Intelligent Instantiation Property Hierarchy</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntelligentInstantiationPropertyHierarchy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression intelligentInstantiationPropertyHierarchyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getClassescontainedin <em>Classescontainedin</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassescontainedin
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression classescontainedinDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getCanApplyDefaultContainment <em>Can Apply Default
	 * Containment</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCanApplyDefaultContainment
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression canApplyDefaultContainmentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedJsonSchema <em>Derived Json Schema</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJsonSchema
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedJsonSchemaDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '
	 * {@link #getSuperType <em>Super Type</em>}' property. Is combined with the
	 * choice construction definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getSuperType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression superTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getInstance
	 * <em>Instance</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression instanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKind <em>Kind</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression kindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getContainingPackage <em>Containing Package</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getContainingPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getRepresentsCoreType <em>Represents Core Type</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRepresentsCoreType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression representsCoreTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllPropertyGroups <em>All Property Groups</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllPropertyGroups
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allPropertyGroupsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOperationAsIdPropertyError <em>Operation As Id Property Error</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOperationAsIdPropertyError
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression operationAsIdPropertyErrorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsClassByStructure <em>Is Class By Structure</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIsClassByStructure
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isClassByStructureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getToggleSuperType <em>Toggle Super Type</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getToggleSuperType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression toggleSuperTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getToggleSuperType <em>Toggle Super Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToggleSuperType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression toggleSuperTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getOrderingStrategy <em>Ordering Strategy</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOrderingStrategy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression orderingStrategyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTestAllProperties <em>Test All Properties</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTestAllProperties
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression testAllPropertiesDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getIdProperty <em>Id Property</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIdProperty
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression idPropertyChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel
	 * <em>Full Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getFullLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel
	 * <em>Kind Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectName <em>Correct Name</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCorrectName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifier <em>Containing Classifier</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainingClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression containingClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSimpleTypeIsCorrect
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression simpleTypeIsCorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalStructuralName <em>Local Structural Name</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLocalStructuralName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression localStructuralNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getCalculatedName <em>Calculated Name</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCalculatedName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedNameDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MClassifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MCLASSIFIER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getSimpleTypeString() {
		/**
		 * @OCL self.simpleTypeAsString(self.simpleType)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING;

		if (simpleTypeStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				simpleTypeStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSimpleType() {
		return simpleType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimpleType(SimpleType newSimpleType) {
		SimpleType oldSimpleType = simpleType;
		simpleType = newSimpleType == null ? SIMPLE_TYPE_EDEFAULT
				: newSimpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__SIMPLE_TYPE, oldSimpleType,
					simpleType, !oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSimpleType() {
		SimpleType oldSimpleType = simpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleType = SIMPLE_TYPE_EDEFAULT;
		simpleTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__SIMPLE_TYPE, oldSimpleType,
					SIMPLE_TYPE_EDEFAULT, oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSimpleType() {
		return simpleTypeESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHasSimpleDataType() {
		/**
		 * @OCL  simpleType=SimpleType::String or
		simpleType=SimpleType::Boolean or
		simpleType=SimpleType::Date or
		simpleType=SimpleType::Double or
		simpleType=SimpleType::Integer or 
		simpleType= SimpleType::Any
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE;

		if (hasSimpleDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				hasSimpleDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(hasSimpleDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHasSimpleModelingType() {
		/**
		 * @OCL  not (simpleType=SimpleType::None or hasSimpleDataType)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE;

		if (hasSimpleModelingTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				hasSimpleModelingTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(hasSimpleModelingTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSimpleTypeIsCorrect() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT;

		if (simpleTypeIsCorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				simpleTypeIsCorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeIsCorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAction getDoAction() {
		/**
		 * @OCL mcore::MClassifierAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifierAction result = (MClassifierAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setDoAction(MClassifierAction newDoAction) {
		MProperty p = McoreFactory.eINSTANCE.createMProperty();
		MOperationSignature s = null;
		MParameter p1 = null;
		MParameter p2 = null;
		Integer count = 1;
		MClassifier previousNonAbstractNonRootClass = this;
		int iteratePos = this.getContainingPackage().getClassifier()
				.indexOf(this) - 1;
		while (iteratePos >= 0) {
			MClassifier abstractNonRootClass = this.getContainingPackage()
					.getClassifier().get(iteratePos);
			if (abstractNonRootClass.getKind() == ClassifierKind.CLASS_TYPE
					&& abstractNonRootClass.getAbstractClass() != true
					&& abstractNonRootClass != this.getContainingPackage()
							.getResourceRootClass()) {
				previousNonAbstractNonRootClass = abstractNonRootClass;
				break;
			} else {
				iteratePos -= 1;
			}
		}

		switch (newDoAction.getValue()) {
		case MClassifierAction.CREATE_INSTANCE_VALUE:
			// complexity is to define where.
			// simple, predictible behavior prefered
			// first version will just find default containment in root object,
			// whose type is matching or supertype.
			// without default containment, it gets more complex. DUPLICATE
			// LOGIC FROM MARIO CODE
			// TODO TODO TODO TODO
			MComponent comp = (MComponent) this.eResource().getContents()
					.get(0);
			if (comp.getResourceFolder().isEmpty()) {
				comp.setDoAction(MComponentAction.OBJECT);
			}
			MResourceFolder f1 = comp.getResourceFolder().get(0);
			if (f1.getFolder().isEmpty()) {
				comp.setDoAction(MComponentAction.OBJECT);
			}
			MResourceFolder f2 = f1.getFolder().get(0);
			if (f2.getResource().isEmpty()) {
				comp.setDoAction(MComponentAction.OBJECT);
			}
			MResource r = f2.getResource().get(0);
			if (r.getObject().isEmpty()) {
				comp.setDoAction(MComponentAction.OBJECT);
			}
			MObject root = r.getObject().get(0);
			root.setDoAction(MObjectAction.COMPLETE_SLOTS);
			MPropertyInstance slot = null;
			for (MPropertyInstance slotIterator : root.getPropertyInstance()) {
				if (slotIterator.getProperty() != null) {
					if (slotIterator.getProperty().getType() != null) {
						MClassifier iteratorType = slotIterator.getProperty()
								.getType();
						if (iteratorType == this
								|| iteratorType.allSubTypes().contains(this)) {
							slot = slotIterator;
							break;
						}
					}
				}
			}
			if (slot == null) {
				for (MPropertyInstance rootSlot : root.getPropertyInstance()) {
					for (MObject childOfRoot : rootSlot
							.getInternalContainedObject()) {
						childOfRoot.setDoAction(MObjectAction.COMPLETE_SLOTS);
						// find type from contained object or from property
						// if no property was here, find whether this new
						// instance would be supertype of contained object -->
						// go for it
						// if property is here, select as above
						for (MPropertyInstance slotOfChildOfRoot : childOfRoot
								.getPropertyInstance()) {
							if (slotOfChildOfRoot.getProperty() != null) {
								if (slotOfChildOfRoot.getProperty()
										.getType() != null) {
									MClassifier slotOfChildOfRootType = slotOfChildOfRoot
											.getProperty().getType();
									if (slotOfChildOfRootType == this
											|| slotOfChildOfRootType
													.allSubTypes()
													.contains(this)) {
										slot = slotOfChildOfRoot;
										break;
									}
								}
							} else {
								if (!slotOfChildOfRoot
										.getInternalContainedObject()
										.isEmpty()) {
									if (slotOfChildOfRoot
											.getInternalContainedObject().get(0)
											.getType() != null) {
										MClassifier slotOfChildOfRootType = slotOfChildOfRoot
												.getInternalContainedObject()
												.get(0).getType();
										if (slotOfChildOfRootType == this
												|| slotOfChildOfRootType
														.allSubTypes()
														.contains(this)
												|| slotOfChildOfRootType
														.allSuperTypes()
														.contains(this)) {
											slot = slotOfChildOfRoot;
											break;
										}
									}
								}
							}

						}
					}
				}
			}
			// TODO Do same for level 3!!
			// create slot if not found until now
			// The code bellow has been commented because it creates not
			// expected containment object
			// for the root classifier slot.
			/*
			 * if (slot == null) {
			 * root.setDoAction(MObjectAction.CONTAINMENT_SLOT); for
			 * (MPropertyInstance slotIterator : root .getPropertyInstance()) {
			 * if (slotIterator.getProperty() != null) { if
			 * (slotIterator.getProperty().getType() != null) { MClassifier
			 * iteratorType = slotIterator .getProperty().getType(); if
			 * (iteratorType == this || iteratorType
			 * .allSubTypes().contains(this)) { slot = slotIterator; break; } }
			 * } else { slot = slotIterator; } }
			 * slot.getInternalContainedObject().clear();
			 * 
			 * }
			 */
			if (slot != null) {
				slot.setDoAction(MPropertyInstanceAction.CONTAINED_OBJECT);
				MObject newObject = slot.getInternalContainedObject()
						.get(slot.getInternalContainedObject().size() - 1);
				newObject.setType(this);
				newObject.setDoAction(MObjectAction.CLEAN_SLOTS);
				newObject.setDoAction(MObjectAction.COMPLETE_SLOTS);
			}
			// create object in slog
			// set type of object (as slot could be of supertype
			// complete slots of new object
			break;
		case MClassifierAction.ABSTRACT_VALUE:
			// create new class at the same position as this class (moves this
			// class down)
			// make that class abstract, and call it "Abstract + name of current
			// class"
			// add new class as supertype of this class.
			// move all properties, propertygroups, and semantics of this class
			// to new class
			MClassifier newAbstraction = McoreFactory.eINSTANCE
					.createMClassifier();
			EList<MClassifier> localClasses = this.getContainingPackage()
					.getClassifier();
			localClasses.add(localClasses.indexOf(this), newAbstraction);
			newAbstraction.getProperty().clear();
			newAbstraction.setName(
					this.getCalculatedShortName().concat(" Abstraction"));
			newAbstraction.setAbstractClass(true);
			this.setToggleSuperType(newAbstraction);
			newAbstraction.getProperty().addAll(this.getProperty());
			newAbstraction.setAnnotations(this.getAnnotations());
			newAbstraction.getPropertyGroup().addAll(this.getPropertyGroup());
			break;
		case MClassifierAction.SPECIALIZE_VALUE:
			// create new class at the end of this package
			// name it this classes name + Subtype + nr of subtype (duplicate
			// code from +Slot logic of MObject)
			// add this class as super type to new class
			MClassifier newSpecialization = McoreFactory.eINSTANCE
					.createMClassifier();
			this.getContainingPackage().getClassifier().add(newSpecialization);

			// suppress "Abstract" or "Abstraction" in a subclass
			String shortName = this.getCalculatedShortName();
			if (shortName.contains("Abstraction")) {
				shortName = shortName.replaceAll("Abstraction", "");
			} else {
				if (shortName.contains("Abstract")) {
					shortName = shortName.replaceAll("Abstract", "");
				}
			}
			shortName = shortName.concat(" Subtype ")
					.concat(Integer.toString(this.allSubTypes().size() + 1));
			//To avoid two following blanks and a blank at the beginning
			shortName = shortName.replaceAll("  ", " ");
			if (shortName.startsWith(" ")) {
				shortName = shortName.replaceFirst(" ", "");
			}

			newSpecialization.setName(shortName);

			newSpecialization.setToggleSuperType(this);

			// Solve potential conflicts with property of super type named "Text
			// 1"
			for (MProperty mProperty : this.allFeatures()) {
				if ((mProperty.getCalculatedSimpleType() == SimpleType.STRING)
						&& mProperty.getContainingPropertiesContainer().eClass()
								.getClassifierID() == McorePackage.MCLASSIFIER) {
					count += 1;
				}
			}
			newSpecialization.getProperty().get(0)
					.setName("Text ".concat(count.toString()));

			// Add containment to root class if super type isn't contained in
			// any class
			if (this.getClassescontainedin().isEmpty()
					&& !this.getContainingPackage().getResourceRootClass()
							.equals(this)) {
				MProperty prop = McoreFactory.eINSTANCE.createMProperty();
				prop.setSimpleType(SimpleType.NONE);
				this.getContainingPackage().getResourceRootClass().getProperty()
						.get(0).getContainingPropertiesContainer().getProperty()
						.add(prop);
				prop.setPropertyBehavior(PropertyBehavior.CONTAINED);
				prop.setMultiplicityCase(MultiplicityCase.ZERO_MANY);
				prop.setType(this);
				this.getContainingPackage().getResourceRootClass().getProperty()
						.add(prop);
			}

			break;
		case MClassifierAction.INTO_ABSTRACT_CLASS_VALUE:
			this.setAbstractClass(true);
			break;
		case MClassifierAction.INTO_CONCRETE_CLASS_VALUE:
			this.setAbstractClass(false);
			break;
		case MClassifierAction.GROUP_OF_PROPERTIES_VALUE:
			MPropertiesGroup mGroup = McoreFactory.eINSTANCE
					.createMPropertiesGroup();
			mGroup.setName("Group "
					+ Integer.toString(this.getPropertyGroup().size() + 1));
			this.getPropertyGroup().add(mGroup);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			if (this.allProperties().isEmpty()) {
				this.setEnforcedClass(true);
			}
			break;
		case MClassifierAction.STRING_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.allFeatures()) {
				if ((mProperty.getCalculatedSimpleType() == SimpleType.STRING)
						&& mProperty.getContainingPropertiesContainer().eClass()
								.getClassifierID() == McorePackage.MCLASSIFIER) {
					count += 1;
				}
			}
			p.setName("Text ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_STRING_ATTRIBUTE);
			break;
		case MClassifierAction.LITERAL_ATTRIBUTE_VALUE:
			if (getContainingPackage().getClassifier().isEmpty())
				return;

			EList<MClassifier> classList = getContainingPackage()
					.getClassifier();

			for (int i = classList.size() - 1; i > 0; i--) {
				MClassifier lastEnumClass = classList.get(i);
				if (lastEnumClass.getKind() == ClassifierKind.ENUMERATION) {

					for (MProperty mProperty : this.getOwnedProperty()) {
						if (mProperty.getType() != null
								&& mProperty.getType() == lastEnumClass)
							count += 1;
					}
					p.setType(lastEnumClass);

					if (count > 1)
						p.setName(lastEnumClass.getName()
								+ "Literal ".concat(count.toString()));

					getProperty().add(p);
					p.setPropertyBehavior(PropertyBehavior.STORED);
					p.setSingular(true);
					break;
				}
			}

			break;

		case MClassifierAction.BOOLEAN_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.allFeatures()) {
				if ((mProperty.getCalculatedSimpleType() == SimpleType.BOOLEAN)
						&& mProperty.getContainingPropertiesContainer().eClass()
								.getClassifierID() == McorePackage.MCLASSIFIER) {
					count += 1;
				}
			}
			p.setName("Flag ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_BOOLEAN_FLAG);
			break;
		case MClassifierAction.INTEGER_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.allFeatures()) {
				if ((mProperty.getCalculatedSimpleType() == SimpleType.INTEGER)
						&& mProperty.getContainingPropertiesContainer().eClass()
								.getClassifierID() == McorePackage.MCLASSIFIER) {
					count += 1;
				}
			}
			p.setName("Number ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_INTEGER_ATTRIBUTE);
			break;
		case MClassifierAction.REAL_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.allFeatures()) {
				if ((mProperty.getCalculatedSimpleType() == SimpleType.DOUBLE)
						&& mProperty.getContainingPropertiesContainer().eClass()
								.getClassifierID() == McorePackage.MCLASSIFIER) {
					count += 1;
				}
			}
			p.setName("Factor ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_REAL_ATTRIBUTE);
			break;
		case MClassifierAction.DATE_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.allFeatures()) {
				if ((mProperty.getCalculatedSimpleType() == SimpleType.DATE)
						&& mProperty.getContainingPropertiesContainer().eClass()
								.getClassifierID() == McorePackage.MCLASSIFIER) {
					count += 1;
				}
			}
			p.setName("Date ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_DATE_ATTRIBUTE);
			break;
		// TODO: select previous class, if existing!
		case MClassifierAction.UNARY_REFERENCE_VALUE:
			getProperty().add(p);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(true);
			p.setPropertyBehavior(PropertyBehavior.STORED);

			break;
		case MClassifierAction.NARY_REFERENCE_VALUE:
			getProperty().add(p);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(false);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			break;
		case MClassifierAction.UNARY_CONTAINMENT_VALUE:
			getProperty().add(p);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			previousNonAbstractNonRootClass = this.getContainingPackage()
					.getClassifier()
					.get(this.getContainingPackage().getClassifier().size()
							- 1);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(true);
			p.setDoAction(MPropertyAction.INTO_CONTAINED);
			break;
		case MClassifierAction.NARY_CONTAINMENT_VALUE:
			getProperty().add(p);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			previousNonAbstractNonRootClass = this.getContainingPackage()
					.getClassifier()
					.get(this.getContainingPackage().getClassifier().size()
							- 1);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(false);
			p.setDoAction(MPropertyAction.INTO_CONTAINED);
			break;
		// TODO CONTAINMENT CASES. select previous class, if existing! CALL
		// "PROPERTY BEHAVIOR
		case MClassifierAction.OPERATION_ONE_PARAMETER_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty
						.getPropertyBehavior() == PropertyBehavior.IS_OPERATION) {
					count += 1;
				}
			}
			p.setName("Operation ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.IS_OPERATION);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_BOOLEAN_FLAG);
			s = McoreFactory.eINSTANCE.createMOperationSignature();
			p.getOperationSignature().add(s);
			p1 = McoreFactory.eINSTANCE.createMParameter();
			s.getParameter().add(p1);
			p1.setSimpleType(SimpleType.STRING);
			break;
		case MClassifierAction.OPERATION_TWO_PARAMETERS_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty
						.getPropertyBehavior() == PropertyBehavior.IS_OPERATION) {
					count += 1;
				}
			}
			p.setName("Operation ".concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.IS_OPERATION);
			this.getLiteral().clear();
			this.setSimpleType(SimpleType.NONE);
			this.setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_BOOLEAN_FLAG);
			s = McoreFactory.eINSTANCE.createMOperationSignature();
			p.getOperationSignature().add(s);
			p1 = McoreFactory.eINSTANCE.createMParameter();
			s.getParameter().add(p1);
			p1.setSimpleType(SimpleType.STRING);
			p2 = McoreFactory.eINSTANCE.createMParameter();
			s.getParameter().add(p2);
			p2.setSimpleType(SimpleType.STRING);
			break;
		case MClassifierAction.LITERAL_VALUE:
			MLiteral li = McoreFactory.eINSTANCE.createMLiteral();
			li.setName("Literal "
					+ Integer.toString(this.getLiteral().size() + 1));
			getLiteral().add(li);
			break;
		case MClassifierAction.INTO_ENUMERATION_VALUE:
			this.getProperty().clear();
			this.setAnnotations(null);
			this.getPropertyGroup().clear();
			this.setEnforcedClass(false);
			this.setSimpleType(SimpleType.NONE);
			MLiteral li2 = McoreFactory.eINSTANCE.createMLiteral();
			getLiteral().add(li2);
			break;
		case MClassifierAction.INTO_DATATYPE_VALUE:
			this.getProperty().clear();
			this.setAnnotations(null);
			this.getPropertyGroup().clear();
			this.setEnforcedClass(false);
			this.setSimpleType(SimpleType.STRING);
			break;

		case MClassifierAction.RESOLVE_INTO_CONTAINER_VALUE:
			ArrayList<MProperty> multContainer = new ArrayList<MProperty>();

			if (this.getClassescontainedin().size() == 1) {
				Boolean singular = false;
				MProperty container = McoreFactory.eINSTANCE.createMProperty();
				for (MProperty mprop : this.getClassescontainedin().get(0)
						.allProperties()) {
					if (mprop.getType() == this) {
						singular = mprop.getCalculatedSingular();
						container = mprop;
						break;
					}
				}
				if (singular) {
					List<MProperty> renamedProperties = new ArrayList<MProperty>();
					for (MProperty mprop : this.getProperty()) {
						renamedProperties.add(copyMProperty(mprop));
					}
					for (int i = 0; i < this.getProperty().size(); i++) {
						MProperty mprop = this.getProperty().get(i);
						if (mprop.getDirectSemantics() != null) {
							MClassifierAnnotations semantics = null;
							if (this.getClassescontainedin().get(0)
									.getAnnotations() == null) {
								semantics = AnnotationsFactory.eINSTANCE
										.createMClassifierAnnotations();
								this.getClassescontainedin().get(0)
										.setAnnotations(semantics);
							} else
								semantics = this.getClassescontainedin().get(0)
										.getAnnotations();
							MPropertyAnnotations anno = mprop
									.getDirectSemantics();
							anno.setProperty(renamedProperties.get(i));
							semantics.getPropertyAnnotations().add(anno);
						}
					}

					ArrayList<MObject> mObjects = new ArrayList<MObject>(
							this.getInstance());
					for (int i = 0; i < mObjects.size(); i++) {
						MObject mObj = mObjects.get(i);

						for (MPropertyInstance mPropIn : mObj
								.getPropertyInstance()) {
							mPropIn.setProperty(
									renamedProperties.get(this.getProperty()
											.indexOf(mPropIn.getProperty())));
						}

						mObj.getContainingObject().getPropertyInstance()
								.addAll(mObj.getPropertyInstance());
						mObj.getContainingObject().getPropertyInstance()
								.remove(mObj.getContainingPropertyInstance());
					}

					this.getClassescontainedin().get(0).getProperty()
							.addAll(renamedProperties);
					this.getClassescontainedin().get(0).getProperty()
							.remove(container);
				} else {
					multContainer.add(container);
				}
			} else {
				for (MClassifier mClass : this.getClassescontainedin()) {
					Boolean singular = false;
					MProperty container = McoreFactory.eINSTANCE
							.createMProperty();
					for (MProperty mprop : mClass.allProperties()) {
						if (mprop.getType() == this) {
							singular = mprop.getCalculatedSingular();
							container = mprop;
							break;
						}
					}
					if (singular) {
						List<MProperty> renamedProperties = new ArrayList<MProperty>();
						for (MProperty mprop : this.getProperty()) {
							renamedProperties.add(copyMProperty(mprop));
						}
						for (int i = 0; i < this.getProperty().size(); i++) {
							mClass.getProperty().add(renamedProperties.get(i));
						}

						ArrayList<MObject> mObjects = new ArrayList<MObject>(
								this.getInstance());
						for (int i = 0; i < mObjects.size(); i++) {
							MObject mObj = mObjects.get(i);

							if (mObj.getContainingObject().getType()
									.equals(mClass)) {
								for (MPropertyInstance mPropIn : mObj
										.getPropertyInstance()) {
									mPropIn.setProperty(renamedProperties
											.get(this.getProperty().indexOf(
													mPropIn.getProperty())));
								}

								mObj.getContainingObject().getPropertyInstance()
										.addAll(mObj.getPropertyInstance());
								mObj.getContainingObject().getPropertyInstance()
										.remove(mObj
												.getContainingPropertyInstance());
							}
						}

						mClass.getProperty().remove(container);
					} else {
						multContainer.add(container);
					}
				}
			}

			if (multContainer.isEmpty()) {
				this.getContainingPackage().getClassifier().remove(this);
			}

			break;

		case MClassifierAction.COMPLETE_SEMANTICS_VALUE:
			if (!this.allSuperProperties().isEmpty()) {
				//So we can check if the property of the super class already has a property annotation in the class
				ArrayList<MProperty> allPropsWithAnno = new ArrayList<MProperty>();
				for (MPropertyAnnotations propAnno : this
						.allPropertyAnnotations()) {
					allPropsWithAnno.add(propAnno.getProperty());
				}
				//So we can check all operation signatures with an annotation
				ArrayList<MOperationSignature> allOpsWithAnno = new ArrayList<MOperationSignature>();
				for (MOperationAnnotations opAnno : this
						.allOperationAnnotations()) {
					allOpsWithAnno.add(opAnno.getOperationSignature());
				}
				MClassifierAnnotations classAnno = this.getAnnotations();
				if (classAnno == null) {
					classAnno = AnnotationsFactory.eINSTANCE
							.createMClassifierAnnotations();
					this.setAnnotations(classAnno);
				}

				for (MProperty superClassProp : this.allSuperProperties()) {
					//is it a property or an operation
					if (superClassProp.getIsOperation()) {
						for (MOperationSignature opSig : superClassProp
								.getOperationSignature()) {
							//there is no operation annotation in the class
							if (!allOpsWithAnno.contains(opSig)) {
								MOperationAnnotations newOpAnno = AnnotationsFactory.eINSTANCE
										.createMOperationAnnotations();
								newOpAnno.setOperationSignature(opSig);
								MResult newResult = AnnotationsFactory.eINSTANCE
										.createMResult();
								newResult.setBase(ExpressionBase.NULL_VALUE);
								newOpAnno.setResult(newResult);
								classAnno.getOperationAnnotations()
										.add(newOpAnno);
							}
						}
					} else {
						//Property annotation doesn't exist 
						if (!allPropsWithAnno.contains(superClassProp)) {
							MPropertyAnnotations newPropAnno = AnnotationsFactory.eINSTANCE
									.createMPropertyAnnotations();
							newPropAnno.setProperty(superClassProp);

							switch (superClassProp.getPropertyBehavior()
									.getValue()) {
							case PropertyBehavior.STORED_VALUE:
								MInitializationValue newInit = AnnotationsFactory.eINSTANCE
										.createMInitializationValue();
								newInit.setBase(ExpressionBase.NULL_VALUE);
								newPropAnno.setInitValue(newInit);
								break;
							case PropertyBehavior.DERIVED_VALUE:
								MResult newResult = AnnotationsFactory.eINSTANCE
										.createMResult();
								newResult.setBase(ExpressionBase.NULL_VALUE);
								newPropAnno.setResult(newResult);
								break;
							default:
								break;
							}
							classAnno.getPropertyAnnotations().add(newPropAnno);
						}
					}
				}
			}
			break;

		default:
			MClassifierAnnotations semantics = getAnnotations();
			if (semantics == null) {
				semantics = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				setAnnotations(semantics);
			}
			switch (newDoAction.getValue()) {
			case MClassifierAction.LABEL_VALUE:
				if (annotations.getLabel() == null) {
					MLabel l = AnnotationsFactory.eINSTANCE.createMLabel();
					annotations.setLabel(l);
					l.setBase(null);
					MSimpleTypeConstantLet constLet = ExpressionsFactory.eINSTANCE
							.createMSimpleTypeConstantLet();
					MNamedConstant nc = ExpressionsFactory.eINSTANCE
							.createMNamedConstant();
					l.getNamedConstant().add(nc);
					nc.setName("Label Prefix");
					nc.setExpression(constLet);
					constLet.setSimpleType(SimpleType.STRING);
					constLet.setConstant1(this.getCalculatedName());
					MSimpleTypeConstantLet sep = ExpressionsFactory.eINSTANCE
							.createMSimpleTypeConstantLet();
					MNamedConstant nSep = ExpressionsFactory.eINSTANCE
							.createMNamedConstant();
					l.getNamedConstant().add(nSep);
					nSep.setName("Label Separator");
					nSep.setExpression(sep);
					constLet.setSimpleType(SimpleType.STRING);
					constLet.setConstant1(" ");
					MApplication a = ExpressionsFactory.eINSTANCE
							.createMApplication();
					MNamedExpression ne = ExpressionsFactory.eINSTANCE
							.createMNamedExpression();
					l.getNamedExpression().add(ne);
					ne.setExpression(a);
					a.setOperator(MOperator.CONCAT);
					MChain op1 = ExpressionsFactory.eINSTANCE.createMChain();
					op1.setBase(ExpressionBase.CONSTANT_VALUE);
					op1.setBaseVar(nc);
					a.getOperands().add(op1);
					MChain separatorOp = null;
					MChain attributeOp = null;
					for (Iterator iterator = allFeatures().iterator(); iterator
							.hasNext();) {
						MProperty prop = (MProperty) iterator.next();
						if (prop.getKind() == PropertyKind.ATTRIBUTE) {
							separatorOp = ExpressionsFactory.eINSTANCE
									.createMChain();
							separatorOp.setBase(ExpressionBase.CONSTANT_VALUE);
							separatorOp.setBaseVar(nSep);
							a.getOperands().add(separatorOp);
							attributeOp = ExpressionsFactory.eINSTANCE
									.createMChain();
							attributeOp.setBase(ExpressionBase.SELF_OBJECT);
							attributeOp.setElement1(prop);
							attributeOp.setProcessor(MProcessor.TO_STRING);
							a.getOperands().add(attributeOp);
						}
					}

				}
				break;
			case MClassifierAction.CONSTRAINT_VALUE:
				MInvariantConstraint newConstraint = AnnotationsFactory.eINSTANCE
						.createMInvariantConstraint();
				semantics.getConstraint().add(newConstraint);
				newConstraint.setBase(ExpressionBase.TRUE_VALUE);
				newConstraint.setName("Constraint ".concat(
						Integer.toString(semantics.getConstraint().size())));
				break;

			default:
				break;
			}
			break;
		}
		if (p.getPropertyBehavior() != PropertyBehavior.IS_OPERATION
				&& !getInstance().isEmpty())
			for (MObject obj : getInstance())
				obj.setDoAction(MObjectAction.COMPLETE_SLOTS);

	}

	/**
	 * @generated NOT
	 * @param original
	 * @return
	 */
	public MProperty copyMProperty(MProperty original) {
		MProperty copy = McoreFactory.eINSTANCE.createMProperty();

		if (original.isSetName()) {
			copy.setName(this.getName().concat(original.getName()));
		} else {
			copy.setName(this.getName());
		}

		copy.setChangeable(original.getChangeable());
		copy.setContainment(original.getContainment());
		copy.setDescription(original.getDescription());
		copy.setEDefaultValueLiteral(original.getEDefaultValueLiteral());
		copy.setHasStorage(original.getHasStorage());
		copy.setMandatory(original.getMandatory());
		copy.setMultiplicityCase(original.getMultiplicityCase());
		copy.setNotUnsettable(original.getNotUnsettable());
		copy.setOpposite(original.getOpposite());
		copy.setOrderWithinGroup(original.getOrderWithinGroup());
		copy.setPersisted(original.getPersisted());
		copy.setShortName(original.getShortName());
		copy.setSimpleType(original.getSimpleType());
		copy.setSingular(original.getSingular());
		copy.setSpecialEName(original.getSpecialEName());
		copy.setType(original.getType());
		copy.setTypePackage(original.getTypePackage());
		if (original.getUseCoreTypes() != null) {
			copy.setUseCoreTypes(original.getUseCoreTypes());
		}
		return copy;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MClassifierAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let classActions:OrderedSet(MClassifierAction) =
	if self.literal->isEmpty() 
	  then OrderedSet{MClassifierAction::CreateInstance, if abstractDoAction then MClassifierAction::Abstract else null endif, MClassifierAction::Specialize,
	  MClassifierAction::StringAttribute, MClassifierAction::BooleanAttribute, MClassifierAction::IntegerAttribute, if not self.containingPackage.classifier->select( not literal->isEmpty())->isEmpty() then MClassifierAction::LiteralAttribute else null endif, MClassifierAction::RealAttribute, MClassifierAction::DateAttribute, MClassifierAction::UnaryReference, MClassifierAction::NaryReference,MClassifierAction::UnaryContainment, MClassifierAction::NaryContainment,MClassifierAction::OperationOneParameter, MClassifierAction::OperationTwoParameters, MClassifierAction::CompleteSemantics} 
	  else OrderedSet{} endif in
	let withLabelAndConstraintAction:OrderedSet(MClassifierAction) =
	if self.literal->isEmpty() then
	   if self.annotations.oclIsUndefined() 
	      then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)
	    else if self.annotations.label.oclIsUndefined()
	      then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)
	      else  classActions->prepend(MClassifierAction::Constraint) endif endif 
	else classActions endif in
	  let intoAbstractOrConcrete: OrderedSet(MClassifierAction) =
	if self.literal->isEmpty() and (self.ownedProperty->notEmpty() or self.enforcedClass=true)
	then withLabelAndConstraintAction->prepend(MClassifierAction::GroupOfProperties)
	else  withLabelAndConstraintAction->append(MClassifierAction::Literal) endif in
	let resolveContainer:OrderedSet(MClassifierAction) =
	if self.literal->isEmpty()
	then if self.abstractClass = true
	 then intoAbstractOrConcrete->prepend(MClassifierAction::IntoConcreteClass)->prepend(MClassifierAction::Do) 
	 else intoAbstractOrConcrete->prepend(MClassifierAction::IntoAbstractClass)->prepend(MClassifierAction::Do) 
	endif
	else intoAbstractOrConcrete->prepend(MClassifierAction::Do)
	endif in
	 if self.resolveContainerAction = true
	then if resolveContainer->includes(MClassifierAction::Label)
		then resolveContainer->insertAt(8, MClassifierAction::ResolveIntoContainer)
		else resolveContainer->insertAt(7, MClassifierAction::ResolveIntoContainer)
		endif
	else resolveContainer
	endif
	
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MClassifierAction> evalDoActionChoiceConstruction(
			List<MClassifierAction> choice) {
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = McorePackage.Literals.MCLASSIFIER__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MClassifierAction> result = new ArrayList<MClassifierAction>(
					(Collection<MClassifierAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getResolveContainerAction() {
		/**
		 * @OCL let var1: OrderedSet(mcore::MProperty)  = let chain: OrderedSet(mcore::MProperty)  = classescontainedin.allFeaturesWithStorage()->asOrderedSet() in
		chain->iterate(i:mcore::MProperty; r: OrderedSet(mcore::MProperty)=OrderedSet{} | if i.oclIsKindOf(mcore::MProperty) then r->including(i.oclAsType(mcore::MProperty))->asOrderedSet() 
		else r endif)->select(it: mcore::MProperty | let e0: Boolean = (let e0: Boolean = it.containment = true in 
		if e0.oclIsInvalid() then null else e0 endif) and (let e0: Boolean = it.calculatedSingular = true in 
		if e0.oclIsInvalid() then null else e0 endif) and (let e0: Boolean = it.type = containingClassifier in 
		if e0.oclIsInvalid() then null else e0 endif) in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let resultofnull: Boolean = if (let e0: Boolean = let e0: Boolean = allFeaturesWithStorage()->asOrderedSet()->includesAll(allProperties()->asOrderedSet())   in 
		if e0.oclIsInvalid() then null else e0 endif and let chain01: OrderedSet(mcore::MClassifier)  = classescontainedin->asOrderedSet() in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif and let chain02: OrderedSet(mcore::MClassifier)  = allDirectSubTypes()->asOrderedSet() in
		if chain02->isEmpty().oclIsUndefined() 
		then null 
		else chain02->isEmpty()
		endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then let chain: OrderedSet(mcore::MProperty)  = var1 in
		if chain->notEmpty().oclIsUndefined() 
		then null 
		else chain->notEmpty()
		endif
		else false
		endif in
		resultofnull
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__RESOLVE_CONTAINER_ACTION;

		if (resolveContainerActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				resolveContainerActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(resolveContainerActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAbstractDoAction() {
		/**
		 * @OCL let var1: OrderedSet(mcore::MClassifier)  = allSuperTypes()->asOrderedSet()->select(it: mcore::MClassifier | let e0: Boolean = let chain01: Boolean = it.abstractClass in
		if chain01 = true then true else false 
		endif and (let e0: Boolean = it.name = (let e0: String = 'Abstract '.concat(name) in 
		if e0.oclIsInvalid() then null else e0 endif) in 
		if e0.oclIsInvalid() then null else e0 endif) in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let resultofAbstractDoAction: Boolean = if (abstractClass) 
		=true 
		then false
		else let chain: OrderedSet(mcore::MClassifier)  = var1 in
		if chain->isEmpty().oclIsUndefined() 
		then null 
		else chain->isEmpty()
		endif
		endif in
		resultofAbstractDoAction
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__ABSTRACT_DO_ACTION;

		if (abstractDoActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				abstractDoActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(abstractDoActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getObjectReferences() {
		/**
		 * @OCL let var1: OrderedSet(mcore::objects::MObject)  = intelligentInstance->asOrderedSet()->select(it: mcore::objects::MObject | let chain: OrderedSet(mcore::objects::MObjectReference)  = it.referencedBy->asOrderedSet() in
		if chain->notEmpty().oclIsUndefined() 
		then null 
		else chain->notEmpty()
		endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		if (canApplyDefaultContainment) 
		=true 
		then var1
		else OrderedSet{}
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__OBJECT_REFERENCES;

		if (objectReferencesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				objectReferencesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(objectReferencesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getNearestInstance() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{}
		else
		if allClassesContainedIn()->isEmpty() then
		MClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).instance
		else
		if allClassesContainedIn().instance->isEmpty() then
		allClassesContainedIn()->first().nearestInstance
		else
		allClassesContainedIn().instance
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__NEAREST_INSTANCE;

		if (nearestInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				nearestInstanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(nearestInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getStrictNearestInstance() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		if strictAllClassesContainedIn->isEmpty() then
		MClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).strictInstance
		-- subpackages dont have resourceRootClass set, change that, saves runtime
		else
		if strictAllClassesContainedIn.instance->isEmpty() then
		strictAllClassesContainedIn->first().strictNearestInstance
		else
		strictAllClassesContainedIn.strictInstance
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__STRICT_NEAREST_INSTANCE;

		if (strictNearestInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				strictNearestInstanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(strictNearestInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getStrictInstance() {
		/**
		 * @OCL let var1: OrderedSet(mcore::objects::MObject)  = instance->asOrderedSet()->select(it: mcore::objects::MObject | let e0: Boolean = it.type = self in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		if (canApplyDefaultContainment) 
		=true 
		then var1
		else OrderedSet{}
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__STRICT_INSTANCE;

		if (strictInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				strictInstanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(strictInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getIntelligentNearestInstance() {
		/**
		 * @OCL if canApplyDefaultContainment= false then OrderedSet{}
		else
		if intelligentAllClassesContainedIn->isEmpty()  then
		MClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).intelligentInstance 
		--subpackages don't have resourceRootClass set ,  change that  => saves runtime here
		else
		if strictNearestInstance->isEmpty() then
		if nearestInstance->isEmpty() then
		intelligentAllClassesContainedIn->first().intelligentNearestInstance
		else 
		nearestInstance
		endif 
		
		else strictNearestInstance
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE;

		if (intelligentNearestInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				intelligentNearestInstanceDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(intelligentNearestInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getObjectUnreferenced() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		if  intelligentInstance->isEmpty() then OrderedSet{}
		else if objectReferences->isEmpty() then intelligentInstance
		else intelligentInstance->symmetricDifference(objectReferences) endif endif
		--add symmetric diff to MRules
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__OBJECT_UNREFERENCED;

		if (objectUnreferencedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				objectUnreferencedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(objectUnreferencedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getOutstandingToCopyObjects() {
		/**
		 * @OCL objectUnreferenced->asOrderedSet()->select(it: mcore::objects::MObject | let e0: Boolean = intelligentNearestInstance->asOrderedSet()->excludes(it.containingObject)   in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() 
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS;

		if (outstandingToCopyObjectsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				outstandingToCopyObjectsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(outstandingToCopyObjectsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyInstance> getPropertyInstances() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		let os:OrderedSet(objects::MPropertyInstance) = 
		objects::MPropertyInstance.allInstances()->asOrderedSet()
		--try to avoid allInstances
		in
		if os->isEmpty() then
		OrderedSet{} 
		else
		os->select(x:objects::MPropertyInstance| not( x.property.oclIsUndefined()) and x.property.simpleType = SimpleType::None and x.property.type = self)->asOrderedSet()
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__PROPERTY_INSTANCES;

		if (propertyInstancesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				propertyInstancesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(propertyInstancesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyInstance> result = (EList<MPropertyInstance>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyInstance> getObjectReferencePropertyInstances() {
		/**
		 * @OCL if propertyInstances->isEmpty() then OrderedSet{} else
		propertyInstances->asOrderedSet()->select(it: mcore::objects::MPropertyInstance | let chain: OrderedSet(mcore::objects::MObjectReference)  = it.internalReferencedObject->asOrderedSet() in
		if chain->notEmpty().oclIsUndefined() 
		then null 
		else chain->notEmpty()
		endif)->asOrderedSet()->excluding(null)->asOrderedSet() 
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES;

		if (objectReferencePropertyInstancesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				objectReferencePropertyInstancesDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(objectReferencePropertyInstancesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyInstance> result = (EList<MPropertyInstance>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getIntelligentInstance() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		if self.strictInstance->isEmpty() and self.instance->isEmpty() then OrderedSet{}
		else
		if self.strictInstance->isEmpty() then self.instance else
		let s: OrderedSet(MClassifier) = self.strictInstance->first().type.intelligentContainmentHierarchy in 
		if self.instance->forAll(type.intelligentContainmentHierarchy = s ) then  instance
		else 
		self.strictInstance
		endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INTELLIGENT_INSTANCE;

		if (intelligentInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				intelligentInstanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(intelligentInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getContainmentHierarchy() {
		/**
		 * @OCL  if allClassesContainedIn()->isEmpty() then OrderedSet{}  --isRoot
		else
		if allClassesContainedIn()->first().allClassesContainedIn()->excludes(self) then
		allClassesContainedIn()->first()->asOrderedSet()->union(allClassesContainedIn()->first().containmentHierarchy)
		else
		OrderedSet{} -- Circle
		endif
		endif 
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__CONTAINMENT_HIERARCHY;

		if (containmentHierarchyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containmentHierarchyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containmentHierarchyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getStrictAllClassesContainedIn() {
		/**
		 * @OCL 
		allClassesContainedIn().property
		->select(p:MProperty| p.type=self.containingClassifier).containingClassifier->asSet()
		
		--Need custom OCL because of Set
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN;

		if (strictAllClassesContainedInDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				strictAllClassesContainedInDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(strictAllClassesContainedInDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getStrictContainmentHierarchy() {
		/**
		 * @OCL if self.canApplyDefaultContainment then
		if strictAllClassesContainedIn->isEmpty() then OrderedSet{null}  --isRoot or non existing
		else
		strictAllClassesContainedIn->first()->asSequence()->union(strictAllClassesContainedIn->first().strictContainmentHierarchy->asSequence())
		endif
		--need custom OCL because of asSequence
		else
		OrderedSet{}endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY;

		if (strictContainmentHierarchyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				strictContainmentHierarchyDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(strictContainmentHierarchyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getIntelligentAllClassesContainedIn() {
		/**
		 * @OCL if (let chain: OrderedSet(mcore::MClassifier)  = strictAllClassesContainedIn->asOrderedSet() in
		if chain->isEmpty().oclIsUndefined() 
		then null 
		else chain->isEmpty()
		endif) 
		=true 
		then allClassesContainedIn()->asOrderedSet()
		else strictAllClassesContainedIn->asOrderedSet()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN;

		if (intelligentAllClassesContainedInDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				intelligentAllClassesContainedInDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(intelligentAllClassesContainedInDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getIntelligentContainmentHierarchy() {
		/**
		 * @OCL if canApplyDefaultContainment then
		if strictAllClassesContainedIn->isEmpty() and allClassesContainedIn()->isEmpty()
		then OrderedSet{null}  --isRoot or non existing
		else
		if  strictAllClassesContainedIn->isEmpty() then
		allClassesContainedIn()->first()->asSequence()->union(allClassesContainedIn()->first().intelligentContainmentHierarchy->asSequence())
		else 
		strictAllClassesContainedIn->first()->asSequence()->union(strictAllClassesContainedIn->first().intelligentContainmentHierarchy->asSequence())
		endif endif
		else
		OrderedSet{} endif
		
		-- Need sequence here again => Custom OCL
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY;

		if (intelligentContainmentHierarchyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				intelligentContainmentHierarchyDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(intelligentContainmentHierarchyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getInstantiationHierarchy() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		
		let s:Set(MClassifier) =
		intelligentContainmentHierarchy->asSet()->symmetricDifference(
		self.intelligentNearestInstance.type.intelligentContainmentHierarchy->asSet())
		in
		if s->isEmpty() then null else s
		endif endif
		--Custom OCL bc  symmetricDiff and Set
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INSTANTIATION_HIERARCHY;

		if (instantiationHierarchyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				instantiationHierarchyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(instantiationHierarchyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getInstantiationPropertyHierarchy() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		if intelligentAllClassesContainedIn->isEmpty() then
		Sequence{}
		else
		let s: Sequence(MProperty) = 
		intelligentAllClassesContainedIn->first().property->asSequence() in
		if s->select(type = self)->isEmpty() then s->select(self.superType->includes(type)) else s->select(type = self) endif
		->asSequence()->union(intelligentAllClassesContainedIn->first().instantiationPropertyHierarchy->asSequence())
		endif endif
		
		--custom ocl bc  sequence 
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY;

		if (instantiationPropertyHierarchyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				instantiationPropertyHierarchyDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(instantiationPropertyHierarchyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MProperty> result = (EList<MProperty>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getIntelligentInstantiationPropertyHierarchy() {
		/**
		 * @OCL if canApplyDefaultContainment = false then OrderedSet{} else
		
		intelligentNearestInstance.type.instantiationPropertyHierarchy->asOrderedSet()->iterate(it: mcore::MProperty;  acc: Sequence(MProperty) = self.instantiationPropertyHierarchy->asSequence()  |  if acc->includes(it)  then acc->excluding(it) else acc endif)->asSequence() endif
		
		--custom ocl bc sequence
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY;

		if (intelligentInstantiationPropertyHierarchyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				intelligentInstantiationPropertyHierarchyDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(
				intelligentInstantiationPropertyHierarchyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MProperty> result = (EList<MProperty>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getClassescontainedin() {
		/**
		 * @OCL allClassesContainedIn()->asOrderedSet()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__CLASSESCONTAINEDIN;

		if (classescontainedinDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				classescontainedinDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(classescontainedinDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MClassifier> result = (EList<MClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCanApplyDefaultContainment() {
		/**
		 * @OCL self.containmentHierarchy->notEmpty() and
		let s: OrderedSet(MClassifier) = 
		
		if strictAllClassesContainedIn->isEmpty() then allClassesContainedIn()
		else strictAllClassesContainedIn
		endif  in 
		
		s->size()=1
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT;

		if (canApplyDefaultContainmentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				canApplyDefaultContainmentDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(canApplyDefaultContainmentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getPropertiesAsText() {
		return propertiesAsText;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertiesAsText(String newPropertiesAsText) {
		String oldPropertiesAsText = propertiesAsText;
		propertiesAsText = newPropertiesAsText;
		boolean oldPropertiesAsTextESet = propertiesAsTextESet;
		propertiesAsTextESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT,
					oldPropertiesAsText, propertiesAsText,
					!oldPropertiesAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPropertiesAsText() {
		String oldPropertiesAsText = propertiesAsText;
		boolean oldPropertiesAsTextESet = propertiesAsTextESet;
		propertiesAsText = PROPERTIES_AS_TEXT_EDEFAULT;
		propertiesAsTextESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT,
					oldPropertiesAsText, PROPERTIES_AS_TEXT_EDEFAULT,
					oldPropertiesAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPropertiesAsText() {
		return propertiesAsTextESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getSemanticsAsText() {
		return semanticsAsText;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemanticsAsText(String newSemanticsAsText) {
		String oldSemanticsAsText = semanticsAsText;
		semanticsAsText = newSemanticsAsText;
		boolean oldSemanticsAsTextESet = semanticsAsTextESet;
		semanticsAsTextESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT,
					oldSemanticsAsText, semanticsAsText,
					!oldSemanticsAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSemanticsAsText() {
		String oldSemanticsAsText = semanticsAsText;
		boolean oldSemanticsAsTextESet = semanticsAsTextESet;
		semanticsAsText = SEMANTICS_AS_TEXT_EDEFAULT;
		semanticsAsTextESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT,
					oldSemanticsAsText, SEMANTICS_AS_TEXT_EDEFAULT,
					oldSemanticsAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSemanticsAsText() {
		return semanticsAsTextESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedJsonSchema() {
		/**
		 * @OCL let lineBreak : String = '\n' in
		let space : String = ' ' in
		let branch : String = '{' in
		let definition : String = '#/definitions/' in
		let closedBranch : String = '}' in
		let mandatoryProperties : OrderedSet(MProperty)= self.property->asOrderedSet()->reject(e : MProperty| not(e.mandatory))in
		let comma : String = ',' in
		-- Classname
		'"'.concat(self.eName).concat('"').concat(':').concat(branch)
		.concat(lineBreak)
		--Description
		.concat('"description": ').concat('"').concat(if self.description.oclIsUndefined() then 'no description' else self.description endif).concat('"').concat(comma)
		.concat(lineBreak)
		--Supertypes
		.concat(if self.superType->isEmpty() then '' else '"allOf": ['.concat(self.superType->iterate(classifier: MClassifier; s : String = '' | s.concat('{ "$ref": ').concat('"').concat(definition).concat(classifier.eName).concat('"').concat('}').concat(if superType->indexOf(classifier) = self.superType->size() then '' else lineBreak.concat(',') endif))).concat(']').concat(comma).concat(lineBreak)endif)
		--Properties
		.concat('"properties":').concat(space).concat(branch)
		.concat(lineBreak)
		.concat(self.property->iterate(property : MProperty;s : String= '' | s.concat(property.derivedJsonSchema).concat(if self.property->indexOf(property) = self.property->size() then ''.concat(lineBreak) else ','.concat(lineBreak) endif)))
		.concat(lineBreak)
		.concat(closedBranch)
		.concat(comma)
		.concat(lineBreak)
		-- Required fields
		.concat('"required": [').concat(mandatoryProperties->iterate(property: MProperty;s : String = ''| if (property.mandatory) then s.concat('"').concat(property.eName).concat('"').concat(if mandatoryProperties->indexOf(property) = mandatoryProperties->size() then '' else ',' endif) else s endif)).concat(']')
		.concat(closedBranch)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__DERIVED_JSON_SCHEMA;

		if (derivedJsonSchemaDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedJsonSchemaDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedJsonSchemaDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate toggleSuperType$Update(MClassifier trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MClassifier triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				McorePackage.eINSTANCE.getMClassifier_ToggleSuperType(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate orderingStrategy$Update(OrderingStrategy trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.OrderingStrategy triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMClassifier_OrderingStrategy(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MClassifierAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MClassifierAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMClassifier_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getSuperTypePackage() {
		if (superTypePackage != null && superTypePackage.eIsProxy()) {
			InternalEObject oldSuperTypePackage = (InternalEObject) superTypePackage;
			superTypePackage = (MPackage) eResolveProxy(oldSuperTypePackage);
			if (superTypePackage != oldSuperTypePackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE,
							oldSuperTypePackage, superTypePackage));
			}
		}
		return superTypePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetSuperTypePackage() {
		return superTypePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperTypePackage(MPackage newSuperTypePackage) {
		MPackage oldSuperTypePackage = superTypePackage;
		superTypePackage = newSuperTypePackage;
		boolean oldSuperTypePackageESet = superTypePackageESet;
		superTypePackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE,
					oldSuperTypePackage, superTypePackage,
					!oldSuperTypePackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSuperTypePackage() {
		MPackage oldSuperTypePackage = superTypePackage;
		boolean oldSuperTypePackageESet = superTypePackageESet;
		superTypePackage = null;
		superTypePackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE,
					oldSuperTypePackage, null, oldSuperTypePackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSuperTypePackage() {
		return superTypePackageESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getSuperType() {
		if (superType == null) {
			superType = new EObjectResolvingEList.Unsettable<MClassifier>(
					MClassifier.class, this,
					McorePackage.MCLASSIFIER__SUPER_TYPE);
		}
		return superType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSuperType() {
		if (superType != null)
			((InternalEList.Unsettable<?>) superType).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSuperType() {
		return superType != null
				&& ((InternalEList.Unsettable<?>) superType).isSet();
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Super Type</b></em>' reference list.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL   not (trg = self) 
	and
	trg.kind = ClassifierKind::ClassType
	and
	trg.selectableClassifier(superType, superTypePackage)
	
	
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalSuperTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		if (superTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = McorePackage.Literals.MCLASSIFIER__SUPER_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				superTypeChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"SuperTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(superTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"SuperTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getInstance() {
		/**
		 * @OCL let os:OrderedSet(objects::MObject) = 
		objects::MObject.allInstances()->asOrderedSet()
		in
		if os->isEmpty() then
		OrderedSet{} 
		else
		os->select(x:objects::MObject| x.isInstanceOf(self) or x.type.allSuperTypes()->includes(self))->asOrderedSet()
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__INSTANCE;

		if (instanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				instanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(instanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObject> result = (EList<MObject>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierKind getKind() {
		/**
		 * @OCL if self.isClassByStructure 
		then if self.literal->isEmpty() 
		then ClassifierKind::ClassType 
		else ClassifierKind::WrongClassWithLiteral endif
		else  
		if self.literal->notEmpty() then 
		ClassifierKind::Enumeration else 
		ClassifierKind::DataType
		endif endif 
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__KIND;

		if (kindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				kindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ClassifierKind result = (ClassifierKind) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getEnforcedClass() {
		return enforcedClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnforcedClass(Boolean newEnforcedClass) {
		Boolean oldEnforcedClass = enforcedClass;
		enforcedClass = newEnforcedClass;
		boolean oldEnforcedClassESet = enforcedClassESet;
		enforcedClassESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__ENFORCED_CLASS, oldEnforcedClass,
					enforcedClass, !oldEnforcedClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEnforcedClass() {
		Boolean oldEnforcedClass = enforcedClass;
		boolean oldEnforcedClassESet = enforcedClassESet;
		enforcedClass = ENFORCED_CLASS_EDEFAULT;
		enforcedClassESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__ENFORCED_CLASS, oldEnforcedClass,
					ENFORCED_CLASS_EDEFAULT, oldEnforcedClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEnforcedClass() {
		return enforcedClassESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAbstractClass() {
		return abstractClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractClass(Boolean newAbstractClass) {
		Boolean oldAbstractClass = abstractClass;
		abstractClass = newAbstractClass;
		boolean oldAbstractClassESet = abstractClassESet;
		abstractClassESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__ABSTRACT_CLASS, oldAbstractClass,
					abstractClass, !oldAbstractClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAbstractClass() {
		Boolean oldAbstractClass = abstractClass;
		boolean oldAbstractClassESet = abstractClassESet;
		abstractClass = ABSTRACT_CLASS_EDEFAULT;
		abstractClassESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__ABSTRACT_CLASS, oldAbstractClass,
					ABSTRACT_CLASS_EDEFAULT, oldAbstractClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAbstractClass() {
		return abstractClassESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier getInternalEClassifier() {
		if (internalEClassifier != null && internalEClassifier.eIsProxy()) {
			InternalEObject oldInternalEClassifier = (InternalEObject) internalEClassifier;
			internalEClassifier = (EClassifier) eResolveProxy(
					oldInternalEClassifier);
			if (internalEClassifier != oldInternalEClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER,
							oldInternalEClassifier, internalEClassifier));
			}
		}
		return internalEClassifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClassifier basicGetInternalEClassifier() {
		return internalEClassifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalEClassifier(EClassifier newInternalEClassifier) {
		EClassifier oldInternalEClassifier = internalEClassifier;
		internalEClassifier = newInternalEClassifier;
		boolean oldInternalEClassifierESet = internalEClassifierESet;
		internalEClassifierESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER,
					oldInternalEClassifier, internalEClassifier,
					!oldInternalEClassifierESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalEClassifier() {
		EClassifier oldInternalEClassifier = internalEClassifier;
		boolean oldInternalEClassifierESet = internalEClassifierESet;
		internalEClassifier = null;
		internalEClassifierESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER,
					oldInternalEClassifier, null, oldInternalEClassifierESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalEClassifier() {
		return internalEClassifierESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getContainingPackage() {
		MPackage containingPackage = basicGetContainingPackage();
		return containingPackage != null && containingPackage.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) containingPackage)
				: containingPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetContainingPackage() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() then null else self.eContainer().oclAsType(MPackage) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__CONTAINING_PACKAGE;

		if (containingPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getRepresentsCoreType() {
		/**
		 * @OCL if containingPackage.oclIsUndefined() 
		then false
		else containingPackage.representsCorePackage endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__REPRESENTS_CORE_TYPE;

		if (representsCoreTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				representsCoreTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(representsCoreTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertiesGroup> getAllPropertyGroups() {
		/**
		 * @OCL propertyGroup->union(propertyGroup->closure(propertyGroup))->sortedBy(indentLevel)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__ALL_PROPERTY_GROUPS;

		if (allPropertyGroupsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				allPropertyGroupsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allPropertyGroupsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertiesGroup> result = (EList<MPropertiesGroup>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> getLiteral() {
		if (literal == null) {
			literal = new EObjectContainmentEList.Unsettable.Resolving<MLiteral>(
					MLiteral.class, this, McorePackage.MCLASSIFIER__LITERAL);
		}
		return literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLiteral() {
		if (literal != null)
			((InternalEList.Unsettable<?>) literal).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLiteral() {
		return literal != null
				&& ((InternalEList.Unsettable<?>) literal).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getEInterface() {
		return eInterface;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEInterface(Boolean newEInterface) {
		Boolean oldEInterface = eInterface;
		eInterface = newEInterface;
		boolean oldEInterfaceESet = eInterfaceESet;
		eInterfaceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCLASSIFIER__EINTERFACE, oldEInterface,
					eInterface, !oldEInterfaceESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEInterface() {
		Boolean oldEInterface = eInterface;
		boolean oldEInterfaceESet = eInterfaceESet;
		eInterface = EINTERFACE_EDEFAULT;
		eInterfaceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCLASSIFIER__EINTERFACE, oldEInterface,
					EINTERFACE_EDEFAULT, oldEInterfaceESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEInterface() {
		return eInterfaceESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getOperationAsIdPropertyError() {
		/**
		 * @OCL self.idProperty->exists(p:MProperty|p.kind=PropertyKind::Operation)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR;

		if (operationAsIdPropertyErrorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				operationAsIdPropertyErrorDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(operationAsIdPropertyErrorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsClassByStructure() {
		/**
		 * @OCL self.enforcedClass or 
		self.superType->notEmpty() or 
		self.ownedProperty->notEmpty() or 
		self.abstractClass or
		self.hasSimpleModelingType 
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__IS_CLASS_BY_STRUCTURE;

		if (isClassByStructureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isClassByStructureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isClassByStructureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getToggleSuperType() {
		MClassifier toggleSuperType = basicGetToggleSuperType();
		return toggleSuperType != null && toggleSuperType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) toggleSuperType)
				: toggleSuperType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetToggleSuperType() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__TOGGLE_SUPER_TYPE;

		if (toggleSuperTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				toggleSuperTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(toggleSuperTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setToggleSuperType(MClassifier newToggleSuperType) {
		EList<MClassifier> l = this.getSuperType();
		boolean newDefaultPropertyForTypeNeeded = false;
		if (l.contains(newToggleSuperType)) {
			newDefaultPropertyForTypeNeeded = l.remove(newToggleSuperType);
		} else {
			l.add(newToggleSuperType);
		}
		// Adding a new superlcass deletes the subclass containment in the
		// RootClass

		if (getContainingClassifier() == null
				|| getContainingClassifier().getContainingPackage() == null
				|| getContainingClassifier().getContainingPackage()
						.getContainingComponent() == null)
			return;

		if (!this.getContainingClassifier().getContainingPackage()
				.getContainingComponent().isSetDisableDefaultContainment()
				|| (this.getContainingClassifier().getContainingPackage()
						.getContainingComponent()
						.getDisableDefaultContainment() == null
						|| this.getContainingClassifier().getContainingPackage()
								.getContainingComponent()
								.getDisableDefaultContainment() != true)) {

			if ((this.getContainingPackage() != null) && (this
					.getContainingPackage().getContainingComponent() != null)) {

				EList<MPackage> packageList = this.getContainingPackage()
						.getContainingComponent().getOwnedPackage();
				MClassifier mClass = null;

				if (newToggleSuperType.getContainingPackage() != null
						&& newToggleSuperType.getContainingPackage()
								.getContainingComponent() != null)
					if (!newToggleSuperType.getContainingPackage()
							.getContainingComponent()
							.equals(this.getContainingPackage()
									.getContainingComponent()))
						return;

				for (MPackage pack : packageList)
					if (pack.getResourceRootClass() != null)
						mClass = pack.getResourceRootClass();

				boolean deleteProp = false;
				MProperty updatingProp = null;
				for (MProperty prop : mClass.getProperty()) {
					if (prop.getContainment() && l.contains(prop.getType())) {
						updatingProp = prop;
						deleteProp = true;
					}
					if (prop.getContainment() && newToggleSuperType
							.getContainingClassifier() != mClass) {
						deleteProp = !newDefaultPropertyForTypeNeeded;
						updatingProp = newToggleSuperType
								.getInstantiationPropertyHierarchy().isEmpty()
										? null
										: newToggleSuperType
												.getInstantiationPropertyHierarchy()
												.get(0);
					}
				}
				// Why is deleteProp true?? Must be exclusive to
				// NewPropertyNeeded
				Iterator<MProperty> iter = mClass.getProperty().iterator();
				MProperty defaultPropertyOfSubType = null;
				if (deleteProp) {
					while (iter.hasNext()) {
						MProperty pro = (MProperty) iter.next();
						if (pro.getContainment() && (pro.getType() == this)) {
							defaultPropertyOfSubType = pro;
							iter.remove();
							break;

						}

					}
				} else if (newDefaultPropertyForTypeNeeded) {

					defaultPropertyOfSubType = McoreFactory.eINSTANCE
							.createMProperty();
					defaultPropertyOfSubType.setType(this);
					mClass.getProperty().add(defaultPropertyOfSubType);

					defaultPropertyOfSubType.setSimpleType(SimpleType.NONE);
					defaultPropertyOfSubType
							.setPropertyBehavior(PropertyBehavior.CONTAINED);

				}
				EList<MResourceFolder> resFolder = (EList<MResourceFolder>) this
						.getContainingClassifier().getContainingPackage()
						.getContainingComponent().getResourceFolder();

				for (MResourceFolder folder : resFolder)
					if (folder != null) {

						folder.updateResource(defaultPropertyOfSubType,
								updatingProp);
					}

			}

		}
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Toggle Super Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL   not (trg = self) 
	and
	trg.kind = ClassifierKind::ClassType
	and
	trg.selectableClassifier(superType, superTypePackage)
	
	
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalToggleSuperTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		if (toggleSuperTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = McorePackage.Literals.MCLASSIFIER__TOGGLE_SUPER_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				toggleSuperTypeChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"ToggleSuperTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(toggleSuperTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"ToggleSuperTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OrderingStrategy getOrderingStrategy() {
		/**
		 * @OCL OrderingStrategy::None
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__ORDERING_STRATEGY;

		if (orderingStrategyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				orderingStrategyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(orderingStrategyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			OrderingStrategy result = (OrderingStrategy) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrderingStrategy(OrderingStrategy newOrderingStrategy) {
		if (newOrderingStrategy == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getTestAllProperties() {
		/**
		 * @OCL self.allProperties()
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		EStructuralFeature eFeature = McorePackage.Literals.MCLASSIFIER__TEST_ALL_PROPERTIES;

		if (testAllPropertiesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				testAllPropertiesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(testAllPropertiesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MProperty> result = (EList<MProperty>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getIdProperty() {
		if (idProperty == null) {
			idProperty = new EObjectResolvingEList.Unsettable<MProperty>(
					MProperty.class, this,
					McorePackage.MCLASSIFIER__ID_PROPERTY);
		}
		return idProperty;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIdProperty() {
		if (idProperty != null)
			((InternalEList.Unsettable<?>) idProperty).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIdProperty() {
		return idProperty != null
				&& ((InternalEList.Unsettable<?>) idProperty).isSet();
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Id Property</b></em>' reference list.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MProperty
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL self.allFeatures()->includes(trg)
	and ( trg.kind<>PropertyKind::Operation)
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalIdPropertyChoiceConstraint(MProperty trg) {
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		if (idPropertyChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = McorePackage.Literals.MCLASSIFIER__ID_PROPERTY;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				idPropertyChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"IdPropertyChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(idPropertyChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"IdPropertyChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty defaultPropertyValue() {

		/**
		 * @OCL Tuple {simpleType=SimpleType::String, name=defaultPropertyDescription()}
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(18);
		if (defaultPropertyValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				defaultPropertyValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultPropertyValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String defaultPropertyDescription() {

		/**
		 * @OCL 'Text 1'
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(19);
		if (defaultPropertyDescriptionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				defaultPropertyDescriptionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultPropertyDescriptionBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteral> allLiterals() {

		/**
		 * @OCL literal
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(3);
		if (allLiteralsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allLiteralsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allLiteralsBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MLiteral>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean selectableClassifier(EList<MClassifier> currentClassifier,
			MPackage filterPackage) {

		/**
		 * @OCL let mClassifier:MClassifier = self in 
		
		if currentClassifier->includes(mClassifier) 
		then true
		else if mClassifier.containingPackage.oclIsUndefined()
		then false
		else if filterPackage.oclIsUndefined()
		then  if mClassifier.containingPackage.usePackageContentOnlyWithExplicitFilter
		then if currentClassifier->isEmpty()
		then false
		else currentClassifier.containingPackage->includes(mClassifier.containingPackage) endif          
		else true endif
		else
		filterPackage=mClassifier.containingPackage
		endif endif 
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(4);
		if (selectableClassifiermcoreMClassifiermcoreMPackageBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				selectableClassifiermcoreMClassifiermcoreMPackageBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				selectableClassifiermcoreMClassifiermcoreMPackageBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("currentClassifier", currentClassifier);

			evalEnv.add("filterPackage", filterPackage);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> allDirectSubTypes() {

		/**
		 * @OCL MClassifier.allInstances()
		->select(c:MClassifier|c.kind=ClassifierKind::ClassType and c.superType->includes(self))
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(5);
		if (allDirectSubTypesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allDirectSubTypesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allDirectSubTypesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MClassifier>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> allSubTypes() {

		/**
		 * @OCL let dS:Set(MClassifier)=self.allDirectSubTypes() in
		let iDS:Set(MClassifier)=dS->iterate(c:MClassifier;i:Set(MClassifier)=Set{}|i->union(c.allSubTypes())) in
		dS->union(iDS)->excluding(null)
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(6);
		if (allSubTypesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allSubTypesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allSubTypesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MClassifier>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String superTypesAsString() {

		/**
		 * @OCL let sts:OrderedSet(MClassifier) = self.superType in
		if sts->isEmpty()then'' else
		sts->iterate(c:MClassifier;res:String='' |
		(if res='' then '' else res.concat(', ') endif)
		.concat(c.eName))
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(7);
		if (superTypesAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				superTypesAsStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(superTypesAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<MClassifier> allSuperTypes() {
		EList<MClassifier> result = new BasicEList<MClassifier>();
		for (MClassifier superType : getSuperType()) {
			result.addAll(superType.allSuperTypes());
			result.add(superType);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allProperties() {

		/**
		 * @OCL ownedProperty
		->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=self.allSuperProperties()| ps->append(p))
		
		/*self.superType.allProperties()->asSet()
		->union(self.ownedProperty)->asOrderedSet()*\/
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(9);
		if (allPropertiesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allPropertiesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allPropertiesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allSuperProperties() {

		/**
		 * @OCL superType.allProperties()
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(10);
		if (allSuperPropertiesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allSuperPropertiesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allSuperPropertiesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allFeatures() {

		/**
		 * @OCL if self.allProperties()->isEmpty() then OrderedSet{} else
		self.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Attribute or p.kind=mcore::PropertyKind::Reference
		)
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(11);
		if (allFeaturesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allFeaturesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allFeaturesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allNonContainmentFeatures() {

		/**
		 * @OCL if allProperties()->isEmpty() then OrderedSet{} else
		allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Attribute or (p.kind=mcore::PropertyKind::Reference and not (p.containment))
		)
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(12);
		if (allNonContainmentFeaturesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allNonContainmentFeaturesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allNonContainmentFeaturesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allStoredNonContainmentFeatures() {

		/**
		 * @OCL if allProperties()->isEmpty() 
		then OrderedSet{} 
		else allProperties()
		->select(p:mcore::MProperty|
		p.kind=mcore::PropertyKind::Attribute 
		or 
		(p.kind=mcore::PropertyKind::Reference 
		and p.hasStorage 
		and not (p.containment))) endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(13);
		if (allStoredNonContainmentFeaturesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allStoredNonContainmentFeaturesBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(allStoredNonContainmentFeaturesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allFeaturesWithStorage() {

		/**
		 * @OCL let ncs:OrderedSet(mcore::MProperty)=self.allNonContainmentFeatures()->select(p:mcore::MProperty|p.hasStorage) in
		let cs:OrderedSet(mcore::MProperty)=self.allContainmentReferences()->select(p:mcore::MProperty|p.hasStorage) in
		cs->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=ncs| ps->append(p))
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(14);
		if (allFeaturesWithStorageBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allFeaturesWithStorageBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allFeaturesWithStorageBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allContainmentReferences() {

		/**
		 * @OCL if self.allProperties()->isEmpty() then OrderedSet{} else
		self.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Reference and p.containment
		)->asOrderedSet()
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(15);
		if (allContainmentReferencesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allContainmentReferencesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allContainmentReferencesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MOperationSignature> allOperationSignatures() {

		/**
		 * @OCL if self.allProperties()->isEmpty() then OrderedSet{} else
		self.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Operation).operationSignature->asOrderedSet()
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(16);
		if (allOperationSignaturesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allOperationSignaturesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allOperationSignaturesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MOperationSignature>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> allClassesContainedIn() {

		/**
		 * @OCL 
		MClassifier.allInstances().ownedProperty->select(containment)
		->select(p:MProperty| (p.type=self.containingClassifier  or self.superType.containingClassifier->includes(p.type )and p<>self)).containingClassifier
		->excluding(self)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(17);
		if (allClassesContainedInBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allClassesContainedInBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allClassesContainedInBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MClassifier>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allIdProperties() {

		/**
		 * @OCL if self.idProperty->isEmpty() then self.allSuperIdProperties() else
		self.idProperty->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=self.allSuperIdProperties()| ps->append(p))
		endif
		/*self.superType.allProperties()->asSet()
		->union(self.ownedProperty)->asOrderedSet()*\/
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(20);
		if (allIdPropertiesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allIdPropertiesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allIdPropertiesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> allSuperIdProperties() {

		/**
		 * @OCL self.superType.allIdProperties()->asOrderedSet()
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(21);
		if (allSuperIdPropertiesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allSuperIdPropertiesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allSuperIdPropertiesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MClassifierAction trg) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		EEnumLiteral eEnumLiteralTRIGGER = McorePackage.Literals.MPROPERTY_ACTION
				.getEEnumLiteral(trg.getValue());
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMProperty_DoAction(),
				XUpdateMode.REDEFINE, null,
				// :=
				eEnumLiteralTRIGGER, null, null);

		currentTrigger.invokeOperationCall(this,
				McorePackage.Literals.MCLASSIFIER___INVOKE_SET_DO_ACTION__MCLASSIFIERACTION,
				trg, null, null);

		return currentTrigger;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object invokeSetDoAction(MClassifierAction mClassifierAction) {

		this.setDoAction(mClassifierAction);

		switch (mClassifierAction) {
		case BOOLEAN_ATTRIBUTE:
		case DATE_ATTRIBUTE:
		case INTEGER_ATTRIBUTE:
		case NARY_CONTAINMENT:
		case NARY_REFERENCE:
		case REAL_ATTRIBUTE:
		case STRING_ATTRIBUTE:
		case UNARY_CONTAINMENT:
		case UNARY_REFERENCE:
			return this.getProperty().get(getProperty().size() - 1);
		case LITERAL:
			return this.getLiteral().get(getLiteral().size() - 1);
		case CONSTRAINT:
			return this.getAllConstraintAnnotations()
					.get(getAllConstraintAnnotations().size() - 1);
		case CREATE_INSTANCE:
			return this.getInstance().get(getInstance().size() - 1);
		case GROUP_OF_PROPERTIES:
			getPropertyGroup().get(getPropertyGroup().size() - 1);
		case INTO_DATATYPE:
		case INTO_ENUMERATION:
		case ABSTRACT:
			return this;
		case LABEL:
			return this.getAnnotations().getLabel();
		case OPERATION_ONE_PARAMETER:
		case OPERATION_TWO_PARAMETERS: {
			for (int i = getProperty().size() - 1; i > 0; i--)
				if (!this.getProperty().get(i).getOperationSignature()
						.isEmpty())
					for (MOperationAnnotations annotation : getAnnotations()
							.getOperationAnnotations())
						if (annotation.getAnnotatedProperty() == getProperty()
								.get(i))
							return annotation.getResult();

		}
		case SPECIALIZE:
			break;

		default:
			return this;
		}

		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object invokeToggleSuperType(MClassifier mClassifier) {

		this.setToggleSuperType(mClassifier);
		return this;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doSuperTypeUpdate(MClassifier trg) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				McorePackage.eINSTANCE.getMClassifier_ToggleSuperType(),
				XUpdateMode.REDEFINE, null,
				// :=
				trg, null, null);

		currentTrigger.invokeOperationCall(this,
				McorePackage.Literals.MCLASSIFIER___INVOKE_TOGGLE_SUPER_TYPE__MCLASSIFIER,
				trg, null, null);

		return currentTrigger;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ambiguousClassifierName() {

		/**
		 * @OCL let allClasses: OrderedSet(mcore::MClassifier)  = let chain: OrderedSet(mcore::MClassifier)  = if containingPackage.oclIsUndefined()
		then OrderedSet{}
		else containingPackage.classifier
		endif in
		chain->iterate(i:mcore::MClassifier; r: OrderedSet(mcore::MClassifier)=OrderedSet{} | if i.oclIsKindOf(mcore::MClassifier) then r->including(i.oclAsType(mcore::MClassifier))->asOrderedSet() 
		else r endif)->select(it: mcore::MClassifier | let e0: Boolean = it <> self in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let sameName: OrderedSet(mcore::MClassifier)  = let chain: OrderedSet(mcore::MClassifier)  = allClasses in
		chain->iterate(i:mcore::MClassifier; r: OrderedSet(mcore::MClassifier)=OrderedSet{} | if i.oclIsKindOf(mcore::MClassifier) then r->including(i.oclAsType(mcore::MClassifier))->asOrderedSet() 
		else r endif)->select(it: mcore::MClassifier | let e0: Boolean = it.name = name in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let var0: Boolean = let chain: OrderedSet(mcore::MClassifier)  = sameName in
		if chain->notEmpty().oclIsUndefined() 
		then null 
		else chain->notEmpty()
		endif in
		var0
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EOperation eOperation = McorePackage.Literals.MCLASSIFIER
				.getEOperations().get(26);
		if (ambiguousClassifierNameBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ambiguousClassifierNameBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ambiguousClassifierNameBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String simpleTypeAsString(SimpleType simpleType) {

		/**
		 * @OCL if  simpleType=SimpleType::None then '-' else
		if  simpleType=SimpleType::String then 'String' else
		if simpleType=SimpleType::Boolean then 'Boolean' else
		if  simpleType=SimpleType::Date then 'Date' else
		if  simpleType=SimpleType::Double then 'Double' else
		if  simpleType=SimpleType::Integer then 'Integer' else
		if  simpleType=SimpleType::Annotation then 'Annotation' else
		if  simpleType=SimpleType::Attribute then 'Attribute' else
		if  simpleType=SimpleType::Class then 'Class' else
		if  simpleType=SimpleType::Classifier then 'Classifier' else
		if  simpleType=SimpleType::DataType then 'Data Type' else
		if simpleType=SimpleType::Enumeration then 'Enumeration' else
		if  simpleType=SimpleType::Feature then 'Feature' else
		if  simpleType=SimpleType::KeyValue then 'KeyValue' else
		if  simpleType=SimpleType::Literal then 'Literal' else
		if  simpleType=SimpleType::Object then 'EObject' else
		if simpleType= SimpleType::Any then 'Object' else
		if  simpleType=SimpleType::Operation then 'Operation' else
		if simpleType=SimpleType::Package then 'Package' else
		if  simpleType=SimpleType::Parameter then 'Parameter' else
		if  simpleType=SimpleType::Reference then 'Reference' else
		if simpleType=SimpleType::NamedElement then 'Named Element' else
		if simpleType=SimpleType::TypedElement then 'Typed Element' else 
		'ERROR' endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MHAS_SIMPLE_TYPE);
		EOperation eOperation = McorePackage.Literals.MHAS_SIMPLE_TYPE
				.getEOperations().get(0);
		if (simpleTypeAsStringmcoreSimpleTypeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				simpleTypeAsStringmcoreSimpleTypeBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MCLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(simpleTypeAsStringmcoreSimpleTypeBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("simpleType", simpleType);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MCLASSIFIER__LITERAL:
			return ((InternalEList<?>) getLiteral()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE_STRING:
			return getSimpleTypeString();
		case McorePackage.MCLASSIFIER__HAS_SIMPLE_DATA_TYPE:
			return getHasSimpleDataType();
		case McorePackage.MCLASSIFIER__HAS_SIMPLE_MODELING_TYPE:
			return getHasSimpleModelingType();
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE_IS_CORRECT:
			return getSimpleTypeIsCorrect();
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE:
			return getSimpleType();
		case McorePackage.MCLASSIFIER__ABSTRACT_CLASS:
			return getAbstractClass();
		case McorePackage.MCLASSIFIER__ENFORCED_CLASS:
			return getEnforcedClass();
		case McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE:
			if (resolve)
				return getSuperTypePackage();
			return basicGetSuperTypePackage();
		case McorePackage.MCLASSIFIER__SUPER_TYPE:
			return getSuperType();
		case McorePackage.MCLASSIFIER__INSTANCE:
			return getInstance();
		case McorePackage.MCLASSIFIER__KIND:
			return getKind();
		case McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER:
			if (resolve)
				return getInternalEClassifier();
			return basicGetInternalEClassifier();
		case McorePackage.MCLASSIFIER__CONTAINING_PACKAGE:
			if (resolve)
				return getContainingPackage();
			return basicGetContainingPackage();
		case McorePackage.MCLASSIFIER__REPRESENTS_CORE_TYPE:
			return getRepresentsCoreType();
		case McorePackage.MCLASSIFIER__ALL_PROPERTY_GROUPS:
			return getAllPropertyGroups();
		case McorePackage.MCLASSIFIER__LITERAL:
			return getLiteral();
		case McorePackage.MCLASSIFIER__EINTERFACE:
			return getEInterface();
		case McorePackage.MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR:
			return getOperationAsIdPropertyError();
		case McorePackage.MCLASSIFIER__IS_CLASS_BY_STRUCTURE:
			return getIsClassByStructure();
		case McorePackage.MCLASSIFIER__TOGGLE_SUPER_TYPE:
			if (resolve)
				return getToggleSuperType();
			return basicGetToggleSuperType();
		case McorePackage.MCLASSIFIER__ORDERING_STRATEGY:
			return getOrderingStrategy();
		case McorePackage.MCLASSIFIER__TEST_ALL_PROPERTIES:
			return getTestAllProperties();
		case McorePackage.MCLASSIFIER__ID_PROPERTY:
			return getIdProperty();
		case McorePackage.MCLASSIFIER__DO_ACTION:
			return getDoAction();
		case McorePackage.MCLASSIFIER__RESOLVE_CONTAINER_ACTION:
			return getResolveContainerAction();
		case McorePackage.MCLASSIFIER__ABSTRACT_DO_ACTION:
			return getAbstractDoAction();
		case McorePackage.MCLASSIFIER__OBJECT_REFERENCES:
			return getObjectReferences();
		case McorePackage.MCLASSIFIER__NEAREST_INSTANCE:
			return getNearestInstance();
		case McorePackage.MCLASSIFIER__STRICT_NEAREST_INSTANCE:
			return getStrictNearestInstance();
		case McorePackage.MCLASSIFIER__STRICT_INSTANCE:
			return getStrictInstance();
		case McorePackage.MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE:
			return getIntelligentNearestInstance();
		case McorePackage.MCLASSIFIER__OBJECT_UNREFERENCED:
			return getObjectUnreferenced();
		case McorePackage.MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS:
			return getOutstandingToCopyObjects();
		case McorePackage.MCLASSIFIER__PROPERTY_INSTANCES:
			return getPropertyInstances();
		case McorePackage.MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES:
			return getObjectReferencePropertyInstances();
		case McorePackage.MCLASSIFIER__INTELLIGENT_INSTANCE:
			return getIntelligentInstance();
		case McorePackage.MCLASSIFIER__CONTAINMENT_HIERARCHY:
			return getContainmentHierarchy();
		case McorePackage.MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN:
			return getStrictAllClassesContainedIn();
		case McorePackage.MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY:
			return getStrictContainmentHierarchy();
		case McorePackage.MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN:
			return getIntelligentAllClassesContainedIn();
		case McorePackage.MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY:
			return getIntelligentContainmentHierarchy();
		case McorePackage.MCLASSIFIER__INSTANTIATION_HIERARCHY:
			return getInstantiationHierarchy();
		case McorePackage.MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY:
			return getInstantiationPropertyHierarchy();
		case McorePackage.MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY:
			return getIntelligentInstantiationPropertyHierarchy();
		case McorePackage.MCLASSIFIER__CLASSESCONTAINEDIN:
			return getClassescontainedin();
		case McorePackage.MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT:
			return getCanApplyDefaultContainment();
		case McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT:
			return getPropertiesAsText();
		case McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT:
			return getSemanticsAsText();
		case McorePackage.MCLASSIFIER__DERIVED_JSON_SCHEMA:
			return getDerivedJsonSchema();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE:
			setSimpleType((SimpleType) newValue);
			return;
		case McorePackage.MCLASSIFIER__ABSTRACT_CLASS:
			setAbstractClass((Boolean) newValue);
			return;
		case McorePackage.MCLASSIFIER__ENFORCED_CLASS:
			setEnforcedClass((Boolean) newValue);
			return;
		case McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE:
			setSuperTypePackage((MPackage) newValue);
			return;
		case McorePackage.MCLASSIFIER__SUPER_TYPE:
			getSuperType().clear();
			getSuperType().addAll((Collection<? extends MClassifier>) newValue);
			return;
		case McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER:
			setInternalEClassifier((EClassifier) newValue);
			return;
		case McorePackage.MCLASSIFIER__LITERAL:
			getLiteral().clear();
			getLiteral().addAll((Collection<? extends MLiteral>) newValue);
			return;
		case McorePackage.MCLASSIFIER__EINTERFACE:
			setEInterface((Boolean) newValue);
			return;
		case McorePackage.MCLASSIFIER__TOGGLE_SUPER_TYPE:
			setToggleSuperType((MClassifier) newValue);
			return;
		case McorePackage.MCLASSIFIER__ORDERING_STRATEGY:
			setOrderingStrategy((OrderingStrategy) newValue);
			return;
		case McorePackage.MCLASSIFIER__ID_PROPERTY:
			getIdProperty().clear();
			getIdProperty().addAll((Collection<? extends MProperty>) newValue);
			return;
		case McorePackage.MCLASSIFIER__DO_ACTION:
			setDoAction((MClassifierAction) newValue);
			return;
		case McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT:
			setPropertiesAsText((String) newValue);
			return;
		case McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT:
			setSemanticsAsText((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE:
			unsetSimpleType();
			return;
		case McorePackage.MCLASSIFIER__ABSTRACT_CLASS:
			unsetAbstractClass();
			return;
		case McorePackage.MCLASSIFIER__ENFORCED_CLASS:
			unsetEnforcedClass();
			return;
		case McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE:
			unsetSuperTypePackage();
			return;
		case McorePackage.MCLASSIFIER__SUPER_TYPE:
			unsetSuperType();
			return;
		case McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER:
			unsetInternalEClassifier();
			return;
		case McorePackage.MCLASSIFIER__LITERAL:
			unsetLiteral();
			return;
		case McorePackage.MCLASSIFIER__EINTERFACE:
			unsetEInterface();
			return;
		case McorePackage.MCLASSIFIER__TOGGLE_SUPER_TYPE:
			setToggleSuperType((MClassifier) null);
			return;
		case McorePackage.MCLASSIFIER__ORDERING_STRATEGY:
			setOrderingStrategy(ORDERING_STRATEGY_EDEFAULT);
			return;
		case McorePackage.MCLASSIFIER__ID_PROPERTY:
			unsetIdProperty();
			return;
		case McorePackage.MCLASSIFIER__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		case McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT:
			unsetPropertiesAsText();
			return;
		case McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT:
			unsetSemanticsAsText();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE_STRING:
			return SIMPLE_TYPE_STRING_EDEFAULT == null
					? getSimpleTypeString() != null
					: !SIMPLE_TYPE_STRING_EDEFAULT
							.equals(getSimpleTypeString());
		case McorePackage.MCLASSIFIER__HAS_SIMPLE_DATA_TYPE:
			return HAS_SIMPLE_DATA_TYPE_EDEFAULT == null
					? getHasSimpleDataType() != null
					: !HAS_SIMPLE_DATA_TYPE_EDEFAULT
							.equals(getHasSimpleDataType());
		case McorePackage.MCLASSIFIER__HAS_SIMPLE_MODELING_TYPE:
			return HAS_SIMPLE_MODELING_TYPE_EDEFAULT == null
					? getHasSimpleModelingType() != null
					: !HAS_SIMPLE_MODELING_TYPE_EDEFAULT
							.equals(getHasSimpleModelingType());
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE_IS_CORRECT:
			return SIMPLE_TYPE_IS_CORRECT_EDEFAULT == null
					? getSimpleTypeIsCorrect() != null
					: !SIMPLE_TYPE_IS_CORRECT_EDEFAULT
							.equals(getSimpleTypeIsCorrect());
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE:
			return isSetSimpleType();
		case McorePackage.MCLASSIFIER__ABSTRACT_CLASS:
			return isSetAbstractClass();
		case McorePackage.MCLASSIFIER__ENFORCED_CLASS:
			return isSetEnforcedClass();
		case McorePackage.MCLASSIFIER__SUPER_TYPE_PACKAGE:
			return isSetSuperTypePackage();
		case McorePackage.MCLASSIFIER__SUPER_TYPE:
			return isSetSuperType();
		case McorePackage.MCLASSIFIER__INSTANCE:
			return !getInstance().isEmpty();
		case McorePackage.MCLASSIFIER__KIND:
			return getKind() != KIND_EDEFAULT;
		case McorePackage.MCLASSIFIER__INTERNAL_ECLASSIFIER:
			return isSetInternalEClassifier();
		case McorePackage.MCLASSIFIER__CONTAINING_PACKAGE:
			return basicGetContainingPackage() != null;
		case McorePackage.MCLASSIFIER__REPRESENTS_CORE_TYPE:
			return REPRESENTS_CORE_TYPE_EDEFAULT == null
					? getRepresentsCoreType() != null
					: !REPRESENTS_CORE_TYPE_EDEFAULT
							.equals(getRepresentsCoreType());
		case McorePackage.MCLASSIFIER__ALL_PROPERTY_GROUPS:
			return !getAllPropertyGroups().isEmpty();
		case McorePackage.MCLASSIFIER__LITERAL:
			return isSetLiteral();
		case McorePackage.MCLASSIFIER__EINTERFACE:
			return isSetEInterface();
		case McorePackage.MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR:
			return OPERATION_AS_ID_PROPERTY_ERROR_EDEFAULT == null
					? getOperationAsIdPropertyError() != null
					: !OPERATION_AS_ID_PROPERTY_ERROR_EDEFAULT
							.equals(getOperationAsIdPropertyError());
		case McorePackage.MCLASSIFIER__IS_CLASS_BY_STRUCTURE:
			return IS_CLASS_BY_STRUCTURE_EDEFAULT == null
					? getIsClassByStructure() != null
					: !IS_CLASS_BY_STRUCTURE_EDEFAULT
							.equals(getIsClassByStructure());
		case McorePackage.MCLASSIFIER__TOGGLE_SUPER_TYPE:
			return basicGetToggleSuperType() != null;
		case McorePackage.MCLASSIFIER__ORDERING_STRATEGY:
			return getOrderingStrategy() != ORDERING_STRATEGY_EDEFAULT;
		case McorePackage.MCLASSIFIER__TEST_ALL_PROPERTIES:
			return !getTestAllProperties().isEmpty();
		case McorePackage.MCLASSIFIER__ID_PROPERTY:
			return isSetIdProperty();
		case McorePackage.MCLASSIFIER__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case McorePackage.MCLASSIFIER__RESOLVE_CONTAINER_ACTION:
			return RESOLVE_CONTAINER_ACTION_EDEFAULT == null
					? getResolveContainerAction() != null
					: !RESOLVE_CONTAINER_ACTION_EDEFAULT
							.equals(getResolveContainerAction());
		case McorePackage.MCLASSIFIER__ABSTRACT_DO_ACTION:
			return ABSTRACT_DO_ACTION_EDEFAULT == null
					? getAbstractDoAction() != null
					: !ABSTRACT_DO_ACTION_EDEFAULT
							.equals(getAbstractDoAction());
		case McorePackage.MCLASSIFIER__OBJECT_REFERENCES:
			return !getObjectReferences().isEmpty();
		case McorePackage.MCLASSIFIER__NEAREST_INSTANCE:
			return !getNearestInstance().isEmpty();
		case McorePackage.MCLASSIFIER__STRICT_NEAREST_INSTANCE:
			return !getStrictNearestInstance().isEmpty();
		case McorePackage.MCLASSIFIER__STRICT_INSTANCE:
			return !getStrictInstance().isEmpty();
		case McorePackage.MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE:
			return !getIntelligentNearestInstance().isEmpty();
		case McorePackage.MCLASSIFIER__OBJECT_UNREFERENCED:
			return !getObjectUnreferenced().isEmpty();
		case McorePackage.MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS:
			return !getOutstandingToCopyObjects().isEmpty();
		case McorePackage.MCLASSIFIER__PROPERTY_INSTANCES:
			return !getPropertyInstances().isEmpty();
		case McorePackage.MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES:
			return !getObjectReferencePropertyInstances().isEmpty();
		case McorePackage.MCLASSIFIER__INTELLIGENT_INSTANCE:
			return !getIntelligentInstance().isEmpty();
		case McorePackage.MCLASSIFIER__CONTAINMENT_HIERARCHY:
			return !getContainmentHierarchy().isEmpty();
		case McorePackage.MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN:
			return !getStrictAllClassesContainedIn().isEmpty();
		case McorePackage.MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY:
			return !getStrictContainmentHierarchy().isEmpty();
		case McorePackage.MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN:
			return !getIntelligentAllClassesContainedIn().isEmpty();
		case McorePackage.MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY:
			return !getIntelligentContainmentHierarchy().isEmpty();
		case McorePackage.MCLASSIFIER__INSTANTIATION_HIERARCHY:
			return !getInstantiationHierarchy().isEmpty();
		case McorePackage.MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY:
			return !getInstantiationPropertyHierarchy().isEmpty();
		case McorePackage.MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY:
			return !getIntelligentInstantiationPropertyHierarchy().isEmpty();
		case McorePackage.MCLASSIFIER__CLASSESCONTAINEDIN:
			return !getClassescontainedin().isEmpty();
		case McorePackage.MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT:
			return CAN_APPLY_DEFAULT_CONTAINMENT_EDEFAULT == null
					? getCanApplyDefaultContainment() != null
					: !CAN_APPLY_DEFAULT_CONTAINMENT_EDEFAULT
							.equals(getCanApplyDefaultContainment());
		case McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT:
			return isSetPropertiesAsText();
		case McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT:
			return isSetSemanticsAsText();
		case McorePackage.MCLASSIFIER__DERIVED_JSON_SCHEMA:
			return DERIVED_JSON_SCHEMA_EDEFAULT == null
					? getDerivedJsonSchema() != null
					: !DERIVED_JSON_SCHEMA_EDEFAULT
							.equals(getDerivedJsonSchema());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MHasSimpleType.class) {
			switch (derivedFeatureID) {
			case McorePackage.MCLASSIFIER__SIMPLE_TYPE_STRING:
				return McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING;
			case McorePackage.MCLASSIFIER__HAS_SIMPLE_DATA_TYPE:
				return McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE;
			case McorePackage.MCLASSIFIER__HAS_SIMPLE_MODELING_TYPE:
				return McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE;
			case McorePackage.MCLASSIFIER__SIMPLE_TYPE_IS_CORRECT:
				return McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT;
			case McorePackage.MCLASSIFIER__SIMPLE_TYPE:
				return McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MHasSimpleType.class) {
			switch (baseFeatureID) {
			case McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING:
				return McorePackage.MCLASSIFIER__SIMPLE_TYPE_STRING;
			case McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE:
				return McorePackage.MCLASSIFIER__HAS_SIMPLE_DATA_TYPE;
			case McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE:
				return McorePackage.MCLASSIFIER__HAS_SIMPLE_MODELING_TYPE;
			case McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT:
				return McorePackage.MCLASSIFIER__SIMPLE_TYPE_IS_CORRECT;
			case McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE:
				return McorePackage.MCLASSIFIER__SIMPLE_TYPE;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MHasSimpleType.class) {
			switch (baseOperationID) {
			case McorePackage.MHAS_SIMPLE_TYPE___SIMPLE_TYPE_AS_STRING__SIMPLETYPE:
				return McorePackage.MCLASSIFIER___SIMPLE_TYPE_AS_STRING__SIMPLETYPE;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MCLASSIFIER___TOGGLE_SUPER_TYPE$_UPDATE__MCLASSIFIER:
			return toggleSuperType$Update((MClassifier) arguments.get(0));
		case McorePackage.MCLASSIFIER___ORDERING_STRATEGY$_UPDATE__ORDERINGSTRATEGY:
			return orderingStrategy$Update((OrderingStrategy) arguments.get(0));
		case McorePackage.MCLASSIFIER___DO_ACTION$_UPDATE__MCLASSIFIERACTION:
			return doAction$Update((MClassifierAction) arguments.get(0));
		case McorePackage.MCLASSIFIER___ALL_LITERALS:
			return allLiterals();
		case McorePackage.MCLASSIFIER___SELECTABLE_CLASSIFIER__ELIST_MPACKAGE:
			return selectableClassifier((EList<MClassifier>) arguments.get(0),
					(MPackage) arguments.get(1));
		case McorePackage.MCLASSIFIER___ALL_DIRECT_SUB_TYPES:
			return allDirectSubTypes();
		case McorePackage.MCLASSIFIER___ALL_SUB_TYPES:
			return allSubTypes();
		case McorePackage.MCLASSIFIER___SUPER_TYPES_AS_STRING:
			return superTypesAsString();
		case McorePackage.MCLASSIFIER___ALL_SUPER_TYPES:
			return allSuperTypes();
		case McorePackage.MCLASSIFIER___ALL_PROPERTIES:
			return allProperties();
		case McorePackage.MCLASSIFIER___ALL_SUPER_PROPERTIES:
			return allSuperProperties();
		case McorePackage.MCLASSIFIER___ALL_FEATURES:
			return allFeatures();
		case McorePackage.MCLASSIFIER___ALL_NON_CONTAINMENT_FEATURES:
			return allNonContainmentFeatures();
		case McorePackage.MCLASSIFIER___ALL_STORED_NON_CONTAINMENT_FEATURES:
			return allStoredNonContainmentFeatures();
		case McorePackage.MCLASSIFIER___ALL_FEATURES_WITH_STORAGE:
			return allFeaturesWithStorage();
		case McorePackage.MCLASSIFIER___ALL_CONTAINMENT_REFERENCES:
			return allContainmentReferences();
		case McorePackage.MCLASSIFIER___ALL_OPERATION_SIGNATURES:
			return allOperationSignatures();
		case McorePackage.MCLASSIFIER___ALL_CLASSES_CONTAINED_IN:
			return allClassesContainedIn();
		case McorePackage.MCLASSIFIER___DEFAULT_PROPERTY_VALUE:
			return defaultPropertyValue();
		case McorePackage.MCLASSIFIER___DEFAULT_PROPERTY_DESCRIPTION:
			return defaultPropertyDescription();
		case McorePackage.MCLASSIFIER___ALL_ID_PROPERTIES:
			return allIdProperties();
		case McorePackage.MCLASSIFIER___ALL_SUPER_ID_PROPERTIES:
			return allSuperIdProperties();
		case McorePackage.MCLASSIFIER___DO_ACTION_UPDATE__MCLASSIFIERACTION:
			return doActionUpdate((MClassifierAction) arguments.get(0));
		case McorePackage.MCLASSIFIER___INVOKE_SET_DO_ACTION__MCLASSIFIERACTION:
			return invokeSetDoAction((MClassifierAction) arguments.get(0));
		case McorePackage.MCLASSIFIER___INVOKE_TOGGLE_SUPER_TYPE__MCLASSIFIER:
			return invokeToggleSuperType((MClassifier) arguments.get(0));
		case McorePackage.MCLASSIFIER___DO_SUPER_TYPE_UPDATE__MCLASSIFIER:
			return doSuperTypeUpdate((MClassifier) arguments.get(0));
		case McorePackage.MCLASSIFIER___AMBIGUOUS_CLASSIFIER_NAME:
			return ambiguousClassifierName();
		case McorePackage.MCLASSIFIER___SIMPLE_TYPE_AS_STRING__SIMPLETYPE:
			return simpleTypeAsString((SimpleType) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (simpleType: ");
		if (simpleTypeESet)
			result.append(simpleType);
		else
			result.append("<unset>");
		result.append(", abstractClass: ");
		if (abstractClassESet)
			result.append(abstractClass);
		else
			result.append("<unset>");
		result.append(", enforcedClass: ");
		if (enforcedClassESet)
			result.append(enforcedClass);
		else
			result.append("<unset>");
		result.append(", eInterface: ");
		if (eInterfaceESet)
			result.append(eInterface);
		else
			result.append("<unset>");
		result.append(", propertiesAsText: ");
		if (propertiesAsTextESet)
			result.append(propertiesAsText);
		else
			result.append("<unset>");
		result.append(", semanticsAsText: ");
		if (semanticsAsTextESet)
			result.append(semanticsAsText);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL eName.concat( if self.superType->isEmpty() then '' else '->'.concat(
	 *      if superType->size() = 1 then superTypesAsString() else
	 *      '('.concat(superTypesAsString()).concat(')') endif ) endif )
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MCLASSIFIER;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL fullLabel (if kind=ClassifierKind::ClassType then '[class] ' else
	if kind=ClassifierKind::DataType then  '[data] ' else 
	if kind=ClassifierKind::Enumeration then '[enum]' else
	'[IMPOSSIBLE]'endif endif endif)
	.concat('localstructuralname')
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getFullLabel() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL kindLabel if self.kind = mcore::ClassifierKind::ClassType then
	if self.eInterface=true then 'Interface' else
	if self.abstractClass = true then 'Abstract Class' else
	   'Class' endif endif else
	if self.kind = mcore::ClassifierKind::DataType then 'Data Type' else
	if self.kind = mcore::ClassifierKind::Enumeration then 'Enumeration' else
	'ERROR' endif endif endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL correctName (not ambiguousClassifierName()) and
	if self.hasSimpleModelingType then self.name=self.simpleTypeAsString(simpleType) else  not self.stringEmpty(self.name) endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectName() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CORRECT_NAME;

		if (correctNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL containingClassifier self
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetContainingClassifier() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER;

		if (containingClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				containingClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL eName if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true
	then if hasSimpleModelingType then simpleTypeString else
	if stringEmpty(containingPackage.derivedNamePrefix)
	then calculatedShortName.camelCaseUpper() 
	else containingPackage.derivedNamePrefix.concat(calculatedShortName).camelCaseUpper()
	endif endif
	else specialEName.camelCaseUpper()
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL localStructuralName containingPackage.localStructuralName
	.concat('/')
	.concat(self.eName)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getLocalStructuralName() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__LOCAL_STRUCTURAL_NAME;

		if (localStructuralNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				localStructuralNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localStructuralNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedName name.concat(if ambiguousClassifierName() then '  (AMBIGUOUS)' else '' endif)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedName() {
		EClass eClass = (McorePackage.Literals.MCLASSIFIER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CALCULATED_NAME;

		if (calculatedNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE,
				McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY };
		return initializedFeatures;
	}

	private void createContainmentFeatureInResourceRootClass() {
		// Adding a new class initializes the standard property and adds a
		// containment feature to the rootclass
		if ((this.getContainingPackage() == null)
				|| (this.getContainingPackage()
						.getContainingComponent() == null)
				|| this.equals(
						this.getContainingPackage().getResourceRootClass())) {
			return;
		}

		EList<MPackage> packageList = this.getContainingPackage()
				.getContainingComponent().getOwnedPackage();
		MClassifier root = null;
		for (MPackage pack : packageList) {
			root = pack.getResourceRootClass();
			if (root != null) {
				break;
			}
		}
		if (root == null) {
			return;
		}

		MProperty newProb = McoreFactory.eINSTANCE.createMProperty();
		root.getProperty().add(newProb);

		newProb.setTypeDefinition(this);
		newProb.setPropertyBehavior(
				com.montages.mcore.PropertyBehavior.CONTAINED);
		newProb.setMultiplicityCase(
				com.montages.mcore.MultiplicityCase.ZERO_MANY);
	}

	/**
	 * @generated NOT
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		createContainmentFeatureInResourceRootClass();
		ensureClassInitializedGen(isLoadInProgress);
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitializedGen(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCLASSIFIER,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} // MClassifierImpl
