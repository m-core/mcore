/**
 */
package com.montages.mcore.impl;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;
import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPackageAction;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPackageAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MPackage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getGeneralAnnotation <em>General Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getSubPackage <em>Sub Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getPackageAnnotations <em>Package Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getUsePackageContentOnlyWithExplicitFilter <em>Use Package Content Only With Explicit Filter</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getResourceRootClass <em>Resource Root Class</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getSpecialNsURI <em>Special Ns URI</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getSpecialNsPrefix <em>Special Ns Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getSpecialFileName <em>Special File Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getUseUUID <em>Use UUID</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getUuidAttributeName <em>Uuid Attribute Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getContainingPackage <em>Containing Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getContainingComponent <em>Containing Component</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getRepresentsCorePackage <em>Represents Core Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getAllSubpackages <em>All Subpackages</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getQualifiedName <em>Qualified Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getIsRootPackage <em>Is Root Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getInternalEPackage <em>Internal EPackage</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDerivedNsURI <em>Derived Ns URI</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDerivedStandardNsURI <em>Derived Standard Ns URI</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDerivedNsPrefix <em>Derived Ns Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDerivedJavaPackageName <em>Derived Java Package Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getNamePrefix <em>Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDerivedNamePrefix <em>Derived Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getNamePrefixScope <em>Name Prefix Scope</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getDerivedJsonSchema <em>Derived Json Schema</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getGenerateJsonSchema <em>Generate Json Schema</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPackageImpl#getGeneratedJsonSchema <em>Generated Json Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MPackageImpl extends MNamedImpl
		implements MPackage, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getGeneralAnnotation() <em>General Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<MGeneralAnnotation> generalAnnotation;

	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EList<MClassifier> classifier;

	/**
	 * The cached value of the '{@link #getSubPackage() <em>Sub Package</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubPackage()
	 * @generated
	 * @ordered
	 */
	protected EList<MPackage> subPackage;

	/**
	 * The cached value of the '{@link #getPackageAnnotations() <em>Package Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageAnnotations()
	 * @generated
	 * @ordered
	 */
	protected MPackageAnnotations packageAnnotations;

	/**
	 * This is true if the Package Annotations containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean packageAnnotationsESet;

	/**
	 * The default value of the '{@link #getUsePackageContentOnlyWithExplicitFilter() <em>Use Package Content Only With Explicit Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsePackageContentOnlyWithExplicitFilter()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getUsePackageContentOnlyWithExplicitFilter() <em>Use Package Content Only With Explicit Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsePackageContentOnlyWithExplicitFilter()
	 * @generated
	 * @ordered
	 */
	protected Boolean usePackageContentOnlyWithExplicitFilter = USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER_EDEFAULT;

	/**
	 * This is true if the Use Package Content Only With Explicit Filter attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean usePackageContentOnlyWithExplicitFilterESet;

	/**
	 * The cached value of the '{@link #getResourceRootClass() <em>Resource Root Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceRootClass()
	 * @generated
	 * @ordered
	 */
	protected MClassifier resourceRootClass;

	/**
	 * This is true if the Resource Root Class reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean resourceRootClassESet;

	/**
	 * The default value of the '{@link #getSpecialNsURI() <em>Special Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialNsURI()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_NS_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialNsURI() <em>Special Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialNsURI()
	 * @generated
	 * @ordered
	 */
	protected String specialNsURI = SPECIAL_NS_URI_EDEFAULT;

	/**
	 * This is true if the Special Ns URI attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean specialNsURIESet;

	/**
	 * The default value of the '{@link #getSpecialNsPrefix() <em>Special Ns Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialNsPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_NS_PREFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialNsPrefix() <em>Special Ns Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialNsPrefix()
	 * @generated
	 * @ordered
	 */
	protected String specialNsPrefix = SPECIAL_NS_PREFIX_EDEFAULT;

	/**
	 * This is true if the Special Ns Prefix attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean specialNsPrefixESet;

	/**
	 * The default value of the '{@link #getSpecialFileName() <em>Special File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialFileName() <em>Special File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialFileName()
	 * @generated
	 * @ordered
	 */
	protected String specialFileName = SPECIAL_FILE_NAME_EDEFAULT;

	/**
	 * This is true if the Special File Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean specialFileNameESet;

	/**
	 * The default value of the '{@link #getUseUUID() <em>Use UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseUUID()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USE_UUID_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getUseUUID() <em>Use UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseUUID()
	 * @generated
	 * @ordered
	 */
	protected Boolean useUUID = USE_UUID_EDEFAULT;

	/**
	 * This is true if the Use UUID attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean useUUIDESet;

	/**
	 * The default value of the '{@link #getUuidAttributeName() <em>Uuid Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuidAttributeName()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_ATTRIBUTE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUuidAttributeName() <em>Uuid Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuidAttributeName()
	 * @generated
	 * @ordered
	 */
	protected String uuidAttributeName = UUID_ATTRIBUTE_NAME_EDEFAULT;

	/**
	 * This is true if the Uuid Attribute Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean uuidAttributeNameESet;

	/**
	 * The default value of the '{@link #getRepresentsCorePackage() <em>Represents Core Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentsCorePackage()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean REPRESENTS_CORE_PACKAGE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getQualifiedName() <em>Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsRootPackage() <em>Is Root Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsRootPackage()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_ROOT_PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInternalEPackage() <em>Internal EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalEPackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage internalEPackage;

	/**
	 * This is true if the Internal EPackage reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean internalEPackageESet;

	/**
	 * The default value of the '{@link #getDerivedNsURI() <em>Derived Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNsURI()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_NS_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedStandardNsURI() <em>Derived Standard Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedStandardNsURI()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_STANDARD_NS_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedNsPrefix() <em>Derived Ns Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNsPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_NS_PREFIX_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedJavaPackageName() <em>Derived Java Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJavaPackageName()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_JAVA_PACKAGE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MPackageAction DO_ACTION_EDEFAULT = MPackageAction.DO;

	/**
	 * The default value of the '{@link #getNamePrefix() <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_PREFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNamePrefix() <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefix()
	 * @generated
	 * @ordered
	 */
	protected String namePrefix = NAME_PREFIX_EDEFAULT;

	/**
	 * This is true if the Name Prefix attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean namePrefixESet;

	/**
	 * The default value of the '{@link #getDerivedNamePrefix() <em>Derived Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNamePrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_NAME_PREFIX_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedJsonSchema() <em>Derived Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJsonSchema()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_JSON_SCHEMA_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getGenerateJsonSchema() <em>Generate Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerateJsonSchema()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean GENERATE_JSON_SCHEMA_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getGeneratedJsonSchema() <em>Generated Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneratedJsonSchema()
	 * @generated
	 * @ordered
	 */
	protected static final String GENERATED_JSON_SCHEMA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGeneratedJsonSchema() <em>Generated Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneratedJsonSchema()
	 * @generated
	 * @ordered
	 */
	protected String generatedJsonSchema = GENERATED_JSON_SCHEMA_EDEFAULT;

	/**
	 * This is true if the Generated Json Schema attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean generatedJsonSchemaESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #generateJsonSchema$update01Object <em>Generate Json Schema$update01 Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #generateJsonSchema$update01Object
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression generateJsonSchema$update01ObjectecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #generateJsonSchema$update01Value <em>Generate Json Schema$update01 Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #generateJsonSchema$update01Value
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression generateJsonSchema$update01ValueecoreEBooleanObjectmcoreMPackageBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMPackageActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #generateJsonSchema$Update <em>Generate Json Schema$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #generateJsonSchema$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression generateJsonSchema$UpdateecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemcoreMPackageActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedNamePrefix <em>Derived Name Prefix</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNamePrefix
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedNamePrefixDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNamePrefixScope <em>Name Prefix Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefixScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression namePrefixScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedJsonSchema <em>Derived Json Schema</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJsonSchema
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedJsonSchemaDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getGenerateJsonSchema <em>Generate Json Schema</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerateJsonSchema
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression generateJsonSchemaDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getParent <em>Parent</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression parentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingPackage <em>Containing Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingComponent <em>Containing Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingComponent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getRepresentsCorePackage <em>Represents Core Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentsCorePackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression representsCorePackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllSubpackages <em>All Subpackages</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllSubpackages
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allSubpackagesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getQualifiedName <em>Qualified Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifiedName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression qualifiedNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsRootPackage <em>Is Root Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsRootPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isRootPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedNsURI <em>Derived Ns URI</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNsURI
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedNsURIDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedStandardNsURI <em>Derived Standard Ns URI</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedStandardNsURI
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedStandardNsURIDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedNsPrefix <em>Derived Ns Prefix</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNsPrefix
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedNsPrefixDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedJavaPackageName <em>Derived Java Package Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJavaPackageName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedJavaPackageNameDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getResourceRootClass <em>Resource Root Class</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceRootClass
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression resourceRootClassChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel <em>Full Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalStructuralName <em>Local Structural Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalStructuralName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression localStructuralNameDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MPACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralAnnotation> getGeneralAnnotation() {
		if (generalAnnotation == null) {
			generalAnnotation = new EObjectContainmentEList.Unsettable.Resolving<MGeneralAnnotation>(
					MGeneralAnnotation.class, this,
					McorePackage.MPACKAGE__GENERAL_ANNOTATION);
		}
		return generalAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralAnnotation() {
		if (generalAnnotation != null)
			((InternalEList.Unsettable<?>) generalAnnotation).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralAnnotation() {
		return generalAnnotation != null
				&& ((InternalEList.Unsettable<?>) generalAnnotation).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackageAction getDoAction() {
		/**
		 * @OCL mcore::MPackageAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackageAction result = (MPackageAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MPackageAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MPackageAction.CLASS_VALUE:
			int nrOfClasses = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.CLASS_TYPE) {
					nrOfClasses += 1;
				}
			}
			MClassifier newClassifier = McoreFactory.eINSTANCE
					.createMClassifier();
			this.getClassifier().add(newClassifier);
			//Classes start counting at 0, as Class 0 is the root class.....
			newClassifier
					.setName("Class ".concat(Integer.toString(nrOfClasses)));
			if (this.getIsRootPackage()
					&& this.getResourceRootClass() == null) {
				this.setResourceRootClass(newClassifier);
			}
			newClassifier.setEnforcedClass(true);
			break;
		case MPackageAction.ENUMERATION_VALUE:
			int nrOfEnum = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.ENUMERATION) {
					nrOfEnum += 1;
				}
			}
			MClassifier newEnumeration = McoreFactory.eINSTANCE
					.createMClassifier();
			newEnumeration.getProperty().clear();
			newEnumeration.getLiteral()
					.add(McoreFactory.eINSTANCE.createMLiteral());
			MClassifier rootClass = getResourceRootClass();
			if (rootClass == null && !getClassifier().isEmpty()) {
				getClassifier().get(0);
			}
			if (rootClass != null) {
				//remove default containment for newEnumeration
				for (Iterator iterator = rootClass.getOwnedProperty()
						.iterator(); iterator.hasNext();) {
					MProperty p = (MProperty) iterator.next();
					if (p.getContainment() && p.getType() == newEnumeration) {
						rootClass.getOwnedProperty().remove(p);
						break;
					}
				}
			}
			this.getClassifier().add(newEnumeration);
			newEnumeration
					.setName("Enum ".concat(Integer.toString(nrOfEnum + 1)));
			break;
		case MPackageAction.DATA_TYPE_VALUE:
			int nrOfDatatype = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.DATA_TYPE) {
					nrOfDatatype += 1;
				}
			}
			MClassifier newDatatype = McoreFactory.eINSTANCE
					.createMClassifier();
			this.getClassifier().add(newDatatype);
			newDatatype.getProperty().clear();
			newDatatype.setSimpleType(SimpleType.STRING);
			newDatatype.setName(
					"Datatype ".concat(Integer.toString(nrOfDatatype + 1)));
			break;
		case MPackageAction.SUB_PACKAGE_VALUE:
			getSubPackage().add(McoreFactory.eINSTANCE.createMPackage());
			break;
		case MPackageAction.GENERATE_PROPERTY_GROUPS_VALUE:
			ArrayList<MPackage> allPacks = new ArrayList<MPackage>();
			allPacks.add(this);
			allPacks.addAll(this.getAllSubpackages());

			for (MPackage mPack : allPacks) {
				for (MClassifier mClass : mPack.getClassifier()) {
					ArrayList<MProperty> propInNoGroup = new ArrayList<MProperty>();

					// Check all owned properties, which are not in a PropertyGroup
					for (MProperty property : mClass.getOwnedProperty()) {
						if (property.eContainer() != null && property
								.eContainer() instanceof MClassifier) {
							propInNoGroup.add(property);
						}
					}

					MComponent comp = mClass.getContainingPackage()
							.getContainingComponent();
					MPropertiesGroup group = McoreFactory.eINSTANCE
							.createMPropertiesGroup();
					group.setName(mClass.getEName());
					group.getProperty().addAll(propInNoGroup);
					MPropertiesGroup abstractGroup = null;
					if (comp.isSetAbstractComponent()
							&& comp.getAbstractComponent()) {
						abstractGroup = McoreFactory.eINSTANCE
								.createMPropertiesGroup();
						abstractGroup.setName("X" + comp.getName());
						abstractGroup.getPropertyGroup().add(0, group);
						mClass.getPropertyGroup().add(abstractGroup);
					} else
						mClass.getPropertyGroup().add(0, group);
				}
			}

			for (MPackage mPack : allPacks) {
				for (MClassifier mClass : mPack.getClassifier()) {
					if (mClass.getAnnotations() == null)
						continue;

					List<MPropertyAnnotations> annotationList = new ArrayList<MPropertyAnnotations>();
					Iterator<MPropertyAnnotations> itr = mClass.getAnnotations()
							.getPropertyAnnotations().iterator();
					while (itr.hasNext()) {
						MPropertyAnnotations element = itr.next();
						annotationList.add(element);
						itr.remove();
					}

					for (MPropertyAnnotations annotation : annotationList) {
						if (annotation.getProperty() != null
								&& annotation.getProperty()
										.getContainingClassifier() != mClass) {
							if (annotation.getProperty()
									.getContainingClassifier() != null
									&& annotation.getProperty()
											.getContainingClassifier()
											.getContainingPackage() != null) {
								MComponent containingComponent = annotation
										.getProperty().getContainingClassifier()
										.getContainingPackage()
										.getContainingComponent();

								if (containingComponent == mPack
										.getContainingComponent()
										&& containingComponent
												.isSetAbstractComponent()
										&& !containingComponent
												.getAbstractComponent()) {
									MPropertiesGroup classSpecificPropertyGroup = null;

									//we override from another class
									if (!mClass.getOwnedProperty().contains(
											annotation.getProperty())) {
										for (MPropertiesGroup mPropertyGroup : mClass
												.getPropertyGroup())
											if (mPropertyGroup.getName()
													.equals(annotation
															.getProperty()
															.getContainingClassifier()
															.getName()))
												classSpecificPropertyGroup = mPropertyGroup;
									}
									if (classSpecificPropertyGroup == null) {
										classSpecificPropertyGroup = McoreFactory.eINSTANCE
												.createMPropertiesGroup();
										classSpecificPropertyGroup.setName(
												annotation.getProperty()
														.getContainingClassifier()
														.getName());
										classSpecificPropertyGroup
												.setAnnotations(
														AnnotationsFactory.eINSTANCE
																.createMClassifierAnnotations());

									}
									classSpecificPropertyGroup.getAnnotations()
											.getPropertyAnnotations()
											.add(annotation);
									mClass.getPropertyGroup()
											.add(classSpecificPropertyGroup);
								}

								if (containingComponent != mPack
										.getContainingComponent()
										|| containingComponent
												.isSetAbstractComponent()
												&& containingComponent
														.getAbstractComponent()) {
									MPropertiesGroup componentPropertyGroup = null;
									MPropertiesGroup classPropertyGroup = null;
									MClassifier containerClass = annotation
											.getProperty()
											.getContainingClassifier();
									for (MPropertiesGroup compGroup : mClass
											.getPropertyGroup()) {
										if (compGroup.getName().equals(
												"X" + containingComponent
														.getName())) {
											componentPropertyGroup = compGroup;

											for (MPropertiesGroup propertyGroup : componentPropertyGroup
													.getPropertyGroup())
												if (propertyGroup.getName()
														.equals(containerClass
																.getEName()))
													classPropertyGroup = propertyGroup;
										}
									}
									if (componentPropertyGroup == null) {
										componentPropertyGroup = McoreFactory.eINSTANCE
												.createMPropertiesGroup();
										componentPropertyGroup.setName(
												"X" + containingComponent
														.getName());
									}

									if (classPropertyGroup == null) {
										classPropertyGroup = McoreFactory.eINSTANCE
												.createMPropertiesGroup();
										classPropertyGroup.setName(
												containerClass.getEName());
										componentPropertyGroup
												.getPropertyGroup()
												.add(classPropertyGroup);
										classPropertyGroup.setAnnotations(
												AnnotationsFactory.eINSTANCE
														.createMClassifierAnnotations());
									}
									classPropertyGroup.getAnnotations()
											.getPropertyAnnotations()
											.add(annotation);

									if (!mClass.getPropertyGroup()
											.contains(componentPropertyGroup))
										mClass.getPropertyGroup()
												.add(componentPropertyGroup);

								}
							}
						} else {
							MPropertiesGroup classSpecificPropertyGroup = mClass
									.getPropertyGroup().get(0);
							if (classSpecificPropertyGroup
									.getAnnotations() == null)
								classSpecificPropertyGroup.setAnnotations(
										AnnotationsFactory.eINSTANCE
												.createMClassifierAnnotations());
							classSpecificPropertyGroup.getAnnotations()
									.getPropertyAnnotations().add(annotation);
						}
					}

					/*
					
					// Add allOwnedProperties, which are not in PropertyGroup to one
					initialPropGroup = McoreFactory.eINSTANCE.createMPropertiesGroup();
					initialPropGroup.setName(mClass.getName());
					initialPropGroup.getProperty().addAll(propInNoGroup);
					mClass.getPropertyGroup().add(0,initialPropGroup);
					classPropGroup = initialPropGroup;
					
					
					
					//TODO Add later!
					
					// Divide the annotations among owned ones on one side and overriden on the other
					if(mClass.getAnnotations() != null)
					for(MPropertyAnnotations ann: mClass.getAnnotations().getPropertyAnnotations())
					{
						if (ann.getOverriding() == true)
						{
							if ( ann.getProperty() != null)
							{
								MClassifier containingClass = ann.getProperty().getContainingClassifier();
								MPackage  containingPackage = containingClass.getContainingPackage();
								if(containingPackage == null)
									break;
								MComponent containingComponent = containingPackage.getContainingComponent();
								boolean propGroupExists = false;
								boolean compGroupExists = false;
								// If the Annotation is in the same component
								if(containingComponent == this.getContainingComponent())
									// Check if we already have an own MProp group
								{
							        
									if(!mClass.getPropertyGroup().isEmpty())
									for (MPropertiesGroup propertyGroup : mClass.getPropertyGroup())
							        	   if(propertyGroup.getName().equals(containingClass.getName()))
							        	   {
							        		   propGroupExists = true;
							        		  if (propertyGroup.getAnnotations() == null)
							        			  propertyGroup.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
							        		  //TODO: FIX HERE
							        		  existingPropertyGroup = propertyGroup;
							        		  initialPropGroupAnnotations.add(ann);
							        	   }
							         if(mClass.getPropertyGroup().isEmpty() || !propGroupExists)
							         {
							        	annotationsGroup = McoreFactory.eINSTANCE.createMPropertiesGroup();
							        	annotationsGroup.setName(containingClass.getName());
							        	annotationsGroup.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
							        	annotationsGroup.getAnnotations().getPropertyAnnotations().add(EcoreUtil.copy(ann));
							        	initialPropGroupAnnotationsDelete.add(ann);
							        	//TODO: ADD LATER
							        	//mClass.getPropertyGroup().add(annotationsGroup);
							        	
							         }
							     
							        	 
								} else if (containingComponent != this.getContainingComponent())
								{
									//We have an external Component
								
									if(!mClass.getPropertyGroup().isEmpty())
										for (MPropertiesGroup compGroup : mClass.getPropertyGroup())
								        	   if(compGroup.getName().matches(containingComponent.getName()))
								        	   {
								        		   compGroupExists = true;
								        		   componentGroup = compGroup;
								        			for (MPropertiesGroup propertyGroup : mClass.getPropertyGroup())
											        	   if(propertyGroup.getName().equals(containingClass.getName()))
											        	   {
											        		   propGroupExists = true;
											        		  if (propertyGroup.getAnnotations() == null)
											        			  propertyGroup.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
											        		  
											        		  existingPropertyGroup = propertyGroup;
											        		  initialPropGroupAnnotations.add(ann);
											        		  //TODO: FIX
											        		  //propertyGroup.getAnnotations().getPropertyAnnotations().add(ann);
											        	   }
								        		if(!propGroupExists)
								        		{
								        			classPropGroup = McoreFactory.eINSTANCE.createMPropertiesGroup();
								        			classPropGroup.setName(containingClass.getName());
								        		  
								        			 classPropGroup.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
								        		  classPropGroup.getAnnotations().getPropertyAnnotations().add(EcoreUtil.copy(ann));
								        		 // componentGroup.getPropertyGroup().add(classPropGroup);
								        		}
								        	   }
								      if(mClass.getPropertyGroup().isEmpty() || !compGroupExists)
								         {
								    	  
								    	   if(!compGroupExists){
								    	    componentGroup = McoreFactory.eINSTANCE.createMPropertiesGroup();
								    	    String componentGroupName = containingComponent.getName();
								    	    if (containingComponent.getAbstractComponent() == true)
								    	    	componentGroupName = "X".concat(componentGroupName);
								    	    componentGroup.setName(componentGroupName);
								    	   }
								    	    
								    	    if(!propGroupExists)
								    	    {
								        	annotationsGroup = McoreFactory.eINSTANCE.createMPropertiesGroup();
								        	componentGroup.getPropertyGroup().add(annotationsGroup);
								        	annotationsGroup.setName(ann.getProperty().getContainingClassifier().getEName());
								        	annotationsGroup.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
								    	    }
								    	    else
								    	    annotationsGroup = existingPropertyGroup;
								    	    annotationsGroup.getAnnotations().getPropertyAnnotations().add(EcoreUtil.copy(ann));
								         }
									
									
								}
							}
						}
						else
							{
							if (classPropGroup.getAnnotations() == null)
								classPropGroup.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
							
							
							classPropGroup.getAnnotations().getPropertyAnnotations().add(EcoreUtil.copy(ann));
								
							}
					
						
					}
					mClass.unsetAnnotations();
					
					
					
					if(existingPropertyGroup != null)
					existingPropertyGroup.getAnnotations().getPropertyAnnotations().addAll(initialPropGroupAnnotations);
					if(componentGroup == null)
					{
					   if(annotationsGroup != null)
					   {
					   mClass.getPropertyGroup().add(0,annotationsGroup);
					   }
					   
					} else
					 mClass.getPropertyGroup().add(mClass.getPropertyGroup().size(),componentGroup);
					//ADD annotations group , add component group
					
					*/
					if (mClass.getAnnotations() != null
							&& mClass.getAnnotations().getPropertyAnnotations()
									.isEmpty())
						mClass.unsetAnnotations();
				}

			}

			break;

		case MPackageAction.REMOVE_PROPERTY_GROUPS_VALUE:
			ArrayList<MPackage> allSubPacks = new ArrayList<MPackage>();
			allSubPacks.add(this);
			allSubPacks.addAll(this.getAllSubpackages());

			for (MPackage mPack : allSubPacks) {
				for (MClassifier mClass : mPack.getClassifier()) {
					MClassifierAnnotations classAnno = mClass.getAnnotations();

					for (MPropertiesGroup propGroup : mClass
							.getAllPropertyGroups()) {
						mClass.getProperty().addAll(propGroup.getProperty());
						if (propGroup.getAnnotations() != null) {
							if (classAnno == null) {
								classAnno = AnnotationsFactory.eINSTANCE
										.createMClassifierAnnotations();
								mClass.setAnnotations(classAnno);
							}
							classAnno.getPropertyAnnotations()
									.addAll(propGroup.allPropertyAnnotations());
							classAnno.getOperationAnnotations().addAll(
									propGroup.allOperationAnnotations());
						}
					}
					mClass.getPropertyGroup().clear();
				}
			}

			break;

		case MPackageAction.COMPLETE_SEMANTICS_VALUE:
			ArrayList<MPackage> packs = new ArrayList<MPackage>();
			packs.add(this);
			packs.addAll(this.getAllSubpackages());

			for (MPackage mPack : packs) {
				for (MClassifier mClass : mPack.getClassifier()) {
					if (!mClass.allSuperProperties().isEmpty()) {
						//So we can check if the property of the super class already has a property annotation in the class
						ArrayList<MProperty> allPropsWithAnno = new ArrayList<MProperty>();
						for (MPropertyAnnotations propAnno : mClass
								.allPropertyAnnotations()) {
							allPropsWithAnno.add(propAnno.getProperty());
						}
						//So we can check all operation signatures with an annotation
						ArrayList<MOperationSignature> allOpsWithAnno = new ArrayList<MOperationSignature>();
						for (MOperationAnnotations opAnno : mClass
								.allOperationAnnotations()) {
							allOpsWithAnno.add(opAnno.getOperationSignature());
						}
						MClassifierAnnotations classAnno = mClass
								.getAnnotations();
						if (classAnno == null) {
							classAnno = AnnotationsFactory.eINSTANCE
									.createMClassifierAnnotations();
							mClass.setAnnotations(classAnno);
						}

						for (MProperty superClassProp : mClass
								.allSuperProperties()) {
							//is it a property or an operation
							if (superClassProp.getIsOperation()) {
								for (MOperationSignature opSig : superClassProp
										.getOperationSignature()) {
									//there is no operation annotation in the class
									if (!allOpsWithAnno.contains(opSig)) {
										MOperationAnnotations newOpAnno = AnnotationsFactory.eINSTANCE
												.createMOperationAnnotations();
										newOpAnno.setOperationSignature(opSig);
										MResult newResult = AnnotationsFactory.eINSTANCE
												.createMResult();
										newResult.setBase(
												ExpressionBase.SELF_OBJECT);
										newOpAnno.setResult(newResult);
										classAnno.getOperationAnnotations()
												.add(newOpAnno);
									}
								}
							} else {
								//Property annotation doesn't exist 
								if (!allPropsWithAnno
										.contains(superClassProp)) {
									MPropertyAnnotations newPropAnno = AnnotationsFactory.eINSTANCE
											.createMPropertyAnnotations();
									newPropAnno.setProperty(superClassProp);

									switch (superClassProp.getPropertyBehavior()
											.getValue()) {
									case PropertyBehavior.STORED_VALUE:
										MInitializationValue newInit = AnnotationsFactory.eINSTANCE
												.createMInitializationValue();
										newInit.setBase(
												ExpressionBase.SELF_OBJECT);
										newPropAnno.setInitValue(newInit);
										break;
									case PropertyBehavior.DERIVED_VALUE:
										MResult newResult = AnnotationsFactory.eINSTANCE
												.createMResult();
										newResult.setBase(
												ExpressionBase.SELF_OBJECT);
										newPropAnno.setResult(newResult);
										break;
									default:
										break;
									}
									classAnno.getPropertyAnnotations()
											.add(newPropAnno);
								}
							}
						}
					}
				}
			}
			break;

		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MPackageAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MPackageAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMPackage_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate generateJsonSchema$Update(Boolean trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		java.lang.Boolean triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMPackage_GenerateJsonSchema(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		for (MPackage mPackage : generateJsonSchema$update01Object(trg)) {

			currentTrigger.addAttributeUpdate(mPackage,
					McorePackage.eINSTANCE.getMPackage_GeneratedJsonSchema(),
					org.xocl.semantics.XUpdateMode.REDEFINE, null,
					generateJsonSchema$update01Value(trg, mPackage), null, null,
					null, null, null, null);

		}

		return currentTrigger;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MPackageAction trg) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		switch (trg.getValue()) {
		case MPackageAction.CLASS_VALUE: {
			int nrOfClasses = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.CLASS_TYPE) {
					nrOfClasses += 1;
				}
			}
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction()
					.getEEnumLiteral(MPackageAction.CLASS_VALUE);

			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			MClassifier newClass = (MClassifier) currentTrigger
					.createNextStateNewObject(
							McorePackage.eINSTANCE.getMClassifier());
			currentTrigger.addReferenceUpdate(this,
					McorePackage.Literals.MPACKAGE__CLASSIFIER, XUpdateMode.ADD,
					XAddUpdateMode.LAST, newClass, null, null);

			currentTrigger.addAttributeUpdate(newClass,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null, "Class ".concat(Integer.toString(nrOfClasses)), null,
					null);

			if (this.getIsRootPackage() && this.getResourceRootClass() == null)
				currentTrigger.addReferenceUpdate(this,
						McorePackage.Literals.MPACKAGE__RESOURCE_ROOT_CLASS,
						XUpdateMode.REDEFINE, null, newClass, null, null);

			if (this.getResourceRootClass() != null) {
				EObject prop = currentTrigger.createNextStateNewObject(
						McorePackage.Literals.MPROPERTY);

				currentTrigger.addReferenceUpdate(this.getResourceRootClass(),
						McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY,
						XUpdateMode.ADD, XAddUpdateMode.LAST, prop, null, null);

				currentTrigger.addAttributeUpdate(prop,
						McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE,
						XUpdateMode.REDEFINE,
						null, McorePackage.eINSTANCE.getSimpleType()
								.getEEnumLiteral(SimpleType.NONE_VALUE),
						null, null);

				MProperty mProperty = (MProperty) prop;

				currentTrigger.addReferenceUpdate(prop,
						McorePackage.Literals.MEXPLICITLY_TYPED__TYPE,
						XUpdateMode.REDEFINE, null, newClass, null, null);

				currentTrigger = currentTrigger.addAndMergeUpdate(
						mProperty.doActionUpdate(MPropertyAction.INTO_NARY));

				EList<Object> s = new BasicEList<Object>();
				s.add(PropertyBehavior.CONTAINED);

				currentTrigger.invokeOperationCall(prop,
						McorePackage.Literals.MPROPERTY___INVOKE_SET_PROPERTY_BEHAVIOUR__PROPERTYBEHAVIOR,
						s, null, null);

				currentTrigger.getContainingTransition().getFocusObjects()
						.add(currentTrigger.nextStateObjectDefinitionFromObject(
								newClass.getProperty().get(0)));
			}

			return currentTrigger;

		}
		case MPackageAction.ABSTRACT_CLASS_VALUE: {
			int nrOfClasses = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.CLASS_TYPE) {
					nrOfClasses += 1;
				}
			}
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction()
					.getEEnumLiteral(MPackageAction.CLASS_VALUE);

			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			MClassifier newClass = (MClassifier) currentTrigger
					.createNextStateNewObject(
							McorePackage.eINSTANCE.getMClassifier());
			currentTrigger.addReferenceUpdate(this,
					McorePackage.Literals.MPACKAGE__CLASSIFIER, XUpdateMode.ADD,
					XAddUpdateMode.LAST, newClass, null, null);

			String className = "Abstract Class "
					.concat(Integer.toString(nrOfClasses));

			currentTrigger.addAttributeUpdate(newClass,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null, className, null, null);
			currentTrigger.addAttributeUpdate(newClass,
					McorePackage.Literals.MCLASSIFIER__ABSTRACT_CLASS,
					XUpdateMode.REDEFINE, null, true, null, null);

			if (this.getIsRootPackage() && this.getResourceRootClass() == null)
				currentTrigger.addReferenceUpdate(this,
						McorePackage.Literals.MPACKAGE__RESOURCE_ROOT_CLASS,
						XUpdateMode.REDEFINE, null, newClass, null, null);

			if (this.getResourceRootClass() != null) {
				MProperty newDefaultContainmentProp = (MProperty) currentTrigger
						.createNextStateNewObject(
								McorePackage.eINSTANCE.getMProperty());

				currentTrigger.addAttributeUpdate(newDefaultContainmentProp,
						McorePackage.Literals.MPROPERTY__CONTAINMENT,
						XUpdateMode.REDEFINE, null, true, null, null);

				currentTrigger.addReferenceUpdate(newDefaultContainmentProp,
						McorePackage.Literals.MEXPLICITLY_TYPED__TYPE,
						XUpdateMode.REDEFINE, null, newClass, null, null);

				// Have to do manually because of derived changeable feat.
				//currentTrigger.addAttributeUpdate(newDefaultContainmentProp,
				//		McorePackage.Literals.MTYPED__MULTIPLICITY_CASE,
				//		XUpdateMode.REDEFINE, null,  MultiplicityCase.ZERO_MANY, null, null);
				newDefaultContainmentProp
						.setMultiplicityCase(MultiplicityCase.ZERO_MANY);

				// Have to reset simpletype manually
				newDefaultContainmentProp.setSimpleType(SimpleType.NONE);
				currentTrigger.addAttributeUpdate(newDefaultContainmentProp,
						McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE,
						XUpdateMode.REDEFINE, null, SimpleType.NONE, null,
						null);

				currentTrigger.addAttributeUpdate(newDefaultContainmentProp,
						McorePackage.Literals.MNAMED__NAME,
						XUpdateMode.REDEFINE, null, className, null, null);

				currentTrigger.addReferenceUpdate(getResourceRootClass(),
						McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY,
						XUpdateMode.ADD, XAddUpdateMode.FIRST,
						newDefaultContainmentProp, null, null);

			}

			//To avoid potential specialize or super type problems
			newClass.getProperty().get(0).setName("Abstract Text 1");

			currentTrigger.getContainingTransition().getFocusObjects()
					.add(currentTrigger.nextStateObjectDefinitionFromObject(
							newClass.getProperty().get(0)));

			return currentTrigger;

		}
		case MPackageAction.DATA_TYPE_VALUE: {
			int nrOfDatatype = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.DATA_TYPE) {
					nrOfDatatype += 1;
				}
			}
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction()
					.getEEnumLiteral(MPackageAction.DATA_TYPE_VALUE);

			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			MClassifier newDataType = (MClassifier) currentTrigger
					.createNextStateNewObject(
							McorePackage.eINSTANCE.getMClassifier());
			currentTrigger.addReferenceUpdate(this,
					McorePackage.Literals.MPACKAGE__CLASSIFIER, XUpdateMode.ADD,
					XAddUpdateMode.LAST, newDataType, null, null);

			currentTrigger.addAttributeUpdate(newDataType,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null, "Datatype ".concat(Integer.toString(nrOfDatatype)),
					null, null);

			currentTrigger.addReferenceUpdate(newDataType,
					McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY,
					XUpdateMode.REMOVE, null, newDataType.getProperty().get(0),
					null, null);

			EEnumLiteral simpleTypeLiteral = McorePackage.eINSTANCE
					.getSimpleType().getEEnumLiteral(SimpleType.STRING_VALUE);
			currentTrigger.addAttributeUpdate(newDataType,
					McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE,
					XUpdateMode.REDEFINE, null, simpleTypeLiteral, null, null);

			return currentTrigger;
		}
		case MPackageAction.ENUMERATION_VALUE: {
			int nrOfDatatype = 0;
			for (MClassifier mClassifier : this.getClassifier()) {
				if (mClassifier.getKind() == ClassifierKind.ENUMERATION) {
					nrOfDatatype += 1;
				}
			}
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction()
					.getEEnumLiteral(MPackageAction.DATA_TYPE_VALUE);

			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			MClassifier newDataType = (MClassifier) currentTrigger
					.createNextStateNewObject(
							McorePackage.eINSTANCE.getMClassifier());
			currentTrigger.addReferenceUpdate(this,
					McorePackage.Literals.MPACKAGE__CLASSIFIER, XUpdateMode.ADD,
					XAddUpdateMode.LAST, newDataType, null, null);

			currentTrigger.addReferenceUpdate(newDataType,
					McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY,
					XUpdateMode.REMOVE, null, newDataType.getProperty().get(0),
					null, null);

			currentTrigger.addAttributeUpdate(newDataType,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null, "Enum ".concat(Integer.toString(nrOfDatatype)), null,
					null);

			//TODO: reusing another action does not work right now, since we need to add +Literal Action and use them 
			/*currentTrigger = currentTrigger.addAndMergeUpdate(
					newDataType.doActionUpdate(MClassifierAction.LITERAL));
			*/
			EObject literalValue = currentTrigger
					.createNextStateNewObject(McorePackage.Literals.MLITERAL);

			currentTrigger.addReferenceUpdate(newDataType,
					McorePackage.Literals.MCLASSIFIER__LITERAL,
					XUpdateMode.REDEFINE, null, literalValue, null, null);

			currentTrigger.addAttributeUpdate(literalValue,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null, "Literal1", null, null);

			//	transition.getFocusObjects().add(transition
			//		.nextStateObjectDefinitionFromObject(literalValue));

			return currentTrigger;
		}
		case MPackageAction.SUB_PACKAGE_VALUE: {
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction()
					.getEEnumLiteral(MPackageAction.SUB_PACKAGE_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			MPackage newSubPack = (MPackage) currentTrigger
					.createNextStateNewObject(
							McorePackage.eINSTANCE.getMPackage());
			currentTrigger.addReferenceUpdate(this,
					McorePackage.Literals.MPACKAGE__SUB_PACKAGE,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newSubPack, null,
					null);

			currentTrigger.addAttributeUpdate(newSubPack,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null,
					"SubPackage".concat(
							Integer.toString(this.getSubPackage().size())),
					null, null);

			return currentTrigger;
		}
		//DISABLE UPDATE ANNOTATION
		case MPackageAction.GENERATE_PROPERTY_GROUPS_VALUE: {
			if (true)
				return null;
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction().getEEnumLiteral(
							MPackageAction.GENERATE_PROPERTY_GROUPS_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			ArrayList<MPackage> allPacks = new ArrayList<MPackage>();
			allPacks.add(this);
			allPacks.addAll(this.getAllSubpackages());

			for (MPackage mPack : allPacks) {
				for (MClassifier mClass : mPack.getClassifier()) {
					ArrayList<MProperty> propInNoGroup = new ArrayList<MProperty>();

					//Saves all owned properties of a class, which are no operation and in no property group 
					for (MProperty mProp : mClass.getOwnedProperty()) {
						if (mProp.getContainingPropertiesContainer()
								.equals(mClass)
								&& !mProp.getPropertyBehavior().equals(
										PropertyBehavior.IS_OPERATION)) {
							propInNoGroup.add(mProp);
						}
					}
					if (!propInNoGroup.isEmpty()) {
						//Creates Component group
						MPropertiesGroup newPropGroup = (MPropertiesGroup) currentTrigger
								.createNextStateNewObject(McorePackage.eINSTANCE
										.getMPropertiesGroup());
						//Takes care for the Name Prefix
						if (mPack.getDerivedNamePrefix() != null && !mPack
								.getContainingComponent().getName()
								.startsWith(mPack.getDerivedNamePrefix())) {

							newPropGroup.setName(
									mPack.getDerivedNamePrefix().concat(" ")
											.concat(this
													.getContainingComponent()
													.getName()));

						} else
							newPropGroup.setName(
									mPack.getContainingComponent().getName());

						currentTrigger.addReferenceUpdate(mClass,
								McorePackage.eINSTANCE
										.getMPropertiesContainer_PropertyGroup(),
								XUpdateMode.ADD, XAddUpdateMode.LAST,
								newPropGroup, null, null);
						MPropertiesGroup nestedPropGroup = (MPropertiesGroup) currentTrigger
								.createNextStateNewObject(McorePackage.eINSTANCE
										.getMPropertiesGroup());
						nestedPropGroup.setName(mClass.getName());

						currentTrigger.addReferenceUpdate(newPropGroup,
								McorePackage.eINSTANCE
										.getMPropertiesContainer_PropertyGroup(),
								XUpdateMode.ADD, XAddUpdateMode.LAST,
								nestedPropGroup, null, null);

						MClassifierAnnotations groupAnno = (MClassifierAnnotations) currentTrigger
								.createNextStateNewObject(
										AnnotationsPackage.eINSTANCE
												.getMClassifierAnnotations());

						for (MProperty mProp : propInNoGroup) {
							currentTrigger.addReferenceUpdate(nestedPropGroup,
									McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY,
									XUpdateMode.ADD, XAddUpdateMode.LAST, mProp,
									null, null);

							if (mProp.getDirectSemantics() != null) {
								currentTrigger.addReferenceUpdate(groupAnno,
										AnnotationsPackage.eINSTANCE
												.getMClassifierAnnotations_PropertyAnnotations(),
										XUpdateMode.ADD, XAddUpdateMode.LAST,
										mProp.getDirectSemantics(), null, null);
							}
						}
						currentTrigger.addReferenceUpdate(nestedPropGroup,
								McorePackage.eINSTANCE
										.getMPropertiesContainer_Annotations(),
								XUpdateMode.REDEFINE, null, groupAnno, null,
								null);
					}
				}
			}
			return currentTrigger;
		}

		case MPackageAction.REMOVE_PROPERTY_GROUPS_VALUE: {
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction().getEEnumLiteral(
							MPackageAction.REMOVE_PROPERTY_GROUPS_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			ArrayList<MPackage> allSubPacks = new ArrayList<MPackage>();
			allSubPacks.add(this);
			allSubPacks.addAll(this.getAllSubpackages());

			for (MPackage mPack : allSubPacks) {
				for (MClassifier mClass : mPack.getClassifier()) {
					MClassifierAnnotations classAnno = mClass.getAnnotations();
					if (classAnno == null) {
						classAnno = (MClassifierAnnotations) currentTrigger
								.createNextStateNewObject(
										AnnotationsPackage.eINSTANCE
												.getMClassifierAnnotations());
						currentTrigger.addReferenceUpdate(mClass,
								McorePackage.eINSTANCE
										.getMPropertiesContainer_Annotations(),
								XUpdateMode.REDEFINE, null, classAnno, null,
								null);
					}

					//Moves all containments of a group in the class
					for (MPropertiesGroup propGroup : mClass
							.getAllPropertyGroups()) {
						currentTrigger.addReferenceUpdate(mClass,
								McorePackage.eINSTANCE
										.getMPropertiesContainer_Property(),
								XUpdateMode.ADD, XAddUpdateMode.LAST,
								propGroup.getProperty(), null, null);
						if (propGroup.getAnnotations() != null) {
							for (MPropertyAnnotations propAnno : propGroup
									.getAnnotations()
									.getPropertyAnnotations()) {
								currentTrigger.addReferenceUpdate(classAnno,
										AnnotationsPackage.eINSTANCE
												.getMClassifierAnnotations_PropertyAnnotations(),
										XUpdateMode.ADD, XAddUpdateMode.LAST,
										propAnno, null, null);
							}
							for (MOperationAnnotations opAnno : propGroup
									.getAnnotations()
									.getOperationAnnotations()) {
								currentTrigger.addReferenceUpdate(classAnno,
										AnnotationsPackage.eINSTANCE
												.getMClassifierAnnotations_OperationAnnotations(),
										XUpdateMode.ADD, XAddUpdateMode.LAST,
										opAnno, null, null);
							}
						}
					}

					//Deletes property Groups
					for (MPropertiesGroup propGroup : mClass
							.getAllPropertyGroups()) {
						currentTrigger.addReferenceUpdate(mClass,
								McorePackage.eINSTANCE
										.getMPropertiesContainer_PropertyGroup(),
								XUpdateMode.REMOVE, null, propGroup, null,
								null);
					}

				}
			}
			return currentTrigger;
		}

		case MPackageAction.COMPLETE_SEMANTICS_VALUE:
			EEnumLiteral triggerLiteral = McorePackage.eINSTANCE
					.getMPackageAction()
					.getEEnumLiteral(MPackageAction.COMPLETE_SEMANTICS_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMPackage_DoAction(),
					XUpdateMode.REDEFINE, null, triggerLiteral, null, null);

			ArrayList<MPackage> packs = new ArrayList<MPackage>();
			packs.add(this);
			packs.addAll(this.getAllSubpackages());

			for (MPackage mPack : packs) {
				for (MClassifier mClass : mPack.getClassifier()) {
					if (!mClass.allSuperProperties().isEmpty()) {
						//So we can check if the property of the super class already has a property annotation in the class
						ArrayList<MProperty> allPropsWithAnno = new ArrayList<MProperty>();
						for (MPropertyAnnotations propAnno : mClass
								.allPropertyAnnotations()) {
							allPropsWithAnno.add(propAnno.getProperty());
						}
						//So we can check all operation signatures with an annotation
						ArrayList<MOperationSignature> allOpsWithAnno = new ArrayList<MOperationSignature>();
						for (MOperationAnnotations opAnno : mClass
								.allOperationAnnotations()) {
							allOpsWithAnno.add(opAnno.getOperationSignature());
						}

						MClassifierAnnotations classAnno = mClass
								.getAnnotations();
						if (classAnno == null) {
							classAnno = (MClassifierAnnotations) currentTrigger
									.createNextStateNewObject(
											AnnotationsPackage.eINSTANCE
													.getMClassifierAnnotations());
							currentTrigger.addReferenceUpdate(mClass,
									McorePackage.eINSTANCE
											.getMPropertiesContainer_Annotations(),
									XUpdateMode.REDEFINE, null, classAnno, null,
									null);
						}

						for (MProperty superClassProp : mClass
								.allSuperProperties()) {
							//is it a property or an operation
							if (superClassProp.getIsOperation()) {
								for (MOperationSignature opSig : superClassProp
										.getOperationSignature()) {
									//there is no operation annotation in the class
									if (!allOpsWithAnno.contains(opSig)) {
										MOperationAnnotations newOpAnno = (MOperationAnnotations) currentTrigger
												.createNextStateNewObject(
														AnnotationsPackage.eINSTANCE
																.getMOperationAnnotations());
										currentTrigger.addReferenceUpdate(
												newOpAnno,
												AnnotationsPackage.eINSTANCE
														.getMOperationAnnotations_OperationSignature(),
												XUpdateMode.REDEFINE, null,
												opSig, null, null);
										MResult newResult = (MResult) currentTrigger
												.createNextStateNewObject(
														AnnotationsPackage.eINSTANCE
																.getMResult());
										currentTrigger.addAttributeUpdate(
												newResult,
												ExpressionsPackage.eINSTANCE
														.getMAbstractExpressionWithBase_Base(),
												XUpdateMode.REDEFINE, null,
												ExpressionsPackage.Literals.EXPRESSION_BASE
														.getEEnumLiteral(
																ExpressionBase.SELF_OBJECT_VALUE),
												null, null);
										currentTrigger.addReferenceUpdate(
												newOpAnno,
												AnnotationsPackage.eINSTANCE
														.getMOperationAnnotations_Result(),
												XUpdateMode.REDEFINE, null,
												newResult, null, null);
										currentTrigger.addReferenceUpdate(
												classAnno,
												AnnotationsPackage.eINSTANCE
														.getMClassifierAnnotations_OperationAnnotations(),
												XUpdateMode.ADD,
												XAddUpdateMode.LAST, newOpAnno,
												null, null);
									}
								}
							} else {
								//Property annotation doesn't exist 
								if (!allPropsWithAnno
										.contains(superClassProp)) {
									MPropertyAnnotations newPropAnno = (MPropertyAnnotations) currentTrigger
											.createNextStateNewObject(
													AnnotationsPackage.eINSTANCE
															.getMPropertyAnnotations());
									currentTrigger.addReferenceUpdate(
											newPropAnno,
											AnnotationsPackage.eINSTANCE
													.getMPropertyAnnotations_Property(),
											XUpdateMode.REDEFINE, null,
											superClassProp, null, null);

									switch (superClassProp.getPropertyBehavior()
											.getValue()) {
									case PropertyBehavior.STORED_VALUE:
										MInitializationValue newInit = (MInitializationValue) currentTrigger
												.createNextStateNewObject(
														AnnotationsPackage.eINSTANCE
																.getMInitializationValue());
										currentTrigger.addAttributeUpdate(
												newInit,
												ExpressionsPackage.eINSTANCE
														.getMAbstractExpressionWithBase_Base(),
												XUpdateMode.REDEFINE, null,
												ExpressionsPackage.Literals.EXPRESSION_BASE
														.getEEnumLiteral(
																ExpressionBase.SELF_OBJECT_VALUE),
												null, null);
										currentTrigger.addReferenceUpdate(
												newPropAnno,
												AnnotationsPackage.eINSTANCE
														.getMPropertyAnnotations_InitValue(),
												XUpdateMode.REDEFINE, null,
												newInit, null, null);
										break;
									case PropertyBehavior.DERIVED_VALUE:
										MResult newResult = (MResult) currentTrigger
												.createNextStateNewObject(
														AnnotationsPackage.eINSTANCE
																.getMResult());
										currentTrigger.addAttributeUpdate(
												newResult,
												ExpressionsPackage.eINSTANCE
														.getMAbstractExpressionWithBase_Base(),
												XUpdateMode.REDEFINE, null,
												ExpressionsPackage.Literals.EXPRESSION_BASE
														.getEEnumLiteral(
																ExpressionBase.SELF_OBJECT_VALUE),
												null, null);
										currentTrigger.addReferenceUpdate(
												newPropAnno,
												AnnotationsPackage.eINSTANCE
														.getMPropertyAnnotations_Result(),
												XUpdateMode.REDEFINE, null,
												newResult, null, null);
										break;
									default:
										break;
									}
									currentTrigger.addReferenceUpdate(classAnno,
											AnnotationsPackage.eINSTANCE
													.getMClassifierAnnotations_PropertyAnnotations(),
											XUpdateMode.ADD,
											XAddUpdateMode.LAST, newPropAnno,
											null, null);
								}
							}
						}
					}
				}
			}
			return currentTrigger;

		}
		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassifier> getClassifier() {
		if (classifier == null) {
			classifier = new EObjectContainmentEList.Unsettable.Resolving<MClassifier>(
					MClassifier.class, this, McorePackage.MPACKAGE__CLASSIFIER);
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetClassifier() {
		if (classifier != null)
			((InternalEList.Unsettable<?>) classifier).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetClassifier() {
		return classifier != null
				&& ((InternalEList.Unsettable<?>) classifier).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPackage> getSubPackage() {
		if (subPackage == null) {
			subPackage = new EObjectContainmentEList.Unsettable.Resolving<MPackage>(
					MPackage.class, this, McorePackage.MPACKAGE__SUB_PACKAGE);
		}
		return subPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSubPackage() {
		if (subPackage != null)
			((InternalEList.Unsettable<?>) subPackage).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSubPackage() {
		return subPackage != null
				&& ((InternalEList.Unsettable<?>) subPackage).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackageAnnotations getPackageAnnotations() {
		if (packageAnnotations != null && packageAnnotations.eIsProxy()) {
			InternalEObject oldPackageAnnotations = (InternalEObject) packageAnnotations;
			packageAnnotations = (MPackageAnnotations) eResolveProxy(
					oldPackageAnnotations);
			if (packageAnnotations != oldPackageAnnotations) {
				InternalEObject newPackageAnnotations = (InternalEObject) packageAnnotations;
				NotificationChain msgs = oldPackageAnnotations.eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
						null, null);
				if (newPackageAnnotations.eInternalContainer() == null) {
					msgs = newPackageAnnotations.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
							oldPackageAnnotations, packageAnnotations));
			}
		}
		return packageAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackageAnnotations basicGetPackageAnnotations() {
		return packageAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageAnnotations(
			MPackageAnnotations newPackageAnnotations, NotificationChain msgs) {
		MPackageAnnotations oldPackageAnnotations = packageAnnotations;
		packageAnnotations = newPackageAnnotations;
		boolean oldPackageAnnotationsESet = packageAnnotationsESet;
		packageAnnotationsESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
					oldPackageAnnotations, newPackageAnnotations,
					!oldPackageAnnotationsESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageAnnotations(
			MPackageAnnotations newPackageAnnotations) {
		if (newPackageAnnotations != packageAnnotations) {
			NotificationChain msgs = null;
			if (packageAnnotations != null)
				msgs = ((InternalEObject) packageAnnotations).eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
						null, msgs);
			if (newPackageAnnotations != null)
				msgs = ((InternalEObject) newPackageAnnotations).eInverseAdd(
						this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
						null, msgs);
			msgs = basicSetPackageAnnotations(newPackageAnnotations, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldPackageAnnotationsESet = packageAnnotationsESet;
			packageAnnotationsESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
						newPackageAnnotations, newPackageAnnotations,
						!oldPackageAnnotationsESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetPackageAnnotations(
			NotificationChain msgs) {
		MPackageAnnotations oldPackageAnnotations = packageAnnotations;
		packageAnnotations = null;
		boolean oldPackageAnnotationsESet = packageAnnotationsESet;
		packageAnnotationsESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
					oldPackageAnnotations, null, oldPackageAnnotationsESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPackageAnnotations() {
		if (packageAnnotations != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) packageAnnotations).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS,
					null, msgs);
			msgs = basicUnsetPackageAnnotations(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldPackageAnnotationsESet = packageAnnotationsESet;
			packageAnnotationsESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS, null, null,
						oldPackageAnnotationsESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPackageAnnotations() {
		return packageAnnotationsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNamePrefix() {
		return namePrefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamePrefix(String newNamePrefix) {
		String oldNamePrefix = namePrefix;
		namePrefix = newNamePrefix;
		boolean oldNamePrefixESet = namePrefixESet;
		namePrefixESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__NAME_PREFIX, oldNamePrefix,
					namePrefix, !oldNamePrefixESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamePrefix() {
		String oldNamePrefix = namePrefix;
		boolean oldNamePrefixESet = namePrefixESet;
		namePrefix = NAME_PREFIX_EDEFAULT;
		namePrefixESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__NAME_PREFIX, oldNamePrefix,
					NAME_PREFIX_EDEFAULT, oldNamePrefixESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamePrefix() {
		return namePrefixESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedNamePrefix() {
		/**
		 * @OCL let getOtherNamePrefix: String = if (let chain: Boolean = isRootPackage in
		if chain = true then true else false 
		endif) 
		=true 
		then if containingComponent.oclIsUndefined()
		then null
		else containingComponent.derivedNamePrefix
		endif
		else if containingPackage.oclIsUndefined()
		then null
		else containingPackage.derivedNamePrefix
		endif
		endif in
		let resultofDerivedNamePrefix: String = if (let e0: Boolean = not((let e0: Boolean =  namePrefix.oclIsInvalid() or  namePrefix.oclIsUndefined() or (let e0: Boolean = namePrefix = '' in 
		if e0.oclIsInvalid() then null else e0 endif) in 
		if e0.oclIsInvalid() then null else e0 endif)) in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then let chain: String = namePrefix in
		if chain.oclIsUndefined() or chain.camelCaseUpper().oclIsUndefined() 
		then null 
		else chain.camelCaseUpper()
		endif
		else getOtherNamePrefix
		endif in
		resultofDerivedNamePrefix
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DERIVED_NAME_PREFIX;

		if (derivedNamePrefixDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedNamePrefixDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedNamePrefixDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getNamePrefixScope() {
		MPackage namePrefixScope = basicGetNamePrefixScope();
		return namePrefixScope != null && namePrefixScope.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) namePrefixScope)
				: namePrefixScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetNamePrefixScope() {
		/**
		 * @OCL let choosePackage: mcore::MPackage = if (isRootPackage) 
		=true 
		then self
		else if containingPackage.oclIsUndefined()
		then null
		else containingPackage.namePrefixScope
		endif
		endif in
		let resultofNamePrefixScope: mcore::MPackage = if (let e0: Boolean =  namePrefix.oclIsInvalid() or  namePrefix.oclIsUndefined() or (let e0: Boolean = namePrefix = '' in 
		if e0.oclIsInvalid() then null else e0 endif) in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then choosePackage
		else self
		endif in
		resultofNamePrefixScope
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__NAME_PREFIX_SCOPE;

		if (namePrefixScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				namePrefixScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(namePrefixScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedJsonSchema() {
		/**
		 * @OCL let space : String = ' ' in
		let lineBreak : String = '\n' in
		let branch : String = '{' in
		let closedBranch : String = '}' in
		let comma : String = ',' in
		let rootPropertyName : String = 'rootProperty' in 
		let reference : String = '#/definitions/' in 
		let jsonHeader: String = '{'
		.concat(lineBreak)
		.concat(' "$schema": "http://json-schema.org/draft-07/schema#"').concat(comma)
		.concat(lineBreak)
		.concat('"title": ')
		.concat('"').concat(self.containingComponent.name).concat('"').concat(comma)
		.concat(lineBreak)
		.concat('"description": ').concat('"').concat(if self.description.oclIsUndefined() then 'no description' else self.description endif).concat('"').concat(comma)
		.concat(lineBreak)
		.concat('"type": ')
		.concat('"object"')
		.concat(comma)
		.concat(lineBreak)
		.concat('"properties":').concat(space).concat(branch)
		.concat(lineBreak)
		.concat('"').concat(rootPropertyName).concat('":').concat(space).concat(branch)
		.concat(lineBreak)
		.concat('"description": ').concat('"').concat(rootPropertyName).concat('"').concat(comma)
		.concat(lineBreak)
		.concat('"$ref":').concat(space).concat('"').concat(reference).concat(self.resourceRootClass.eName).concat('"')
		.concat(lineBreak)
		.concat(closedBranch)
		.concat(lineBreak)
		.concat(closedBranch).concat(comma)
		.concat(lineBreak)
		.concat('"required":').concat(space).concat('[').concat(space).concat('"').concat(rootPropertyName).concat('"').concat(space).concat(']')
		in
		jsonHeader.concat(comma)
		.concat(lineBreak)
		.concat('"definitions":').concat(space).concat(branch)
		.concat(lineBreak)
		.concat(self.classifier->iterate(test : MClassifier; s : String = '' | s.concat(test.derivedJsonSchema).concat(if self.classifier->indexOf(test) = self.classifier->size() then '}' else lineBreak.concat(',') endif)))
		.concat(lineBreak)
		.concat(closedBranch)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DERIVED_JSON_SCHEMA;

		if (derivedJsonSchemaDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedJsonSchemaDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedJsonSchemaDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getGenerateJsonSchema() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__GENERATE_JSON_SCHEMA;

		if (generateJsonSchemaDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				generateJsonSchemaDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(generateJsonSchemaDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerateJsonSchema(Boolean newGenerateJsonSchema) {
		if (newGenerateJsonSchema == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGeneratedJsonSchema() {
		return generatedJsonSchema;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGeneratedJsonSchema(String newGeneratedJsonSchema) {
		String oldGeneratedJsonSchema = generatedJsonSchema;
		generatedJsonSchema = newGeneratedJsonSchema;
		boolean oldGeneratedJsonSchemaESet = generatedJsonSchemaESet;
		generatedJsonSchemaESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA,
					oldGeneratedJsonSchema, generatedJsonSchema,
					!oldGeneratedJsonSchemaESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneratedJsonSchema() {
		String oldGeneratedJsonSchema = generatedJsonSchema;
		boolean oldGeneratedJsonSchemaESet = generatedJsonSchemaESet;
		generatedJsonSchema = GENERATED_JSON_SCHEMA_EDEFAULT;
		generatedJsonSchemaESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA,
					oldGeneratedJsonSchema, GENERATED_JSON_SCHEMA_EDEFAULT,
					oldGeneratedJsonSchemaESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneratedJsonSchema() {
		return generatedJsonSchemaESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPackage> generateJsonSchema$update01Object(Boolean trg) {

		/**
		 * @OCL self
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPACKAGE);
		EOperation eOperation = McorePackage.Literals.MPACKAGE.getEOperations()
				.get(0);
		if (generateJsonSchema$update01ObjectecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				generateJsonSchema$update01ObjectecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				generateJsonSchema$update01ObjectecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("trg", trg);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MPackage>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String generateJsonSchema$update01Value(Boolean trg, MPackage obj) {

		/**
		 * @OCL derivedJsonSchema
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPACKAGE);
		EOperation eOperation = McorePackage.Literals.MPACKAGE.getEOperations()
				.get(1);
		if (generateJsonSchema$update01ValueecoreEBooleanObjectmcoreMPackageBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				generateJsonSchema$update01ValueecoreEBooleanObjectmcoreMPackageBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				generateJsonSchema$update01ValueecoreEBooleanObjectmcoreMPackageBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("trg", trg);

			evalEnv.add("obj", obj);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUsePackageContentOnlyWithExplicitFilter() {
		return usePackageContentOnlyWithExplicitFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsePackageContentOnlyWithExplicitFilter(
			Boolean newUsePackageContentOnlyWithExplicitFilter) {
		Boolean oldUsePackageContentOnlyWithExplicitFilter = usePackageContentOnlyWithExplicitFilter;
		usePackageContentOnlyWithExplicitFilter = newUsePackageContentOnlyWithExplicitFilter;
		boolean oldUsePackageContentOnlyWithExplicitFilterESet = usePackageContentOnlyWithExplicitFilterESet;
		usePackageContentOnlyWithExplicitFilterESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER,
					oldUsePackageContentOnlyWithExplicitFilter,
					usePackageContentOnlyWithExplicitFilter,
					!oldUsePackageContentOnlyWithExplicitFilterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUsePackageContentOnlyWithExplicitFilter() {
		Boolean oldUsePackageContentOnlyWithExplicitFilter = usePackageContentOnlyWithExplicitFilter;
		boolean oldUsePackageContentOnlyWithExplicitFilterESet = usePackageContentOnlyWithExplicitFilterESet;
		usePackageContentOnlyWithExplicitFilter = USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER_EDEFAULT;
		usePackageContentOnlyWithExplicitFilterESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER,
					oldUsePackageContentOnlyWithExplicitFilter,
					USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER_EDEFAULT,
					oldUsePackageContentOnlyWithExplicitFilterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUsePackageContentOnlyWithExplicitFilter() {
		return usePackageContentOnlyWithExplicitFilterESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialNsURI() {
		return specialNsURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialNsURI(String newSpecialNsURI) {
		String oldSpecialNsURI = specialNsURI;
		specialNsURI = newSpecialNsURI;
		boolean oldSpecialNsURIESet = specialNsURIESet;
		specialNsURIESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__SPECIAL_NS_URI, oldSpecialNsURI,
					specialNsURI, !oldSpecialNsURIESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpecialNsURI() {
		String oldSpecialNsURI = specialNsURI;
		boolean oldSpecialNsURIESet = specialNsURIESet;
		specialNsURI = SPECIAL_NS_URI_EDEFAULT;
		specialNsURIESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__SPECIAL_NS_URI, oldSpecialNsURI,
					SPECIAL_NS_URI_EDEFAULT, oldSpecialNsURIESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpecialNsURI() {
		return specialNsURIESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialNsPrefix() {
		return specialNsPrefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialNsPrefix(String newSpecialNsPrefix) {
		String oldSpecialNsPrefix = specialNsPrefix;
		specialNsPrefix = newSpecialNsPrefix;
		boolean oldSpecialNsPrefixESet = specialNsPrefixESet;
		specialNsPrefixESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__SPECIAL_NS_PREFIX,
					oldSpecialNsPrefix, specialNsPrefix,
					!oldSpecialNsPrefixESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpecialNsPrefix() {
		String oldSpecialNsPrefix = specialNsPrefix;
		boolean oldSpecialNsPrefixESet = specialNsPrefixESet;
		specialNsPrefix = SPECIAL_NS_PREFIX_EDEFAULT;
		specialNsPrefixESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__SPECIAL_NS_PREFIX,
					oldSpecialNsPrefix, SPECIAL_NS_PREFIX_EDEFAULT,
					oldSpecialNsPrefixESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpecialNsPrefix() {
		return specialNsPrefixESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialFileName() {
		return specialFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialFileName(String newSpecialFileName) {
		String oldSpecialFileName = specialFileName;
		specialFileName = newSpecialFileName;
		boolean oldSpecialFileNameESet = specialFileNameESet;
		specialFileNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__SPECIAL_FILE_NAME,
					oldSpecialFileName, specialFileName,
					!oldSpecialFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpecialFileName() {
		String oldSpecialFileName = specialFileName;
		boolean oldSpecialFileNameESet = specialFileNameESet;
		specialFileName = SPECIAL_FILE_NAME_EDEFAULT;
		specialFileNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__SPECIAL_FILE_NAME,
					oldSpecialFileName, SPECIAL_FILE_NAME_EDEFAULT,
					oldSpecialFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpecialFileName() {
		return specialFileNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUseUUID() {
		return useUUID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseUUID(Boolean newUseUUID) {
		Boolean oldUseUUID = useUUID;
		useUUID = newUseUUID;
		boolean oldUseUUIDESet = useUUIDESet;
		useUUIDESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__USE_UUID, oldUseUUID, useUUID,
					!oldUseUUIDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUseUUID() {
		Boolean oldUseUUID = useUUID;
		boolean oldUseUUIDESet = useUUIDESet;
		useUUID = USE_UUID_EDEFAULT;
		useUUIDESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__USE_UUID, oldUseUUID,
					USE_UUID_EDEFAULT, oldUseUUIDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUseUUID() {
		return useUUIDESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUuidAttributeName() {
		return uuidAttributeName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUuidAttributeName(String newUuidAttributeName) {
		String oldUuidAttributeName = uuidAttributeName;
		uuidAttributeName = newUuidAttributeName;
		boolean oldUuidAttributeNameESet = uuidAttributeNameESet;
		uuidAttributeNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME,
					oldUuidAttributeName, uuidAttributeName,
					!oldUuidAttributeNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUuidAttributeName() {
		String oldUuidAttributeName = uuidAttributeName;
		boolean oldUuidAttributeNameESet = uuidAttributeNameESet;
		uuidAttributeName = UUID_ATTRIBUTE_NAME_EDEFAULT;
		uuidAttributeNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME,
					oldUuidAttributeName, UUID_ATTRIBUTE_NAME_EDEFAULT,
					oldUuidAttributeNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUuidAttributeName() {
		return uuidAttributeNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getParent() {
		MPackage parent = basicGetParent();
		return parent != null && parent.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) parent) : parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetParent() {
		/**
		 * @OCL if self.isRootPackage then null else self.eContainer().oclAsType(MPackage) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__PARENT;

		if (parentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				parentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(parentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getContainingPackage() {
		MPackage containingPackage = basicGetContainingPackage();
		return containingPackage != null && containingPackage.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) containingPackage)
				: containingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetContainingPackage() {
		/**
		 * @OCL if isRootPackage then null else
		eContainer().oclAsType(MPackage) endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__CONTAINING_PACKAGE;

		if (containingPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MComponent getContainingComponent() {
		MComponent containingComponent = basicGetContainingComponent();
		return containingComponent != null && containingComponent.eIsProxy()
				? (MComponent) eResolveProxy(
						(InternalEObject) containingComponent)
				: containingComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MComponent basicGetContainingComponent() {
		/**
		 * @OCL if eContainer().oclIsUndefined() then null
		else 
		if isRootPackage then eContainer().oclAsType(MComponent) 
		else containingPackage.containingComponent endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__CONTAINING_COMPONENT;

		if (containingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MComponent result = (MComponent) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getRepresentsCorePackage() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() 
		then false
		else if isRootPackage 
		then containingComponent.representsCoreComponent 
		else containingPackage.representsCorePackage endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__REPRESENTS_CORE_PACKAGE;

		if (representsCorePackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				representsCorePackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(representsCorePackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPackage> getAllSubpackages() {
		/**
		 * @OCL subPackage->union(subPackage->closure(subPackage))
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__ALL_SUBPACKAGES;

		if (allSubpackagesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				allSubpackagesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allSubpackagesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPackage> result = (EList<MPackage>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQualifiedName() {
		/**
		 * @OCL if (let e0: Boolean = if (representsCorePackage)= true 
		then true 
		else if (isRootPackage)= true 
		then true 
		else if ((representsCorePackage)= null or (isRootPackage)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then let chain: String = eName in
		if chain.allLowerCase().oclIsUndefined() 
		then null 
		else chain.allLowerCase()
		endif
		else (let e0: String = if containingPackage.oclIsUndefined()
		then null
		else containingPackage.qualifiedName
		endif.concat('::').concat(let chain02: String = eName in
		if chain02.allLowerCase().oclIsUndefined() 
		then null 
		else chain02.allLowerCase()
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__QUALIFIED_NAME;

		if (qualifiedNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				qualifiedNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(qualifiedNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsRootPackage() {
		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then false
		else eContainer().oclIsTypeOf(MComponent) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__IS_ROOT_PACKAGE;

		if (isRootPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isRootPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isRootPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getInternalEPackage() {
		if (internalEPackage != null && internalEPackage.eIsProxy()) {
			InternalEObject oldInternalEPackage = (InternalEObject) internalEPackage;
			internalEPackage = (EPackage) eResolveProxy(oldInternalEPackage);
			if (internalEPackage != oldInternalEPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPACKAGE__INTERNAL_EPACKAGE,
							oldInternalEPackage, internalEPackage));
			}
		}
		return internalEPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetInternalEPackage() {
		return internalEPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalEPackage(EPackage newInternalEPackage) {
		EPackage oldInternalEPackage = internalEPackage;
		internalEPackage = newInternalEPackage;
		boolean oldInternalEPackageESet = internalEPackageESet;
		internalEPackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__INTERNAL_EPACKAGE,
					oldInternalEPackage, internalEPackage,
					!oldInternalEPackageESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalEPackage() {
		EPackage oldInternalEPackage = internalEPackage;
		boolean oldInternalEPackageESet = internalEPackageESet;
		internalEPackage = null;
		internalEPackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__INTERNAL_EPACKAGE,
					oldInternalEPackage, null, oldInternalEPackageESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalEPackage() {
		return internalEPackageESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedNsURI() {
		/**
		 * @OCL if not (self.specialNsURI.oclIsUndefined() or self.specialNsURI.trim().size() = 0) then
		self.specialNsURI
		else self.derivedStandardNsURI endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DERIVED_NS_URI;

		if (derivedNsURIDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedNsURIDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedNsURIDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedStandardNsURI() {
		/**
		 * @OCL if self.isRootPackage then
		self.containingComponent.derivedURI.concat('/').concat(self.calculatedShortName.camelCaseUpper()) else
		self.containingPackage.derivedStandardNsURI.concat('/').concat(self.calculatedShortName.camelCaseUpper().firstUpperCase())
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DERIVED_STANDARD_NS_URI;

		if (derivedStandardNsURIDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedStandardNsURIDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedStandardNsURIDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedNsPrefix() {
		/**
		 * @OCL (if isRootPackage then '' else
		containingPackage.derivedNsPrefix.concat('.') endif)
		.concat(calculatedShortName.camelCaseLower().allLowerCase())
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DERIVED_NS_PREFIX;

		if (derivedNsPrefixDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedNsPrefixDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedNsPrefixDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedJavaPackageName() {
		/**
		 * @OCL if containingComponent.calculatedShortName = containingComponent.rootPackage.calculatedShortName
		then (
		if containingComponent.domainType.oclIsUndefined()
		then 'MISSING domain type'
		else containingComponent.derivedDomainType
		endif
		).concat('.').concat(
		if containingComponent.domainName.oclIsUndefined()
		then 'MISSING domain name'
		else containingComponent.derivedDomainName
		endif
		).concat('.').concat(derivedNsPrefix)
		else
		containingComponent.derivedBundleName.concat('.').concat(derivedNsPrefix)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPACKAGE;
		EStructuralFeature eFeature = McorePackage.Literals.MPACKAGE__DERIVED_JAVA_PACKAGE_NAME;

		if (derivedJavaPackageNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedJavaPackageNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPACKAGE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedJavaPackageNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getResourceRootClass() {
		if (resourceRootClass != null && resourceRootClass.eIsProxy()) {
			InternalEObject oldResourceRootClass = (InternalEObject) resourceRootClass;
			resourceRootClass = (MClassifier) eResolveProxy(
					oldResourceRootClass);
			if (resourceRootClass != oldResourceRootClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS,
							oldResourceRootClass, resourceRootClass));
			}
		}
		return resourceRootClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetResourceRootClass() {
		return resourceRootClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceRootClass(MClassifier newResourceRootClass) {
		MClassifier oldResourceRootClass = resourceRootClass;
		resourceRootClass = newResourceRootClass;
		boolean oldResourceRootClassESet = resourceRootClassESet;
		resourceRootClassESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS,
					oldResourceRootClass, resourceRootClass,
					!oldResourceRootClassESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResourceRootClass() {
		MClassifier oldResourceRootClass = resourceRootClass;
		boolean oldResourceRootClassESet = resourceRootClassESet;
		resourceRootClass = null;
		resourceRootClassESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS,
					oldResourceRootClass, null, oldResourceRootClassESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResourceRootClass() {
		return resourceRootClassESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Resource Root Class</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self->union(allSubpackages)->includes(trg.containingPackage)
	and
	not (trg.abstractClass)
	and
	trg.kind = ClassifierKind::ClassType
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalResourceRootClassChoiceConstraint(MClassifier trg) {
		EClass eClass = McorePackage.Literals.MPACKAGE;
		if (resourceRootClassChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = McorePackage.Literals.MPACKAGE__RESOURCE_ROOT_CLASS;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				resourceRootClassChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"ResourceRootClassChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(resourceRootClassChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"ResourceRootClassChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
			return ((InternalEList<?>) getGeneralAnnotation())
					.basicRemove(otherEnd, msgs);
		case McorePackage.MPACKAGE__CLASSIFIER:
			return ((InternalEList<?>) getClassifier()).basicRemove(otherEnd,
					msgs);
		case McorePackage.MPACKAGE__SUB_PACKAGE:
			return ((InternalEList<?>) getSubPackage()).basicRemove(otherEnd,
					msgs);
		case McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS:
			return basicUnsetPackageAnnotations(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
			return getGeneralAnnotation();
		case McorePackage.MPACKAGE__CLASSIFIER:
			return getClassifier();
		case McorePackage.MPACKAGE__SUB_PACKAGE:
			return getSubPackage();
		case McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS:
			if (resolve)
				return getPackageAnnotations();
			return basicGetPackageAnnotations();
		case McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER:
			return getUsePackageContentOnlyWithExplicitFilter();
		case McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS:
			if (resolve)
				return getResourceRootClass();
			return basicGetResourceRootClass();
		case McorePackage.MPACKAGE__SPECIAL_NS_URI:
			return getSpecialNsURI();
		case McorePackage.MPACKAGE__SPECIAL_NS_PREFIX:
			return getSpecialNsPrefix();
		case McorePackage.MPACKAGE__SPECIAL_FILE_NAME:
			return getSpecialFileName();
		case McorePackage.MPACKAGE__USE_UUID:
			return getUseUUID();
		case McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME:
			return getUuidAttributeName();
		case McorePackage.MPACKAGE__PARENT:
			if (resolve)
				return getParent();
			return basicGetParent();
		case McorePackage.MPACKAGE__CONTAINING_PACKAGE:
			if (resolve)
				return getContainingPackage();
			return basicGetContainingPackage();
		case McorePackage.MPACKAGE__CONTAINING_COMPONENT:
			if (resolve)
				return getContainingComponent();
			return basicGetContainingComponent();
		case McorePackage.MPACKAGE__REPRESENTS_CORE_PACKAGE:
			return getRepresentsCorePackage();
		case McorePackage.MPACKAGE__ALL_SUBPACKAGES:
			return getAllSubpackages();
		case McorePackage.MPACKAGE__QUALIFIED_NAME:
			return getQualifiedName();
		case McorePackage.MPACKAGE__IS_ROOT_PACKAGE:
			return getIsRootPackage();
		case McorePackage.MPACKAGE__INTERNAL_EPACKAGE:
			if (resolve)
				return getInternalEPackage();
			return basicGetInternalEPackage();
		case McorePackage.MPACKAGE__DERIVED_NS_URI:
			return getDerivedNsURI();
		case McorePackage.MPACKAGE__DERIVED_STANDARD_NS_URI:
			return getDerivedStandardNsURI();
		case McorePackage.MPACKAGE__DERIVED_NS_PREFIX:
			return getDerivedNsPrefix();
		case McorePackage.MPACKAGE__DERIVED_JAVA_PACKAGE_NAME:
			return getDerivedJavaPackageName();
		case McorePackage.MPACKAGE__DO_ACTION:
			return getDoAction();
		case McorePackage.MPACKAGE__NAME_PREFIX:
			return getNamePrefix();
		case McorePackage.MPACKAGE__DERIVED_NAME_PREFIX:
			return getDerivedNamePrefix();
		case McorePackage.MPACKAGE__NAME_PREFIX_SCOPE:
			if (resolve)
				return getNamePrefixScope();
			return basicGetNamePrefixScope();
		case McorePackage.MPACKAGE__DERIVED_JSON_SCHEMA:
			return getDerivedJsonSchema();
		case McorePackage.MPACKAGE__GENERATE_JSON_SCHEMA:
			return getGenerateJsonSchema();
		case McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA:
			return getGeneratedJsonSchema();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
			getGeneralAnnotation().clear();
			getGeneralAnnotation().addAll(
					(Collection<? extends MGeneralAnnotation>) newValue);
			return;
		case McorePackage.MPACKAGE__CLASSIFIER:
			getClassifier().clear();
			getClassifier()
					.addAll((Collection<? extends MClassifier>) newValue);
			return;
		case McorePackage.MPACKAGE__SUB_PACKAGE:
			getSubPackage().clear();
			getSubPackage().addAll((Collection<? extends MPackage>) newValue);
			return;
		case McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS:
			setPackageAnnotations((MPackageAnnotations) newValue);
			return;
		case McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER:
			setUsePackageContentOnlyWithExplicitFilter((Boolean) newValue);
			return;
		case McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS:
			setResourceRootClass((MClassifier) newValue);
			return;
		case McorePackage.MPACKAGE__SPECIAL_NS_URI:
			setSpecialNsURI((String) newValue);
			return;
		case McorePackage.MPACKAGE__SPECIAL_NS_PREFIX:
			setSpecialNsPrefix((String) newValue);
			return;
		case McorePackage.MPACKAGE__SPECIAL_FILE_NAME:
			setSpecialFileName((String) newValue);
			return;
		case McorePackage.MPACKAGE__USE_UUID:
			setUseUUID((Boolean) newValue);
			return;
		case McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME:
			setUuidAttributeName((String) newValue);
			return;
		case McorePackage.MPACKAGE__INTERNAL_EPACKAGE:
			setInternalEPackage((EPackage) newValue);
			return;
		case McorePackage.MPACKAGE__DO_ACTION:
			setDoAction((MPackageAction) newValue);
			return;
		case McorePackage.MPACKAGE__NAME_PREFIX:
			setNamePrefix((String) newValue);
			return;
		case McorePackage.MPACKAGE__GENERATE_JSON_SCHEMA:
			setGenerateJsonSchema((Boolean) newValue);
			return;
		case McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA:
			setGeneratedJsonSchema((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
			unsetGeneralAnnotation();
			return;
		case McorePackage.MPACKAGE__CLASSIFIER:
			unsetClassifier();
			return;
		case McorePackage.MPACKAGE__SUB_PACKAGE:
			unsetSubPackage();
			return;
		case McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS:
			unsetPackageAnnotations();
			return;
		case McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER:
			unsetUsePackageContentOnlyWithExplicitFilter();
			return;
		case McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS:
			unsetResourceRootClass();
			return;
		case McorePackage.MPACKAGE__SPECIAL_NS_URI:
			unsetSpecialNsURI();
			return;
		case McorePackage.MPACKAGE__SPECIAL_NS_PREFIX:
			unsetSpecialNsPrefix();
			return;
		case McorePackage.MPACKAGE__SPECIAL_FILE_NAME:
			unsetSpecialFileName();
			return;
		case McorePackage.MPACKAGE__USE_UUID:
			unsetUseUUID();
			return;
		case McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME:
			unsetUuidAttributeName();
			return;
		case McorePackage.MPACKAGE__INTERNAL_EPACKAGE:
			unsetInternalEPackage();
			return;
		case McorePackage.MPACKAGE__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		case McorePackage.MPACKAGE__NAME_PREFIX:
			unsetNamePrefix();
			return;
		case McorePackage.MPACKAGE__GENERATE_JSON_SCHEMA:
			setGenerateJsonSchema(GENERATE_JSON_SCHEMA_EDEFAULT);
			return;
		case McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA:
			unsetGeneratedJsonSchema();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
			return isSetGeneralAnnotation();
		case McorePackage.MPACKAGE__CLASSIFIER:
			return isSetClassifier();
		case McorePackage.MPACKAGE__SUB_PACKAGE:
			return isSetSubPackage();
		case McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS:
			return isSetPackageAnnotations();
		case McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER:
			return isSetUsePackageContentOnlyWithExplicitFilter();
		case McorePackage.MPACKAGE__RESOURCE_ROOT_CLASS:
			return isSetResourceRootClass();
		case McorePackage.MPACKAGE__SPECIAL_NS_URI:
			return isSetSpecialNsURI();
		case McorePackage.MPACKAGE__SPECIAL_NS_PREFIX:
			return isSetSpecialNsPrefix();
		case McorePackage.MPACKAGE__SPECIAL_FILE_NAME:
			return isSetSpecialFileName();
		case McorePackage.MPACKAGE__USE_UUID:
			return isSetUseUUID();
		case McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME:
			return isSetUuidAttributeName();
		case McorePackage.MPACKAGE__PARENT:
			return basicGetParent() != null;
		case McorePackage.MPACKAGE__CONTAINING_PACKAGE:
			return basicGetContainingPackage() != null;
		case McorePackage.MPACKAGE__CONTAINING_COMPONENT:
			return basicGetContainingComponent() != null;
		case McorePackage.MPACKAGE__REPRESENTS_CORE_PACKAGE:
			return REPRESENTS_CORE_PACKAGE_EDEFAULT == null
					? getRepresentsCorePackage() != null
					: !REPRESENTS_CORE_PACKAGE_EDEFAULT
							.equals(getRepresentsCorePackage());
		case McorePackage.MPACKAGE__ALL_SUBPACKAGES:
			return !getAllSubpackages().isEmpty();
		case McorePackage.MPACKAGE__QUALIFIED_NAME:
			return QUALIFIED_NAME_EDEFAULT == null ? getQualifiedName() != null
					: !QUALIFIED_NAME_EDEFAULT.equals(getQualifiedName());
		case McorePackage.MPACKAGE__IS_ROOT_PACKAGE:
			return IS_ROOT_PACKAGE_EDEFAULT == null ? getIsRootPackage() != null
					: !IS_ROOT_PACKAGE_EDEFAULT.equals(getIsRootPackage());
		case McorePackage.MPACKAGE__INTERNAL_EPACKAGE:
			return isSetInternalEPackage();
		case McorePackage.MPACKAGE__DERIVED_NS_URI:
			return DERIVED_NS_URI_EDEFAULT == null ? getDerivedNsURI() != null
					: !DERIVED_NS_URI_EDEFAULT.equals(getDerivedNsURI());
		case McorePackage.MPACKAGE__DERIVED_STANDARD_NS_URI:
			return DERIVED_STANDARD_NS_URI_EDEFAULT == null
					? getDerivedStandardNsURI() != null
					: !DERIVED_STANDARD_NS_URI_EDEFAULT
							.equals(getDerivedStandardNsURI());
		case McorePackage.MPACKAGE__DERIVED_NS_PREFIX:
			return DERIVED_NS_PREFIX_EDEFAULT == null
					? getDerivedNsPrefix() != null
					: !DERIVED_NS_PREFIX_EDEFAULT.equals(getDerivedNsPrefix());
		case McorePackage.MPACKAGE__DERIVED_JAVA_PACKAGE_NAME:
			return DERIVED_JAVA_PACKAGE_NAME_EDEFAULT == null
					? getDerivedJavaPackageName() != null
					: !DERIVED_JAVA_PACKAGE_NAME_EDEFAULT
							.equals(getDerivedJavaPackageName());
		case McorePackage.MPACKAGE__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case McorePackage.MPACKAGE__NAME_PREFIX:
			return isSetNamePrefix();
		case McorePackage.MPACKAGE__DERIVED_NAME_PREFIX:
			return DERIVED_NAME_PREFIX_EDEFAULT == null
					? getDerivedNamePrefix() != null
					: !DERIVED_NAME_PREFIX_EDEFAULT
							.equals(getDerivedNamePrefix());
		case McorePackage.MPACKAGE__NAME_PREFIX_SCOPE:
			return basicGetNamePrefixScope() != null;
		case McorePackage.MPACKAGE__DERIVED_JSON_SCHEMA:
			return DERIVED_JSON_SCHEMA_EDEFAULT == null
					? getDerivedJsonSchema() != null
					: !DERIVED_JSON_SCHEMA_EDEFAULT
							.equals(getDerivedJsonSchema());
		case McorePackage.MPACKAGE__GENERATE_JSON_SCHEMA:
			return GENERATE_JSON_SCHEMA_EDEFAULT == null
					? getGenerateJsonSchema() != null
					: !GENERATE_JSON_SCHEMA_EDEFAULT
							.equals(getGenerateJsonSchema());
		case McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA:
			return isSetGeneratedJsonSchema();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
				return McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (baseFeatureID) {
			case McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION:
				return McorePackage.MPACKAGE__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_OBJECT__BOOLEAN:
			return generateJsonSchema$update01Object(
					(Boolean) arguments.get(0));
		case McorePackage.MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_VALUE__BOOLEAN_MPACKAGE:
			return generateJsonSchema$update01Value((Boolean) arguments.get(0),
					(MPackage) arguments.get(1));
		case McorePackage.MPACKAGE___DO_ACTION$_UPDATE__MPACKAGEACTION:
			return doAction$Update((MPackageAction) arguments.get(0));
		case McorePackage.MPACKAGE___GENERATE_JSON_SCHEMA$_UPDATE__BOOLEAN:
			return generateJsonSchema$Update((Boolean) arguments.get(0));
		case McorePackage.MPACKAGE___DO_ACTION_UPDATE__MPACKAGEACTION:
			return doActionUpdate((MPackageAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (usePackageContentOnlyWithExplicitFilter: ");
		if (usePackageContentOnlyWithExplicitFilterESet)
			result.append(usePackageContentOnlyWithExplicitFilter);
		else
			result.append("<unset>");
		result.append(", specialNsURI: ");
		if (specialNsURIESet)
			result.append(specialNsURI);
		else
			result.append("<unset>");
		result.append(", specialNsPrefix: ");
		if (specialNsPrefixESet)
			result.append(specialNsPrefix);
		else
			result.append("<unset>");
		result.append(", specialFileName: ");
		if (specialFileNameESet)
			result.append(specialFileName);
		else
			result.append("<unset>");
		result.append(", useUUID: ");
		if (useUUIDESet)
			result.append(useUUID);
		else
			result.append("<unset>");
		result.append(", uuidAttributeName: ");
		if (uuidAttributeNameESet)
			result.append(uuidAttributeName);
		else
			result.append("<unset>");
		result.append(", namePrefix: ");
		if (namePrefixESet)
			result.append(namePrefix);
		else
			result.append("<unset>");
		result.append(", generatedJsonSchema: ");
		if (generatedJsonSchemaESet)
			result.append(generatedJsonSchema);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL eName
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MPACKAGE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL fullLabel self.localStructuralName
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getFullLabel() {
		EClass eClass = (McorePackage.Literals.MPACKAGE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Package'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MPACKAGE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL eName if self.representsCorePackage then 'ecore' else 
	if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true
	then self.calculatedShortName.camelCaseLower() 
	else self.specialEName.camelCaseLower()
	endif endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (McorePackage.Literals.MPACKAGE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL localStructuralName (if self.isRootPackage then '' else self.parent.localStructuralName.concat('/') endif).concat(self.eName)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getLocalStructuralName() {
		EClass eClass = (McorePackage.Literals.MPACKAGE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__LOCAL_STRUCTURAL_NAME;

		if (localStructuralNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				localStructuralNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localStructuralNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override

	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				McorePackage.Literals.MPACKAGE__USE_UUID };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPACKAGE,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MPackageImpl
