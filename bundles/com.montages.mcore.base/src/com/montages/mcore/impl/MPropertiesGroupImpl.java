/**
 */
package com.montages.mcore.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MParameter;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MPropertiesGroupAction;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.expressions.ExpressionBase;
import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MProperties Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MPropertiesGroupImpl#getOrder <em>Order</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesGroupImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MPropertiesGroupImpl extends MPropertiesContainerImpl
		implements MPropertiesGroup {
	/**
	 * The default value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected static final Integer ORDER_EDEFAULT = new Integer(0);

	/**
	 * The cached value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected Integer order = ORDER_EDEFAULT;

	/**
	 * This is true if the Order attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean orderESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MPropertiesGroupAction DO_ACTION_EDEFAULT = MPropertiesGroupAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMPropertiesGroupActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemcoreMPropertiesGroupActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeSetDoAction <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #invokeSetDoAction
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeSetDoActionmcoreMPropertiesGroupActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifier <em>Containing Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression containingClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPropertiesGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MPROPERTIES_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesGroupAction getDoAction() {
		/**
		 * @OCL mcore::MPropertiesGroupAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTIES_GROUP;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTIES_GROUP__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_GROUP, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_GROUP, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertiesGroupAction result = (MPropertiesGroupAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MPropertiesGroupAction newDoAction) {
		MProperty p = McoreFactory.eINSTANCE.createMProperty();
		MOperationSignature s = null;
		MParameter p1 = null;
		MParameter p2 = null;
		Integer count = 1;
		MClassifier previousNonAbstractNonRootClass = this
				.getContainingClassifier();
		int iteratePos = this.getContainingClassifier().getContainingPackage()
				.getClassifier().indexOf(this) - 1;
		while (iteratePos >= 0) {
			MClassifier abstractNonRootClass = this.getContainingClassifier()
					.getContainingPackage().getClassifier().get(iteratePos);
			if (abstractNonRootClass.getKind() == ClassifierKind.CLASS_TYPE
					&& abstractNonRootClass.getAbstractClass() != true
					&& abstractNonRootClass != this.getContainingClassifier()
							.getContainingPackage().getResourceRootClass()) {
				previousNonAbstractNonRootClass = abstractNonRootClass;
				break;
			} else {
				iteratePos -= 1;
			}
		}

		switch (newDoAction.getValue()) {
		case MPropertiesGroupAction.CONSTRAINT_VALUE:
			MClassifierAnnotations semantics = getAnnotations();
			if (semantics == null) {
				semantics = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				setAnnotations(semantics);
			}
			MInvariantConstraint newConstraint = AnnotationsFactory.eINSTANCE
					.createMInvariantConstraint();
			semantics.getConstraint().add(newConstraint);
			newConstraint.setBase(ExpressionBase.TRUE_VALUE);
			newConstraint.setName("Constraint ".concat(
					Integer.toString(semantics.getConstraint().size())));
			break;
		case MPropertiesGroupAction.STRING_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty.getCalculatedSimpleType() == SimpleType.STRING) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Text ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_STRING_ATTRIBUTE);
			break;
		case MPropertiesGroupAction.BOOLEAN_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty.getCalculatedSimpleType() == SimpleType.BOOLEAN) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Flag ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_BOOLEAN_FLAG);
			break;
		case MPropertiesGroupAction.INTEGER_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty.getCalculatedSimpleType() == SimpleType.INTEGER) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Number ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_INTEGER_ATTRIBUTE);
			break;
		case MPropertiesGroupAction.REAL_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty.getCalculatedSimpleType() == SimpleType.DOUBLE) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Factor ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_REAL_ATTRIBUTE);
			break;
		case MPropertiesGroupAction.DATE_ATTRIBUTE_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty.getCalculatedSimpleType() == SimpleType.DATE) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Date ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			p.setSingular(true);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_DATE_ATTRIBUTE);
		case MPropertiesGroupAction.GROUP_OF_PROPERTIES_VALUE:
			MPropertiesGroup mGroup = McoreFactory.eINSTANCE
					.createMPropertiesGroup();
			mGroup.setName("Sub Group "
					.concat(Integer
							.toString(this.getPropertyGroup().size() + 1))
					.concat(" Of ").concat(this.getName()));
			this.getPropertyGroup().add(mGroup);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			if (this.getContainingClassifier().allProperties().isEmpty()) {
				this.getContainingClassifier().setEnforcedClass(true);
			}
			break;
		case MPropertiesGroupAction.UNARY_REFERENCE_VALUE:
			getProperty().add(p);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(true);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			break;
		case MPropertiesGroupAction.NARY_REFERENCE_VALUE:
			getProperty().add(p);

			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(false);
			p.setPropertyBehavior(PropertyBehavior.STORED);
			break;
		case MPropertiesGroupAction.UNARY_CONTAINMENT_VALUE:
			getProperty().add(p);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(true);
			p.setDoAction(MPropertyAction.INTO_CONTAINED);
			break;
		case MPropertiesGroupAction.NARY_CONTAINMENT_VALUE:
			getProperty().add(p);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setTypeDefinition(previousNonAbstractNonRootClass);
			p.setSingular(false);
			p.setDoAction(MPropertyAction.INTO_CONTAINED);
			break;
		case MPropertiesGroupAction.OPERATION_ONE_PARAMETER_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty
						.getPropertyBehavior() == PropertyBehavior.IS_OPERATION) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Operation ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.IS_OPERATION);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_BOOLEAN_FLAG);
			s = McoreFactory.eINSTANCE.createMOperationSignature();
			p.getOperationSignature().add(s);
			p1 = McoreFactory.eINSTANCE.createMParameter();
			s.getParameter().add(p1);
			p1.setSimpleType(SimpleType.STRING);
			break;
		case MPropertiesGroupAction.OPERATION_TWO_PARAMETERS_VALUE:
			for (MProperty mProperty : this.getProperty()) {
				if (mProperty
						.getPropertyBehavior() == PropertyBehavior.IS_OPERATION) {
					count += 1;
				}
			}
			p.setName(this.getCalculatedShortName().concat(" Operation ")
					.concat(count.toString()));
			getProperty().add(p);
			p.setPropertyBehavior(PropertyBehavior.IS_OPERATION);
			this.getContainingClassifier().getLiteral().clear();
			this.getContainingClassifier().setSimpleType(SimpleType.NONE);
			this.getContainingClassifier().setEnforcedClass(false);
			p.setDoAction(MPropertyAction.INTO_BOOLEAN_FLAG);
			s = McoreFactory.eINSTANCE.createMOperationSignature();
			p.getOperationSignature().add(s);
			p1 = McoreFactory.eINSTANCE.createMParameter();
			s.getParameter().add(p1);
			p1.setSimpleType(SimpleType.STRING);
			p2 = McoreFactory.eINSTANCE.createMParameter();
			s.getParameter().add(p2);
			p2.setSimpleType(SimpleType.STRING);
			break;
		default:

			break;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MPropertiesGroupAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MPropertiesGroupAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMPropertiesGroup_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(
			MPropertiesGroupAction mPropertiesGroupAction) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		EEnumLiteral eEnumLiteralTRIGGER = McorePackage.Literals.MPROPERTIES_GROUP_ACTION
				.getEEnumLiteral(mPropertiesGroupAction.getValue());
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMProperty_DoAction(),
				XUpdateMode.REDEFINE, null,
				// :=
				eEnumLiteralTRIGGER, null, null);

		currentTrigger.invokeOperationCall(this,
				McorePackage.Literals.MPROPERTIES_GROUP___INVOKE_SET_DO_ACTION__MPROPERTIESGROUPACTION,
				mPropertiesGroupAction, null, null);

		return currentTrigger;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object invokeSetDoAction(
			MPropertiesGroupAction mPropertiesGroupAction) {

		this.setDoAction(mPropertiesGroupAction);

		switch (mPropertiesGroupAction) {
		case BOOLEAN_ATTRIBUTE:
		case DATE_ATTRIBUTE:
		case INTEGER_ATTRIBUTE:
		case NARY_CONTAINMENT:
		case NARY_REFERENCE:
		case REAL_ATTRIBUTE:
		case STRING_ATTRIBUTE:
		case UNARY_CONTAINMENT:
		case UNARY_REFERENCE:
			return this.getProperty().get(getProperty().size() - 1);
		case OPERATION_ONE_PARAMETER:
		case OPERATION_TWO_PARAMETERS: {
			for (int i = getProperty().size() - 1; i > 0; i--)
				if (!this.getProperty().get(i).getOperationSignature()
						.isEmpty())
					for (MOperationAnnotations annotation : getAnnotations()
							.getOperationAnnotations())
						if (annotation.getAnnotatedProperty() == getProperty()
								.get(i))
							return annotation.getResult();

		}
		default:
			return this;
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrder(Integer newOrder) {
		Integer oldOrder = order;
		order = newOrder;
		boolean oldOrderESet = orderESet;
		orderESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTIES_GROUP__ORDER, oldOrder, order,
					!oldOrderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOrder() {
		Integer oldOrder = order;
		boolean oldOrderESet = orderESet;
		order = ORDER_EDEFAULT;
		orderESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTIES_GROUP__ORDER, oldOrder,
					ORDER_EDEFAULT, oldOrderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOrder() {
		return orderESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_GROUP__ORDER:
			return getOrder();
		case McorePackage.MPROPERTIES_GROUP__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_GROUP__ORDER:
			setOrder((Integer) newValue);
			return;
		case McorePackage.MPROPERTIES_GROUP__DO_ACTION:
			setDoAction((MPropertiesGroupAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_GROUP__ORDER:
			unsetOrder();
			return;
		case McorePackage.MPROPERTIES_GROUP__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_GROUP__ORDER:
			return isSetOrder();
		case McorePackage.MPROPERTIES_GROUP__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MPROPERTIES_GROUP___DO_ACTION$_UPDATE__MPROPERTIESGROUPACTION:
			return doAction$Update((MPropertiesGroupAction) arguments.get(0));
		case McorePackage.MPROPERTIES_GROUP___DO_ACTION_UPDATE__MPROPERTIESGROUPACTION:
			return doActionUpdate((MPropertiesGroupAction) arguments.get(0));
		case McorePackage.MPROPERTIES_GROUP___INVOKE_SET_DO_ACTION__MPROPERTIESGROUPACTION:
			return invokeSetDoAction((MPropertiesGroupAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (order: ");
		if (orderESet)
			result.append(order);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.eName.concat('(').concat(self.nrOfPropertiesAndSignatures.toString()).concat('):')
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MPROPERTIES_GROUP;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Group'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MPROPERTIES_GROUP);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL containingClassifier if self.eContainer().oclIsUndefined() then null else
	self.eContainer().oclAsType(MPropertiesContainer).containingClassifier
	endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetContainingClassifier() {
		EClass eClass = (McorePackage.Literals.MPROPERTIES_GROUP);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER;

		if (containingClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				containingClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL eName if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true
	then self.calculatedShortName.camelCaseUpper()
	else self.specialEName.camelCaseUpper()
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (McorePackage.Literals.MPROPERTIES_GROUP);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MPropertiesGroupImpl
