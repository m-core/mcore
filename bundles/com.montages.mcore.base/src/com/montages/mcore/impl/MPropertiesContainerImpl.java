/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MProperties Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getGeneralAnnotation <em>General Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getPropertyGroup <em>Property Group</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getAllConstraintAnnotations <em>All Constraint Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getOwnedProperty <em>Owned Property</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertiesContainerImpl#getNrOfPropertiesAndSignatures <em>Nr Of Properties And Signatures</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MPropertiesContainerImpl extends MNamedImpl
		implements MPropertiesContainer {
	/**
	 * The cached value of the '{@link #getGeneralAnnotation() <em>General Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<MGeneralAnnotation> generalAnnotation;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<MProperty> property;

	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected MClassifierAnnotations annotations;

	/**
	 * This is true if the Annotations containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean annotationsESet;

	/**
	 * The cached value of the '{@link #getPropertyGroup() <em>Property Group</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyGroup()
	 * @generated
	 * @ordered
	 */
	protected EList<MPropertiesGroup> propertyGroup;

	/**
	 * The default value of the '{@link #getNrOfPropertiesAndSignatures() <em>Nr Of Properties And Signatures</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNrOfPropertiesAndSignatures()
	 * @generated
	 * @ordered
	 */
	protected static final Integer NR_OF_PROPERTIES_AND_SIGNATURES_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #allPropertyAnnotations <em>All Property Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #allPropertyAnnotations
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allPropertyAnnotationsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allOperationAnnotations <em>All Operation Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #allOperationAnnotations
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allOperationAnnotationsBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllConstraintAnnotations <em>All Constraint Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllConstraintAnnotations
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allConstraintAnnotationsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifier <em>Containing Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOwnedProperty <em>Owned Property</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedProperty
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression ownedPropertyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNrOfPropertiesAndSignatures <em>Nr Of Properties And Signatures</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNrOfPropertiesAndSignatures
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression nrOfPropertiesAndSignaturesDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPropertiesContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MPROPERTIES_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralAnnotation> getGeneralAnnotation() {
		if (generalAnnotation == null) {
			generalAnnotation = new EObjectContainmentEList.Unsettable.Resolving<MGeneralAnnotation>(
					MGeneralAnnotation.class, this,
					McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION);
		}
		return generalAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralAnnotation() {
		if (generalAnnotation != null)
			((InternalEList.Unsettable<?>) generalAnnotation).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralAnnotation() {
		return generalAnnotation != null
				&& ((InternalEList.Unsettable<?>) generalAnnotation).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getProperty() {
		if (property == null) {
			property = new EObjectContainmentEList.Unsettable.Resolving<MProperty>(
					MProperty.class, this,
					McorePackage.MPROPERTIES_CONTAINER__PROPERTY);
		}
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProperty() {
		if (property != null)
			((InternalEList.Unsettable<?>) property).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProperty() {
		return property != null
				&& ((InternalEList.Unsettable<?>) property).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAnnotations getAnnotations() {
		if (annotations != null && annotations.eIsProxy()) {
			InternalEObject oldAnnotations = (InternalEObject) annotations;
			annotations = (MClassifierAnnotations) eResolveProxy(
					oldAnnotations);
			if (annotations != oldAnnotations) {
				InternalEObject newAnnotations = (InternalEObject) annotations;
				NotificationChain msgs = oldAnnotations.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
						null, null);
				if (newAnnotations.eInternalContainer() == null) {
					msgs = newAnnotations.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
							oldAnnotations, annotations));
			}
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAnnotations basicGetAnnotations() {
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotations(
			MClassifierAnnotations newAnnotations, NotificationChain msgs) {
		MClassifierAnnotations oldAnnotations = annotations;
		annotations = newAnnotations;
		boolean oldAnnotationsESet = annotationsESet;
		annotationsESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
					oldAnnotations, newAnnotations, !oldAnnotationsESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotations(MClassifierAnnotations newAnnotations) {
		if (newAnnotations != annotations) {
			NotificationChain msgs = null;
			if (annotations != null)
				msgs = ((InternalEObject) annotations).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
						null, msgs);
			if (newAnnotations != null)
				msgs = ((InternalEObject) newAnnotations).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
						null, msgs);
			msgs = basicSetAnnotations(newAnnotations, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAnnotationsESet = annotationsESet;
			annotationsESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
						newAnnotations, newAnnotations, !oldAnnotationsESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAnnotations(NotificationChain msgs) {
		MClassifierAnnotations oldAnnotations = annotations;
		annotations = null;
		boolean oldAnnotationsESet = annotationsESet;
		annotationsESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
					oldAnnotations, null, oldAnnotationsESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAnnotations() {
		if (annotations != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) annotations).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS,
					null, msgs);
			msgs = basicUnsetAnnotations(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAnnotationsESet = annotationsESet;
			annotationsESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS, null,
						null, oldAnnotationsESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAnnotations() {
		return annotationsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertiesGroup> getPropertyGroup() {
		if (propertyGroup == null) {
			propertyGroup = new EObjectContainmentEList.Unsettable.Resolving<MPropertiesGroup>(
					MPropertiesGroup.class, this,
					McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP);
		}
		return propertyGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPropertyGroup() {
		if (propertyGroup != null)
			((InternalEList.Unsettable<?>) propertyGroup).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPropertyGroup() {
		return propertyGroup != null
				&& ((InternalEList.Unsettable<?>) propertyGroup).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MInvariantConstraint> getAllConstraintAnnotations() {
		/**
		 * @OCL 
		let localAnnotations:Sequence(annotations::MInvariantConstraint) = 
		if self.annotations.oclIsUndefined()
		then Sequence{}
		else self.annotations.constraint->asSequence() endif
		in 
		let groupedAnnotations: Sequence(annotations::MInvariantConstraint) = 
		self.propertyGroup.allConstraintAnnotations
		in  localAnnotations->union(groupedAnnotations)
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTIES_CONTAINER;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS;

		if (allConstraintAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				allConstraintAnnotationsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allConstraintAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MInvariantConstraint> result = (EList<MInvariantConstraint>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getContainingClassifier() {
		MClassifier containingClassifier = basicGetContainingClassifier();
		return containingClassifier != null && containingClassifier.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) containingClassifier)
				: containingClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetContainingClassifier() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTIES_CONTAINER;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER;

		if (containingClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getOwnedProperty() {
		/**
		 * @OCL self.property->asSequence()->union(self.propertyGroup.ownedProperty)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTIES_CONTAINER;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTIES_CONTAINER__OWNED_PROPERTY;

		if (ownedPropertyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				ownedPropertyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(ownedPropertyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MProperty> result = (EList<MProperty>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getNrOfPropertiesAndSignatures() {
		/**
		 * @OCL let ps:OrderedSet(MProperty)= property->select(p:MProperty|p.kind=PropertyKind::Attribute or p.kind = PropertyKind::Reference) 
		in
		let
		nrOfPs:Integer = if ps->isEmpty() then 0 else  ps->size() endif
		in
		let os:OrderedSet(MProperty)=  property->select(p:MProperty|p.kind=PropertyKind::Operation) 
		in
		let sigs:OrderedSet(MOperationSignature)= os.operationSignature->asOrderedSet()
		in
		let nrOfSigs:Integer =if sigs->isEmpty() then 0 else  sigs->size() endif
		in
		let sigs2:OrderedSet(MOperationSignature)= OrderedSet{} 
		in
		let nrOfSigs2:Integer = if sigs2->isEmpty() then 0 else sigs2->size() endif
		in
		let nestedNr:Integer= 
		if self.propertyGroup->isEmpty() then 0 else
		propertyGroup->iterate(g:MPropertiesGroup;i:Integer=0| i+ g.nrOfPropertiesAndSignatures) endif
		in
		
		nrOfPs+nrOfSigs+nestedNr
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTIES_CONTAINER;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES;

		if (nrOfPropertiesAndSignaturesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				nrOfPropertiesAndSignaturesDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(nrOfPropertiesAndSignaturesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_CONTAINER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Integer result = (Integer) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAnnotations> allPropertyAnnotations() {

		/**
		 * @OCL let localAnnotations:Sequence(annotations::MPropertyAnnotations) = 
		if self.annotations.oclIsUndefined()
		then Sequence{}
		else self.annotations.propertyAnnotations->asSequence() endif
		in 
		let groupedAnnotations: Sequence(annotations::MPropertyAnnotations) = 
		self.propertyGroup.allPropertyAnnotations()
		in  localAnnotations->union(groupedAnnotations)
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPROPERTIES_CONTAINER);
		EOperation eOperation = McorePackage.Literals.MPROPERTIES_CONTAINER
				.getEOperations().get(0);
		if (allPropertyAnnotationsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allPropertyAnnotationsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_CONTAINER,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allPropertyAnnotationsBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_CONTAINER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MPropertyAnnotations>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MOperationAnnotations> allOperationAnnotations() {

		/**
		 * @OCL let localAnnotations:Sequence(annotations::MOperationAnnotations) = 
		if self.annotations.oclIsUndefined()
		then Sequence{}
		else self.annotations.operationAnnotations->asSequence() endif
		in 
		let groupedAnnotations: Sequence(annotations::MOperationAnnotations) = 
		self.propertyGroup.allOperationAnnotations()
		in  localAnnotations->union(groupedAnnotations)
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPROPERTIES_CONTAINER);
		EOperation eOperation = McorePackage.Literals.MPROPERTIES_CONTAINER
				.getEOperations().get(1);
		if (allOperationAnnotationsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allOperationAnnotationsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPROPERTIES_CONTAINER,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allOperationAnnotationsBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTIES_CONTAINER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MOperationAnnotations>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
			return ((InternalEList<?>) getGeneralAnnotation())
					.basicRemove(otherEnd, msgs);
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY:
			return ((InternalEList<?>) getProperty()).basicRemove(otherEnd,
					msgs);
		case McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS:
			return basicUnsetAnnotations(msgs);
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP:
			return ((InternalEList<?>) getPropertyGroup()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
			return getGeneralAnnotation();
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY:
			return getProperty();
		case McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS:
			if (resolve)
				return getAnnotations();
			return basicGetAnnotations();
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP:
			return getPropertyGroup();
		case McorePackage.MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS:
			return getAllConstraintAnnotations();
		case McorePackage.MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER:
			if (resolve)
				return getContainingClassifier();
			return basicGetContainingClassifier();
		case McorePackage.MPROPERTIES_CONTAINER__OWNED_PROPERTY:
			return getOwnedProperty();
		case McorePackage.MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES:
			return getNrOfPropertiesAndSignatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
			getGeneralAnnotation().clear();
			getGeneralAnnotation().addAll(
					(Collection<? extends MGeneralAnnotation>) newValue);
			return;
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY:
			getProperty().clear();
			getProperty().addAll((Collection<? extends MProperty>) newValue);
			return;
		case McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS:
			setAnnotations((MClassifierAnnotations) newValue);
			return;
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP:
			getPropertyGroup().clear();
			getPropertyGroup()
					.addAll((Collection<? extends MPropertiesGroup>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
			unsetGeneralAnnotation();
			return;
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY:
			unsetProperty();
			return;
		case McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS:
			unsetAnnotations();
			return;
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP:
			unsetPropertyGroup();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
			return isSetGeneralAnnotation();
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY:
			return isSetProperty();
		case McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS:
			return isSetAnnotations();
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP:
			return isSetPropertyGroup();
		case McorePackage.MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS:
			return !getAllConstraintAnnotations().isEmpty();
		case McorePackage.MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER:
			return basicGetContainingClassifier() != null;
		case McorePackage.MPROPERTIES_CONTAINER__OWNED_PROPERTY:
			return !getOwnedProperty().isEmpty();
		case McorePackage.MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES:
			return NR_OF_PROPERTIES_AND_SIGNATURES_EDEFAULT == null
					? getNrOfPropertiesAndSignatures() != null
					: !NR_OF_PROPERTIES_AND_SIGNATURES_EDEFAULT
							.equals(getNrOfPropertiesAndSignatures());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
				return McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (baseFeatureID) {
			case McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION:
				return McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MPROPERTIES_CONTAINER___ALL_PROPERTY_ANNOTATIONS:
			return allPropertyAnnotations();
		case McorePackage.MPROPERTIES_CONTAINER___ALL_OPERATION_ANNOTATIONS:
			return allOperationAnnotations();
		}
		return super.eInvoke(operationID, arguments);
	}

} //MPropertiesContainerImpl
