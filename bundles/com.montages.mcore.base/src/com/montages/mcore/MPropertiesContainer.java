/**
 */
package com.montages.mcore;

import org.eclipse.emf.common.util.EList;

import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MProperties Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getProperty <em>Property</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getPropertyGroup <em>Property Group</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getAllConstraintAnnotations <em>All Constraint Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getOwnedProperty <em>Owned Property</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesContainer#getNrOfPropertiesAndSignatures <em>Nr Of Properties And Signatures</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMPropertiesContainer()
 * @model abstract="true"
 * @generated
 */

public interface MPropertiesContainer extends MNamed, MModelElement {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference list.
	 * @see #isSetProperty()
	 * @see #unsetProperty()
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_Property()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MProperty> getProperty();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPropertiesContainer#getProperty <em>Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProperty()
	 * @see #getProperty()
	 * @generated
	 */
	void unsetProperty();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPropertiesContainer#getProperty <em>Property</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Property</em>' containment reference list is set.
	 * @see #unsetProperty()
	 * @see #getProperty()
	 * @generated
	 */
	boolean isSetProperty();

	/**
	 * Returns the value of the '<em><b>Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations</em>' containment reference.
	 * @see #isSetAnnotations()
	 * @see #unsetAnnotations()
	 * @see #setAnnotations(MClassifierAnnotations)
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_Annotations()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MClassifierAnnotations getAnnotations();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPropertiesContainer#getAnnotations <em>Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotations</em>' containment reference.
	 * @see #isSetAnnotations()
	 * @see #unsetAnnotations()
	 * @see #getAnnotations()
	 * @generated
	 */
	void setAnnotations(MClassifierAnnotations value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPropertiesContainer#getAnnotations <em>Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAnnotations()
	 * @see #getAnnotations()
	 * @see #setAnnotations(MClassifierAnnotations)
	 * @generated
	 */
	void unsetAnnotations();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPropertiesContainer#getAnnotations <em>Annotations</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Annotations</em>' containment reference is set.
	 * @see #unsetAnnotations()
	 * @see #getAnnotations()
	 * @see #setAnnotations(MClassifierAnnotations)
	 * @generated
	 */
	boolean isSetAnnotations();

	/**
	 * Returns the value of the '<em><b>Property Group</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MPropertiesGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Group</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Group</em>' containment reference list.
	 * @see #isSetPropertyGroup()
	 * @see #unsetPropertyGroup()
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_PropertyGroup()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 * @generated
	 */
	EList<MPropertiesGroup> getPropertyGroup();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPropertiesContainer#getPropertyGroup <em>Property Group</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPropertyGroup()
	 * @see #getPropertyGroup()
	 * @generated
	 */
	void unsetPropertyGroup();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPropertiesContainer#getPropertyGroup <em>Property Group</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Property Group</em>' containment reference list is set.
	 * @see #unsetPropertyGroup()
	 * @see #getPropertyGroup()
	 * @generated
	 */
	boolean isSetPropertyGroup();

	/**
	 * Returns the value of the '<em><b>All Constraint Annotations</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MInvariantConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Constraint Annotations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Constraint Annotations</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_AllConstraintAnnotations()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\r\nlet localAnnotations:Sequence(annotations::MInvariantConstraint) = \r\n  if self.annotations.oclIsUndefined()\r\n   then Sequence{}\r\n   else self.annotations.constraint->asSequence() endif\r\nin \r\nlet groupedAnnotations: Sequence(annotations::MInvariantConstraint) = \r\n  self.propertyGroup.allConstraintAnnotations\r\nin  localAnnotations->union(groupedAnnotations)\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MInvariantConstraint> getAllConstraintAnnotations();

	/**
	 * Returns the value of the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Classifier</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_ContainingClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getContainingClassifier();

	/**
	 * Returns the value of the '<em><b>Owned Property</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Property</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Property</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_OwnedProperty()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.property->asSequence()->union(self.propertyGroup.ownedProperty)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MProperty> getOwnedProperty();

	/**
	 * Returns the value of the '<em><b>Nr Of Properties And Signatures</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nr Of Properties And Signatures</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nr Of Properties And Signatures</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPropertiesContainer_NrOfPropertiesAndSignatures()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let ps:OrderedSet(MProperty)= property->select(p:MProperty|p.kind=PropertyKind::Attribute or p.kind = PropertyKind::Reference) \r\nin\r\nlet\r\n nrOfPs:Integer = if ps->isEmpty() then 0 else  ps->size() endif\r\nin\r\nlet os:OrderedSet(MProperty)=  property->select(p:MProperty|p.kind=PropertyKind::Operation) \r\nin\r\nlet sigs:OrderedSet(MOperationSignature)= os.operationSignature->asOrderedSet()\r\nin\r\nlet nrOfSigs:Integer =if sigs->isEmpty() then 0 else  sigs->size() endif\r\nin\r\nlet sigs2:OrderedSet(MOperationSignature)= OrderedSet{} \r\nin\r\nlet nrOfSigs2:Integer = if sigs2->isEmpty() then 0 else sigs2->size() endif\r\nin\r\nlet nestedNr:Integer= \r\nif self.propertyGroup->isEmpty() then 0 else\r\npropertyGroup->iterate(g:MPropertiesGroup;i:Integer=0| i+ g.nrOfPropertiesAndSignatures) endif\r\nin\r\n\r\nnrOfPs+nrOfSigs+nestedNr'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getNrOfPropertiesAndSignatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let localAnnotations:Sequence(annotations::MPropertyAnnotations) = \r\n  if self.annotations.oclIsUndefined()\r\n   then Sequence{}\r\n   else self.annotations.propertyAnnotations->asSequence() endif\r\nin \r\nlet groupedAnnotations: Sequence(annotations::MPropertyAnnotations) = \r\n  self.propertyGroup.allPropertyAnnotations()\r\nin  localAnnotations->union(groupedAnnotations)'"
	 * @generated
	 */
	EList<MPropertyAnnotations> allPropertyAnnotations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let localAnnotations:Sequence(annotations::MOperationAnnotations) = \r\n  if self.annotations.oclIsUndefined()\r\n   then Sequence{}\r\n   else self.annotations.operationAnnotations->asSequence() endif\r\nin \r\nlet groupedAnnotations: Sequence(annotations::MOperationAnnotations) = \r\n  self.propertyGroup.allOperationAnnotations()\r\nin  localAnnotations->union(groupedAnnotations)'"
	 * @generated
	 */
	EList<MOperationAnnotations> allOperationAnnotations();

} // MPropertiesContainer
