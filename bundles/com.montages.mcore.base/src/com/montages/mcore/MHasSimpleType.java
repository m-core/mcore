/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MHas Simple Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MHasSimpleType#getSimpleTypeString <em>Simple Type String</em>}</li>
 *   <li>{@link com.montages.mcore.MHasSimpleType#getHasSimpleDataType <em>Has Simple Data Type</em>}</li>
 *   <li>{@link com.montages.mcore.MHasSimpleType#getHasSimpleModelingType <em>Has Simple Modeling Type</em>}</li>
 *   <li>{@link com.montages.mcore.MHasSimpleType#getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}</li>
 *   <li>{@link com.montages.mcore.MHasSimpleType#getSimpleType <em>Simple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMHasSimpleType()
 * @model abstract="true"
 * @generated
 */

public interface MHasSimpleType extends EObject {
	/**
	 * Returns the value of the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type String</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMHasSimpleType_SimpleTypeString()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.simpleTypeAsString(self.simpleType)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getSimpleTypeString();

	/**
	 * Returns the value of the '<em><b>Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @see com.montages.mcore.McorePackage#getMHasSimpleType_SimpleType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='let stringType: SimpleType = SimpleType::String in  stringType'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	SimpleType getSimpleType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MHasSimpleType#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @generated
	 */
	void setSimpleType(SimpleType value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MHasSimpleType#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @generated
	 */
	void unsetSimpleType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MHasSimpleType#getSimpleType <em>Simple Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Simple Type</em>' attribute is set.
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @generated
	 */
	boolean isSetSimpleType();

	/**
	 * Returns the value of the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Simple Data Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Simple Data Type</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMHasSimpleType_HasSimpleDataType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive=' simpleType=SimpleType::String or\r\n  simpleType=SimpleType::Boolean or\r\n   simpleType=SimpleType::Date or\r\n    simpleType=SimpleType::Double or\r\n     simpleType=SimpleType::Integer or \r\n     simpleType= SimpleType::Any'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getHasSimpleDataType();

	/**
	 * Returns the value of the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Simple Modeling Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Simple Modeling Type</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMHasSimpleType_HasSimpleModelingType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive=' not (simpleType=SimpleType::None or hasSimpleDataType)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getHasSimpleModelingType();

	/**
	 * Returns the value of the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type Is Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type Is Correct</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMHasSimpleType_SimpleTypeIsCorrect()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getSimpleTypeIsCorrect();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if  simpleType=SimpleType::None then \'-\' else\r\nif  simpleType=SimpleType::String then \'String\' else\r\n if simpleType=SimpleType::Boolean then \'Boolean\' else\r\n if  simpleType=SimpleType::Date then \'Date\' else\r\n if  simpleType=SimpleType::Double then \'Double\' else\r\n if  simpleType=SimpleType::Integer then \'Integer\' else\r\n if  simpleType=SimpleType::Annotation then \'Annotation\' else\r\n if  simpleType=SimpleType::Attribute then \'Attribute\' else\r\n if  simpleType=SimpleType::Class then \'Class\' else\r\n if  simpleType=SimpleType::Classifier then \'Classifier\' else\r\n if  simpleType=SimpleType::DataType then \'Data Type\' else\r\n  if simpleType=SimpleType::Enumeration then \'Enumeration\' else\r\n  if  simpleType=SimpleType::Feature then \'Feature\' else\r\n  if  simpleType=SimpleType::KeyValue then \'KeyValue\' else\r\n  if  simpleType=SimpleType::Literal then \'Literal\' else\r\n  if  simpleType=SimpleType::Object then \'EObject\' else\r\n  if simpleType= SimpleType::Any then \'Object\' else\r\n  if  simpleType=SimpleType::Operation then \'Operation\' else\r\n   if simpleType=SimpleType::Package then \'Package\' else\r\n   if  simpleType=SimpleType::Parameter then \'Parameter\' else\r\n   if  simpleType=SimpleType::Reference then \'Reference\' else\r\n   if simpleType=SimpleType::NamedElement then \'Named Element\' else\r\n   if simpleType=SimpleType::TypedElement then \'Typed Element\' else \r\n   \'ERROR\' endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif '"
	 * @generated
	 */
	String simpleTypeAsString(SimpleType simpleType);

} // MHasSimpleType
