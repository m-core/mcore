/**
 */
package com.montages.mcore;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.annotations.MPackageAnnotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPackage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MPackage#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getSubPackage <em>Sub Package</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getPackageAnnotations <em>Package Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getUsePackageContentOnlyWithExplicitFilter <em>Use Package Content Only With Explicit Filter</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getResourceRootClass <em>Resource Root Class</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getSpecialNsURI <em>Special Ns URI</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getSpecialNsPrefix <em>Special Ns Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getSpecialFileName <em>Special File Name</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getUseUUID <em>Use UUID</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getUuidAttributeName <em>Uuid Attribute Name</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getParent <em>Parent</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getContainingPackage <em>Containing Package</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getContainingComponent <em>Containing Component</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getRepresentsCorePackage <em>Represents Core Package</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getAllSubpackages <em>All Subpackages</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getQualifiedName <em>Qualified Name</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getIsRootPackage <em>Is Root Package</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getInternalEPackage <em>Internal EPackage</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDerivedNsURI <em>Derived Ns URI</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDerivedStandardNsURI <em>Derived Standard Ns URI</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDerivedNsPrefix <em>Derived Ns Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDerivedJavaPackageName <em>Derived Java Package Name</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getNamePrefix <em>Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDerivedNamePrefix <em>Derived Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getNamePrefixScope <em>Name Prefix Scope</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getDerivedJsonSchema <em>Derived Json Schema</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getGenerateJsonSchema <em>Generate Json Schema</em>}</li>
 *   <li>{@link com.montages.mcore.MPackage#getGeneratedJsonSchema <em>Generated Json Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMPackage()
 * @model annotation="http://www.xocl.org/OCL label='eName'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL fullLabelDerive='self.localStructuralName' kindLabelDerive='\'Package\'\n' eNameDerive='if self.representsCorePackage then \'ecore\' else \r\nif stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseLower() \r\nelse self.specialEName.camelCaseLower()\r\nendif endif' localStructuralNameDerive='(if self.isRootPackage then \'\' else self.parent.localStructuralName.concat(\'/\') endif).concat(self.eName)'"
 * @generated
 */

public interface MPackage extends MNamed, MModelElement {
	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MPackageAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MPackageAction
	 * @see #setDoAction(MPackageAction)
	 * @see com.montages.mcore.McorePackage#getMPackage_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::MPackageAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='false'"
	 * @generated
	 */
	MPackageAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MPackageAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MPackageAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MPackageAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate generateJsonSchema$Update(Boolean trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	XUpdate doActionUpdate(MPackageAction trg);

	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier</em>' containment reference list.
	 * @see #isSetClassifier()
	 * @see #unsetClassifier()
	 * @see com.montages.mcore.McorePackage#getMPackage_Classifier()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 * @generated
	 */
	EList<MClassifier> getClassifier();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getClassifier <em>Classifier</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetClassifier()
	 * @see #getClassifier()
	 * @generated
	 */
	void unsetClassifier();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getClassifier <em>Classifier</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Classifier</em>' containment reference list is set.
	 * @see #unsetClassifier()
	 * @see #getClassifier()
	 * @generated
	 */
	boolean isSetClassifier();

	/**
	 * Returns the value of the '<em><b>Sub Package</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Package</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Package</em>' containment reference list.
	 * @see #isSetSubPackage()
	 * @see #unsetSubPackage()
	 * @see com.montages.mcore.McorePackage#getMPackage_SubPackage()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 * @generated
	 */
	EList<MPackage> getSubPackage();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getSubPackage <em>Sub Package</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSubPackage()
	 * @see #getSubPackage()
	 * @generated
	 */
	void unsetSubPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getSubPackage <em>Sub Package</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Sub Package</em>' containment reference list is set.
	 * @see #unsetSubPackage()
	 * @see #getSubPackage()
	 * @generated
	 */
	boolean isSetSubPackage();

	/**
	 * Returns the value of the '<em><b>Package Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Annotations</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Annotations</em>' containment reference.
	 * @see #isSetPackageAnnotations()
	 * @see #unsetPackageAnnotations()
	 * @see #setPackageAnnotations(MPackageAnnotations)
	 * @see com.montages.mcore.McorePackage#getMPackage_PackageAnnotations()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MPackageAnnotations getPackageAnnotations();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getPackageAnnotations <em>Package Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Annotations</em>' containment reference.
	 * @see #isSetPackageAnnotations()
	 * @see #unsetPackageAnnotations()
	 * @see #getPackageAnnotations()
	 * @generated
	 */
	void setPackageAnnotations(MPackageAnnotations value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getPackageAnnotations <em>Package Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPackageAnnotations()
	 * @see #getPackageAnnotations()
	 * @see #setPackageAnnotations(MPackageAnnotations)
	 * @generated
	 */
	void unsetPackageAnnotations();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getPackageAnnotations <em>Package Annotations</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Package Annotations</em>' containment reference is set.
	 * @see #unsetPackageAnnotations()
	 * @see #getPackageAnnotations()
	 * @see #setPackageAnnotations(MPackageAnnotations)
	 * @generated
	 */
	boolean isSetPackageAnnotations();

	/**
	 * Returns the value of the '<em><b>Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Prefix</em>' attribute.
	 * @see #isSetNamePrefix()
	 * @see #unsetNamePrefix()
	 * @see #setNamePrefix(String)
	 * @see com.montages.mcore.McorePackage#getMPackage_NamePrefix()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	String getNamePrefix();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getNamePrefix <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Prefix</em>' attribute.
	 * @see #isSetNamePrefix()
	 * @see #unsetNamePrefix()
	 * @see #getNamePrefix()
	 * @generated
	 */
	void setNamePrefix(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getNamePrefix <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamePrefix()
	 * @see #getNamePrefix()
	 * @see #setNamePrefix(String)
	 * @generated
	 */
	void unsetNamePrefix();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getNamePrefix <em>Name Prefix</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name Prefix</em>' attribute is set.
	 * @see #unsetNamePrefix()
	 * @see #getNamePrefix()
	 * @see #setNamePrefix(String)
	 * @generated
	 */
	boolean isSetNamePrefix();

	/**
	 * Returns the value of the '<em><b>Derived Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Name Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Name Prefix</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_DerivedNamePrefix()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let getOtherNamePrefix: String = if (let chain: Boolean = isRootPackage in\nif chain = true then true else false \n  endif) \n  =true \nthen if containingComponent.oclIsUndefined()\n  then null\n  else containingComponent.derivedNamePrefix\nendif\n  else if containingPackage.oclIsUndefined()\n  then null\n  else containingPackage.derivedNamePrefix\nendif\nendif in\nlet resultofDerivedNamePrefix: String = if (let e0: Boolean = not((let e0: Boolean =  namePrefix.oclIsInvalid() or  namePrefix.oclIsUndefined() or (let e0: Boolean = namePrefix = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif)) in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: String = namePrefix in\nif chain.oclIsUndefined() or chain.camelCaseUpper().oclIsUndefined() \n then null \n else chain.camelCaseUpper()\n  endif\n  else getOtherNamePrefix\nendif in\nresultofDerivedNamePrefix\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedNamePrefix();

	/**
	 * Returns the value of the '<em><b>Name Prefix Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Prefix Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Prefix Scope</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMPackage_NamePrefixScope()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let choosePackage: mcore::MPackage = if (isRootPackage) \n  =true \nthen self\n  else if containingPackage.oclIsUndefined()\n  then null\n  else containingPackage.namePrefixScope\nendif\nendif in\nlet resultofNamePrefixScope: mcore::MPackage = if (let e0: Boolean =  namePrefix.oclIsInvalid() or  namePrefix.oclIsUndefined() or (let e0: Boolean = namePrefix = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen choosePackage\n  else self\nendif in\nresultofNamePrefixScope\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	MPackage getNamePrefixScope();

	/**
	 * Returns the value of the '<em><b>Derived Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Json Schema</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Json Schema</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_DerivedJsonSchema()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/GENMODEL propertyMultiLine='true'"
	 *        annotation="http://www.xocl.org/OCL derive='let space : String = \' \' in\nlet lineBreak : String = \'\\n\' in\nlet branch : String = \'{\' in\nlet closedBranch : String = \'}\' in\nlet comma : String = \',\' in\nlet rootPropertyName : String = \'rootProperty\' in \nlet reference : String = \'#/definitions/\' in \nlet jsonHeader: String = \'{\'\n.concat(lineBreak)\n.concat(\' \"$schema\": \"http://json-schema.org/draft-07/schema#\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"title\": \')\n.concat(\'\"\').concat(self.containingComponent.name).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"description\": \').concat(\'\"\').concat(if self.description.oclIsUndefined() then \'no description\' else self.description endif).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"type\": \')\n.concat(\'\"object\"\')\n.concat(comma)\n.concat(lineBreak)\n.concat(\'\"properties\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(\'\"\').concat(rootPropertyName).concat(\'\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(\'\"description\": \').concat(\'\"\').concat(rootPropertyName).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"$ref\":\').concat(space).concat(\'\"\').concat(reference).concat(self.resourceRootClass.eName).concat(\'\"\')\n.concat(lineBreak)\n.concat(closedBranch)\n.concat(lineBreak)\n.concat(closedBranch).concat(comma)\n.concat(lineBreak)\n.concat(\'\"required\":\').concat(space).concat(\'[\').concat(space).concat(\'\"\').concat(rootPropertyName).concat(\'\"\').concat(space).concat(\']\')\nin\njsonHeader.concat(comma)\n.concat(lineBreak)\n.concat(\'\"definitions\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(self.classifier->iterate(test : MClassifier; s : String = \'\' | s.concat(test.derivedJsonSchema).concat(if self.classifier->indexOf(test) = self.classifier->size() then \'}\' else lineBreak.concat(\',\') endif)))\n.concat(lineBreak)\n.concat(closedBranch)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='JSON Schema' createColumn='false'"
	 * @generated
	 */
	String getDerivedJsonSchema();

	/**
	 * Returns the value of the '<em><b>Generate Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Json Schema</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generate Json Schema</em>' attribute.
	 * @see #setGenerateJsonSchema(Boolean)
	 * @see com.montages.mcore.McorePackage#getMPackage_GenerateJsonSchema()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null' update01Object='generateJsonSchema$update01Object' update01Feature='generatedJsonSchema' update01Value='generateJsonSchema$update01Value' update01Mode='MofRedefine'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='JSON Schema' createColumn='false'"
	 * @generated
	 */
	Boolean getGenerateJsonSchema();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getGenerateJsonSchema <em>Generate Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generate Json Schema</em>' attribute.
	 * @see #getGenerateJsonSchema()
	 * @generated
	 */

	void setGenerateJsonSchema(Boolean value);

	/**
	 * Returns the value of the '<em><b>Generated Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generated Json Schema</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generated Json Schema</em>' attribute.
	 * @see #isSetGeneratedJsonSchema()
	 * @see #unsetGeneratedJsonSchema()
	 * @see #setGeneratedJsonSchema(String)
	 * @see com.montages.mcore.McorePackage#getMPackage_GeneratedJsonSchema()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/GENMODEL propertyMultiLine='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='JSON Schema' createColumn='false'"
	 * @generated
	 */
	String getGeneratedJsonSchema();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getGeneratedJsonSchema <em>Generated Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generated Json Schema</em>' attribute.
	 * @see #isSetGeneratedJsonSchema()
	 * @see #unsetGeneratedJsonSchema()
	 * @see #getGeneratedJsonSchema()
	 * @generated
	 */

	void setGeneratedJsonSchema(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getGeneratedJsonSchema <em>Generated Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGeneratedJsonSchema()
	 * @see #getGeneratedJsonSchema()
	 * @see #setGeneratedJsonSchema(String)
	 * @generated
	 */
	void unsetGeneratedJsonSchema();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getGeneratedJsonSchema <em>Generated Json Schema</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Generated Json Schema</em>' attribute is set.
	 * @see #unsetGeneratedJsonSchema()
	 * @see #getGeneratedJsonSchema()
	 * @see #setGeneratedJsonSchema(String)
	 * @generated
	 */
	boolean isSetGeneratedJsonSchema();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='self'"
	 * @generated
	 */
	EList<MPackage> generateJsonSchema$update01Object(Boolean trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model objRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='derivedJsonSchema\n'"
	 * @generated
	 */
	String generateJsonSchema$update01Value(Boolean trg, MPackage obj);

	/**
	 * Returns the value of the '<em><b>Use Package Content Only With Explicit Filter</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Package Content Only With Explicit Filter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Package Content Only With Explicit Filter</em>' attribute.
	 * @see #isSetUsePackageContentOnlyWithExplicitFilter()
	 * @see #unsetUsePackageContentOnlyWithExplicitFilter()
	 * @see #setUsePackageContentOnlyWithExplicitFilter(Boolean)
	 * @see com.montages.mcore.McorePackage#getMPackage_UsePackageContentOnlyWithExplicitFilter()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	Boolean getUsePackageContentOnlyWithExplicitFilter();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getUsePackageContentOnlyWithExplicitFilter <em>Use Package Content Only With Explicit Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Package Content Only With Explicit Filter</em>' attribute.
	 * @see #isSetUsePackageContentOnlyWithExplicitFilter()
	 * @see #unsetUsePackageContentOnlyWithExplicitFilter()
	 * @see #getUsePackageContentOnlyWithExplicitFilter()
	 * @generated
	 */
	void setUsePackageContentOnlyWithExplicitFilter(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getUsePackageContentOnlyWithExplicitFilter <em>Use Package Content Only With Explicit Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUsePackageContentOnlyWithExplicitFilter()
	 * @see #getUsePackageContentOnlyWithExplicitFilter()
	 * @see #setUsePackageContentOnlyWithExplicitFilter(Boolean)
	 * @generated
	 */
	void unsetUsePackageContentOnlyWithExplicitFilter();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getUsePackageContentOnlyWithExplicitFilter <em>Use Package Content Only With Explicit Filter</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Use Package Content Only With Explicit Filter</em>' attribute is set.
	 * @see #unsetUsePackageContentOnlyWithExplicitFilter()
	 * @see #getUsePackageContentOnlyWithExplicitFilter()
	 * @see #setUsePackageContentOnlyWithExplicitFilter(Boolean)
	 * @generated
	 */
	boolean isSetUsePackageContentOnlyWithExplicitFilter();

	/**
	 * Returns the value of the '<em><b>Special Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Special Ns URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Ns URI</em>' attribute.
	 * @see #isSetSpecialNsURI()
	 * @see #unsetSpecialNsURI()
	 * @see #setSpecialNsURI(String)
	 * @see com.montages.mcore.McorePackage#getMPackage_SpecialNsURI()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	String getSpecialNsURI();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getSpecialNsURI <em>Special Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Ns URI</em>' attribute.
	 * @see #isSetSpecialNsURI()
	 * @see #unsetSpecialNsURI()
	 * @see #getSpecialNsURI()
	 * @generated
	 */
	void setSpecialNsURI(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getSpecialNsURI <em>Special Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSpecialNsURI()
	 * @see #getSpecialNsURI()
	 * @see #setSpecialNsURI(String)
	 * @generated
	 */
	void unsetSpecialNsURI();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getSpecialNsURI <em>Special Ns URI</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Special Ns URI</em>' attribute is set.
	 * @see #unsetSpecialNsURI()
	 * @see #getSpecialNsURI()
	 * @see #setSpecialNsURI(String)
	 * @generated
	 */
	boolean isSetSpecialNsURI();

	/**
	 * Returns the value of the '<em><b>Special Ns Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Special Ns Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Ns Prefix</em>' attribute.
	 * @see #isSetSpecialNsPrefix()
	 * @see #unsetSpecialNsPrefix()
	 * @see #setSpecialNsPrefix(String)
	 * @see com.montages.mcore.McorePackage#getMPackage_SpecialNsPrefix()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	String getSpecialNsPrefix();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getSpecialNsPrefix <em>Special Ns Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Ns Prefix</em>' attribute.
	 * @see #isSetSpecialNsPrefix()
	 * @see #unsetSpecialNsPrefix()
	 * @see #getSpecialNsPrefix()
	 * @generated
	 */
	void setSpecialNsPrefix(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getSpecialNsPrefix <em>Special Ns Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSpecialNsPrefix()
	 * @see #getSpecialNsPrefix()
	 * @see #setSpecialNsPrefix(String)
	 * @generated
	 */
	void unsetSpecialNsPrefix();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getSpecialNsPrefix <em>Special Ns Prefix</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Special Ns Prefix</em>' attribute is set.
	 * @see #unsetSpecialNsPrefix()
	 * @see #getSpecialNsPrefix()
	 * @see #setSpecialNsPrefix(String)
	 * @generated
	 */
	boolean isSetSpecialNsPrefix();

	/**
	 * Returns the value of the '<em><b>Special File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This fields stores file name for the ECore file containing th epackage (only for root packages), in case this name does not follow standard naming.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Special File Name</em>' attribute.
	 * @see #isSetSpecialFileName()
	 * @see #unsetSpecialFileName()
	 * @see #setSpecialFileName(String)
	 * @see com.montages.mcore.McorePackage#getMPackage_SpecialFileName()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	String getSpecialFileName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getSpecialFileName <em>Special File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special File Name</em>' attribute.
	 * @see #isSetSpecialFileName()
	 * @see #unsetSpecialFileName()
	 * @see #getSpecialFileName()
	 * @generated
	 */
	void setSpecialFileName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getSpecialFileName <em>Special File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSpecialFileName()
	 * @see #getSpecialFileName()
	 * @see #setSpecialFileName(String)
	 * @generated
	 */
	void unsetSpecialFileName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getSpecialFileName <em>Special File Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Special File Name</em>' attribute is set.
	 * @see #unsetSpecialFileName()
	 * @see #getSpecialFileName()
	 * @see #setSpecialFileName(String)
	 * @generated
	 */
	boolean isSetSpecialFileName();

	/**
	 * Returns the value of the '<em><b>Use UUID</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use UUID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use UUID</em>' attribute.
	 * @see #isSetUseUUID()
	 * @see #unsetUseUUID()
	 * @see #setUseUUID(Boolean)
	 * @see com.montages.mcore.McorePackage#getMPackage_UseUUID()
	 * @model default="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	Boolean getUseUUID();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getUseUUID <em>Use UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use UUID</em>' attribute.
	 * @see #isSetUseUUID()
	 * @see #unsetUseUUID()
	 * @see #getUseUUID()
	 * @generated
	 */
	void setUseUUID(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getUseUUID <em>Use UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUseUUID()
	 * @see #getUseUUID()
	 * @see #setUseUUID(Boolean)
	 * @generated
	 */
	void unsetUseUUID();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getUseUUID <em>Use UUID</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Use UUID</em>' attribute is set.
	 * @see #unsetUseUUID()
	 * @see #getUseUUID()
	 * @see #setUseUUID(Boolean)
	 * @generated
	 */
	boolean isSetUseUUID();

	/**
	 * Returns the value of the '<em><b>Uuid Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uuid Attribute Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uuid Attribute Name</em>' attribute.
	 * @see #isSetUuidAttributeName()
	 * @see #unsetUuidAttributeName()
	 * @see #setUuidAttributeName(String)
	 * @see com.montages.mcore.McorePackage#getMPackage_UuidAttributeName()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='uuid Attribute Name'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	String getUuidAttributeName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getUuidAttributeName <em>Uuid Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uuid Attribute Name</em>' attribute.
	 * @see #isSetUuidAttributeName()
	 * @see #unsetUuidAttributeName()
	 * @see #getUuidAttributeName()
	 * @generated
	 */

	void setUuidAttributeName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getUuidAttributeName <em>Uuid Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUuidAttributeName()
	 * @see #getUuidAttributeName()
	 * @see #setUuidAttributeName(String)
	 * @generated
	 */
	void unsetUuidAttributeName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getUuidAttributeName <em>Uuid Attribute Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Uuid Attribute Name</em>' attribute is set.
	 * @see #unsetUuidAttributeName()
	 * @see #getUuidAttributeName()
	 * @see #setUuidAttributeName(String)
	 * @generated
	 */
	boolean isSetUuidAttributeName();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMPackage_Parent()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isRootPackage then null else self.eContainer().oclAsType(MPackage) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getParent();

	/**
	 * Returns the value of the '<em><b>Containing Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Package</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMPackage_ContainingPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if isRootPackage then null else\r\n eContainer().oclAsType(MPackage) endif '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getContainingPackage();

	/**
	 * Returns the value of the '<em><b>Containing Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Component</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMPackage_ContainingComponent()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsUndefined() then null\r\nelse \r\nif isRootPackage then eContainer().oclAsType(MComponent) \r\nelse containingPackage.containingComponent endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MComponent getContainingComponent();

	/**
	 * Returns the value of the '<em><b>Represents Core Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Represents Core Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Represents Core Package</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_RepresentsCorePackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() \r\n  then false\r\n  else if isRootPackage \r\n    then containingComponent.representsCoreComponent \r\n    else containingPackage.representsCorePackage endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getRepresentsCorePackage();

	/**
	 * Returns the value of the '<em><b>All Subpackages</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Subpackages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Subpackages</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMPackage_AllSubpackages()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='subPackage->union(subPackage->closure(subPackage))'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPackage> getAllSubpackages();

	/**
	 * Returns the value of the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualified Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_QualifiedName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = if (representsCorePackage)= true \n then true \n else if (isRootPackage)= true \n then true \nelse if ((representsCorePackage)= null or (isRootPackage)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: String = eName in\nif chain.allLowerCase().oclIsUndefined() \n then null \n else chain.allLowerCase()\n  endif\n  else (let e0: String = if containingPackage.oclIsUndefined()\n  then null\n  else containingPackage.qualifiedName\nendif.concat(\'::\').concat(let chain02: String = eName in\nif chain02.allLowerCase().oclIsUndefined() \n then null \n else chain02.allLowerCase()\n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getQualifiedName();

	/**
	 * Returns the value of the '<em><b>Is Root Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Root Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Root Package</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_IsRootPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsUndefined() \r\n  then false\r\n  else eContainer().oclIsTypeOf(MComponent) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getIsRootPackage();

	/**
	 * Returns the value of the '<em><b>Internal EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal EPackage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal EPackage</em>' reference.
	 * @see #isSetInternalEPackage()
	 * @see #unsetInternalEPackage()
	 * @see #setInternalEPackage(EPackage)
	 * @see com.montages.mcore.McorePackage#getMPackage_InternalEPackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Relation To ECore'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EPackage getInternalEPackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getInternalEPackage <em>Internal EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal EPackage</em>' reference.
	 * @see #isSetInternalEPackage()
	 * @see #unsetInternalEPackage()
	 * @see #getInternalEPackage()
	 * @generated
	 */
	void setInternalEPackage(EPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getInternalEPackage <em>Internal EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalEPackage()
	 * @see #getInternalEPackage()
	 * @see #setInternalEPackage(EPackage)
	 * @generated
	 */
	void unsetInternalEPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getInternalEPackage <em>Internal EPackage</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal EPackage</em>' reference is set.
	 * @see #unsetInternalEPackage()
	 * @see #getInternalEPackage()
	 * @see #setInternalEPackage(EPackage)
	 * @generated
	 */
	boolean isSetInternalEPackage();

	/**
	 * Returns the value of the '<em><b>Derived Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Ns URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Ns URI</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_DerivedNsURI()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not (self.specialNsURI.oclIsUndefined() or self.specialNsURI.trim().size() = 0) then\r\n\tself.specialNsURI\r\nelse self.derivedStandardNsURI endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='URI'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedNsURI();

	/**
	 * Returns the value of the '<em><b>Derived Standard Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Standard Ns URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Standard Ns URI</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_DerivedStandardNsURI()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isRootPackage then\r\nself.containingComponent.derivedURI.concat(\'/\').concat(self.calculatedShortName.camelCaseUpper()) else\r\nself.containingPackage.derivedStandardNsURI.concat(\'/\').concat(self.calculatedShortName.camelCaseUpper().firstUpperCase())\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='URI'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedStandardNsURI();

	/**
	 * Returns the value of the '<em><b>Derived Ns Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Ns Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Ns Prefix</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_DerivedNsPrefix()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='(if isRootPackage then \'\' else\r\ncontainingPackage.derivedNsPrefix.concat(\'.\') endif)\r\n.concat(calculatedShortName.camelCaseLower().allLowerCase())'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='URI'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedNsPrefix();

	/**
	 * Returns the value of the '<em><b>Derived Java Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Java Package Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Java Package Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMPackage_DerivedJavaPackageName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingComponent.calculatedShortName = containingComponent.rootPackage.calculatedShortName\r\nthen (\r\n\t\tif containingComponent.domainType.oclIsUndefined()\r\n\t\tthen \'MISSING domain type\'\r\n\t\telse containingComponent.derivedDomainType\r\n\t\tendif\r\n\t).concat(\'.\').concat(\r\n\t\tif containingComponent.domainName.oclIsUndefined()\r\n\t\tthen \'MISSING domain name\'\r\n\t\telse containingComponent.derivedDomainName\r\n\t\tendif\r\n\t).concat(\'.\').concat(derivedNsPrefix)\r\nelse\r\n\tcontainingComponent.derivedBundleName.concat(\'.\').concat(derivedNsPrefix)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='URI'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedJavaPackageName();

	/**
	 * Returns the value of the '<em><b>Resource Root Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Root Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Root Class</em>' reference.
	 * @see #isSetResourceRootClass()
	 * @see #unsetResourceRootClass()
	 * @see #setResourceRootClass(MClassifier)
	 * @see com.montages.mcore.McorePackage#getMPackage_ResourceRootClass()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='self->union(allSubpackages)->includes(trg.containingPackage)\r\n and\r\n not (trg.abstractClass)\r\nand\r\n trg.kind = ClassifierKind::ClassType'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	MClassifier getResourceRootClass();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPackage#getResourceRootClass <em>Resource Root Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Root Class</em>' reference.
	 * @see #isSetResourceRootClass()
	 * @see #unsetResourceRootClass()
	 * @see #getResourceRootClass()
	 * @generated
	 */
	void setResourceRootClass(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPackage#getResourceRootClass <em>Resource Root Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResourceRootClass()
	 * @see #getResourceRootClass()
	 * @see #setResourceRootClass(MClassifier)
	 * @generated
	 */
	void unsetResourceRootClass();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPackage#getResourceRootClass <em>Resource Root Class</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Resource Root Class</em>' reference is set.
	 * @see #unsetResourceRootClass()
	 * @see #getResourceRootClass()
	 * @see #setResourceRootClass(MClassifier)
	 * @generated
	 */
	boolean isSetResourceRootClass();

} // MPackage
