/**
 */
package org.langlets.autil.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.autil.ANamed;
import org.langlets.autil.AutilPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ANamed</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.autil.impl.ANamedImpl#getAName <em>AName</em>}</li>
 *   <li>{@link org.langlets.autil.impl.ANamedImpl#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link org.langlets.autil.impl.ANamedImpl#getABusinessNameFromName <em>ABusiness Name From Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class ANamedImpl extends AElementImpl implements ANamed {
	/**
	 * The default value of the '{@link #getAName() <em>AName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName()
	 * @generated
	 * @ordered
	 */
	protected static final String ANAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedNameConstant() <em>AUndefined Name Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_NAME_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABusinessNameFromName() <em>ABusiness Name From Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessNameFromName()
	 * @generated
	 * @ordered
	 */
	protected static final String ABUSINESS_NAME_FROM_NAME_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAName <em>AName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedNameConstant <em>AUndefined Name Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedNameConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABusinessNameFromName <em>ABusiness Name From Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessNameFromName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBusinessNameFromNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ANamedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AutilPackage.Literals.ANAMED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAName() {
		/**
		 * @OCL aUndefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AutilPackage.Literals.ANAMED;
		EStructuralFeature eFeature = AutilPackage.Literals.ANAMED__ANAME;

		if (aNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(),
						AutilPackage.Literals.ANAMED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.ANAMED, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedNameConstant() {
		/**
		 * @OCL let undefinedNameConstant: String = '<A Name Is Undefined> ' in
		undefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AutilPackage.Literals.ANAMED;
		EStructuralFeature eFeature = AutilPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT;

		if (aUndefinedNameConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedNameConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(),
						AutilPackage.Literals.ANAMED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedNameConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.ANAMED, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABusinessNameFromName() {
		/**
		 * @OCL let chain : String = aName in
		if chain.oclIsUndefined()
		then null
		else chain .camelCaseToBusiness()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AutilPackage.Literals.ANAMED;
		EStructuralFeature eFeature = AutilPackage.Literals.ANAMED__ABUSINESS_NAME_FROM_NAME;

		if (aBusinessNameFromNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBusinessNameFromNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(),
						AutilPackage.Literals.ANAMED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBusinessNameFromNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.ANAMED, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AutilPackage.ANAMED__ANAME:
			return getAName();
		case AutilPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
			return getAUndefinedNameConstant();
		case AutilPackage.ANAMED__ABUSINESS_NAME_FROM_NAME:
			return getABusinessNameFromName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AutilPackage.ANAMED__ANAME:
			return ANAME_EDEFAULT == null ? getAName() != null : !ANAME_EDEFAULT.equals(getAName());
		case AutilPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
			return AUNDEFINED_NAME_CONSTANT_EDEFAULT == null ? getAUndefinedNameConstant() != null
					: !AUNDEFINED_NAME_CONSTANT_EDEFAULT.equals(getAUndefinedNameConstant());
		case AutilPackage.ANAMED__ABUSINESS_NAME_FROM_NAME:
			return ABUSINESS_NAME_FROM_NAME_EDEFAULT == null ? getABusinessNameFromName() != null
					: !ABUSINESS_NAME_FROM_NAME_EDEFAULT.equals(getABusinessNameFromName());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel aName
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (AutilPackage.Literals.ANAMED);
		EStructuralFeature eOverrideFeature = AutilPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //ANamedImpl
