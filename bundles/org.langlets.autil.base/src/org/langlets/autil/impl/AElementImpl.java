/**
 */
package org.langlets.autil.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.autil.AElement;
import org.langlets.autil.AutilPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AElement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.autil.impl.AElementImpl#getALabel <em>ALabel</em>}</li>
 *   <li>{@link org.langlets.autil.impl.AElementImpl#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link org.langlets.autil.impl.AElementImpl#getARenderedKind <em>ARendered Kind</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AElementImpl extends MinimalEObjectImpl.Container implements AElement {
	/**
	 * The default value of the '{@link #getALabel() <em>ALabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ALABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAKindBase() <em>AKind Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase()
	 * @generated
	 * @ordered
	 */
	protected static final String AKIND_BASE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getARenderedKind() <em>ARendered Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ARENDERED_KIND_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentLevel <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringOrMissing <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringIsEmpty <em>String Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringIsEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringIsEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #listOfStringToStringWithSeparator <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #listOfStringToStringWithSeparator <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase <em>AKind Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARenderedKind <em>ARendered Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRenderedKindDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AutilPackage.Literals.AELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getALabel() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AutilPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AutilPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKindBase() {
		/**
		 * @OCL self.eClass().name
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AutilPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AutilPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getARenderedKind() {
		/**
		 * @OCL let renderedKind: String = let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in
		if chain12.toUpperCase().oclIsUndefined() 
		then null 
		else chain12.toUpperCase()
		endif) in 
		if e1.oclIsInvalid() then null else e1 endif in
		renderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AutilPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AutilPackage.Literals.AELEMENT__ARENDERED_KIND;

		if (aRenderedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRenderedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, derive, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRenderedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer indentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(AElement)
		then eContainer().oclAsType(AElement).indentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(0);
		if (indentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces() {

		/**
		 * @OCL let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in 
		if e1.oclIsInvalid() then null else e1 endif in
		let result: String = indentationSpaces(numberOfSpaces) in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(1);
		if (indentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces(Integer size) {

		/**
		 * @OCL let sizeMinusOne: Integer =  size - 1 in
		let result: String = if (let e0: Boolean = size < 1 in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then ''
		else (let e0: String = indentationSpaces(sizeMinusOne).concat(' ') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(2);
		if (indentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String stringOrMissing(String p) {

		/**
		 * @OCL let result: String = if (stringIsEmpty(p)) 
		=true 
		then 'MISSING'
		else p
		endif in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(3);
		if (stringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean stringIsEmpty(String s) {

		/**
		 * @OCL let result: Boolean = if ( s.oclIsUndefined()) 
		=true 
		then true else if (let e0: Boolean = '' = let e0: String = s.trim() in 
		if e0.oclIsInvalid() then null else e0 endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(4);
		if (stringIsEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringIsEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringIsEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements, String separator) {

		/**
		 * @OCL let f:String = elements->asOrderedSet()->first() in
		if f.oclIsUndefined()
		then ''
		else if elements-> size()=1 then f else
		let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=''|ac.concat(separator).concat(it)) in
		f.concat(rest)
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(5);
		if (listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			evalEnv.add("separator", separator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements) {

		/**
		 * @OCL let defaultSeparator: String = ', ' in
		listOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(6);
		if (listOfStringToStringWithSeparatorecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AutilPackage.PLUGIN_ID, body, helper.getProblems(),
						AutilPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(listOfStringToStringWithSeparatorecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AutilPackage.PLUGIN_ID, query, AutilPackage.Literals.AELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AutilPackage.AELEMENT__ALABEL:
			return getALabel();
		case AutilPackage.AELEMENT__AKIND_BASE:
			return getAKindBase();
		case AutilPackage.AELEMENT__ARENDERED_KIND:
			return getARenderedKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AutilPackage.AELEMENT__ALABEL:
			return ALABEL_EDEFAULT == null ? getALabel() != null : !ALABEL_EDEFAULT.equals(getALabel());
		case AutilPackage.AELEMENT__AKIND_BASE:
			return AKIND_BASE_EDEFAULT == null ? getAKindBase() != null : !AKIND_BASE_EDEFAULT.equals(getAKindBase());
		case AutilPackage.AELEMENT__ARENDERED_KIND:
			return ARENDERED_KIND_EDEFAULT == null ? getARenderedKind() != null
					: !ARENDERED_KIND_EDEFAULT.equals(getARenderedKind());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case AutilPackage.AELEMENT___INDENT_LEVEL:
			return indentLevel();
		case AutilPackage.AELEMENT___INDENTATION_SPACES:
			return indentationSpaces();
		case AutilPackage.AELEMENT___INDENTATION_SPACES__INTEGER:
			return indentationSpaces((Integer) arguments.get(0));
		case AutilPackage.AELEMENT___STRING_OR_MISSING__STRING:
			return stringOrMissing((String) arguments.get(0));
		case AutilPackage.AELEMENT___STRING_IS_EMPTY__STRING:
			return stringIsEmpty((String) arguments.get(0));
		case AutilPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
			return listOfStringToStringWithSeparator((EList<String>) arguments.get(0), (String) arguments.get(1));
		case AutilPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
			return listOfStringToStringWithSeparator((EList<String>) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //AElementImpl
