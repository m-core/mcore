/**
 */

package com.montages.acore.values;

import com.montages.acore.abstractions.AElement;

import com.montages.acore.classifiers.AClassifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AValue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.values.AValue#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link com.montages.acore.values.AValue#getAContainingSlot <em>AContaining Slot</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.values.ValuesPackage#getAValue()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Value'"
 * @generated
 */

public interface AValue extends AElement {
	/**
	 * Returns the value of the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClassifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClassifier</em>' reference.
	 * @see com.montages.acore.values.ValuesPackage#getAValue_AClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassifier = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Values'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getAClassifier();

	/**
	 * Returns the value of the '<em><b>AContaining Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Slot</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Slot</em>' reference.
	 * @see com.montages.acore.values.ValuesPackage#getAValue_AContainingSlot()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Slot'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::values::ASlot = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Values'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ASlot getAContainingSlot();

} // AValue
