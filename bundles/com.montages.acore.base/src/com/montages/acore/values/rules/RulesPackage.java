/**
 */
package com.montages.acore.values.rules;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.acore.values.rules.RulesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.acore.values'"
 * @generated
 */
public interface RulesPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "rules";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/aCore/ACore/Values/Rules";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.values.rules";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RulesPackage eINSTANCE = com.montages.acore.values.rules.impl.RulesPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.acore.values.rules.impl.ARuleImpl <em>ARule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.values.rules.impl.ARuleImpl
	 * @see com.montages.acore.values.rules.impl.RulesPackageImpl#getARule()
	 * @generated
	 */
	int ARULE = 0;

	/**
	 * The feature id for the '<em><b>ASelf Context</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE__ASELF_CONTEXT = 0;

	/**
	 * The feature id for the '<em><b>ATrg Context</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE__ATRG_CONTEXT = 1;

	/**
	 * The feature id for the '<em><b>ATrg Context Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE__ATRG_CONTEXT_SINGULAR = 2;

	/**
	 * The feature id for the '<em><b>ASrc Context</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE__ASRC_CONTEXT = 3;

	/**
	 * The feature id for the '<em><b>ASrc Context Singular</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE__ASRC_CONTEXT_SINGULAR = 4;

	/**
	 * The number of structural features of the '<em>ARule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>ARule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARULE_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link com.montages.acore.values.rules.ARule <em>ARule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ARule</em>'.
	 * @see com.montages.acore.values.rules.ARule
	 * @generated
	 */
	EClass getARule();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.rules.ARule#getASelfContext <em>ASelf Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASelf Context</em>'.
	 * @see com.montages.acore.values.rules.ARule#getASelfContext()
	 * @see #getARule()
	 * @generated
	 */
	EReference getARule_ASelfContext();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.rules.ARule#getATrgContext <em>ATrg Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ATrg Context</em>'.
	 * @see com.montages.acore.values.rules.ARule#getATrgContext()
	 * @see #getARule()
	 * @generated
	 */
	EReference getARule_ATrgContext();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.rules.ARule#getATrgContextSingular <em>ATrg Context Singular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ATrg Context Singular</em>'.
	 * @see com.montages.acore.values.rules.ARule#getATrgContextSingular()
	 * @see #getARule()
	 * @generated
	 */
	EAttribute getARule_ATrgContextSingular();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.rules.ARule#getASrcContext <em>ASrc Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASrc Context</em>'.
	 * @see com.montages.acore.values.rules.ARule#getASrcContext()
	 * @see #getARule()
	 * @generated
	 */
	EReference getARule_ASrcContext();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.acore.values.rules.ARule#getASrcContextSingular <em>ASrc Context Singular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>ASrc Context Singular</em>'.
	 * @see com.montages.acore.values.rules.ARule#getASrcContextSingular()
	 * @see #getARule()
	 * @generated
	 */
	EAttribute getARule_ASrcContextSingular();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RulesFactory getRulesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.acore.values.rules.impl.ARuleImpl <em>ARule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.values.rules.impl.ARuleImpl
		 * @see com.montages.acore.values.rules.impl.RulesPackageImpl#getARule()
		 * @generated
		 */
		EClass ARULE = eINSTANCE.getARule();

		/**
		 * The meta object literal for the '<em><b>ASelf Context</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARULE__ASELF_CONTEXT = eINSTANCE.getARule_ASelfContext();

		/**
		 * The meta object literal for the '<em><b>ATrg Context</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARULE__ATRG_CONTEXT = eINSTANCE.getARule_ATrgContext();

		/**
		 * The meta object literal for the '<em><b>ATrg Context Singular</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARULE__ATRG_CONTEXT_SINGULAR = eINSTANCE.getARule_ATrgContextSingular();

		/**
		 * The meta object literal for the '<em><b>ASrc Context</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARULE__ASRC_CONTEXT = eINSTANCE.getARule_ASrcContext();

		/**
		 * The meta object literal for the '<em><b>ASrc Context Singular</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARULE__ASRC_CONTEXT_SINGULAR = eINSTANCE.getARule_ASrcContextSingular();

	}

} //RulesPackage
