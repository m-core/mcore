/**
 */

package com.montages.acore.values.rules.impl;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;

import com.montages.acore.values.rules.ARule;
import com.montages.acore.values.rules.RulesPackage;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ARule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.values.rules.impl.ARuleImpl#getASelfContext <em>ASelf Context</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.impl.ARuleImpl#getATrgContext <em>ATrg Context</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.impl.ARuleImpl#getATrgContextSingular <em>ATrg Context Singular</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.impl.ARuleImpl#getASrcContext <em>ASrc Context</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.impl.ARuleImpl#getASrcContextSingular <em>ASrc Context Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class ARuleImpl extends MinimalEObjectImpl.Container implements ARule {
	/**
	 * The default value of the '{@link #getATrgContextSingular() <em>ATrg Context Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATrgContextSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ATRG_CONTEXT_SINGULAR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getASrcContext() <em>ASrc Context</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASrcContext()
	 * @generated
	 * @ordered
	 */
	protected EList<AClassifier> aSrcContext;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASelfContext <em>ASelf Context</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASelfContext
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSelfContextDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATrgContext <em>ATrg Context</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATrgContext
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTrgContextDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATrgContextSingular <em>ATrg Context Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATrgContextSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTrgContextSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASrcContextSingular <em>ASrc Context Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASrcContextSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSrcContextSingularDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ARuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RulesPackage.Literals.ARULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassType> getASelfContext() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = RulesPackage.Literals.ARULE;
		EStructuralFeature eFeature = RulesPackage.Literals.ARULE__ASELF_CONTEXT;

		if (aSelfContextDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSelfContextDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(RulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						RulesPackage.Literals.ARULE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSelfContextDeriveOCL);
		try {
			XoclErrorHandler.enterContext(RulesPackage.PLUGIN_ID, query, RulesPackage.Literals.ARULE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AClassType> result = (EList<AClassType>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassifier> getATrgContext() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = RulesPackage.Literals.ARULE;
		EStructuralFeature eFeature = RulesPackage.Literals.ARULE__ATRG_CONTEXT;

		if (aTrgContextDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTrgContextDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(RulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						RulesPackage.Literals.ARULE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTrgContextDeriveOCL);
		try {
			XoclErrorHandler.enterContext(RulesPackage.PLUGIN_ID, query, RulesPackage.Literals.ARULE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AClassifier> result = (EList<AClassifier>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getATrgContextSingular() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = RulesPackage.Literals.ARULE;
		EStructuralFeature eFeature = RulesPackage.Literals.ARULE__ATRG_CONTEXT_SINGULAR;

		if (aTrgContextSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTrgContextSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(RulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						RulesPackage.Literals.ARULE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTrgContextSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(RulesPackage.PLUGIN_ID, query, RulesPackage.Literals.ARULE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassifier> getASrcContext() {
		if (aSrcContext == null) {
			aSrcContext = new EObjectResolvingEList.Unsettable<AClassifier>(AClassifier.class, this,
					RulesPackage.ARULE__ASRC_CONTEXT);
		}
		return aSrcContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetASrcContext() {
		if (aSrcContext != null)
			((InternalEList.Unsettable<?>) aSrcContext).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetASrcContext() {
		return aSrcContext != null && ((InternalEList.Unsettable<?>) aSrcContext).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Boolean> getASrcContextSingular() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = RulesPackage.Literals.ARULE;
		EStructuralFeature eFeature = RulesPackage.Literals.ARULE__ASRC_CONTEXT_SINGULAR;

		if (aSrcContextSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSrcContextSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(RulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						RulesPackage.Literals.ARULE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSrcContextSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(RulesPackage.PLUGIN_ID, query, RulesPackage.Literals.ARULE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<Boolean> result = (EList<Boolean>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case RulesPackage.ARULE__ASELF_CONTEXT:
			return getASelfContext();
		case RulesPackage.ARULE__ATRG_CONTEXT:
			return getATrgContext();
		case RulesPackage.ARULE__ATRG_CONTEXT_SINGULAR:
			return getATrgContextSingular();
		case RulesPackage.ARULE__ASRC_CONTEXT:
			return getASrcContext();
		case RulesPackage.ARULE__ASRC_CONTEXT_SINGULAR:
			return getASrcContextSingular();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case RulesPackage.ARULE__ASRC_CONTEXT:
			getASrcContext().clear();
			getASrcContext().addAll((Collection<? extends AClassifier>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case RulesPackage.ARULE__ASRC_CONTEXT:
			unsetASrcContext();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case RulesPackage.ARULE__ASELF_CONTEXT:
			return !getASelfContext().isEmpty();
		case RulesPackage.ARULE__ATRG_CONTEXT:
			return !getATrgContext().isEmpty();
		case RulesPackage.ARULE__ATRG_CONTEXT_SINGULAR:
			return ATRG_CONTEXT_SINGULAR_EDEFAULT == null ? getATrgContextSingular() != null
					: !ATRG_CONTEXT_SINGULAR_EDEFAULT.equals(getATrgContextSingular());
		case RulesPackage.ARULE__ASRC_CONTEXT:
			return isSetASrcContext();
		case RulesPackage.ARULE__ASRC_CONTEXT_SINGULAR:
			return !getASrcContextSingular().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ARuleImpl
