/**
 */

package com.montages.acore.annotations;

import com.montages.acore.abstractions.AAnnotation;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AFeature Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.annotations.AFeatureAnnotation#getAAnnotatedFeature <em>AAnnotated Feature</em>}</li>
 *   <li>{@link com.montages.acore.annotations.AFeatureAnnotation#getAAnnotationContainingClass <em>AAnnotation Containing Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.annotations.AnnotationsPackage#getAFeatureAnnotation()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Feature Annotation'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aAnnotatedDerive='aAnnotatedFeature\n'"
 * @generated
 */

public interface AFeatureAnnotation extends AAnnotation {
	/**
	 * Returns the value of the '<em><b>AAnnotated Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotated Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotated Feature</em>' reference.
	 * @see com.montages.acore.annotations.AnnotationsPackage#getAFeatureAnnotation_AAnnotatedFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Annotated Feature'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Annotations' createColumn='false'"
	 * @generated
	 */
	AFeature getAAnnotatedFeature();

	/**
	 * Returns the value of the '<em><b>AAnnotation Containing Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotation Containing Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotation Containing Class</em>' reference.
	 * @see com.montages.acore.annotations.AnnotationsPackage#getAFeatureAnnotation_AAnnotationContainingClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Annotation Containing Class'"
	 *        annotation="http://www.xocl.org/OCL derive='if aAnnotatedFeature.oclIsUndefined()\n  then null\n  else aAnnotatedFeature.aContainingClass\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Annotations' createColumn='false'"
	 * @generated
	 */
	AClassType getAAnnotationContainingClass();

} // AFeatureAnnotation
