/**
 */

package com.montages.acore.annotations;

import com.montages.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AObject Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.acore.annotations.AnnotationsPackage#getAObjectConstraint()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Object Constraint'"
 * @generated
 */

public interface AObjectConstraint extends AClassAnnotation, ANamed {
} // AObjectConstraint
