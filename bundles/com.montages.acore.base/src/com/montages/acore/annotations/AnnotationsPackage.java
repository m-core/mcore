/**
 */
package com.montages.acore.annotations;

import com.montages.acore.abstractions.AbstractionsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.acore.annotations.AnnotationsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.acore'"
 * @generated
 */
public interface AnnotationsPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "annotations";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/aCore/ACore/Annotations";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.annotations";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AnnotationsPackage eINSTANCE = com.montages.acore.annotations.impl.AnnotationsPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.acore.annotations.impl.AClassAnnotationImpl <em>AClass Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.annotations.impl.AClassAnnotationImpl
	 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAClassAnnotation()
	 * @generated
	 */
	int ACLASS_ANNOTATION = 0;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_ANNOTATION__AANNOTATED = AbstractionsPackage.AANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_ANNOTATION__AANNOTATING = AbstractionsPackage.AANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>AAnnotated Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_ANNOTATION__AANNOTATED_CLASS = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AClass Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_ANNOTATION_FEATURE_COUNT = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AClass Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_ANNOTATION_OPERATION_COUNT = AbstractionsPackage.AANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.annotations.impl.AObjectLabelImpl <em>AObject Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.annotations.impl.AObjectLabelImpl
	 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAObjectLabel()
	 * @generated
	 */
	int AOBJECT_LABEL = 1;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_LABEL__AANNOTATED = ACLASS_ANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_LABEL__AANNOTATING = ACLASS_ANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>AAnnotated Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_LABEL__AANNOTATED_CLASS = ACLASS_ANNOTATION__AANNOTATED_CLASS;

	/**
	 * The number of structural features of the '<em>AObject Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_LABEL_FEATURE_COUNT = ACLASS_ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>AObject Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_LABEL_OPERATION_COUNT = ACLASS_ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.annotations.impl.AObjectConstraintImpl <em>AObject Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.annotations.impl.AObjectConstraintImpl
	 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAObjectConstraint()
	 * @generated
	 */
	int AOBJECT_CONSTRAINT = 2;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AANNOTATED = ACLASS_ANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AANNOTATING = ACLASS_ANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>AAnnotated Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AANNOTATED_CLASS = ACLASS_ANNOTATION__AANNOTATED_CLASS;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__ALABEL = ACLASS_ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AKIND_BASE = ACLASS_ANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__ARENDERED_KIND = ACLASS_ANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__ACONTAINING_COMPONENT = ACLASS_ANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_PACKAGE_URI = ACLASS_ANNOTATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME = ACLASS_ANNOTATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_FEATURE_NAME = ACLASS_ANNOTATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_PACKAGE = ACLASS_ANNOTATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_CLASSIFIER = ACLASS_ANNOTATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_FEATURE = ACLASS_ANNOTATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AT_CORE_ASTRING_CLASS = ACLASS_ANNOTATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__ANAME = ACLASS_ANNOTATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__AUNDEFINED_NAME_CONSTANT = ACLASS_ANNOTATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT__ABUSINESS_NAME = ACLASS_ANNOTATION_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>AObject Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT_FEATURE_COUNT = ACLASS_ANNOTATION_FEATURE_COUNT + 14;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___AINDENT_LEVEL = ACLASS_ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___AINDENTATION_SPACES = ACLASS_ANNOTATION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___AINDENTATION_SPACES__INTEGER = ACLASS_ANNOTATION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ASTRING_OR_MISSING__STRING = ACLASS_ANNOTATION_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ASTRING_IS_EMPTY__STRING = ACLASS_ANNOTATION_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ACLASS_ANNOTATION_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ACLASS_ANNOTATION_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___APACKAGE_FROM_URI__STRING = ACLASS_ANNOTATION_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ACLASS_ANNOTATION_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ACLASS_ANNOTATION_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ACORE_ASTRING_CLASS = ACLASS_ANNOTATION_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ACORE_AREAL_CLASS = ACLASS_ANNOTATION_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ACORE_AINTEGER_CLASS = ACLASS_ANNOTATION_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT___ACORE_AOBJECT_CLASS = ACLASS_ANNOTATION_OPERATION_COUNT + 13;

	/**
	 * The number of operations of the '<em>AObject Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_CONSTRAINT_OPERATION_COUNT = ACLASS_ANNOTATION_OPERATION_COUNT + 14;

	/**
	 * The meta object id for the '{@link com.montages.acore.annotations.impl.AFeatureAnnotationImpl <em>AFeature Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.annotations.impl.AFeatureAnnotationImpl
	 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAFeatureAnnotation()
	 * @generated
	 */
	int AFEATURE_ANNOTATION = 3;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_ANNOTATION__AANNOTATED = AbstractionsPackage.AANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_ANNOTATION__AANNOTATING = AbstractionsPackage.AANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>AAnnotated Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_ANNOTATION__AANNOTATED_FEATURE = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AAnnotation Containing Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_ANNOTATION__AANNOTATION_CONTAINING_CLASS = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AFeature Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_ANNOTATION_FEATURE_COUNT = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>AFeature Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_ANNOTATION_OPERATION_COUNT = AbstractionsPackage.AANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.annotations.impl.AOperationAnnotationImpl <em>AOperation Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.annotations.impl.AOperationAnnotationImpl
	 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAOperationAnnotation()
	 * @generated
	 */
	int AOPERATION_ANNOTATION = 4;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_ANNOTATION__AANNOTATED = AbstractionsPackage.AANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_ANNOTATION__AANNOTATING = AbstractionsPackage.AANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>AAnnotated Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_ANNOTATION__AANNOTATED_OPERATION = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AOperation Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_ANNOTATION_FEATURE_COUNT = AbstractionsPackage.AANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AOperation Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_ANNOTATION_OPERATION_COUNT = AbstractionsPackage.AANNOTATION_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link com.montages.acore.annotations.AClassAnnotation <em>AClass Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AClass Annotation</em>'.
	 * @see com.montages.acore.annotations.AClassAnnotation
	 * @generated
	 */
	EClass getAClassAnnotation();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.annotations.AClassAnnotation#getAAnnotatedClass <em>AAnnotated Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAnnotated Class</em>'.
	 * @see com.montages.acore.annotations.AClassAnnotation#getAAnnotatedClass()
	 * @see #getAClassAnnotation()
	 * @generated
	 */
	EReference getAClassAnnotation_AAnnotatedClass();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.annotations.AObjectLabel <em>AObject Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AObject Label</em>'.
	 * @see com.montages.acore.annotations.AObjectLabel
	 * @generated
	 */
	EClass getAObjectLabel();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.annotations.AObjectConstraint <em>AObject Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AObject Constraint</em>'.
	 * @see com.montages.acore.annotations.AObjectConstraint
	 * @generated
	 */
	EClass getAObjectConstraint();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.annotations.AFeatureAnnotation <em>AFeature Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AFeature Annotation</em>'.
	 * @see com.montages.acore.annotations.AFeatureAnnotation
	 * @generated
	 */
	EClass getAFeatureAnnotation();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.annotations.AFeatureAnnotation#getAAnnotatedFeature <em>AAnnotated Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAnnotated Feature</em>'.
	 * @see com.montages.acore.annotations.AFeatureAnnotation#getAAnnotatedFeature()
	 * @see #getAFeatureAnnotation()
	 * @generated
	 */
	EReference getAFeatureAnnotation_AAnnotatedFeature();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.annotations.AFeatureAnnotation#getAAnnotationContainingClass <em>AAnnotation Containing Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAnnotation Containing Class</em>'.
	 * @see com.montages.acore.annotations.AFeatureAnnotation#getAAnnotationContainingClass()
	 * @see #getAFeatureAnnotation()
	 * @generated
	 */
	EReference getAFeatureAnnotation_AAnnotationContainingClass();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.annotations.AOperationAnnotation <em>AOperation Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AOperation Annotation</em>'.
	 * @see com.montages.acore.annotations.AOperationAnnotation
	 * @generated
	 */
	EClass getAOperationAnnotation();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.annotations.AOperationAnnotation#getAAnnotatedOperation <em>AAnnotated Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAnnotated Operation</em>'.
	 * @see com.montages.acore.annotations.AOperationAnnotation#getAAnnotatedOperation()
	 * @see #getAOperationAnnotation()
	 * @generated
	 */
	EReference getAOperationAnnotation_AAnnotatedOperation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AnnotationsFactory getAnnotationsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.acore.annotations.impl.AClassAnnotationImpl <em>AClass Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.annotations.impl.AClassAnnotationImpl
		 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAClassAnnotation()
		 * @generated
		 */
		EClass ACLASS_ANNOTATION = eINSTANCE.getAClassAnnotation();

		/**
		 * The meta object literal for the '<em><b>AAnnotated Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_ANNOTATION__AANNOTATED_CLASS = eINSTANCE.getAClassAnnotation_AAnnotatedClass();

		/**
		 * The meta object literal for the '{@link com.montages.acore.annotations.impl.AObjectLabelImpl <em>AObject Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.annotations.impl.AObjectLabelImpl
		 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAObjectLabel()
		 * @generated
		 */
		EClass AOBJECT_LABEL = eINSTANCE.getAObjectLabel();

		/**
		 * The meta object literal for the '{@link com.montages.acore.annotations.impl.AObjectConstraintImpl <em>AObject Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.annotations.impl.AObjectConstraintImpl
		 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAObjectConstraint()
		 * @generated
		 */
		EClass AOBJECT_CONSTRAINT = eINSTANCE.getAObjectConstraint();

		/**
		 * The meta object literal for the '{@link com.montages.acore.annotations.impl.AFeatureAnnotationImpl <em>AFeature Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.annotations.impl.AFeatureAnnotationImpl
		 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAFeatureAnnotation()
		 * @generated
		 */
		EClass AFEATURE_ANNOTATION = eINSTANCE.getAFeatureAnnotation();

		/**
		 * The meta object literal for the '<em><b>AAnnotated Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AFEATURE_ANNOTATION__AANNOTATED_FEATURE = eINSTANCE.getAFeatureAnnotation_AAnnotatedFeature();

		/**
		 * The meta object literal for the '<em><b>AAnnotation Containing Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AFEATURE_ANNOTATION__AANNOTATION_CONTAINING_CLASS = eINSTANCE
				.getAFeatureAnnotation_AAnnotationContainingClass();

		/**
		 * The meta object literal for the '{@link com.montages.acore.annotations.impl.AOperationAnnotationImpl <em>AOperation Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.annotations.impl.AOperationAnnotationImpl
		 * @see com.montages.acore.annotations.impl.AnnotationsPackageImpl#getAOperationAnnotation()
		 * @generated
		 */
		EClass AOPERATION_ANNOTATION = eINSTANCE.getAOperationAnnotation();

		/**
		 * The meta object literal for the '<em><b>AAnnotated Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOPERATION_ANNOTATION__AANNOTATED_OPERATION = eINSTANCE
				.getAOperationAnnotation_AAnnotatedOperation();

	}

} //AnnotationsPackage
