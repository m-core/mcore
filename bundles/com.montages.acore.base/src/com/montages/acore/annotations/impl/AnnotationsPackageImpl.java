/**
 */
package com.montages.acore.annotations.impl;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.impl.AbstractionsPackageImpl;

import com.montages.acore.annotations.AClassAnnotation;
import com.montages.acore.annotations.AFeatureAnnotation;
import com.montages.acore.annotations.AObjectConstraint;
import com.montages.acore.annotations.AObjectLabel;
import com.montages.acore.annotations.AOperationAnnotation;
import com.montages.acore.annotations.AnnotationsFactory;
import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.classifiers.impl.ClassifiersPackageImpl;

import com.montages.acore.impl.AcorePackageImpl;

import com.montages.acore.updates.UpdatesPackage;

import com.montages.acore.updates.impl.UpdatesPackageImpl;

import com.montages.acore.values.ValuesPackage;

import com.montages.acore.values.impl.ValuesPackageImpl;

import com.montages.acore.values.rules.RulesPackage;

import com.montages.acore.values.rules.impl.RulesPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationsPackageImpl extends EPackageImpl implements AnnotationsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aClassAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aObjectLabelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aObjectConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aFeatureAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aOperationAnnotationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.acore.annotations.AnnotationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AnnotationsPackageImpl() {
		super(eNS_URI, AnnotationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AnnotationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AnnotationsPackage init() {
		if (isInited)
			return (AnnotationsPackage) EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);

		// Obtain or create and register package
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new AnnotationsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		RulesPackageImpl theRulesPackage = (RulesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theAnnotationsPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theRulesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theAnnotationsPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theRulesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAnnotationsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AnnotationsPackage.eNS_URI, theAnnotationsPackage);
		return theAnnotationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAClassAnnotation() {
		return aClassAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassAnnotation_AAnnotatedClass() {
		return (EReference) aClassAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAObjectLabel() {
		return aObjectLabelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAObjectConstraint() {
		return aObjectConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAFeatureAnnotation() {
		return aFeatureAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAFeatureAnnotation_AAnnotatedFeature() {
		return (EReference) aFeatureAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAFeatureAnnotation_AAnnotationContainingClass() {
		return (EReference) aFeatureAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAOperationAnnotation() {
		return aOperationAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAOperationAnnotation_AAnnotatedOperation() {
		return (EReference) aOperationAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsFactory getAnnotationsFactory() {
		return (AnnotationsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aClassAnnotationEClass = createEClass(ACLASS_ANNOTATION);
		createEReference(aClassAnnotationEClass, ACLASS_ANNOTATION__AANNOTATED_CLASS);

		aObjectLabelEClass = createEClass(AOBJECT_LABEL);

		aObjectConstraintEClass = createEClass(AOBJECT_CONSTRAINT);

		aFeatureAnnotationEClass = createEClass(AFEATURE_ANNOTATION);
		createEReference(aFeatureAnnotationEClass, AFEATURE_ANNOTATION__AANNOTATED_FEATURE);
		createEReference(aFeatureAnnotationEClass, AFEATURE_ANNOTATION__AANNOTATION_CONTAINING_CLASS);

		aOperationAnnotationEClass = createEClass(AOPERATION_ANNOTATION);
		createEReference(aOperationAnnotationEClass, AOPERATION_ANNOTATION__AANNOTATED_OPERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AbstractionsPackage theAbstractionsPackage = (AbstractionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aClassAnnotationEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotation());
		aObjectLabelEClass.getESuperTypes().add(this.getAClassAnnotation());
		aObjectConstraintEClass.getESuperTypes().add(this.getAClassAnnotation());
		aObjectConstraintEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aFeatureAnnotationEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotation());
		aOperationAnnotationEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotation());

		// Initialize classes, features, and operations; add parameters
		initEClass(aClassAnnotationEClass, AClassAnnotation.class, "AClassAnnotation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAClassAnnotation_AAnnotatedClass(), theClassifiersPackage.getAClassType(), null,
				"aAnnotatedClass", null, 0, 1, AClassAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aObjectLabelEClass, AObjectLabel.class, "AObjectLabel", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(aObjectConstraintEClass, AObjectConstraint.class, "AObjectConstraint", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(aFeatureAnnotationEClass, AFeatureAnnotation.class, "AFeatureAnnotation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAFeatureAnnotation_AAnnotatedFeature(), theClassifiersPackage.getAFeature(), null,
				"aAnnotatedFeature", null, 0, 1, AFeatureAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAFeatureAnnotation_AAnnotationContainingClass(), theClassifiersPackage.getAClassType(), null,
				"aAnnotationContainingClass", null, 0, 1, AFeatureAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aOperationAnnotationEClass, AOperationAnnotation.class, "AOperationAnnotation", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAOperationAnnotation_AAnnotatedOperation(), theClassifiersPackage.getAOperation(), null,
				"aAnnotatedOperation", null, 0, 1, AOperationAnnotation.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Create annotations
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(aClassAnnotationEClass, source, new String[] { "mName", "Class Annotation" });
		addAnnotation(getAClassAnnotation_AAnnotatedClass(), source, new String[] { "mName", "Annotated Class" });
		addAnnotation(aObjectLabelEClass, source, new String[] { "mName", "Object Label" });
		addAnnotation(aObjectConstraintEClass, source, new String[] { "mName", "Object Constraint" });
		addAnnotation(aFeatureAnnotationEClass, source, new String[] { "mName", "Feature Annotation" });
		addAnnotation(getAFeatureAnnotation_AAnnotatedFeature(), source, new String[] { "mName", "Annotated Feature" });
		addAnnotation(getAFeatureAnnotation_AAnnotationContainingClass(), source,
				new String[] { "mName", "Annotation Containing Class" });
		addAnnotation(aOperationAnnotationEClass, source, new String[] { "mName", "Operation Annotation" });
		addAnnotation(getAOperationAnnotation_AAnnotatedOperation(), source,
				new String[] { "mName", "Annotated Operation" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aClassAnnotationEClass, source, new String[] { "aAnnotatedDerive", "aAnnotatedClass\n" });
		addAnnotation(aFeatureAnnotationEClass, source, new String[] { "aAnnotatedDerive", "aAnnotatedFeature\n" });
		addAnnotation(aOperationAnnotationEClass, source, new String[] { "aAnnotatedDerive", "aAnnotatedOperation\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getAClassAnnotation_AAnnotatedClass(), source, new String[] { "derive", "null\n" });
		addAnnotation(getAFeatureAnnotation_AAnnotatedFeature(), source, new String[] { "derive", "null\n" });
		addAnnotation(getAFeatureAnnotation_AAnnotationContainingClass(), source, new String[] { "derive",
				"if aAnnotatedFeature.oclIsUndefined()\n  then null\n  else aAnnotatedFeature.aContainingClass\nendif\n" });
		addAnnotation(getAOperationAnnotation_AAnnotatedOperation(), source, new String[] { "derive", "null\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getAClassAnnotation_AAnnotatedClass(), source,
				new String[] { "propertyCategory", "z A Core/Annotations", "createColumn", "false" });
		addAnnotation(getAFeatureAnnotation_AAnnotatedFeature(), source,
				new String[] { "propertyCategory", "z A Core/Annotations", "createColumn", "false" });
		addAnnotation(getAFeatureAnnotation_AAnnotationContainingClass(), source,
				new String[] { "propertyCategory", "z A Core/Annotations", "createColumn", "false" });
		addAnnotation(getAOperationAnnotation_AAnnotatedOperation(), source,
				new String[] { "propertyCategory", "z A Core/Annotations", "createColumn", "false" });
	}

} //AnnotationsPackageImpl
