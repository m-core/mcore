/**
 */

package com.montages.acore.annotations.impl;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.impl.AAnnotationImpl;

import com.montages.acore.annotations.AFeatureAnnotation;
import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AFeature;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AFeature Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.annotations.impl.AFeatureAnnotationImpl#getAAnnotatedFeature <em>AAnnotated Feature</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AFeatureAnnotationImpl#getAAnnotationContainingClass <em>AAnnotation Containing Class</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AFeatureAnnotationImpl extends AAnnotationImpl implements AFeatureAnnotation {
	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotatedFeature <em>AAnnotated Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotatedFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotatedFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotationContainingClass <em>AAnnotation Containing Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotationContainingClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotationContainingClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotated <em>AAnnotated</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotated
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aAnnotatedDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AFeatureAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.AFEATURE_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getAAnnotatedFeature() {
		AFeature aAnnotatedFeature = basicGetAAnnotatedFeature();
		return aAnnotatedFeature != null && aAnnotatedFeature.eIsProxy()
				? (AFeature) eResolveProxy((InternalEObject) aAnnotatedFeature) : aAnnotatedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetAAnnotatedFeature() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AFEATURE_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.AFEATURE_ANNOTATION__AANNOTATED_FEATURE;

		if (aAnnotatedFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotatedFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AFEATURE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatedFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AFEATURE_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AFeature result = (AFeature) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType getAAnnotationContainingClass() {
		AClassType aAnnotationContainingClass = basicGetAAnnotationContainingClass();
		return aAnnotationContainingClass != null && aAnnotationContainingClass.eIsProxy()
				? (AClassType) eResolveProxy((InternalEObject) aAnnotationContainingClass) : aAnnotationContainingClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType basicGetAAnnotationContainingClass() {
		/**
		 * @OCL if aAnnotatedFeature.oclIsUndefined()
		then null
		else aAnnotatedFeature.aContainingClass
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AFEATURE_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.AFEATURE_ANNOTATION__AANNOTATION_CONTAINING_CLASS;

		if (aAnnotationContainingClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotationContainingClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AFEATURE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotationContainingClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AFEATURE_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassType result = (AClassType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.AFEATURE_ANNOTATION__AANNOTATED_FEATURE:
			if (resolve)
				return getAAnnotatedFeature();
			return basicGetAAnnotatedFeature();
		case AnnotationsPackage.AFEATURE_ANNOTATION__AANNOTATION_CONTAINING_CLASS:
			if (resolve)
				return getAAnnotationContainingClass();
			return basicGetAAnnotationContainingClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.AFEATURE_ANNOTATION__AANNOTATED_FEATURE:
			return basicGetAAnnotatedFeature() != null;
		case AnnotationsPackage.AFEATURE_ANNOTATION__AANNOTATION_CONTAINING_CLASS:
			return basicGetAAnnotationContainingClass() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aAnnotated aAnnotatedFeature
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AAnnotatable basicGetAAnnotated() {
		EClass eClass = (AnnotationsPackage.Literals.AFEATURE_ANNOTATION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATED;

		if (aAnnotatedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aAnnotatedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AAnnotatable) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AFeatureAnnotationImpl
