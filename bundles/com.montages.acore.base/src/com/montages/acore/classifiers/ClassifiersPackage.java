/**
 */
package com.montages.acore.classifiers;

import com.montages.acore.abstractions.AbstractionsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.acore.classifiers.ClassifiersFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.acore'"
 * @generated
 */
public interface ClassifiersPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "classifiers";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/aCore/ACore/Classifiers";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.classifiers";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassifiersPackage eINSTANCE = com.montages.acore.classifiers.impl.ClassifiersPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AClassifierImpl <em>AClassifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AClassifierImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAClassifier()
	 * @generated
	 */
	int ACLASSIFIER = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ALABEL = AbstractionsPackage.ANAMED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AKIND_BASE = AbstractionsPackage.ANAMED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ARENDERED_KIND = AbstractionsPackage.ANAMED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ACONTAINING_COMPONENT = AbstractionsPackage.ANAMED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_PACKAGE_URI = AbstractionsPackage.ANAMED__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_CLASSIFIER_NAME = AbstractionsPackage.ANAMED__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_FEATURE_NAME = AbstractionsPackage.ANAMED__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_PACKAGE = AbstractionsPackage.ANAMED__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_CLASSIFIER = AbstractionsPackage.ANAMED__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_FEATURE = AbstractionsPackage.ANAMED__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AT_CORE_ASTRING_CLASS = AbstractionsPackage.ANAMED__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ANAME = AbstractionsPackage.ANAMED__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AUNDEFINED_NAME_CONSTANT = AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ABUSINESS_NAME = AbstractionsPackage.ANAMED__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ASPECIALIZED_CLASSIFIER = AbstractionsPackage.ANAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AACTIVE_DATA_TYPE = AbstractionsPackage.ANAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AACTIVE_ENUMERATION = AbstractionsPackage.ANAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AACTIVE_CLASS = AbstractionsPackage.ANAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__ACONTAINING_PACKAGE = AbstractionsPackage.ANAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AAs Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AAS_DATA_TYPE = AbstractionsPackage.ANAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AAs Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AAS_CLASS = AbstractionsPackage.ANAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER__AIS_STRING_CLASSIFIER = AbstractionsPackage.ANAMED_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>AClassifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER_FEATURE_COUNT = AbstractionsPackage.ANAMED_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AINDENT_LEVEL = AbstractionsPackage.ANAMED___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AINDENTATION_SPACES = AbstractionsPackage.ANAMED___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AINDENTATION_SPACES__INTEGER = AbstractionsPackage.ANAMED___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ASTRING_OR_MISSING__STRING = AbstractionsPackage.ANAMED___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ASTRING_IS_EMPTY__STRING = AbstractionsPackage.ANAMED___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.ANAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.ANAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___APACKAGE_FROM_URI__STRING = AbstractionsPackage.ANAMED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.ANAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.ANAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_ASTRING_CLASS = AbstractionsPackage.ANAMED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_AREAL_CLASS = AbstractionsPackage.ANAMED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_AINTEGER_CLASS = AbstractionsPackage.ANAMED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___ACORE_AOBJECT_CLASS = AbstractionsPackage.ANAMED___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER = AbstractionsPackage.ANAMED_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>AAll Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER___AALL_PROPERTY = AbstractionsPackage.ANAMED_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>AClassifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASSIFIER_OPERATION_COUNT = AbstractionsPackage.ANAMED_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.ADataTypeImpl <em>AData Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.ADataTypeImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getADataType()
	 * @generated
	 */
	int ADATA_TYPE = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ALABEL = ACLASSIFIER__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AKIND_BASE = ACLASSIFIER__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ARENDERED_KIND = ACLASSIFIER__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ACONTAINING_COMPONENT = ACLASSIFIER__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_PACKAGE_URI = ACLASSIFIER__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_CLASSIFIER_NAME = ACLASSIFIER__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_FEATURE_NAME = ACLASSIFIER__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_PACKAGE = ACLASSIFIER__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_CLASSIFIER = ACLASSIFIER__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_FEATURE = ACLASSIFIER__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AT_CORE_ASTRING_CLASS = ACLASSIFIER__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ANAME = ACLASSIFIER__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AUNDEFINED_NAME_CONSTANT = ACLASSIFIER__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ABUSINESS_NAME = ACLASSIFIER__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ASPECIALIZED_CLASSIFIER = ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AACTIVE_DATA_TYPE = ACLASSIFIER__AACTIVE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AACTIVE_ENUMERATION = ACLASSIFIER__AACTIVE_ENUMERATION;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AACTIVE_CLASS = ACLASSIFIER__AACTIVE_CLASS;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ACONTAINING_PACKAGE = ACLASSIFIER__ACONTAINING_PACKAGE;

	/**
	 * The feature id for the '<em><b>AAs Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AAS_DATA_TYPE = ACLASSIFIER__AAS_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AAs Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AAS_CLASS = ACLASSIFIER__AAS_CLASS;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__AIS_STRING_CLASSIFIER = ACLASSIFIER__AIS_STRING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>ASpecialized Data Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ASPECIALIZED_DATA_TYPE = ACLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AData Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE__ADATA_TYPE_PACKAGE = ACLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AData Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE_FEATURE_COUNT = ACLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AINDENT_LEVEL = ACLASSIFIER___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AINDENTATION_SPACES = ACLASSIFIER___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AINDENTATION_SPACES__INTEGER = ACLASSIFIER___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ASTRING_OR_MISSING__STRING = ACLASSIFIER___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ASTRING_IS_EMPTY__STRING = ACLASSIFIER___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ACLASSIFIER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ACLASSIFIER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___APACKAGE_FROM_URI__STRING = ACLASSIFIER___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ACLASSIFIER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ACLASSIFIER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_ASTRING_CLASS = ACLASSIFIER___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_AREAL_CLASS = ACLASSIFIER___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_AINTEGER_CLASS = ACLASSIFIER___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___ACORE_AOBJECT_CLASS = ACLASSIFIER___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AASSIGNABLE_TO__ACLASSIFIER = ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER;

	/**
	 * The operation id for the '<em>AAll Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE___AALL_PROPERTY = ACLASSIFIER___AALL_PROPERTY;

	/**
	 * The number of operations of the '<em>AData Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_TYPE_OPERATION_COUNT = ACLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AEnumerationImpl <em>AEnumeration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AEnumerationImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAEnumeration()
	 * @generated
	 */
	int AENUMERATION = 2;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ALABEL = ADATA_TYPE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AKIND_BASE = ADATA_TYPE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ARENDERED_KIND = ADATA_TYPE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ACONTAINING_COMPONENT = ADATA_TYPE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_PACKAGE_URI = ADATA_TYPE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_CLASSIFIER_NAME = ADATA_TYPE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_FEATURE_NAME = ADATA_TYPE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_PACKAGE = ADATA_TYPE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_CLASSIFIER = ADATA_TYPE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_FEATURE = ADATA_TYPE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AT_CORE_ASTRING_CLASS = ADATA_TYPE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ANAME = ADATA_TYPE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AUNDEFINED_NAME_CONSTANT = ADATA_TYPE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ABUSINESS_NAME = ADATA_TYPE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ASPECIALIZED_CLASSIFIER = ADATA_TYPE__ASPECIALIZED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AACTIVE_DATA_TYPE = ADATA_TYPE__AACTIVE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AACTIVE_ENUMERATION = ADATA_TYPE__AACTIVE_ENUMERATION;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AACTIVE_CLASS = ADATA_TYPE__AACTIVE_CLASS;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ACONTAINING_PACKAGE = ADATA_TYPE__ACONTAINING_PACKAGE;

	/**
	 * The feature id for the '<em><b>AAs Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AAS_DATA_TYPE = ADATA_TYPE__AAS_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AAs Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AAS_CLASS = ADATA_TYPE__AAS_CLASS;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__AIS_STRING_CLASSIFIER = ADATA_TYPE__AIS_STRING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>ASpecialized Data Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ASPECIALIZED_DATA_TYPE = ADATA_TYPE__ASPECIALIZED_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AData Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ADATA_TYPE_PACKAGE = ADATA_TYPE__ADATA_TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>ALiteral</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION__ALITERAL = ADATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AEnumeration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION_FEATURE_COUNT = ADATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AINDENT_LEVEL = ADATA_TYPE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AINDENTATION_SPACES = ADATA_TYPE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AINDENTATION_SPACES__INTEGER = ADATA_TYPE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ASTRING_OR_MISSING__STRING = ADATA_TYPE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ASTRING_IS_EMPTY__STRING = ADATA_TYPE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ADATA_TYPE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ADATA_TYPE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___APACKAGE_FROM_URI__STRING = ADATA_TYPE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ADATA_TYPE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ADATA_TYPE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_ASTRING_CLASS = ADATA_TYPE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_AREAL_CLASS = ADATA_TYPE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_AINTEGER_CLASS = ADATA_TYPE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___ACORE_AOBJECT_CLASS = ADATA_TYPE___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AASSIGNABLE_TO__ACLASSIFIER = ADATA_TYPE___AASSIGNABLE_TO__ACLASSIFIER;

	/**
	 * The operation id for the '<em>AAll Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION___AALL_PROPERTY = ADATA_TYPE___AALL_PROPERTY;

	/**
	 * The number of operations of the '<em>AEnumeration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AENUMERATION_OPERATION_COUNT = ADATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AClassTypeImpl <em>AClass Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AClassTypeImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAClassType()
	 * @generated
	 */
	int ACLASS_TYPE = 3;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ALABEL = ACLASSIFIER__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AKIND_BASE = ACLASSIFIER__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ARENDERED_KIND = ACLASSIFIER__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ACONTAINING_COMPONENT = ACLASSIFIER__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_PACKAGE_URI = ACLASSIFIER__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_CLASSIFIER_NAME = ACLASSIFIER__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_FEATURE_NAME = ACLASSIFIER__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_PACKAGE = ACLASSIFIER__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_CLASSIFIER = ACLASSIFIER__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_FEATURE = ACLASSIFIER__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AT_CORE_ASTRING_CLASS = ACLASSIFIER__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ANAME = ACLASSIFIER__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AUNDEFINED_NAME_CONSTANT = ACLASSIFIER__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ABUSINESS_NAME = ACLASSIFIER__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ASPECIALIZED_CLASSIFIER = ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AACTIVE_DATA_TYPE = ACLASSIFIER__AACTIVE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AACTIVE_ENUMERATION = ACLASSIFIER__AACTIVE_ENUMERATION;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AACTIVE_CLASS = ACLASSIFIER__AACTIVE_CLASS;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ACONTAINING_PACKAGE = ACLASSIFIER__ACONTAINING_PACKAGE;

	/**
	 * The feature id for the '<em><b>AAs Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AAS_DATA_TYPE = ACLASSIFIER__AAS_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>AAs Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AAS_CLASS = ACLASSIFIER__AAS_CLASS;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AIS_STRING_CLASSIFIER = ACLASSIFIER__AIS_STRING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AAnnotation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AANNOTATION = ACLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AAbstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AABSTRACT = ACLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ASpecialized Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ASPECIALIZED_CLASS = ACLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AFeature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AFEATURE = ACLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AOperation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AOPERATION = ACLASSIFIER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AAll Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AALL_FEATURE = ACLASSIFIER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AAll Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AALL_OPERATION = ACLASSIFIER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AObject Label</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AOBJECT_LABEL = ACLASSIFIER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>ASuper Types Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__ASUPER_TYPES_LABEL = ACLASSIFIER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>AAll Stored Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE__AALL_STORED_FEATURE = ACLASSIFIER_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>AClass Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE_FEATURE_COUNT = ACLASSIFIER_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AINDENT_LEVEL = ACLASSIFIER___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AINDENTATION_SPACES = ACLASSIFIER___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AINDENTATION_SPACES__INTEGER = ACLASSIFIER___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ASTRING_OR_MISSING__STRING = ACLASSIFIER___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ASTRING_IS_EMPTY__STRING = ACLASSIFIER___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ACLASSIFIER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ACLASSIFIER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___APACKAGE_FROM_URI__STRING = ACLASSIFIER___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ACLASSIFIER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ACLASSIFIER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ACORE_ASTRING_CLASS = ACLASSIFIER___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ACORE_AREAL_CLASS = ACLASSIFIER___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ACORE_AINTEGER_CLASS = ACLASSIFIER___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ACORE_AOBJECT_CLASS = ACLASSIFIER___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AASSIGNABLE_TO__ACLASSIFIER = ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER;

	/**
	 * The operation id for the '<em>ACreate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___ACREATE = ACLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>AFeature From Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AFEATURE_FROM_NAME__STRING = ACLASSIFIER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>AOperation From Name And Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER = ACLASSIFIER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>AAll Generalized Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AALL_GENERALIZED_CLASS = ACLASSIFIER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>AAll Specialized Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AALL_SPECIALIZED_CLASS = ACLASSIFIER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>AAll Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE___AALL_PROPERTY = ACLASSIFIER_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>AClass Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACLASS_TYPE_OPERATION_COUNT = ACLASSIFIER_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.APropertyImpl <em>AProperty</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.APropertyImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAProperty()
	 * @generated
	 */
	int APROPERTY = 9;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ALABEL = AbstractionsPackage.ATYPED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AKIND_BASE = AbstractionsPackage.ATYPED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ARENDERED_KIND = AbstractionsPackage.ATYPED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ACONTAINING_COMPONENT = AbstractionsPackage.ATYPED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_PACKAGE_URI = AbstractionsPackage.ATYPED__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_CLASSIFIER_NAME = AbstractionsPackage.ATYPED__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_FEATURE_NAME = AbstractionsPackage.ATYPED__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_PACKAGE = AbstractionsPackage.ATYPED__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_CLASSIFIER = AbstractionsPackage.ATYPED__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_FEATURE = AbstractionsPackage.ATYPED__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AT_CORE_ASTRING_CLASS = AbstractionsPackage.ATYPED__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ACLASSIFIER = AbstractionsPackage.ATYPED__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AMANDATORY = AbstractionsPackage.ATYPED__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ASINGULAR = AbstractionsPackage.ATYPED__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ATYPE_LABEL = AbstractionsPackage.ATYPED__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AUNDEFINED_TYPE_CONSTANT = AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ASIMPLE_TYPE = AbstractionsPackage.ATYPED__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ANAME = AbstractionsPackage.ATYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AUNDEFINED_NAME_CONSTANT = AbstractionsPackage.ATYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ABUSINESS_NAME = AbstractionsPackage.ATYPED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ACONTAINING_CLASS = AbstractionsPackage.ATYPED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AOperation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__AOPERATION = AbstractionsPackage.ATYPED_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>AProperty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY_FEATURE_COUNT = AbstractionsPackage.ATYPED_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___AINDENT_LEVEL = AbstractionsPackage.ATYPED___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___AINDENTATION_SPACES = AbstractionsPackage.ATYPED___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___AINDENTATION_SPACES__INTEGER = AbstractionsPackage.ATYPED___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ASTRING_OR_MISSING__STRING = AbstractionsPackage.ATYPED___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ASTRING_IS_EMPTY__STRING = AbstractionsPackage.ATYPED___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.ATYPED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.ATYPED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___APACKAGE_FROM_URI__STRING = AbstractionsPackage.ATYPED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ACORE_ASTRING_CLASS = AbstractionsPackage.ATYPED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ACORE_AREAL_CLASS = AbstractionsPackage.ATYPED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ACORE_AINTEGER_CLASS = AbstractionsPackage.ATYPED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ACORE_AOBJECT_CLASS = AbstractionsPackage.ATYPED___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = AbstractionsPackage.ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The number of operations of the '<em>AProperty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY_OPERATION_COUNT = AbstractionsPackage.ATYPED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AFeatureImpl <em>AFeature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AFeatureImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAFeature()
	 * @generated
	 */
	int AFEATURE = 4;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ALABEL = APROPERTY__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AKIND_BASE = APROPERTY__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ARENDERED_KIND = APROPERTY__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACONTAINING_COMPONENT = APROPERTY__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_PACKAGE_URI = APROPERTY__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_CLASSIFIER_NAME = APROPERTY__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_FEATURE_NAME = APROPERTY__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_PACKAGE = APROPERTY__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_CLASSIFIER = APROPERTY__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_FEATURE = APROPERTY__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AT_CORE_ASTRING_CLASS = APROPERTY__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACLASSIFIER = APROPERTY__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AMANDATORY = APROPERTY__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ASINGULAR = APROPERTY__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ATYPE_LABEL = APROPERTY__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AUNDEFINED_TYPE_CONSTANT = APROPERTY__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ASIMPLE_TYPE = APROPERTY__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ANAME = APROPERTY__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AUNDEFINED_NAME_CONSTANT = APROPERTY__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ABUSINESS_NAME = APROPERTY__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACONTAINING_CLASS = APROPERTY__ACONTAINING_CLASS;

	/**
	 * The feature id for the '<em><b>AOperation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AOPERATION = APROPERTY__AOPERATION;

	/**
	 * The feature id for the '<em><b>AAnnotation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AANNOTATION = APROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ASTORED = APROPERTY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__APERSISTED = APROPERTY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__ACHANGEABLE = APROPERTY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE__AACTIVE_FEATURE = APROPERTY_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>AFeature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_FEATURE_COUNT = APROPERTY_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___AINDENT_LEVEL = APROPERTY___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___AINDENTATION_SPACES = APROPERTY___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___AINDENTATION_SPACES__INTEGER = APROPERTY___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ASTRING_OR_MISSING__STRING = APROPERTY___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ASTRING_IS_EMPTY__STRING = APROPERTY___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = APROPERTY___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = APROPERTY___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___APACKAGE_FROM_URI__STRING = APROPERTY___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = APROPERTY___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = APROPERTY___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_ASTRING_CLASS = APROPERTY___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_AREAL_CLASS = APROPERTY___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_AINTEGER_CLASS = APROPERTY___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ACORE_AOBJECT_CLASS = APROPERTY___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = APROPERTY___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The number of operations of the '<em>AFeature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFEATURE_OPERATION_COUNT = APROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AAttributeImpl <em>AAttribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AAttributeImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAAttribute()
	 * @generated
	 */
	int AATTRIBUTE = 5;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ALABEL = AFEATURE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AKIND_BASE = AFEATURE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ARENDERED_KIND = AFEATURE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACONTAINING_COMPONENT = AFEATURE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_PACKAGE_URI = AFEATURE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_CLASSIFIER_NAME = AFEATURE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_FEATURE_NAME = AFEATURE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_PACKAGE = AFEATURE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_CLASSIFIER = AFEATURE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_FEATURE = AFEATURE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AT_CORE_ASTRING_CLASS = AFEATURE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACLASSIFIER = AFEATURE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AMANDATORY = AFEATURE__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ASINGULAR = AFEATURE__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ATYPE_LABEL = AFEATURE__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AUNDEFINED_TYPE_CONSTANT = AFEATURE__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ASIMPLE_TYPE = AFEATURE__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ANAME = AFEATURE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AUNDEFINED_NAME_CONSTANT = AFEATURE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ABUSINESS_NAME = AFEATURE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACONTAINING_CLASS = AFEATURE__ACONTAINING_CLASS;

	/**
	 * The feature id for the '<em><b>AOperation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AOPERATION = AFEATURE__AOPERATION;

	/**
	 * The feature id for the '<em><b>AAnnotation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AANNOTATION = AFEATURE__AANNOTATION;

	/**
	 * The feature id for the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ASTORED = AFEATURE__ASTORED;

	/**
	 * The feature id for the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__APERSISTED = AFEATURE__APERSISTED;

	/**
	 * The feature id for the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ACHANGEABLE = AFEATURE__ACHANGEABLE;

	/**
	 * The feature id for the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AACTIVE_FEATURE = AFEATURE__AACTIVE_FEATURE;

	/**
	 * The feature id for the '<em><b>AData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__ADATA_TYPE = AFEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AActive Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE__AACTIVE_ATTRIBUTE = AFEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AAttribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE_FEATURE_COUNT = AFEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___AINDENT_LEVEL = AFEATURE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___AINDENTATION_SPACES = AFEATURE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___AINDENTATION_SPACES__INTEGER = AFEATURE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ASTRING_OR_MISSING__STRING = AFEATURE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ASTRING_IS_EMPTY__STRING = AFEATURE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AFEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AFEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___APACKAGE_FROM_URI__STRING = AFEATURE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AFEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AFEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_ASTRING_CLASS = AFEATURE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_AREAL_CLASS = AFEATURE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_AINTEGER_CLASS = AFEATURE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ACORE_AOBJECT_CLASS = AFEATURE___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = AFEATURE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The number of operations of the '<em>AAttribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AATTRIBUTE_OPERATION_COUNT = AFEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AReferenceImpl <em>AReference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AReferenceImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAReference()
	 * @generated
	 */
	int AREFERENCE = 6;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ALABEL = AFEATURE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AKIND_BASE = AFEATURE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ARENDERED_KIND = AFEATURE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACONTAINING_COMPONENT = AFEATURE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_PACKAGE_URI = AFEATURE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_CLASSIFIER_NAME = AFEATURE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_FEATURE_NAME = AFEATURE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_PACKAGE = AFEATURE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_CLASSIFIER = AFEATURE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_FEATURE = AFEATURE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AT_CORE_ASTRING_CLASS = AFEATURE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACLASSIFIER = AFEATURE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AMANDATORY = AFEATURE__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ASINGULAR = AFEATURE__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ATYPE_LABEL = AFEATURE__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AUNDEFINED_TYPE_CONSTANT = AFEATURE__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ASIMPLE_TYPE = AFEATURE__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ANAME = AFEATURE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AUNDEFINED_NAME_CONSTANT = AFEATURE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ABUSINESS_NAME = AFEATURE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACONTAINING_CLASS = AFEATURE__ACONTAINING_CLASS;

	/**
	 * The feature id for the '<em><b>AOperation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AOPERATION = AFEATURE__AOPERATION;

	/**
	 * The feature id for the '<em><b>AAnnotation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AANNOTATION = AFEATURE__AANNOTATION;

	/**
	 * The feature id for the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ASTORED = AFEATURE__ASTORED;

	/**
	 * The feature id for the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__APERSISTED = AFEATURE__APERSISTED;

	/**
	 * The feature id for the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACHANGEABLE = AFEATURE__ACHANGEABLE;

	/**
	 * The feature id for the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AACTIVE_FEATURE = AFEATURE__AACTIVE_FEATURE;

	/**
	 * The feature id for the '<em><b>AClass Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACLASS_TYPE = AFEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AContainement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__ACONTAINEMENT = AFEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AActive Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE__AACTIVE_REFERENCE = AFEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AReference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE_FEATURE_COUNT = AFEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___AINDENT_LEVEL = AFEATURE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___AINDENTATION_SPACES = AFEATURE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___AINDENTATION_SPACES__INTEGER = AFEATURE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ASTRING_OR_MISSING__STRING = AFEATURE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ASTRING_IS_EMPTY__STRING = AFEATURE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AFEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AFEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___APACKAGE_FROM_URI__STRING = AFEATURE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AFEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AFEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_ASTRING_CLASS = AFEATURE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_AREAL_CLASS = AFEATURE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_AINTEGER_CLASS = AFEATURE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ACORE_AOBJECT_CLASS = AFEATURE___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = AFEATURE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The number of operations of the '<em>AReference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AREFERENCE_OPERATION_COUNT = AFEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AOperationImpl <em>AOperation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AOperationImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAOperation()
	 * @generated
	 */
	int AOPERATION = 7;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ALABEL = AbstractionsPackage.ATYPED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AKIND_BASE = AbstractionsPackage.ATYPED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ARENDERED_KIND = AbstractionsPackage.ATYPED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ACONTAINING_COMPONENT = AbstractionsPackage.ATYPED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_PACKAGE_URI = AbstractionsPackage.ATYPED__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_CLASSIFIER_NAME = AbstractionsPackage.ATYPED__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_FEATURE_NAME = AbstractionsPackage.ATYPED__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_PACKAGE = AbstractionsPackage.ATYPED__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_CLASSIFIER = AbstractionsPackage.ATYPED__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_FEATURE = AbstractionsPackage.ATYPED__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AT_CORE_ASTRING_CLASS = AbstractionsPackage.ATYPED__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ACLASSIFIER = AbstractionsPackage.ATYPED__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AMANDATORY = AbstractionsPackage.ATYPED__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ASINGULAR = AbstractionsPackage.ATYPED__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ATYPE_LABEL = AbstractionsPackage.ATYPED__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AUNDEFINED_TYPE_CONSTANT = AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ASIMPLE_TYPE = AbstractionsPackage.ATYPED__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ANAME = AbstractionsPackage.ATYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AUNDEFINED_NAME_CONSTANT = AbstractionsPackage.ATYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ABUSINESS_NAME = AbstractionsPackage.ATYPED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AAnnotation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__AANNOTATION = AbstractionsPackage.ATYPED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AParameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__APARAMETER = AbstractionsPackage.ATYPED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AContaining Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION__ACONTAINING_PROPERTY = AbstractionsPackage.ATYPED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>AOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_FEATURE_COUNT = AbstractionsPackage.ATYPED_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___AINDENT_LEVEL = AbstractionsPackage.ATYPED___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___AINDENTATION_SPACES = AbstractionsPackage.ATYPED___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___AINDENTATION_SPACES__INTEGER = AbstractionsPackage.ATYPED___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ASTRING_OR_MISSING__STRING = AbstractionsPackage.ATYPED___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ASTRING_IS_EMPTY__STRING = AbstractionsPackage.ATYPED___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.ATYPED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.ATYPED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___APACKAGE_FROM_URI__STRING = AbstractionsPackage.ATYPED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_ASTRING_CLASS = AbstractionsPackage.ATYPED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_AREAL_CLASS = AbstractionsPackage.ATYPED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_AINTEGER_CLASS = AbstractionsPackage.ATYPED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ACORE_AOBJECT_CLASS = AbstractionsPackage.ATYPED___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = AbstractionsPackage.ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The number of operations of the '<em>AOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOPERATION_OPERATION_COUNT = AbstractionsPackage.ATYPED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.impl.AParameterImpl <em>AParameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.AParameterImpl
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAParameter()
	 * @generated
	 */
	int APARAMETER = 8;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ALABEL = AbstractionsPackage.AVARIABLE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AKIND_BASE = AbstractionsPackage.AVARIABLE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ARENDERED_KIND = AbstractionsPackage.AVARIABLE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ACONTAINING_COMPONENT = AbstractionsPackage.AVARIABLE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_PACKAGE_URI = AbstractionsPackage.AVARIABLE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_CLASSIFIER_NAME = AbstractionsPackage.AVARIABLE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_FEATURE_NAME = AbstractionsPackage.AVARIABLE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_PACKAGE = AbstractionsPackage.AVARIABLE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_CLASSIFIER = AbstractionsPackage.AVARIABLE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_FEATURE = AbstractionsPackage.AVARIABLE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AT_CORE_ASTRING_CLASS = AbstractionsPackage.AVARIABLE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ANAME = AbstractionsPackage.AVARIABLE__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AUNDEFINED_NAME_CONSTANT = AbstractionsPackage.AVARIABLE__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ABUSINESS_NAME = AbstractionsPackage.AVARIABLE__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ACLASSIFIER = AbstractionsPackage.AVARIABLE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AMANDATORY = AbstractionsPackage.AVARIABLE__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ASINGULAR = AbstractionsPackage.AVARIABLE__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ATYPE_LABEL = AbstractionsPackage.AVARIABLE__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__AUNDEFINED_TYPE_CONSTANT = AbstractionsPackage.AVARIABLE__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ASIMPLE_TYPE = AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AContaining Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER__ACONTAINING_OPERATION = AbstractionsPackage.AVARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AParameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER_FEATURE_COUNT = AbstractionsPackage.AVARIABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___AINDENT_LEVEL = AbstractionsPackage.AVARIABLE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___AINDENTATION_SPACES = AbstractionsPackage.AVARIABLE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___AINDENTATION_SPACES__INTEGER = AbstractionsPackage.AVARIABLE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ASTRING_OR_MISSING__STRING = AbstractionsPackage.AVARIABLE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ASTRING_IS_EMPTY__STRING = AbstractionsPackage.AVARIABLE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.AVARIABLE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.AVARIABLE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___APACKAGE_FROM_URI__STRING = AbstractionsPackage.AVARIABLE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.AVARIABLE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.AVARIABLE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_ASTRING_CLASS = AbstractionsPackage.AVARIABLE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_AREAL_CLASS = AbstractionsPackage.AVARIABLE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_AINTEGER_CLASS = AbstractionsPackage.AVARIABLE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ACORE_AOBJECT_CLASS = AbstractionsPackage.AVARIABLE___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = AbstractionsPackage.AVARIABLE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The number of operations of the '<em>AParameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APARAMETER_OPERATION_COUNT = AbstractionsPackage.AVARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.classifiers.ASimpleType <em>ASimple Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getASimpleType()
	 * @generated
	 */
	int ASIMPLE_TYPE = 10;

	/**
	 * The meta object id for the '<em>AString</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAString()
	 * @generated
	 */
	int ASTRING = 11;

	/**
	 * The meta object id for the '<em>AInteger</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAInteger()
	 * @generated
	 */
	int AINTEGER = 12;

	/**
	 * The meta object id for the '<em>ABoolean</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getABoolean()
	 * @generated
	 */
	int ABOOLEAN = 13;

	/**
	 * The meta object id for the '<em>AReal</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAReal()
	 * @generated
	 */
	int AREAL = 14;

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AClassifier</em>'.
	 * @see com.montages.acore.classifiers.AClassifier
	 * @generated
	 */
	EClass getAClassifier();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassifier#getASpecializedClassifier <em>ASpecialized Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASpecialized Classifier</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getASpecializedClassifier()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_ASpecializedClassifier();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AClassifier#getAActiveDataType <em>AActive Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Data Type</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAActiveDataType()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AActiveDataType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AClassifier#getAActiveEnumeration <em>AActive Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Enumeration</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAActiveEnumeration()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AActiveEnumeration();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AClassifier#getAActiveClass <em>AActive Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Class</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAActiveClass()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AActiveClass();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AClassifier#getAContainingPackage <em>AContaining Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Package</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAContainingPackage()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_AContainingPackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AClassifier#getAAsDataType <em>AAs Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAs Data Type</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAAsDataType()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_AAsDataType();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AClassifier#getAAsClass <em>AAs Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAs Class</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAAsClass()
	 * @see #getAClassifier()
	 * @generated
	 */
	EReference getAClassifier_AAsClass();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AClassifier#getAIsStringClassifier <em>AIs String Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AIs String Classifier</em>'.
	 * @see com.montages.acore.classifiers.AClassifier#getAIsStringClassifier()
	 * @see #getAClassifier()
	 * @generated
	 */
	EAttribute getAClassifier_AIsStringClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassifier#aAssignableTo(com.montages.acore.classifiers.AClassifier) <em>AAssignable To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AAssignable To</em>' operation.
	 * @see com.montages.acore.classifiers.AClassifier#aAssignableTo(com.montages.acore.classifiers.AClassifier)
	 * @generated
	 */
	EOperation getAClassifier__AAssignableTo__AClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassifier#aAllProperty() <em>AAll Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AAll Property</em>' operation.
	 * @see com.montages.acore.classifiers.AClassifier#aAllProperty()
	 * @generated
	 */
	EOperation getAClassifier__AAllProperty();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.ADataType <em>AData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AData Type</em>'.
	 * @see com.montages.acore.classifiers.ADataType
	 * @generated
	 */
	EClass getADataType();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.ADataType#getASpecializedDataType <em>ASpecialized Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASpecialized Data Type</em>'.
	 * @see com.montages.acore.classifiers.ADataType#getASpecializedDataType()
	 * @see #getADataType()
	 * @generated
	 */
	EReference getADataType_ASpecializedDataType();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.ADataType#getADataTypePackage <em>AData Type Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AData Type Package</em>'.
	 * @see com.montages.acore.classifiers.ADataType#getADataTypePackage()
	 * @see #getADataType()
	 * @generated
	 */
	EReference getADataType_ADataTypePackage();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AEnumeration <em>AEnumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AEnumeration</em>'.
	 * @see com.montages.acore.classifiers.AEnumeration
	 * @generated
	 */
	EClass getAEnumeration();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AEnumeration#getALiteral <em>ALiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ALiteral</em>'.
	 * @see com.montages.acore.classifiers.AEnumeration#getALiteral()
	 * @see #getAEnumeration()
	 * @generated
	 */
	EReference getAEnumeration_ALiteral();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AClassType <em>AClass Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AClass Type</em>'.
	 * @see com.montages.acore.classifiers.AClassType
	 * @generated
	 */
	EClass getAClassType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AClassType#getAAbstract <em>AAbstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AAbstract</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAAbstract()
	 * @see #getAClassType()
	 * @generated
	 */
	EAttribute getAClassType_AAbstract();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassType#getASpecializedClass <em>ASpecialized Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASpecialized Class</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getASpecializedClass()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_ASpecializedClass();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassType#getAFeature <em>AFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AFeature</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAFeature()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_AFeature();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassType#getAOperation <em>AOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AOperation</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAOperation()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_AOperation();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassType#getAAllFeature <em>AAll Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Feature</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAAllFeature()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_AAllFeature();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassType#getAAllOperation <em>AAll Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Operation</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAAllOperation()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_AAllOperation();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AClassType#getAObjectLabel <em>AObject Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AObject Label</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAObjectLabel()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_AObjectLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AClassType#getASuperTypesLabel <em>ASuper Types Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ASuper Types Label</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getASuperTypesLabel()
	 * @see #getAClassType()
	 * @generated
	 */
	EAttribute getAClassType_ASuperTypesLabel();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AClassType#getAAllStoredFeature <em>AAll Stored Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Stored Feature</em>'.
	 * @see com.montages.acore.classifiers.AClassType#getAAllStoredFeature()
	 * @see #getAClassType()
	 * @generated
	 */
	EReference getAClassType_AAllStoredFeature();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassType#aCreate() <em>ACreate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACreate</em>' operation.
	 * @see com.montages.acore.classifiers.AClassType#aCreate()
	 * @generated
	 */
	EOperation getAClassType__ACreate();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassType#aFeatureFromName(java.lang.String) <em>AFeature From Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AFeature From Name</em>' operation.
	 * @see com.montages.acore.classifiers.AClassType#aFeatureFromName(java.lang.String)
	 * @generated
	 */
	EOperation getAClassType__AFeatureFromName__String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassType#aOperationFromNameAndTypes(java.lang.String, com.montages.acore.classifiers.AClassifier) <em>AOperation From Name And Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AOperation From Name And Types</em>' operation.
	 * @see com.montages.acore.classifiers.AClassType#aOperationFromNameAndTypes(java.lang.String, com.montages.acore.classifiers.AClassifier)
	 * @generated
	 */
	EOperation getAClassType__AOperationFromNameAndTypes__String_AClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassType#aAllGeneralizedClass() <em>AAll Generalized Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AAll Generalized Class</em>' operation.
	 * @see com.montages.acore.classifiers.AClassType#aAllGeneralizedClass()
	 * @generated
	 */
	EOperation getAClassType__AAllGeneralizedClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassType#aAllSpecializedClass() <em>AAll Specialized Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AAll Specialized Class</em>' operation.
	 * @see com.montages.acore.classifiers.AClassType#aAllSpecializedClass()
	 * @generated
	 */
	EOperation getAClassType__AAllSpecializedClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.classifiers.AClassType#aAllProperty() <em>AAll Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AAll Property</em>' operation.
	 * @see com.montages.acore.classifiers.AClassType#aAllProperty()
	 * @generated
	 */
	EOperation getAClassType__AAllProperty();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AFeature <em>AFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AFeature</em>'.
	 * @see com.montages.acore.classifiers.AFeature
	 * @generated
	 */
	EClass getAFeature();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AFeature#getAStored <em>AStored</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AStored</em>'.
	 * @see com.montages.acore.classifiers.AFeature#getAStored()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_AStored();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AFeature#getAPersisted <em>APersisted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>APersisted</em>'.
	 * @see com.montages.acore.classifiers.AFeature#getAPersisted()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_APersisted();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AFeature#getAChangeable <em>AChangeable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AChangeable</em>'.
	 * @see com.montages.acore.classifiers.AFeature#getAChangeable()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_AChangeable();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AFeature#getAActiveFeature <em>AActive Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Feature</em>'.
	 * @see com.montages.acore.classifiers.AFeature#getAActiveFeature()
	 * @see #getAFeature()
	 * @generated
	 */
	EAttribute getAFeature_AActiveFeature();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AAttribute <em>AAttribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AAttribute</em>'.
	 * @see com.montages.acore.classifiers.AAttribute
	 * @generated
	 */
	EClass getAAttribute();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AAttribute#getADataType <em>AData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AData Type</em>'.
	 * @see com.montages.acore.classifiers.AAttribute#getADataType()
	 * @see #getAAttribute()
	 * @generated
	 */
	EReference getAAttribute_ADataType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AAttribute#getAActiveAttribute <em>AActive Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Attribute</em>'.
	 * @see com.montages.acore.classifiers.AAttribute#getAActiveAttribute()
	 * @see #getAAttribute()
	 * @generated
	 */
	EAttribute getAAttribute_AActiveAttribute();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AReference <em>AReference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AReference</em>'.
	 * @see com.montages.acore.classifiers.AReference
	 * @generated
	 */
	EClass getAReference();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AReference#getAClassType <em>AClass Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AClass Type</em>'.
	 * @see com.montages.acore.classifiers.AReference#getAClassType()
	 * @see #getAReference()
	 * @generated
	 */
	EReference getAReference_AClassType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AReference#getAContainement <em>AContainement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AContainement</em>'.
	 * @see com.montages.acore.classifiers.AReference#getAContainement()
	 * @see #getAReference()
	 * @generated
	 */
	EAttribute getAReference_AContainement();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.classifiers.AReference#getAActiveReference <em>AActive Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AActive Reference</em>'.
	 * @see com.montages.acore.classifiers.AReference#getAActiveReference()
	 * @see #getAReference()
	 * @generated
	 */
	EAttribute getAReference_AActiveReference();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AOperation <em>AOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AOperation</em>'.
	 * @see com.montages.acore.classifiers.AOperation
	 * @generated
	 */
	EClass getAOperation();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AOperation#getAParameter <em>AParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AParameter</em>'.
	 * @see com.montages.acore.classifiers.AOperation#getAParameter()
	 * @see #getAOperation()
	 * @generated
	 */
	EReference getAOperation_AParameter();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AOperation#getAContainingProperty <em>AContaining Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Property</em>'.
	 * @see com.montages.acore.classifiers.AOperation#getAContainingProperty()
	 * @see #getAOperation()
	 * @generated
	 */
	EReference getAOperation_AContainingProperty();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AParameter <em>AParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AParameter</em>'.
	 * @see com.montages.acore.classifiers.AParameter
	 * @generated
	 */
	EClass getAParameter();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AParameter#getAContainingOperation <em>AContaining Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Operation</em>'.
	 * @see com.montages.acore.classifiers.AParameter#getAContainingOperation()
	 * @see #getAParameter()
	 * @generated
	 */
	EReference getAParameter_AContainingOperation();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.classifiers.AProperty <em>AProperty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AProperty</em>'.
	 * @see com.montages.acore.classifiers.AProperty
	 * @generated
	 */
	EClass getAProperty();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.classifiers.AProperty#getAContainingClass <em>AContaining Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Class</em>'.
	 * @see com.montages.acore.classifiers.AProperty#getAContainingClass()
	 * @see #getAProperty()
	 * @generated
	 */
	EReference getAProperty_AContainingClass();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.classifiers.AProperty#getAOperation <em>AOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AOperation</em>'.
	 * @see com.montages.acore.classifiers.AProperty#getAOperation()
	 * @see #getAProperty()
	 * @generated
	 */
	EReference getAProperty_AOperation();

	/**
	 * Returns the meta object for enum '{@link com.montages.acore.classifiers.ASimpleType <em>ASimple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ASimple Type</em>'.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @generated
	 */
	EEnum getASimpleType();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>AString</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>AString</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        annotation="http://www.montages.com/mCore/MCore mName='String'"
	 * @generated
	 */
	EDataType getAString();

	/**
	 * Returns the meta object for data type '<em>AInteger</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>AInteger</em>'.
	 * @model instanceClass="int"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Integer'"
	 * @generated
	 */
	EDataType getAInteger();

	/**
	 * Returns the meta object for data type '<em>ABoolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ABoolean</em>'.
	 * @model instanceClass="boolean"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Boolean'"
	 * @generated
	 */
	EDataType getABoolean();

	/**
	 * Returns the meta object for data type '<em>AReal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>AReal</em>'.
	 * @model instanceClass="double"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Real'"
	 * @generated
	 */
	EDataType getAReal();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassifiersFactory getClassifiersFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AClassifierImpl <em>AClassifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AClassifierImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAClassifier()
		 * @generated
		 */
		EClass ACLASSIFIER = eINSTANCE.getAClassifier();

		/**
		 * The meta object literal for the '<em><b>ASpecialized Classifier</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__ASPECIALIZED_CLASSIFIER = eINSTANCE.getAClassifier_ASpecializedClassifier();

		/**
		 * The meta object literal for the '<em><b>AActive Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AACTIVE_DATA_TYPE = eINSTANCE.getAClassifier_AActiveDataType();

		/**
		 * The meta object literal for the '<em><b>AActive Enumeration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AACTIVE_ENUMERATION = eINSTANCE.getAClassifier_AActiveEnumeration();

		/**
		 * The meta object literal for the '<em><b>AActive Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AACTIVE_CLASS = eINSTANCE.getAClassifier_AActiveClass();

		/**
		 * The meta object literal for the '<em><b>AContaining Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__ACONTAINING_PACKAGE = eINSTANCE.getAClassifier_AContainingPackage();

		/**
		 * The meta object literal for the '<em><b>AAs Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__AAS_DATA_TYPE = eINSTANCE.getAClassifier_AAsDataType();

		/**
		 * The meta object literal for the '<em><b>AAs Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASSIFIER__AAS_CLASS = eINSTANCE.getAClassifier_AAsClass();

		/**
		 * The meta object literal for the '<em><b>AIs String Classifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASSIFIER__AIS_STRING_CLASSIFIER = eINSTANCE.getAClassifier_AIsStringClassifier();

		/**
		 * The meta object literal for the '<em><b>AAssignable To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER = eINSTANCE.getAClassifier__AAssignableTo__AClassifier();

		/**
		 * The meta object literal for the '<em><b>AAll Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASSIFIER___AALL_PROPERTY = eINSTANCE.getAClassifier__AAllProperty();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.ADataTypeImpl <em>AData Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.ADataTypeImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getADataType()
		 * @generated
		 */
		EClass ADATA_TYPE = eINSTANCE.getADataType();

		/**
		 * The meta object literal for the '<em><b>ASpecialized Data Type</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADATA_TYPE__ASPECIALIZED_DATA_TYPE = eINSTANCE.getADataType_ASpecializedDataType();

		/**
		 * The meta object literal for the '<em><b>AData Type Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADATA_TYPE__ADATA_TYPE_PACKAGE = eINSTANCE.getADataType_ADataTypePackage();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AEnumerationImpl <em>AEnumeration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AEnumerationImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAEnumeration()
		 * @generated
		 */
		EClass AENUMERATION = eINSTANCE.getAEnumeration();

		/**
		 * The meta object literal for the '<em><b>ALiteral</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AENUMERATION__ALITERAL = eINSTANCE.getAEnumeration_ALiteral();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AClassTypeImpl <em>AClass Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AClassTypeImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAClassType()
		 * @generated
		 */
		EClass ACLASS_TYPE = eINSTANCE.getAClassType();

		/**
		 * The meta object literal for the '<em><b>AAbstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASS_TYPE__AABSTRACT = eINSTANCE.getAClassType_AAbstract();

		/**
		 * The meta object literal for the '<em><b>ASpecialized Class</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__ASPECIALIZED_CLASS = eINSTANCE.getAClassType_ASpecializedClass();

		/**
		 * The meta object literal for the '<em><b>AFeature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__AFEATURE = eINSTANCE.getAClassType_AFeature();

		/**
		 * The meta object literal for the '<em><b>AOperation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__AOPERATION = eINSTANCE.getAClassType_AOperation();

		/**
		 * The meta object literal for the '<em><b>AAll Feature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__AALL_FEATURE = eINSTANCE.getAClassType_AAllFeature();

		/**
		 * The meta object literal for the '<em><b>AAll Operation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__AALL_OPERATION = eINSTANCE.getAClassType_AAllOperation();

		/**
		 * The meta object literal for the '<em><b>AObject Label</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__AOBJECT_LABEL = eINSTANCE.getAClassType_AObjectLabel();

		/**
		 * The meta object literal for the '<em><b>ASuper Types Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACLASS_TYPE__ASUPER_TYPES_LABEL = eINSTANCE.getAClassType_ASuperTypesLabel();

		/**
		 * The meta object literal for the '<em><b>AAll Stored Feature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACLASS_TYPE__AALL_STORED_FEATURE = eINSTANCE.getAClassType_AAllStoredFeature();

		/**
		 * The meta object literal for the '<em><b>ACreate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS_TYPE___ACREATE = eINSTANCE.getAClassType__ACreate();

		/**
		 * The meta object literal for the '<em><b>AFeature From Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS_TYPE___AFEATURE_FROM_NAME__STRING = eINSTANCE.getAClassType__AFeatureFromName__String();

		/**
		 * The meta object literal for the '<em><b>AOperation From Name And Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS_TYPE___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER = eINSTANCE
				.getAClassType__AOperationFromNameAndTypes__String_AClassifier();

		/**
		 * The meta object literal for the '<em><b>AAll Generalized Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS_TYPE___AALL_GENERALIZED_CLASS = eINSTANCE.getAClassType__AAllGeneralizedClass();

		/**
		 * The meta object literal for the '<em><b>AAll Specialized Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS_TYPE___AALL_SPECIALIZED_CLASS = eINSTANCE.getAClassType__AAllSpecializedClass();

		/**
		 * The meta object literal for the '<em><b>AAll Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACLASS_TYPE___AALL_PROPERTY = eINSTANCE.getAClassType__AAllProperty();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AFeatureImpl <em>AFeature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AFeatureImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAFeature()
		 * @generated
		 */
		EClass AFEATURE = eINSTANCE.getAFeature();

		/**
		 * The meta object literal for the '<em><b>AStored</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__ASTORED = eINSTANCE.getAFeature_AStored();

		/**
		 * The meta object literal for the '<em><b>APersisted</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__APERSISTED = eINSTANCE.getAFeature_APersisted();

		/**
		 * The meta object literal for the '<em><b>AChangeable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__ACHANGEABLE = eINSTANCE.getAFeature_AChangeable();

		/**
		 * The meta object literal for the '<em><b>AActive Feature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFEATURE__AACTIVE_FEATURE = eINSTANCE.getAFeature_AActiveFeature();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AAttributeImpl <em>AAttribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AAttributeImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAAttribute()
		 * @generated
		 */
		EClass AATTRIBUTE = eINSTANCE.getAAttribute();

		/**
		 * The meta object literal for the '<em><b>AData Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AATTRIBUTE__ADATA_TYPE = eINSTANCE.getAAttribute_ADataType();

		/**
		 * The meta object literal for the '<em><b>AActive Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AATTRIBUTE__AACTIVE_ATTRIBUTE = eINSTANCE.getAAttribute_AActiveAttribute();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AReferenceImpl <em>AReference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AReferenceImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAReference()
		 * @generated
		 */
		EClass AREFERENCE = eINSTANCE.getAReference();

		/**
		 * The meta object literal for the '<em><b>AClass Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AREFERENCE__ACLASS_TYPE = eINSTANCE.getAReference_AClassType();

		/**
		 * The meta object literal for the '<em><b>AContainement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AREFERENCE__ACONTAINEMENT = eINSTANCE.getAReference_AContainement();

		/**
		 * The meta object literal for the '<em><b>AActive Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AREFERENCE__AACTIVE_REFERENCE = eINSTANCE.getAReference_AActiveReference();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AOperationImpl <em>AOperation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AOperationImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAOperation()
		 * @generated
		 */
		EClass AOPERATION = eINSTANCE.getAOperation();

		/**
		 * The meta object literal for the '<em><b>AParameter</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOPERATION__APARAMETER = eINSTANCE.getAOperation_AParameter();

		/**
		 * The meta object literal for the '<em><b>AContaining Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOPERATION__ACONTAINING_PROPERTY = eINSTANCE.getAOperation_AContainingProperty();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.AParameterImpl <em>AParameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.AParameterImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAParameter()
		 * @generated
		 */
		EClass APARAMETER = eINSTANCE.getAParameter();

		/**
		 * The meta object literal for the '<em><b>AContaining Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APARAMETER__ACONTAINING_OPERATION = eINSTANCE.getAParameter_AContainingOperation();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.impl.APropertyImpl <em>AProperty</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.APropertyImpl
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAProperty()
		 * @generated
		 */
		EClass APROPERTY = eINSTANCE.getAProperty();

		/**
		 * The meta object literal for the '<em><b>AContaining Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APROPERTY__ACONTAINING_CLASS = eINSTANCE.getAProperty_AContainingClass();

		/**
		 * The meta object literal for the '<em><b>AOperation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APROPERTY__AOPERATION = eINSTANCE.getAProperty_AOperation();

		/**
		 * The meta object literal for the '{@link com.montages.acore.classifiers.ASimpleType <em>ASimple Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.ASimpleType
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getASimpleType()
		 * @generated
		 */
		EEnum ASIMPLE_TYPE = eINSTANCE.getASimpleType();

		/**
		 * The meta object literal for the '<em>AString</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAString()
		 * @generated
		 */
		EDataType ASTRING = eINSTANCE.getAString();

		/**
		 * The meta object literal for the '<em>AInteger</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAInteger()
		 * @generated
		 */
		EDataType AINTEGER = eINSTANCE.getAInteger();

		/**
		 * The meta object literal for the '<em>ABoolean</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getABoolean()
		 * @generated
		 */
		EDataType ABOOLEAN = eINSTANCE.getABoolean();

		/**
		 * The meta object literal for the '<em>AReal</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.classifiers.impl.ClassifiersPackageImpl#getAReal()
		 * @generated
		 */
		EDataType AREAL = eINSTANCE.getAReal();

	}

} //ClassifiersPackage
