/**
 */
package com.montages.acore.classifiers.util;

import com.montages.acore.classifiers.ClassifiersPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifiersXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		ClassifiersPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the ClassifiersResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new ClassifiersResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new ClassifiersResourceFactoryImpl());
		}
		return registrations;
	}

} //ClassifiersXMLProcessor
