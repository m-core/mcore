/**
 */

package com.montages.acore.classifiers;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AReference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AReference#getAClassType <em>AClass Type</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AReference#getAContainement <em>AContainement</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AReference#getAActiveReference <em>AActive Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAReference()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Reference'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aClassType\n' aActiveFeatureDerive='aActiveReference\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aClassifierCreateColumn='false'"
 * @generated
 */

public interface AReference extends AFeature {
	/**
	 * Returns the value of the '<em><b>AClass Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClass Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClass Type</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAReference_AClassType()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassType = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassType getAClassType();

	/**
	 * Returns the value of the '<em><b>AContainement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContainement</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContainement</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAReference_AContainement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containement'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAContainement();

	/**
	 * Returns the value of the '<em><b>AActive Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Reference</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAReference_AActiveReference()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Active Reference'"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveReference();

} // AReference
