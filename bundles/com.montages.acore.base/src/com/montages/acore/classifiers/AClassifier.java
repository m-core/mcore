/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.APackage;

import com.montages.acore.abstractions.ANamed;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AClassifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getASpecializedClassifier <em>ASpecialized Classifier</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAActiveDataType <em>AActive Data Type</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAActiveEnumeration <em>AActive Enumeration</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAActiveClass <em>AActive Class</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAContainingPackage <em>AContaining Package</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAAsDataType <em>AAs Data Type</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAAsClass <em>AAs Class</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassifier#getAIsStringClassifier <em>AIs String Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Classifier'"
 * @generated
 */

public interface AClassifier extends ANamed {
	/**
	 * Returns the value of the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASpecialized Classifier</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASpecialized Classifier</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_ASpecializedClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Specialized Classifier'"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AClassifier> getASpecializedClassifier();

	/**
	 * Returns the value of the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Data Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Data Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AActiveDataType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Active Data Type'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveDataType();

	/**
	 * Returns the value of the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Enumeration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Enumeration</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AActiveEnumeration()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Active Enumeration'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveEnumeration();

	/**
	 * Returns the value of the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Class</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AActiveClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Active Class'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveClass();

	/**
	 * Returns the value of the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Package</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AContainingPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Package'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::APackage = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getAContainingPackage();

	/**
	 * Returns the value of the '<em><b>AAs Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAs Data Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAs Data Type</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AAsDataType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='As Data Type'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = if (aActiveDataType)= true \n then true \n else if (aActiveEnumeration)= true \n then true \nelse if ((aActiveDataType)= null or (aActiveEnumeration)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::ADataType)\n    then chain.oclAsType(acore::classifiers::ADataType)\n    else null\n  endif\n  endif\n  else null\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ADataType getAAsDataType();

	/**
	 * Returns the value of the '<em><b>AAs Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAs Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAs Class</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AAsClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='As Class'"
	 *        annotation="http://www.xocl.org/OCL derive='if (aActiveClass) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClassType)\n    then chain.oclAsType(acore::classifiers::AClassType)\n    else null\n  endif\n  endif\n  else null\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassType getAAsClass();

	/**
	 * Returns the value of the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AIs String Classifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AIs String Classifier</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassifier_AIsStringClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Is String Classifier'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: Boolean = if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif))= false \n then false \n else if (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= false \n then false \nelse if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif)= null or (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Package'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAIsStringClassifier();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Assignable To'"
	 *        annotation="http://www.xocl.org/OCL body='if (let e0: Boolean = self = classifier in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen true\n  else let e0: Boolean = aSpecializedClassifier.aAssignableTo(classifier)->reject(oclIsUndefined())->asOrderedSet()->includes(true)   in \n if e0.oclIsInvalid() then null else e0 endif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	Boolean aAssignableTo(AClassifier classifier);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='All Property'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	EList<AProperty> aAllProperty();

} // AClassifier
