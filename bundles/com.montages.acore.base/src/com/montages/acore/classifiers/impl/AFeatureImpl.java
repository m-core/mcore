/**
 */

package com.montages.acore.classifiers.impl;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.ClassifiersPackage;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AFeature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.impl.AFeatureImpl#getAAnnotation <em>AAnnotation</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AFeatureImpl#getAStored <em>AStored</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AFeatureImpl#getAPersisted <em>APersisted</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AFeatureImpl#getAChangeable <em>AChangeable</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AFeatureImpl#getAActiveFeature <em>AActive Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AFeatureImpl extends APropertyImpl implements AFeature {
	/**
	 * The default value of the '{@link #getAStored() <em>AStored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAStored()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ASTORED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAPersisted() <em>APersisted</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPersisted()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean APERSISTED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAChangeable() <em>AChangeable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChangeable()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ACHANGEABLE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveFeature() <em>AActive Feature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveFeature()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_FEATURE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotation <em>AAnnotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAStored <em>AStored</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAStored
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aStoredDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAPersisted <em>APersisted</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPersisted
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aPersistedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChangeable <em>AChangeable</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChangeable
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aChangeableDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveFeature <em>AActive Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveFeatureDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.AFEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AAnnotation> getAAnnotation() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AANNOTATABLE__AANNOTATION;

		if (aAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AAnnotation> result = (EList<AAnnotation>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAStored() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__ASTORED;

		if (aStoredDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aStoredDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aStoredDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAPersisted() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__APERSISTED;

		if (aPersistedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aPersistedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aPersistedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAChangeable() {
		/**
		 * @OCL if (aStored) 
		=true 
		then true
		else false
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__ACHANGEABLE;

		if (aChangeableDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aChangeableDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChangeableDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveFeature() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.AFEATURE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.AFEATURE__AACTIVE_FEATURE;

		if (aActiveFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.AFEATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.AFEATURE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ClassifiersPackage.AFEATURE__AANNOTATION:
			return getAAnnotation();
		case ClassifiersPackage.AFEATURE__ASTORED:
			return getAStored();
		case ClassifiersPackage.AFEATURE__APERSISTED:
			return getAPersisted();
		case ClassifiersPackage.AFEATURE__ACHANGEABLE:
			return getAChangeable();
		case ClassifiersPackage.AFEATURE__AACTIVE_FEATURE:
			return getAActiveFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ClassifiersPackage.AFEATURE__AANNOTATION:
			return !getAAnnotation().isEmpty();
		case ClassifiersPackage.AFEATURE__ASTORED:
			return ASTORED_EDEFAULT == null ? getAStored() != null : !ASTORED_EDEFAULT.equals(getAStored());
		case ClassifiersPackage.AFEATURE__APERSISTED:
			return APERSISTED_EDEFAULT == null ? getAPersisted() != null : !APERSISTED_EDEFAULT.equals(getAPersisted());
		case ClassifiersPackage.AFEATURE__ACHANGEABLE:
			return ACHANGEABLE_EDEFAULT == null ? getAChangeable() != null
					: !ACHANGEABLE_EDEFAULT.equals(getAChangeable());
		case ClassifiersPackage.AFEATURE__AACTIVE_FEATURE:
			return AACTIVE_FEATURE_EDEFAULT == null ? getAActiveFeature() != null
					: !AACTIVE_FEATURE_EDEFAULT.equals(getAActiveFeature());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AAnnotatable.class) {
			switch (derivedFeatureID) {
			case ClassifiersPackage.AFEATURE__AANNOTATION:
				return AbstractionsPackage.AANNOTATABLE__AANNOTATION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AAnnotatable.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.AANNOTATABLE__AANNOTATION:
				return ClassifiersPackage.AFEATURE__AANNOTATION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //AFeatureImpl
