/**
 */

package com.montages.acore.classifiers.impl;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.annotations.AObjectLabel;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.AOperation;
import com.montages.acore.classifiers.AProperty;
import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.values.AObject;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AClass Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAAnnotation <em>AAnnotation</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAAbstract <em>AAbstract</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getASpecializedClass <em>ASpecialized Class</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAFeature <em>AFeature</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAOperation <em>AOperation</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAAllFeature <em>AAll Feature</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAAllOperation <em>AAll Operation</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAObjectLabel <em>AObject Label</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getASuperTypesLabel <em>ASuper Types Label</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.impl.AClassTypeImpl#getAAllStoredFeature <em>AAll Stored Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AClassTypeImpl extends AClassifierImpl implements AClassType {
	/**
	 * The default value of the '{@link #getAAbstract() <em>AAbstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AABSTRACT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAObjectLabel() <em>AObject Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAObjectLabel()
	 * @generated
	 * @ordered
	 */
	protected AObjectLabel aObjectLabel;

	/**
	 * This is true if the AObject Label reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aObjectLabelESet;

	/**
	 * The default value of the '{@link #getASuperTypesLabel() <em>ASuper Types Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASuperTypesLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ASUPER_TYPES_LABEL_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCreate <em>ACreate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCreate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCreateBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aFeatureFromName <em>AFeature From Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aFeatureFromName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aFeatureFromNameecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aOperationFromNameAndTypes <em>AOperation From Name And Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aOperationFromNameAndTypes
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aOperationFromNameAndTypesecoreEStringclassifiersAClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aAllGeneralizedClass <em>AAll Generalized Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aAllGeneralizedClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aAllGeneralizedClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aAllSpecializedClass <em>AAll Specialized Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aAllSpecializedClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aAllSpecializedClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aAllProperty <em>AAll Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aAllProperty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aAllPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotation <em>AAnnotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAbstract <em>AAbstract</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAbstract
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAbstractDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASpecializedClass <em>ASpecialized Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASpecializedClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSpecializedClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFeature <em>AFeature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAOperation <em>AOperation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAOperation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aOperationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllFeature <em>AAll Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllOperation <em>AAll Operation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllOperation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllOperationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASuperTypesLabel <em>ASuper Types Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASuperTypesLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSuperTypesLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllStoredFeature <em>AAll Stored Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllStoredFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllStoredFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveClass <em>AActive Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveClass
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aActiveClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASpecializedClassifier <em>ASpecialized Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASpecializedClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSpecializedClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AClassTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ACLASS_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AAnnotation> getAAnnotation() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AANNOTATABLE__AANNOTATION;

		if (aAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AAnnotation> result = (EList<AAnnotation>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAAbstract() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__AABSTRACT;

		if (aAbstractDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAbstractDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAbstractDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassType> getASpecializedClass() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__ASPECIALIZED_CLASS;

		if (aSpecializedClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSpecializedClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSpecializedClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AClassType> result = (EList<AClassType>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AFeature> getAFeature() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__AFEATURE;

		if (aFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AFeature> result = (EList<AFeature>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AOperation> getAOperation() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__AOPERATION;

		if (aOperationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aOperationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aOperationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AOperation> result = (EList<AOperation>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AFeature> getAAllFeature() {
		/**
		 * @OCL let e1: OrderedSet(acore::classifiers::AFeature)  = aFeature->asOrderedSet()->union(aSpecializedClass.aAllFeature->asOrderedSet()) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__AALL_FEATURE;

		if (aAllFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AFeature> result = (EList<AFeature>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AOperation> getAAllOperation() {
		/**
		 * @OCL let e1: OrderedSet(acore::classifiers::AOperation)  = aOperation->asOrderedSet()->union(aSpecializedClass.aAllOperation->asOrderedSet()) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__AALL_OPERATION;

		if (aAllOperationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllOperationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllOperationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AOperation> result = (EList<AOperation>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObjectLabel getAObjectLabel() {
		if (aObjectLabel != null && aObjectLabel.eIsProxy()) {
			InternalEObject oldAObjectLabel = (InternalEObject) aObjectLabel;
			aObjectLabel = (AObjectLabel) eResolveProxy(oldAObjectLabel);
			if (aObjectLabel != oldAObjectLabel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL, oldAObjectLabel, aObjectLabel));
			}
		}
		return aObjectLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObjectLabel basicGetAObjectLabel() {
		return aObjectLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAObjectLabel(AObjectLabel newAObjectLabel) {
		AObjectLabel oldAObjectLabel = aObjectLabel;
		aObjectLabel = newAObjectLabel;
		boolean oldAObjectLabelESet = aObjectLabelESet;
		aObjectLabelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL,
					oldAObjectLabel, aObjectLabel, !oldAObjectLabelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAObjectLabel() {
		AObjectLabel oldAObjectLabel = aObjectLabel;
		boolean oldAObjectLabelESet = aObjectLabelESet;
		aObjectLabel = null;
		aObjectLabelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL,
					oldAObjectLabel, null, oldAObjectLabelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAObjectLabel() {
		return aObjectLabelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getASuperTypesLabel() {
		/**
		 * @OCL let length: Integer = let chain: OrderedSet(acore::classifiers::AClassType)  = aSpecializedClass->asOrderedSet() in
		if chain->size().oclIsUndefined() 
		then null 
		else chain->size()
		endif in
		let superTypeNames: OrderedSet(String)  = aSpecializedClass.aName->reject(oclIsUndefined())->asOrderedSet() in
		if (let e0: Boolean = length = 0 in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then '' else if (let e0: Boolean = length = 1 in 
		if e0.oclIsInvalid() then null else e0 endif)=true then (let e0: String = '->'.concat(let chain01: OrderedSet(String)  = superTypeNames in
		if chain01->first().oclIsUndefined() 
		then null 
		else chain01->first()
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)
		else (let e0: String = '->('.concat(aListOfStringToStringWithSeparator(superTypeNames)).concat(')') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__ASUPER_TYPES_LABEL;

		if (aSuperTypesLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSuperTypesLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSuperTypesLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AFeature> getAAllStoredFeature() {
		/**
		 * @OCL aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | it.aStored)->asOrderedSet()->excluding(null)->asOrderedSet() 
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASS_TYPE;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASS_TYPE__AALL_STORED_FEATURE;

		if (aAllStoredFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllStoredFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllStoredFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AFeature> result = (EList<AFeature>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject aCreate() {

		/**
		 * @OCL let nl: acore::values::AObject = null in nl
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASS_TYPE.getEOperations().get(0);
		if (aCreateBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCreateBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCreateBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AObject) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature aFeatureFromName(String featureName) {

		/**
		 * @OCL let fs: OrderedSet(acore::classifiers::AFeature)  = aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | let e0: Boolean = it.aName = featureName in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		if (let chain: OrderedSet(acore::classifiers::AFeature)  = fs in
		if chain->isEmpty().oclIsUndefined() 
		then null 
		else chain->isEmpty()
		endif) 
		=true 
		then null
		else let chain: OrderedSet(acore::classifiers::AFeature)  = fs in
		if chain->first().oclIsUndefined() 
		then null 
		else chain->first()
		endif
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASS_TYPE.getEOperations().get(1);
		if (aFeatureFromNameecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aFeatureFromNameecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureFromNameecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("featureName", featureName);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AFeature) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AOperation aOperationFromNameAndTypes(String operationName, AClassifier parameterType) {

		/**
		 * @OCL let nl: acore::classifiers::AOperation = null in nl
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASS_TYPE.getEOperations().get(2);
		if (aOperationFromNameAndTypesecoreEStringclassifiersAClassifierBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aOperationFromNameAndTypesecoreEStringclassifiersAClassifierBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aOperationFromNameAndTypesecoreEStringclassifiersAClassifierBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("operationName", operationName);

			evalEnv.add("parameterType", parameterType);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AOperation) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassType> aAllGeneralizedClass() {

		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASS_TYPE.getEOperations().get(3);
		if (aAllGeneralizedClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aAllGeneralizedClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aAllGeneralizedClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<AClassType>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassType> aAllSpecializedClass() {

		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASS_TYPE.getEOperations().get(4);
		if (aAllSpecializedClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aAllSpecializedClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aAllSpecializedClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<AClassType>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AProperty> aAllProperty() {

		/**
		 * @OCL let var0: OrderedSet(acore::classifiers::AFeature)  = let e1: OrderedSet(acore::classifiers::AFeature)  = self.aAllFeature->asOrderedSet()->union(let chain: OrderedSet(acore::classifiers::AProperty)  = aAllOperation.aContainingProperty->reject(oclIsUndefined())->asOrderedSet() in
		chain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() 
		else r endif)) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif in
		var0
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASS_TYPE.getEOperations().get(5);
		if (aAllPropertyBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aAllPropertyBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASS_TYPE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aAllPropertyBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASS_TYPE,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<AProperty>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ClassifiersPackage.ACLASS_TYPE__AANNOTATION:
			return getAAnnotation();
		case ClassifiersPackage.ACLASS_TYPE__AABSTRACT:
			return getAAbstract();
		case ClassifiersPackage.ACLASS_TYPE__ASPECIALIZED_CLASS:
			return getASpecializedClass();
		case ClassifiersPackage.ACLASS_TYPE__AFEATURE:
			return getAFeature();
		case ClassifiersPackage.ACLASS_TYPE__AOPERATION:
			return getAOperation();
		case ClassifiersPackage.ACLASS_TYPE__AALL_FEATURE:
			return getAAllFeature();
		case ClassifiersPackage.ACLASS_TYPE__AALL_OPERATION:
			return getAAllOperation();
		case ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL:
			if (resolve)
				return getAObjectLabel();
			return basicGetAObjectLabel();
		case ClassifiersPackage.ACLASS_TYPE__ASUPER_TYPES_LABEL:
			return getASuperTypesLabel();
		case ClassifiersPackage.ACLASS_TYPE__AALL_STORED_FEATURE:
			return getAAllStoredFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL:
			setAObjectLabel((AObjectLabel) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL:
			unsetAObjectLabel();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ClassifiersPackage.ACLASS_TYPE__AANNOTATION:
			return !getAAnnotation().isEmpty();
		case ClassifiersPackage.ACLASS_TYPE__AABSTRACT:
			return AABSTRACT_EDEFAULT == null ? getAAbstract() != null : !AABSTRACT_EDEFAULT.equals(getAAbstract());
		case ClassifiersPackage.ACLASS_TYPE__ASPECIALIZED_CLASS:
			return !getASpecializedClass().isEmpty();
		case ClassifiersPackage.ACLASS_TYPE__AFEATURE:
			return !getAFeature().isEmpty();
		case ClassifiersPackage.ACLASS_TYPE__AOPERATION:
			return !getAOperation().isEmpty();
		case ClassifiersPackage.ACLASS_TYPE__AALL_FEATURE:
			return !getAAllFeature().isEmpty();
		case ClassifiersPackage.ACLASS_TYPE__AALL_OPERATION:
			return !getAAllOperation().isEmpty();
		case ClassifiersPackage.ACLASS_TYPE__AOBJECT_LABEL:
			return isSetAObjectLabel();
		case ClassifiersPackage.ACLASS_TYPE__ASUPER_TYPES_LABEL:
			return ASUPER_TYPES_LABEL_EDEFAULT == null ? getASuperTypesLabel() != null
					: !ASUPER_TYPES_LABEL_EDEFAULT.equals(getASuperTypesLabel());
		case ClassifiersPackage.ACLASS_TYPE__AALL_STORED_FEATURE:
			return !getAAllStoredFeature().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AAnnotatable.class) {
			switch (derivedFeatureID) {
			case ClassifiersPackage.ACLASS_TYPE__AANNOTATION:
				return AbstractionsPackage.AANNOTATABLE__AANNOTATION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AAnnotatable.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.AANNOTATABLE__AANNOTATION:
				return ClassifiersPackage.ACLASS_TYPE__AANNOTATION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ClassifiersPackage.ACLASS_TYPE___ACREATE:
			return aCreate();
		case ClassifiersPackage.ACLASS_TYPE___AFEATURE_FROM_NAME__STRING:
			return aFeatureFromName((String) arguments.get(0));
		case ClassifiersPackage.ACLASS_TYPE___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER:
			return aOperationFromNameAndTypes((String) arguments.get(0), (AClassifier) arguments.get(1));
		case ClassifiersPackage.ACLASS_TYPE___AALL_GENERALIZED_CLASS:
			return aAllGeneralizedClass();
		case ClassifiersPackage.ACLASS_TYPE___AALL_SPECIALIZED_CLASS:
			return aAllSpecializedClass();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aActiveClass true
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getAActiveClass() {
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EStructuralFeature eOverrideFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_CLASS;

		if (aActiveClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aActiveClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aSpecializedClassifier aSpecializedClass->asOrderedSet()
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public EList<AClassifier> getASpecializedClassifier() {
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EStructuralFeature eOverrideFeature = ClassifiersPackage.Literals.ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

		if (aSpecializedClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSpecializedClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSpecializedClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<AClassifier>) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel let e1: String = aName.concat(aSuperTypesLabel) in 
	if e1.oclIsInvalid() then null else e1 endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (ClassifiersPackage.Literals.ACLASS_TYPE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AClassTypeImpl
