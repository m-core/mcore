/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.abstractions.AAnnotatable;

import com.montages.acore.annotations.AObjectLabel;

import com.montages.acore.values.AObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AClass Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAAbstract <em>AAbstract</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getASpecializedClass <em>ASpecialized Class</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAFeature <em>AFeature</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAOperation <em>AOperation</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAAllFeature <em>AAll Feature</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAAllOperation <em>AAll Operation</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAObjectLabel <em>AObject Label</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getASuperTypesLabel <em>ASuper Types Label</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AClassType#getAAllStoredFeature <em>AAll Stored Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Class Type'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aActiveClassDerive='true\n' aSpecializedClassifierDerive='aSpecializedClass->asOrderedSet()\n' aLabelDerive='let e1: String = aName.concat(aSuperTypesLabel) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 * @generated
 */

public interface AClassType extends AClassifier, AAnnotatable {
	/**
	 * Returns the value of the '<em><b>AAbstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAbstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAbstract</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AAbstract()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Abstract'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAAbstract();

	/**
	 * Returns the value of the '<em><b>ASpecialized Class</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AClassType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASpecialized Class</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASpecialized Class</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_ASpecializedClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Specialized Class'"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AClassType> getASpecializedClass();

	/**
	 * Returns the value of the '<em><b>AFeature</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFeature> getAFeature();

	/**
	 * Returns the value of the '<em><b>AOperation</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AOperation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AOperation</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AOperation> getAOperation();

	/**
	 * Returns the value of the '<em><b>AAll Feature</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Feature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Feature</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AAllFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='All Feature'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(acore::classifiers::AFeature)  = aFeature->asOrderedSet()->union(aSpecializedClass.aAllFeature->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFeature> getAAllFeature();

	/**
	 * Returns the value of the '<em><b>AAll Operation</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Operation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Operation</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AAllOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='All Operation'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(acore::classifiers::AOperation)  = aOperation->asOrderedSet()->union(aSpecializedClass.aAllOperation->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AOperation> getAAllOperation();

	/**
	 * Returns the value of the '<em><b>AObject Label</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AObject Label</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AObject Label</em>' reference.
	 * @see #isSetAObjectLabel()
	 * @see #unsetAObjectLabel()
	 * @see #setAObjectLabel(AObjectLabel)
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AObjectLabel()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Semantics' createColumn='false'"
	 * @generated
	 */
	AObjectLabel getAObjectLabel();

	/** 
	 * Sets the value of the '{@link com.montages.acore.classifiers.AClassType#getAObjectLabel <em>AObject Label</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AObject Label</em>' reference.
	 * @see #isSetAObjectLabel()
	 * @see #unsetAObjectLabel()
	 * @see #getAObjectLabel()
	 * @generated
	 */

	void setAObjectLabel(AObjectLabel value);

	/**
	 * Unsets the value of the '{@link com.montages.acore.classifiers.AClassType#getAObjectLabel <em>AObject Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAObjectLabel()
	 * @see #getAObjectLabel()
	 * @see #setAObjectLabel(AObjectLabel)
	 * @generated
	 */
	void unsetAObjectLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.acore.classifiers.AClassType#getAObjectLabel <em>AObject Label</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AObject Label</em>' reference is set.
	 * @see #unsetAObjectLabel()
	 * @see #getAObjectLabel()
	 * @see #setAObjectLabel(AObjectLabel)
	 * @generated
	 */
	boolean isSetAObjectLabel();

	/**
	 * Returns the value of the '<em><b>ASuper Types Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASuper Types Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASuper Types Label</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_ASuperTypesLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Super Types Label'"
	 *        annotation="http://www.xocl.org/OCL derive='let length: Integer = let chain: OrderedSet(acore::classifiers::AClassType)  = aSpecializedClass->asOrderedSet() in\nif chain->size().oclIsUndefined() \n then null \n else chain->size()\n  endif in\nlet superTypeNames: OrderedSet(String)  = aSpecializedClass.aName->reject(oclIsUndefined())->asOrderedSet() in\nif (let e0: Boolean = length = 0 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\' else if (let e0: Boolean = length = 1 in \n if e0.oclIsInvalid() then null else e0 endif)=true then (let e0: String = \'->\'.concat(let chain01: OrderedSet(String)  = superTypeNames in\nif chain01->first().oclIsUndefined() \n then null \n else chain01->first()\n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: String = \'->(\'.concat(aListOfStringToStringWithSeparator(superTypeNames)).concat(\')\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getASuperTypesLabel();

	/**
	 * Returns the value of the '<em><b>AAll Stored Feature</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Stored Feature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Stored Feature</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAClassType_AAllStoredFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='All Stored Feature'"
	 *        annotation="http://www.xocl.org/OCL derive='aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | it.aStored)->asOrderedSet()->excluding(null)->asOrderedSet() \n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core Helpers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFeature> getAAllStoredFeature();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Create'"
	 *        annotation="http://www.xocl.org/OCL body='let nl: acore::values::AObject = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	AObject aCreate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Feature From Name'"
	 *        annotation="http://www.xocl.org/OCL body='let fs: OrderedSet(acore::classifiers::AFeature)  = aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | let e0: Boolean = it.aName = featureName in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen null\n  else let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	AFeature aFeatureFromName(String featureName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Operation From Name And Types'"
	 *        annotation="http://www.xocl.org/OCL body='let nl: acore::classifiers::AOperation = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	AOperation aOperationFromNameAndTypes(String operationName, AClassifier parameterType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='All Generalized Class'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	EList<AClassType> aAllGeneralizedClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='All Specialized Class'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	EList<AClassType> aAllSpecializedClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='All Property'"
	 *        annotation="http://www.xocl.org/OCL body='let var0: OrderedSet(acore::classifiers::AFeature)  = let e1: OrderedSet(acore::classifiers::AFeature)  = self.aAllFeature->asOrderedSet()->union(let chain: OrderedSet(acore::classifiers::AProperty)  = aAllOperation.aContainingProperty->reject(oclIsUndefined())->asOrderedSet() in\nchain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() \n else r endif)) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif in\nvar0\n'"
	 * @generated
	 */
	EList<AProperty> aAllProperty();

} // AClassType
