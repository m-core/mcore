/**
 */

package com.montages.acore.impl;

import com.montages.acore.AComponent;
import com.montages.acore.APackage;
import com.montages.acore.AResource;
import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AComponent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getAComponentId <em>AComponent Id</em>}</li>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getABaseUri <em>ABase Uri</em>}</li>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getADefaultUri <em>ADefault Uri</em>}</li>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getAUndefinedIdConstant <em>AUndefined Id Constant</em>}</li>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getAUsed <em>AUsed</em>}</li>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getAMainPackage <em>AMain Package</em>}</li>
 *   <li>{@link com.montages.acore.impl.AComponentImpl#getAMainResource <em>AMain Resource</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AComponentImpl extends AAbstractFolderImpl implements AComponent {
	/**
	 * The default value of the '{@link #getAComponentId() <em>AComponent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAComponentId()
	 * @generated
	 * @ordered
	 */
	protected static final String ACOMPONENT_ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABaseUri() <em>ABase Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABaseUri()
	 * @generated
	 * @ordered
	 */
	protected static final String ABASE_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getADefaultUri() <em>ADefault Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADefaultUri()
	 * @generated
	 * @ordered
	 */
	protected static final String ADEFAULT_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedIdConstant() <em>AUndefined Id Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedIdConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_ID_CONSTANT_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #aPackageFromUri <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aPackageFromUri
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aPackageFromUriecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAComponentId <em>AComponent Id</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAComponentId
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aComponentIdDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABaseUri <em>ABase Uri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABaseUri
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBaseUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getADefaultUri <em>ADefault Uri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADefaultUri
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aDefaultUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedIdConstant <em>AUndefined Id Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedIdConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedIdConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUsed <em>AUsed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUsed
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUsedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMainPackage <em>AMain Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMainPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aMainPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMainResource <em>AMain Resource</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMainResource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aMainResourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUri <em>AUri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUri
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingComponent <em>AContaining Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingComponent
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aContainingComponentDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcorePackage.Literals.ACOMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAComponentId() {
		/**
		 * @OCL aUndefinedIdConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__ACOMPONENT_ID;

		if (aComponentIdDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aComponentIdDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aComponentIdDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABaseUri() {
		/**
		 * @OCL aDefaultUri
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__ABASE_URI;

		if (aBaseUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBaseUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBaseUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getADefaultUri() {
		/**
		 * @OCL 'http://www.langlets.org'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__ADEFAULT_URI;

		if (aDefaultUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aDefaultUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aDefaultUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedIdConstant() {
		/**
		 * @OCL '<A Id Is Undefined> '
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__AUNDEFINED_ID_CONSTANT;

		if (aUndefinedIdConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedIdConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedIdConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AComponent> getAUsed() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__AUSED;

		if (aUsedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUsedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUsedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AComponent> result = (EList<AComponent>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getAMainPackage() {
		APackage aMainPackage = basicGetAMainPackage();
		return aMainPackage != null && aMainPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) aMainPackage) : aMainPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetAMainPackage() {
		/**
		 * @OCL let nl: acore::APackage = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__AMAIN_PACKAGE;

		if (aMainPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aMainPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMainPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AResource getAMainResource() {
		AResource aMainResource = basicGetAMainResource();
		return aMainResource != null && aMainResource.eIsProxy()
				? (AResource) eResolveProxy((InternalEObject) aMainResource) : aMainResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AResource basicGetAMainResource() {
		/**
		 * @OCL let nl: acore::AResource = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ACOMPONENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ACOMPONENT__AMAIN_RESOURCE;

		if (aMainResourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aMainResourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMainResourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AResource result = (AResource) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage aPackageFromUri(String packageUri) {

		/**
		 * @OCL let localPackage: OrderedSet(acore::APackage)  = aAllPackages->asOrderedSet()->select(it: acore::APackage | let e0: Boolean = it.aUri = packageUri in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		if (let chain: OrderedSet(acore::APackage)  = localPackage in
		if chain->notEmpty().oclIsUndefined() 
		then null 
		else chain->notEmpty()
		endif) 
		=true 
		then let chain: OrderedSet(acore::APackage)  = localPackage in
		if chain->first().oclIsUndefined() 
		then null 
		else chain->first()
		endif
		else let chain: OrderedSet(acore::APackage)  = aUsed.aPackageFromUri(packageUri)->reject(oclIsUndefined())->asOrderedSet() in
		if chain->first().oclIsUndefined() 
		then null 
		else chain->first()
		endif
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AcorePackage.Literals.ACOMPONENT);
		EOperation eOperation = AcorePackage.Literals.ACOMPONENT.getEOperations().get(0);
		if (aPackageFromUriecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aPackageFromUriecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, body, helper.getProblems(),
						AcorePackage.Literals.ACOMPONENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageFromUriecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ACOMPONENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("packageUri", packageUri);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AcorePackage.ACOMPONENT__ACOMPONENT_ID:
			return getAComponentId();
		case AcorePackage.ACOMPONENT__ABASE_URI:
			return getABaseUri();
		case AcorePackage.ACOMPONENT__ADEFAULT_URI:
			return getADefaultUri();
		case AcorePackage.ACOMPONENT__AUNDEFINED_ID_CONSTANT:
			return getAUndefinedIdConstant();
		case AcorePackage.ACOMPONENT__AUSED:
			return getAUsed();
		case AcorePackage.ACOMPONENT__AMAIN_PACKAGE:
			if (resolve)
				return getAMainPackage();
			return basicGetAMainPackage();
		case AcorePackage.ACOMPONENT__AMAIN_RESOURCE:
			if (resolve)
				return getAMainResource();
			return basicGetAMainResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AcorePackage.ACOMPONENT__ACOMPONENT_ID:
			return ACOMPONENT_ID_EDEFAULT == null ? getAComponentId() != null
					: !ACOMPONENT_ID_EDEFAULT.equals(getAComponentId());
		case AcorePackage.ACOMPONENT__ABASE_URI:
			return ABASE_URI_EDEFAULT == null ? getABaseUri() != null : !ABASE_URI_EDEFAULT.equals(getABaseUri());
		case AcorePackage.ACOMPONENT__ADEFAULT_URI:
			return ADEFAULT_URI_EDEFAULT == null ? getADefaultUri() != null
					: !ADEFAULT_URI_EDEFAULT.equals(getADefaultUri());
		case AcorePackage.ACOMPONENT__AUNDEFINED_ID_CONSTANT:
			return AUNDEFINED_ID_CONSTANT_EDEFAULT == null ? getAUndefinedIdConstant() != null
					: !AUNDEFINED_ID_CONSTANT_EDEFAULT.equals(getAUndefinedIdConstant());
		case AcorePackage.ACOMPONENT__AUSED:
			return !getAUsed().isEmpty();
		case AcorePackage.ACOMPONENT__AMAIN_PACKAGE:
			return basicGetAMainPackage() != null;
		case AcorePackage.ACOMPONENT__AMAIN_RESOURCE:
			return basicGetAMainResource() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aUri let e1: String = aBaseUri.concat('/').concat(aName) in 
	if e1.oclIsInvalid() then null else e1 endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAUri() {
		EClass eClass = (AcorePackage.Literals.ACOMPONENT);
		EStructuralFeature eOverrideFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AURI;

		if (aUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aContainingComponent self
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AComponent basicGetAContainingComponent() {
		EClass eClass = (AcorePackage.Literals.ACOMPONENT);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT;

		if (aContainingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aContainingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AComponent) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AComponentImpl
