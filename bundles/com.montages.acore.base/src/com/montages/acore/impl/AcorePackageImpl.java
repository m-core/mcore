/**
 */
package com.montages.acore.impl;

import com.montages.acore.AAbstractFolder;
import com.montages.acore.AComponent;
import com.montages.acore.AFolder;
import com.montages.acore.APackage;
import com.montages.acore.AResource;
import com.montages.acore.AStructuringElement;
import com.montages.acore.AcoreFactory;
import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.impl.AbstractionsPackageImpl;

import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.annotations.impl.AnnotationsPackageImpl;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.classifiers.impl.ClassifiersPackageImpl;

import com.montages.acore.updates.UpdatesPackage;

import com.montages.acore.updates.impl.UpdatesPackageImpl;

import com.montages.acore.values.ValuesPackage;

import com.montages.acore.values.impl.ValuesPackageImpl;

import com.montages.acore.values.rules.RulesPackage;

import com.montages.acore.values.rules.impl.RulesPackageImpl;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.xocl.core.util.XoclErrorHandler;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AcorePackageImpl extends EPackageImpl implements AcorePackage {
	/**
	 * The cached OCL constraint restricting the classes of root elements.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails().get("rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID, choiceConstraint, helper.getProblems(),
						AcorePackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aStructuringElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aAbstractFolderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aFolderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aResourceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.acore.AcorePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AcorePackageImpl() {
		super(eNS_URI, AcoreFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AcorePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AcorePackage init() {
		if (isInited)
			return (AcorePackage) EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI);

		// Obtain or create and register package
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof AcorePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new AcorePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI)
						: AnnotationsPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		RulesPackageImpl theRulesPackage = (RulesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theRulesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theRulesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAcorePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AcorePackage.eNS_URI, theAcorePackage);
		return theAcorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAStructuringElement() {
		return aStructuringElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAStructuringElement_AContainingFolder() {
		return (EReference) aStructuringElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAStructuringElement_AUri() {
		return (EAttribute) aStructuringElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAStructuringElement_AAllPackages() {
		return (EReference) aStructuringElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAAbstractFolder() {
		return aAbstractFolderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAbstractFolder_APackage() {
		return (EReference) aAbstractFolderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAbstractFolder_AResource() {
		return (EReference) aAbstractFolderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAbstractFolder_AFolder() {
		return (EReference) aAbstractFolderEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAComponent() {
		return aComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAComponent_AComponentId() {
		return (EAttribute) aComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAComponent_ABaseUri() {
		return (EAttribute) aComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAComponent_ADefaultUri() {
		return (EAttribute) aComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAComponent_AUndefinedIdConstant() {
		return (EAttribute) aComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAComponent_AUsed() {
		return (EReference) aComponentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAComponent_AMainPackage() {
		return (EReference) aComponentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAComponent_AMainResource() {
		return (EReference) aComponentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAComponent__APackageFromUri__String() {
		return aComponentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAFolder() {
		return aFolderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAPackage() {
		return aPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAPackage_ARootObjectClass() {
		return (EReference) aPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAPackage_AClassifier() {
		return (EReference) aPackageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAPackage_ASubPackage() {
		return (EReference) aPackageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAPackage_AContainingPackage() {
		return (EReference) aPackageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAPackage_ASpecialUri() {
		return (EAttribute) aPackageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAPackage_AActivePackage() {
		return (EAttribute) aPackageEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAPackage_AActiveRootPackage() {
		return (EAttribute) aPackageEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAPackage_AActiveSubPackage() {
		return (EAttribute) aPackageEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAPackage__AClassifierFromName__String() {
		return aPackageEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAResource() {
		return aResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAResource_AObject() {
		return (EReference) aResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAResource_AActiveResource() {
		return (EAttribute) aResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAResource_ARootObjectPackage() {
		return (EReference) aResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcoreFactory getAcoreFactory() {
		return (AcoreFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aStructuringElementEClass = createEClass(ASTRUCTURING_ELEMENT);
		createEReference(aStructuringElementEClass, ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER);
		createEAttribute(aStructuringElementEClass, ASTRUCTURING_ELEMENT__AURI);
		createEReference(aStructuringElementEClass, ASTRUCTURING_ELEMENT__AALL_PACKAGES);

		aAbstractFolderEClass = createEClass(AABSTRACT_FOLDER);
		createEReference(aAbstractFolderEClass, AABSTRACT_FOLDER__APACKAGE);
		createEReference(aAbstractFolderEClass, AABSTRACT_FOLDER__ARESOURCE);
		createEReference(aAbstractFolderEClass, AABSTRACT_FOLDER__AFOLDER);

		aComponentEClass = createEClass(ACOMPONENT);
		createEAttribute(aComponentEClass, ACOMPONENT__ACOMPONENT_ID);
		createEAttribute(aComponentEClass, ACOMPONENT__ABASE_URI);
		createEAttribute(aComponentEClass, ACOMPONENT__ADEFAULT_URI);
		createEAttribute(aComponentEClass, ACOMPONENT__AUNDEFINED_ID_CONSTANT);
		createEReference(aComponentEClass, ACOMPONENT__AUSED);
		createEReference(aComponentEClass, ACOMPONENT__AMAIN_PACKAGE);
		createEReference(aComponentEClass, ACOMPONENT__AMAIN_RESOURCE);
		createEOperation(aComponentEClass, ACOMPONENT___APACKAGE_FROM_URI__STRING);

		aFolderEClass = createEClass(AFOLDER);

		aPackageEClass = createEClass(APACKAGE);
		createEReference(aPackageEClass, APACKAGE__AROOT_OBJECT_CLASS);
		createEReference(aPackageEClass, APACKAGE__ACLASSIFIER);
		createEReference(aPackageEClass, APACKAGE__ASUB_PACKAGE);
		createEReference(aPackageEClass, APACKAGE__ACONTAINING_PACKAGE);
		createEAttribute(aPackageEClass, APACKAGE__ASPECIAL_URI);
		createEAttribute(aPackageEClass, APACKAGE__AACTIVE_PACKAGE);
		createEAttribute(aPackageEClass, APACKAGE__AACTIVE_ROOT_PACKAGE);
		createEAttribute(aPackageEClass, APACKAGE__AACTIVE_SUB_PACKAGE);
		createEOperation(aPackageEClass, APACKAGE___ACLASSIFIER_FROM_NAME__STRING);

		aResourceEClass = createEClass(ARESOURCE);
		createEReference(aResourceEClass, ARESOURCE__AOBJECT);
		createEAttribute(aResourceEClass, ARESOURCE__AACTIVE_RESOURCE);
		createEReference(aResourceEClass, ARESOURCE__AROOT_OBJECT_PACKAGE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AbstractionsPackage theAbstractionsPackage = (AbstractionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI);
		ValuesPackage theValuesPackage = (ValuesPackage) EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI);
		UpdatesPackage theUpdatesPackage = (UpdatesPackage) EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theAbstractionsPackage);
		getESubpackages().add(theClassifiersPackage);
		getESubpackages().add(theAnnotationsPackage);
		getESubpackages().add(theValuesPackage);
		getESubpackages().add(theUpdatesPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aStructuringElementEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aAbstractFolderEClass.getESuperTypes().add(this.getAStructuringElement());
		aComponentEClass.getESuperTypes().add(this.getAAbstractFolder());
		aFolderEClass.getESuperTypes().add(this.getAAbstractFolder());
		aPackageEClass.getESuperTypes().add(this.getAStructuringElement());
		aResourceEClass.getESuperTypes().add(this.getAStructuringElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(aStructuringElementEClass, AStructuringElement.class, "AStructuringElement", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAStructuringElement_AContainingFolder(), this.getAAbstractFolder(), null, "aContainingFolder",
				null, 0, 1, AStructuringElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAStructuringElement_AUri(), ecorePackage.getEString(), "aUri", null, 0, 1,
				AStructuringElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAStructuringElement_AAllPackages(), this.getAPackage(), null, "aAllPackages", null, 0, -1,
				AStructuringElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aAbstractFolderEClass, AAbstractFolder.class, "AAbstractFolder", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAAbstractFolder_APackage(), this.getAPackage(), null, "aPackage", null, 0, -1,
				AAbstractFolder.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAAbstractFolder_AResource(), this.getAResource(), null, "aResource", null, 0, -1,
				AAbstractFolder.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAAbstractFolder_AFolder(), this.getAFolder(), null, "aFolder", null, 0, -1,
				AAbstractFolder.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aComponentEClass, AComponent.class, "AComponent", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAComponent_AComponentId(), ecorePackage.getEString(), "aComponentId", null, 1, 1,
				AComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAComponent_ABaseUri(), ecorePackage.getEString(), "aBaseUri", null, 0, 1, AComponent.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAComponent_ADefaultUri(), ecorePackage.getEString(), "aDefaultUri", null, 0, 1,
				AComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAComponent_AUndefinedIdConstant(), ecorePackage.getEString(), "aUndefinedIdConstant", null, 0,
				1, AComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAComponent_AUsed(), this.getAComponent(), null, "aUsed", null, 0, -1, AComponent.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAComponent_AMainPackage(), this.getAPackage(), null, "aMainPackage", null, 0, 1,
				AComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAComponent_AMainResource(), this.getAResource(), null, "aMainResource", null, 0, 1,
				AComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getAComponent__APackageFromUri__String(), this.getAPackage(), "aPackageFromUri",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "packageUri", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aFolderEClass, AFolder.class, "AFolder", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aPackageEClass, APackage.class, "APackage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAPackage_ARootObjectClass(), theClassifiersPackage.getAClassifier(), null, "aRootObjectClass",
				null, 0, 1, APackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAPackage_AClassifier(), theClassifiersPackage.getAClassifier(), null, "aClassifier", null, 0,
				-1, APackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAPackage_ASubPackage(), this.getAPackage(), null, "aSubPackage", null, 0, -1, APackage.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAPackage_AContainingPackage(), this.getAPackage(), null, "aContainingPackage", null, 0, 1,
				APackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAPackage_ASpecialUri(), ecorePackage.getEString(), "aSpecialUri", null, 0, 1, APackage.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAPackage_AActivePackage(), ecorePackage.getEBooleanObject(), "aActivePackage", null, 0, 1,
				APackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAPackage_AActiveRootPackage(), ecorePackage.getEBooleanObject(), "aActiveRootPackage", null,
				0, 1, APackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAPackage_AActiveSubPackage(), ecorePackage.getEBooleanObject(), "aActiveSubPackage", null, 0,
				1, APackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAPackage__AClassifierFromName__String(), theClassifiersPackage.getAClassifier(),
				"aClassifierFromName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "classifierName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aResourceEClass, AResource.class, "AResource", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAResource_AObject(), theValuesPackage.getAObject(), null, "aObject", null, 0, -1,
				AResource.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAResource_AActiveResource(), ecorePackage.getEBooleanObject(), "aActiveResource", null, 0, 1,
				AResource.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAResource_ARootObjectPackage(), this.getAPackage(), null, "aRootObjectPackage", null, 0, 1,
				AResource.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source, new String[] { "rootConstraint", "trg.name = \'AComponent\'" });
		addAnnotation(getAStructuringElement_AContainingFolder(), source, new String[] { "derive",
				"let chain: ecore::EObject = eContainer() in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::AAbstractFolder)\n    then chain.oclAsType(acore::AAbstractFolder)\n    else null\n  endif\n  endif\n" });
		addAnnotation(getAStructuringElement_AUri(), source,
				new String[] { "derive", "let nl: String = null in nl\n" });
		addAnnotation(getAStructuringElement_AAllPackages(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAAbstractFolder_APackage(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAAbstractFolder_AResource(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAAbstractFolder_AFolder(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAComponent__APackageFromUri__String(), source, new String[] { "body",
				"let localPackage: OrderedSet(acore::APackage)  = aAllPackages->asOrderedSet()->select(it: acore::APackage | let e0: Boolean = it.aUri = packageUri in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (let chain: OrderedSet(acore::APackage)  = localPackage in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif) \n  =true \nthen let chain: OrderedSet(acore::APackage)  = localPackage in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n  else let chain: OrderedSet(acore::APackage)  = aUsed.aPackageFromUri(packageUri)->reject(oclIsUndefined())->asOrderedSet() in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\nendif\n" });
		addAnnotation(getAComponent_AComponentId(), source, new String[] { "derive", "aUndefinedIdConstant\n" });
		addAnnotation(getAComponent_ABaseUri(), source, new String[] { "derive", "aDefaultUri\n" });
		addAnnotation(getAComponent_ADefaultUri(), source, new String[] { "derive", "\'http://www.langlets.org\'\n" });
		addAnnotation(getAComponent_AUndefinedIdConstant(), source,
				new String[] { "derive", "\'<A Id Is Undefined> \'\n" });
		addAnnotation(getAComponent_AUsed(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAComponent_AMainPackage(), source,
				new String[] { "derive", "let nl: acore::APackage = null in nl\n" });
		addAnnotation(getAComponent_AMainResource(), source,
				new String[] { "derive", "let nl: acore::AResource = null in nl\n" });
		addAnnotation(getAPackage__AClassifierFromName__String(), source, new String[] { "body",
				"let classifiers: OrderedSet(acore::classifiers::AClassifier)  = aClassifier->asOrderedSet()->select(it: acore::classifiers::AClassifier | let e0: Boolean = it.aName = classifierName in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet chain: OrderedSet(acore::classifiers::AClassifier)  = classifiers in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n" });
		addAnnotation(getAPackage_ARootObjectClass(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassifier = null in nl\n" });
		addAnnotation(getAPackage_AClassifier(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAPackage_ASubPackage(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAPackage_AContainingPackage(), source,
				new String[] { "derive", "let nl: acore::APackage = null in nl\n" });
		addAnnotation(getAPackage_ASpecialUri(), source, new String[] { "derive", "let nl: String = null in nl\n" });
		addAnnotation(getAPackage_AActivePackage(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAPackage_AActiveRootPackage(), source, new String[] { "derive",
				"let e1: Boolean = if (aActivePackage)= false \n then false \n else if (let e2: Boolean = aContainingPackage = null in \n if e2.oclIsInvalid() then null else e2 endif)= false \n then false \nelse if ((aActivePackage)= null or (let e2: Boolean = aContainingPackage = null in \n if e2.oclIsInvalid() then null else e2 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getAPackage_AActiveSubPackage(), source, new String[] { "derive",
				"let e1: Boolean = if (aActivePackage)= false \n then false \n else if (let e2: Boolean = aContainingPackage <> null in \n if e2.oclIsInvalid() then null else e2 endif)= false \n then false \nelse if ((aActivePackage)= null or (let e2: Boolean = aContainingPackage <> null in \n if e2.oclIsInvalid() then null else e2 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getAResource_AObject(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAResource_AActiveResource(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAResource_ARootObjectPackage(), source,
				new String[] { "derive", "let nl: acore::APackage = null in nl\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true", "uuidAttributeName", "_uuid" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source, new String[] { "hideAdvancedProperties", "null" });
		addAnnotation(getAStructuringElement_AContainingFolder(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAStructuringElement_AUri(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAStructuringElement_AAllPackages(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core Helpers" });
		addAnnotation(getAAbstractFolder_APackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAAbstractFolder_AResource(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAAbstractFolder_AFolder(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_AComponentId(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_ABaseUri(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_ADefaultUri(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_AUndefinedIdConstant(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_AUsed(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_AMainPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAComponent_AMainResource(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage__AClassifierFromName__String(), source,
				new String[] { "propertyCategory", "z A Core", "createColumn", "true" });
		addAnnotation(getAPackage_ARootObjectClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_AClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_ASubPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_AContainingPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_ASpecialUri(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_AActivePackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_AActiveRootPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAPackage_AActiveSubPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAResource_AObject(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAResource_AActiveResource(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
		addAnnotation(getAResource_ARootObjectPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(aStructuringElementEClass, source, new String[] { "mName", "Structuring Element" });
		addAnnotation(getAStructuringElement_AContainingFolder(), source,
				new String[] { "mName", "Containing Folder" });
		addAnnotation(getAStructuringElement_AUri(), source, new String[] { "mName", "Uri" });
		addAnnotation(getAStructuringElement_AAllPackages(), source, new String[] { "mName", "All Packages" });
		addAnnotation(aAbstractFolderEClass, source, new String[] { "mName", "Abstract Folder" });
		addAnnotation(aComponentEClass, source, new String[] { "mName", "Component" });
		addAnnotation(getAComponent__APackageFromUri__String(), source, new String[] { "mName", "Package From Uri" });
		addAnnotation(getAComponent_AComponentId(), source, new String[] { "mName", "Component Id" });
		addAnnotation(getAComponent_ABaseUri(), source, new String[] { "mName", "Base Uri" });
		addAnnotation(getAComponent_ADefaultUri(), source, new String[] { "mName", "Default Uri" });
		addAnnotation(getAComponent_AUndefinedIdConstant(), source, new String[] { "mName", "Undefined Id Constant" });
		addAnnotation(getAComponent_AUsed(), source, new String[] { "mName", "Used" });
		addAnnotation(getAComponent_AMainPackage(), source, new String[] { "mName", "Main Package" });
		addAnnotation(getAComponent_AMainResource(), source, new String[] { "mName", "Main Resource" });
		addAnnotation(aFolderEClass, source, new String[] { "mName", "Folder" });
		addAnnotation(aPackageEClass, source, new String[] { "mName", "Package" });
		addAnnotation(getAPackage__AClassifierFromName__String(), source,
				new String[] { "mName", "Classifier From Name" });
		addAnnotation(getAPackage_ARootObjectClass(), source, new String[] { "mName", "Root Object Class" });
		addAnnotation(getAPackage_ASubPackage(), source, new String[] { "mName", "Sub Package" });
		addAnnotation(getAPackage_AContainingPackage(), source, new String[] { "mName", "Containing Package" });
		addAnnotation(getAPackage_ASpecialUri(), source, new String[] { "mName", "Special Uri" });
		addAnnotation(getAPackage_AActivePackage(), source, new String[] { "mName", "Active Package" });
		addAnnotation(getAPackage_AActiveRootPackage(), source, new String[] { "mName", "Active Root Package" });
		addAnnotation(getAPackage_AActiveSubPackage(), source, new String[] { "mName", "Active Sub Package" });
		addAnnotation(aResourceEClass, source, new String[] { "mName", "Resource" });
		addAnnotation(getAResource_AActiveResource(), source, new String[] { "mName", "Active Resource" });
		addAnnotation(getAResource_ARootObjectPackage(), source, new String[] { "mName", "Root Object Package" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getAStructuringElement_AContainingFolder(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAStructuringElement_AUri(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAStructuringElement_AAllPackages(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAbstractFolder_APackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAbstractFolder_AResource(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAbstractFolder_AFolder(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_AComponentId(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_ABaseUri(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_ADefaultUri(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_AUndefinedIdConstant(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_AUsed(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_AMainPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAComponent_AMainResource(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_ARootObjectClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_AClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_ASubPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_AContainingPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_ASpecialUri(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_AActivePackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_AActiveRootPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAPackage_AActiveSubPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAResource_AObject(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAResource_AActiveResource(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAResource_ARootObjectPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aAbstractFolderEClass, source, new String[] { "aAllPackagesDerive",
				"let e1: OrderedSet(acore::APackage)  = let chain11: OrderedSet(acore::APackage)  = aPackage.aAllPackages->asOrderedSet() in\nif chain11->asOrderedSet()->oclIsUndefined() \n then null \n else chain11->asOrderedSet()\n  endif->union(aFolder.aAllPackages->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(aComponentEClass, source,
				new String[] { "aUriDerive",
						"let e1: String = aBaseUri.concat(\'/\').concat(aName) in \n if e1.oclIsInvalid() then null else e1 endif\n",
						"aContainingComponentDerive", "self\n" });
		addAnnotation(aFolderEClass, source, new String[] { "aUriDerive",
				"let e1: String = if aContainingFolder.oclIsUndefined()\n  then null\n  else aContainingFolder.aUri\nendif.concat(\'/\').concat(aName) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(aPackageEClass, source, new String[] { "aUriDerive",
				"let specialUri: String = aSpecialUri in\nif (let e0: Boolean = not(aStringIsEmpty(specialUri)) in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen specialUri else if (aActiveRootPackage)=true then (let e0: String = if aContainingFolder.oclIsUndefined()\n  then null\n  else aContainingFolder.aUri\nendif.concat(\'/\').concat(aName) in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: String = if aContainingPackage.oclIsUndefined()\n  then null\n  else aContainingPackage.aUri\nendif.concat(\'/\').concat(aName) in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n",
				"aAllPackagesDerive",
				"let e1: OrderedSet(acore::APackage)  = let chain11: acore::APackage = self in\nif chain11->asOrderedSet()->oclIsUndefined() \n then null \n else chain11->asOrderedSet()\n  endif->union(aSubPackage.aAllPackages->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(aComponentEClass, source,
				new String[] { "aUriCreateColumn", "false", "aContainingComponentCreateColumn", "false" });
	}

} //AcorePackageImpl
