/**
 */

package com.montages.acore.abstractions;

import com.montages.acore.AComponent;
import com.montages.acore.APackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AFeature;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AElement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.AElement#getALabel <em>ALabel</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getAContainingComponent <em>AContaining Component</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATPackageUri <em>AT Package Uri</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATClassifierName <em>AT Classifier Name</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATFeatureName <em>AT Feature Name</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATPackage <em>AT Package</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATClassifier <em>AT Classifier</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATFeature <em>AT Feature</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AElement#getATCoreAStringClass <em>AT Core AString Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Element'"
 * @generated
 */

public interface AElement extends EObject {
	/**
	 * Returns the value of the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ALabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALabel</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ALabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Label'"
	 *        annotation="http://www.xocl.org/OCL derive='aRenderedKind\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getALabel();

	/**
	 * Returns the value of the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AKind Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AKind Base</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_AKindBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Kind Base'"
	 *        annotation="http://www.xocl.org/OCL derive='self.eClass().name\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAKindBase();

	/**
	 * Returns the value of the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ARendered Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ARendered Kind</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ARenderedKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Rendered Kind'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: String = aIndentationSpaces().concat(let chain12: String = aKindBase in\nif chain12.toUpperCase().oclIsUndefined() \n then null \n else chain12.toUpperCase()\n  endif) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getARenderedKind();

	/**
	 * Returns the value of the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Component</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_AContainingComponent()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Component'"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsTypeOf(acore::AComponent)\r\n  then self.eContainer().oclAsType(acore::AComponent)\r\n  else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)\r\n  then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent\r\n  else null endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AComponent getAContainingComponent();

	/**
	 * Returns the value of the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Package Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Package Uri</em>' attribute.
	 * @see #isSetATPackageUri()
	 * @see #unsetATPackageUri()
	 * @see #setATPackageUri(String)
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATPackageUri()
	 * @model unsettable="true" transient="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Package Uri'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getATPackageUri();

	/** 
	 * Sets the value of the '{@link com.montages.acore.abstractions.AElement#getATPackageUri <em>AT Package Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AT Package Uri</em>' attribute.
	 * @see #isSetATPackageUri()
	 * @see #unsetATPackageUri()
	 * @see #getATPackageUri()
	 * @generated
	 */

	void setATPackageUri(String value);

	/**
	 * Unsets the value of the '{@link com.montages.acore.abstractions.AElement#getATPackageUri <em>AT Package Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetATPackageUri()
	 * @see #getATPackageUri()
	 * @see #setATPackageUri(String)
	 * @generated
	 */
	void unsetATPackageUri();

	/**
	 * Returns whether the value of the '{@link com.montages.acore.abstractions.AElement#getATPackageUri <em>AT Package Uri</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AT Package Uri</em>' attribute is set.
	 * @see #unsetATPackageUri()
	 * @see #getATPackageUri()
	 * @see #setATPackageUri(String)
	 * @generated
	 */
	boolean isSetATPackageUri();

	/**
	 * Returns the value of the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Classifier Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Classifier Name</em>' attribute.
	 * @see #isSetATClassifierName()
	 * @see #unsetATClassifierName()
	 * @see #setATClassifierName(String)
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATClassifierName()
	 * @model unsettable="true" transient="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Classifier Name'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getATClassifierName();

	/** 
	 * Sets the value of the '{@link com.montages.acore.abstractions.AElement#getATClassifierName <em>AT Classifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AT Classifier Name</em>' attribute.
	 * @see #isSetATClassifierName()
	 * @see #unsetATClassifierName()
	 * @see #getATClassifierName()
	 * @generated
	 */

	void setATClassifierName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.acore.abstractions.AElement#getATClassifierName <em>AT Classifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetATClassifierName()
	 * @see #getATClassifierName()
	 * @see #setATClassifierName(String)
	 * @generated
	 */
	void unsetATClassifierName();

	/**
	 * Returns whether the value of the '{@link com.montages.acore.abstractions.AElement#getATClassifierName <em>AT Classifier Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AT Classifier Name</em>' attribute is set.
	 * @see #unsetATClassifierName()
	 * @see #getATClassifierName()
	 * @see #setATClassifierName(String)
	 * @generated
	 */
	boolean isSetATClassifierName();

	/**
	 * Returns the value of the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Feature Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Feature Name</em>' attribute.
	 * @see #isSetATFeatureName()
	 * @see #unsetATFeatureName()
	 * @see #setATFeatureName(String)
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATFeatureName()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Feature Name'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getATFeatureName();

	/** 
	 * Sets the value of the '{@link com.montages.acore.abstractions.AElement#getATFeatureName <em>AT Feature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AT Feature Name</em>' attribute.
	 * @see #isSetATFeatureName()
	 * @see #unsetATFeatureName()
	 * @see #getATFeatureName()
	 * @generated
	 */

	void setATFeatureName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.acore.abstractions.AElement#getATFeatureName <em>AT Feature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetATFeatureName()
	 * @see #getATFeatureName()
	 * @see #setATFeatureName(String)
	 * @generated
	 */
	void unsetATFeatureName();

	/**
	 * Returns whether the value of the '{@link com.montages.acore.abstractions.AElement#getATFeatureName <em>AT Feature Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AT Feature Name</em>' attribute is set.
	 * @see #unsetATFeatureName()
	 * @see #getATFeatureName()
	 * @see #setATFeatureName(String)
	 * @generated
	 */
	boolean isSetATFeatureName();

	/**
	 * Returns the value of the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Package</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Package'"
	 *        annotation="http://www.xocl.org/OCL derive='let p: String = aTPackageUri in\naPackageFromUri(p)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getATPackage();

	/**
	 * Returns the value of the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Classifier</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Classifier'"
	 *        annotation="http://www.xocl.org/OCL derive='let p: String = aTPackageUri in\nlet c: String = aTClassifierName in\naClassifierFromUriAndName(p, c)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getATClassifier();

	/**
	 * Returns the value of the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Feature</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Feature'"
	 *        annotation="http://www.xocl.org/OCL derive='let p: String = aTPackageUri in\nlet c: String = aTClassifierName in\nlet f: String = aTFeatureName in\naFeatureFromUriAndNames(p, c, f)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AFeature getATFeature();

	/**
	 * Returns the value of the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AT Core AString Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AT Core AString Class</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAElement_ATCoreAStringClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='T Core A String Class'"
	 *        annotation="http://www.xocl.org/OCL derive='aCoreAStringClass()\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Package'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getATCoreAStringClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Indent Level'"
	 *        annotation="http://www.xocl.org/OCL body='if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(AElement)\n  then eContainer().oclAsType(AElement).aIndentLevel() + 1\n  else 0 endif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	Integer aIndentLevel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Indentation Spaces'"
	 *        annotation="http://www.xocl.org/OCL body='let numberOfSpaces: Integer = let e1: Integer = aIndentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\naIndentationSpaces(numberOfSpaces)\n'"
	 * @generated
	 */
	String aIndentationSpaces();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Indentation Spaces'"
	 *        annotation="http://www.xocl.org/OCL body='let sizeMinusOne: Integer = let e1: Integer = size - 1 in \n if e1.oclIsInvalid() then null else e1 endif in\nif (let e0: Boolean = size < 1 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\'\n  else (let e0: String = aIndentationSpaces(sizeMinusOne).concat(\' \') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	String aIndentationSpaces(Integer size);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pAnnotation="http://www.montages.com/mCore/MCore mName='p'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='String Or Missing'"
	 *        annotation="http://www.xocl.org/OCL body='if (aStringIsEmpty(p)) \n  =true \nthen \'MISSING\'\n  else p\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	String aStringOrMissing(String p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='String Is Empty'"
	 *        annotation="http://www.xocl.org/OCL body='if ( s.oclIsUndefined()) \n  =true \nthen true else if (let e0: Boolean = \'\' = let e0: String = s.trim() in \n if e0.oclIsInvalid() then null else e0 endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	Boolean aStringIsEmpty(String s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementsMany="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='List Of String To String With Separator'"
	 *        annotation="http://www.xocl.org/OCL body='let f:String = elements->asOrderedSet()->first() in\r\nif f.oclIsUndefined()\r\n  then \'\'\r\n  else if elements-> size()=1 then f else\r\n    let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=\'\'|ac.concat(separator).concat(it)) in\r\n    f.concat(rest)\r\n  endif endif\r\n'"
	 * @generated
	 */
	String aListOfStringToStringWithSeparator(EList<String> elements, String separator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementsMany="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='List Of String To String With Separator'"
	 *        annotation="http://www.xocl.org/OCL body='let defaultSeparator: String = \', \' in\naListOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	String aListOfStringToStringWithSeparator(EList<String> elements);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Package From Uri'"
	 *        annotation="http://www.xocl.org/OCL body='if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aPackageFromUri(packageUri)\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	APackage aPackageFromUri(String packageUri);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Classifier From Uri And Name'"
	 *        annotation="http://www.xocl.org/OCL body='let p: acore::APackage = aPackageFromUri(uri) in\nif p = null\n  then null\n  else p.aClassifierFromName(name) endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	AClassifier aClassifierFromUriAndName(String uri, String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Feature From Uri And Names'"
	 *        annotation="http://www.xocl.org/OCL body='let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in\nlet cAsClass: acore::classifiers::AClassType = let chain: acore::classifiers::AClassifier = c in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClassType)\n    then chain.oclAsType(acore::classifiers::AClassType)\n    else null\n  endif\n  endif in\nif cAsClass = null\n  then null\n  else cAsClass.aFeatureFromName(featureName) endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='true'"
	 * @generated
	 */
	AFeature aFeatureFromUriAndNames(String uri, String className, String featureName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Core A String Class'"
	 *        annotation="http://www.xocl.org/OCL body='let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAStringName: String = \'AString\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreAStringClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Core A Real Class'"
	 *        annotation="http://www.xocl.org/OCL body='let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreARealName: String = \'AReal\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreARealClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Core A Integer Class'"
	 *        annotation="http://www.xocl.org/OCL body='let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAIntegerName: String = \'AInteger\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreAIntegerClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Core A Object Class'"
	 *        annotation="http://www.xocl.org/OCL body='let aCoreValuesPackageUri: String = \'http://www.langlets.org/ACore/ACore/Values\' in\nlet aCoreAObjectName: String = \'AObject\' in\naClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Package' createColumn='true'"
	 * @generated
	 */
	AClassifier aCoreAObjectClass();

} // AElement
