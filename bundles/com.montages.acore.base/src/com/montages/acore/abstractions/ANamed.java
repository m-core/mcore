/**
 */

package com.montages.acore.abstractions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ANamed</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.ANamed#getAName <em>AName</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ANamed#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ANamed#getABusinessName <em>ABusiness Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.abstractions.AbstractionsPackage#getANamed()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Named'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aLabelDerive='aName\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aLabelCreateColumn='false'"
 * @generated
 */

public interface ANamed extends AElement {
	/**
	 * Returns the value of the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AName</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AName</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getANamed_AName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Name'"
	 *        annotation="http://www.xocl.org/OCL derive='aUndefinedNameConstant\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAName();

	/**
	 * Returns the value of the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUndefined Name Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUndefined Name Constant</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getANamed_AUndefinedNameConstant()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Undefined Name Constant'"
	 *        annotation="http://www.xocl.org/OCL derive='\'<A Name Is Undefined> \'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAUndefinedNameConstant();

	/**
	 * Returns the value of the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ABusiness Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABusiness Name</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getANamed_ABusinessName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Business Name'"
	 *        annotation="http://www.xocl.org/OCL derive='let chain : String = aName in\nif chain.oclIsUndefined()\n  then null\n  else chain .camelCaseToBusiness()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Abstractions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getABusinessName();

} // ANamed
