/**
 */

package com.montages.acore.abstractions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AVariable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.acore.abstractions.AbstractionsPackage#getAVariable()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Variable'"
 * @generated
 */

public interface AVariable extends ANamed, ATyped {
} // AVariable
