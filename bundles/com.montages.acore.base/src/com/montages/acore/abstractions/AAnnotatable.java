/**
 */

package com.montages.acore.abstractions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AAnnotatable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.AAnnotatable#getAAnnotation <em>AAnnotation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.abstractions.AbstractionsPackage#getAAnnotatable()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Annotatable'"
 * @generated
 */

public interface AAnnotatable extends EObject {
	/**
	 * Returns the value of the '<em><b>AAnnotation</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.abstractions.AAnnotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotation</em>' reference list.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAAnnotatable_AAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	EList<AAnnotation> getAAnnotation();

} // AAnnotatable
