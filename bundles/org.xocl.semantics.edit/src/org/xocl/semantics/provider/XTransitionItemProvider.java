/**
 */
package org.xocl.semantics.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XTransition;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XTransition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XTransitionItemProvider extends XUpdateContainerItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTransitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner, org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof XTransition) {
			XTransition typedOwner = ((XTransition) owner);

			if (feature == SemanticsPackage.eINSTANCE.getXTransition_ExecuteNow())
				myUpdate = typedOwner.executeNow$Update((java.lang.Boolean) value);

		}

		return myUpdate == null ? super.createSetCommand(domain, owner, feature, value, index)
				: myUpdate.getContainingTransition().interpreteTransitionAsCommand();

	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addExecuteAutomaticallyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addExecuteNowPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addExecutionTimePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAllUpdatesPropertyDescriptor(object);
			}
			addFocusObjectsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Execute Automatically feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecuteAutomaticallyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Execute Automatically feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XTransition_executeAutomatically_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XTransition_executeAutomatically_feature",
								"_UI_XTransition_type"),
						SemanticsPackage.Literals.XTRANSITION__EXECUTE_AUTOMATICALLY, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Execute Now feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecuteNowPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Execute Now feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XTransition_executeNow_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XTransition_executeNow_feature",
								"_UI_XTransition_type"),
						SemanticsPackage.Literals.XTRANSITION__EXECUTE_NOW, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Execution Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionTimePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Execution Time feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XTransition_executionTime_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XTransition_executionTime_feature",
								"_UI_XTransition_type"),
						SemanticsPackage.Literals.XTRANSITION__EXECUTION_TIME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the All Updates feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllUpdatesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the All Updates feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XTransition_allUpdates_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XTransition_allUpdates_feature",
								"_UI_XTransition_type"),
						SemanticsPackage.Literals.XTRANSITION__ALL_UPDATES, false, false, false, null, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Focus Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFocusObjectsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Focus Objects feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XTransition_focusObjects_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XTransition_focusObjects_feature",
								"_UI_XTransition_type"),
						SemanticsPackage.Literals.XTRANSITION__FOCUS_OBJECTS, true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SemanticsPackage.Literals.XTRANSITION__TRIGGERING_UPDATE);
			childrenFeatures.add(SemanticsPackage.Literals.XTRANSITION__UPDATED_OBJECT);
			childrenFeatures.add(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns XTransition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/XTransition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XTransition) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XTransition.class)) {
		case SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY:
		case SemanticsPackage.XTRANSITION__EXECUTE_NOW:
		case SemanticsPackage.XTRANSITION__EXECUTION_TIME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE:
		case SemanticsPackage.XTRANSITION__UPDATED_OBJECT:
		case SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__TRIGGERING_UPDATE,
				SemanticsFactory.eINSTANCE.createXTriggeredUpdate()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__UPDATED_OBJECT,
				SemanticsFactory.eINSTANCE.createXUpdatedObject()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXSemantics()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXTransition()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXAddSpecification()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXAdditionalTriggeringSpecification()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXTriggeredUpdate()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXOriginalTriggeringSpecification()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXUpdatedObject()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXUpdatedFeature()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createXUpdatedOperation()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT,
				SemanticsFactory.eINSTANCE.createDUmmy()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == SemanticsPackage.Literals.XTRANSITION__TRIGGERING_UPDATE
				|| childFeature == SemanticsPackage.Literals.XTRANSITION__DANGLING_REMOVED_OBJECT
				|| childFeature == SemanticsPackage.Literals.XTRANSITION__UPDATED_OBJECT;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !SemanticsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
