/**
 */
package org.xocl.semantics.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XUpdate;

import org.xocl.semantics.impl.XUpdateImpl;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XUpdate} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XUpdateItemProvider extends XUpdateContainerItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addUpdatedObjectPropertyDescriptor(object);
			addFeaturePropertyDescriptor(object);
			addOperationPropertyDescriptor(object);
			addModePropertyDescriptor(object);
			addContributesToPropertyDescriptor(object);
			addObjectPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Updated Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpdatedObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Updated Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdate_updatedObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdate_updatedObject_feature",
								"_UI_XUpdate_type"),
						SemanticsPackage.Literals.XUPDATE__UPDATED_OBJECT, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature feature.
		 * The list of possible choices is constructed by OCL if updatedObject.oclIsUndefined() then OrderedSet{} else 
		
		if updatedObject.object.oclIsUndefined() then
		if updatedObject.classForCreation.oclIsUndefined() then OrderedSet{} else
		  updatedObject.classForCreation.eAllStructuralFeatures endif
		 else updatedObject.object.eClass().eAllStructuralFeatures endif
		endif
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdate_feature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdate_feature_feature",
								"_UI_XUpdate_type"),
						SemanticsPackage.Literals.XUPDATE__FEATURE, true, false, false, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<EStructuralFeature> result = new ArrayList<EStructuralFeature>();
						Collection<? extends EStructuralFeature> superResult = (Collection<? extends EStructuralFeature>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((XUpdateImpl) object).evalFeatureChoiceConstruction(result);

						result.remove(null);

						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Mode feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdate_mode_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdate_mode_feature", "_UI_XUpdate_type"),
						SemanticsPackage.Literals.XUPDATE__MODE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Contributes To feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContributesToPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Contributes To feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdate_contributesTo_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdate_contributesTo_feature",
								"_UI_XUpdate_type"),
						SemanticsPackage.Literals.XUPDATE__CONTRIBUTES_TO, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdate_object_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdate_object_feature",
								"_UI_XUpdate_type"),
						SemanticsPackage.Literals.XUPDATE__OBJECT, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Operation feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdate_operation_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdate_operation_feature",
								"_UI_XUpdate_type"),
						SemanticsPackage.Literals.XUPDATE__OPERATION, true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATE__VALUE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATE__ADD_SPECIFICATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XUpdate) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XUpdate.class)) {
		case SemanticsPackage.XUPDATE__MODE:
		case SemanticsPackage.XUPDATE__OBJECT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SemanticsPackage.XUPDATE__VALUE:
		case SemanticsPackage.XUPDATE__ADD_SPECIFICATION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATE__VALUE,
				SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATE__ADD_SPECIFICATION,
				SemanticsFactory.eINSTANCE.createXAddSpecification()));
	}

}
