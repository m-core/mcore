/**
 */
package org.xocl.semantics.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.EPackage.Registry;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XTriggeringSpecification;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XTriggeringSpecification} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XTriggeringSpecificationItemProvider extends XSemanticsElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTriggeringSpecificationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFeatureOfTriggeringUpdateAnnotationPropertyDescriptor(object);
			addNameofTriggeringUpdateAnnotationPropertyDescriptor(object);
			addTriggeringUpdatePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Feature Of Triggering Update Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeatureOfTriggeringUpdateAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature Of Triggering Update Annotation feature.
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XTriggeringSpecification_featureOfTriggeringUpdateAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XTriggeringSpecification_featureOfTriggeringUpdateAnnotation_feature",
						"_UI_XTriggeringSpecification_type"),
				SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION, false,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<EStructuralFeature> result = new ArrayList<EStructuralFeature>();
				Collection<? extends EStructuralFeature> superResult = (Collection<? extends EStructuralFeature>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(result);
				Resource resource = ((EObject) object).eResource();
				if (resource != null) {
					ResourceSet resourceSet = resource.getResourceSet();
					if (resourceSet != null) {
						Collection<EObject> visited = new HashSet<EObject>(eObjects);
						Registry packageRegistry = resourceSet.getPackageRegistry();
						for (Iterator<String> i = packageRegistry.keySet().iterator(); i.hasNext();) {
							collectReachableObjectsOfType(visited, eObjects, packageRegistry.getEPackage(i.next()),
									SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION
											.getEType());
						}
						result = (List<EStructuralFeature>) (List<?>) eObjects;
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Nameof Triggering Update Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNameofTriggeringUpdateAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Nameof Triggering Update Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XTriggeringSpecification_nameofTriggeringUpdateAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XTriggeringSpecification_nameofTriggeringUpdateAnnotation_feature",
						"_UI_XTriggeringSpecification_type"),
				SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Triggering Update feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTriggeringUpdatePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Triggering Update feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XTriggeringSpecification_triggeringUpdate_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XTriggeringSpecification_triggeringUpdate_feature",
						"_UI_XTriggeringSpecification_type"),
				SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE, false, false, false, null, null,
				null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XTriggeringSpecification) object).getNameofTriggeringUpdateAnnotation();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XTriggeringSpecification.class)) {
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
