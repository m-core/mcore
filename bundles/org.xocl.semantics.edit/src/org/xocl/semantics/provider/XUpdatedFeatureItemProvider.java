/**
 */
package org.xocl.semantics.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XUpdatedFeature;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XUpdatedFeature} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XUpdatedFeatureItemProvider extends XSemanticsElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedFeatureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContributingUpdatePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addExpectationsMetPropertyDescriptor(object);
			}
			addInconsistentPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addExecutedPropertyDescriptor(object);
			}
			addTowardsEndInsertPositionPropertyDescriptor(object);
			addInTheMiddleInsertPositionPropertyDescriptor(object);
			addTowardsBeginningInsertPositionPropertyDescriptor(object);
			addTowardsBeginningInsertValuesPropertyDescriptor(object);
			addFeaturePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature feature.
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdatedFeature_feature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedFeature_feature_feature",
								"_UI_XUpdatedFeature_type"),
						SemanticsPackage.Literals.XUPDATED_FEATURE__FEATURE, true, false, true, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<EStructuralFeature> result = new ArrayList<EStructuralFeature>();
						Collection<? extends EStructuralFeature> superResult = (Collection<? extends EStructuralFeature>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(result);
						Resource resource = ((EObject) object).eResource();
						if (resource != null) {
							ResourceSet resourceSet = resource.getResourceSet();
							if (resourceSet != null) {
								Collection<EObject> visited = new HashSet<EObject>(eObjects);
								Registry packageRegistry = resourceSet.getPackageRegistry();
								for (Iterator<String> i = packageRegistry.keySet().iterator(); i.hasNext();) {
									collectReachableObjectsOfType(visited, eObjects,
											packageRegistry.getEPackage(i.next()),
											SemanticsPackage.Literals.XUPDATED_FEATURE__FEATURE.getEType());
								}
								result = (List<EStructuralFeature>) (List<?>) eObjects;
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Contributing Update feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContributingUpdatePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Contributing Update feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdatedTypedElement_contributingUpdate_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_XUpdatedTypedElement_contributingUpdate_feature", "_UI_XUpdatedTypedElement_type"),
						SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE, true, false, true, null,
						null, null));
	}

	/**
	 * This adds a property descriptor for the Inconsistent feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInconsistentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Inconsistent feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdatedTypedElement_inconsistent_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedTypedElement_inconsistent_feature",
								"_UI_XUpdatedTypedElement_type"),
						SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__INCONSISTENT, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_TransitionExecutionPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expectations Met feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectationsMetPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Expectations Met feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdatedTypedElement_expectationsMet_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_XUpdatedTypedElement_expectationsMet_feature", "_UI_XUpdatedTypedElement_type"),
						SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ExpectationTestingPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Executed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Executed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedTypedElement_executed_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedTypedElement_executed_feature",
						"_UI_XUpdatedTypedElement_type"),
				SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXECUTED, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TransitionExecutionPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Towards End Insert Position feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTowardsEndInsertPositionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Towards End Insert Position feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedTypedElement_towardsEndInsertPosition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XUpdatedTypedElement_towardsEndInsertPosition_feature", "_UI_XUpdatedTypedElement_type"),
				SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TransitionExecutionPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the In The Middle Insert Position feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInTheMiddleInsertPositionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the In The Middle Insert Position feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedTypedElement_inTheMiddleInsertPosition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XUpdatedTypedElement_inTheMiddleInsertPosition_feature", "_UI_XUpdatedTypedElement_type"),
				SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TransitionExecutionPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Towards Beginning Insert Position feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTowardsBeginningInsertPositionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Towards Beginning Insert Position feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedTypedElement_towardsBeginningInsertPosition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XUpdatedTypedElement_towardsBeginningInsertPosition_feature",
						"_UI_XUpdatedTypedElement_type"),
				SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TransitionExecutionPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Towards Beginning Insert Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTowardsBeginningInsertValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Towards Beginning Insert Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedTypedElement_towardsBeginningInsertValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XUpdatedTypedElement_towardsBeginningInsertValues_feature",
						"_UI_XUpdatedTypedElement_type"),
				SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES, true, false, true,
				null, getString("_UI_TransitionExecutionPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__OLD_VALUE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__NEW_VALUE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__DELETE_VALUES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns XUpdatedFeature.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/XUpdatedFeature"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XUpdatedFeature) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XUpdatedFeature.class)) {
		case SemanticsPackage.XUPDATED_FEATURE__EXPECTATIONS_MET:
		case SemanticsPackage.XUPDATED_FEATURE__INCONSISTENT:
		case SemanticsPackage.XUPDATED_FEATURE__EXECUTED:
		case SemanticsPackage.XUPDATED_FEATURE__TOWARDS_END_INSERT_POSITION:
		case SemanticsPackage.XUPDATED_FEATURE__IN_THE_MIDDLE_INSERT_POSITION:
		case SemanticsPackage.XUPDATED_FEATURE__TOWARDS_BEGINNING_INSERT_POSITION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SemanticsPackage.XUPDATED_FEATURE__EXPECTED_TO_BE_ADDED_VALUE:
		case SemanticsPackage.XUPDATED_FEATURE__EXPECTED_TO_BE_REMOVED_VALUE:
		case SemanticsPackage.XUPDATED_FEATURE__EXPECTED_RESULTING_VALUE:
		case SemanticsPackage.XUPDATED_FEATURE__OLD_VALUE:
		case SemanticsPackage.XUPDATED_FEATURE__NEW_VALUE:
		case SemanticsPackage.XUPDATED_FEATURE__TOWARDS_END_INSERT_VALUES:
		case SemanticsPackage.XUPDATED_FEATURE__IN_THE_MIDDLE_INSERT_VALUES:
		case SemanticsPackage.XUPDATED_FEATURE__AS_FIRST_INSERT_VALUES:
		case SemanticsPackage.XUPDATED_FEATURE__AS_LAST_INSERT_VALUES:
		case SemanticsPackage.XUPDATED_FEATURE__DELETE_VALUES:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors.add(
				createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__OLD_VALUE,
				SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__NEW_VALUE,
				SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES,
						SemanticsFactory.eINSTANCE.createXValue()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__DELETE_VALUES,
				SemanticsFactory.eINSTANCE.createXValue()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__OLD_VALUE
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__NEW_VALUE
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES
				|| childFeature == SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__DELETE_VALUES;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !SemanticsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
