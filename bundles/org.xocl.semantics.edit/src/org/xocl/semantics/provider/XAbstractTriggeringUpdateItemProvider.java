/**
 */
package org.xocl.semantics.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XAbstractTriggeringUpdate} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XAbstractTriggeringUpdateItemProvider extends XUpdateItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAbstractTriggeringUpdateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addProcesssedPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAdditionallyTriggeringUpdatePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAllOwnedUpdatesPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Processsed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcesssedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processsed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XAbstractTriggeringUpdate_processsed_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XAbstractTriggeringUpdate_processsed_feature",
						"_UI_XAbstractTriggeringUpdate_type"),
				SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Additionally Triggering Update feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAdditionallyTriggeringUpdatePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Additionally Triggering Update feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XAbstractTriggeringUpdate_additionallyTriggeringUpdate_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XAbstractTriggeringUpdate_additionallyTriggeringUpdate_feature",
						"_UI_XAbstractTriggeringUpdate_type"),
				SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE, false, false,
				false, null, null, new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the All Owned Updates feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllOwnedUpdatesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the All Owned Updates feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XAbstractTriggeringUpdate_allOwnedUpdates_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XAbstractTriggeringUpdate_allOwnedUpdates_feature",
						"_UI_XAbstractTriggeringUpdate_type"),
				SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES, false, false, false, null,
				null, new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE);
			childrenFeatures.add(SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XAbstractTriggeringUpdate) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XAbstractTriggeringUpdate.class)) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE,
						SemanticsFactory.eINSTANCE.createXTriggeredUpdate()));

		newChildDescriptors.add(createChildParameter(
				SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS,
				SemanticsFactory.eINSTANCE.createXAdditionalTriggeringSpecification()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !SemanticsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
