/**
 */
package org.xocl.semantics.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XUpdatedObject;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XUpdatedObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XUpdatedObjectItemProvider extends XSemanticsElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addObjectUpdateTypePropertyDescriptor(object);
			addObjectPropertyDescriptor(object);
			addClassForCreationPropertyDescriptor(object);
			addObjectIsFocusedAfterExecutionPropertyDescriptor(object);
			addXUpdatedTypedElementPropertyDescriptor(object);
			addPersistenceLocationPropertyDescriptor(object);
			addAlternativePersistencePackagePropertyDescriptor(object);
			addAlternativePersistenceRootPropertyDescriptor(object);
			addAlternativePersistenceReferencePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdatedObject_object_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedObject_object_feature",
								"_UI_XUpdatedObject_type"),
						SemanticsPackage.Literals.XUPDATED_OBJECT__OBJECT, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Object Update Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectUpdateTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object Update Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_objectUpdateType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedObject_objectUpdateType_feature",
						"_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Class For Creation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassForCreationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Class For Creation feature.
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_classForCreation_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedObject_classForCreation_feature",
						"_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__CLASS_FOR_CREATION, true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<EClass> result = new ArrayList<EClass>();
				Collection<? extends EClass> superResult = (Collection<? extends EClass>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(result);
				Resource resource = ((EObject) object).eResource();
				if (resource != null) {
					ResourceSet resourceSet = resource.getResourceSet();
					if (resourceSet != null) {
						Collection<EObject> visited = new HashSet<EObject>(eObjects);
						Registry packageRegistry = resourceSet.getPackageRegistry();
						for (Iterator<String> i = packageRegistry.keySet().iterator(); i.hasNext();) {
							collectReachableObjectsOfType(visited, eObjects, packageRegistry.getEPackage(i.next()),
									SemanticsPackage.Literals.XUPDATED_OBJECT__CLASS_FOR_CREATION.getEType());
						}
						result = (List<EClass>) (List<?>) eObjects;
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Object Is Focused After Execution feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectIsFocusedAfterExecutionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object Is Focused After Execution feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XUpdatedObject_objectIsFocusedAfterExecution_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_XUpdatedObject_objectIsFocusedAfterExecution_feature", "_UI_XUpdatedObject_type"),
						SemanticsPackage.Literals.XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION, false, false,
						false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the XUpdated Typed Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addXUpdatedTypedElementPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the XUpdated Typed Element feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_xUpdatedTypedElement_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedObject_xUpdatedTypedElement_feature",
						"_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT, false, false, false, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Persistence Location feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPersistenceLocationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Persistence Location feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_persistenceLocation_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedObject_persistenceLocation_feature",
						"_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__PERSISTENCE_LOCATION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_PersistencePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Alternative Persistence Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAlternativePersistencePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Alternative Persistence Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_alternativePersistencePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XUpdatedObject_alternativePersistencePackage_feature", "_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_PersistencePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Alternative Persistence Root feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAlternativePersistenceRootPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Alternative Persistence Root feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_alternativePersistenceRoot_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_XUpdatedObject_alternativePersistenceRoot_feature",
						"_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_PersistencePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Alternative Persistence Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAlternativePersistenceReferencePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Alternative Persistence Reference feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_XUpdatedObject_alternativePersistenceReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_XUpdatedObject_alternativePersistenceReference_feature", "_UI_XUpdatedObject_type"),
				SemanticsPackage.Literals.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_PersistencePropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_OBJECT__UPDATED_FEATURE);
			childrenFeatures.add(SemanticsPackage.Literals.XUPDATED_OBJECT__UPDATED_OPERATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns XUpdatedObject.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/XUpdatedObject"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XUpdatedObject) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XUpdatedObject.class)) {
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE:
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION:
		case SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION:
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE:
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT:
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE:
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_OBJECT__UPDATED_FEATURE,
				SemanticsFactory.eINSTANCE.createXUpdatedFeature()));

		newChildDescriptors.add(createChildParameter(SemanticsPackage.Literals.XUPDATED_OBJECT__UPDATED_OPERATION,
				SemanticsFactory.eINSTANCE.createXUpdatedOperation()));
	}

}
