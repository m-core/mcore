package org.xocl.common.ui.celleditors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.xocl.common.ui.dialogs.DateTimeDialog;

public class DateTimeCellEditor extends TextAndDialogCellEditor {

	private Date myDate;
	private String myToolTipText;

	public DateTimeCellEditor(Composite parent, Date date, ICellEditorValidator validator) {
		super(parent);
		
		myDate = date;

		setValidator(validator);
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		DateTimeDialog dialog = new DateTimeDialog(cellEditorWindow.getShell(), getDate());
		if (dialog.open() == Window.OK) {
			return dialog.getValue();
		} else {
			return null;
		}
	}
	
	@Override
	protected Object convertTextToValue(String text) {
		if ((text == null) || (text.length() == 0)) {
			return null;
		}
		return EcoreUtil.createFromString(EcorePackage.Literals.EDATE, text);
	}
	
	@Override
	protected String convertValueToText(Object value) {
	      String result = EcoreUtil.convertToString(EcorePackage.Literals.EDATE, value);
	      return result == null ? "" : result; //$NON-NLS-1$
	}
	
	@Override
	protected String getToolTipText() {
		if (myToolTipText == null) {
			DateFormat[] dateFormats = new EFactoryImpl() {
				public DateFormat[] getDateFormats() {
					return EDATE_FORMATS;
				}
			}.getDateFormats();
			String toolTipText = Messages.DateTimeCellEditor_DateTimeFormatsHint;
			String formats = ""; //$NON-NLS-1$
			for (DateFormat dateFormat : dateFormats) {
				if (!(dateFormat instanceof SimpleDateFormat)) {
					continue;
				}
				if (formats.length() > 0) {
					formats += ", "; //$NON-NLS-1$
				}
				formats += ((SimpleDateFormat) dateFormat).toPattern(); 
			}
			myToolTipText = NLS.bind(toolTipText, formats);
		}
		return myToolTipText;
	}

	public Date getDate() {
		return myDate;
	}

	public void setDate(Date date) {
		myDate = date;
	}
}