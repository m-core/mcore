/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.common.ui.celleditors;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

/**
 * @author Max Stepanov
 *
 */
public class FontCellEditor extends CellEditor {

	private static final String FONT_SCHEMA = "font"; //$NON-NLS-1$
	private static final String BOLD = "bold"; //$NON-NLS-1$
	private static final String ITALIC = "italic"; //$NON-NLS-1$

	private Button boldButton;
	private Button italicButton;
	private Spinner sizeSpinner;
	private KeyListener keyListener;

	/**
	 * @param parent
	 */
	public FontCellEditor(Composite parent) {
		super(parent);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createControl(Composite parent) {
		Font font = parent.getFont();
		Color background = parent.getBackground();
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setFont(font);
		composite.setBackground(background);
		composite.setLayout(GridLayoutFactory.fillDefaults().numColumns(4).create());

		boldButton = new Button(composite, SWT.CHECK);
		boldButton.setText(BOLD);
		boldButton.setFont(font);
		boldButton.setBackground(background);
		boldButton.setLayoutData(GridDataFactory.fillDefaults().create());

		italicButton = new Button(composite, SWT.CHECK);
		italicButton.setText(ITALIC);
		italicButton.setFont(font);
		italicButton.setBackground(background);
		italicButton.setLayoutData(GridDataFactory.fillDefaults().create());

		sizeSpinner = new Spinner(composite, SWT.NONE);
		sizeSpinner.setFont(font);
		sizeSpinner.setBackground(background);
		sizeSpinner.setIncrement(1);
		sizeSpinner.setMinimum(-10);
		sizeSpinner.setMaximum(10);
		sizeSpinner.setLayoutData(GridDataFactory.fillDefaults().hint(SWT.DEFAULT, boldButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y).create());

		Label label = new Label(composite, SWT.NONE);
		label.setText("size"); //$NON-NLS-1$
		label.setFont(font);
		label.setBackground(background);
		label.setLayoutData(GridDataFactory.fillDefaults().create());

		return composite;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#activate()
	 */
	@Override
	public void activate() {
		if (keyListener == null) {
			getControl().getParent().addKeyListener(keyListener = new KeyListener() {
				public void keyReleased(KeyEvent e) {
					keyReleaseOccured(e);
				}
				public void keyPressed(KeyEvent e) {
				}
			});
			sizeSpinner.addKeyListener(keyListener);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#deactivate()
	 */
	@Override
	public void deactivate() {
		if (keyListener != null) {
			if (!sizeSpinner.isDisposed()) {
				sizeSpinner.removeKeyListener(keyListener);
			}
			Control control = getControl();
			Composite parent = control != null ? control.getParent() : null;
			if (parent != null && !parent.isDisposed()) {
				parent.removeKeyListener(keyListener);
			}
			keyListener = null;
		}
		super.deactivate();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doGetValue()
	 */
	@Override
	protected Object doGetValue() {
		String fontStyle = boldButton.getSelection() ? BOLD : ""; //$NON-NLS-1$
		if (italicButton.getSelection()) {
			if (fontStyle.length() > 0) {
				fontStyle += '+';
			}
			fontStyle += ITALIC;
		}
		int fontSize = sizeSpinner.getSelection();
		return URI.createHierarchicalURI(FONT_SCHEMA, "", null, new String[] { fontSize > 0 ? '+'+Integer.toString(fontSize) : (fontSize < 0 ? Integer.toString(fontSize) : ""), fontStyle}, null, null);  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doSetFocus()
	 */
	@Override
	protected void doSetFocus() {
		boldButton.setFocus();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doSetValue(java.lang.Object)
	 */
	@Override
	protected void doSetValue(Object value) {
		Assert.isTrue(value == null || (value instanceof URI && FONT_SCHEMA.equals(((URI) value).scheme())));
		boldButton.setSelection(false);
		italicButton.setSelection(false);
		sizeSpinner.setSelection(0);
		if (value != null) {
			String[] segments = ((URI) value).segments();
			if (segments.length >= 2) {
				try {
					String text = segments[0];
					if (text.length() > 0 && text.charAt(0) == '+') {
						text = text.substring(1);
					}
					sizeSpinner.setSelection(Integer.parseInt(text));
				} catch (NumberFormatException ignore) {
				}
				List<String> styles = Arrays.asList(segments[1].split("\\+")); //$NON-NLS-1$
				boldButton.setSelection(styles.contains(BOLD));
				italicButton.setSelection(styles.contains(ITALIC));
			}
		}
	}

}
