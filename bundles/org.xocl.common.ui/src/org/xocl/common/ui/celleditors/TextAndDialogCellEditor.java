package org.xocl.common.ui.celleditors;

import java.text.MessageFormat;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;

/**
 * A CellEditor that is a blending of DialogCellEditor and TextCellEditor. The user can either type
 * directly into the Text or use the button to open a Dialog for editing the cell's value.
 * 
 */
public abstract class TextAndDialogCellEditor extends TextCellEditor {
	/**
	 * Image registry key for three dot image (value <code>"cell_editor_dots_button_image"</code>).
	 */
	public static final String CELL_EDITOR_IMG_DOTS_BUTTON = "cell_editor_dots_button_image";//$NON-NLS-1$

	/**
	 * Default DialogCellEditor style
	 */
	private static final int defaultStyle = SWT.SINGLE;

	/**
	 * The editor control.
	 */
	private Composite myEditorComposite;

	/**
	 * The current contents.
	 */
	private Control myContentsControl;

	/**
	 * The button.
	 */
	private Button myButton;

	/**
	 * Listens for 'focusLost' events and  fires the 'apply' event as long
	 * as the focus wasn't lost because the dialog was opened.
	 */
	private FocusListener myButtonFocusListener;

	/**
	 * The value of this cell editor; initially <code>null</code>.
	 */
	private Object myValue = null;

	private boolean isProcessingButtonSelected = false;

	static {
		ImageRegistry reg = JFaceResources.getImageRegistry();
		reg.put(CELL_EDITOR_IMG_DOTS_BUTTON, ImageDescriptor.createFromFile(
				DialogCellEditor.class, "images/dots_button.gif"));//$NON-NLS-1$
	}

	public TextAndDialogCellEditor(Composite parent) {
		this(parent, defaultStyle);
	}

	public TextAndDialogCellEditor(Composite parent, int style) {
		this(parent, style, true);
	}

	public TextAndDialogCellEditor(Composite parent, int style, boolean isEditable) {
		super(parent, style);
		text.setEditable(isEditable);
	}

	/**
	 * Creates the button for this cell editor under the given parent control.
	 * <p>
	 * The default implementation of this framework method creates the button 
	 * display on the right hand side of the dialog cell editor. Subclasses
	 * may extend or reimplement.
	 * </p>
	 *
	 * @param parent the parent control
	 * @return the new button control
	 */
	protected Button createButton(Composite parent) {
		Button result = new Button(parent, SWT.DOWN);
		result.setText("..."); //$NON-NLS-1$
		return result;
	}

	/**
	 * Creates the controls used to show the value of this cell editor.
	 * <p>
	 * The default implementation of this framework method creates
	 * a label widget, using the same font and background color as the parent control.
	 * </p>
	 * <p>
	 * Subclasses may reimplement.  If you reimplement this method, you
	 * should also reimplement <code>updateContents</code>.
	 * </p>
	 *
	 * @param cell the control for this cell editor 
	 * @return the underlying control
	 */
	protected Control createContents(Composite cell) {
		return text;
	}
	
	@Override
	protected void focusLost() {
		if ((isProcessingButtonSelected) || (text == null) || text.isDisposed()) {
			return;
		}
		Display display = Display.getCurrent();
		if (display == null) {
			display = Display.getDefault();
		}
		Point cursorLocation = display.getCursorLocation();
		Rectangle bounds = myButton.getBounds();
		Point topLeft = myButton.getParent().toDisplay(bounds.x, bounds.y);
		Point bottomRight = myButton.getParent().toDisplay(bounds.x + bounds.width, bounds.y + bounds.height);
		boolean isCursorInsideControl = (cursorLocation.x >= topLeft.x) && (cursorLocation.x <= bottomRight.x)
			&& (cursorLocation.y >= topLeft.y) && (cursorLocation.y <= bottomRight.y);
		if (!isCursorInsideControl) {
			setValueToModel();
			super.focusLost();
		}
	}

	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 */
	protected Control createControl(Composite parent) {
		Font font = parent.getFont();
		Color bg = parent.getBackground();

		myEditorComposite = new Composite(parent, getStyle()) {
			@Override
			public boolean isFocusControl() {
				for (Control child : getChildren()) {
					if (child.isFocusControl()) {
						return true;
					}
				}
				return super.isFocusControl();
			}
		};
		myEditorComposite.setFont(font);
		myEditorComposite.setBackground(bg);
		myEditorComposite.setLayout(new DialogCellLayout());

		super.createControl(myEditorComposite);

		myContentsControl = createContents(myEditorComposite);
		updateContents(myValue);

		myButton = createButton(myEditorComposite);
		myButton.setFont(font);

		myButton.addKeyListener(new KeyAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
			 */
			public void keyReleased(KeyEvent e) {
				if (e.character == '\u001b') { // Escape
					fireCancelEditor();
				}
			}
		});

		myButton.addFocusListener(getButtonFocusListener());

		myButton.addSelectionListener(new SelectionAdapter() {
			/* (non-Javadoc)
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent event) {
				try {
					isProcessingButtonSelected = true;
					// Remove the button's focus listener since it's guaranteed
					// to lose focus when the dialog opens
					myButton.removeFocusListener(getButtonFocusListener());

					Object newValue = openDialogBox(myEditorComposite);

					// Re-add the listener once the dialog closes
					myButton.addFocusListener(getButtonFocusListener());

					if (newValue != null) {
						boolean newValidState = isCorrect(newValue);
						if (newValidState) {
							markDirty();
							doSetValue(newValue);
						} else {
							// try to insert the current value into the error message.
							setErrorMessage(MessageFormat.format(getErrorMessage(),
									new Object[] { newValue.toString() }));
						}
						fireApplyEditorValue();
					}
				} finally {
					isProcessingButtonSelected = false;
				}
			}
		});

		setValueValid(true);
		
		return myEditorComposite;
	}

	/* (non-Javadoc)
	 * 
	 * Override in order to remove the button's focus listener if the celleditor
	 * is deactivating.
	 * 
	 * @see org.eclipse.jface.viewers.CellEditor#deactivate()
	 */
	public void deactivate() {
		if (myButton != null && !myButton.isDisposed()) {
			myButton.removeFocusListener(getButtonFocusListener());
		}

		super.deactivate();
	}

	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 */
	protected Object doGetValue() {
		return myValue;
	}

	/**
	 * Return a listener for button focus.
	 * @return FocusListener
	 */
	private FocusListener getButtonFocusListener() {
		if (myButtonFocusListener == null) {
			myButtonFocusListener = new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					TextAndDialogCellEditor.this.focusLost();
				}
			};
		}

		return myButtonFocusListener;
	}

	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 */
	protected void doSetValue(Object value) {
		myValue = value;
		updateContents(value);
	}

	/**
	 * Opens a dialog box under the given parent control and returns the
	 * dialog's value when it closes, or <code>null</code> if the dialog
	 * was canceled or no selection was made in the dialog.
	 * <p>
	 * This framework method must be implemented by concrete subclasses.
	 * It is called when the user has pressed the button and the dialog
	 * box must pop up.
	 * </p>
	 *
	 * @param cellEditorWindow the parent control cell editor's window
	 *   so that a subclass can adjust the dialog box accordingly
	 * @return the selected value, or <code>null</code> if the dialog was 
	 *   canceled or no selection was made in the dialog
	 */
	protected abstract Object openDialogBox(Control cellEditorWindow);

	protected abstract Object convertTextToValue(String text);
	protected abstract String convertValueToText(Object value);

	protected void keyReleaseOccured(KeyEvent keyEvent) {
		if (keyEvent.keyCode == SWT.CR || keyEvent.keyCode == SWT.KEYPAD_CR) { // Enter key
			setValueToModel();
		}
		
		// Copied from CellEditor. Disables TextCellEditor behaviour with SWT.MULTI
		// ignoring '\r' without CTRL co-pressed.
		if (keyEvent.character == '\u001b') { // Escape character
			fireCancelEditor();
		} else if (keyEvent.character == '\r') { // Return key
			fireApplyEditorValue();
			deactivate();
		}		
	}

	protected void setValueToModel() {
		String newText = text.getText();

		boolean newValidState = isCorrect(newText);

		Object newValue = convertTextToValue(newText);
		if (newValidState) {
			markDirty();
			doSetValue(newValue);
		} else {
			// try to insert the current value into the error message.
			setErrorMessage(MessageFormat.format(getErrorMessage(), new Object[] { newText }));
		}
	}

	protected String getToolTipText() {
		return text.getText();
	}

	protected void updateContents(Object value) {
		if (text == null) {
			return;
		}

		String textString = ""; //$NON-NLS-1$
		if (value != null) {
			textString = convertValueToText(value);
		}
		text.setText(textString);
		
		String toolTipText = getToolTipText();
		if (toolTipText != null) {
			text.setToolTipText(toolTipText);
		}
	}

	protected String getDialogInitialValue() {
		Object value = getValue();
		if (value == null) {
			return null;
		} else {
			return value.toString();
		}
	}
	
	@Override
	protected boolean dependsOnExternalFocusListener() {
		return false;
	}

	/**
	 * Internal class for laying out the dialog.
	 */
	private class DialogCellLayout extends Layout {
		public void layout(Composite editor, boolean force) {
			Rectangle bounds = editor.getClientArea();
			Point size = myButton.computeSize(SWT.DEFAULT, SWT.DEFAULT, force);
			if (myContentsControl != null) {
				myContentsControl.setBounds(0, 0, bounds.width - size.x, bounds.height);
			}
			myButton.setBounds(bounds.width - size.x, 0, size.x, bounds.height);
		}

		public Point computeSize(Composite editor, int wHint, int hHint,
				boolean force) {
			if (wHint != SWT.DEFAULT && hHint != SWT.DEFAULT) {
				return new Point(wHint, hHint);
			}
			Point contentsSize = myContentsControl.computeSize(SWT.DEFAULT, SWT.DEFAULT,
					force);
			Point buttonSize = myButton.computeSize(SWT.DEFAULT, SWT.DEFAULT,
					force);
			// Just return the button width to ensure the button is not clipped
			// if the label is long.
			// The label will just use whatever extra width there is
			Point result = new Point(buttonSize.x, Math.max(contentsSize.y,
					buttonSize.y));
			return result;
		}
	}
}