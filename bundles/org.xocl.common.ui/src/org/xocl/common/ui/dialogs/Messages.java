package org.xocl.common.ui.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.common.ui.dialogs.messages"; //$NON-NLS-1$
	public static String DateTimeDialog_DateLabel;
	public static String DateTimeDialog_DefaultTitle;
	public static String DateTimeDialog_TimeLabel;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
