package org.xocl.startup;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ui.IStartup;

/**
 * Startup extension used to create and open xocl/mcore project 
 * in runtime workbench.
 * 
 */
public class XOCLStartup implements IStartup {

	@Override
	public void earlyStartup() {
		refreshWorkspace();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		File root = workspace.getRoot().getLocation().toFile();
		// List all folders in the runtime workbench, those correspond 
		// to the xocl/mcore project created in an mcore model
		File[] children = root.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return !(pathname.isHidden() || pathname.getName().startsWith("."));
			}
		});

		for (int i = 0; i < children.length; i++) {
			File child = children[i];

			if (child.isDirectory()) {
				IProject project = workspace.getRoot().getProject(child.getName());

				if (!project.exists()) {
					IProjectDescription description = workspace.newProjectDescription(project.getName());
					try {
						project.create(description, null);
					} catch (CoreException e) {
						e.printStackTrace();
					}
					if (!project.isOpen())
						try {
							project.open(null);
						} catch (CoreException e) {
							e.printStackTrace();
						}
				}
			}
		}
	}

	private void refreshWorkspace() {
		try {
			ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

}
