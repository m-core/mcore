/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.classifiers.AEnumeration;

import com.montages.acore.values.ALiteral;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MLiteralValueExpr;

import com.montages.mrules.expressions.impl.MLiteralValueExprImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MLiteralValueExpr} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MLiteralValueExprItemProvider extends MChainOrApplicationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralValueExprItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAEnumerationTypePropertyDescriptor(object);
			addAValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AEnumeration Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAEnumerationTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AEnumeration Type feature.
		 * The list of possible choices is constraint by OCL trg.aActiveEnumeration 
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MLiteralValueExpr_aEnumerationType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MLiteralValueExpr_aEnumerationType_feature", "_UI_MLiteralValueExpr_type"),
						ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR__AENUMERATION_TYPE, true, false, true, null,
						null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<AEnumeration> result = new ArrayList<AEnumeration>();
						Collection<? extends AEnumeration> superResult = (Collection<? extends AEnumeration>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						for (Iterator<AEnumeration> iterator = result.iterator(); iterator.hasNext();) {
							AEnumeration trg = iterator.next();
							if (trg == null) {
								continue;
							}
							if (!((MLiteralValueExprImpl) object).evalAEnumerationTypeChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the AValue feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AValue feature.
		 * The list of possible choices is constructed by OCL getAllowedLiterals()
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MLiteralValueExpr_aValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MLiteralValueExpr_aValue_feature",
								"_UI_MLiteralValueExpr_type"),
						ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR__AVALUE, true, false, false, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<ALiteral> result = new ArrayList<ALiteral>();
						Collection<? extends ALiteral> superResult = (Collection<? extends ALiteral>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((MLiteralValueExprImpl) object).evalAValueChoiceConstruction(result);

						if (!result.contains(null)) {
							result.add(null);
						}

						return result;
					}
				});
	}

	/**
	 * This returns MLiteralValueExpr.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MLiteralValueExpr"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MLiteralValueExpr) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
