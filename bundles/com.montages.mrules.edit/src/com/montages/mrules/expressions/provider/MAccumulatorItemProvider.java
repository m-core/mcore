/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.mrules.expressions.ExpressionsFactory;
import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAccumulator;

import com.montages.mrules.expressions.impl.MAccumulatorImpl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MAccumulator} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAccumulatorItemProvider extends MCollectionVarItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulatorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSimpleTypePropertyDescriptor(object);
			addATypePropertyDescriptor(object);
			addMandatoryPropertyDescriptor(object);
			addSingularPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAccumulator_simpleType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAccumulator_simpleType_feature",
						"_UI_MAccumulator_type"),
				ExpressionsPackage.Literals.MACCUMULATOR__SIMPLE_TYPE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AType feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AType feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAccumulator_aType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAccumulator_aType_feature",
								"_UI_MAccumulator_type"),
						ExpressionsPackage.Literals.MACCUMULATOR__ATYPE, true, false, true, null,
						getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAccumulator_mandatory_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAccumulator_mandatory_feature",
						"_UI_MAccumulator_type"),
				ExpressionsPackage.Literals.MACCUMULATOR__MANDATORY, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAccumulator_singular_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAccumulator_singular_feature",
						"_UI_MAccumulator_type"),
				ExpressionsPackage.Literals.MACCUMULATOR__SINGULAR, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MAccumulator.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MAccumulator"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MAccumulatorImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAccumulator.class)) {
		case ExpressionsPackage.MACCUMULATOR__SIMPLE_TYPE:
		case ExpressionsPackage.MACCUMULATOR__MANDATORY:
		case ExpressionsPackage.MACCUMULATOR__SINGULAR:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case ExpressionsPackage.MACCUMULATOR__ACC_DEFINITION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
				ExpressionsFactory.eINSTANCE.createMChain()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
				ExpressionsFactory.eINSTANCE.createMDataValueExpr()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
				ExpressionsFactory.eINSTANCE.createMLiteralValueExpr()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
				ExpressionsFactory.eINSTANCE.createMApplication()));
	}

}
