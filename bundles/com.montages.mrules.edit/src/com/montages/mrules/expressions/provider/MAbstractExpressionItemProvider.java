/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractExpression;

import com.montages.mrules.provider.MRulesElementItemProvider;
import com.montages.mrules.provider.MrulesEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MAbstractExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractExpressionItemProvider extends MRulesElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addALabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAKindBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addARenderedKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingComponentPropertyDescriptor(object);
			}
			addATPackageUriPropertyDescriptor(object);
			addATClassifierNamePropertyDescriptor(object);
			addATFeatureNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addATPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATCoreAStringClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASingularPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATypeLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedTypeConstantPropertyDescriptor(object);
			}
			addASimpleTypePropertyDescriptor(object);
			addAsCodePropertyDescriptor(object);
			addAsBasicCodePropertyDescriptor(object);
			addCollectorPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addEntireScopePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSelfPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeTrgPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeObjPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSimpleTypeConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeLiteralConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeObjectReferenceConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeVariablesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeIteratorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeAccumulatorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeParametersPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeContainerBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeNumberBasePropertyDescriptor(object);
			}
			addScopeSourcePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addLocalEntireScopePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSelfPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeTrgPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeObjPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSourcePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSimpleTypeConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeLiteralConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeObjectReferenceConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeVariablesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeIteratorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeAccumulatorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeParametersPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeNumberBaseDefinitionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeContainerPropertyDescriptor(object);
			}
			addContainingAnnotationPropertyDescriptor(object);
			addContainingExpressionPropertyDescriptor(object);
			addIsComplexExpressionPropertyDescriptor(object);
			addIsSubExpressionPropertyDescriptor(object);
			addACalculatedOwnTypePropertyDescriptor(object);
			addCalculatedOwnMandatoryPropertyDescriptor(object);
			addCalculatedOwnSingularPropertyDescriptor(object);
			addACalculatedOwnSimpleTypePropertyDescriptor(object);
			addASelfObjectTypePropertyDescriptor(object);
			addASelfObjectPackagePropertyDescriptor(object);
			addATargetObjectTypePropertyDescriptor(object);
			addATargetSimpleTypePropertyDescriptor(object);
			addAObjectObjectTypePropertyDescriptor(object);
			addAExpectedReturnTypePropertyDescriptor(object);
			addAExpectedReturnSimpleTypePropertyDescriptor(object);
			addIsReturnValueMandatoryPropertyDescriptor(object);
			addIsReturnValueSingularPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addASrcObjectTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASrcObjectSimpleTypePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ALabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addALabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALabel feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aLabel_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ALABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AKind Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAKindBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKind Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aKindBase_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aKindBase_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AKIND_BASE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ARendered Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addARenderedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARendered Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aRenderedKind_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aRenderedKind_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Component feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Component feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aContainingComponent_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aContainingComponent_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Package Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATPackageUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Package Uri feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aTPackageUri_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTPackageUri_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE_URI, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AT Classifier Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATClassifierNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Classifier Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTClassifierName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTClassifierName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AT Feature Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATFeatureNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Feature Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTFeatureName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTFeatureName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_FEATURE_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AT Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTPackage_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Classifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTClassifier_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Feature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTFeature_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_FEATURE, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Core AString Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATCoreAStringClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Core AString Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTCoreAStringClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTCoreAStringClass_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CORE_ASTRING_CLASS, false, false, false, null,
						getString("_UI_zACorePackagePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AClassifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AClassifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aClassifier_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ACLASSIFIER, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AMandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AMandatory feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aMandatory_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aMandatory_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__AMANDATORY, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASingular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASingular feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aSingular_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aSingular_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ASINGULAR, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AType Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATypeLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AType Label feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aTypeLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aTypeLabel_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ATYPE_LABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Type Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedTypeConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Type Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ATyped_aUndefinedTypeConstant_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aUndefinedTypeConstant_feature",
						"_UI_ATyped_type"),
				AbstractionsPackage.Literals.ATYPED__AUNDEFINED_TYPE_CONSTANT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASimple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASimple Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aSimpleType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aSimpleType_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ASIMPLE_TYPE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_asCode_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_asCode_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the As Basic Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsBasicCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Basic Code feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_asBasicCode_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_asBasicCode_feature",
								"_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Collector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Collector feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_collector_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_collector_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR, false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Entire Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEntireScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Entire Scope feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_entireScope_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_entireScope_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ENTIRE_SCOPE, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeBase_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_BASE, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Self feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSelfPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Self feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSelf_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeSelf_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SELF, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Trg feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeTrgPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Trg feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeTrg_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeTrg_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_TRG, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Obj feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeObjPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Obj feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeObj_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeObj_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJ, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Simple Type Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSimpleTypeConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Simple Type Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSimpleTypeConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSimpleTypeConstants_feature", "_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS, false, false, false,
				null, getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Literal Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeLiteralConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Literal Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeLiteralConstants_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeLiteralConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Object Reference Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeObjectReferenceConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Object Reference Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeObjectReferenceConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeObjectReferenceConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS, false, false, false,
				null, getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeVariablesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Variables feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeVariables_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeVariables_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_VARIABLES, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Iterator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeIteratorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Iterator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeIterator_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeIterator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ITERATOR, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Accumulator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeAccumulatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Accumulator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeAccumulator_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeAccumulator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeParametersPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Parameters feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeParameters_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeParameters_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Container Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeContainerBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Container Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeContainerBase_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeContainerBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Number Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeNumberBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Number Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeNumberBase_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeNumberBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Source feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSource_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_scopeSource_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SOURCE, false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Local Entire Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalEntireScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Entire Scope feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localEntireScope_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localEntireScope_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE, false, false, false, null,
						getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeBase_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeBase_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE, false, false, false, null,
						getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Self feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSelfPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Self feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeSelf_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeSelf_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF, false, false, false, null,
						getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Trg feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeTrgPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Trg feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeTrg_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_localScopeTrg_feature",
								"_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG, false, false, false, null,
						getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Obj feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeObjPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Obj feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeObj_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_localScopeObj_feature",
								"_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ, false, false, false, null,
						getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Source feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeSource_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeSource_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE, false, false, false, null,
						getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Simple Type Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSimpleTypeConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Simple Type Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeSimpleTypeConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSimpleTypeConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS, false, false,
				false, null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Literal Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeLiteralConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Literal Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeLiteralConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeLiteralConstants_feature", "_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS, false, false, false,
				null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Object Reference Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeObjectReferenceConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Object Reference Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeObjectReferenceConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeObjectReferenceConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS, false, false,
				false, null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeVariablesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Variables feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeVariables_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeVariables_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES, false, false, false,
						null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Iterator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeIteratorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Iterator feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeIterator_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeIterator_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR, false, false, false,
						null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Accumulator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeAccumulatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Accumulator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeAccumulator_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_localScopeAccumulator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR, false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeParametersPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Parameters feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeParameters_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeParameters_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS, false, false, false,
						null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Number Base Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeNumberBaseDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Number Base Definition feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeNumberBaseDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeNumberBaseDefinition_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION, false, false,
				false, null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeContainerPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Container feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_localScopeContainer_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_localScopeContainer_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER, false, false, false,
						null, getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Annotation feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_containingAnnotation_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_containingAnnotation_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION, false, false, false,
						null, getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Containing Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Expression feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_containingExpression_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_containingExpression_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION, false, false, false,
						null, getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Complex Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsComplexExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Complex Expression feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_isComplexExpression_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_isComplexExpression_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Sub Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsSubExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Sub Expression feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_isSubExpression_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_isSubExpression_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ACalculated Own Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addACalculatedOwnTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ACalculated Own Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aCalculatedOwnType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aCalculatedOwnType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE, false, false, false,
						null, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_calculatedOwnMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnMandatory_feature", "_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_calculatedOwnSingular_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_calculatedOwnSingular_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ACalculated Own Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addACalculatedOwnSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ACalculated Own Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_aCalculatedOwnSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_aCalculatedOwnSimpleType_feature", "_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ASelf Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASelfObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASelf Object Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aSelfObjectType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aSelfObjectType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASELF_OBJECT_TYPE, false, false, false, null,
						getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ASelf Object Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASelfObjectPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASelf Object Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aSelfObjectPackage_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aSelfObjectPackage_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASELF_OBJECT_PACKAGE, false, false, false,
						null, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ATarget Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATargetObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ATarget Object Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aTargetObjectType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aTargetObjectType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ATARGET_OBJECT_TYPE, false, false, false,
						null, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ATarget Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATargetSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ATarget Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_aTargetSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_aTargetSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ATARGET_SIMPLE_TYPE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AObject Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAObjectObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AObject Object Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aObjectObjectType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aObjectObjectType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AOBJECT_OBJECT_TYPE, false, false, false,
						null, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AExpected Return Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAExpectedReturnTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AExpected Return Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aExpectedReturnType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aExpectedReturnType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_TYPE, false, false, false,
						null, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AExpected Return Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAExpectedReturnSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AExpected Return Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_aExpectedReturnSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_aExpectedReturnSimpleType_feature", "_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_SIMPLE_TYPE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_isReturnValueMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isReturnValueMandatory_feature", "_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpression_isReturnValueSingular_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpression_isReturnValueSingular_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ASrc Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASrcObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASrc Object Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aSrcObjectType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aSrcObjectType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASRC_OBJECT_TYPE, false, false, false, null,
						getString("_UI_TypeInformationPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASrc Object Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASrcObjectSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASrc Object Simple Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpression_aSrcObjectSimpleType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpression_aSrcObjectSimpleType_feature", "_UI_MAbstractExpression_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASRC_OBJECT_SIMPLE_TYPE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypeInformationPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MAbstractExpression) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAbstractExpression.class)) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ALABEL:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AKIND_BASE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ARENDERED_KIND:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AMANDATORY:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASINGULAR:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATYPE_LABEL:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AUNDEFINED_TYPE_CONSTANT:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATARGET_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASRC_OBJECT_SIMPLE_TYPE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MrulesEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ExpressionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
