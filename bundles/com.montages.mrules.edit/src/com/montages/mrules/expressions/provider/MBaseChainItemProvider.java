/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AProperty;

import com.montages.mrules.expressions.ExpressionsFactory;
import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MBaseChain;

import com.montages.mrules.expressions.impl.MBaseChainImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MBaseChain} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MBaseChainItemProvider extends MAbstractExpressionWithBaseItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MBaseChainItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAChainEntryTypePropertyDescriptor(object);
			addChainAsCodePropertyDescriptor(object);
			addAElement1PropertyDescriptor(object);
			addElement1CorrectPropertyDescriptor(object);
			addAElement2EntryTypePropertyDescriptor(object);
			addAElement2PropertyDescriptor(object);
			addElement2CorrectPropertyDescriptor(object);
			addAElement3EntryTypePropertyDescriptor(object);
			addAElement3PropertyDescriptor(object);
			addElement3CorrectPropertyDescriptor(object);
			addACastTypePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addALastElementPropertyDescriptor(object);
			}
			addAChainCalculatedTypePropertyDescriptor(object);
			addAChainCalculatedSimpleTypePropertyDescriptor(object);
			addChainCalculatedSingularPropertyDescriptor(object);
			addProcessorPropertyDescriptor(object);
			addProcessorDefinitionPropertyDescriptor(object);
			addTypeMismatchPropertyDescriptor(object);
			addChainCodeforSubchainsPropertyDescriptor(object);
			addIsOwnXOCLOpPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AChain Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAChainEntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AChain Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_aChainEntryType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aChainEntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_ENTRY_TYPE, false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain As Code feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_chainAsCode_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_chainAsCode_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the AElement1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAElement1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AElement1 feature.
		 * The list of possible choices is constructed by OCL 
		let annotatedProp: acore::classifiers::AProperty = 
		self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)
		in
		if self.aChainEntryType.oclIsUndefined() then 
		OrderedSet{} else
		self.aChainEntryType.aAllProperty()
		endif
		
		--
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aElement1_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aElement1_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT1, true, false, false, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<AProperty> result = new ArrayList<AProperty>();
						Collection<? extends AProperty> superResult = (Collection<? extends AProperty>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((MBaseChainImpl) object).evalAElement1ChoiceConstruction(result);

						if (!result.contains(null)) {
							result.add(null);
						}

						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Element1 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement1CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element1 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_element1Correct_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_element1Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1_CORRECT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the AElement2 Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAElement2EntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AElement2 Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_aElement2EntryType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aElement2EntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT2_ENTRY_TYPE, false, false, false, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the AElement2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAElement2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AElement2 feature.
		 * The list of possible choices is constructed by OCL let annotatedProp: acore::classifiers::AProperty = 
		self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)
		in
		
		if aElement1.oclIsUndefined() 
		then OrderedSet{}
		else if not(aElement1.aOperation->isEmpty())
		then OrderedSet{} 
		else if aElement2EntryType.oclIsUndefined() 
		  then OrderedSet{}
		  else aElement2EntryType.aAllProperty() endif
		 endif
		endif
		
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aElement2_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aElement2_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT2, true, false, false, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<AProperty> result = new ArrayList<AProperty>();
						Collection<? extends AProperty> superResult = (Collection<? extends AProperty>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((MBaseChainImpl) object).evalAElement2ChoiceConstruction(result);

						if (!result.contains(null)) {
							result.add(null);
						}

						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Element2 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_element2Correct_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_element2Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_CORRECT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the AElement3 Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAElement3EntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AElement3 Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_aElement3EntryType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aElement3EntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT3_ENTRY_TYPE, false, false, false, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the AElement3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAElement3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AElement3 feature.
		 * The list of possible choices is constructed by OCL let annotatedProp: acore::classifiers::AProperty = 
		self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty )
		in
		
		if aElement2.oclIsUndefined() 
		then OrderedSet{}
		else if not aElement2.aOperation->isEmpty()
		then OrderedSet{} 
		else if aElement3EntryType.oclIsUndefined() 
		  then OrderedSet{}
		  else aElement3EntryType.aAllProperty() endif
		 endif
		endif
		
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aElement3_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aElement3_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT3, true, false, false, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<AProperty> result = new ArrayList<AProperty>();
						Collection<? extends AProperty> superResult = (Collection<? extends AProperty>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((MBaseChainImpl) object).evalAElement3ChoiceConstruction(result);

						if (!result.contains(null)) {
							result.add(null);
						}

						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Element3 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_element3Correct_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_element3Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_CORRECT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the ACast Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addACastTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ACast Type feature.
		 * The list of possible choices is constraint by OCL trg.aActiveClass
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aCastType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aCastType_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACAST_TYPE, true, false, true, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<AClassifier> result = new ArrayList<AClassifier>();
						Collection<? extends AClassifier> superResult = (Collection<? extends AClassifier>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						for (Iterator<AClassifier> iterator = result.iterator(); iterator.hasNext();) {
							AClassifier trg = iterator.next();
							if (trg == null) {
								continue;
							}
							if (!((MBaseChainImpl) object).evalACastTypeChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the ALast Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addALastElementPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALast Element feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aLastElement_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aLastElement_feature",
								"_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__ALAST_ELEMENT, false, false, false, null, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AChain Calculated Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAChainCalculatedTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AChain Calculated Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_aChainCalculatedType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_aChainCalculatedType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_CALCULATED_TYPE, false, false, false, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the AChain Calculated Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAChainCalculatedSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AChain Calculated Simple Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_aChainCalculatedSimpleType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractChain_aChainCalculatedSimpleType_feature", "_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Singular feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractChain_chainCalculatedSingular_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractChain_chainCalculatedSingular_feature", "_UI_MAbstractChain_type"),
						ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Processor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processor feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_processor_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_processor_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ProcessorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Processor Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessorDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processor Definition feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractChain_processorDefinition_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractChain_processorDefinition_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION, false, false, false, null,
				getString("_UI_ProcessorDefinitionPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Type Mismatch feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeMismatchPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type Mismatch feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MBaseChain_typeMismatch_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MBaseChain_typeMismatch_feature",
								"_UI_MBaseChain_type"),
						ExpressionsPackage.Literals.MBASE_CHAIN__TYPE_MISMATCH, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Codefor Subchains feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCodeforSubchainsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Codefor Subchains feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MBaseChain_chainCodeforSubchains_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MBaseChain_chainCodeforSubchains_feature",
						"_UI_MBaseChain_type"),
				ExpressionsPackage.Literals.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Is Own XOCL Op feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsOwnXOCLOpPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Own XOCL Op feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MBaseChain_isOwnXOCLOp_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MBaseChain_isOwnXOCLOp_feature",
						"_UI_MBaseChain_type"),
				ExpressionsPackage.Literals.MBASE_CHAIN__IS_OWN_XOCL_OP, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ProcessorPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ExpressionsPackage.Literals.MBASE_CHAIN__CALL_ARGUMENT);
			childrenFeatures.add(ExpressionsPackage.Literals.MBASE_CHAIN__SUB_EXPRESSION);
			childrenFeatures.add(ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MBaseChain) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MBaseChain.class)) {
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_AS_CODE:
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1_CORRECT:
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_CORRECT:
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_CORRECT:
		case ExpressionsPackage.MBASE_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE:
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SINGULAR:
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR:
		case ExpressionsPackage.MBASE_CHAIN__TYPE_MISMATCH:
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS:
		case ExpressionsPackage.MBASE_CHAIN__IS_OWN_XOCL_OP:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
		case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
		case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MBASE_CHAIN__CALL_ARGUMENT,
				ExpressionsFactory.eINSTANCE.createMCallArgument()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MBASE_CHAIN__SUB_EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMSubChain()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR,
				ExpressionsFactory.eINSTANCE.createMCollectionExpression()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ExpressionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
