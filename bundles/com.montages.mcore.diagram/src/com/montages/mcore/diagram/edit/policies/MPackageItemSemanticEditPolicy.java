package com.montages.mcore.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import com.montages.mcore.diagram.edit.commands.MClassifierCreateCommand;
import com.montages.mcore.diagram.edit.commands.MObjectCreateCommand;
import com.montages.mcore.diagram.edit.commands.MObjectWithSlotCreateCommand;
import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
* @generated
*/
public class MPackageItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public MPackageItemSemanticEditPolicy() {
		super(McoreElementTypes.MPackage_1000);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (McoreElementTypes.MClassifier_2001 == req.getElementType()) {
			return getGEFWrapper(new MClassifierCreateCommand(req));
		}
		if (McoreElementTypes.MObject_2002 == req.getElementType()) {
			return getGEFWrapper(new MObjectCreateCommand(req));
		}
		if (McoreElementTypes.MObject_2003 == req.getElementType()) {
			return getGEFWrapper(new MObjectWithSlotCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	* @generated
	*/
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	* @generated
	*/
	private static class DuplicateAnythingCommand extends DuplicateEObjectsCommand {

		/**
		* @generated
		*/
		public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain, DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req.getElementsToBeDuplicated(), req.getAllDuplicatedElementsMap());
		}

	}

}
