package com.montages.mcore.diagram.edit.commands;

import java.util.Collection;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.UndoContext;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.Transaction;
import org.eclipse.emf.workspace.EMFCommandOperation;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPackageAction;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;

/**
 * @generated
 */
public class MClassifierCreateCommand extends EditElementCommand {

	/**
	 * @added
	 */
	private UndoContext updateActionCommandContext = new UndoContext();

	/**
	* @generated
	*/
	public MClassifierCreateCommand(CreateElementRequest req) {
		super(req.getLabel(), null, req);
	}

	/**
	* FIXME: replace with setElementToEdit()
	* @generated
	*/
	protected EObject getElementToEdit() {
		EObject container = ((CreateElementRequest) getRequest()).getContainer();
		if (container instanceof View) {
			container = ((View) container).getElement();
		}
		return container;
	}

	/**
	* @generated
	*/
	public boolean canExecute() {
		return true;

	}

	/**
	* @generated NOT
	*/
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		MPackage owner = (MPackage) getElementToEdit();
		Command updateActionCommand = owner.doActionUpdate(MPackageAction.CLASS).getContainingTransition().interpreteTransitionAsCommand();
		if (updateActionCommand == null || !updateActionCommand.canExecute()) {
			return CommandResult.newErrorCommandResult("Can't execute " + MPackageAction.CLASS + " package action.");
		}

		EMFCommandOperation oper = new EMFCommandOperation(((McoreDiagramEditingDoamin) getEditingDomain()), updateActionCommand, null);

		// add the appropriate context
		oper.addContext(updateActionCommandContext);
		((McoreDiagramEditingDoamin) getEditingDomain()).getHistory().execute(oper, monitor, info);
		MClassifier newElement = getCreatedClassifierFromResult(updateActionCommand.getResult());
		if (newElement == null) {
			return CommandResult.newErrorCommandResult("Can't find a created MObject int the command result.");
		}
		((CreateElementRequest) getRequest()).setNewElement(newElement);
		return CommandResult.newOKCommandResult(newElement);
	}

	/**
	* @added
	*/
	private MClassifier getCreatedClassifierFromResult(Collection<?> result) {
		for (Object nextResult : result) {
			if (nextResult instanceof MClassifier) {
				return (MClassifier) nextResult;
			}
		}
		return null;
	}

	/**
	* @generated
	*/
	protected void doConfigure(MClassifier newElement, IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IElementType elementType = ((CreateElementRequest) getRequest()).getElementType();
		ConfigureRequest configureRequest = new ConfigureRequest(getEditingDomain(), newElement, elementType);
		configureRequest.setClientContext(((CreateElementRequest) getRequest()).getClientContext());
		configureRequest.addParameters(getRequest().getParameters());
		ICommand configureCommand = elementType.getEditCommand(configureRequest);
		if (configureCommand != null && configureCommand.canExecute()) {
			configureCommand.execute(monitor, info);
		}
	}

	@Override
	protected IStatus doUndo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IStatus result = null;
		((McoreDiagramEditingDoamin) getEditingDomain()).getHistory().undo(updateActionCommandContext, null, null);

		return result;
	}

	protected Transaction createLocalTransaction(Map<?, ?> options) throws InterruptedException {
		return ((McoreDiagramEditingDoamin) getEditingDomain()).startTransaction(false, options);
	}

	@Override
	protected IStatus doRedo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		((McoreDiagramEditingDoamin) getEditingDomain()).getHistory().redo(updateActionCommandContext, monitor, info);
		return null;
	}
}
