package com.montages.mcore.diagram.edit.policies;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class ClassifierDescriptionCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ClassifierDescriptionCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MClassifier_2001);
	}

}
