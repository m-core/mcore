package com.montages.mcore.diagram.edit.parts;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.eclipse.gmf.tooling.runtime.directedit.locator.TextCellEditorLocator;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;

import com.montages.mcore.diagram.part.McoreVisualIDRegistry;

/**
 * @generated
 */
public class McoreEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (McoreVisualIDRegistry.getVisualID(view)) {

			case MPackageEditPart.VISUAL_ID:
				return new MPackageEditPart(view);

			case MClassifierEditPart.VISUAL_ID:
				return new MClassifierEditPart(view);

			case MClassifierENameEditPart.VISUAL_ID:
				return new MClassifierENameEditPart(view);

			case MClassifierNameEditPart.VISUAL_ID:
				return new MClassifierNameEditPart(view);

			case MClassifierShortNameEditPart.VISUAL_ID:
				return new MClassifierShortNameEditPart(view);

			case MClassifierDescriptionLabelEditPart.VISUAL_ID:
				return new MClassifierDescriptionLabelEditPart(view);

			case MClassifierDoActionLabelEditPart.VISUAL_ID:
				return new MClassifierDoActionLabelEditPart(view);

			case MObjectEditPart.VISUAL_ID:
				return new MObjectEditPart(view);

			case MObjectIdEditPart.VISUAL_ID:
				return new MObjectIdEditPart(view);

			case MObjectTypeEditPart.VISUAL_ID:
				return new MObjectTypeEditPart(view);

			case MObjectDescriptionLabelEditPart.VISUAL_ID:
				return new MObjectDescriptionLabelEditPart(view);

			case MObjectDoActionLabelEditPart.VISUAL_ID:
				return new MObjectDoActionLabelEditPart(view);

			case MObjectWithSlotEditPart.VISUAL_ID:
				return new MObjectWithSlotEditPart(view);

			case MObjectWithSlotIdEditPart.VISUAL_ID:
				return new MObjectWithSlotIdEditPart(view);

			case MObjectTypeWithSlotEditPart.VISUAL_ID:
				return new MObjectTypeWithSlotEditPart(view);

			case MObjectWithSlotDescriptionLabelEditPart.VISUAL_ID:
				return new MObjectWithSlotDescriptionLabelEditPart(view);

			case MObjectWithSlotDoActionLabelEditPart.VISUAL_ID:
				return new MObjectWithSlotDoActionLabelEditPart(view);

			case MObjectSlotEditPart.VISUAL_ID:
				return new MObjectSlotEditPart(view);

			case MClassifierPropertyEditPart.VISUAL_ID:
				return new MClassifierPropertyEditPart(view);

			case MClassifierLiteralEditPart.VISUAL_ID:
				return new MClassifierLiteralEditPart(view);

			case MObjectSlotCompartmentEditPart.VISUAL_ID:
				return new MObjectSlotCompartmentEditPart(view);

			case InstanceDescriptionCompartmentEditPart.VISUAL_ID:
				return new InstanceDescriptionCompartmentEditPart(view);

			case InstanceDerivationsCompartmentEditPart.VISUAL_ID:
				return new InstanceDerivationsCompartmentEditPart(view);

			case MClassiferPropertiesCompartmentEditPart.VISUAL_ID:
				return new MClassiferPropertiesCompartmentEditPart(view);

			case ClassifierDescriptionCompartmentEditPart.VISUAL_ID:
				return new ClassifierDescriptionCompartmentEditPart(view);

			case MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID:
				return new MObjectWithSlotSlotCompartmentEditPart(view);

			case MObjectWithSlotInstanceDescriptionCompartmentEditPart.VISUAL_ID:
				return new MObjectWithSlotInstanceDescriptionCompartmentEditPart(view);

			case MObjectWithSlotInstanceDerivationsCompartmentEditPart.VISUAL_ID:
				return new MObjectWithSlotInstanceDerivationsCompartmentEditPart(view);

			case MObjectType2EditPart.VISUAL_ID:
				return new MObjectType2EditPart(view);

			case MClassifierSuperTypeEditPart.VISUAL_ID:
				return new MClassifierSuperTypeEditPart(view);

			case MPropertyEditPart.VISUAL_ID:
				return new MPropertyEditPart(view);

			case MPropertyNameEditPart.VISUAL_ID:
				return new MPropertyNameEditPart(view);

			case MPropertyMultiplicityAsStringSinEditPart.VISUAL_ID:
				return new MPropertyMultiplicityAsStringSinEditPart(view);

			case MPropertyInstanceEditPart.VISUAL_ID:
				return new MPropertyInstanceEditPart(view);

			case MPropertyInstancePropertyEditPart.VISUAL_ID:
				return new MPropertyInstancePropertyEditPart(view);

			case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
				return new MPropertyInstanceContainmentsLinkEditPart(view);

			case MPropertyInstanceContainmentLinkLabelEditPart.VISUAL_ID:
				return new MPropertyInstanceContainmentLinkLabelEditPart(view);

			case MPropertyContainmentEditPart.VISUAL_ID:
				return new MPropertyContainmentEditPart(view);

			case MPropertyContainmentNameEditPart.VISUAL_ID:
				return new MPropertyContainmentNameEditPart(view);

			case MPropertyContainmentMultiplicityAsStringSinEditPart.VISUAL_ID:
				return new MPropertyContainmentMultiplicityAsStringSinEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated NOT
	 */
	public static CellEditorLocator getTextCellEditorLocator(ITextAwareEditPart source) {
		if (source instanceof MClassifierDescriptionLabelEditPart) {
			return new TextCellEditorLocator((WrappingLabel)source.getFigure()) {

				public void relocate(CellEditor celleditor) {
					Control text = celleditor.getControl();
					Rectangle rect = getWrapLabel().getTextBounds().getCopy();
					getWrapLabel().translateToAbsolute(rect);
					if (getWrapLabel().isTextWrapOn()) {
						rect.setSize(new Dimension(text.computeSize(rect.width, SWT.DEFAULT)));
						Dimension minSize = getWrapLabel().getMinimumSize(0, 0);
						rect.setWidth(Math.max(minSize.width, rect.width));
					}
					if (!rect.equals(new Rectangle(text.getBounds()))) {
						text.setBounds(rect.x, rect.y, rect.width, rect.height);
					}
				}
			};
		}
		return CellEditorLocatorAccess.INSTANCE.getTextCellEditorLocator(source);
	}
}
