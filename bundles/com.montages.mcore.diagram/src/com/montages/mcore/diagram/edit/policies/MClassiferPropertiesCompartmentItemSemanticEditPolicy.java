package com.montages.mcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import com.montages.mcore.diagram.edit.commands.MClassifierLiteralCreateCommand;
import com.montages.mcore.diagram.edit.commands.MClassifierPropertyCreateCommand;
import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MClassiferPropertiesCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MClassiferPropertiesCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MClassifier_2001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (McoreElementTypes.MProperty_3004 == req.getElementType()) {
			return getGEFWrapper(new MClassifierPropertyCreateCommand(req));
		}
		if (McoreElementTypes.MLiteral_3005 == req.getElementType()) {
			return getGEFWrapper(new MClassifierLiteralCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
