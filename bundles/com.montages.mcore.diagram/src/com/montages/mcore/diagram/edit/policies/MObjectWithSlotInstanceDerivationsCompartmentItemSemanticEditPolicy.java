package com.montages.mcore.diagram.edit.policies;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MObjectWithSlotInstanceDerivationsCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MObjectWithSlotInstanceDerivationsCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MObject_2003);
	}

}
