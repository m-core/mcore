package com.montages.mcore.diagram.edit.policies;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class InstanceDescriptionCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public InstanceDescriptionCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MObject_2002);
	}

}
