package com.montages.mcore.diagram.edit.parts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.diagram.edit.policies.MClassifierSuperTypeItemSemanticEditPolicy;

/**
 * @generated
 */
public class MClassifierSuperTypeEditPart extends ConnectionNodeEditPart implements ITreeBranchEditPart {

	/**
	* @generated
	*/
	public static final int VISUAL_ID = 4002;

	/**
	* @generated
	*/
	public MClassifierSuperTypeEditPart(View view) {
		super(view);
	}

	/**
	* @generated
	*/
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new MClassifierSuperTypeItemSemanticEditPolicy());
	}

	/**
	* Creates figure for this edit part.
	* 
	* Body of this method does not depend on settings in generation model
	* so you may safely remove <i>generated</i> tag and modify it.
	* 
	* @generated
	*/

	protected Connection createConnectionFigure() {
		return new SolidLineWithClosedArrow();
	}

	/**
	* @generated
	*/
	public SolidLineWithClosedArrow getPrimaryShape() {
		return (SolidLineWithClosedArrow) getFigure();
	}

	/**
	* @generated
	*/
	public class SolidLineWithClosedArrow extends PolylineConnectionEx {

		/**
		 * @generated
		 */
		public SolidLineWithClosedArrow() {

			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolygonDecoration df = new PolygonDecoration();
			df.setFill(true);
			df.setBackgroundColor(ColorConstants.white);
			PointList pl = new PointList();
			pl.addPoint(0, 0);
			pl.addPoint(-2, 2);
			pl.addPoint(-2, -2);
			pl.addPoint(0, 0);
			df.setTemplate(pl);
			df.setScale(7, 3);
			return df;
		}

	}

}
