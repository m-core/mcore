package com.montages.mcore.diagram.edit.parts;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.OneLineBorder;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.edit.policies.reparent.CreationEditPolicyWithCustomReparent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import com.montages.mcore.diagram.draw2d.figures.HTMLWrappingLabel;
import com.montages.mcore.diagram.edit.policies.MClassifierItemSemanticEditPolicy;
import com.montages.mcore.diagram.edit.policies.ShowHideCompartmentEditPolicy;
import com.montages.mcore.diagram.edit.policies.ShowHideRelatedElementsEditPolicy;
import com.montages.mcore.diagram.layout.FlexCompartmentToolbarLayout;
import com.montages.mcore.diagram.layout.SubCompartmentLayoutManager;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MClassifierEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public MClassifierEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE, new CreationEditPolicyWithCustomReparent(McoreVisualIDRegistry.TYPED_INSTANCE));
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new MClassifierItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		installEditPolicy(ShowHideCompartmentEditPolicy.SHOW_HIDE_COMPARTMENTS_POLICY, new ShowHideCompartmentEditPolicy());
		installEditPolicy(ShowHideRelatedElementsEditPolicy.SHOW_HIDE_RELATED_ELEMENTS_POLICY, new ShowHideRelatedElementsEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new ClassifierFigure();
	}

	/**
	 * @generated
	 */
	public ClassifierFigure getPrimaryShape() {
		return (ClassifierFigure) primaryShape;
	}

	/**
	* @generated NOT
	*/
	protected boolean addFixedChild(EditPart childEditPart) {
		boolean result = addFixedChildGen(childEditPart);
		if (childEditPart instanceof ClassifierDescriptionCompartmentEditPart) {
			// add fixed description label to the compartment view port
			IFigure compartmentFigure = ((ClassifierDescriptionCompartmentEditPart) childEditPart).getFigure();
			IFigure children = (IFigure) compartmentFigure.getChildren().get(1);
			IFigure viewPort = (IFigure) children.getChildren().get(0);
			IFigure viewPortChildren = (IFigure) viewPort.getChildren().get(0);
			viewPortChildren.add(getPrimaryShape().getClassifierFigure_Description());
			return true;
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChildGen(EditPart childEditPart) {
		if (childEditPart instanceof MClassifierENameEditPart) {
			((MClassifierENameEditPart) childEditPart).setLabel(getPrimaryShape().getFigureClassifierFigure_ename());
			return true;
		}
		if (childEditPart instanceof MClassifierNameEditPart) {
			((MClassifierNameEditPart) childEditPart).setLabel(getPrimaryShape().getFigureClassifierFigure_name());
			return true;
		}
		if (childEditPart instanceof MClassifierShortNameEditPart) {
			((MClassifierShortNameEditPart) childEditPart).setLabel(getPrimaryShape().getFigureClassifierFigure_shortName());
			return true;
		}
		if (childEditPart instanceof MClassifierDescriptionLabelEditPart) {
			((MClassifierDescriptionLabelEditPart) childEditPart).setLabel(getPrimaryShape().getClassifierFigure_Description());
			return true;
		}
		if (childEditPart instanceof MClassifierDoActionLabelEditPart) {
			((MClassifierDoActionLabelEditPart) childEditPart).setLabel(getPrimaryShape().getDoActionClassifierLabel());
			return true;
		}
		if (childEditPart instanceof ClassifierDescriptionCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getDescriptionCompartmentFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((ClassifierDescriptionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof MClassiferPropertiesCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getPropertiesCompartmentFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((MClassiferPropertiesCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof MClassifierENameEditPart) {
			return true;
		}
		if (childEditPart instanceof MClassifierNameEditPart) {
			return true;
		}
		if (childEditPart instanceof MClassifierShortNameEditPart) {
			return true;
		}
		if (childEditPart instanceof MClassifierDescriptionLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof MClassifierDoActionLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof ClassifierDescriptionCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getDescriptionCompartmentFigure();
			pane.remove(((ClassifierDescriptionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof MClassiferPropertiesCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getPropertiesCompartmentFigure();
			pane.remove(((MClassiferPropertiesCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof ClassifierDescriptionCompartmentEditPart) {
			return getPrimaryShape().getDescriptionCompartmentFigure();
		}
		if (editPart instanceof MClassiferPropertiesCompartmentEditPart) {
			return getPrimaryShape().getPropertiesCompartmentFigure();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(180, 70);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(McoreVisualIDRegistry.getType(MClassifierENameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request) {
		if (request instanceof CreateViewAndElementRequest) {
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request).getViewAndElementDescriptor().getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter.getAdapter(IElementType.class);
			if (type == McoreElementTypes.MProperty_3004) {
				return getChildBySemanticHint(McoreVisualIDRegistry.getType(MClassiferPropertiesCompartmentEditPart.VISUAL_ID));
			}
			if (type == McoreElementTypes.MLiteral_3005) {
				return getChildBySemanticHint(McoreVisualIDRegistry.getType(MClassiferPropertiesCompartmentEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	protected void handleNotificationEvent(Notification event) {
		if (event.getNotifier() == getModel() && EcorePackage.eINSTANCE.getEModelElement_EAnnotations().equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
	}

	/**
	 * @generated
	 */
	public class ClassifierFigure extends RoundedRectangle {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureClassifierFigure_shortName;

		/**
		 * @generated
		 */
		private WrappingLabel myClassifierFigure_ename;

		/**
		 * @generated
		 */
		private RectangleFigure fPropertiesCompartmentFigure;

		/**
		* @generated
		*/
		private RectangleFigure fDescriptionCompartmentFigure;

		/**
		   * @generated
		   */
		private WrappingLabel fClassifierFigure_Description;

		/**
		* @generated
		*/
		private WrappingLabel fDoActionClassifierLabel;

		/**
				 * @generated
				 */
		private WrappingLabel myClassifierFigure_name;

		/**
		 * @generated
		 */
		public ClassifierFigure() {

			ToolbarLayout layoutThis = new ToolbarLayout();
			layoutThis.setStretchMinorAxis(true);
			layoutThis.setMinorAlignment(ToolbarLayout.ALIGN_TOPLEFT);

			layoutThis.setSpacing(0);
			layoutThis.setVertical(true);

			this.setLayoutManager(layoutThis);

			this.setCornerDimensions(new Dimension(8, 8));
			this.setForegroundColor(THIS_FORE);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContentsGen() {

			RectangleFigure classifierFigure_NamesBlock0 = new RectangleFigure();

			classifierFigure_NamesBlock0.setFill(false);
			classifierFigure_NamesBlock0.setOutline(false);

			this.add(classifierFigure_NamesBlock0, BorderLayout.TOP);

			BorderLayout layoutClassifierFigure_NamesBlock0 = new BorderLayout();
			classifierFigure_NamesBlock0.setLayoutManager(layoutClassifierFigure_NamesBlock0);

			RectangleFigure classifierFigure_NameAndEnameBlock1 = new RectangleFigure();

			classifierFigure_NameAndEnameBlock1.setFill(false);
			classifierFigure_NameAndEnameBlock1.setOutline(false);

			classifierFigure_NamesBlock0.add(classifierFigure_NameAndEnameBlock1, BorderLayout.TOP);

			BorderLayout layoutClassifierFigure_NameAndEnameBlock1 = new BorderLayout();
			classifierFigure_NameAndEnameBlock1.setLayoutManager(layoutClassifierFigure_NameAndEnameBlock1);

			myClassifierFigure_ename = new WrappingLabel();

			myClassifierFigure_ename.setBorder(new MarginBorder(5, 5, 5, 5));
			myClassifierFigure_ename.setAlignment(PositionConstants.CENTER);

			classifierFigure_NameAndEnameBlock1.add(myClassifierFigure_ename, BorderLayout.CENTER);

			myClassifierFigure_name = new WrappingLabel();

			myClassifierFigure_name.setFont(MYCLASSIFIERFIGURE_NAME_FONT);

			myClassifierFigure_name.setBorder(new MarginBorder(0, 9, 0, 5));
			myClassifierFigure_name.setAlignment(PositionConstants.LEFT);

			classifierFigure_NameAndEnameBlock1.add(myClassifierFigure_name);

			RectangleFigure doActionClassifierRectangle1 = new RectangleFigure();

			doActionClassifierRectangle1.setFill(false);
			doActionClassifierRectangle1.setOutline(false);

			classifierFigure_NamesBlock0.add(doActionClassifierRectangle1, BorderLayout.BOTTOM);

			BorderLayout layoutDoActionClassifierRectangle1 = new BorderLayout();
			doActionClassifierRectangle1.setLayoutManager(layoutDoActionClassifierRectangle1);

			fDoActionClassifierLabel = new WrappingLabel();

			fDoActionClassifierLabel.setText("Do...");

			fDoActionClassifierLabel.setFont(FDOACTIONCLASSIFIERLABEL_FONT);

			doActionClassifierRectangle1.add(fDoActionClassifierLabel, BorderLayout.TOP);

			RectangleFigure classifierFigure_ShortNameBlock1 = new RectangleFigure();

			classifierFigure_ShortNameBlock1.setFill(false);
			classifierFigure_ShortNameBlock1.setOutline(false);

			classifierFigure_NamesBlock0.add(classifierFigure_ShortNameBlock1);

			BorderLayout layoutClassifierFigure_ShortNameBlock1 = new BorderLayout();
			classifierFigure_ShortNameBlock1.setLayoutManager(layoutClassifierFigure_ShortNameBlock1);

			WrappingLabel classifierFigure_shortName_padding2 = new WrappingLabel();

			classifierFigure_shortName_padding2.setText("short name:");
			classifierFigure_shortName_padding2.setForegroundColor(ColorConstants.black);

			classifierFigure_shortName_padding2.setFont(CLASSIFIERFIGURE_SHORTNAME_PADDING2_FONT);

			classifierFigure_shortName_padding2.setBorder(new MarginBorder(3, 9, 0, 0));

			classifierFigure_ShortNameBlock1.add(classifierFigure_shortName_padding2, BorderLayout.LEFT);

			fFigureClassifierFigure_shortName = new WrappingLabel();

			fFigureClassifierFigure_shortName.setText("<not set>");

			fFigureClassifierFigure_shortName.setFont(FFIGURECLASSIFIERFIGURE_SHORTNAME_FONT);

			classifierFigure_ShortNameBlock1.add(fFigureClassifierFigure_shortName, BorderLayout.CENTER);

			fDescriptionCompartmentFigure = new RectangleFigure();

			fDescriptionCompartmentFigure.setFill(false);
			fDescriptionCompartmentFigure.setOutline(false);
			fDescriptionCompartmentFigure.setBorder(createBorder0());

			this.add(fDescriptionCompartmentFigure);

			SubCompartmentLayoutManager layoutFDescriptionCompartmentFigure = new SubCompartmentLayoutManager();
			fDescriptionCompartmentFigure.setLayoutManager(layoutFDescriptionCompartmentFigure);

			fClassifierFigure_Description = new WrappingLabel();

			fClassifierFigure_Description.setText("<enter description>");

			fClassifierFigure_Description.setFont(FCLASSIFIERFIGURE_DESCRIPTION_FONT);

			fDescriptionCompartmentFigure.add(fClassifierFigure_Description);

			fPropertiesCompartmentFigure = new RectangleFigure();

			fPropertiesCompartmentFigure.setFill(false);
			fPropertiesCompartmentFigure.setOutline(false);
			fPropertiesCompartmentFigure.setBorder(createBorder1());

			this.add(fPropertiesCompartmentFigure);

			SubCompartmentLayoutManager layoutFPropertiesCompartmentFigure = new SubCompartmentLayoutManager();
			fPropertiesCompartmentFigure.setLayoutManager(layoutFPropertiesCompartmentFigure);

		}

		/**
		* @generated NOT
		*/
		private void createContents() {
			setupLayout();
			createContentsGen();
			fDescriptionCompartmentFigure.remove(fClassifierFigure_Description);
			fClassifierFigure_Description = new HTMLWrappingLabel();
			fClassifierFigure_Description.setText("<enter description>");
			fClassifierFigure_Description.setFont(FCLASSIFIERFIGURE_DESCRIPTION_FONT);
			fDescriptionCompartmentFigure.add(fClassifierFigure_Description);

			fDoActionClassifierLabel.setAlignment(PositionConstants.LEFT);
		}
		
		/**
		* @generated NOT
		*/
		private void setupLayout() {
			ToolbarLayout layoutThis = new FlexCompartmentToolbarLayout(1);
			layoutThis.setStretchMinorAxis(true);
			layoutThis.setMinorAlignment(ToolbarLayout.ALIGN_TOPLEFT);

			layoutThis.setSpacing(0);
			layoutThis.setVertical(true);

			this.setLayoutManager(layoutThis);
		}

		/**
		* @generated
		*/
		private Border createBorder0() {
			OneLineBorder result = new OneLineBorder();

			return result;
		}

		/**
		   * @generated
		   */
		private Border createBorder1() {
			OneLineBorder result = new OneLineBorder();

			return result;
		}

		/**
			 * @generated
			 */
		public WrappingLabel getFigureClassifierFigure_name() {
			return myClassifierFigure_name;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureClassifierFigure_shortName() {
			return fFigureClassifierFigure_shortName;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureClassifierFigure_ename() {
			return myClassifierFigure_ename;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getPropertiesCompartmentFigure() {
			return fPropertiesCompartmentFigure;
		}

		/**
		   * @generated
		   */
		public RectangleFigure getDescriptionCompartmentFigure() {
			return fDescriptionCompartmentFigure;
		}

		/**
		   * @generated
		   */
		public WrappingLabel getClassifierFigure_Description() {
			return fClassifierFigure_Description;
		}

		/**
		   * @generated
		   */
		public WrappingLabel getDoActionClassifierLabel() {
			return fDoActionClassifierLabel;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 47, 214, 45);

	/**
	 * @generated
	 */
	static final Font MYCLASSIFIERFIGURE_NAME_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 8, SWT.BOLD);

	/**
	* @generated
	*/
	static final Font FDOACTIONCLASSIFIERLABEL_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 7, SWT.NORMAL);

	/**
	 * @generated
	 */
	static final Font CLASSIFIERFIGURE_SHORTNAME_PADDING2_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 7, SWT.ITALIC);

	/**
	 * @generated
	 */
	static final Font FFIGURECLASSIFIERFIGURE_SHORTNAME_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 7, SWT.NORMAL);

	/**
	 * @generated
	 */
	static final Font FCLASSIFIERFIGURE_DESCRIPTION_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 7, SWT.NORMAL);

}
