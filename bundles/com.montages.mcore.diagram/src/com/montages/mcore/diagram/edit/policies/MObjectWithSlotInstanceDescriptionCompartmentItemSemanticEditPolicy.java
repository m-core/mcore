package com.montages.mcore.diagram.edit.policies;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MObjectWithSlotInstanceDescriptionCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MObjectWithSlotInstanceDescriptionCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MObject_2003);
	}

}
