package com.montages.mcore.diagram.edit.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.util.DisplayUtils;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.diagram.actions.EObjectSelectionDialog;
import com.montages.mcore.diagram.parsers.SlotParser;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;

/**
 * @generated
 */
public class MObjectCreateCommand extends EditElementCommand {

	/**
	* @generated
	*/
	public MObjectCreateCommand(CreateElementRequest req) {
		super(req.getLabel(), null, req);
	}

	/**
	* FIXME: replace with setElementToEdit()
	* @generated 
	*/
	protected EObject getElementToEdit() {
		EObject container = ((CreateElementRequest) getRequest()).getContainer();
		if (container instanceof View) {
			container = ((View) container).getElement();
		}
		return container;
	}

	/**
	* @generated
	*/
	public boolean canExecute() {
		return true;

	}

	/**
	* @generated NOT
	*/
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		MResource resource = findResource();
		List<MPropertyInstance> propertyInstances = collectContainmentSlots(resource);
		if (resource == null || (resource.getObject().isEmpty() && (propertyInstances == null || propertyInstances.isEmpty()))) {
			MPackage owner = (MPackage) getElementToEdit();
			MClassifier rootClassifier = owner.getResourceRootClass();
			if (rootClassifier == null) {
				return CommandResult.newErrorCommandResult("Resource root class should be setup.");
			}
			Command cmd = rootClassifier.doActionUpdate(MClassifierAction.CREATE_INSTANCE).getContainingTransition().interpreteTransitionAsCommand();
			if (cmd == null || !cmd.canExecute()) {
				return CommandResult.newErrorCommandResult("Can't execute " + MClassifierAction.CREATE_INSTANCE + " classifier action.");
			}
			getEditingDomain().getCommandStack().execute(cmd);
			resource = findResource();
			List<MObject> mObjects = resource.getObject();
			if (mObjects == null || mObjects.isEmpty()) {
				return CommandResult.newErrorCommandResult("Looks like an MObject does not created.");
			}
			MObject newElement = mObjects.get(0);
			((CreateElementRequest) getRequest()).setNewElement(newElement);
			return CommandResult.newOKCommandResult(newElement);
		}
		EObjectSelectionDialog dialog = new EObjectSelectionDialog(DisplayUtils.getDisplay().getActiveShell(), propertyInstances, new SlotParser() {

			@Override
			public String getPrintString(EObject element) {
				if (false == element instanceof MPropertyInstance) {
					throw new IllegalArgumentException("Unexpected element type");
				}

				MPropertyInstance mPropertyInstance = (MPropertyInstance) element;
				String containerNumericId = ((MObject) mPropertyInstance.getContainingObject()).getNumericIdOfObject();
				return containerNumericId + "/" + super.getPrintString(element);
			}

		});
		int res = dialog.open();
		if (res != 0) {
			return CommandResult.newCancelledCommandResult();
		}
		Object[] selection = dialog.getResult();
		if (selection == null || selection.length != 1) {
			return CommandResult.newCancelledCommandResult();
		}
		MPropertyInstance selected = (MPropertyInstance) selection[0];
		//createDoActionCommand(selected, MPropertyInstanceAction.CONTAINED_OBJECT);
		MObject newElement = createNewMObject(resource, selected, monitor, info);
		((CreateElementRequest) getRequest()).setNewElement(newElement);
		return CommandResult.newOKCommandResult(newElement);
	}

	/**
	 * @added
	 */
	private static List<MPropertyInstance> collectContainmentSlots(MResource resource) {
		if (resource == null) {
			return Collections.emptyList();
		}
		List<MPropertyInstance> result = new ArrayList<MPropertyInstance>();
		for (MObject nextObject : resource.getObject()) {
			result.addAll(collectContainmentSlots(nextObject));
		}
		return result;
	}

	private static List<MPropertyInstance> collectContainmentSlots(MObject mObject) {
		if (mObject.getPropertyInstance().isEmpty()) {
			return Collections.emptyList();
		}
		List<MPropertyInstance> result = new ArrayList<MPropertyInstance>();
		for (MPropertyInstance nextPropertyInstance : mObject.getPropertyInstance()) {
			MProperty nextProperty = nextPropertyInstance.getProperty();
			if (nextProperty == null || !nextProperty.isSetContainment()) {
				continue;
			}
			result.add(nextPropertyInstance);
			for (MObject child : nextPropertyInstance.getInternalContainedObject()) {
				result.addAll(collectContainmentSlots(child));
			}
		}
		return result;
	}

	/**
	 * @added
	 */
	private MObject createNewMObject(MResource resource, MPropertyInstance slot, IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		MObject result = ObjectsFactory.eINSTANCE.createMObject();
		if (slot != null) {
			slot.getInternalContainedObject().add(result);
		} else {
			resource.getObject().add(result);
		}
		doConfigure(result, monitor, info);
		return result;
	}

	/**
	 * @added
	 */
	private MResource findResource() {
		MPackage mPackage = (MPackage) getElementToEdit();
		MComponent component = mPackage.getContainingComponent();
		MResource resource = findResourceForCondition(component, HAS_OBJECTS);
		if (resource == null) {
			resource = findResourceForCondition(component, ANY_RESOURCE);
		}
		return resource;
	}

	/**
	 * @added
	 */
	private MResource findResourceForCondition(MComponent component, MResourceCondition condition) {
		LinkedList<MResourceFolder> folders = new LinkedList<MResourceFolder>();
		folders.addAll(component.getResourceFolder());
		while (!folders.isEmpty()) {
			MResourceFolder nextFolder = folders.removeFirst();
			for (MResource nextResource : nextFolder.getResource()) {
				if (condition.accept(nextResource)) {
					return nextResource;
				}
			}
			folders.addAll(nextFolder.getFolder());
		}
		return null;
	}

	/**
	 * @added
	 */
	private interface MResourceCondition {

		public boolean accept(MResource resource);
	}

	/**
	 * @added
	 */
	private static final MResourceCondition ANY_RESOURCE = new MResourceCondition() {

		@Override
		public boolean accept(MResource resource) {
			return true;
		}
	};

	/**
	 * @added
	 */
	private static final MResourceCondition HAS_OBJECTS = new MResourceCondition() {

		@Override
		public boolean accept(MResource resource) {
			return !resource.getObject().isEmpty();
		}
	};

	/**
	* @generated
	*/
	protected void doConfigure(MObject newElement, IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IElementType elementType = ((CreateElementRequest) getRequest()).getElementType();
		ConfigureRequest configureRequest = new ConfigureRequest(getEditingDomain(), newElement, elementType);
		configureRequest.setClientContext(((CreateElementRequest) getRequest()).getClientContext());
		configureRequest.addParameters(getRequest().getParameters());
		ICommand configureCommand = elementType.getEditCommand(configureRequest);
		if (configureCommand != null && configureCommand.canExecute()) {
			configureCommand.execute(monitor, info);
		}
	}

}
