package com.montages.mcore.diagram.providers;

import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;

/**
 * @generated
 */
public class McoreIconProvider extends DefaultElementTypeIconProvider implements IIconProvider {

	/**
	* @generated
	*/
	public McoreIconProvider() {
		super(McoreElementTypes.TYPED_INSTANCE);
	}
}
