package com.montages.mcore.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.diagram.providers.McoreModelingAssistantProvider;

/**
 * @generated
 */
public class McoreModelingAssistantProviderOfMClassifierEditPart extends McoreModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(McoreElementTypes.MProperty_3004);
		types.add(McoreElementTypes.MLiteral_3005);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((MClassifierEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(MClassifierEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(McoreElementTypes.MClassifierSuperType_4002);
		types.add(McoreElementTypes.MProperty_4003);
		types.add(McoreElementTypes.MProperty_4006);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source, IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget((MClassifierEditPart) sourceEditPart, targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(MClassifierEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof MClassifierEditPart) {
			types.add(McoreElementTypes.MClassifierSuperType_4002);
		}
		if (targetEditPart instanceof MClassifierEditPart) {
			types.add(McoreElementTypes.MProperty_4003);
		}
		if (targetEditPart instanceof MClassifierEditPart) {
			types.add(McoreElementTypes.MProperty_4006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source, IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((MClassifierEditPart) sourceEditPart, relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(MClassifierEditPart source, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == McoreElementTypes.MClassifierSuperType_4002) {
			types.add(McoreElementTypes.MClassifier_2001);
		} else if (relationshipType == McoreElementTypes.MProperty_4003) {
			types.add(McoreElementTypes.MClassifier_2001);
		} else if (relationshipType == McoreElementTypes.MProperty_4006) {
			types.add(McoreElementTypes.MClassifier_2001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((MClassifierEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(MClassifierEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(4);
		types.add(McoreElementTypes.MObjectType_4001);
		types.add(McoreElementTypes.MClassifierSuperType_4002);
		types.add(McoreElementTypes.MProperty_4003);
		types.add(McoreElementTypes.MProperty_4006);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target, IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((MClassifierEditPart) targetEditPart, relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(MClassifierEditPart target, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == McoreElementTypes.MObjectType_4001) {
			types.add(McoreElementTypes.MObject_2002);
			types.add(McoreElementTypes.MObject_2003);
		} else if (relationshipType == McoreElementTypes.MClassifierSuperType_4002) {
			types.add(McoreElementTypes.MClassifier_2001);
		} else if (relationshipType == McoreElementTypes.MProperty_4003) {
			types.add(McoreElementTypes.MClassifier_2001);
		} else if (relationshipType == McoreElementTypes.MProperty_4006) {
			types.add(McoreElementTypes.MClassifier_2001);
		}
		return types;
	}

}
