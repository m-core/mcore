package com.montages.mcore.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.diagram.providers.McoreModelingAssistantProvider;

/**
 * @generated
 */
public class McoreModelingAssistantProviderOfMPackageEditPart extends McoreModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override
	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(McoreElementTypes.MClassifier_2001);
		types.add(McoreElementTypes.MObject_2002);
		types.add(McoreElementTypes.MObject_2003);
		return types;
	}

}