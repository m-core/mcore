package com.montages.mcore.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;
import org.eclipse.gmf.runtime.diagram.ui.preferences.IPreferenceConstants;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

	/**
	 * @generated NOT
	 */
	public DiagramGeneralPreferencePage() {
		setPreferenceStore(McoreDiagramEditorPlugin.getInstance().getPreferenceStore());

		getPreferenceStore().setDefault(IPreferenceConstants.PREF_SHOW_CONNECTION_HANDLES, false);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_SHOW_POPUP_BARS, false);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_ENABLE_ANIMATED_LAYOUT, false);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_ENABLE_ANIMATED_ZOOM, false);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_SHOW_STATUS_LINE, true);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_ENABLE_ANTIALIAS, true);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_SHOW_GRID, true);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_SNAP_TO_GRID, true);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_RULER_UNITS, 0);
		getPreferenceStore().setDefault(IPreferenceConstants.PREF_GRID_SPACING, "0.2");
	}

}
