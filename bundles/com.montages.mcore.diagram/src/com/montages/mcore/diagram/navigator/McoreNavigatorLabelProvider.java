package com.montages.mcore.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import com.montages.mcore.MPackage;
import com.montages.mcore.diagram.edit.parts.MClassifierENameEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierSuperTypeEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectIdEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectType2EditPart;
import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstancePropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyNameEditPart;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.diagram.providers.McoreParserProvider;

/**
 * @generated
 */
public class McoreNavigatorLabelProvider extends LabelProvider implements ICommonLabelProvider, ITreePathLabelProvider {

	/**
	* @generated
	*/
	static {
		McoreDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		McoreDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof McoreNavigatorItem && !isOwnView(((McoreNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	* @generated
	*/
	public Image getImage(Object element) {
		if (element instanceof McoreNavigatorGroup) {
			McoreNavigatorGroup group = (McoreNavigatorGroup) element;
			return McoreDiagramEditorPlugin.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof McoreNavigatorItem) {
			McoreNavigatorItem navigatorItem = (McoreNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	* @generated
	*/
	public Image getImage(View view) {
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MPackageEditPart.VISUAL_ID:
			return getImage("Navigator?Diagram?http://www.montages.com/mCore/MCore?MPackage", McoreElementTypes.MPackage_1000); //$NON-NLS-1$
		case MClassifierEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.montages.com/mCore/MCore?MClassifier", McoreElementTypes.MClassifier_2001); //$NON-NLS-1$
		case MObjectEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.montages.com/mCore/MCore/Objects?MObject", McoreElementTypes.MObject_2002); //$NON-NLS-1$
		case MObjectType2EditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://www.montages.com/mCore/MCore/Objects?MObject?type", McoreElementTypes.MObjectType_4001); //$NON-NLS-1$
		case MClassifierSuperTypeEditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://www.montages.com/mCore/MCore?MClassifier?superType", McoreElementTypes.MClassifierSuperType_4002); //$NON-NLS-1$
		case MPropertyEditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://www.montages.com/mCore/MCore?MProperty", McoreElementTypes.MProperty_4003); //$NON-NLS-1$
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://www.montages.com/mCore/MCore/Objects?MPropertyInstance", McoreElementTypes.MPropertyInstance_4004); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = McoreDiagramEditorPlugin.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null && McoreElementTypes.isKnownElementType(elementType)) {
			image = McoreElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	* @generated
	*/
	public String getText(Object element) {
		if (element instanceof McoreNavigatorGroup) {
			McoreNavigatorGroup group = (McoreNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof McoreNavigatorItem) {
			McoreNavigatorItem navigatorItem = (McoreNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	* @generated
	*/
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MPackageEditPart.VISUAL_ID:
			return getMPackage_1000Text(view);
		case MClassifierEditPart.VISUAL_ID:
			return getMClassifier_2001Text(view);
		case MObjectEditPart.VISUAL_ID:
			return getMObject_2002Text(view);
		case MObjectType2EditPart.VISUAL_ID:
			return getMObjectType_4001Text(view);
		case MClassifierSuperTypeEditPart.VISUAL_ID:
			return getMClassifierSuperType_4002Text(view);
		case MPropertyEditPart.VISUAL_ID:
			return getMProperty_4003Text(view);
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getMPropertyInstance_4004Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	* @generated
	*/
	private String getMObjectType_4001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getMClassifierSuperType_4002Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getMPackage_1000Text(View view) {
		MPackage domainModelElement = (MPackage) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			McoreDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMClassifier_2001Text(View view) {
		IParser parser = McoreParserProvider.getParser(McoreElementTypes.MClassifier_2001, view.getElement() != null ? view.getElement() : view,
				McoreVisualIDRegistry.getType(MClassifierENameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view), ParserOptions.NONE.intValue());
		} else {
			McoreDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMObject_2002Text(View view) {
		IParser parser = McoreParserProvider.getParser(McoreElementTypes.MObject_2002, view.getElement() != null ? view.getElement() : view,
				McoreVisualIDRegistry.getType(MObjectIdEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view), ParserOptions.NONE.intValue());
		} else {
			McoreDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMPropertyInstance_4004Text(View view) {
		IParser parser = McoreParserProvider.getParser(McoreElementTypes.MPropertyInstance_4004, view.getElement() != null ? view.getElement() : view,
				McoreVisualIDRegistry.getType(MPropertyInstancePropertyEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view), ParserOptions.NONE.intValue());
		} else {
			McoreDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMProperty_4003Text(View view) {
		IParser parser = McoreParserProvider.getParser(McoreElementTypes.MProperty_4003, view.getElement() != null ? view.getElement() : view,
				McoreVisualIDRegistry.getType(MPropertyNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view), ParserOptions.NONE.intValue());
		} else {
			McoreDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	* @generated
	*/
	public void restoreState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public void saveState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	* @generated
	*/
	private boolean isOwnView(View view) {
		return MPackageEditPart.MODEL_ID.equals(McoreVisualIDRegistry.getModelID(view));
	}

}
