package com.montages.mcore.diagram.part;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.edit.parts.ClassifierDescriptionCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.InstanceDerivationsCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.InstanceDescriptionCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MClassiferPropertiesCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierDescriptionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierDoActionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierENameEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierLiteralEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierNameEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierShortNameEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectDescriptionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectDoActionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectIdEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectSlotCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectTypeEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectTypeWithSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotDescriptionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotDoActionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotIdEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotInstanceDerivationsCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotInstanceDescriptionCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotSlotCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentMultiplicityAsStringSinEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentNameEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentLinkLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentsLinkEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstancePropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyMultiplicityAsStringSinEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyNameEditPart;
import com.montages.mcore.diagram.expressions.McoreOCLFactory;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class McoreVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "com.montages.mcore.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (MPackageEditPart.MODEL_ID.equals(view.getType())) {
				return MPackageEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return com.montages.mcore.diagram.part.McoreVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(Platform.getDebugOption(DEBUG_KEY))) {
				McoreDiagramEditorPlugin.getInstance().logError("Unable to parse view type as a visualID number: " + type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (McorePackage.eINSTANCE.getMPackage().isSuperTypeOf(domainElement.eClass()) && isDiagram((MPackage) domainElement)) {
			return MPackageEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = com.montages.mcore.diagram.part.McoreVisualIDRegistry.getModelID(containerView);
		if (!MPackageEditPart.MODEL_ID.equals(containerModelID) && !"Mcore".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (MPackageEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = com.montages.mcore.diagram.part.McoreVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = MPackageEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case MPackageEditPart.VISUAL_ID:
			if (McorePackage.eINSTANCE.getMClassifier().isSuperTypeOf(domainElement.eClass())) {
				return MClassifierEditPart.VISUAL_ID;
			}
			if (ObjectsPackage.eINSTANCE.getMObject().isSuperTypeOf(domainElement.eClass()) && isMObject_2002((MObject) domainElement)) {
				return MObjectEditPart.VISUAL_ID;
			}
			if (ObjectsPackage.eINSTANCE.getMObject().isSuperTypeOf(domainElement.eClass())) {
				return MObjectWithSlotEditPart.VISUAL_ID;
			}
			break;
		case MObjectSlotCompartmentEditPart.VISUAL_ID:
			if (ObjectsPackage.eINSTANCE.getMPropertyInstance().isSuperTypeOf(domainElement.eClass())) {
				return MObjectSlotEditPart.VISUAL_ID;
			}
			break;
		case MClassiferPropertiesCompartmentEditPart.VISUAL_ID:
			if (McorePackage.eINSTANCE.getMProperty().isSuperTypeOf(domainElement.eClass())) {
				return MClassifierPropertyEditPart.VISUAL_ID;
			}
			if (McorePackage.eINSTANCE.getMLiteral().isSuperTypeOf(domainElement.eClass())) {
				return MClassifierLiteralEditPart.VISUAL_ID;
			}
			break;
		case MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID:
			if (ObjectsPackage.eINSTANCE.getMPropertyInstance().isSuperTypeOf(domainElement.eClass())) {
				return MObjectSlotEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = com.montages.mcore.diagram.part.McoreVisualIDRegistry.getModelID(containerView);
		if (!MPackageEditPart.MODEL_ID.equals(containerModelID) && !"Mcore".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (MPackageEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = com.montages.mcore.diagram.part.McoreVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = MPackageEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case MPackageEditPart.VISUAL_ID:
			if (MClassifierEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectWithSlotEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MClassifierEditPart.VISUAL_ID:
			if (MClassifierENameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MClassifierNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MClassifierShortNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MClassifierDescriptionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MClassifierDoActionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ClassifierDescriptionCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MClassiferPropertiesCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MObjectEditPart.VISUAL_ID:
			if (MObjectIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectTypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectDescriptionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectDoActionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InstanceDescriptionCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectSlotCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InstanceDerivationsCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MObjectWithSlotEditPart.VISUAL_ID:
			if (MObjectWithSlotIdEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectTypeWithSlotEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectWithSlotDescriptionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectWithSlotDoActionLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectWithSlotInstanceDescriptionCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MObjectWithSlotInstanceDerivationsCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MObjectSlotCompartmentEditPart.VISUAL_ID:
			if (MObjectSlotEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MClassiferPropertiesCompartmentEditPart.VISUAL_ID:
			if (MClassifierPropertyEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MClassifierLiteralEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID:
			if (MObjectSlotEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MPropertyEditPart.VISUAL_ID:
			if (MPropertyNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MPropertyMultiplicityAsStringSinEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MPropertyInstanceEditPart.VISUAL_ID:
			if (MPropertyInstancePropertyEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
			if (MPropertyInstanceContainmentLinkLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MPropertyContainmentEditPart.VISUAL_ID:
			if (MPropertyContainmentNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MPropertyContainmentMultiplicityAsStringSinEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (McorePackage.eINSTANCE.getMProperty().isSuperTypeOf(domainElement.eClass()) && isMProperty_4003((MProperty) domainElement)) {
			return MPropertyEditPart.VISUAL_ID;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance().isSuperTypeOf(domainElement.eClass())) {
			return MPropertyInstanceEditPart.VISUAL_ID;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance().isSuperTypeOf(domainElement.eClass())) {
			return MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID;
		}
		if (McorePackage.eINSTANCE.getMProperty().isSuperTypeOf(domainElement.eClass())) {
			return MPropertyContainmentEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @added
	 */
	public static List<Integer> getLinkListWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return Collections.emptyList();
		}
		EClass eClass = domainElement.eClass();
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance().isSuperTypeOf(eClass) || McorePackage.eINSTANCE.getMProperty().isSuperTypeOf(eClass)) {
			return Arrays.asList(
					new Integer[] { MPropertyInstanceEditPart.VISUAL_ID, MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID, MPropertyContainmentEditPart.VISUAL_ID, MPropertyEditPart.VISUAL_ID });
		}
		return Collections.emptyList();
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(MPackage element) {
		return true;
	}

	/**
	* @generated
	*/
	private static boolean isMObject_2002(MObject domainElement) {
		Object result = McoreOCLFactory.getExpression(5, ObjectsPackage.eINSTANCE.getMObject(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	* @generated
	*/
	private static boolean isMProperty_4003(MProperty domainElement) {
		Object result = McoreOCLFactory.getExpression(4, McorePackage.eINSTANCE.getMProperty(), null).evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case MObjectSlotCompartmentEditPart.VISUAL_ID:
		case InstanceDescriptionCompartmentEditPart.VISUAL_ID:
		case InstanceDerivationsCompartmentEditPart.VISUAL_ID:
		case MClassiferPropertiesCompartmentEditPart.VISUAL_ID:
		case ClassifierDescriptionCompartmentEditPart.VISUAL_ID:
		case MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID:
		case MObjectWithSlotInstanceDescriptionCompartmentEditPart.VISUAL_ID:
		case MObjectWithSlotInstanceDerivationsCompartmentEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case MPackageEditPart.VISUAL_ID:
			return false;
		case MObjectSlotEditPart.VISUAL_ID:
		case MClassifierPropertyEditPart.VISUAL_ID:
		case MClassifierLiteralEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {

		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return com.montages.mcore.diagram.part.McoreVisualIDRegistry.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return com.montages.mcore.diagram.part.McoreVisualIDRegistry.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return com.montages.mcore.diagram.part.McoreVisualIDRegistry.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
			return com.montages.mcore.diagram.part.McoreVisualIDRegistry.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return com.montages.mcore.diagram.part.McoreVisualIDRegistry.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return com.montages.mcore.diagram.part.McoreVisualIDRegistry.isSemanticLeafVisualID(visualID);
		}
	};

}
