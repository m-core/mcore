package com.montages.mcore.diagram.part;

import java.util.Objects;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.util.Proxy;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;

import com.montages.mcore.diagram.edit.policies.EdgeWithNoSemanticElementRepresentationImpl;

/**
* @generated
*/
public class McoreLinkDescriptor extends UpdaterLinkDescriptor {

	/**
	* @generated
	*/
	public McoreLinkDescriptor(EObject source, EObject destination, IElementType elementType, int linkVID) {
		super(source, destination, elementType, linkVID);
	}

	/**
	* @generated
	*/
	public McoreLinkDescriptor(EObject source, EObject destination, EObject linkElement, IElementType elementType, int linkVID) {
		super(source, destination, linkElement, elementType, linkVID);
	}
	
	@Override
	public String toString() {
		return "Link[" + Integer.toHexString(System.identityHashCode(this))+ "]:" + getVisualID() + ", model: " + getModelElement() + ", from: " + getSource() + ", to: " + getDestination();
	}
	
	@Override
	public boolean equals(Object arg) {
		if (arg == this) {
			return true;
		}
		if (arg == null) {
			return false;
		}
		if (arg instanceof McoreLinkDescriptor) {
			McoreLinkDescriptor that = (McoreLinkDescriptor)arg;
			return Objects.equals(this.getVisualID(), that.getVisualID()) &&
					Objects.equals(this.getSource(), that.getSource()) && 
					Objects.equals(this.getDestination(), that.getDestination()) && 
					Objects.equals(getErasedSemanticElement(this.getModelElement()), getErasedSemanticElement(that.getModelElement()))
					// && Objects.equals(this.getSafeSemanticAdapter(), that.getSafeSemanticAdapter()) sic! with this being checked the type links gets doubled
					; 
		}
		return false;
	}
	
	private static Object getErasedSemanticElement(EObject semanticElement) {
		if (semanticElement instanceof EdgeWithNoSemanticElementRepresentationImpl) {
			return null; 
		} else {
			return semanticElement;
		}
	}
	
	@Override
	public int hashCode() {
		int result = Objects.hash(
				this.getVisualID(),
				this.getSource(), 
				this.getDestination(), 
				getErasedSemanticElement(this.getModelElement())
				//, getSafeSemanticAdapter() // sic! ignored in equals for descriptors without element, like type links
		);
		return result;
	}
	
	private Object getSafeSemanticAdapter() { // prevent weird NPE
		IAdaptable adapter = getSemanticAdapter();
		if (adapter instanceof Proxy && ((Proxy)adapter).getRealObject() == null) {
			return null;
		}
		return adapter;
	}

}
