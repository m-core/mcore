package com.montages.mcore.diagram.part;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

/**
 * @generated
 */
public class McoreCreationWizard extends Wizard implements INewWizard {

	public static final String DOMAIN_MODEL_FILE_PAGE = "DomainModelFile";

	public static final String DIAGRAM_MODEL_FILE_PAGE = "DiagramModelFile";

	/**
	* @generated
	*/
	private IWorkbench workbench;

	/**
	* @generated
	*/
	protected IStructuredSelection selection;

	/**
	* @generated
	*/
	protected McoreCreationWizardPage diagramModelFilePage;

	/**
	* @generated
	*/
	protected McoreCreationWizardPage domainModelFilePage;

	/**
	* @generated
	*/
	protected Resource diagram;

	/**
	* @generated
	*/
	private boolean openNewlyCreatedDiagramEditor = true;

	/**
	* @generated
	*/
	public IWorkbench getWorkbench() {
		return workbench;
	}

	/**
	* @generated
	*/
	public IStructuredSelection getSelection() {
		return selection;
	}

	/**
	* @generated
	*/
	public final Resource getDiagram() {
		return diagram;
	}

	/**
	* @generated
	*/
	public final boolean isOpenNewlyCreatedDiagramEditor() {
		return openNewlyCreatedDiagramEditor;
	}

	/**
	* @generated
	*/
	public void setOpenNewlyCreatedDiagramEditor(boolean openNewlyCreatedDiagramEditor) {
		this.openNewlyCreatedDiagramEditor = openNewlyCreatedDiagramEditor;
	}

	/**
	* @generated
	*/
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
		setWindowTitle(Messages.McoreCreationWizardTitle);
		setDefaultPageImageDescriptor(McoreDiagramEditorPlugin.getBundledImageDescriptor("icons/wizban/NewMcoreWizard.gif")); //$NON-NLS-1$
		setNeedsProgressMonitor(true);
	}

	/**
	* @generated NOT
	*/
	public void addPages() {
		diagramModelFilePage = new McoreCreationWizardPage(DIAGRAM_MODEL_FILE_PAGE, getSelection(), "mcore_diagram"); //$NON-NLS-1$ //$NON-NLS-2$
		diagramModelFilePage.setTitle(Messages.McoreCreationWizard_DiagramModelFilePageTitle);
		diagramModelFilePage.setDescription(Messages.McoreCreationWizard_DiagramModelFilePageDescription);
		addPage(diagramModelFilePage);

		domainModelFilePage = new McoreCreationWizardPage(DOMAIN_MODEL_FILE_PAGE, getSelection(), "mcore") { //$NON-NLS-1$ //$NON-NLS-2$

			public void setVisible(boolean visible) {
				if (visible) {
					String fileName = diagramModelFilePage.getFileName();
					fileName = fileName.substring(0, fileName.length() - ".mcore_diagram".length()); //$NON-NLS-1$
					setFileName(McoreDiagramEditorUtil.getUniqueFileName(getContainerFullPath(), fileName, "mcore")); //$NON-NLS-1$
				}
				super.setVisible(visible);
			}

			/**
			* @added
			*/
			public URI getURI() {
				if (selection != null && selection.size() == 1) {
					if (selection.getFirstElement() instanceof URI) {
						URI uri = (URI) selection.getFirstElement();
						if (getExtension().equals(uri.fileExtension())) {
							return uri;
						}
					}
				}
				return URI.createPlatformResourceURI(getFilePath().toString(), false);
			}
		};
		domainModelFilePage.setTitle(Messages.McoreCreationWizard_DomainModelFilePageTitle);
		domainModelFilePage.setDescription(Messages.McoreCreationWizard_DomainModelFilePageDescription);
		addPage(domainModelFilePage);
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		if (page == diagramModelFilePage && domainModelFilePage.getURI() != getSelection().getFirstElement()) {
			return domainModelFilePage;
		}
		return null;
	}

	@Override
	public boolean canFinish() {
		if (diagramModelFilePage.isPageComplete() && domainModelFilePage.getURI() == getSelection().getFirstElement()) {
			return true;
		}
		return super.canFinish();
	}

	/**
	* @generated
	*/
	public boolean performFinish() {
		IRunnableWithProgress op = new WorkspaceModifyOperation(null) {

			protected void execute(IProgressMonitor monitor) throws CoreException, InterruptedException {
				diagram = McoreDiagramEditorUtil.createDiagram(diagramModelFilePage.getURI(), domainModelFilePage.getURI(), monitor);
				if (isOpenNewlyCreatedDiagramEditor() && diagram != null) {
					try {
						McoreDiagramEditorUtil.openDiagram(diagram);
					} catch (PartInitException e) {
						ErrorDialog.openError(getContainer().getShell(), Messages.McoreCreationWizardOpenEditorError, null, e.getStatus());
					}
				}
			}
		};
		try {
			getContainer().run(false, true, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			if (e.getTargetException() instanceof CoreException) {
				ErrorDialog.openError(getContainer().getShell(), Messages.McoreCreationWizardCreationError, null, ((CoreException) e.getTargetException()).getStatus());
			} else {
				McoreDiagramEditorPlugin.getInstance().logError("Error creating diagram", e.getTargetException()); //$NON-NLS-1$
			}
			return false;
		}
		return diagram != null;
	}
}
