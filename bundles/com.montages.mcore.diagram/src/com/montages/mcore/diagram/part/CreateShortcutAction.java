package com.montages.mcore.diagram.part;

import java.util.List;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.LogHelper;
import org.eclipse.gmf.tooling.runtime.part.DefaultCreateShortcutHandler;
import org.eclipse.gmf.tooling.runtime.part.DefaultElementChooserDialog;
import org.eclipse.swt.widgets.Shell;

import com.montages.mcore.diagram.edit.commands.McoreCreateShortcutDecorationsCommand;

/**
 * @generated
 */
public class CreateShortcutAction extends DefaultCreateShortcutHandler {

	/**
	* @generated
	*/
	public CreateShortcutAction() {
		this(McoreDiagramEditorPlugin.getInstance().getLogHelper());
	}

	/**
	* @generated
	*/
	public CreateShortcutAction(LogHelper logHelper) {
		super(logHelper, McoreDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
	}

	/**
	* @generated
	*/
	@Override
	public DefaultElementChooserDialog createChooserDialog(Shell parentShell, View view) {
		return new McoreElementChooserDialog(parentShell, view);
	}

	/**
	* @generated
	*/
	@Override
	public ICommand createShortcutDecorationCommand(View view, TransactionalEditingDomain editingDomain, List<CreateViewRequest.ViewDescriptor> descriptors) {
		return new McoreCreateShortcutDecorationsCommand(editingDomain, view, descriptors);
	}

}
