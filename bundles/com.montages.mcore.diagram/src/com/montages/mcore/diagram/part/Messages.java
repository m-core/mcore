package com.montages.mcore.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String McoreCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String McoreCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String McoreCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String McoreCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String McoreCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String McoreCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String McoreCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String McoreCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String McoreDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String McoreNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String McoreDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String McoreElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String ModelElements1Group_title;

	/**
	 * @generated
	 */
	public static String ModelElements1Group_desc;

	/**
	* @generated
	*/
	public static String Object1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Object1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddNewlyContainedObject2CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddNewlyContainedObject2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ClassofObject3CreationTool_title;

	/**
	 * @generated
	 */
	public static String ClassofObject3CreationTool_desc;

	/**
	* @generated
	*/
	public static String Reference4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Reference4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Classifier6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Classifier6CreationTool_desc;

	/**
	* @generated
	*/
	public static String Generalization7CreationTool_title;

	/**
	 * @generated
	 */
	public static String Generalization7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Reference8CreationTool_title;

	/**
	 * @generated
	 */
	public static String Reference8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Containment9CreationTool_title;

	/**
	 * @generated
	 */
	public static String Containment9CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Property10CreationTool_title;

	/**
	 * @generated
	 */
	public static String Property10CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Literal11CreationTool_title;

	/**
	 * @generated
	 */
	public static String Literal11CreationTool_desc;

	/**
	 * @generated
	 */
	public static String MObjectSlotCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String InstanceDescriptionCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String InstanceDerivationsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String MClassiferPropertiesCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String ClassifierDescriptionCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String MObjectWithSlotSlotCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String MObjectWithSlotInstanceDescriptionCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String MObjectWithSlotInstanceDerivationsCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MObjectType_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MObjectType_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MClassifierSuperType_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MClassifierSuperType_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MObject_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MObject_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MPropertyInstance_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MPropertyInstance_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MProperty_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MProperty_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MPackage_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MClassifier_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MClassifier_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String McoreModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String McoreModelingAssistantProviderMessage;
	
	/**
	 * @generated NOT
	 */
	public static String EObjectSelectionDialogTitle;
	
	/**
	 * @generated NOT
	 */
	public static String MObjectWithSlotCreateCommanDialogTitle;


	// TODO: put accessor fields manually
}
