package com.montages.mcore.diagram.part;

import java.util.Collections;

import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultLinkToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultNodeToolEntry;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class McorePaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createModelElements1Group());
	}

	/**
	 * Creates "Model Elements" palette tool group
	 * 
	 * @generated
	 */
	private PaletteContainer createModelElements1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(Messages.ModelElements1Group_title);
		paletteContainer.setId("createModelElements1Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.ModelElements1Group_desc);
		paletteContainer.add(createObject1CreationTool());
		paletteContainer.add(createAddNewlyContainedObject2CreationTool());
		paletteContainer.add(createClassofObject3CreationTool());
		paletteContainer.add(createReference4CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createClassifier6CreationTool());
		paletteContainer.add(createGeneralization7CreationTool());
		paletteContainer.add(createReference8CreationTool());
		paletteContainer.add(createContainment9CreationTool());
		paletteContainer.add(createProperty10CreationTool());
		paletteContainer.add(createLiteral11CreationTool());
		return paletteContainer;
	}

	/**
	* @generated
	*/
	private ToolEntry createObject1CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Object1CreationTool_title, Messages.Object1CreationTool_desc, Collections.singletonList(McoreElementTypes.MObject_2002));
		entry.setId("createObject1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreElementTypes.getImageDescriptor(McoreElementTypes.MObject_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createAddNewlyContainedObject2CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.AddNewlyContainedObject2CreationTool_title, Messages.AddNewlyContainedObject2CreationTool_desc,
				Collections.singletonList(McoreElementTypes.MObject_2003));
		entry.setId("createAddNewlyContainedObject2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreElementTypes.getImageDescriptor(McoreElementTypes.MObject_2003));
		entry.setLargeIcon(McoreDiagramEditorPlugin.findImageDescriptor("")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated NOT
	 */
	private ToolEntry createClassofObject3CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.ClassofObject3CreationTool_title, Messages.ClassofObject3CreationTool_desc,
				Collections.singletonList(McoreElementTypes.MObjectType_4001));
		entry.setId("createClassofObject3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreDiagramEditorPlugin.findImageDescriptor("icons/palette/class-of-object.png"));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createReference4CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Reference4CreationTool_title, Messages.Reference4CreationTool_desc,
				Collections.singletonList(McoreElementTypes.MPropertyInstance_4004));
		entry.setId("createReference4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreDiagramEditorPlugin.findImageDescriptor("icons/palette/property-type.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createClassifier6CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Classifier6CreationTool_title, Messages.Classifier6CreationTool_desc,
				Collections.singletonList(McoreElementTypes.MClassifier_2001));
		entry.setId("createClassifier6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreDiagramEditorPlugin.findImageDescriptor("icons/palette/EClass.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createGeneralization7CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Generalization7CreationTool_title, Messages.Generalization7CreationTool_desc,
				Collections.singletonList(McoreElementTypes.MClassifierSuperType_4002));
		entry.setId("createGeneralization7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreDiagramEditorPlugin.findImageDescriptor("icons/palette/generalization.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createReference8CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Reference8CreationTool_title, Messages.Reference8CreationTool_desc, Collections.singletonList(McoreElementTypes.MProperty_4003));
		entry.setId("createReference8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreDiagramEditorPlugin.findImageDescriptor("icons/palette/property-type.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated NOT
	*/
	private ToolEntry createContainment9CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Containment9CreationTool_title, Messages.Containment9CreationTool_desc,
				Collections.singletonList(McoreElementTypes.MProperty_4006));
		entry.setId("createContainment9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreDiagramEditorPlugin.findImageDescriptor("icons/palette/containment_arrow.png"));
		entry.setLargeIcon(entry.getSmallIcon()); //$NON-NLS-1$
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createProperty10CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Property10CreationTool_title, Messages.Property10CreationTool_desc, Collections.singletonList(McoreElementTypes.MProperty_3004));
		entry.setId("createProperty10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreElementTypes.getImageDescriptor(McoreElementTypes.MProperty_3004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createLiteral11CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Literal11CreationTool_title, Messages.Literal11CreationTool_desc, Collections.singletonList(McoreElementTypes.MLiteral_3005));
		entry.setId("createLiteral11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(McoreElementTypes.getImageDescriptor(McoreElementTypes.MLiteral_3005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}
}
