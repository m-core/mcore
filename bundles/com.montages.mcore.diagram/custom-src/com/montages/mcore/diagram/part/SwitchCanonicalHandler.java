package com.montages.mcore.diagram.part;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.policies.CustomMPackageCanonicalEditPolicy;
import com.montages.mcore.diagram.edit.policies.MPackageCanonicalEditPolicy;

public class SwitchCanonicalHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (false == selection instanceof IStructuredSelection) {
			return null;
		}
		IStructuredSelection structured = (IStructuredSelection) selection;
		if (structured.size() != 1) {
			return null;
		}
		if (false == structured.getFirstElement() instanceof MPackageEditPart) {
			return null;
		}
		MPackageEditPart part = (MPackageEditPart)structured.getFirstElement();
		String keyInstall = null;
		String keyUninstall = null;
		EditPolicy policyToInstall = null;
		if (part.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE) != null) {
			keyInstall = CustomMPackageCanonicalEditPolicy.ROLE;
			keyUninstall = EditPolicyRoles.CANONICAL_ROLE;
			policyToInstall = new CustomMPackageCanonicalEditPolicy();
		} else {
			keyInstall = EditPolicyRoles.CANONICAL_ROLE;
			keyUninstall = CustomMPackageCanonicalEditPolicy.ROLE;
			policyToInstall = new MPackageCanonicalEditPolicy();
		}

		part.removeEditPolicy(keyUninstall);
		part.installEditPolicy(keyInstall, policyToInstall);

		return null;
	}

}
