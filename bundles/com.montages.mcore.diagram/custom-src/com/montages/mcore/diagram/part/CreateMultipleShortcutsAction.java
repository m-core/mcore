package com.montages.mcore.diagram.part;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.CommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

import com.montages.mcore.diagram.edit.commands.McoreCreateShortcutDecorationsCommand;

public class CreateMultipleShortcutsAction extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart diagramEditor = HandlerUtil.getActiveEditorChecked(event);
		Shell shell = diagramEditor.getEditorSite().getShell();
		assert diagramEditor instanceof DiagramEditor;
		TransactionalEditingDomain editingDomain = ((DiagramEditor) diagramEditor).getEditingDomain();
		ISelection selection = HandlerUtil.getCurrentSelectionChecked(event);
		assert selection instanceof IStructuredSelection;
		assert ((IStructuredSelection) selection).size() == 1;
		assert ((IStructuredSelection) selection).getFirstElement() instanceof EditPart;
		EditPart selectedDiagramPart = (EditPart) ((IStructuredSelection) selection).getFirstElement();
		final View view = (View) selectedDiagramPart.getModel();

		org.eclipse.swt.graphics.Point swtLastClick = getLastClickPosition(shell, event);
		Point d2dCursorBeforeDialog = adjustForCanvasLocation(swtLastClick, (IGraphicalEditPart) selectedDiagramPart, (DiagramEditor) diagramEditor);

		McoreElementChooserDialog elementChooser = new McoreElementChooserDialog(shell, view, true);
		int result = elementChooser.open();
		if (result != Window.OK) {
			return null;
		}
		List<URI> allSelectedModelElementURIs = elementChooser.getSelectedModelElementURIs();
		List<EObject> allSelectedObjects = new ArrayList<EObject>();
		for (URI nextURI : allSelectedModelElementURIs) {
			try {
				EObject next = editingDomain.getResourceSet().getEObject(nextURI, true);
				if (next != null) {
					allSelectedObjects.add(next);
				}
			} catch (WrappedException e) {
				McoreDiagramEditorPlugin.getInstance().logError("Exception while loading object: " + nextURI.toString(), e); //$NON-NLS-1$
			}
		}

		if (allSelectedObjects.isEmpty()) {
			return null;
		}

		DropObjectsRequest req = new DropObjectsRequest();
		req.setObjects(allSelectedObjects);
		req.setAllowedDetail(DND.DROP_COPY);
		req.setRequiredDetail(DND.DROP_COPY);
		req.setLocation(d2dCursorBeforeDialog);

		ICommand iCommand;
		Command gefCommand = selectedDiagramPart.getCommand(req);
		if (gefCommand != null && gefCommand.canExecute()) {
			iCommand = new CommandProxy(gefCommand);
		} else {
			CompositeTransactionalCommand iCompositeC = new CompositeTransactionalCommand(editingDomain, "Add Shortcuts");
			List<ViewDescriptor> viewDescriptors = new ArrayList<CreateViewRequest.ViewDescriptor>(allSelectedObjects.size());
			for (EObject next : allSelectedObjects) {
				CreateViewRequest.ViewDescriptor viewDescriptor = new CreateViewRequest.ViewDescriptor(new EObjectAdapter(next), Node.class, null, McoreDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
				ICommand createCommand = new CreateCommand(editingDomain, viewDescriptor, view);
				if (createCommand.canExecute()) {
					iCompositeC.add(createCommand);
					viewDescriptors.add(viewDescriptor);
				}
			}
			if (iCompositeC.isEmpty()) {
				return null;
			}
			iCompositeC.add(new McoreCreateShortcutDecorationsCommand(editingDomain, view, viewDescriptors));

			iCommand = iCompositeC;
		}

		try {
			OperationHistoryFactory.getOperationHistory().execute(iCommand, new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			McoreDiagramEditorPlugin.getInstance().logError("Unable to create shortcut", e); //$NON-NLS-1$
		}
		return null;
	}

	private org.eclipse.swt.graphics.Point getLastClickPosition(Shell shell, ExecutionEvent event) {
		org.eclipse.swt.graphics.Point fromSourceProvider = (org.eclipse.swt.graphics.Point) HandlerUtil.getVariable(event, LastClickPositionProvider.SOURCE_LAST_CLICK);
		if (fromSourceProvider != null) {
			//System.err.println("From source provider: " + fromSourceProvider);
			return fromSourceProvider;
		}

		org.eclipse.swt.graphics.Point fromCurrentPosition = shell.getDisplay().getCursorLocation();
		//System.err.println("Nothing from sourceProvider, will use the current position from shell: " + fromCurrentPosition);
		return fromCurrentPosition;
	}

	private Point adjustForCanvasLocation(org.eclipse.swt.graphics.Point swtPoint, IGraphicalEditPart selectedDiagramPart, DiagramEditor diagramEditor) {
		Control diagramCanvas = ((DiagramEditor) diagramEditor).getDiagramGraphicalViewer().getControl();
		org.eclipse.swt.graphics.Point swtLocation = diagramCanvas.toDisplay(0, 0);
		//System.err.println("Diagram Canvas SWT position: " + swtLocation);

		Point d2dPoint = new Point(swtPoint.x, swtPoint.y);
		selectedDiagramPart.getFigure().translateToRelative(d2dPoint);
		//System.err.println("After D2D translation: " + d2dPoint);

		d2dPoint.translate(-swtLocation.x, -swtLocation.y);
		//System.err.println("After SWT translation: " + d2dPoint);

		return d2dPoint;

	}
}
