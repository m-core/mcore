package com.montages.mcore.diagram.part;

import java.util.Collections;
import java.util.Map;

import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;
import org.eclipse.ui.services.IEvaluationService;

public class LastClickPositionProvider extends AbstractSourceProvider {

	public static String SOURCE_LAST_CLICK = "com.montages.mcore.diagram.part.lastClickPosition";

	private MouseListener myMouseListener;

	private Control myEditorCanvas;

	private DiagramEditor myDiagramEditor;

	private IEvaluationService myEvaluationService;

	private Point myLastClickedAt = new Point(0, 0);

	public LastClickPositionProvider(DiagramEditor diagramEditor) {
		myDiagramEditor = diagramEditor;
		myEditorCanvas = diagramEditor.getDiagramGraphicalViewer().getControl();
		myMouseListener = new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent e) {
				//System.err.println("mouseDown: " + e);
				//System.err.println("event location: " + e.x + ", " + e.y);
				Control source = (Control) e.widget;
				org.eclipse.swt.graphics.Point click = source.toDisplay(e.x, e.y);
				//System.err.println("In Display coordinates: " + click);
				fireLastClickedAt(click.x, click.y);
			}

		};
		myEditorCanvas.addMouseListener(myMouseListener);
	}

	public void attachToService() {
		if (myEvaluationService == null) {
			myEvaluationService = (IEvaluationService) myDiagramEditor.getSite().getService(IEvaluationService.class);
			if (myEvaluationService != null) {
				myEvaluationService.addSourceProvider(this);
			}
		}
	}

	public void detachFromService() {
		if (myEvaluationService != null) {
			myEvaluationService.removeSourceProvider(this);
			myEvaluationService = null;
		}
	}

	public void fireLastClickedAt(int x, int y) {
		myLastClickedAt = new Point(x, y);
		fireSourceChanged(ISources.WORKBENCH, SOURCE_LAST_CLICK, myLastClickedAt);
	}

	@Override
	public void dispose() {
		if (!myEditorCanvas.isDisposed()) {
			if (myMouseListener != null) {
				myEditorCanvas.removeMouseListener(myMouseListener);
			}
		}
		if (myMouseListener != null) {
			myMouseListener = null;
		}
	}

	@Override
	public Map<?, ?> getCurrentState() {
		return Collections.singletonMap(SOURCE_LAST_CLICK, myLastClickedAt);
	}

	@Override
	public String[] getProvidedSourceNames() {
		return new String[] { SOURCE_LAST_CLICK };
	}

}