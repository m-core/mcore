package com.montages.mcore.diagram.providers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.menus.IWorkbenchContribution;
import org.eclipse.ui.services.IServiceLocator;
import org.xocl.semantics.commands.ChangeCommandWithOperationCall;

import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MPackageAction;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.commands.wrappers.EMFtoGMFCommandWrapper;
import com.montages.mcore.diagram.commands.wrappers.GMFtoEMFCommandWrapper;
import com.montages.mcore.diagram.edit.policies.DoActionLabelDirectEditPolicy.DoActionResultCreateViewCommand;
import com.montages.mcore.impl.MClassifierImpl;
import com.montages.mcore.impl.MPackageImpl;
import com.montages.mcore.impl.MPropertyImpl;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MPropertyInstanceAction;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.MObjectImpl;
import com.montages.mcore.objects.impl.MPropertyInstanceImpl;

public class DoActionMenuContributionItem extends ContributionItem implements IWorkbenchContribution {

	/**
	 * Service locator given to this contribution item using the {@link IWorkbenchContribution} interface.
	 */
	private IServiceLocator myServiceLocator;

	public DoActionMenuContributionItem() {
		setId("org.eclipse.montages.mcore.diagram.doaction.menuitem");
	}

	public DoActionMenuContributionItem(String id) {
		super(id);
	}

	@Override
	public void fill(Menu menu, int index) {
		final Object selectedElement = getSelectedElement();
		if (selectedElement == null) {
			return;
		}
		if (false == selectedElement instanceof IGraphicalEditPart) {
			return;
		}
		IGraphicalEditPart view = (IGraphicalEditPart) selectedElement;
		EObject selectedEObject = view.resolveSemanticElement();
		if (selectedEObject == null) {
			return;
		}
		if (selectedEObject instanceof MClassifierImpl) {
			EAttribute feature = McorePackage.Literals.MCLASSIFIER__DO_ACTION;
			MClassifierAction[] excluded = new MClassifierAction[] { MClassifierAction.DO };
			List<MClassifierAction> calculatedList = ((MClassifierImpl) selectedEObject).evalDoActionChoiceConstruction(getAllSupportedActions(selectedEObject, feature, MClassifierAction.class));
			createSubMenu(menu, feature, filterActions(calculatedList, Arrays.asList(excluded)));
			return;
		}
		if (selectedEObject instanceof MObjectImpl) {
			EAttribute feature = ObjectsPackage.Literals.MOBJECT__DO_ACTION;
			MObjectAction[] excluded = new MObjectAction[] { MObjectAction.DO };
			List<MObjectAction> calculatedList = ((MObjectImpl) selectedEObject).evalDoActionChoiceConstruction(getAllSupportedActions(selectedEObject, feature, MObjectAction.class));
			createSubMenu(menu, feature, filterActions(calculatedList, Arrays.asList(excluded)));
			return;
		}
		if (selectedEObject instanceof MPackageImpl) {
			EAttribute feature = McorePackage.Literals.MPACKAGE__DO_ACTION;
			MPackageAction[] excluded = new MPackageAction[] { MPackageAction.DO };
			createSubMenu(menu, feature, filterActions(getAllSupportedActions((MPackageImpl) selectedEObject, feature, MPackageAction.class), Arrays.asList(excluded)));
			return;
		}
		if (selectedEObject instanceof MPropertyImpl) {
			EAttribute feature = McorePackage.Literals.MPROPERTY__DO_ACTION;
			MPropertyAction[] excluded = new MPropertyAction[] { MPropertyAction.DO };
			List<MPropertyAction> calculatedList = ((MPropertyImpl) selectedEObject)
					.evalDoActionChoiceConstruction(getAllSupportedActions((MPropertyImpl) selectedEObject, feature, MPropertyAction.class));
			createSubMenu(menu, feature, filterActions(calculatedList, Arrays.asList(excluded)));
			return;
		}
		if (selectedEObject instanceof MPropertyInstanceImpl) {
			EAttribute feature = ObjectsPackage.Literals.MPROPERTY_INSTANCE__DO_ACTION;
			MPropertyInstanceAction[] excluded = new MPropertyInstanceAction[] { MPropertyInstanceAction.DO };
			List<MPropertyInstanceAction> calculatedList = ((MPropertyInstanceImpl) selectedEObject)
					.evalDoActionChoiceConstruction(getAllSupportedActions((MPropertyInstanceImpl) selectedEObject, feature, MPropertyInstanceAction.class));
			createSubMenu(menu, feature, filterActions(calculatedList, Arrays.asList(excluded)));
			return;
		}
	}

	private <T> List<T> filterActions(List<T> input, List<T> excluded) {
		List<T> result = new ArrayList<T>();
		for (T nextInput : input) {
			if (excluded.contains(nextInput)) {
				continue;
			}
			result.add(nextInput);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private <T> List<T> getAllSupportedActions(EObject selection, EAttribute feature, Class<T> classT) {
		EGenericType eGenericType = selection.eClass().getFeatureType(feature);
		EClassifier eType = eGenericType.getERawType();
		if (false == eType instanceof EEnum) {
			return Collections.emptyList();
		}
		EEnum eEnum = (EEnum) eType;
		List<T> enumerators = new ArrayList<T>();
		for (EEnumLiteral eEnumLiteral : eEnum.getELiterals()) {
			enumerators.add((T) eEnumLiteral.getInstance());
		}
		return enumerators;
	}

	private void createSubMenu(Menu menu, EAttribute feature, List<?> enumerations) {
		createSubMenuItems(menu, feature, enumerations);
	}

	private void createSubMenuItems(Menu root, final EAttribute feature, List<?> enumerations) {
		for (final Object nextObject : enumerations) {
			if (false == nextObject instanceof Enumerator) {
				continue;
			}
			final Enumerator nextAction = (Enumerator) nextObject;
			MenuItem nextActionMenuItem = new MenuItem(root, SWT.PUSH);
			nextActionMenuItem.setText(nextAction.getLiteral());
			nextActionMenuItem.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					doActionExecute(feature, nextAction);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// TODO Auto-generated method stub
				}
			});
		}
	}

	private void doActionExecute(EAttribute feature, Enumerator value) {
		Object element = getSelectedElement();
		if (!(element instanceof IGraphicalEditPart)) {
			return;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) element;
		editPart.getEditingDomain().getCommandStack().execute(getCommand(editPart, feature, value));
	}

	public Command getCommand(IGraphicalEditPart editPart, EAttribute feature, Enumerator value) {
		TransactionalEditingDomain editingDomain = editPart.getEditingDomain();
		CompositeTransactionalCommand cc = new CompositeTransactionalCommand(editingDomain, DiagramUIMessages.AddCommand_Label);
		Command doActionCommand = editPart.getEditingDomain().createCommand(SetCommand.class, new CommandParameter(editPart.resolveSemanticElement(), feature, value, -1));
		ICommand iCommandDoAction = wrapICommand(doActionCommand);
		CompositeCommand composite = new CompositeCommand("");//$NON-NLS-1$
		composite.add(EMFtoGMFCommandWrapper.wrap(doActionCommand));
		if (iCommandDoAction != null) {
			composite.add(new DoActionResultCreateViewCommand(iCommandDoAction, findDiagramEditPart(editPart)) {

				@Override
				protected Collection<?> getPreviusCommandResult() {
					EMFtoGMFCommandWrapper previousCommand = (EMFtoGMFCommandWrapper) getPreviousCommand();
					Command previous = previousCommand.getEMFCommand();
					if (previous instanceof CompoundCommand) {
						CompoundCommand compound = (CompoundCommand)previous;
						if (!compound.isEmpty()) {
							Set<Object> result = new HashSet<Object>();
							for (Command cmd : compound.getCommandList()) {
								if (false == cmd instanceof SetCommand && false == cmd instanceof ChangeCommandWithOperationCall) {
									Collection<?> affectedObjects = cmd.getAffectedObjects();
									if (affectedObjects != null) {
										result.addAll(affectedObjects);
									}
								}
							}
							return result;
						}
					}
					return previous.getAffectedObjects();
				}
			});
		}
		cc.compose(composite);
		return GMFtoEMFCommandWrapper.wrap(cc.reduce());
	}

	private DiagramEditPart findDiagramEditPart(EditPart host) {
		if (host == null) {
			return null;
		}
		if (host instanceof DiagramEditPart) {
			return (DiagramEditPart) host;
		}
		return (DiagramEditPart)((IGraphicalEditPart)host).getRoot().getChildren().get(0);
	}

	private ICommand wrapICommand(Command doActionCommand) {
		if (doActionCommand == null) {
			return null;
		}
		return EMFtoGMFCommandWrapper.wrap(doActionCommand);
	}

	private Object getSelectedElement() {
		ISelection selection = getSelection();
		if (selection instanceof IStructuredSelection) {
			return ((IStructuredSelection) selection).getFirstElement();
		}
		return null;
	}

	private ISelection getSelection() {
		if (myServiceLocator == null) {
			return null;
		}
		ISelectionService selectionService = (ISelectionService) myServiceLocator.getService(ISelectionService.class);
		return selectionService == null ? null : selectionService.getSelection();
	}

	@Override
	public void initialize(IServiceLocator serviceLocator) {
		myServiceLocator = serviceLocator;
	}
}