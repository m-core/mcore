package com.montages.mcore.diagram.providers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;

import com.montages.mcore.diagram.actions.AbstractShowHideAction.EditPartRepresentation;
import com.montages.mcore.diagram.parsers.SlotParser;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.objects.MPropertyInstance;

public class EditPartRepresentationLabelProvider extends LabelProvider {

	private static final String ITEM_PROVIDER_ADAPTER_FACTORIES = "org.eclipse.emf.edit.itemProviderAdapterFactories"; //$NON-NLS-1$

	private static final String IITEM_LABEL_PROVIDER = "org.eclipse.emf.edit.provider.IItemLabelProvider"; //$NON-NLS-1$

	private static final Map<String, AdapterFactory> FACTORIES = new HashMap<String, AdapterFactory>();

	public EditPartRepresentationLabelProvider() {
	}

	@Override
	public String getText(Object element) {
		if (element instanceof EditPartRepresentation) {
			return ((EditPartRepresentation) element).getLabel();
		}
		if (element instanceof MPropertyInstance) {
			return new SlotParser().getPrintString((MPropertyInstance) element);
		}
		if (element instanceof EObject) {
			return getText((EObject) element);
		}
		return element.toString();
	}

	private String getText(EObject eObject) {
		IItemLabelProvider itemLabelProvider = getItemLabelProvider(eObject);
		if (itemLabelProvider != null) {
			return itemLabelProvider.getText(eObject);
		}
		return "";
	}

	private IItemLabelProvider getItemLabelProvider(EObject eObject) {
		if (eObject == null) {
			return null;
		}
		AdapterFactory adapterFactory = getEditFactory(eObject);
		if (adapterFactory != null) {
			return (IItemLabelProvider) adapterFactory.adapt(eObject, IItemLabelProvider.class);
		}
		return null;
	}

	private AdapterFactory getEditFactory(EObject eobject) {
		String uri = eobject.eClass().getEPackage().getNsURI();
		AdapterFactory factory = FACTORIES.get(uri);
		if (factory != null) {
			return factory;
		}
		IConfigurationElement[] extensions = Platform.getExtensionRegistry().getConfigurationElementsFor(ITEM_PROVIDER_ADAPTER_FACTORIES);
		for (IConfigurationElement nextExtension : extensions) {
			if (!uri.equals(nextExtension.getAttribute("uri"))) { //$NON-NLS-1$
				continue;
			}
			String supportedTypes = nextExtension.getAttribute("supportedTypes"); //$NON-NLS-1$
			List<String> types = Arrays.asList(supportedTypes.split("\\s+")); //$NON-NLS-1$
			if (!types.contains(IITEM_LABEL_PROVIDER)) {
				continue;
			}
			try {
				factory = (AdapterFactory) nextExtension.createExecutableExtension("class"); //$NON-NLS-1$
				if (factory != null) {
					FACTORIES.put(uri, factory);
				}
			} catch (CoreException e) {
				McoreDiagramEditorPlugin.getInstance().getLogHelper().logError("Can't create factory.", e); //$NON-NLS-1$
			}
		}
		return factory;
	}
}