package com.montages.mcore.diagram.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;

public class CreateLinkCommandWithEnds extends CreateCommand {

	private Object mySource;

	private Object myTarget;

	public CreateLinkCommandWithEnds(TransactionalEditingDomain editingDomain, ViewDescriptor viewDescriptor, View diagram) {
		super(editingDomain, viewDescriptor, diagram);
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		super.doExecuteWithResult(monitor, info);
		Edge link = (Edge) getViewDescriptor().getAdapter(View.class);
		link.setSource(getSourceView());
		link.setTarget(getTargetView());
		return CommandResult.newOKCommandResult(link);
	}

	@Override
	public boolean canExecute() {
		return super.canExecute() && mySource != null && myTarget != null;
	}

	public View getSourceView() {
		if (mySource instanceof View) {
			return (View) mySource;
		}
		if (mySource instanceof IGraphicalEditPart) {
			return ((IGraphicalEditPart) mySource).getNotationView();
		}
		if (mySource instanceof ViewDescriptor) {
			return (View) ((ViewDescriptor) mySource).getAdapter(View.class);
		}
		return null;
	}

	public View getTargetView() {
		if (myTarget instanceof View) {
			return (View) myTarget;
		}
		if (myTarget instanceof IGraphicalEditPart) {
			return ((IGraphicalEditPart) myTarget).getNotationView();
		}
		if (myTarget instanceof ViewDescriptor) {
			return (View) ((ViewDescriptor) myTarget).getAdapter(View.class);
		}
		return null;
	}

	public void setSource(Object source) {
		mySource = source;
	}

	public void setTarget(Object target) {
		myTarget = target;
	}
}
