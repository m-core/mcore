package com.montages.mcore.diagram.commands;

import static com.montages.mcore.diagram.commands.CommandUtils.getLinkDescriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.diagram.edit.policies.PolicyUtils;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.objects.MObject;

public class CreateClassifierViewCommand extends CreateCommand {

	private Stack<View> children = new Stack<View>();

	public CreateClassifierViewCommand(TransactionalEditingDomain editingDomain, ViewDescriptor viewDescriptor, View containerView) {
		super(editingDomain, viewDescriptor, containerView);
	}

	@Override
	protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IStatus status = super.doExecute(monitor, info);

		if (!status.isOK()) {
			return status;
		}

		MClassifier mClassifier = getViewDescriptor().getElementAdapter().getAdapter(MClassifier.class);
		if (mClassifier == null) {
			return Status.OK_STATUS;
		}

		PreferencesHint hint = getViewDescriptor().getPreferencesHint();
		for (MProperty mProperty : collectReferences(mClassifier, containerView)) {
			if (mProperty.getType() != null) {
				IElementType type = null;
				if (mProperty.getContainment() != null && mProperty.getContainment().booleanValue()) {
					type = McoreElementTypes.MProperty_4006;
				} else {
					type = McoreElementTypes.MProperty_4003;
				}
				ViewDescriptor linkDescriptor = getLinkDescriptor(hint, mProperty, type);
				CreateLinkCommandWithEnds createLink = new CreateLinkCommandWithEnds(getEditingDomain(), linkDescriptor, containerView);
				MClassifier referenceEnd = mProperty.getType();
				Object source = null;
				Object target = null;
				if (referenceEnd == mClassifier) {
					source = PolicyUtils.findView(mProperty.getContainingClassifier(), containerView).get(0);
					target = getViewDescriptor();
				} else {
					source = getViewDescriptor();
					target = PolicyUtils.findView(mProperty.getType(), containerView).get(0);
				}
				createLink.setSource(source);
				createLink.setTarget(target);
				if (createLink.canExecute()) {
					try {
						createLink.execute(monitor, info);
					} finally {
						View edge = (View)linkDescriptor.getAdapter(View.class);
						if (edge != null) {
							children.push(edge);
						}
					}
				}
			}
		}

		for (MObject mObject : mClassifier.getInstance()) {
			List<View> targetViews = PolicyUtils.findView(mObject, containerView);
			if (!targetViews.isEmpty()) {
				ViewDescriptor linkDescriptor = getLinkDescriptor(hint, null, McoreElementTypes.MObjectType_4001);
				CreateLinkCommandWithEnds createLink = new CreateLinkCommandWithEnds(getEditingDomain(), linkDescriptor, containerView);
				createLink.setSource(targetViews.get(0));
				createLink.setTarget(getViewDescriptor());
				try {
					createLink.execute(monitor, info);
				} finally {
					View edge = (View)linkDescriptor.getAdapter(View.class);
					if (edge != null) {
						children.push(edge);
					}
				}
			}
		}

		return Status.OK_STATUS;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected IStatus doUndo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		((Diagram)getContainerView()).getChildren().removeAll(children);
		children.clear();
		return super.doUndo(monitor, info);
	}

	public static List<MProperty> collectReferences(MClassifier elements, View parent) {
		List<MProperty> properties = new ArrayList<MProperty>();
		properties.addAll(filterReferenceProperties(elements.getProperty(), properties, parent));
		properties.addAll(collectProperties(elements.getPropertyGroup(), properties, parent));
		properties.addAll(collectReferencesTo(elements, parent));
		return properties;
	}

	public static List<MProperty> collectProperties(List<MPropertiesGroup> groups, List<MProperty> result, View parent) {
		List<MProperty> properties = new ArrayList<MProperty>();
		for (MPropertiesGroup group : groups) {
			properties.addAll(filterReferenceProperties(group.getProperty(), result, parent));
			properties.addAll(collectProperties(group.getPropertyGroup(), result, parent));
		}
		return properties;
	}

	public static List<MProperty> collectReferencesTo(MClassifier element, View parent) {
		ArrayList<MProperty> result = new ArrayList<MProperty>();
		for (Iterator<EObject> it = element.getContainingPackage().getContainingComponent().eAllContents(); it.hasNext();) {
			EObject next = it.next();
			if (next instanceof MProperty) {
				MProperty mProperty = (MProperty) next;
				if (mProperty.getType() == element && !PolicyUtils.findView(mProperty.getContainingClassifier(), parent).isEmpty()) {
					result.add(mProperty);
				}
			}
		}
		return result;
	}

	public static List<MProperty> filterReferenceProperties(List<MProperty> properties, List<MProperty> resultReferences, View parent) {
		ArrayList<MProperty> result = new ArrayList<MProperty>();
		for (MProperty property : properties) {
			MClassifier type = property.getType();
			if (type != null && !PolicyUtils.findView(type, parent).isEmpty()) {
				resultReferences.add(property);
			}
		}
		return result;
	}
}
