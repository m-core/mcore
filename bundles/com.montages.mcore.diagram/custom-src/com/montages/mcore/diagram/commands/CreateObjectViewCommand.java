package com.montages.mcore.diagram.commands;

import static com.montages.mcore.diagram.commands.CommandUtils.getLinkDescriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.diagram.edit.policies.CustomMPackageCanonicalEditPolicy;
import com.montages.mcore.diagram.edit.policies.PolicyUtils;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;

public class CreateObjectViewCommand extends CreateCommand {

	private Stack<View> children = new Stack<View>();

	public CreateObjectViewCommand(TransactionalEditingDomain editingDomain, ViewDescriptor viewDescriptor,
			View containerView) {
		super(editingDomain, viewDescriptor, containerView);
	}

	@Override
	protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IStatus status = super.doExecute(monitor, info);

		if (!status.isOK()) {
			return status;
		}

		MObject mObject = getViewDescriptor().getElementAdapter().getAdapter(MObject.class);
		if (mObject == null) {
			return Status.OK_STATUS;
		}
		PreferencesHint preferencesHint = getViewDescriptor().getPreferencesHint();

		//create type link
		if (mObject.getType() != null) {
			List<View> targetViews = PolicyUtils.findView(mObject.getType(), containerView);
			if (!targetViews.isEmpty()) {
				ViewDescriptor linkDescriptor = getLinkDescriptor(preferencesHint, null, McoreElementTypes.MObjectType_4001);
				CreateLinkCommandWithEnds createLink = new CreateLinkCommandWithEnds(getEditingDomain(), linkDescriptor, containerView);
				createLink.setSource(getViewDescriptor());
				createLink.setTarget(targetViews.get(0));
				executeChildCreateCommand(createLink, linkDescriptor, monitor, info);
			}
		}

		//create containment link
		List<View> containingMObjectViews = PolicyUtils.findView(mObject.getContainingObject(), containerView);
		if (!containingMObjectViews.isEmpty()) {
			MPropertyInstance instance = null;
			for (MPropertyInstance propertyInstance: mObject.getContainingObject().getPropertyInstance()) {
				if (propertyInstance.getInternalContainedObject().contains(mObject)) {
					instance = propertyInstance;
					break;
				}
			}
			if (instance != null) {
				for (View containingMObjetView : containingMObjectViews) {
					ViewDescriptor containingLinkDescr = getLinkDescriptor(preferencesHint, instance, McoreElementTypes.MPropertyInstance_4005);
					CreateLinkCommandWithEnds createContainmentLink = new CreateLinkCommandWithEnds(getEditingDomain(), containingLinkDescr, containerView);
					createContainmentLink.setTarget(getViewDescriptor());
					createContainmentLink.setSource(containingMObjetView);
					executeChildCreateCommand(createContainmentLink, containingLinkDescr, monitor, info);
				}
			}
		}

		// create containment references
		for (MPropertyInstance mPropertyInstance: collectReferencesTo(mObject, containerView)) {
			List<View> referencesTo = PolicyUtils.findView(mPropertyInstance.getContainingObject(), containerView);
			for (View referenceTo : referencesTo) {
				ViewDescriptor linkDescriptor = getLinkDescriptor(preferencesHint, mPropertyInstance, McoreElementTypes.MPropertyInstance_4005);
				CreateLinkCommandWithEnds createLink = new CreateLinkCommandWithEnds(getEditingDomain(), linkDescriptor, containerView);
				createLink.setTarget(getViewDescriptor());
				createLink.setSource(referenceTo);
				executeChildCreateCommand(createLink, linkDescriptor, monitor, info);
			}
		}

		//create references
		for (MPropertyInstance mPropertyInstance : mObject.getPropertyInstance()) {

			for (MObject containedMObject: mPropertyInstance.getInternalContainedObject()) {
				List<View> targets = PolicyUtils.findView(containedMObject, containerView);
				for (View target : targets) {
					ViewDescriptor linkDescriptor = getLinkDescriptor(preferencesHint, mPropertyInstance, McoreElementTypes.MPropertyInstance_4005);
					CreateLinkCommandWithEnds createLink = new CreateLinkCommandWithEnds(getEditingDomain(), linkDescriptor, containerView);
					createLink.setSource(getViewDescriptor());
					createLink.setTarget(target);
					executeChildCreateCommand(createLink, linkDescriptor, monitor, info);
				}
			}

			for (MObjectReference mObjectReference: mPropertyInstance.getInternalReferencedObject()) {
				MObject referencedObject = mObjectReference.getReferencedObject();
				if (referencedObject != null) {
					List<View> targets = PolicyUtils.findView(referencedObject, containerView);
					for (View target : targets) {
						ViewDescriptor linkDescriptor = getLinkDescriptor(preferencesHint, mPropertyInstance, McoreElementTypes.MPropertyInstance_4005);
						CreateLinkCommandWithEnds createLink = new CreateLinkCommandWithEnds(getEditingDomain(), linkDescriptor, containerView);
						createLink.setSource(getViewDescriptor());
						createLink.setTarget(target);
						executeChildCreateCommand(createLink, linkDescriptor, monitor, info);
					}
				}
			}
		}

		return Status.OK_STATUS;
	}

	private List<MPropertyInstance> collectReferencesTo(MObject mObject, View container) {
		ArrayList<MPropertyInstance> result = new ArrayList<MPropertyInstance>();

		for (Iterator<EObject> it = mObject.getResource().getContainingFolder().getRootFolder().eAllContents(); it.hasNext();) {
			EObject next = it.next();

			if (next instanceof MPropertyInstance) {
				MPropertyInstance mPropertyInstance = (MPropertyInstance) next;
				MObject containingObject = mPropertyInstance.getContainingObject();
				if (CustomMPackageCanonicalEditPolicy.isReferenceToObject(mPropertyInstance, mObject) && !PolicyUtils.findView(containingObject, container).isEmpty()) {
					result.add(mPropertyInstance);
				}
			}
		}

		return result;
	}

	private void executeChildCreateCommand(CreateCommand cmd, ViewDescriptor descr, IProgressMonitor monitor, IAdaptable info) {
		try {
			if (cmd.canExecute()) {
				cmd.execute(monitor, info);
			}
		} catch (ExecutionException e) {
			e.printStackTrace();
		} finally {
			View result = (View) descr.getAdapter(View.class);
			if (result != null) {
				children.push(result);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected IStatus doUndo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
//		((Diagram)getContainerView()).getChildren().removeAll(children);
//		children.clear();
		return super.doUndo(monitor, info);
	}
}
