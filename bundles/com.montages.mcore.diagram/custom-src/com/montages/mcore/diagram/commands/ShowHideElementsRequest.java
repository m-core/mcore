package com.montages.mcore.diagram.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.notation.View;

public class ShowHideElementsRequest extends Request {

	public static final String SHOW_HIDE_ELEMENTS = "Show/Hide elements";

	private final EditPart myEditPartForHiding;

	private final View myContainerView;

	private final EObject mySemanticElement;

	private Point myLocation;

	public ShowHideElementsRequest(EditPart ep) {
		this(ep, null, null);
	}

	public ShowHideElementsRequest(EditPart ep, View container, EObject semanticElement) {
		super(SHOW_HIDE_ELEMENTS);
		myEditPartForHiding = ep;
		myContainerView = container;
		mySemanticElement = semanticElement;
	}

	public EditPart getHidedEditPart() {
		return myEditPartForHiding;
	}

	public View getContainer() {
		return myContainerView;
	}

	public EObject getSemanticElement() {
		return mySemanticElement;
	}

	public void setLocation(Point location) {
		myLocation = location;
	}

	public Point getLocation() {
		return myLocation;
	}
}