package com.montages.mcore.diagram.tools;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.celleditor.FeatureEditorDialog;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Widget;

import com.montages.mcore.diagram.parsers.IChoiceParserEx;
import com.montages.mcore.objects.ObjectsPackage;

public class CheckDirectEditManager extends org.eclipse.gmf.runtime.diagram.ui.tools.DirectEditManagerBase {

	public CheckDirectEditManager(GraphicalEditPart source, Class editorType, CellEditorLocator locator) {
		super(source, editorType, locator);
	}

	@Override
	protected CellEditor doCreateCellEditorOn(Composite composite) {
		
		final ITextAwareEditPart textEditPart = (ITextAwareEditPart) getEditPart();
		final IChoiceParserEx parser = (IChoiceParserEx)textEditPart.getParser();
		final EObject parserElement = ((IGraphicalEditPart) getEditPart()).resolveSemanticElement();
		final EObjectAdapter element = new EObjectAdapter(parserElement);
		final List<String> editChoices = parser.getEditChoices(element);
		final List<String> selectedChoices = parser.getSelectedChoises(element);

		return new ExtendedDialogCellEditor(composite, new LabelProvider()) {

			private Button b;

			private Control c;

			@Override
			public void setFocus() {
				super.setFocus();
				if (b != null) {
					Listener[] ls = b.getListeners(SWT.FocusIn);

					for (Listener l : ls) {
						b.removeListener(SWT.FocusIn, l);
						b.removeListener(SWT.FocusOut, l);
					}

					Object newValue = openDialogBox(c);

					for (Listener l : ls) {
						b.addListener(SWT.FocusIn, l);
						b.addListener(SWT.FocusOut, l);
					}
					if (newValue != null) {
						boolean newValidState = isCorrect(newValue);
						if (newValidState) {
							markDirty();
							doSetValue(newValue);
						} else {
							// try to insert the current value into the error message.
							setErrorMessage(MessageFormat.format(getErrorMessage(), new Object[] { newValue.toString() }));
						}
					}
				}
			}

			@Override
			protected Control createControl(Composite parent) {
				c = super.createControl(parent);
				return c;
			}

			@Override
			protected Button createButton(Composite parent) {
				b = super.createButton(parent);
				return b;
			}

			@Override
			protected Object openDialogBox(Control cellEditorWindow) {
				FeatureEditorDialog dialog = new FeatureEditorDialog(//
						cellEditorWindow.getShell(), //
						new LabelProvider(), //
						parserElement, //
						ObjectsPackage.eINSTANCE.getMPropertyInstance(), //
						selectedChoices, //
						"Select elements", //
						new ArrayList<Object>(editChoices), //
						false, //
						true, //
						true);//
				if(Dialog.OK == dialog.open()) {
					setDirty(true);
					List<?> result = dialog.getResult();
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i <result.size(); i++) {
						if (i > 0) {
							sb.append(",");
						}
						sb.append(result.get(i).toString());
					}
					return sb.toString();
				}
				
				return null;
			}
		};
	}

	
	@Override
	protected void initCellEditor() {
		super.initCellEditor();
		setEditText("Edit dialog");
	}

	@Override
	protected void createContentAssistant(Control control, Color proposalPopupForegroundColor, Color proposalPopupBackgroundColor, IContentAssistProcessor processor) {
	}

	private static class LabelProvider implements ILabelProvider {

		@Override
		public void addListener(ILabelProviderListener listener) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
		}

		@Override
		public Image getImage(Object element) {
			return null;
		}

		@Override
		public String getText(Object element) {
			return element == null ? "" : element.toString();
		}
		
	}
}
