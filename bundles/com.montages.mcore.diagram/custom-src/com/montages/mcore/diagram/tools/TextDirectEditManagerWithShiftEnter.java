package com.montages.mcore.diagram.tools;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.label.ILabelDelegate;
import org.eclipse.gmf.runtime.diagram.ui.tools.TextDirectEditManager;
import org.eclipse.gmf.runtime.gef.ui.internal.parts.TextCellEditorEx;
import org.eclipse.gmf.runtime.gef.ui.internal.parts.WrapTextCellEditor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Composite;

public class TextDirectEditManagerWithShiftEnter extends TextDirectEditManager {

	public TextDirectEditManagerWithShiftEnter(GraphicalEditPart source, Class editorType, CellEditorLocator locator) {
		super(source, editorType, locator);
	}

	@SuppressWarnings("restriction")
	@Override
	protected CellEditor doCreateCellEditorOn(Composite composite) {
		ILabelDelegate label = (ILabelDelegate) getEditPart().getAdapter(ILabelDelegate.class);
		if (label != null && label.isTextWrapOn()) {
			int style = SWT.WRAP | SWT.MULTI;
			switch (label.getTextJustification()) {
			case PositionConstants.LEFT:
				style = style | SWT.LEAD;
				break;
			case PositionConstants.RIGHT:
				style = style | SWT.TRAIL;
				break;
			case PositionConstants.CENTER:
				style = style | SWT.CENTER;
				break;
			default:
				break;
			}
			return new WrapTextCellEditor(composite, style) {

				@Override
				protected void keyReleaseOccured(KeyEvent keyEvent) {
					/**
					 * SCHIFT+CTRL key is used to insert a new line in the multiline editor
					 */
					if (keyEvent.character == '\r') {
						if (text != null && !text.isDisposed() && (text.getStyle() & SWT.MULTI) != 0) {
							if ((keyEvent.stateMask & SWT.SHIFT) != 0) {
								keyEvent.stateMask &= ~SWT.CTRL;
							} else {
								keyEvent.stateMask |= SWT.SHIFT;
								super.keyReleaseOccured(keyEvent);
							}
						}
						return;
					}

					super.keyReleaseOccured(keyEvent);
				}
			};
		} else {
			return new TextCellEditorEx(composite);
		}
	}
}
