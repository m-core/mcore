package com.montages.mcore.diagram.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramRootEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramGraphicalViewer;
import org.eclipse.gmf.runtime.emf.core.util.EMFCoreUtil;
import org.eclipse.gmf.runtime.notation.CanonicalStyle;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Different utility methods to manage and manipulate edit parts in diagrams.
 */
public class DiagramEditPartsUtil {

	/**
	 * Returns the edit part that controls the given view.
	 *
	 * @param view
	 *            the view for which the edit part should be found. This should not be <code>null</code>
	 * @param anyEditPart
	 *            any edit part from which to get the edit part registry
	 *
	 * @return the edit part that controls the given view or <code>null</code> if none was found
	 */
	public static EditPart getEditPartFromView(View view, EditPart anyEditPart) {
		if (view != null && anyEditPart != null) {
			return (EditPart) anyEditPart.getViewer().getEditPartRegistry().get(view);
		}
		return null;
	}

	/**
	 * Gets the diagram edit part.
	 *
	 * @param editPart
	 *            the edit part
	 *
	 * @return the diagram edit part
	 */
	public static DiagramEditPart getDiagramEditPart(EditPart editPart) {
		if (editPart == null) {
			return null;
		}

		if (editPart instanceof IGraphicalEditPart) {
			IGraphicalEditPart graphicalEditPart = (IGraphicalEditPart) editPart;
			View view = graphicalEditPart.getNotationView();
			Diagram diagram = view.getDiagram();
			Object object = graphicalEditPart.getViewer().getEditPartRegistry().get(diagram);
			if (object instanceof DiagramEditPart) {
				return (DiagramEditPart) object;
			}
		}

		if (editPart instanceof DiagramEditPart) {
			return (DiagramEditPart) editPart;
		}

		EditPart actual = editPart;
		EditPart parent = null;
		while ((parent = actual.getParent()) != null) {
			if (parent instanceof DiagramEditPart) {
				return (DiagramEditPart) parent;
			} else {
				actual = parent;
			}
		}

		return null;
	}

	/**
	 *
	 * @param ep
	 *            an edit part
	 * @return
	 *         all children edit part which are "top" semantic edit part
	 */
	public static Collection<EditPart> getAllTopSemanticEditPart(final EditPart ep) {
		final Collection<EditPart> editparts = new HashSet<EditPart>();
		for (final Object current : ep.getChildren()) {
			if (current instanceof EditPart) {
				editparts.addAll(getAllTopSemanticEditPart((EditPart) current));
				final EditPart topEP = getTopSemanticEditPart((EditPart) current);
				if (topEP != null) {
					editparts.add(topEP);
				}
			}
		}
		return editparts;
	}

	/**
	 *
	 * @param ep
	 *            an editpart
	 * @return
	 *         the top edit part representing the same eobject or <code>null</code> if ep doesn't represent an editpart
	 */
	public static final EditPart getTopSemanticEditPart(final EditPart ep) {
		final EObject currentEObject = (EObject) ep.getAdapter(EObject.class);
		if (currentEObject != null) {
			EditPart previousParent = ep;
			EditPart parent = ep;
			while (parent != null) {
				if (parent.getAdapter(EObject.class) != currentEObject || parent instanceof DiagramEditPart) {
					return previousParent;
				}
				previousParent = parent;
				parent = parent.getParent();
			}
			return previousParent;
		}
		return null;
	}

	/**
	 *
	 * @param anEditPart
	 *            an edit part
	 * @return
	 *         the preference store for the diagram owning this edit part or <code>null</code> if not found
	 *
	 */
	public static final IPreferenceStore getDiagramWorkspacePreferenceStore(final EditPart anEditPart) {
		final EditPartViewer viewer = anEditPart.getViewer();
		if (viewer instanceof DiagramGraphicalViewer) {
			return ((DiagramGraphicalViewer) viewer).getWorkspaceViewerPreferenceStore();
		}
		return null;
	}

	/**
	 *
	 * @param anEditPart
	 *            an edit part
	 * @return
	 *         the value of the grid spacing or -1 if not found
	 */
	public static final double getDiagramGridSpacing(final EditPart anEditPart) {
		final RootEditPart rootEP = anEditPart.getRoot();
		if (rootEP instanceof DiagramRootEditPart) {
			return ((DiagramRootEditPart) rootEP).getGridSpacing();
		}
		return -1.0;
	}

	/**
	 * This Method return the Graphical container of an EditPart.
	 * Depending on the type of EditPart, the container can be the Direct Parent or the grand parent.
	 * 
	 * @param currentEP
	 * @return
	 */
	public static final EditPart getContainerEditPart(GraphicalEditPart currentEP) {

		EditPart container;
		EditPart parent = currentEP.getParent();
		if (parent instanceof AbstractConnectionEditPart) {
			container = parent.getParent();
		} else if (parent instanceof AbstractBorderItemEditPart) {
			container = parent.getParent().getParent();
		} else if (currentEP instanceof AbstractBorderItemEditPart) {
			container = parent.getParent();
		} else {
			container = parent;
		}

		return container;
	}

	/*
	 * @param anEditPart
	 * an edit part
	 * 
	 * @return
	 * the zoom level in the diagram or 1.0 when {@link ZoomManager} has not been found
	 */

	public static final double getDiagramZoomLevel(final EditPart anEditPart) {

		final RootEditPart rootEP = anEditPart.getRoot();
		if (rootEP instanceof DiagramRootEditPart) {
			final ZoomManager zoomManager = ((DiagramRootEditPart) rootEP).getZoomManager();
			if (zoomManager != null) {
				return zoomManager.getZoom();
			}
		}
		return 1.0;
	}

	/** The Constant BelongToDiagramSource. */
	// @unused
	public static final String BelongToDiagramSource = "es.cv.gvcase.mdt.uml2.diagram.common.Belongs_To_This_Diagram";

	/** EAnnotation Source for diagrams that grow from this a view. */
	// @unused
	public static final String DiagramsRelatedToElement = "es.cv.gvcase.mdt.uml2.diagram.common.DiagramsRelatedToElement";

	/**
	 * Gets a list of all GraphicalEditParts in the diagram of the given
	 * EditPart.
	 *
	 * @param editPart
	 *            Any <code>EditPart</code> in the diagram. The TopGraphicalNode
	 *            will be found from this.
	 *
	 * @return a list containing all <code>GraphicalEditPart</code> in the
	 *         diagram.
	 */
	// @unused
	public static List<IGraphicalEditPart> getAllEditParts(EditPart editPart) {

		if (editPart == null) {
			return null;
		}

		EditPart topEditPart = getTopMostEditPart(editPart);

		List<IGraphicalEditPart> editParts = new ArrayList<IGraphicalEditPart>();

		if (editPart instanceof IGraphicalEditPart) {
			editParts.add((IGraphicalEditPart) editPart);
		}
		addEditPartGraphicalChildren(editPart, editParts);

		return editParts;
	}

	/**
	 * Returns the upper most EditPart in the hierarchy of the given EditPart.
	 *
	 * @param editPart
	 *            A non-null EditPart
	 *
	 * @return The upper most EditPart in the hierarchy; may be the same
	 *         EditPart
	 */
	public static EditPart getTopMostEditPart(EditPart editPart) {

		if (editPart == null) {
			return null;
		}

		EditPart actual, parent;

		actual = editPart;

		while ((parent = actual.getParent()) != null) {
			actual = parent;
		}

		return actual;
	}

	/**
	 * Handle notification for diagram.
	 *
	 * @param editPart
	 *            the edit part
	 * @param notification
	 *            the notification
	 * @param features
	 *            the features
	 */
	// @unused
	public static void handleNotificationForDiagram(IGraphicalEditPart editPart, Notification notification, List<EStructuralFeature> features) {
		EObject element = editPart.resolveSemanticElement();
		Object notifier = notification.getNotifier();
		Object feature = notification.getFeature();
		Object oldValue = notification.getOldValue();
		Object newValue = notification.getNewValue();
		if (notifier != null && notifier == element) {
			if (feature != null && oldValue != null && oldValue != newValue && features.contains(feature)) {
				DiagramEditPartsUtil.updateDiagram(editPart);
			}

		}
	}

	/**
	 * Handle notification for view.
	 *
	 * @param editPart
	 *            the edit part
	 * @param notification
	 *            the notification
	 * @param features
	 *            the features
	 */
	// @unused
	public static void handleNotificationForView(IGraphicalEditPart editPart, Notification notification, List<EStructuralFeature> features) {
		EObject element = editPart.resolveSemanticElement();
		Object notifier = notification.getNotifier();
		Object feature = notification.getFeature();
		Object oldValue = notification.getOldValue();
		Object newValue = notification.getNewValue();
		if (notifier != null && notifier == element) {
			if (feature != null && oldValue != null && oldValue != newValue && features.contains(feature)) {
				DiagramEditPartsUtil.updateEditPart(editPart);
			}

		}
	}

	/**
	 * Update a <View>.
	 *
	 * @param view
	 *            the view
	 */
	// @unused
	public static void updateDiagram(View view) {
		if (view == null) {
			return;
		}
		view = view.getDiagram();
		if (view == null) {
			return;
		}
		EObject element = view.getElement();
		if (element == null) {
			return;
		}

		List editPolicies = CanonicalEditPolicy.getRegisteredEditPolicies(element);
		for (Iterator it = editPolicies.iterator(); it.hasNext();) {
			CanonicalEditPolicy nextEditPolicy = (CanonicalEditPolicy) it.next();
			nextEditPolicy.refresh();
		}
	}

	/**
	 * Update diagram.
	 *
	 * @param editPart
	 *            any edit part in the <Diagram>
	 */
	public static void updateDiagram(IGraphicalEditPart editPart) {
		if (editPart == null) {
			return;
		}
		View view = editPart.getNotationView();
		if (view == null) {
			return;
		}
		view = view.getDiagram();
		if (view == null) {
			return;
		}
		EObject element = view.getElement();
		if (element == null) {
			return;
		}

		List editPolicies = CanonicalEditPolicy.getRegisteredEditPolicies(element);
		for (Iterator it = editPolicies.iterator(); it.hasNext();) {
			CanonicalEditPolicy nextEditPolicy = (CanonicalEditPolicy) it.next();
			nextEditPolicy.refresh();
		}
	}

	/**
	 * Update EditPart.
	 *
	 * @param editPart
	 *            the edit part
	 */
	public static void updateEditPart(IGraphicalEditPart editPart) {
		if (editPart == null) {
			return;
		}
		View view = editPart.getNotationView();
		if (view == null) {
			return;
		}
		EObject element = view.getElement();
		if (element == null) {
			return;
		}

		List editPolicies = CanonicalEditPolicy.getRegisteredEditPolicies(element);
		for (Iterator it = editPolicies.iterator(); it.hasNext();) {
			CanonicalEditPolicy nextEditPolicy = (CanonicalEditPolicy) it.next();
			nextEditPolicy.refresh();
		}
	}

	/**
	 * Update EditPart and all contained EditPart that share the same type of
	 * model element.
	 *
	 * @param editPart
	 *            the edit part
	 * @param eClass
	 *            the e class
	 */
	public static void updateEditPartAndChildren(IGraphicalEditPart editPart, EClass eClass) {
		if (editPart == null) {
			return;
		}
		View view = editPart.getNotationView();
		if (view == null) {
			return;
		}

		for (Object child : editPart.getChildren()) {
			if (child instanceof IGraphicalEditPart) {
				updateEditPartAndChildren(((IGraphicalEditPart) child), eClass);
			}
		}

		EObject element = view.getElement();
		if (eClass != null && eClass.isInstance(element)) {
			List editPolicies = CanonicalEditPolicy.getRegisteredEditPolicies(element);
			for (Iterator it = editPolicies.iterator(); it.hasNext();) {
				CanonicalEditPolicy nextEditPolicy = (CanonicalEditPolicy) it.next();
				nextEditPolicy.refresh();
			}
		}
	}

	/**
	 * Adds the edit part graphical children.
	 *
	 * @param editPart
	 *            the edit part
	 * @param list
	 *            the list
	 */
	private static void addEditPartGraphicalChildren(EditPart editPart, List<IGraphicalEditPart> list) {

		if (editPart == null) {
			return;
		}

		List<EditPart> children = editPart.getChildren();

		for (EditPart ep : children) {
			if (ep instanceof IGraphicalEditPart) {
				list.add((IGraphicalEditPart) ep);
			}
			addEditPartGraphicalChildren(ep, list);
		}
	}

	// Code extracted from getViewReferers in CanonicalEditPolicy
	/**
	 * Gets the e object views.
	 *
	 * @param element
	 *            the element
	 *
	 * @return the e object views
	 */
	public static List getEObjectViews(EObject element) {
		List views = new ArrayList();
		if (element != null) {
			EReference[] features = { NotationPackage.eINSTANCE.getView_Element() };
			views.addAll(EMFCoreUtil.getReferencers(element, features));
		}
		return views;
	}

	/**
	 * Find the views associated with the given eObject in the viewer
	 *
	 * @param parserElement
	 *            the
	 * @param viewer
	 *            the viewer
	 * @return views found if any
	 */
	public static List<View> findViews(EObject parserElement, EditPartViewer viewer) {
		List<View> modelElements = new ArrayList<View>();
		if (parserElement != null) {
			for (Object ep : viewer.getEditPartRegistry().keySet()) {
				if (ep instanceof View) {
					View view = (View) ep;
					if (parserElement.equals(view.getElement())) {
						modelElements.add(view);
					}
				}
			}
		}
		return modelElements;
	}

	/**
	 * Finds the <EditPart>s for the <EObject>s in the selection.
	 *
	 * @param selection
	 *            the selection
	 * @param viewer
	 *            the viewer
	 *
	 * @return the edits the parts from selection
	 */
	// @unused
	public static List<EditPart> getEditPartsFromSelection(ISelection selection, IDiagramGraphicalViewer viewer) {
		if (selection instanceof StructuredSelection && !selection.isEmpty()) {
			StructuredSelection structuredSelection = (StructuredSelection) selection;
			// look for Views of the EObjects in the selection
			List<View> views = new ArrayList<View>();
			for (Object o : structuredSelection.toList()) {
				if (o instanceof EObject) {
					List referencerViews = getEObjectViews((EObject) o);
					for (Object ro : referencerViews) {
						if (ro instanceof View) {
							views.add((View) ro);
						}
					}
				}
			}
			if (!views.isEmpty()) {
				List<EditPart> editParts = new ArrayList<EditPart>();
				for (View view : views) {
					Object ep = viewer.getEditPartRegistry().get(view);
					if (ep instanceof EditPart) {
						editParts.add((EditPart) ep);
					}
				}
				if (!editParts.isEmpty()) {
					return editParts;
				}
			}
		}
		return Collections.EMPTY_LIST;
	}

	// Code extracted from PackageCanonicalEditPolicy

	// *****************************************//

	// ********************************************//

	/**
	 * Find diagram from plugin.
	 *
	 * @param plugin
	 *            the plugin
	 *
	 * @return the diagram
	 */
	public static Diagram findDiagramFromPlugin(AbstractUIPlugin plugin) {
		IEditorPart editor = plugin.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();

		if (editor instanceof DiagramEditor) {
			return ((DiagramEditor) editor).getDiagram();
		}

		return null;
	}

	/**
	 * Find diagram from edit part.
	 *
	 * @param editPart
	 *            the edit part
	 *
	 * @return the diagram
	 */
	public static Diagram findDiagramFromEditPart(EditPart editPart) {
		Object object = editPart.getModel();

		if (object instanceof View) {
			return ((View) object).getDiagram();
		}

		return null;
	}

	// **//

	/**
	 * Refresh i text aware edit parts.
	 *
	 * @param editPart
	 *            the edit part
	 */
	public static void refreshITextAwareEditParts(EditPart editPart) {

		for (Object obj : editPart.getChildren()) {
			if (obj instanceof EditPart) {
				refreshITextAwareEditParts((EditPart) obj);
			}
		}

		if (editPart instanceof ITextAwareEditPart) {
			editPart.refresh();
		}
	}

	/**
	 * Queries whether an {@code editPart} has canonical synchronization enabled.
	 * 
	 * @param editPart
	 *            an edit part
	 * @return whether it has canonical synchronization enabled
	 */
	public static boolean isCanonical(EditPart editPart) {
		boolean result = false;

		if (editPart instanceof IGraphicalEditPart) {
			View view = ((IGraphicalEditPart) editPart).getNotationView();
			if (view != null) {
				CanonicalStyle style = (CanonicalStyle) view.getStyle(NotationPackage.Literals.CANONICAL_STYLE);
				if (style != null) {
					result = style.isCanonical();
				}
			}
		}

		return result;
	}

}
