package com.montages.mcore.diagram.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.util.McoreSwitch;

/**
 * The Class LinkMappingHelper is used as a declaration of contract. This class
 * is used to express the mapping of link at semantic level. for a semantic what
 * is the source what is the target?
 */
public class LinkMappingHelper {

	/**
	 * Gets the source.
	 *
	 * @param link
	 *            the link
	 *
	 * @return the source
	 */
	public static Collection<?> getSource(EObject link) {
		return getSource(link, new CommonSourceMCoreSwitch());
	}

	/**
	 * Gets the source.
	 *
	 * @param link
	 *            the link
	 * @param mcoreSwitch
	 *            the uml switch
	 *
	 * @return the source
	 */
	public static Collection<?> getSource(EObject link, CommonSourceMCoreSwitch mcoreSwitch) {
		return mcoreSwitch.doSwitch(link);
	}

	/**
	 * Gets the source.
	 *
	 * @param link
	 *            the link
	 *
	 * @return the source
	 */
	// @unused
	public static Collection<?> getTarget(EObject link) {
		return getTarget(link, new CommonTargetMCoreSwitch());
	}

	/**
	 * Gets the source.
	 *
	 * @param link
	 *            the link
	 * @param mcoreSwitch
	 *            the uml switch
	 *
	 * @return the source
	 */
	public static Collection<?> getTarget(EObject link, CommonTargetMCoreSwitch mcoreSwitch) {
		return mcoreSwitch.doSwitch(link);
	}

	public static class CommonSourceMCoreSwitch extends McoreSwitch<Collection<?>> {

		@Override
		public Collection<?> doSwitch(EObject eObject) {
			return super.doSwitch(eObject);
		}

		@Override
		public Collection<MClassifier> caseMProperty(MProperty object) {
			if (!object.isSetContainment()) {
				return Collections.emptyList();
			}
			Collection<MClassifier> result = new ArrayList<MClassifier>();
			result.add((MClassifier) object.eContainer());
			result.add(object.getType());
			return result;
		}

		@Override
		public Collection<?> defaultCase(org.eclipse.emf.ecore.EObject object) {
			return Collections.EMPTY_LIST;
		};
	}

	public static class CommonTargetMCoreSwitch extends McoreSwitch<Collection<?>> {

		@Override
		public Collection<?> doSwitch(EObject eObject) {
			return super.doSwitch(eObject);
		}

		@Override
		public Collection<?> defaultCase(org.eclipse.emf.ecore.EObject object) {
			return Collections.EMPTY_LIST;
		};
	}

}
