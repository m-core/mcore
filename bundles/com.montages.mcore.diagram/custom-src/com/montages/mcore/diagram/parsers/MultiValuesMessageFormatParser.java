package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;

import com.montages.mcore.McorePackage;

public class MultiValuesMessageFormatParser extends MessageFormatParser {

	private static final EAttribute[] NAME_FEATURES = new EAttribute[] {
			McorePackage.eINSTANCE.getMNamed_Name()
//			,McorePackage.eINSTANCE.getMNamed_ShortName()
			};

	private static final String DEFAULT_PATTERN = "{0}";

	public MultiValuesMessageFormatParser() {
		super(NAME_FEATURES);
		setEditorPattern(DEFAULT_PATTERN);
		setViewPattern(DEFAULT_PATTERN);
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable adapter, String editString) {
		return ParserEditStatus.EDITABLE_STATUS;
	}

	@Override
	public ICommand getParseCommand(IAdaptable adapter, String newString, int flags) {
		Object[] values = new Object[] { newString };
		if (values.length < NAME_FEATURES.length) {
			values = complementMissingValues(NAME_FEATURES, values);
		}
		return getParseCommand(adapter, values, flags);
	}

	@Override
	protected Object[] getValues(EObject element) {
		String result = "<unset>";
		for (int i = 0; i < NAME_FEATURES.length; i++) {
			Object temp = getValue(element, NAME_FEATURES[i]);
			if (temp == null || false == temp instanceof String) {
				continue;
			}
			if (((String) temp).trim().isEmpty()) {
				continue;
			}
			result = (String) temp;
		}
		return new Object[] { result };
	}

	private Object[] complementMissingValues(EAttribute[] features, Object[] values) {
		List<Object> result = new ArrayList<Object>();
		Object lastValue = null;
		for (int i = 0; i < features.length; i++) {
			if (i < values.length) {
				lastValue = values[i];
			}
			if (lastValue != null) {
				result.add(lastValue);
			}
		}
		return result.toArray();
	}
}
