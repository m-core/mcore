package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ISemanticParser;

import com.montages.mcore.MProperty;

public class PropertyNameMessageFormatParser extends MessageFormatParser implements ISemanticParser {

	public PropertyNameMessageFormatParser(EAttribute[] features, EAttribute[] editableFeatures) {
		super(features, editableFeatures);
	}

	@Override
	public List getSemanticElementsBeingParsed(EObject element) {
		List<Object> result = new ArrayList<Object>();
		if (element instanceof MProperty) {
			result.add(element);
		}
		return result;
	}

	@Override
	public boolean areSemanticElementsAffected(EObject listener, Object notification) {
		if (false == notification instanceof ENotificationImpl) {
			return false;
		}
		return Notification.SET == ((ENotificationImpl) notification).getEventType();
	}
}
