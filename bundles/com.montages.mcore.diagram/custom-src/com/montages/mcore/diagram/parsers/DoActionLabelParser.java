package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.gmf.tooling.runtime.directedit.ComboDirectEditManager;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.tools.DoActionHelper;

public class DoActionLabelParser implements ComboDirectEditManager.IChoiceParser {

	private static String DO_LABEL_NAME = "do...";

	private final Map<String, Object> myKey2DoActionMap;

	public DoActionLabelParser() {
		super();
		myKey2DoActionMap = new HashMap<String, Object>();
	}

	@Override
	public List<String> getEditChoices(IAdaptable element) {
		myKey2DoActionMap.clear();
		EObject eObject = (EObject) element.getAdapter(EObject.class);
		EAttribute doActionFeature = DoActionHelper.getDoActionFeature(eObject);
		if (doActionFeature == null) {
			return Collections.emptyList();
		}
		AdapterFactoryEditingDomain editingDomain = getEditingDomain(eObject);
		if (editingDomain == null) {
			return Collections.emptyList();
		}
		ItemProviderAdapter itemProvider = getItemProviderAdapter(editingDomain, eObject);
		if (itemProvider == null) {
			return Collections.emptyList();
		}
		IItemPropertyDescriptor propertyDescriptor = itemProvider.getPropertyDescriptor(eObject, doActionFeature);
		if (propertyDescriptor == null) {
			return Collections.emptyList();
		}
		List<String> result = new ArrayList<String>();
		for (Object nextChoice : propertyDescriptor.getChoiceOfValues(eObject)) {
			myKey2DoActionMap.put(nextChoice.toString(), nextChoice);
			result.add(nextChoice.toString());
		}
		return result;
	}

	private ItemProviderAdapter getItemProviderAdapter(AdapterFactoryEditingDomain domain, EObject element) {
		return (ItemProviderAdapter) domain.getAdapterFactory().adapt(element, IItemLabelProvider.class);
	}

	private AdapterFactoryEditingDomain getEditingDomain(EObject element) {
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(element);
		if (editingDomain == null || false == editingDomain instanceof AdapterFactoryEditingDomain) {
			return null;
		}
		return (AdapterFactoryEditingDomain) editingDomain;
	}

	@Override
	public String getEditString(IAdaptable element, int flags) {
		return DO_LABEL_NAME;
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable element, String editString) {
		if (getEditChoices(element).contains(editString)) {
			return ParserEditStatus.EDITABLE_STATUS;
		}
		return new ParserEditStatus(McoreDiagramEditorPlugin.ID, IParserEditStatus.UNEDITABLE, editString);
	}

	@Override
	public ICommand getParseCommand(IAdaptable element, String newString, int flags) {
		EObject eObject = (EObject) element.getAdapter(EObject.class);
		EAttribute doActionFeature = DoActionHelper.getDoActionFeature(eObject);
		if (doActionFeature == null) {
			return UnexecutableCommand.INSTANCE;
		}
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(eObject);
		if (editingDomain == null) {
			return UnexecutableCommand.INSTANCE;
		}
		Object value = myKey2DoActionMap.get(newString);
		if (value == null) {
			return UnexecutableCommand.INSTANCE;
		}
		CompositeTransactionalCommand command = new CompositeTransactionalCommand(editingDomain, "Set Values"); //$NON-NLS-1$
		SetRequest request = new SetRequest(eObject, doActionFeature, value);
		command.compose(new SetValueCommand(request) {

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

				EStructuralFeature feature = ((SetRequest) getRequest()).getFeature();
				Object value = ((SetRequest) getRequest()).getValue();
				EObject elementToEdit = getElementToEdit();
				boolean many = FeatureMapUtil.isMany(elementToEdit, feature);
				if (many) {
					return super.doExecuteWithResult(monitor, info);
				}
				if (getEditingDomain() == null) {
					elementToEdit.eSet(feature, value);
				}
				Command emfSetCommand = getEditingDomain().createCommand(SetCommand.class, new CommandParameter(elementToEdit, feature, value, -1));
				if (emfSetCommand == null || !emfSetCommand.canExecute()) {
					return CommandResult.newErrorCommandResult("Can't execute " + emfSetCommand);
				}
				getEditingDomain().getCommandStack().execute(emfSetCommand);
				return CommandResult.newOKCommandResult(emfSetCommand.getAffectedObjects());
			}
		});
		return command;
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		return getEditString(element, flags);
	}

	@Override
	public boolean isAffectingEvent(Object event, int flags) {
		return false;
	}

	@Override
	public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
		return null;
	}
}
