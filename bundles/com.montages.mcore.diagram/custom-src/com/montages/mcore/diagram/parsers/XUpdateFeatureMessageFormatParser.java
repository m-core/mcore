package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ISemanticParser;

import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;

public class XUpdateFeatureMessageFormatParser extends MessageFormatParser implements ISemanticParser {

	public XUpdateFeatureMessageFormatParser(EAttribute[] features) {
		super(features);
	}

	public XUpdateFeatureMessageFormatParser(EAttribute[] features, EAttribute[] editableFeatures) {
		super(features, editableFeatures);
	}

	@Override
	public ICommand getParseCommand(IAdaptable element, String newString, int flags) {
		EObject eObject = (EObject) element.getAdapter(EObject.class);
		EAttribute[] editableFeatures = getEditableFeatures();
		if (editableFeatures == null || editableFeatures.length == 0) {
			return UnexecutableCommand.INSTANCE;
		}
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(eObject);
		if (editingDomain == null) {
			return UnexecutableCommand.INSTANCE;
		}
		CompositeTransactionalCommand command = new CompositeTransactionalCommand(editingDomain, "Set Values"); //$NON-NLS-1$
		for (EAttribute nextFeature : editableFeatures) {
			SetRequest request = new SetRequest(eObject, nextFeature, newString);
			command.compose(new SetValueCommandAsItemProviderSetCommand(request));
		}
		return command;
	}

	@Override
	public List getSemanticElementsBeingParsed(EObject element) {
		List<Object> result = new ArrayList<Object>();
		if (element instanceof MObject) {
			result.add(((MObject) element).getType());
		}
		if (element instanceof MPropertyInstance) {
			result.add(((MPropertyInstance) element).getProperty());
		}
		return result;
	}

	@Override
	public boolean areSemanticElementsAffected(EObject listener, Object notification) {
		if (false == notification instanceof ENotificationImpl) {
			return false;
		}
		return Notification.SET == ((ENotificationImpl) notification).getEventType();
	}

	protected EAttribute[] getEditableFeatures() {
		return editableFeatures;
	}

	private static class SetValueCommandAsItemProviderSetCommand extends SetValueCommand {

		public SetValueCommandAsItemProviderSetCommand(SetRequest request) {
			super(request);
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
			EStructuralFeature feature = ((SetRequest) getRequest()).getFeature();
			Object value = ((SetRequest) getRequest()).getValue();
			EObject elementToEdit = getElementToEdit();
			boolean many = FeatureMapUtil.isMany(elementToEdit, feature);
			if (many) {
				super.doExecuteWithResult(monitor, info);
			} else {
				if (getEditingDomain() == null) {
					elementToEdit.eSet(feature, value);
				}
				Command emfSetCommand = getEditingDomain().createCommand(SetCommand.class, new CommandParameter(elementToEdit, feature, value, -1));
				if (emfSetCommand == null || !emfSetCommand.canExecute()) {
					return CommandResult.newErrorCommandResult("Can't execute " + emfSetCommand); //$NON-NLS-1$
				}
				getEditingDomain().getCommandStack().execute(emfSetCommand);
				return CommandResult.newOKCommandResult(emfSetCommand.getAffectedObjects());
			}
			return CommandResult.newOKCommandResult();
		}
	}
}
