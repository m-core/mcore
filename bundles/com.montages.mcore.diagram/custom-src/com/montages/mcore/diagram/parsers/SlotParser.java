package com.montages.mcore.diagram.parsers;

import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ISemanticParser;
import org.eclipse.gmf.tooling.runtime.directedit.ComboDirectEditManager.IChoiceParser;
import org.eclipse.osgi.util.NLS;

import com.montages.mcore.PropertyKind;
import com.montages.mcore.diagram.parsers.CompositeSlotProvider.IAdapdableParser;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.part.Messages;
import com.montages.mcore.diagram.tools.MSlotsUtils;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * this is a parser to display a slot in the diagram
 *
 */
public class SlotParser extends MCorePropertyLabelParser implements ISemanticParser, IChoiceParser, IAdapdableParser {

	public SlotParser() {
		super(new EAttribute[] { ObjectsPackage.eINSTANCE.getMDataValue_DataValue() });
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable adapter, String editString) {
		ParsePosition pos = new ParsePosition(0);
		Object[] values = getEditProcessor().parse(editString, pos);
		if (values == null) {
			return new ParserEditStatus(McoreDiagramEditorPlugin.ID, IParserEditStatus.UNEDITABLE, NLS.bind(Messages.MessageFormatParser_InvalidInputError, new Integer(pos.getErrorIndex())));
		}
		return validateNewValues(values);
	}

	@Override
	public ICommand getParseCommand(IAdaptable adapter, String newString, int flags) {
		Object[] values = getEditProcessor().parse(newString, new ParsePosition(0));
		return getParseCommand(adapter, values, flags);
	}

	@Override
	protected ICommand getParseCommand(IAdaptable adapter, Object[] values, int flags) {
		if (values == null || validateNewValues(values).getCode() != IParserEditStatus.EDITABLE) {
			return UnexecutableCommand.INSTANCE;
		}
		MPropertyInstance mPropertyInstance = (MPropertyInstance) adapter.getAdapter(EObject.class);
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(mPropertyInstance);
		if (editingDomain == null) {
			return UnexecutableCommand.INSTANCE;
		}
		CompositeTransactionalCommand command = new CompositeTransactionalCommand(editingDomain, "Set Values"); //$NON-NLS-1$
		if (PropertyKind.ATTRIBUTE == mPropertyInstance.getDerivedKind()) {
			command.compose(getModificationCommand(mPropertyInstance, ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE, getMDataValues(values)));
			return command;
		}
		for (int i = 0; i < values.length; i++) {
			command.compose(getModificationCommand(mPropertyInstance, editableFeatures[i], values[i]));
		}
		return command;
	}

	private List<MDataValue> getMDataValues(Object[] values) {
		List<MDataValue> result = new ArrayList<MDataValue>();
		for (Object nextValue : values) {
			MDataValue mDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
			mDataValue.setDataValue((String) nextValue);
			result.add(mDataValue);
		}
		return result;
	}

	@Override
	public String getEditString(IAdaptable adapter, int flags) {
		MPropertyInstance element = (MPropertyInstance) adapter.getAdapter(MPropertyInstance.class);
		if (element == null || PropertyKind.ATTRIBUTE != element.getDerivedKind()) {
			return "";
		}
		Object[] res = getEditableValues(element);
		return res.length == 0 ? "" : res[0].toString();
	}

	protected Object[] getEditableValues(MPropertyInstance element) {
		List<MDataValue> dataValues = element.getInternalDataValue();
		List<String> result = new ArrayList<String>();
		Iterator<MDataValue> iter = dataValues.iterator();
		while (iter.hasNext()) {
			result.add(iter.next().getDataValue());
		}
		return result.toArray();
	}

	@Override
	public List<?> getSemanticElementsBeingParsed(EObject element) {
		if (false == element instanceof MPropertyInstance) {
			return Collections.emptyList();
		}
		return Arrays.asList(new EObject[] { element });
	}

	@Override
	public boolean areSemanticElementsAffected(EObject listener, Object notification) {
		if (false == notification instanceof ENotificationImpl) {
			return false;
		}
		return Notification.ADD == ((ENotificationImpl) notification).getEventType();
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		if (false == element instanceof EObjectAdapter) {
			return "<UNDEFINED>";
		}
		String result = super.getPrintString(element, flags);
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());
		List<String> internalDataValue = processInternalDataValue(mPropertyInstance.getInternalDataValue());
		if (internalDataValue.isEmpty()) {
			return result;
		}
		if (result.contains("=")) {
			result = result.substring(0, result.indexOf("="));
		}
		StringBuilder sb = new StringBuilder(result + " = ");
		sb.append(MSlotsUtils.outputSlotValues(internalDataValue));
		return sb.toString();
	}

	private List<String> processInternalDataValue(List<MDataValue> internalDataValue) {
		if (internalDataValue == null || internalDataValue.isEmpty()) {
			return Collections.emptyList();
		}
		List<String> result = new ArrayList<String>();
		Iterator<MDataValue> iter = internalDataValue.iterator();
		while (iter.hasNext()) {
			String nextValue = iter.next().getDataValue();
			if (nextValue == null || nextValue.isEmpty()) {
				continue;
			}
			result.add(nextValue);
		}
		return result;
	}

	@Override
	public List<String> getEditChoices(IAdaptable adapter) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) adapter).getRealObject());

		if (MSlotsUtils.isBoolean(mPropertyInstance)) {
			return BOOLEAN_CHOICES;
		}

		return Collections.emptyList();
	}

	private static final List<String> BOOLEAN_CHOICES = Arrays.asList(new String[] { "true", "false" });

	@Override
	public boolean isAdaptableFor(MPropertyInstance mPropertyInstance) {
		return MSlotsUtils.isSlotParser(mPropertyInstance);
	}
}
