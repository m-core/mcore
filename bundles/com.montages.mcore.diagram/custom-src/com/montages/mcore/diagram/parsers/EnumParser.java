package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MLiteral;
import com.montages.mcore.diagram.parsers.CompositeSlotProvider.IAdapdableParser;
import com.montages.mcore.diagram.tools.MSlotsUtils;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

public class EnumParser implements IAdapdableParser, IChoiceParserEx {

	private static final String NAME_VALUE_SEPARATOR = " = ";

	@Override
	public List<String> getEditChoices(IAdaptable adapter) {
		final MPropertyInstance mPropertyInstance = getMPropertyInstance(adapter);
		if (MSlotsUtils.isEnum(mPropertyInstance)) {
			return getTypeChoices(mPropertyInstance.getProperty().getType());
		}
		return Collections.emptyList();
	}

	public boolean isAdaptableFor(MPropertyInstance mPropertyInstance) {
		return MSlotsUtils.isEnum(mPropertyInstance);
	}

	@Override
	public String getEditString(IAdaptable element, int flags) {
		return getPrintString(element, flags) + "edit";
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable element, String editString) {
		final MPropertyInstance mPropertyInstance = getMPropertyInstance(element);
		return MSlotsUtils.isEnum(mPropertyInstance) ? ParserEditStatus.EDITABLE_STATUS : ParserEditStatus.UNEDITABLE_STATUS;
	}

	@Override
	public ICommand getParseCommand(IAdaptable element, String newString, int flags) {
		final MPropertyInstance mPropertyInstance = getMPropertyInstance(element);
		List<MLiteralValue> values = new ArrayList<MLiteralValue>();

		for (String s : newString.split(",")) {
			if (s.length() > 0) {
				MLiteralValue value = ObjectsFactory.eINSTANCE.createMLiteralValue();
				MLiteral mLiteral = getValue(s, mPropertyInstance.getProperty().getType());
				if (mLiteral != null) {
					value.setLiteralValue(mLiteral);
					values.add(value);
				}
			}
		}

		return new SetValueCommand(new SetRequest(mPropertyInstance, ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalLiteralValue(), values));
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		final MPropertyInstance mPropertyInstance = getMPropertyInstance(element);

		if (!MSlotsUtils.isEnum(mPropertyInstance)) {
			return "";
		}

		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(mPropertyInstance);
		if (editingDomain == null || false == editingDomain instanceof AdapterFactoryEditingDomain) {
			return "";
		}

		EList<MLiteralValue> internalLiteralValue = mPropertyInstance.getInternalLiteralValue();
		StringBuilder result = new StringBuilder();
		if (internalLiteralValue != null && !internalLiteralValue.isEmpty()) {
			result.append(NAME_VALUE_SEPARATOR);
			for (int i = 0; i < internalLiteralValue.size(); i++) {
				if (result.length() > NAME_VALUE_SEPARATOR.length()) {
					result.append(", ");
				}
				if (internalLiteralValue.get(i).getLiteralValue() != null) {
					result.append(internalLiteralValue.get(i).getLiteralValue().getEName());
				}
			}
		}
		IItemLabelProvider labelProvider = (IItemLabelProvider) ((AdapterFactoryEditingDomain) editingDomain).getAdapterFactory().adapt(mPropertyInstance, IItemLabelProvider.class);

		return labelProvider.getText(mPropertyInstance) + result.toString();
	}

	@Override
	public boolean isAffectingEvent(Object event, int flags) {
		return false;
	}

	@Override
	public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
		return null;
	}

	private List<String> getTypeChoices(MClassifier type) {
		if (type.getKind() != ClassifierKind.ENUMERATION) {
			return Collections.emptyList();
		}

		List<MLiteral> literals = type.getLiteral();
		ArrayList<String> result = new ArrayList<String>();
		for (MLiteral literal : literals) {
			result.add(literal.getAsString());
		}
		return result;
	}

	private MLiteral getValue(String literalString, MClassifier type) {
		if (literalString == null || type == null) {
			return null;
		}

		for (MLiteral l : type.getLiteral()) {
			if (literalString.equals(l.getAsString())) {
				return l;
			}
		}
		return null;
	}

	@Override
	public List<String> getSelectedChoises(IAdaptable element) {
		final MPropertyInstance mPropertyInstance = getMPropertyInstance(element);

		if (!MSlotsUtils.isEnum(mPropertyInstance)) {
			return Collections.emptyList();
		}

		ArrayList<String> result = new ArrayList<String>();
		for (MLiteralValue literal : mPropertyInstance.getInternalLiteralValue()) {
			if (literal.getLiteralValue() != null) {
				result.add(literal.getLiteralValue().getAsString());
			}
		}
		return result;
	}

	private MPropertyInstance getMPropertyInstance(IAdaptable element) {
		return (MPropertyInstance) ((EObjectAdapter) element).getRealObject();
	}
}
