package com.montages.mcore.diagram.draw2d.figures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.BlockFlow;
import org.eclipse.draw2d.text.FlowFigure;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.draw2d.ui.text.TextFlowEx;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class HTMLWrappingLabel extends WrappingLabel {

	/** text styles stack */
	private List<List<Styles>> myStyles;

	/** font used by default by this figure */
	private static final FontData DEFAULT_FONT = new FontData("Arial", 8, SWT.NORMAL);

	/** font used for sampleCode by this figure */
	private static final FontData CODE_SAMPLE_FONT = new FontData("Lucida Console", 8, SWT.NORMAL);

	/** font used for quote by this figure */
	private static final FontData QUOTE_FONT = new FontData("Monotype Corsiva", 10, SWT.NORMAL);

	/** key for the font style, corresponding to the type of font */
	private static final String FONT_NAME = "face";

	/** key for the font style, corresponding to the type of font */
	private static final String FONT_SIZE = "size";

	private String myText = ""; // Used as the default text (instead of null)

	@Override
	public void setText(String text) {
		if (text == null || text.equals(myText)) {
			return;
		}
		myText = text;
		getFlowPage().removeAll();
		// generates new ones
		generateBlockForText(text, getFlowPage());
	}

	@Override
	public String getText() {
		return myText;
	}

	@Override
	public Dimension getMinimumSize(int w, int h) {
		Dimension minimumSize = super.getMinimumSize(w, h).getCopy();
		if (minimumSize.width == 0) {
			Rectangle area = getClientArea();
			minimumSize.width = area.width;
		}
		return minimumSize;
	}

	/**
	 * Overides Figure.setFont() method
	 * setup font for all TextFlow childes of this figure
	 */
	@Override
	public void setFont(Font f) {
		List<TextFlow> textFlowList = findTextFlowChildList(getFlowPage());
		int i = 0;
		for (TextFlow nextTextFlow : textFlowList) {
			if (i == getStyles().size()) {
				// make code more robust
				break;
			}
			nextTextFlow.setFont(calculateCurrentFont(getStyles().get(i)));
			i++;
		}
	}

	/**
	 * Calculates current font as default figure font + styles
	 * styles has overrides default font
	 * It's main method for getting font value by this figure
	 * 
	 * @return
	 */
	private Font calculateCurrentFont(List<Styles> styles) {
		return composeFontWithStyles(getDefaultFontData(), styles);
	}

	private List<List<Styles>> getStyles() {
		if (myStyles == null) {
			myStyles = new ArrayList<List<Styles>>();
		}
		return myStyles;
	}

	/**
	 * Compose figure font with styles
	 * 
	 * @param defaultFontData
	 * @return
	 */
	private Font composeFontWithStyles(FontData defaultFontData, List<Styles> styles) {
		if (styles == null || styles.isEmpty()) {
			return (Font) JFaceResources.getResources().get(FontDescriptor.createFrom(defaultFontData));
		}
		FontData defaultFontDataCopy = FontDescriptor.copy(defaultFontData);
		int defaultStyle = defaultFontDataCopy.getStyle();
		boolean quote = false;
		boolean codeSample = false;
		// calculate the font to apply
		for (Styles style : styles) {
			switch (style) {
			case italic:
				defaultStyle = defaultStyle | SWT.ITALIC;
				break;
			case strong:
				defaultStyle = defaultStyle | SWT.BOLD;
				break;
			case quote:
				quote = true;
				break;
			case code:
				codeSample = true;
				break;
			case font:
				if (Styles.font.getData().get(FONT_NAME) != null) {
					defaultFontDataCopy.setName((String) Styles.font.getData().get(FONT_NAME));
				}
				if (Styles.font.getData().get(FONT_SIZE) != null) {
					// font size = [1..7] in html, but does not correspond to system
					// size... 2 by default => 8 in real size.
					// so: real size = (html font size)+6
					defaultFontDataCopy.setHeight(((Integer) Styles.font.getData().get(FONT_SIZE)) * 2 + 4);
				}
				break;
			default:
				break;
			}
		}
		defaultFontDataCopy.setStyle(defaultStyle);
		if (codeSample) {
			defaultFontDataCopy = CODE_SAMPLE_FONT;
		} else if (quote) {
			defaultFontDataCopy = QUOTE_FONT;
		}
		return (Font) JFaceResources.getResources().get(FontDescriptor.createFrom(defaultFontDataCopy));
	}

	/**
	 * Clculates default fontData without styles
	 * 
	 * @return
	 */
	private FontData getDefaultFontData() {
		boolean quote = false;
		boolean codeSample = false;
		if (codeSample) {
			return CODE_SAMPLE_FONT;
		}
		if (quote) {
			return QUOTE_FONT;
		}
		FontData fromDefaultFigure = getCurrentFigureFontData();
		if (fromDefaultFigure == null) {
			return DEFAULT_FONT;
		}
		return fromDefaultFigure;
	}

	/**
	 * Font -> FontData converter
	 * 
	 * @return Current Figure FonData value
	 */
	private FontData getCurrentFigureFontData() {
		if (getFont() == null) {
			return null;
		}
		if (getFont().getFontData() == null || getFont().getFontData().length == 0) {
			return null;
		}
		return getFont().getFontData()[0];
	}

	/**
	 * @see HTMLCornerBentFigure.setFont() for using
	 * 
	 * @param parent
	 * @return TextFlow childs list
	 */
	private List<TextFlow> findTextFlowChildList(IFigure parent) {
		List<TextFlow> result = new ArrayList<TextFlow>();
		for (Object nextFigure : parent.getChildren()) {
			if (!(nextFigure instanceof TextFlow)) {
				result.addAll(findTextFlowChildList((IFigure) nextFigure));
				continue;
			}
			result.add((TextFlow) nextFigure);
		}
		return result;
	}

	/**
	 * Casts the text figure to a flowpage.
	 * 
	 * @return
	 */
	protected FlowPage getFlowPage() {
		return (FlowPage) getTextFigure();
	}

	protected FlowFigure getTextFlow() {
		List<?> children = getFlowPage().getChildren();
		if (children.isEmpty()) {
			return null;
		}
		return (FlowFigure) children.get(0);
	}

	@Override
	public Rectangle getTextBounds() {
		Rectangle bounds = getFlowPage().getBounds();
		return new Rectangle(bounds.getLocation(), bounds.getSize());
	}

	@Override
	public void layout() {
		Rectangle textBounds = new Rectangle();
		Rectangle iconBounds = new Rectangle();

		calculateSizes(textBounds, iconBounds);
		calculatePlacement(textBounds, iconBounds);
		calculateAlignment(textBounds, iconBounds);
		calculateLabelAlignment(textBounds, iconBounds);

		if (hasIcons()) {
			setIconLocation(iconBounds.getLocation());
		}

		getTextFigure().setBounds(textBounds.getTranslated(getBounds().getLocation()));
	}

	private void calculateLabelAlignment(Rectangle textBounds, Rectangle iconBounds) {

		Dimension offset = getClientArea().getSize().getDifference(textBounds.getUnion(iconBounds).getSize());
		switch (getAlignment()) {
		case TOP | LEFT:
			offset.height = 0;
			offset.width = 0;
			break;
		case TOP:
			offset.height = 0;
			offset.scale(0.5f);
			break;
		case TOP | RIGHT:
			offset.height = 0;
		case RIGHT:
			offset.width = offset.width * 2;
			offset.scale(0.5f);
			break;
		case BOTTOM | RIGHT:
			break;
		case BOTTOM:
			offset.height = offset.height * 2;
			offset.scale(0.5f);
			break;
		case BOTTOM | LEFT:
			offset.width = 0;
			break;
		case LEFT:
			offset.width = 0;
			offset.scale(0.5f);
			break;
		case CENTER:
			offset.scale(0.5f);
			break;
		default:
			offset.scale(0.5f);
			break;
		}
		textBounds.translate(offset.width, offset.height);
		iconBounds.translate(offset.width, offset.height);
	}

	private void calculateAlignment(Rectangle textBounds, Rectangle iconBounds) {
		Rectangle areaUsed = textBounds.getUnion(iconBounds);
		areaUsed.x = getInsets().left;
		areaUsed.y = getInsets().top;

		switch (getTextPlacement()) {
		case EAST:
		case WEST:
			alignOnHeight(areaUsed, textBounds, getTextAlignment());
			alignOnHeight(areaUsed, iconBounds, getIconAlignment());
			break;
		case NORTH:
		case SOUTH:
			alignOnWidth(areaUsed, textBounds, getTextAlignment());
			alignOnWidth(areaUsed, iconBounds, getIconAlignment());
			break;
		}
	}

	private void alignOnHeight(Rectangle area, Rectangle childBounds, int alignment) {

		switch (alignment) {
		case TOP:
			childBounds.y = area.y;
			childBounds.y = area.y;
			break;
		case BOTTOM:
			childBounds.y = area.getBottom().y - childBounds.height;
			break;
		default:
			childBounds.y = area.y + (area.height - childBounds.height) / 2;
		}
	}

	private void alignOnWidth(Rectangle area, Rectangle childBounds, int alignment) {

		switch (alignment) {
		case LEFT:
			childBounds.x = area.x;
			break;
		case RIGHT:
			childBounds.x = area.getRight().x - childBounds.width;
			break;
		default:
			childBounds.x = area.x + (area.width - childBounds.width) / 2;
		}
	}

	private void calculatePlacement(Rectangle textBounds, Rectangle iconBounds) {
		int gap = (textBounds.isEmpty() || iconBounds.isEmpty()) ? 0 : getIconTextGap();
		Insets insets = getInsets();
		switch (getTextPlacement()) {
		case EAST:
			iconBounds.x = insets.left;
			textBounds.x = iconBounds.width + gap + insets.left;
			break;
		case WEST:
			textBounds.x = insets.left;
			iconBounds.x = textBounds.width + gap + insets.left;
			break;
		case NORTH:
			textBounds.y = insets.top;
			iconBounds.y = textBounds.height + gap + insets.top;
			break;
		case SOUTH:
			textBounds.y = iconBounds.height + gap + insets.top;
			iconBounds.y = insets.top;
		}
	}

	/**
	 * @param container
	 * @param textBounds
	 * @param iconBounds
	 */
	private void calculateSizes(Rectangle textBounds, Rectangle iconBounds) {
		Rectangle area = getClientArea();

		Dimension preferredSize = getPreferredSize(area.width, area.height);

		Dimension minimumSize = getMinimumSize(area.width, area.height);

		Dimension shrinkAmount = preferredSize.getDifference(getBounds().getSize().getUnioned(minimumSize));

		Dimension textSize = preferredTextSize.getCopy();
		if (shrinkAmount.width > 0) {
			textSize.shrink(shrinkAmount.width, 0);
		}
		if (shrinkAmount.height > 0) {
			textSize.shrink(0, shrinkAmount.height);
		}

		/*
		 * if (getTextFlow().isTextTruncated()) {
		 * textBounds.setSize(textSize);
		 * } else {
		 */
		// This is needed for label alignment to work. The preferred text
		// size will extend the entire width, so use the actual text size
		// instead.
		if (getTextFlow() != null) {
			Dimension d = getTextFlow().getSize().intersect(textSize);
			textBounds.setWidth(Math.max(textSize.width, d.width));
			textBounds.setHeight(d.height);
//			getTextFlow().getSize().intersect(textSize)
		} else {
			textBounds.setHeight(textSize.width);
		}
		// }

		iconBounds.setSize(getTotalIconSize());
	}

	@Override
	public boolean isTextWrapOn() {
		return true;
	}

	/**
	 * Generates block list for the given text, and adds it to the root flow
	 * page
	 *
	 * @param text
	 *            the string to display
	 */
	protected void generateBlockForText(String text, FlowPage page) {
		// parse the HMTL text and transforms it into a tree. "Body" tags
		// enforce the character chain to be a valid xml chain
		NodeList nodeList = generateNodeList("<body>" + text + "</body>");
		getStyles().clear();
		// generate blocks from this list and adds it to the flow page children
		if (nodeList.getLength() > 0) {
			generateBlocksFromNodeList(nodeList, page, new Stack<Styles>(), 0);
		} else {
			// problem during parsing
			// return only one text flow with the content of the text
			TextFlowEx textFlow = new TextFlowEx(text);
			page.add(textFlow);
		}
	}

	/**
	 * Generates a list of nodes from the parse of an html text
	 *
	 * @param text
	 *            the text to parse
	 * @return the parsed text under the form of a list of nodes
	 */
	protected NodeList generateNodeList(String text) {
		return HTMLCommentParser.parse(text);
	}

	/**
	 * Builds the structure and content of block flows for a given list of nodes
	 *
	 * @param nodeList
	 *            the list of nodes from which to generates the blockflows
	 */
	protected int generateBlocksFromNodeList(NodeList nodeList, BlockFlow parentFlow, Stack<Styles> parentStyles, int newLineAppend) {
		// for each element in the list, generates the corresponding blocks
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			String nodeName = node.getNodeName();

			short nodeType = node.getNodeType();
			if (nodeType == Node.TEXT_NODE) {
				String text = getNodeStringValue(node);
				if (text == null || text.trim().isEmpty()) {
					continue;
				}
				StringBuilder sb = new StringBuilder();
				for (int nl = 0; nl < newLineAppend; nl++) {
					sb.append("\n");
				}
				newLineAppend = 0;
				sb.append(text);
				List<Styles> styles = Collections.list(parentStyles.elements());
				getStyles().add(styles);
				generateTextFromTextNode(sb.toString(), parentFlow, styles);
			} else {
				try {
					switch (HTMLTags.valueOf(nodeName)) {
					case body: // main tag for the comment body
						// create a block for the body
						BlockFlow blockFlow = new BlockFlow();
						newLineAppend = generateStyledBlock(node, blockFlow, parentStyles, null, newLineAppend);
						parentFlow.add(blockFlow);
						break;
					case strong: // bold character
					case b:
						newLineAppend = generateStyledBlock(node, parentFlow, parentStyles, Styles.strong, newLineAppend);
						break;
					case i: // italic
						newLineAppend = generateStyledBlock(node, parentFlow, parentStyles, Styles.italic, newLineAppend);
						break;
					case br:
						String prevNodeValue = extractTextNodeValueIfPresent(i > 0 ? nodeList.item(i-1) : null);
						if (prevNodeValue == null || !prevNodeValue.endsWith("\n")) {
							newLineAppend++;
						}
						break;
					default:
						break;
					}
				} catch (IllegalArgumentException ex) {
					// Ignored. Unsupported HTML Tag.
				}
			}
		}
		return newLineAppend;
	}
	
	private static String extractTextNodeValueIfPresent(Node node) {
		return Optional.ofNullable(node).filter(n -> Node.TEXT_NODE == n.getNodeType())
			.map(HTMLWrappingLabel::getNodeStringValue).orElse(null);
	}
	
	private static String getNodeStringValue(Node node) {
		return HTMLCleaner.cleanHTMLTags(node.getNodeValue());
	}

	protected int generateStyledBlock(Node node, BlockFlow parentFlow, Stack<Styles> styles, Styles newStyle, int newLineToAppend) {
		NodeList childrenNodes = node.getChildNodes();
		if (newStyle != null) {
			styles.push(newStyle);
		}
		int result = generateBlocksFromNodeList(childrenNodes, parentFlow, styles, newLineToAppend);
		if (newStyle != null) {
			styles.pop();
		}
		return result;
	}

	/**
	 * Generates code from a node representing a text.
	 * IFigure default font value will setup if styles list is empty.
	 * 
	 * @param node
	 *            the node from which to generate belowk flows
	 * @param parentFlow
	 *            the parent block flow which will contain the block created
	 */
	protected void generateTextFromTextNode(String text, BlockFlow parentFlow, List<Styles> styles) {
		TextFlowEx textFlow = new TextFlowEx(text);
		// textFlow.setTextUnderline(getUnderLineFromStyles(styles));
		textFlow.setFont(calculateCurrentFont(styles));
		parentFlow.add(textFlow);
	}


	/**
	 * Styles to apply to the text
	 */
	protected enum Styles {
		strong, header3, header4, header5, underline, italic, code, subscript, supscript, quote, font(new HashMap<String, Object>());

		/** additional data */
		private Map<String, Object> myData;

		Styles() {
			myData = null;
		}

		Styles(Map<String, Object> data) {
			myData = data;
		}

		/**
		 * sets the data associated to this enum
		 *
		 * @param data
		 *            the data to set
		 */
		public void setData(Map<String, Object> data) {
			myData = data;
		}

		/**
		 * Returns the data for this enum
		 *
		 * @return the data for this enum
		 */
		public Map<String, Object> getData() {
			return myData;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTextUnderline(boolean underline) {

	}

	/**
	 * {@inheritDoc}
	 */
	public void setTextStrikeThrough(boolean strikeThrough) {

	}

	/**
	 * Valid HTML tags enumeration
	 */
	protected enum HTMLTags {

		body(""), // main tag for the comment body
		h3(""), // section heading
		h4(""), // sub section heading
		h5(""), // sub sub section heading
		strong(""), // bold character
		b(""), // bold character
		i(""), // italic
		u(""), // underline
		sub(""), // subscript
		sup(""), // superscript
		blockquote(""), // indent left or right
		table(""), // table
		p(""), // paragraph
		br(""), // new line
		font(""); // specific font

		/** additional data for this enum */
		protected String myData;

		HTMLTags(String data) {
			myData = data;
		}

		/**
		 * Sets the data for this enum
		 *
		 * @param data
		 *            the data to set
		 */
		public void setData(String data) {
			myData = data;
		}

		/**
		 * Returns the data associated to this enum
		 *
		 * @return the data associated to this enum
		 */
		public String getData() {
			return myData;
		}

	}
}
