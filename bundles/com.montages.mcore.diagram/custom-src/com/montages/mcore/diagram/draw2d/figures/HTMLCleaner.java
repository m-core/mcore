/*****************************************************************************
 * Copyright (c) 2009 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Remi Schnekenburger (CEA LIST) remi.schnekenburger@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package com.montages.mcore.diagram.draw2d.figures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.montages.mcore.diagram.draw2d.figures.HTMLExpression.NodeExpression;
import com.montages.mcore.diagram.draw2d.figures.HTMLExpression.TextExpression;

/**
 * Class that provides a html cleaner.
 */
public class HTMLCleaner {

	/** map of special html strings and their real value */
	protected static final Map<String, String> specials = new HashMap<String, String>();

	/** symbol requiring new lines */
	protected static final List<String> newLine = new ArrayList<String>();

	/** map of special html strings and their real value */
	protected static final Map<String, String> xmlSpecials = new HashMap<String, String>();

	protected static final List<String> htmlTags = new ArrayList<String>();

	static {
		// initialize the special character map
		specials.put("nbsp", " "); // no-break space
		specials.put("iexcl", "?"); // inverted exclamation mark
		specials.put("cent", "?"); // cent sign
		specials.put("pound", "?"); // pound sign
		specials.put("curren", "�"); // currency sign
		specials.put("yen", "?"); // yen sign = yuan sign
		specials.put("brvbar", "�"); // broken bar = brolen vertical bar
		specials.put("sect", "�"); // section sign
		specials.put("uml", "?"); // diaeresis = spacing diaeresis
		specials.put("copy", "�"); // copyright sign
		specials.put("ordf", "?"); // feminine ordinal indicator
		specials.put("laquo", "�"); // left-pointing double angle quotation mark = left pointing guillemet
		specials.put("not", "�"); // not sign = discretionary hyphen
		specials.put("shy", "�"); // soft hyphen = discretionary hyphen
		specials.put("reg", "�"); // registered sign = registered trade mark sign
		specials.put("macr", "?"); // macron = spacing macron = overline = APL overbar
		specials.put("deg", "�"); // degree sign
		specials.put("plusmn", "�"); // plus-minus sign = plus-or-minus sign
		specials.put("sup2", "?"); // superscript two = superscript digit two = squared
		specials.put("sup3", "?"); // superscript three = superscript digit three = cubed
		specials.put("acute", "?"); // acute accent = spacing acute
		specials.put("micro", "�"); // micro sign
		specials.put("para", "�"); // pilcrow sign = paragraph sign
		specials.put("middot", "�"); // middle dot = Georgian comma = Greek middle dot
		specials.put("ccedil", "?"); // cedilla = spacing cedilla
		specials.put("sup1", "?"); // superscript one = superscript digit one
		specials.put("ordm", "?"); // masculine ordinal indicator
		specials.put("raquo", "�"); // right-pointing double angle quotation mark = right pointing guillemet
		specials.put("frac14", "?"); // vulgar fraction one quarter = fraction one quarter
		specials.put("frac12", "?"); // vulgar fraction one half = fraction one half
		specials.put("frac34", "?"); // vulgar fraction three quarters = fraction three quarters
		specials.put("iquest", "?"); // inverted question mark = turned question mark
		specials.put("Agrave", "?"); // latin capital letter A with grave = latin capital letter A grave
		specials.put("Aacute", "?"); // latin capital letter A with acute
		specials.put("Acirc", "?"); // latin capital letter A with circumflex
		specials.put("Atilde", "?"); // latin capital letter A with tilde
		specials.put("Auml", "?"); // latin capital letter A with diaeresis
		specials.put("Aring", "?"); // latin capital letter A with ring above = latin capital letter A ring
		specials.put("AElig", "?"); // latin capital letter AE = latin capital ligature AE
		specials.put("Ccedil", "?"); // latin capital letter C with cedilla
		specials.put("Egrave", "?"); // latin capital letter E with grave
		specials.put("Eacute", "?"); // latin capital letter E with acute
		specials.put("Ecirc", "?"); // latin capital letter E with circumflex
		specials.put("Euml", "?"); // latin capital letter E with diaeresis
		specials.put("Igrave", "?"); // latin capital letter I with grave
		specials.put("Iacute", "?"); // latin capital letter I with acute
		specials.put("Icirc", "?"); // latin capital letter I with circumflex
		specials.put("Iuml", "?"); // latin capital letter I with diaeresis
		specials.put("ETH", "?"); // latin capital letter ETH
		specials.put("Ntilde", "?"); // latin capital letter N with tilde
		specials.put("Ograve", "?"); // latin capital letter O with grave
		specials.put("Oacute", "?"); // latin capital letter O with acute
		specials.put("Ocirc", "?"); // latin capital letter O with circumflex
		specials.put("Otilde", "?"); // latin capital letter O with tilde
		specials.put("Ouml", "?"); // latin capital letter O with diaeresis
		specials.put("times", "?"); // multiplication sign
		specials.put("Oslash", "?"); // latin capital letter O with stroke = latin capital letter O slash
		specials.put("Ugrave", "?"); // latin capital letter U with grave
		specials.put("Uacute", "?"); // latin capital letter U with acute
		specials.put("Ucirc", "?"); // latin capital letter U with circumflex
		specials.put("Uuml", "?"); // latin capital letter U with diaeresis
		specials.put("Yacute", "?"); // latin capital letter Y with acute
		specials.put("THORN", "?"); // latin capital letter THORN
		specials.put("szlig", "?"); // latin small letter sharp s = ess-zed
		specials.put("agrave", "?"); // latin small letter a with grave = latin small letter a grave
		specials.put("aacute", "?"); // latin small letter a with acute
		specials.put("acirc", "?"); // latin small letter a with circumflex
		specials.put("atilde", "?"); // latin small letter a with tilde
		specials.put("auml", "?"); // latin small letter a with diaeresis
		specials.put("aring", "?"); // latin small letter a with ring above = latin small letter a ring
		specials.put("aelig", "?"); // latin small letter ae = latin small ligature ae
		specials.put("ccedil", "?"); // latin small letter c with cedilla
		specials.put("egrave", "?"); // latin small letter e with grave
		specials.put("eacute", "?"); // latin small letter e with acute
		specials.put("ecirc", "?"); // latin small letter e with circumflex
		specials.put("euml", "?"); // latin small letter e with diaeresis
		specials.put("igrave", "?"); // latin small letter i with grave
		specials.put("iacute", "?"); // latin small letter i with acute
		specials.put("icirc", "?"); // latin small letter i with circumflex
		specials.put("iuml", "?"); // latin small letter i with diaeresis
		specials.put("eth", "?"); // latin small letter eth
		specials.put("ntilde", "?"); // latin small letter n with tilde
		specials.put("ograve", "?"); // latin small letter o with grave
		specials.put("oacute", "?"); // latin small letter o with acute
		specials.put("ocirc", "?"); // latin small letter o with circumflex
		specials.put("otilde", "?"); // latin small letter o with tilde
		specials.put("ouml", "?"); // latin small letter o with diaeresis
		specials.put("divide", "?"); // division sign
		specials.put("oslash", "?"); // latin small letter o with stroke = latin small letter o slash
		specials.put("ugrave", "?"); // latin small letter u with grave
		specials.put("uacute", "?"); // latin small letter u with acute
		specials.put("ucirc", "?"); // latin small letter u with circumflex
		specials.put("uuml", "?"); // latin small letter u with diaeresis
		specials.put("yacute", "?"); // latin small letter y with acute
		specials.put("thorn", "?"); // latin small letter thorn with
		specials.put("yuml", "?"); // latin small letter y with diaeresis
		specials.put("quot", "\""); // quotation mark = APL quote
		specials.put("radic", "?"); // square root = radical sign
		specials.put("infin", "?"); // infinity
		specials.put("cap", "?"); // intersection = cap
		specials.put("int", "?"); // integral
		xmlSpecials.put("amp", "&"); // ampersand
		xmlSpecials.put("lt", "<"); // less-than sign
		xmlSpecials.put("gt", ">"); // greater-than sign
		// new line list
		newLine.add("BR"); // new line request
		newLine.add("br");
		newLine.add("BR/");
		newLine.add("br/");
		newLine.add("br /");
		newLine.add("BR /");
		newLine.add("/H1"); // end of header
		newLine.add("/H2");
		newLine.add("/H3");
		newLine.add("/h1");
		newLine.add("/h2");
		newLine.add("/h3");
		newLine.add("/p"); // end of paragraph
		newLine.add("/P");
		newLine.add("/li"); // end of item list

		htmlTags.add("h1");
		htmlTags.add("H1");
		htmlTags.add("h2");
		htmlTags.add("H2");
		htmlTags.add("h3");
		htmlTags.add("H3");
		htmlTags.add("BR");
		htmlTags.add("/BR");
		htmlTags.add("br");
		htmlTags.add("/br");
		htmlTags.add("strong");
		htmlTags.add("/strong");
		htmlTags.add("b");
		htmlTags.add("/b");
		htmlTags.add("u");
		htmlTags.add("/u");
		htmlTags.add("i");
		htmlTags.add("/i");
		htmlTags.add("ul");
		htmlTags.add("/ul");
		htmlTags.add("li");
		htmlTags.add("/li");
		htmlTags.add("p");
		htmlTags.add("/p");

	}

	public static String removeHTMLTags(String htmlString) throws Exception {
		StringBuffer buffer = new StringBuffer();
		// indicating if parser is in tag
		boolean inTag = false;
		// indicating if parser is in special character
		boolean inSpecial = false;
		// skip the next character
		boolean skip = false;
		// ignore or keep whitespace ?
		boolean keepWhitespace = true;
		// ignore or keep whitespace ?
		boolean keepCarriageReturn = false;
		int length = htmlString.length();
		for (int i = 0; i < length; i++) {
			skip = false;
			char c = htmlString.charAt(i);
			if (c == '<') { // opening a new tag...

				// should do specific check for new lines (<BR>, <P>, <H1>,
				// <H2>, etc..)
				// get tag value
				int closingTagIndex = htmlString.indexOf('>', i);
				if (closingTagIndex > i + 1) {
					String tagValue = htmlString.substring(i + 1, closingTagIndex);

					if (tagValue != null) {
						if (htmlTags.contains(tagValue.toLowerCase()) || newLine.contains(tagValue.toLowerCase())) {
							inTag = true;
							if (newLine.contains(tagValue)) {
								if (keepCarriageReturn) {
									buffer.append("\n");
									keepCarriageReturn = false;
								}
								keepWhitespace = false;
							}
						} else {
							buffer.append('<');
						}
					} else {
						buffer.append('<');
					}
				} else {
					buffer.append('<');
				}

			} else if (c == '>' && inTag) { // closing tag. must be in tag to
											// close it...
				inTag = false;
				skip = true;
				keepWhitespace = true;
			} else if (c == '&') {
				// this is a special character
				// look for next ';', which closes the special character
				int nextSemiColonIndex = htmlString.indexOf(';', i);
				if (nextSemiColonIndex > i + 1) {
					String specialCharacter = htmlString.substring(i + 1, htmlString.indexOf(';', i));
					String replacement = specials.get(specialCharacter);
					if (replacement == null) {
						replacement = xmlSpecials.get(specialCharacter);
					}
					if (replacement != null) {
						inSpecial = true;
						buffer.append(replacement);
					} else {
						// simple '&' in a non html comment => keep it
						buffer.append('&');
						keepWhitespace = true;
					}
				} else {
					// simple '&' in a non html comment => keep it
					buffer.append('&');
					keepWhitespace = true;
				}
			} else if (c == ';' && inSpecial) {
				inSpecial = false;
				skip = true;
				keepWhitespace = true;
			} else if (c == ' ' || c == '\t') {
				if (keepWhitespace) {
					buffer.append(" ");
				}
				keepWhitespace = false;
			} else if (c == '\n' || c == '\r') {
				if (keepCarriageReturn) {
					buffer.append("\n");
					keepCarriageReturn = false;
					keepWhitespace = false;
				}
			} else if (!skip && !inSpecial && !inTag) {
				buffer.append(c);
				keepWhitespace = true;
				keepCarriageReturn = true;
			}
		}
		return buffer.toString();
	}

	/**
	 * Returns a string derived from the specified string. It removes htlm tags,
	 * adding new line separator when useful.
	 *
	 * @param htmlString
	 *            the string to clean. It should be neither <code>null</code>,
	 *            nor empty
	 * @return a cleaned string.
	 */
	public static String cleanHTMLTags(String htmlString) {
		StringBuffer buffer = new StringBuffer();
		// indicating if parser is in tag
		boolean inTag = false;
		// indicating if parser is in special character
		boolean inSpecial = false;
		// skip the next character
		boolean skip = false;
		// ignore or keep whitespace ?
		boolean keepWhitespace = true;
		// ignore or keep whitespace ?
		boolean keepCarriageReturn = false;
		int length = htmlString.length();
		for (int i = 0; i < length; i++) {
			skip = false;
			char c = htmlString.charAt(i);
			if (c == ' ' || c == '\t') {
				if (keepWhitespace) {
					buffer.append(" ");
				}
				keepWhitespace = false;
			} else if (c == '\n' || c == '\r') {
				if (keepCarriageReturn) {
					buffer.append("\n");
					keepCarriageReturn = false;
					keepWhitespace = false;
				}
			} else if (!skip && !inSpecial && !inTag) {
				buffer.append(c);
				keepWhitespace = true;
				keepCarriageReturn = true;
			}
		}
		return buffer.toString();
	}

	public static String prepareForDocumentBuilder(String htmlString) {
		return new HTMLDocument(htmlString).prepareForDocumentBuilder().replace("\n", "<br/>");
	}
	
	/**
	 * Build tree for valid html and do not valid
	 */
	public static class HTMLDocument extends TextExpression implements NodeExpression {
		
		private List<HTMLExpression> myChildren;
		
		public HTMLDocument(String text) {
			super(text);
			parse();
		}
		
		public List<HTMLExpression> getChildren() {
			if (myChildren == null) {
				myChildren = new LinkedList<HTMLExpression>();
			}
			return myChildren;
		}
		
		@Override
		public void addChild(HTMLExpression child) {
			getChildren().add(child);
		}
		
		private void parse() {
			List<BaseHTMLExpressionParser> parsers = getValueBuilders();
			
			int pos = 0;
			int length = getValue().length();
			List<HTMLExpression> parsedDoc = new LinkedList<HTMLExpression>();
			while (pos < length) {
				int nextValidValueIndex = length;
				HTMLExpression currentExpression = null;
				for (BaseHTMLExpressionParser parser: parsers) {
					int next = parser.findNextValidExpression(pos);
					if (next != -1 && next < nextValidValueIndex) {
						nextValidValueIndex = next;
						currentExpression = parser.parse(nextValidValueIndex);
					}
				}
				if (nextValidValueIndex != pos) {
					String text = getValue().substring(pos, nextValidValueIndex);
					TextExpression textExpression = new TextExpression(text);
					parsedDoc.add(textExpression);
				}
				if (currentExpression != null) {
					parsedDoc.add(currentExpression);
					pos = nextValidValueIndex + currentExpression.getExpression().length();
				} else {
					break;
				}
			}
			buildTree(this, parsedDoc, 0);
		}

		protected int buildTree(NodeExpression parent, List<HTMLExpression> elements, int pos) {
			int size = elements.size();
			int i = pos;
			while (i < size) {
				
				HTMLExpression element = elements.get(i);
				
				if (element instanceof CloseTag && parent instanceof OpenTag) {
					CloseTag closeTag = (CloseTag) element;
					OpenTag openTag = (OpenTag) parent;
					
					if (closeTag.getRelated() == null) { 
						if (openTag.getRelated() == null && closeTag.isTagValue(openTag.getValue())) {
							closeTag.setRelatedTag(openTag);
							return i;
						} else if (hasOpenTagInTree(openTag, closeTag)) {
							return i;
						}
					}
				}
				
				parent.addChild(element);
				i++;

				if (element instanceof NodeExpression) {
					i = buildTree((NodeExpression) element, elements, i);
				}
			}
			return i;
		}

		private boolean hasOpenTagInTree(OpenTag openTag, CloseTag closeTag) {
			if (openTag == null) {
				return false;
			}
			if (openTag.isTagValue(closeTag.getValue())) {
				return true;
			}
			return hasOpenTagInTree((OpenTag)openTag.getParent(), closeTag);
		}

		private List<BaseHTMLExpressionParser> getValueBuilders() {
			String value = getValue();
			return Arrays.asList(new BaseHTMLExpressionParser[]{
					new BaseHTMLExpressionParser.OpanTagParser(value),
					new BaseHTMLExpressionParser.CloseTagParser(value),
					new BaseHTMLExpressionParser.SpecialXmlExpressionParser(value)
			});
		}
		
		@Override
		public NodeExpression getParent() {
			return null;
		}
		
		@Override
		public void setParent(NodeExpression parent) {
		}
		
		@Override
		public String prepareForDocumentBuilder() {
			StringBuilder result = new StringBuilder();
			for (HTMLExpression expr: getChildren()) {
				result.append(expr.prepareForDocumentBuilder());
			}
			return result.toString();
		}
	}
}
