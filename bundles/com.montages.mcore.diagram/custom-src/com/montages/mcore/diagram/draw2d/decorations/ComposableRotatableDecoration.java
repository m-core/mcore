package com.montages.mcore.diagram.draw2d.decorations;

import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.Point;


public interface ComposableRotatableDecoration extends RotatableDecoration {
	Point getBoundPoint();
}
