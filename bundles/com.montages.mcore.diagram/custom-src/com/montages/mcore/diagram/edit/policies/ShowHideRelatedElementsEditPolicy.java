package com.montages.mcore.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Relationship;

import com.montages.mcore.diagram.tools.Domain2Notation;
import com.montages.mcore.diagram.tools.LinkEndsMapper;
import com.montages.mcore.diagram.tools.LinkMappingHelper;

public class ShowHideRelatedElementsEditPolicy extends AbstractShowHideRelatedLinkEditPolicy {

	public static final String SHOW_HIDE_RELATED_ELEMENTS_POLICY = "Show/Hide related elements policy"; //$NON-NLS-1$

	public ShowHideRelatedElementsEditPolicy(final DiagramEditPart host) {
		super(host);
	}

	public ShowHideRelatedElementsEditPolicy() {
		super(null);
	}

	/**
	 *
	 * @see org.eclipse.papyrus.infra.gmfdiag.common.editpolicies.AbstractShowHideRelatedLinkEditPolicy#getShowHideRelatedLinkCommandWithDialog(java.util.Collection, java.util.Map, java.util.Set, java.util.Map, java.util.Collection)
	 *
	 * @param selectedEditParts
	 * @param availableLinks
	 * @param visibleLinks
	 * @param domain2NotationMap
	 * @param linksDescriptors
	 * @return
	 *         the command which open a dialog to ask for the user to select visible links, chained with the command to show/hide the links according
	 *         to the user selection
	 */
	@Override
	protected Command getShowHideRelatedLinkCommandWithDialog(final Collection<EditPart> selectedEditParts, final Map<EditPart, Set<EObject>> availableLinks, final Set<EObject> visibleLinks,
			final Domain2Notation domain2NotationMap, final Collection<UpdaterLinkDescriptor> linksDescriptors) {
		// 0. build the mapping between semantic link and their representations used in the dialog
		final Map<EObject, LinkEndsMapper> linkMapping = new HashMap<EObject, LinkEndsMapper>();
		final Iterator<UpdaterLinkDescriptor> iter = linksDescriptors.iterator();
		while (iter.hasNext()) {
			final UpdaterLinkDescriptor current = iter.next();
			final EObject link = current.getModelElement();
			if (link == null) {
				continue;
			}
			if (link instanceof EdgeWithNoSemanticElementRepresentationImpl) {
				final EObject source = ((EdgeWithNoSemanticElementRepresentationImpl) link).getSource();
				if (source instanceof Comment || source instanceof Constraint) {
					linkMapping.put(link, createLinkEndMapper((Element) source, current));
				}
			} else {
				linkMapping.put(link, createLinkEndMapper(link, current));
			}
		}
		final TransactionalEditingDomain domain = getEditingDomain();
		final CompositeCommand compositeCommand = new CompositeCommand("Show/Hide Related Link Command"); //$NON-NLS-1$
		final ICommand cmd = getOpenDialogCommand(domain, selectedEditParts, availableLinks, visibleLinks, linkMapping);
		compositeCommand.add(cmd);
		compositeCommand.add(getShowHideRelatedElementsCommand(domain, cmd, visibleLinks, new ViewAndViewDescriptorWrapper(domain2NotationMap), getNotVisibleConnections(), linksDescriptors));
		return new ICommandProxy(compositeCommand);
	}

	@Override
	protected Collection<UpdaterLinkDescriptor> removeInvalidLinkDescriptor(final Collection<UpdaterLinkDescriptor> descriptor) {
		final Collection<UpdaterLinkDescriptor> firstResult = super.removeInvalidLinkDescriptor(descriptor);
		final Collection<UpdaterLinkDescriptor> result = new ArrayList<UpdaterLinkDescriptor>();
		final Iterator<UpdaterLinkDescriptor> iter = firstResult.iterator();
		while (iter.hasNext()) {
			final UpdaterLinkDescriptor current = iter.next();
			EObject src = current.getSource();
			EObject target = current.getDestination();
			if (src == null || target == null) {
				continue;
			}
			result.add(current);
		}
		return result;
	}

	/**
	 *
	 * @return
	 *         <code>true</code> if the link is oriented and <code>false</code> if not.
	 *         If not, that is to say than {@link LinkMappingHelper} should returns the same values for sources and targets
	 */
	public static final boolean isAnOrientedLink(final EObject link) {
		if (link instanceof DirectedRelationship) {
			return true;
		} else if (link instanceof Relationship) {
			return false;
		} else if (link instanceof Connector) {
			return false;
		} else if (link instanceof Comment || link instanceof Constraint) {
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param element
	 *            an element
	 * @return
	 *         a linkEndsMapper according to this element
	 */
	public static final LinkEndsMapper createLinkEndMapper(final EObject element, final UpdaterLinkDescriptor descriptor) {
		Collection<?> ends;
		Collection<?> sources;
		Collection<?> targets;
		if (element instanceof Comment || element instanceof Constraint) {
			ends = Collections.emptyList();
			sources = Collections.singletonList(descriptor.getSource());
			targets = Collections.singletonList(descriptor.getDestination());
		} else if (isAnOrientedLink(element)) {
			ends = Collections.emptyList();
			sources = LinkMappingHelper.getSource(element);
			targets = LinkMappingHelper.getTarget(element);
		} else {
			ends = LinkMappingHelper.getSource(element);
			sources = Collections.emptyList();
			targets = Collections.emptyList();
		}
		return new LinkEndsMapper(element, ends, sources, targets);
	}

	/**
	 *
	 * @param domain
	 *            the editing domain
	 * @return
	 *         the command to open the dialog to choose the link to show
	 */
	protected ICommand getOpenDialogCommand(final TransactionalEditingDomain domain, final Collection<EditPart> selectedEditPart, final Map<EditPart, Set<EObject>> availableLinks,
			final Collection<EObject> initialSelection, final Map<EObject, LinkEndsMapper> linkMapping) {
		final ICommand cmd = new AbstractTransactionalCommand(domain, "Open Show/HideDialogCommand", null) {//$NON-NLS-1$

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
				/*final ShowHideRelatedLinkSelectionDialog dialog = new ShowHideRelatedLinkSelectionDialog(Display.getDefault().getActiveShell(), getLabelProvider(), new AbstractShowHideRelatedLinkEditPolicy.LinkContentProvider(availableLinks), availableLinks,
						linkMapping);
				dialog.setTitle("Show/Hide Links");//$NON-NLS-1$
				dialog.setMessage("Choose the links to show.");//$NON-NLS-1$
				dialog.setInput(selectedEditPart);
				dialog.setInitialSelection(initialSelection);
				dialog.setExpandedElements(selectedEditPart.toArray());
				dialog.setContainerMode(true);
				int status = dialog.open();
				if (status == Window.CANCEL) {
					return CommandResult.newCancelledCommandResult();
				}*/
				//return CommandResult.newOKCommandResult(Arrays.asList(dialog.getResult()));
				return CommandResult.newOKCommandResult(availableLinks.values());
			}
		};
		return cmd;
	}
}
