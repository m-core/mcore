package com.montages.mcore.diagram.edit.policies;

import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;

import com.montages.mcore.diagram.edit.parts.MPackageEditPart;

/**
 * Canonical Editpolicy does not provide hints for create requests. 
 * Thus, until the MCoreViewViewProvider is instantiated, its xml definition can't correctly answer its availability for Canonical requests.
 * <p>
 * This editpolicy workarounds the problem by ensuring that the MCoreViewProvider is instantiated after the first diagram opening.  
 * @author Golubev
 *
 */
public class ForceViewProviderActivationEditPolicy extends AbstractEditPolicy {
	public static final String KEY = ForceViewProviderActivationEditPolicy.class.getSimpleName() + ":Key";

	private static boolean ourActivatedOnce;

	@Override
	public void activate() {
		if (!ourActivatedOnce) {
			ensureViewProviderIsInstantiated();
			ourActivatedOnce = true;
		}
	}

	private void ensureViewProviderIsInstantiated() {
		ViewService.createDiagram(getHost().resolveSemanticElement(), MPackageEditPart.MODEL_ID, getHost().getDiagramPreferencesHint());
		//after this call the MCoreViewProvider is instantiated and ViewService will ask it directly instead of checking with XML definition.
		//this should solve the problem with unhandled canonical view creation requests  
	}

	@Override
	public GraphicalEditPart getHost() {
		return (GraphicalEditPart) super.getHost();
	}

}
