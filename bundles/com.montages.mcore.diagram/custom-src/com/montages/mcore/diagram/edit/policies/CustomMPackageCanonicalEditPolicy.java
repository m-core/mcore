package com.montages.mcore.diagram.edit.policies;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.Transaction;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.common.core.util.StringStatics;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.util.EditPartUtil;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectType2EditPart;
import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentsLinkEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.part.McoreDiagramUpdater;
import com.montages.mcore.diagram.part.McoreNodeDescriptor;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsPackage;

public class CustomMPackageCanonicalEditPolicy extends AbstractEditPolicy {

	public static final String ROLE = "CustomMPackageCanonicalEditPolicy";

	private static final ObjectsPackage OBJECT = ObjectsPackage.eINSTANCE;

	private static final McorePackage MCORE = McorePackage.eINSTANCE;

	private ResourceListener semanticListener = new ResourceListener();

	@Override
	public void activate() {
		super.activate();
		if (resolveSemanticElement() != null) {
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(resolveSemanticElement());
			domain.addResourceSetListener(semanticListener);
			removeInvalidElements();
		}
	}

	@Override
	public void deactivate() {
		if (resolveSemanticElement() != null) {
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(resolveSemanticElement());
			if (domain != null) {
				domain.removeResourceSetListener(semanticListener);
			}
		}
		super.deactivate();
	}

	private Set<EStructuralFeature> myFeaturesToSynchronize;

	protected Set<EStructuralFeature> getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet<EStructuralFeature>();
			myFeaturesToSynchronize.add(MCORE.getMPackage_Classifier());
			myFeaturesToSynchronize.add(OBJECT.getMResource_Object());
		}
		return myFeaturesToSynchronize;
	}

	@SuppressWarnings("rawtypes")
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		LinkedList<EObject> result = new LinkedList<EObject>();
		List<McoreNodeDescriptor> childDescriptors = McoreDiagramUpdater.getMPackage_1000SemanticChildren(viewObject);
		for (McoreNodeDescriptor d : childDescriptors) {
			result.add(d.getModelElement());
		}
		return result;
	}

	protected boolean isOrphaned(Collection<EObject> semanticChildren, final View view) {
		return isMyDiagramElement(view) && !semanticChildren.contains(view.getElement());
	}

	private boolean isMyDiagramElement(View view) {
		int visualID = McoreVisualIDRegistry.getVisualID(view);
		EObject element = view.getElement();
		return (visualID == MClassifierEditPart.VISUAL_ID || visualID == MObjectEditPart.VISUAL_ID) && (element instanceof MClassifier || element instanceof MObject);
	}

	protected MPackage resolveSemanticElement() {
		return (MPackage) getHost().resolveSemanticElement();
	}

	@Override
	public MPackageEditPart getHost() {
		return (MPackageEditPart) super.getHost();
	}

	@SuppressWarnings("unchecked")
	protected List<View> getViewChildren() {
		return getHost().getDiagramView().getChildren();
	}

	protected void removeInvalidElements() {
		if (resolveSemanticElement() == null) {
			return;
		}
		Set<View> orphaned = new HashSet<View>();
		// we care to check only views we recognize as ours and not shortcuts
		for (View v : getViewChildren()) {
			if (McoreDiagramUpdater.isShortcutOrphaned(v)) {
				for (Object source: ViewUtil.getSourceConnections(v)) {
					orphaned.add((View)source);
				}
				for (Object target: ViewUtil.getTargetConnections(v)) {
					orphaned.add((View)target);
				}
				orphaned.add(v);
//				deleteEdges(ViewUtil.getSourceConnections(v), orphaned);
//				deleteEdges(ViewUtil.getTargetConnections(v), orphaned);
			}
		}

		// links
		deleteEdges(getHost().getDiagramView().getEdges(), orphaned);
		deleteViews(orphaned.iterator());
	}

	private static void deleteEdges(List elements, Set<View> orphaned) {
		for (Object element: elements) {
			Edge edge = (Edge)element;
			if (shouldBeDeletedLink(edge)) {
				orphaned.add(edge);
			}
		}
	}

	private static boolean shouldBeDeletedLink(Edge edge) {
		View targetView = edge.getTarget();
		View sourceView = edge.getSource();
		if (targetView == null || sourceView == null) {
			return true;
		}
		EObject targetElement = targetView.getElement();
		EObject sourceElement = sourceView.getElement();
		if (targetElement == null || sourceElement == null) {
			return true;
		}
		switch (Integer.parseInt(edge.getType())) {
		case MPropertyEditPart.VISUAL_ID:
		case MPropertyContainmentEditPart.VISUAL_ID:
			if (false == edge.getElement() instanceof MProperty) {
				return true;
			}
			MProperty mProperty = (MProperty) edge.getElement();
			if (sourceElement != mProperty.getContainingClassifier() || targetElement != mProperty.getType()) {
				return true;
			}
			break;
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
		case MPropertyInstanceEditPart.VISUAL_ID:
			if (false == edge.getElement() instanceof MPropertyInstance) {
				return true;
			}
			MPropertyInstance mPropertyInstance = (MPropertyInstance) edge.getElement();
			if (sourceElement != mPropertyInstance.getContainingObject()
					|| !(mPropertyInstance.getInternalContainedObject().contains(targetElement) || isReferenceToObject(mPropertyInstance, targetElement))) {
				return true;
			}
			break;
		case MObjectType2EditPart.VISUAL_ID:
			if (false == sourceElement instanceof MObject) {
				return true;
			}
			if (((MObject) sourceElement).getType() != targetElement) {
				return true;
			}
		}
		return false;
	}

	public static boolean isReferenceToObject(MPropertyInstance mPropertyInstance, EObject referencedMObject) {
		for (MObjectReference reference : mPropertyInstance.getInternalReferencedObject()) {
			if (reference.getReferencedObject() == referencedMObject) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("serial")
	protected static class Domain2Notation extends HashMap<EObject, View> {

		/**
		* @generated
		*/
		public boolean containsDomainElement(EObject domainElement) {
			return this.containsKey(domainElement);
		}

		/**
		* @generated
		*/
		public View getHinted(EObject domainEObject, String hint) {
			return this.get(domainEObject);
		}

		/**
		* @generated
		*/
		public void putView(EObject domainElement, View view) {
			this.put(domainElement, view);
		}

	}

	public void refreshMPackage(List<Notification> notifications) {
		removeInvalidElements();
		refreshCreatedMObject(notifications);

		//refresh children
		refresh(getHost());
	}

	private void refresh(IGraphicalEditPart ep) {
		ep.refresh();
		for (Iterator<?> it = ep.getChildren().iterator(); it.hasNext();) {
			Object next = it.next();
			if (next instanceof IGraphicalEditPart) {
				IGraphicalEditPart child = (IGraphicalEditPart) next;
				refresh(child);
			}
		}
	}

	public void refreshCreatedMObject(List<Notification> notifications) {
		for (Notification notification : notifications) {
			if (notification.getFeature() == OBJECT.getMPropertyInstance_InternalContainedObject()) {
				if (notification.getEventType() == Notification.ADD) {
					EObjectAdapter adapter = new EObjectAdapter((EObject) notification.getNewValue());
					ViewDescriptor vd = new ViewDescriptor(adapter, Node.class, ((IHintedType) McoreElementTypes.MObject_2002).getSemanticHint(), -1, true, getHost().getDiagramPreferencesHint());
					Command cmd = getHost().getCommand(new CreateViewRequest(vd));
					executeCommand(cmd);
				}
			}
		}
	}

	/**
	 * 
	 * CanonicalEditPolicy methods
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	public boolean isEnabled() {
		// if the editing domain is null then there is no point in enabling the edit policy
		// the editing domain could be null because the view is detached or if the host is detached
		if (TransactionUtil.getEditingDomain((EObject) getHost().getModel()) == null) {
			return false;
		}
		return true;
	}

	protected final boolean deleteViews(Iterator<View> views) {
		if (!isEnabled()) {
			return false;
		}
		final CompoundCommand cc = new CompoundCommand(DiagramUIMessages.DeleteCommand_Label);
		while (views.hasNext()) {
			View view = (View) views.next();
			if (true) {
				cc.add(getDeleteViewCommand(view));
			}
		}

		boolean doDelete = !cc.isEmpty() && cc.canExecute();
		if (doDelete) {
			executeCommand(cc);
		}
		return doDelete;
	}

	protected void executeCommand(final Command cmd) {
		Map<String, Boolean> options = null;
		EditPart ep = getHost();
		boolean isActivating = true;
		// use the viewer to determine if we are still initializing the diagram
		// do not use the DiagramEditPart.isActivating since ConnectionEditPart's
		// parent will not be a diagram edit part
		EditPartViewer viewer = ep.getViewer();
		if (viewer instanceof DiagramGraphicalViewer) {
			isActivating = ((DiagramGraphicalViewer) viewer).isInitializing();
		}

		if (isActivating || !EditPartUtil.isWriteTransactionInProgress((IGraphicalEditPart) getHost(), false, false))
			options = Collections.singletonMap(Transaction.OPTION_UNPROTECTED, Boolean.TRUE);

		AbstractEMFOperation operation = new AbstractEMFOperation(((IGraphicalEditPart) getHost()).getEditingDomain(), StringStatics.BLANK, options) {

			protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

				cmd.execute();

				return Status.OK_STATUS;
			}
		};
		try {
			operation.execute(new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			McoreDiagramEditorPlugin.getInstance().getLogHelper().logError("", e);
		}
	}

	protected Command getDeleteViewCommand(View view) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		return new ICommandProxy(new DeleteCommand(editingDomain, view));
	}

	private class ResourceListener implements ResourceSetListener {

		@Override
		public NotificationFilter getFilter() {
			NotificationFilter propertyFilter = NotificationFilter.createFeatureFilter(MCORE.getMPropertiesContainer_Property());
			NotificationFilter propertyTypeFilter = NotificationFilter.createFeatureFilter(MCORE.getMExplicitlyTyped_Type());
			NotificationFilter propertyInstanceContainmentFilter = NotificationFilter.createFeatureFilter(OBJECT.getMPropertyInstance_InternalContainedObject());
			NotificationFilter propertyInstanceReferenceFilter = NotificationFilter.createFeatureFilter(OBJECT.getMPropertyInstance_InternalReferencedObject());
			NotificationFilter objectReferenceFilter = NotificationFilter.createFeatureFilter(OBJECT.getMObjectReference_ReferencedObject());
			NotificationFilter objectTypeFilter = NotificationFilter.createFeatureFilter(OBJECT.getMObject_Type());
			NotificationFilter resourceFolderFilter = NotificationFilter.createFeatureFilter(OBJECT.getMResourceFolder_Folder());
			NotificationFilter resourceFilter = NotificationFilter.createFeatureFilter(OBJECT.getMResourceFolder_Resource());
			return propertyFilter//
					.or(propertyTypeFilter)//
					.or(propertyInstanceContainmentFilter)//
					.or(propertyInstanceReferenceFilter)//
					.or(objectReferenceFilter)//
					.or(objectReferenceFilter)//
					.or(objectTypeFilter).or(resourceFolderFilter).or(resourceFilter);
		}

		@Override
		public org.eclipse.emf.common.command.Command transactionAboutToCommit(ResourceSetChangeEvent event) throws RollbackException {
			return null;
		}

		@Override
		public void resourceSetChanged(ResourceSetChangeEvent event) {
			refreshMPackage(event.getNotifications());
		}

		@Override
		public boolean isAggregatePrecommitListener() {
			return false;
		}

		@Override
		public boolean isPrecommitOnly() {
			return false;
		}

		@Override
		public boolean isPostcommitOnly() {
			return true;
		}
	}
}
