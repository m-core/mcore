package com.montages.mcore.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;

public class PolicyUtils {

	/**
	 * Collect all child views which have element is searchedElement
	 * <br>
	 * e.g.<br>
	 * <code>
	 * {@link IGraphicalEditPart} diagramEP = getDiagramEP();<br>
	 * {@link List} views = PolicyUtils.findView(diagramEP.getNotationView());<br>
	 * if (!views.isEmpty()) {<br>
	 * 	//process view<br>
	 * }<br>
	 * </code>
	 * 
	 * @param parentView is container view
	 * @return {@link List} with views
	 */
	public static List<View> findView(EObject searchedElement, View parentView) {
		if (searchedElement == null) {
			return Collections.emptyList();
		}
		List<View> result = new ArrayList<View>();
		for (Object child : parentView.getChildren()) {
			if (child instanceof View) {
				View view = (View) child;
				if (view.getElement() == searchedElement) {
					result.add(view);
				}
			}
		}
		return result;
	}
}
