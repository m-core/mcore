package com.montages.mcore.diagram.edit.policies;

import com.montages.mcore.diagram.draw2d.decorations.AssociationDecoration;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.policies.base.ImpactAnalyzerVisualEffectEditPolicyBase;

/**
 * @generated
 */
public class AggregationDiamondEditPloicy extends ImpactAnalyzerVisualEffectEditPolicyBase {

	/**
	 * @generated
	 */
	public static final String KEY = AggregationDiamondEditPloicy.class.getName() + ":KEY";

	@Override
	protected void setVisualEffectValue(Object value) {
		boolean containment = (Boolean) value;
		AssociationDecoration decoration = (AssociationDecoration) getHostImpl().getPrimaryShape().getSourceDecoration();
		decoration.showDiamond(containment);
	}

	/**
	 * @generated
	 */
	@Override
	protected MPropertyEditPart getHostImpl() {
		return (MPropertyEditPart) super.getHostImpl();
	}

	/**
	 * @generated
	 */
	@Override
	protected String getExpressionBody() {
		return "" + //
				" self.containment";
	}
}
