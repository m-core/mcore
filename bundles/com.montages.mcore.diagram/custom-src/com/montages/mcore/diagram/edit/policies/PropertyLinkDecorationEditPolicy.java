package com.montages.mcore.diagram.edit.policies;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart.PropertyLinkFigure;

public class PropertyLinkDecorationEditPolicy extends AbstractEditPolicy {

	public static final String ROLE = "PropertyLinkDecorationEditPolicy";

	private Adapter semanticListener = new Adapter() {

		private Notifier myTarget;

		@Override
		public void notifyChanged(Notification notification) {
			if (notification.getFeature() == McorePackage.eINSTANCE.getMProperty_Containment()) {
				refresh();
			}
		}

		@Override
		public Notifier getTarget() {
			return myTarget;
		}

		@Override
		public void setTarget(Notifier newTarget) {
			myTarget = newTarget;
		}

		@Override
		public boolean isAdapterForType(Object type) {
			return false;
		}
	};

	@Override
	public void activate() {
		super.activate();
		if (getSemanticElement() != null) {
			getSemanticElement().eAdapters().add(semanticListener);
		}
		refresh();
	}

	@Override
	public void deactivate() {
		if (getSemanticElement() != null) {
			getSemanticElement().eAdapters().remove(semanticListener);
		}
		super.deactivate();
	}

	@Override
	public IGraphicalEditPart getHost() {
		return (IGraphicalEditPart)super.getHost();
	}

	public MProperty getSemanticElement() {
		EObject semanticElement = getHost().resolveSemanticElement();
		if (semanticElement instanceof MProperty) {
			return (MProperty)semanticElement;
		}
		return null;
	}

	public void refresh() {
		MProperty mProperty = getSemanticElement();
		if (mProperty != null) {
			if (mProperty.getContainment() == null || !mProperty.getContainment()) {
				getFigure().removeDiamondDecoration();
			} else {
				getFigure().addDiamondDecoration();
			}
		}
	}

	private PropertyLinkFigure getFigure() {
		return (MPropertyEditPart.PropertyLinkFigure)getHost().getFigure();
	}
}
