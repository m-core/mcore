package com.montages.mcore.diagram.actions;

import com.montages.mcore.diagram.edit.policies.ShowHideSlotEditPolicy;

public class ShowHideSlotsHandler extends ShowHideHandler {

	public ShowHideSlotsHandler() {
		super(new ShowHideSlotAction(), ShowHideSlotEditPolicy.SHOW_HIDE_SLOT_POLICY);
	}
}