package com.montages.mcore.diagram.actions;

import com.montages.mcore.diagram.edit.policies.ShowHideCompartmentEditPolicy;

public class ShowHideCompartmentsHandler extends ShowHideHandler {

	public ShowHideCompartmentsHandler() {
		super(new ShowHideCompartmentsAction(), ShowHideCompartmentEditPolicy.SHOW_HIDE_COMPARTMENTS_POLICY);
	}
}