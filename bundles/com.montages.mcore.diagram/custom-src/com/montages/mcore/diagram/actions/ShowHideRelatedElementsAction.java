package com.montages.mcore.diagram.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IActionDelegate;

import com.montages.mcore.diagram.commands.ShowHideRelatedElementsRequest;
import com.montages.mcore.diagram.commands.ShowHideRelatedElementsRequest.ShowHideKind;
import com.montages.mcore.diagram.edit.policies.ShowHideRelatedElementsEditPolicy;

public class ShowHideRelatedElementsAction implements IActionDelegate {

	private final String myEditPolicyKey = ShowHideRelatedElementsEditPolicy.SHOW_HIDE_RELATED_ELEMENTS_POLICY;

	private List<EditPart> mySelections;

	@Override
	public void run(IAction action) {
		if (!isEnabled()) {
			return;
		}
		final Command command = getActionCommand();
		if (command == null || !command.canExecute()) {
			return;
		}
		executeCommand(command);
	}

	public void setSelection(List<EditPart> selection) {
		mySelections = selection;
	}

	private Command getActionCommand() {
		CompoundCommand completeCmd = new CompoundCommand("Show/Hide command"); //$NON-NLS-1$
		Command cmd = mySelections.get(0).getCommand(new ShowHideRelatedElementsRequest(mySelections, ShowHideKind.SHOW_RELATED_ELEMENTS));
		if (cmd != null && cmd.canExecute()) {
			completeCmd.add(cmd);
		}
		return completeCmd;
	}

	private void executeCommand(Command command) {
		getDomain().getCommandStack().execute(command);
	}

	private EditDomain getDomain() {
		return mySelections.get(0).getRoot().getViewer().getEditDomain();
	}

	public boolean isEnabled() {
		if (mySelections.isEmpty()) {
			return false;
		}
		for (EditPart object : mySelections) {
			EditPolicy policy = object.getEditPolicy(myEditPolicyKey);
			if (policy == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 *
	 * @param action
	 *            the current action
	 * @param selection
	 *            the current selection
	 */
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			if (((StructuredSelection) selection).size() > 1) {
				action.setEnabled(false);
				return;
			}
		}
		boolean enabled = false;
		if (myEditPolicyKey != null) {
			enabled = setupSelectedElelements(selection);
		}
		if (action != null) {
			action.setEnabled(enabled);
		}
	}

	private boolean setupSelectedElelements(ISelection selection) {
		mySelections = new ArrayList<EditPart>();
		if (selection instanceof StructuredSelection) {
			for (Object current : ((StructuredSelection) selection).toArray()) {
				if (false == current instanceof IGraphicalEditPart) {
					continue;
				}
				mySelections.add((IGraphicalEditPart) current);
				EditPolicy policy = ((IGraphicalEditPart) current).getEditPolicy(myEditPolicyKey);
				if (policy != null) {
					return true;
				}
			}
		}
		return false;
	}

}
