package com.montages.mcore.diagram.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.ui.util.DisplayUtils;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IResizableCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.editpart.EditPartService;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;

import com.montages.mcore.diagram.commands.ShowHideElementsRequest;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.providers.EditPartRepresentationLabelProvider;
import com.montages.mcore.diagram.providers.MCoreTreeContentProvider;

public abstract class AbstractShowHideAction implements IActionDelegate, IWorkbenchWindowActionDelegate {

	private List<IGraphicalEditPart> mySelections;

	private ILabelProvider myLabelProvider;

	private final String myTitle;

	private final String myMessage;

	private ITreeContentProvider myContentProvider = null;

	private final String myEditPolicyKey;

	private List<Object> myInitialSelection;

	private List<EditPartRepresentation> myViewsToDestroy;

	private List<EditPartRepresentation> myViewsToCreate;

	private List<EditPartRepresentation> myRepresentations;

	public AbstractShowHideAction(String title, String message, String editPolicyKey) {
		myTitle = title;
		myMessage = message;
		myEditPolicyKey = editPolicyKey;
	}

	public void setSelection(List<IGraphicalEditPart> selection) {
		mySelections = selection;
	}

	/**
	 *
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 *
	 * @param action
	 *            the current action
	 * @param selection
	 *            the current selection
	 */
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		boolean enabled = myEditPolicyKey != null && setupSelectedElelements(selection);
		if (action != null) {
			action.setEnabled(enabled);
		}
	}

	private Object[] createExpandedElements() {
		List<Object> result = new ArrayList<Object>();
		for (EditPartRepresentation current : myRepresentations) {
			result.add(current);
			result.addAll(current.getPossibleElement());
		}
		return result.toArray();
	}

	protected Request createDestroyViewRequest(EditPart editPart) {
		return new ShowHideElementsRequest(editPart);
	}

	private List<Command> getDestroyViewCommands() {
		List<Command> result = new ArrayList<Command>();
		for (EditPartRepresentation current : myViewsToDestroy) {
			EditPart ep = current.getRepresentedEditPart();
			if (ep == null) {
				McoreDiagramEditorPlugin.getInstance().getLogHelper().logError("The edit part for " + current + " representation should not be null");
				continue;
			}
			EditPart parent = ep.getParent();
			if (parent instanceof CompartmentEditPart) {
				parent = parent.getParent();
			}
			Command cmd = parent.getCommand(createDestroyViewRequest(ep));
			if (cmd != null && cmd.canExecute()) {
				result.add(cmd);
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private Command getActionCommand() {
		CompoundCommand completeCmd = new CompoundCommand("Show/Hide command"); //$NON-NLS-1$
		completeCmd.getCommands().addAll(getDestroyViewCommands());
		completeCmd.getCommands().addAll(getCreateViewCommands());
		return completeCmd;
	}

	protected abstract boolean canCreateView(EditPartRepresentation rep);

	protected Request createViewRequest(View view, EObject semantic) {
		return new ShowHideElementsRequest(null, view, semantic);
	}

	private List<Command> getCreateViewCommands() {
		List<Command> result = new ArrayList<Command>();
		for (EditPartRepresentation rep : myViewsToCreate) {
			if (!canCreateView(rep)) {
				continue;
			}
			View compartmentView = null;
			EditPart ep = null;
			if (rep instanceof CompartmentEditPartRepresentation) {
				CompartmentEditPartRepresentation currentRepresentation = (CompartmentEditPartRepresentation) rep;
				compartmentView = currentRepresentation.getView();
				EditPartRepresentation parentRepresentation = currentRepresentation.getParentRepresentation();
				ep = parentRepresentation.getRepresentedEditPart();
			} else {
				ep = rep.getParentRepresentation().getParentRepresentation().getRepresentedEditPart();
				compartmentView = rep.getParentRepresentation().getRepresentedEditPart().getNotationView();
			}
			if (compartmentView == null || ep == null) {
				continue;
			}
			Command cmd = ep.getCommand(createViewRequest(compartmentView, rep.getSemanticElement()));
			if (cmd != null && cmd.canExecute()) {
				result.add(cmd);
			}
		}
		return result;
	}

	protected abstract EditPartRepresentation createRootRepresentation(IGraphicalEditPart ep, EObject semantic, ILabelProvider labelProvider);

	private void initAction() {
		myLabelProvider = new EditPartRepresentationLabelProvider();
		myRepresentations = new ArrayList<EditPartRepresentation>();
		for (IGraphicalEditPart current : mySelections) {
			EObject element = ((View) current.getModel()).getElement();
			myRepresentations.add(createRootRepresentation(current, element, myLabelProvider));
		}
		setupInitialSelection();
		myContentProvider = new MCoreTreeContentProvider();
	}

	private boolean setupSelectedElelements(ISelection selection) {
		mySelections = new ArrayList<IGraphicalEditPart>();
		if (selection instanceof StructuredSelection) {
			for (Object current : ((StructuredSelection) selection).toArray()) {
				if (false == current instanceof IGraphicalEditPart) {
					continue;
				}
				mySelections.add((IGraphicalEditPart) current);
				EditPolicy policy = ((IGraphicalEditPart) current).getEditPolicy(myEditPolicyKey);
				if (policy != null) {
					return true;
				}
			}
		}
		return false;
	}

	private EditDomain getDomain() {
		return mySelections.get(0).getRoot().getViewer().getEditDomain();
	}

	@Override
	public void run(IAction action) {
		if (!isEnabled()) {
			return;
		}
		initAction();
		SelectionDialog selectionDialog = createSelectionDialog();
		selectionDialog.open();
		if (selectionDialog.getReturnCode() != Window.OK) {
			return;
		}
		setupCreateDestroyViewList(selectionDialog.getResult());
		final Command command = getActionCommand();
		if (command == null || !command.canExecute()) {
			return;
		}
		executeCommand(command);
	}

	public boolean isEnabled() {
		if (mySelections.isEmpty()) {
			return false;
		}
		for (IGraphicalEditPart object : mySelections) {
			EditPolicy policy = object.getEditPolicy(myEditPolicyKey);
			if (policy == null) {
				return false;
			}
		}
		return true;
	}

	protected boolean canNotAddToDestroyViewList(Object source) {
		return source instanceof CompartmentRootEditPartRepresentation;
	}

	private void setupCreateDestroyViewList(Object[] source) {
		myViewsToCreate = new ArrayList<EditPartRepresentation>();
		myViewsToDestroy = new ArrayList<EditPartRepresentation>();
		List<Object> filteredSource = new ArrayList<Object>();
		for (int i = 0; i < source.length; i++) {
			if (canNotAddToDestroyViewList(source[i])) {
				continue;
			} else {
				filteredSource.add(source[i]);
			}
		}
		for (Object element : filteredSource) {
			if (myInitialSelection.contains(element)) {
				continue;
			} else if (element instanceof EditPartRepresentation) {
				myViewsToCreate.add((EditPartRepresentation) element);
			}
		}
		for (Object current : myInitialSelection) {
			if (!filteredSource.contains(current) && current instanceof EditPartRepresentation) {
				myViewsToDestroy.add((EditPartRepresentation) current);
			}
		}
	}

	private void executeCommand(Command command) {
		getDomain().getCommandStack().execute(command);
	}

	private SelectionDialog createSelectionDialog() {
		CheckedTreeSelectionDialog selectionDialog = new CheckedTreeSelectionDialog(DisplayUtils.getDisplay().getActiveShell(), myLabelProvider, myContentProvider);
		selectionDialog.setTitle(myTitle);
		selectionDialog.setMessage(myMessage);
		selectionDialog.setContainerMode(true);
		selectionDialog.setInput(new ArrayList<EditPartRepresentation>(myRepresentations));
		selectionDialog.setExpandedElements(createExpandedElements());
		selectionDialog.setInitialElementSelections(myInitialSelection);
		return selectionDialog;
	}

	private void setupInitialSelection() {
		myInitialSelection = new ArrayList<Object>();
		for (EditPartRepresentation current : myRepresentations) {
			setupInitialSelectionChildrens(myInitialSelection, current);
		}
	}

	private void setupInitialSelectionChildrens(List<Object> listToComplete, EditPartRepresentation representation) {
		listToComplete.addAll(representation.getInitialSelection());
		List<EditPartRepresentation> children = representation.getPossibleElement();
		if (children == null) {
			return;
		}
		for (EditPartRepresentation child : children) {
			setupInitialSelectionChildrens(listToComplete, child);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void init(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
	}

	public static class EditPartRepresentation {

		private final EObject mySemantic;

		private final EditPartRepresentation myParentRepresentation;

		private final IGraphicalEditPart myEditPart;

		private final List<EditPartRepresentation> myInitialSelection;

		private final List<EditPartRepresentation> myElementsToSelect;

		private final ILabelProvider myLabelProvider;

		public EditPartRepresentation(IGraphicalEditPart representedEditPart, EObject eObject, ILabelProvider labelProvider) {
			this(representedEditPart, eObject, null, labelProvider);
		}

		public EditPartRepresentation(IGraphicalEditPart representedEditPart, EObject eObject, EditPartRepresentation parentRepresentation, ILabelProvider labelProvider) {
			myEditPart = representedEditPart;
			mySemantic = eObject;
			myParentRepresentation = parentRepresentation;
			myLabelProvider = labelProvider;
			myInitialSelection = new ArrayList<EditPartRepresentation>();
			myElementsToSelect = new ArrayList<EditPartRepresentation>();
			init();
		}

		public IGraphicalEditPart getRepresentedEditPart() {
			return myEditPart;
		}

		public EObject getSemanticElement() {
			return mySemantic;
		}

		public List<EditPartRepresentation> getInitialSelection() {
			return myInitialSelection;
		}

		public List<EditPartRepresentation> getPossibleElement() {
			return myElementsToSelect;
		}

		public EditPartRepresentation getParentRepresentation() {
			return myParentRepresentation;
		}

		public String getLabel() {
			return myLabelProvider.getText(getSemanticElement());
		}

		public Image getImage() {
			return myLabelProvider.getImage(getSemanticElement());
		}

		public ILabelProvider getLabelProvider() {
			return myLabelProvider;
		}

		protected void init() {
		}
	}

	public abstract static class CompartmentRootEditPartRepresentation extends EditPartRepresentation {

		public CompartmentRootEditPartRepresentation(IGraphicalEditPart representedEditPart, EObject element, ILabelProvider labelProvider) {
			super(representedEditPart, element, labelProvider);
		}

		@Override
		protected abstract void init();
	}

	public abstract static class CompartmentEditPartRepresentation extends EditPartRepresentation {

		private final View myView;

		public CompartmentEditPartRepresentation(IResizableCompartmentEditPart compartmentEditPart, View view, EObject element, EditPartRepresentation parentRepresentation,
				ILabelProvider labelProvider) {
			super(compartmentEditPart, element, parentRepresentation, labelProvider);
			myView = view;
		}

		public View getView() {
			return myView;
		}

		@Override
		public String getLabel() {
			if (getRepresentedEditPart() != null && getRepresentedEditPart() instanceof IResizableCompartmentEditPart) {
				return ((IResizableCompartmentEditPart) getRepresentedEditPart()).getCompartmentName();
			} else {
				EditPart dummyEP = EditPartService.getInstance().createGraphicEditPart(myView);
				if (dummyEP instanceof IResizableCompartmentEditPart) {
					return ((IResizableCompartmentEditPart) dummyEP).getCompartmentName();
				}
			}
			return super.getLabel();
		}

		@Override
		protected abstract void init();
	}
}
