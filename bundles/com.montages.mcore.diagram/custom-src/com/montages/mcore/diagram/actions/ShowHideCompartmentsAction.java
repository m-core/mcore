package com.montages.mcore.diagram.actions;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IResizableCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.BasicCompartment;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ILabelProvider;

import com.montages.mcore.diagram.commands.ShowHideCompartmentRequest;
import com.montages.mcore.diagram.edit.policies.ShowHideCompartmentEditPolicy;

public class ShowHideCompartmentsAction extends AbstractShowHideAction {

	public ShowHideCompartmentsAction() {
		super("Show/Hide compartments", "Select elements to show", ShowHideCompartmentEditPolicy.SHOW_HIDE_COMPARTMENTS_POLICY);
	}

	@Override
	protected boolean canCreateView(EditPartRepresentation rep) {
		return rep instanceof CompartmentEditPartRepresentation;
	}

	@Override
	protected Request createDestroyViewRequest(EditPart editPart) {
		return new ShowHideCompartmentRequest(ShowHideCompartmentRequest.HIDE, ((IGraphicalEditPart) editPart).getNotationView());
	}

	@Override
	protected Request createViewRequest(View view, EObject semantic) {
		return new ShowHideCompartmentRequest(ShowHideCompartmentRequest.SHOW, view);
	}

	@Override
	protected EditPartRepresentation createRootRepresentation(IGraphicalEditPart ep, EObject semantic, ILabelProvider labelProvider) {
		return new CompartmentsRootEditPartRepresenation(ep, semantic, labelProvider);
	}

	private static class CompartmentsRootEditPartRepresenation extends CompartmentRootEditPartRepresentation {

		public CompartmentsRootEditPartRepresenation(IGraphicalEditPart representedEditPart, EObject element, ILabelProvider labelProvider) {
			super(representedEditPart, element, labelProvider);
		}

		@Override
		protected void init() {
			for (Object currentObject : getRepresentedEditPart().getNotationView().getChildren()) {
				if (false == currentObject instanceof BasicCompartment) {
					continue;
				}
				View currentView = (View) currentObject;
				boolean memberViewPresent = false;
				IResizableCompartmentEditPart compartmentEP = null;
				for (Object nextChildren : getRepresentedEditPart().getChildren()) {
					if (false == nextChildren instanceof IResizableCompartmentEditPart) {
						continue;
					}
					if (nextChildren instanceof ITextAwareEditPart) {
						continue;
					}
					if (currentView.equals(((IGraphicalEditPart) nextChildren).getNotationView())) {
						compartmentEP = (IResizableCompartmentEditPart) nextChildren;
						memberViewPresent = true;
						break;
					}
				}
				CompartmentEditPartRepresentation nextMemberEditPart = new CompartmentsCompartmentEditPartRepresentation(compartmentEP, currentView, getSemanticElement(), this, getLabelProvider());
				getPossibleElement().add(nextMemberEditPart);
				if (memberViewPresent) {
					getInitialSelection().add(nextMemberEditPart);
				}
			}
		}
	}

	private static class CompartmentsCompartmentEditPartRepresentation extends CompartmentEditPartRepresentation {

		public CompartmentsCompartmentEditPartRepresentation(IResizableCompartmentEditPart compartmentEditPart, View view, EObject element, EditPartRepresentation parentRepresentation,
				ILabelProvider labelProvider) {
			super(compartmentEditPart, view, element, parentRepresentation, labelProvider);
		}

		@Override
		protected void init() {
		}
	}
}