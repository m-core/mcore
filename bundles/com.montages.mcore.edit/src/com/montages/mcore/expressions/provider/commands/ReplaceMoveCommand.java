package com.montages.mcore.expressions.provider.commands;

import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MChainOrApplication;
import com.montages.mcore.expressions.MIf;
import com.montages.mcore.expressions.MThen;

/**
 * ReplaceMoveCommand
 * 
 * Special Command used on MChain that allow replacement of
 * child elements of type MAppplication, MChain by MApplication 
 * or MIf and move replaced element as child of newly created element.
 * 
//- if MChain -> can be replaced by MApplication or MIf
//
//		- if creation of MApplication is requested: 
//			-	create an MApplication instance
//			-	move MChain to "operands" feature of MApplication
//
//		- if creation of MIf is requested:
//			-	create an MIf instance, 
//			-	create an MThen instance, 
//			-	move MChain in MThen

//	- if MApplication -> can be replaced by MApplication or MIf
//  - if MIf -> do not allow to create any instance
**/
public class ReplaceMoveCommand extends CommandWrapper {

	private final EObject owner;
	private final EStructuralFeature feature;
	private final EObject previous;
	
	public ReplaceMoveCommand(EditingDomain domain, 
			EObject owner, 
			EStructuralFeature feature, 
			Object child, 
			EObject previous) {
		
		super(SetCommand.create(domain, owner, feature, child));
		this.owner = owner;
		this.feature = feature;
		this.previous = previous;
	}
	
	public void execute() {
		super.execute();
		
		Object child = owner.eGet(feature);
		
		if (child instanceof MIf) {
			executeOnMIf((MIf) child, previous);
		} else if (child instanceof MApplication) {
			executeOnMApplication((MApplication) child, previous);
		}
	};
		
	private void executeOnMIf(MIf next, EObject previous) {
		if (next.getThenPart() == null) {
			MThen then = ExpressionsFactory.eINSTANCE.createMThen();
			next.setThenPart(then);
		}
		next.getThenPart().setExpression((MChainOrApplication) previous);
	}
	
	private void executeOnMApplication(MApplication next, EObject previous) {
		// clear init values
		next.getOperands().clear();
		next.getOperands().add((MChainOrApplication) previous);
	}
}