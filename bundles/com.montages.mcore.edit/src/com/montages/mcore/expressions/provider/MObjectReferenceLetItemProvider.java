/**
 */
package com.montages.mcore.expressions.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.MObjectReferenceLetImpl;
import com.montages.mcore.objects.MObject;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.expressions.MObjectReferenceLet} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MObjectReferenceLetItemProvider extends MConstantLetItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectReferenceLetItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addObjectTypePropertyDescriptor(object);
			addConstant1PropertyDescriptor(object);
			addConstant2PropertyDescriptor(object);
			addConstant3PropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReferenceLet_objectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReferenceLet_objectType_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__OBJECT_TYPE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Constant1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant1 feature.
		 * The list of possible choices is constraint by OCL if objectType.oclIsUndefined() then false
		else trg.type = objectType endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReferenceLet_constant1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReferenceLet_constant1_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__CONSTANT1,
				true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MObject> result = new ArrayList<MObject>();
				Collection<? extends MObject> superResult = (Collection<? extends MObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MObject> iterator = result.iterator(); iterator
						.hasNext();) {
					MObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceLetImpl) object)
							.evalConstant1ChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Constant2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant2 feature.
		 * The list of possible choices is constraint by OCL if objectType.oclIsUndefined() then false
		else trg.type = objectType endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReferenceLet_constant2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReferenceLet_constant2_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__CONSTANT2,
				true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MObject> result = new ArrayList<MObject>();
				Collection<? extends MObject> superResult = (Collection<? extends MObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MObject> iterator = result.iterator(); iterator
						.hasNext();) {
					MObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceLetImpl) object)
							.evalConstant2ChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Constant3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant3 feature.
		 * The list of possible choices is constraint by OCL if objectType.oclIsUndefined() then false
		else trg.type = objectType endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReferenceLet_constant3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReferenceLet_constant3_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__CONSTANT3,
				true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MObject> result = new ArrayList<MObject>();
				Collection<? extends MObject> superResult = (Collection<? extends MObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MObject> iterator = result.iterator(); iterator
						.hasNext();) {
					MObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceLetImpl) object)
							.evalConstant3ChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MObjectReferenceLetImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
