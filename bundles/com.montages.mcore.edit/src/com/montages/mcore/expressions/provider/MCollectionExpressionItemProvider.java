/**
 */
package com.montages.mcore.expressions.provider;

import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.impl.MCollectionExpressionImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.expressions.MCollectionExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MCollectionExpressionItemProvider
		extends MAbstractExpressionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCollectionOperatorPropertyDescriptor(object);
			addCollectionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Collection Operator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionOperatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Collection Operator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MCollectionExpression_collectionOperator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MCollectionExpression_collectionOperator_feature",
						"_UI_MCollectionExpression_type"),
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Collection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Collection feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MCollectionExpression_collection_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MCollectionExpression_collection_feature",
						"_UI_MCollectionExpression_type"),
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__COLLECTION,
				false, false, false, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__ITERATOR_VAR);
			childrenFeatures.add(
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR);
			childrenFeatures.add(
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MCollectionExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator()
				.getImage("full/obj16/MCollectionExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MCollectionExpressionImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MCollectionExpression.class)) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR:
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR:
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__ITERATOR_VAR,
				ExpressionsFactory.eINSTANCE.createMIterator()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR,
				ExpressionsFactory.eINSTANCE.createMAccumulator()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMChain()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMDataValueExpr()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMLiteralValueExpr()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMApplication()));
	}

}
