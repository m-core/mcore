/**
 */
package com.montages.mcore.expressions.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import com.montages.mcore.expressions.util.ExpressionsAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpressionsItemProviderAdapterFactory
		extends ExpressionsAdapterFactory
		implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MAbstractExpressionWithBase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractExpressionWithBaseItemProvider mAbstractExpressionWithBaseItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MAbstractExpressionWithBase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMAbstractExpressionWithBaseAdapter() {
		if (mAbstractExpressionWithBaseItemProvider == null) {
			mAbstractExpressionWithBaseItemProvider = new MAbstractExpressionWithBaseItemProvider(
					this);
		}

		return mAbstractExpressionWithBaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MContainerBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MContainerBaseDefinitionItemProvider mContainerBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MContainerBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMContainerBaseDefinitionAdapter() {
		if (mContainerBaseDefinitionItemProvider == null) {
			mContainerBaseDefinitionItemProvider = new MContainerBaseDefinitionItemProvider(
					this);
		}

		return mContainerBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MBaseDefinitionItemProvider mBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMBaseDefinitionAdapter() {
		if (mBaseDefinitionItemProvider == null) {
			mBaseDefinitionItemProvider = new MBaseDefinitionItemProvider(this);
		}

		return mBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MNumberBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNumberBaseDefinitionItemProvider mNumberBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MNumberBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMNumberBaseDefinitionAdapter() {
		if (mNumberBaseDefinitionItemProvider == null) {
			mNumberBaseDefinitionItemProvider = new MNumberBaseDefinitionItemProvider(
					this);
		}

		return mNumberBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MSelfBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSelfBaseDefinitionItemProvider mSelfBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MSelfBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMSelfBaseDefinitionAdapter() {
		if (mSelfBaseDefinitionItemProvider == null) {
			mSelfBaseDefinitionItemProvider = new MSelfBaseDefinitionItemProvider(
					this);
		}

		return mSelfBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MTargetBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MTargetBaseDefinitionItemProvider mTargetBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MTargetBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMTargetBaseDefinitionAdapter() {
		if (mTargetBaseDefinitionItemProvider == null) {
			mTargetBaseDefinitionItemProvider = new MTargetBaseDefinitionItemProvider(
					this);
		}

		return mTargetBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MSourceBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSourceBaseDefinitionItemProvider mSourceBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MSourceBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMSourceBaseDefinitionAdapter() {
		if (mSourceBaseDefinitionItemProvider == null) {
			mSourceBaseDefinitionItemProvider = new MSourceBaseDefinitionItemProvider(
					this);
		}

		return mSourceBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MObjectBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectBaseDefinitionItemProvider mObjectBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MObjectBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMObjectBaseDefinitionAdapter() {
		if (mObjectBaseDefinitionItemProvider == null) {
			mObjectBaseDefinitionItemProvider = new MObjectBaseDefinitionItemProvider(
					this);
		}

		return mObjectBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSimpleTypeConstantBaseDefinitionItemProvider mSimpleTypeConstantBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMSimpleTypeConstantBaseDefinitionAdapter() {
		if (mSimpleTypeConstantBaseDefinitionItemProvider == null) {
			mSimpleTypeConstantBaseDefinitionItemProvider = new MSimpleTypeConstantBaseDefinitionItemProvider(
					this);
		}

		return mSimpleTypeConstantBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MLiteralConstantBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MLiteralConstantBaseDefinitionItemProvider mLiteralConstantBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MLiteralConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMLiteralConstantBaseDefinitionAdapter() {
		if (mLiteralConstantBaseDefinitionItemProvider == null) {
			mLiteralConstantBaseDefinitionItemProvider = new MLiteralConstantBaseDefinitionItemProvider(
					this);
		}

		return mLiteralConstantBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectReferenceConstantBaseDefinitionItemProvider mObjectReferenceConstantBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMObjectReferenceConstantBaseDefinitionAdapter() {
		if (mObjectReferenceConstantBaseDefinitionItemProvider == null) {
			mObjectReferenceConstantBaseDefinitionItemProvider = new MObjectReferenceConstantBaseDefinitionItemProvider(
					this);
		}

		return mObjectReferenceConstantBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MVariableBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MVariableBaseDefinitionItemProvider mVariableBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MVariableBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMVariableBaseDefinitionAdapter() {
		if (mVariableBaseDefinitionItemProvider == null) {
			mVariableBaseDefinitionItemProvider = new MVariableBaseDefinitionItemProvider(
					this);
		}

		return mVariableBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MParameterBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MParameterBaseDefinitionItemProvider mParameterBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MParameterBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMParameterBaseDefinitionAdapter() {
		if (mParameterBaseDefinitionItemProvider == null) {
			mParameterBaseDefinitionItemProvider = new MParameterBaseDefinitionItemProvider(
					this);
		}

		return mParameterBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MIteratorBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MIteratorBaseDefinitionItemProvider mIteratorBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MIteratorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMIteratorBaseDefinitionAdapter() {
		if (mIteratorBaseDefinitionItemProvider == null) {
			mIteratorBaseDefinitionItemProvider = new MIteratorBaseDefinitionItemProvider(
					this);
		}

		return mIteratorBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MAccumulatorBaseDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAccumulatorBaseDefinitionItemProvider mAccumulatorBaseDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MAccumulatorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMAccumulatorBaseDefinitionAdapter() {
		if (mAccumulatorBaseDefinitionItemProvider == null) {
			mAccumulatorBaseDefinitionItemProvider = new MAccumulatorBaseDefinitionItemProvider(
					this);
		}

		return mAccumulatorBaseDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MTuple} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MTupleItemProvider mTupleItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MTuple}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMTupleAdapter() {
		if (mTupleItemProvider == null) {
			mTupleItemProvider = new MTupleItemProvider(this);
		}

		return mTupleItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MTupleEntry} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MTupleEntryItemProvider mTupleEntryItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MTupleEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMTupleEntryAdapter() {
		if (mTupleEntryItemProvider == null) {
			mTupleEntryItemProvider = new MTupleEntryItemProvider(this);
		}

		return mTupleEntryItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MNewObjecct} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNewObjecctItemProvider mNewObjecctItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MNewObjecct}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMNewObjecctAdapter() {
		if (mNewObjecctItemProvider == null) {
			mNewObjecctItemProvider = new MNewObjecctItemProvider(this);
		}

		return mNewObjecctItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MNewObjectFeatureValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNewObjectFeatureValueItemProvider mNewObjectFeatureValueItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MNewObjectFeatureValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMNewObjectFeatureValueAdapter() {
		if (mNewObjectFeatureValueItemProvider == null) {
			mNewObjectFeatureValueItemProvider = new MNewObjectFeatureValueItemProvider(
					this);
		}

		return mNewObjectFeatureValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MNamedExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNamedExpressionItemProvider mNamedExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MNamedExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMNamedExpressionAdapter() {
		if (mNamedExpressionItemProvider == null) {
			mNamedExpressionItemProvider = new MNamedExpressionItemProvider(
					this);
		}

		return mNamedExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MProcessorDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MProcessorDefinitionItemProvider mProcessorDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MProcessorDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMProcessorDefinitionAdapter() {
		if (mProcessorDefinitionItemProvider == null) {
			mProcessorDefinitionItemProvider = new MProcessorDefinitionItemProvider(
					this);
		}

		return mProcessorDefinitionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MChain} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MChainItemProvider mChainItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MChain}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMChainAdapter() {
		if (mChainItemProvider == null) {
			mChainItemProvider = new MChainItemProvider(this);
		}

		return mChainItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MCallArgument} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCallArgumentItemProvider mCallArgumentItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MCallArgument}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMCallArgumentAdapter() {
		if (mCallArgumentItemProvider == null) {
			mCallArgumentItemProvider = new MCallArgumentItemProvider(this);
		}

		return mCallArgumentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MDataValueExpr} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MDataValueExprItemProvider mDataValueExprItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MDataValueExpr}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMDataValueExprAdapter() {
		if (mDataValueExprItemProvider == null) {
			mDataValueExprItemProvider = new MDataValueExprItemProvider(this);
		}

		return mDataValueExprItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MLiteralValueExpr} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MLiteralValueExprItemProvider mLiteralValueExprItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MLiteralValueExpr}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMLiteralValueExprAdapter() {
		if (mLiteralValueExprItemProvider == null) {
			mLiteralValueExprItemProvider = new MLiteralValueExprItemProvider(
					this);
		}

		return mLiteralValueExprItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MIf} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MIfItemProvider mIfItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MIf}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMIfAdapter() {
		if (mIfItemProvider == null) {
			mIfItemProvider = new MIfItemProvider(this);
		}

		return mIfItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MThen} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MThenItemProvider mThenItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MThen}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMThenAdapter() {
		if (mThenItemProvider == null) {
			mThenItemProvider = new MThenItemProvider(this);
		}

		return mThenItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MElseIf} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MElseIfItemProvider mElseIfItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MElseIf}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMElseIfAdapter() {
		if (mElseIfItemProvider == null) {
			mElseIfItemProvider = new MElseIfItemProvider(this);
		}

		return mElseIfItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MElse} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MElseItemProvider mElseItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MElse}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMElseAdapter() {
		if (mElseItemProvider == null) {
			mElseItemProvider = new MElseItemProvider(this);
		}

		return mElseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MSubChain} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSubChainItemProvider mSubChainItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MSubChain}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMSubChainAdapter() {
		if (mSubChainItemProvider == null) {
			mSubChainItemProvider = new MSubChainItemProvider(this);
		}

		return mSubChainItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MCollectionExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCollectionExpressionItemProvider mCollectionExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MCollectionExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMCollectionExpressionAdapter() {
		if (mCollectionExpressionItemProvider == null) {
			mCollectionExpressionItemProvider = new MCollectionExpressionItemProvider(
					this);
		}

		return mCollectionExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MIterator} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MIteratorItemProvider mIteratorItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MIterator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMIteratorAdapter() {
		if (mIteratorItemProvider == null) {
			mIteratorItemProvider = new MIteratorItemProvider(this);
		}

		return mIteratorItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MAccumulator} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAccumulatorItemProvider mAccumulatorItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MAccumulator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMAccumulatorAdapter() {
		if (mAccumulatorItemProvider == null) {
			mAccumulatorItemProvider = new MAccumulatorItemProvider(this);
		}

		return mAccumulatorItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MNamedConstant} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNamedConstantItemProvider mNamedConstantItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MNamedConstant}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMNamedConstantAdapter() {
		if (mNamedConstantItemProvider == null) {
			mNamedConstantItemProvider = new MNamedConstantItemProvider(this);
		}

		return mNamedConstantItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MSimpleTypeConstantLet} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSimpleTypeConstantLetItemProvider mSimpleTypeConstantLetItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MSimpleTypeConstantLet}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMSimpleTypeConstantLetAdapter() {
		if (mSimpleTypeConstantLetItemProvider == null) {
			mSimpleTypeConstantLetItemProvider = new MSimpleTypeConstantLetItemProvider(
					this);
		}

		return mSimpleTypeConstantLetItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MLiteralLet} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MLiteralLetItemProvider mLiteralLetItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MLiteralLet}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMLiteralLetAdapter() {
		if (mLiteralLetItemProvider == null) {
			mLiteralLetItemProvider = new MLiteralLetItemProvider(this);
		}

		return mLiteralLetItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MApplication} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MApplicationItemProvider mApplicationItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MApplication}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMApplicationAdapter() {
		if (mApplicationItemProvider == null) {
			mApplicationItemProvider = new MApplicationItemProvider(this);
		}

		return mApplicationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link com.montages.mcore.expressions.MOperatorDefinition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MOperatorDefinitionItemProvider mOperatorDefinitionItemProvider;

	/**
	 * This creates an adapter for a {@link com.montages.mcore.expressions.MOperatorDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMOperatorDefinitionAdapter() {
		if (mOperatorDefinitionItemProvider == null) {
			mOperatorDefinitionItemProvider = new MOperatorDefinitionItemProvider(
					this);
		}

		return mOperatorDefinitionItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this
				: parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(
			ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>)
					|| (((Class<?>) type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (mAbstractExpressionWithBaseItemProvider != null)
			mAbstractExpressionWithBaseItemProvider.dispose();
		if (mContainerBaseDefinitionItemProvider != null)
			mContainerBaseDefinitionItemProvider.dispose();
		if (mBaseDefinitionItemProvider != null)
			mBaseDefinitionItemProvider.dispose();
		if (mNumberBaseDefinitionItemProvider != null)
			mNumberBaseDefinitionItemProvider.dispose();
		if (mSelfBaseDefinitionItemProvider != null)
			mSelfBaseDefinitionItemProvider.dispose();
		if (mTargetBaseDefinitionItemProvider != null)
			mTargetBaseDefinitionItemProvider.dispose();
		if (mSourceBaseDefinitionItemProvider != null)
			mSourceBaseDefinitionItemProvider.dispose();
		if (mObjectBaseDefinitionItemProvider != null)
			mObjectBaseDefinitionItemProvider.dispose();
		if (mSimpleTypeConstantBaseDefinitionItemProvider != null)
			mSimpleTypeConstantBaseDefinitionItemProvider.dispose();
		if (mLiteralConstantBaseDefinitionItemProvider != null)
			mLiteralConstantBaseDefinitionItemProvider.dispose();
		if (mObjectReferenceConstantBaseDefinitionItemProvider != null)
			mObjectReferenceConstantBaseDefinitionItemProvider.dispose();
		if (mVariableBaseDefinitionItemProvider != null)
			mVariableBaseDefinitionItemProvider.dispose();
		if (mIteratorBaseDefinitionItemProvider != null)
			mIteratorBaseDefinitionItemProvider.dispose();
		if (mParameterBaseDefinitionItemProvider != null)
			mParameterBaseDefinitionItemProvider.dispose();
		if (mAccumulatorBaseDefinitionItemProvider != null)
			mAccumulatorBaseDefinitionItemProvider.dispose();
		if (mTupleItemProvider != null)
			mTupleItemProvider.dispose();
		if (mTupleEntryItemProvider != null)
			mTupleEntryItemProvider.dispose();
		if (mNewObjecctItemProvider != null)
			mNewObjecctItemProvider.dispose();
		if (mNewObjectFeatureValueItemProvider != null)
			mNewObjectFeatureValueItemProvider.dispose();
		if (mNamedExpressionItemProvider != null)
			mNamedExpressionItemProvider.dispose();
		if (mProcessorDefinitionItemProvider != null)
			mProcessorDefinitionItemProvider.dispose();
		if (mChainItemProvider != null)
			mChainItemProvider.dispose();
		if (mCallArgumentItemProvider != null)
			mCallArgumentItemProvider.dispose();
		if (mSubChainItemProvider != null)
			mSubChainItemProvider.dispose();
		if (mDataValueExprItemProvider != null)
			mDataValueExprItemProvider.dispose();
		if (mLiteralValueExprItemProvider != null)
			mLiteralValueExprItemProvider.dispose();
		if (mIfItemProvider != null)
			mIfItemProvider.dispose();
		if (mThenItemProvider != null)
			mThenItemProvider.dispose();
		if (mElseIfItemProvider != null)
			mElseIfItemProvider.dispose();
		if (mElseItemProvider != null)
			mElseItemProvider.dispose();
		if (mCollectionExpressionItemProvider != null)
			mCollectionExpressionItemProvider.dispose();
		if (mIteratorItemProvider != null)
			mIteratorItemProvider.dispose();
		if (mAccumulatorItemProvider != null)
			mAccumulatorItemProvider.dispose();
		if (mNamedConstantItemProvider != null)
			mNamedConstantItemProvider.dispose();
		if (mSimpleTypeConstantLetItemProvider != null)
			mSimpleTypeConstantLetItemProvider.dispose();
		if (mLiteralLetItemProvider != null)
			mLiteralLetItemProvider.dispose();
		if (mApplicationItemProvider != null)
			mApplicationItemProvider.dispose();
		if (mOperatorDefinitionItemProvider != null)
			mOperatorDefinitionItemProvider.dispose();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final boolean HIDE_ADVANCED_PROPERTIES = false;

}
