package com.montages.mcore.expressions.provider.commands;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MIf;

/**
 * {@link ReplaceMoveChildCommand}
 *  
 * Specific {@link CreateChildCommand} for {@link MChain} that allows replacement 
 * of already set unary containment features.
 *
 */
public class ReplaceMoveChildCommand extends CreateChildCommand {

	public ReplaceMoveChildCommand(EditingDomain domain, 
			EObject owner,
			EStructuralFeature feature, 
			Object child, 
			Collection<?> selection) {

		super(domain, owner, feature, child, selection);
	}

	public ReplaceMoveChildCommand(EditingDomain domain,
			EObject owner,
			EStructuralFeature feature,
			Object child,
			int index,
			Collection<?> selection,
			CreateChildCommand.Helper helper) {

		super(domain, owner, feature, child, index, selection,helper);
	}

	@Override
	protected Command createCommand() {
		if (owner == null || feature == null || child == null)  {
			return UnexecutableCommand.INSTANCE;
		}

		if (feature.isMany())  {
			return AddCommand.create(domain, owner, feature, child, index);
		} else {
			if (owner.eGet(feature) == null)  {
				return SetCommand.create(domain, owner, feature, child);
			} else  {
				final EObject previousValue = (EObject) owner.eGet(feature);
				if (canReplace(previousValue, child))  {
					return new ReplaceMoveCommand(domain, owner, feature, child, previousValue);
				} else  {
					return UnexecutableCommand.INSTANCE;
				}
			}
		}
	}

	private boolean canReplace(EObject previous, Object next) {
		if (previous instanceof MChain)
			return next instanceof MIf || next instanceof MApplication;
		else if (previous instanceof MApplication)
			return next instanceof MIf || next instanceof MApplication;
		else return false;
	}

	public static Command create(EditingDomain domain, 
			Object owner, 
			Object descriptor, 
			Collection<?> selection) {

		CommandParameter parameter = new CommandParameter(owner, null, descriptor, selection);
		return 	domain.createCommand(ReplaceMoveChildCommand.class, parameter);
	}

}
