/**
 */
package com.montages.mcore.objects.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MLiteral;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.MLiteralValueImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.objects.MLiteralValue} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MLiteralValueItemProvider extends MValueItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralValueItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLiteralValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Literal Value feature.
		 * The list of possible choices is constraint by OCL let p:MProperty=self.containingPropertyInstance.property in
		if p.oclIsUndefined() 
		then  let pos:Integer
		           =self.containingPropertyInstance
		                .internalLiteralValue->indexOf(self) in
		if pos=1 
		   then true
		else if trg.containingEnumeration.oclIsUndefined() 
		   then false
		else let firstLiteralValue:MLiteralValue 
		         =  self.containingPropertyInstance 
		               .internalLiteralValue->first() in
		   if firstLiteralValue.literalValue.oclIsUndefined() 
		 then false
		 else firstLiteralValue.literalValue.containingEnumeration
		             = trg.containingEnumeration endif endif endif
		else if p.type.oclIsUndefined() 
		then false 
		else p.type.allLiterals()->includes(trg)
		endif endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MLiteralValue_literalValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralValue_literalValue_feature",
						"_UI_MLiteralValue_type"),
				ObjectsPackage.Literals.MLITERAL_VALUE__LITERAL_VALUE, true,
				false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MLiteral> result = new ArrayList<MLiteral>();
				Collection<? extends MLiteral> superResult = (Collection<? extends MLiteral>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MLiteral> iterator = result.iterator(); iterator
						.hasNext();) {
					MLiteral trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MLiteralValueImpl) object)
							.evalLiteralValueChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This returns MLiteralValue.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MLiteralValue"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MLiteralValueImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
