/**
 */
package com.montages.mcore.objects.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XUpdate;

import com.montages.mcore.objects.MResourceAction;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.MResourceFolderAction;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.MResourceFolderImpl;
import com.montages.mcore.provider.MNamedItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;
import com.montages.mcore.provider.commands.MCoreCommandClasses.MResource_OpenDynamicEditorCommand;

/**
 * This is the item provider adapter for a
 * {@link com.montages.mcore.objects.MResourceFolder} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class MResourceFolderItemProvider extends MNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MResourceFolderItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc --> Override SetCommand for XUpdate execution <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */

	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		XUpdate myUpdate = null;
		if (owner instanceof MResourceFolder) {
			MResourceFolder typedOwner = ((MResourceFolder) owner);

			if (feature == ObjectsPackage.eINSTANCE
					.getMResourceFolder_DoAction()) {
				if (value == MResourceFolderAction.OPEN_EDITOR) {
					MResourceFolder folder = (MResourceFolder) owner;

					if (!folder.getResource().isEmpty()) {
						return new MResource_OpenDynamicEditorCommand(domain,
								folder.getResource().get(0),
								ObjectsPackage.eINSTANCE
										.getMResource_DoAction(),
								MResourceAction.OPEN_EDITOR);
					} else if (!folder.getFolder().isEmpty()) {
						return createSetCommand(domain,
								folder.getFolder().get(0), feature, value,
								index);
					}
				}
				myUpdate = typedOwner.doAction$Update(
						(com.montages.mcore.objects.MResourceFolderAction) value);

			}

		}

		return myUpdate == null
				? super.createSetCommand(domain, owner, feature, value, index)
				: myUpdate.getContainingTransition()
						.interpreteTransitionAsCommand();

	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addRootFolderPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Root Folder feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRootFolderPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Root Folder feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MResourceFolder_rootFolder_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MResourceFolder_rootFolder_feature",
						"_UI_MResourceFolder_type"),
				ObjectsPackage.Literals.MRESOURCE_FOLDER__ROOT_FOLDER, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL if self.rootFolder = self or self.indentLevel = 1 then OrderedSet{
		mcore::objects::MResourceFolderAction::Do, 
		mcore::objects::MResourceFolderAction::AddProject}
		else OrderedSet{
		mcore::objects::MResourceFolderAction::Do, 
		mcore::objects::MResourceFolderAction::AddFolder,
		mcore::objects::MResourceFolderAction::AddResource}  endif           
		->append(MResourceFolderAction::OpenEditor)                         
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MResourceFolder_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MResourceFolder_doAction_feature",
						"_UI_MResourceFolder_type"),
				ObjectsPackage.Literals.MRESOURCE_FOLDER__DO_ACTION, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MResourceFolderAction> result = new ArrayList<MResourceFolderAction>();
				Collection<? extends MResourceFolderAction> superResult = (Collection<? extends MResourceFolderAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MResourceFolderImpl) object)
						.evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ObjectsPackage.Literals.MRESOURCE_FOLDER__RESOURCE);
			childrenFeatures
					.add(ObjectsPackage.Literals.MRESOURCE_FOLDER__FOLDER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MResourceFolder.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Association_none"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MResourceFolderImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MResourceFolder.class)) {
		case ObjectsPackage.MRESOURCE_FOLDER__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case ObjectsPackage.MRESOURCE_FOLDER__RESOURCE:
		case ObjectsPackage.MRESOURCE_FOLDER__FOLDER:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MRESOURCE_FOLDER__RESOURCE,
				ObjectsFactory.eINSTANCE.createMResource()));

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MRESOURCE_FOLDER__FOLDER,
				ObjectsFactory.eINSTANCE.createMResourceFolder()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ObjectsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
