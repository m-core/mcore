/**
 */
package com.montages.mcore.objects.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import com.montages.mcore.objects.MValue;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.provider.MRepositoryElementItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.objects.MValue} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MValueItemProvider extends MRepositoryElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MValueItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContainingPropertyInstancePropertyDescriptor(object);
			addContainingObjectPropertyDescriptor(object);
			addCorrectAndDefinedValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Containing Property Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingPropertyInstancePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Containing Property Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MValue_containingPropertyInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MValue_containingPropertyInstance_feature",
						"_UI_MValue_type"),
				ObjectsPackage.Literals.MVALUE__CONTAINING_PROPERTY_INSTANCE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Containing Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MValue_containingObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MValue_containingObject_feature",
						"_UI_MValue_type"),
				ObjectsPackage.Literals.MVALUE__CONTAINING_OBJECT, false, false,
				false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Correct And Defined Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectAndDefinedValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correct And Defined Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MValue_correctAndDefinedValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MValue_correctAndDefinedValue_feature",
						"_UI_MValue_type"),
				ObjectsPackage.Literals.MVALUE__CORRECT_AND_DEFINED_VALUE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MValue) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MValue.class)) {
		case ObjectsPackage.MVALUE__CORRECT_AND_DEFINED_VALUE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

}
