/**
 */
package com.montages.mcore.objects.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XUpdate;

import com.montages.mcore.MProperty;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MPropertyInstanceAction;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.MPropertyInstanceImpl;
import com.montages.mcore.provider.MRepositoryElementItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.objects.MPropertyInstance} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MPropertyInstanceItemProvider
		extends MRepositoryElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstanceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		if (owner instanceof MPropertyInstance)
			if (feature == ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION)
				if (value instanceof String) {
					XUpdate myUpdate = ((MPropertyInstance) owner)
							.propertyNameOrLocalKeyDefinitionUpdate(
									(String) value);

					return myUpdate == null ? null
							: myUpdate.getContainingTransition()
									.interpreteTransitionAsCommand();
				}

		return super.createSetCommand(domain, owner, feature, value, index);

	};

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContainingObjectPropertyDescriptor(object);
			addDuplicateInstancePropertyDescriptor(object);
			addAllInstancesOfThisPropertyPropertyDescriptor(object);
			addWrongInternalContainedObjectDueToKindPropertyDescriptor(object);
			addWrongInternalObjectReferenceDueToKindPropertyDescriptor(object);
			addWrongInternalLiteralValueDueToKindPropertyDescriptor(object);
			addWrongInternalDataValueDueToKindPropertyDescriptor(object);
			addNumericIdOfPropertyInstancePropertyDescriptor(object);
			addDerivedContainmentPropertyDescriptor(object);
			addLocalKeyPropertyDescriptor(object);
			addPropertyPropertyDescriptor(object);
			addKeyPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addPropertyNameOrLocalKeyDefinitionPropertyDescriptor(object);
			}
			addWrongLocalKeyPropertyDescriptor(object);
			addDerivedKindPropertyDescriptor(object);
			addPropertyKindAsStringPropertyDescriptor(object);
			addCorrectPropertyKindPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoAddValuePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSimpleTypeFromValuesPropertyDescriptor(object);
			}
			addEnumerationFromValuesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Property feature.
		 * The list of possible choices is constructed by OCL self.choosableProperties()
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_property_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_property_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY, true,
				false, false, null,
				getString("_UI_KeyandPropertyPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyInstanceImpl) object)
						.evalPropertyChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Property Name Or Local Key Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyNameOrLocalKeyDefinitionPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Property Name Or Local Key Definition feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_propertyNameOrLocalKeyDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_propertyNameOrLocalKeyDefinition_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_containingObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_containingObject_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__CONTAINING_OBJECT,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Duplicate Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDuplicateInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Duplicate Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_duplicateInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_duplicateInstance_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__DUPLICATE_INSTANCE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the All Instances Of This Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllInstancesOfThisPropertyPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the All Instances Of This Property feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_allInstancesOfThisProperty_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_allInstancesOfThisProperty_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Wrong Internal Contained Object Due To Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWrongInternalContainedObjectDueToKindPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Wrong Internal Contained Object Due To Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_wrongInternalContainedObjectDueToKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_wrongInternalContainedObjectDueToKind_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Wrong Internal Object Reference Due To Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWrongInternalObjectReferenceDueToKindPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Wrong Internal Object Reference Due To Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_wrongInternalObjectReferenceDueToKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_wrongInternalObjectReferenceDueToKind_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Wrong Internal Literal Value Due To Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWrongInternalLiteralValueDueToKindPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Wrong Internal Literal Value Due To Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_wrongInternalLiteralValueDueToKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_wrongInternalLiteralValueDueToKind_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Wrong Internal Data Value Due To Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWrongInternalDataValueDueToKindPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Wrong Internal Data Value Due To Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_wrongInternalDataValueDueToKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_wrongInternalDataValueDueToKind_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Local Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Key feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_localKey_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_localKey_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__LOCAL_KEY, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Wrong Local Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWrongLocalKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Wrong Local Key feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_wrongLocalKey_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_wrongLocalKey_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__WRONG_LOCAL_KEY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Key feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_key_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_key_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__KEY, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Numeric Id Of Property Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumericIdOfPropertyInstancePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Numeric Id Of Property Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_numericIdOfPropertyInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_numericIdOfPropertyInstance_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Derived Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_derivedKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_derivedKind_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__DERIVED_KIND, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Containment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedContainmentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Containment feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_derivedContainment_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_derivedContainment_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__DERIVED_CONTAINMENT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Property Kind As String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyKindAsStringPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Property Kind As String feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_propertyKindAsString_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_propertyKindAsString_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Correct Property Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectPropertyKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correct Property Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_correctPropertyKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_correctPropertyKind_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KeyandPropertyPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL let onlyDataValues:Boolean = internalLiteralValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in
		let onlyLiteralValues:Boolean = internalDataValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in
		let onlyReferencedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalContainedObject->isEmpty()  in
		let onlyContainedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalReferencedObject->isEmpty()   in
		
		let start:OrderedSet(MPropertyInstanceAction) = OrderedSet{MPropertyInstanceAction::Do} in start->append(
		if containingObject.type.oclIsUndefined() then null else
		if property.oclIsUndefined() then  
		MPropertyInstanceAction::AddPropertyToType else
		if containingObject.type.allFeaturesWithStorage()->excludes(property) then
		MPropertyInstanceAction::AddPropertyToType else null endif endif endif)->append(
		if not property.oclIsUndefined() then 
		if property.calculatedSingular then
		MPropertyInstanceAction::IntoNary else MPropertyInstanceAction::IntoUnary endif else null endif)->append(
		if property.oclIsUndefined() then 
		 if self.derivedKind = PropertyKind::Reference and self.derivedContainment = false then     
		   MPropertyInstanceAction::IntoContainment else null endif
		else if property.kind = PropertyKind::Reference and property.containment = false then 
		   MPropertyInstanceAction::IntoContainment else null endif endif)->append(
		if ((not property.oclIsUndefined()) and property.calculatedSimpleType = SimpleType::String) or self.simpleTypeFromValues = SimpleType::String
		then MPropertyInstanceAction::ClassWithStringAttribute
		else null endif)->append(
		if property.oclIsUndefined() then 
		if onlyDataValues then
		MPropertyInstanceAction::StringValue else null endif else
		if property.calculatedSimpleType = SimpleType::String then
		if (not property.singular) or internalDataValue->isEmpty() then 
		MPropertyInstanceAction::StringValue else null endif else null endif endif)->append(
		if property.oclIsUndefined() then 
		if onlyDataValues then
		MPropertyInstanceAction::BooleanValue else null endif else
		if property.calculatedSimpleType = SimpleType::Boolean then 
		if (not property.singular) or internalDataValue->isEmpty() then 
		MPropertyInstanceAction::BooleanValue else null endif else null endif endif)->append(
		if property.oclIsUndefined() then 
		if onlyDataValues then
		MPropertyInstanceAction::IntegerValue else null endif else
		if property.calculatedSimpleType = SimpleType::Integer then 
		if (not property.singular) or internalDataValue->isEmpty() then 
		MPropertyInstanceAction::IntegerValue else null endif else null endif endif)->append(
		if property.oclIsUndefined() then 
		if onlyDataValues then
		MPropertyInstanceAction::RealValue else null endif else
		if property.calculatedSimpleType = SimpleType::Double then 
		if (not property.singular) or internalDataValue->isEmpty() then 
		MPropertyInstanceAction::RealValue else null endif else null endif endif)->append(
		if property.oclIsUndefined() then 
		if onlyDataValues then
		MPropertyInstanceAction::DateValue else null endif else
		if property.calculatedSimpleType = SimpleType::Date then 
		 if (not property.singular) or internalDataValue->isEmpty() then 
		MPropertyInstanceAction::DateValue else null endif else null endif endif)->append(
		if property.oclIsUndefined() then 
		if onlyReferencedObjects then
		MPropertyInstanceAction::ReferencedObjectValue else null endif else
		if property.kind = PropertyKind::Reference and property.containment = false then 
		 if (not property.singular) or internalReferencedObject->isEmpty() then 
		MPropertyInstanceAction::ReferencedObjectValue else null endif else null endif endif)->append(
		if property.oclIsUndefined() then 
		if onlyContainedObjects then
		MPropertyInstanceAction::ContainedObject else null endif else
		if property.kind = PropertyKind::Reference and property.containment = true then 
		MPropertyInstanceAction::ContainedObject else null endif endif)->append(
		if internalDataValue->notEmpty() or internalLiteralValue->notEmpty() or internalReferencedObject->notEmpty() or internalContainedObject->notEmpty() then 
		MPropertyInstanceAction::ClearValues else null endif)->excluding(null)
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_doAction_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__DO_ACTION, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MPropertyInstanceAction> result = new ArrayList<MPropertyInstanceAction>();
				Collection<? extends MPropertyInstanceAction> superResult = (Collection<? extends MPropertyInstanceAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyInstanceImpl) object)
						.evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Do Add Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoAddValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Add Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_doAddValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_doAddValue_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__DO_ADD_VALUE, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Simple Type From Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypeFromValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type From Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyInstance_simpleTypeFromValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_simpleTypeFromValues_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Enumeration From Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnumerationFromValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Enumeration From Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertyInstance_enumerationFromValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyInstance_enumerationFromValues_feature",
						"_UI_MPropertyInstance_type"),
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES,
				false, false, false, null,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE);
			childrenFeatures.add(
					ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE);
			childrenFeatures.add(
					ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT);
			childrenFeatures.add(
					ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MPropertyInstance.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		MPropertyInstance c = (MPropertyInstance) object;
		MProperty property = c != null ? c.getProperty() : null;
		if (property != null) {
			if (property.getKind() == PropertyKind.ATTRIBUTE
					|| (property.getKind() == PropertyKind.REFERENCE
							&& !property.getContainment())) {
				return overlayImage(object, getResourceLocator()
						.getImage("full/obj16/attribute_obj"));
			}
		}

		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MPropertyInstance"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MPropertyInstanceImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MPropertyInstance.class)) {
		case ObjectsPackage.MPROPERTY_INSTANCE__DUPLICATE_INSTANCE:
		case ObjectsPackage.MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE:
		case ObjectsPackage.MPROPERTY_INSTANCE__DERIVED_CONTAINMENT:
		case ObjectsPackage.MPROPERTY_INSTANCE__LOCAL_KEY:
		case ObjectsPackage.MPROPERTY_INSTANCE__KEY:
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION:
		case ObjectsPackage.MPROPERTY_INSTANCE__WRONG_LOCAL_KEY:
		case ObjectsPackage.MPROPERTY_INSTANCE__DERIVED_KIND:
		case ObjectsPackage.MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING:
		case ObjectsPackage.MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND:
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ACTION:
		case ObjectsPackage.MPROPERTY_INSTANCE__DO_ADD_VALUE:
		case ObjectsPackage.MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE:
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE:
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT:
		case ObjectsPackage.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE,
				ObjectsFactory.eINSTANCE.createMDataValue()));

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE,
				ObjectsFactory.eINSTANCE.createMLiteralValue()));

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT,
				ObjectsFactory.eINSTANCE.createMObjectReference()));

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT,
				ObjectsFactory.eINSTANCE.createMObject()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ObjectsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
