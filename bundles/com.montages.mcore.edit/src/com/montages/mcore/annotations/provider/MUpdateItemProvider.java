/**
 */
package com.montages.mcore.annotations.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.impl.MUpdateImpl;
import com.montages.mcore.provider.MNamedItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MUpdate} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MUpdateItemProvider extends MNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addModePropertyDescriptor(object);
			addFeaturePackageFilterPropertyDescriptor(object);
			addFeatureClassFilterPropertyDescriptor(object);
			addFeaturePropertyDescriptor(object);
			addContainingPropertyAnnotationsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Mode feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MUpdate_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MUpdate_mode_feature", "_UI_MUpdate_type"),
				AnnotationsPackage.Literals.MUPDATE__MODE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Feature Package Filter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeaturePackageFilterPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature Package Filter feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MUpdate_featurePackageFilter_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MUpdate_featurePackageFilter_feature",
						"_UI_MUpdate_type"),
				AnnotationsPackage.Literals.MUPDATE__FEATURE_PACKAGE_FILTER,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Feature Class Filter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeatureClassFilterPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature Class Filter feature.
		 * The list of possible choices is constraint by OCL trg.kind=ClassifierKind::ClassType and
		if self.featurePackageFilter.oclIsUndefined() 
		then true else
		trg.containingPackage = self.featurePackageFilter endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MUpdate_featureClassFilter_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MUpdate_featureClassFilter_feature",
						"_UI_MUpdate_type"),
				AnnotationsPackage.Literals.MUPDATE__FEATURE_CLASS_FILTER, true,
				false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MUpdateImpl) object)
							.evalFeatureClassFilterChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature feature.
		 * The list of possible choices is constraint by OCL trg.kind<>PropertyKind::Operation
		and
		if self.featureClassFilter.oclIsUndefined()
		then 
		if self.featurePackageFilter.oclIsUndefined() 
		  then true
		  else trg.containingClassifier.containingPackage=
		self.featurePackageFilter endif
		else featureClassFilter.allFeatures()->includes(trg) endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MUpdate_feature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MUpdate_feature_feature", "_UI_MUpdate_type"),
				AnnotationsPackage.Literals.MUPDATE__FEATURE, true, false, true,
				null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MProperty> iterator = result.iterator(); iterator
						.hasNext();) {
					MProperty trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MUpdateImpl) object)
							.evalFeatureChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Containing Property Annotations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingPropertyAnnotationsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Containing Property Annotations feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MUpdate_containingPropertyAnnotations_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MUpdate_containingPropertyAnnotations_feature",
						"_UI_MUpdate_type"),
				AnnotationsPackage.Literals.MUPDATE__CONTAINING_PROPERTY_ANNOTATIONS,
				false, false, false, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(AnnotationsPackage.Literals.MUPDATE__OBJECT);
			childrenFeatures.add(AnnotationsPackage.Literals.MUPDATE__VALUE);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE);
			childrenFeatures.add(AnnotationsPackage.Literals.MUPDATE__LOCATION);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MUPDATE__MUPDATE_CONDITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MUpdate.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MUpdate"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MUpdateImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MUpdate.class)) {
		case AnnotationsPackage.MUPDATE__MODE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case AnnotationsPackage.MUPDATE__OBJECT:
		case AnnotationsPackage.MUPDATE__VALUE:
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE:
		case AnnotationsPackage.MUPDATE__LOCATION:
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT:
		case AnnotationsPackage.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE:
		case AnnotationsPackage.MUPDATE__MUPDATE_CONDITION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MUPDATE__OBJECT,
				AnnotationsFactory.eINSTANCE.createMUpdateObject()));

		newChildDescriptors.add(
				createChildParameter(AnnotationsPackage.Literals.MUPDATE__VALUE,
						AnnotationsFactory.eINSTANCE.createMUpdateValue()));

		newChildDescriptors.add(
				createChildParameter(AnnotationsPackage.Literals.MUPDATE__VALUE,
						AnnotationsFactory.eINSTANCE.createMUpdateCondition()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MUPDATE__ALTERNATIVE_PERSISTENCE_PACKAGE,
				AnnotationsFactory.eINSTANCE
						.createMUpdateAlternativePersistencePackage()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MUPDATE__LOCATION,
				AnnotationsFactory.eINSTANCE
						.createMUpdatePersistenceLocation()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MUPDATE__ALTERNATIVE_PERSISTENCE_ROOT,
				AnnotationsFactory.eINSTANCE
						.createMUpdateAlternativePersistenceRoot()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MUPDATE__ALTERNATIVE_PERSISTENCE_REFERENCE,
				AnnotationsFactory.eINSTANCE
						.createMUpdateAlternativePersistenceReference()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MUPDATE__MUPDATE_CONDITION,
				AnnotationsFactory.eINSTANCE.createMUpdateCondition()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child,
			Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == AnnotationsPackage.Literals.MUPDATE__VALUE
				|| childFeature == AnnotationsPackage.Literals.MUPDATE__MUPDATE_CONDITION;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject),
							getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

}
