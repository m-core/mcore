/**
 */
package com.montages.mcore.annotations.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.provider.MRepositoryElementItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MAbstractPropertyAnnotations} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractPropertyAnnotationsItemProvider
		extends MRepositoryElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractPropertyAnnotationsItemProvider(
			AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addContainingPropertiesContainerPropertyDescriptor(object);
			}
			addContainingClassifierPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addAnnotatedPropertyPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Annotated Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAnnotatedPropertyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Annotated Property feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractPropertyAnnotations_annotatedProperty_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractPropertyAnnotations_annotatedProperty_feature",
						"_UI_MAbstractPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY,
				false, false, false, null,
				getString("_UI_AnnotatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractPropertyAnnotations_containingClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractPropertyAnnotations_containingClassifier_feature",
						"_UI_MAbstractPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_CLASSIFIER,
				false, false, false, null,
				getString("_UI_AnnotatedPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Containing Properties Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingPropertiesContainerPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Containing Properties Container feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractPropertyAnnotations_containingPropertiesContainer_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractPropertyAnnotations_containingPropertiesContainer_feature",
						"_UI_MAbstractPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__CONTAINING_PROPERTIES_CONTAINER,
				false, false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MAbstractPropertyAnnotations) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
