/**
 */
package com.montages.mcore.annotations.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractAnnotion;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.provider.MRepositoryElementItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MAbstractAnnotion} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractAnnotionItemProvider
		extends MRepositoryElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractAnnotionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addGeneralReferencePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addAnnotatedElementPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingAbstractPropertyAnnotationsPropertyDescriptor(
						object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the General Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGeneralReferencePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the General Reference feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractAnnotion_generalReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractAnnotion_generalReference_feature",
						"_UI_MAbstractAnnotion_type"),
				AnnotationsPackage.Literals.MABSTRACT_ANNOTION__GENERAL_REFERENCE,
				true, false, true, null,
				getString("_UI_SpecialGeneralAnnotationPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Annotated Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAnnotatedElementPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Annotated Element feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractAnnotion_annotatedElement_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractAnnotion_annotatedElement_feature",
						"_UI_MAbstractAnnotion_type"),
				AnnotationsPackage.Literals.MABSTRACT_ANNOTION__ANNOTATED_ELEMENT,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Abstract Property Annotations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingAbstractPropertyAnnotationsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Containing Abstract Property Annotations feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractAnnotion_containingAbstractPropertyAnnotations_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractAnnotion_containingAbstractPropertyAnnotations_feature",
						"_UI_MAbstractAnnotion_type"),
				AnnotationsPackage.Literals.MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MABSTRACT_ANNOTION__GENERAL_CONTENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MAbstractAnnotion) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAbstractAnnotion.class)) {
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MABSTRACT_ANNOTION__GENERAL_CONTENT,
				ObjectsFactory.eINSTANCE.createMObject()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
