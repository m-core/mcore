/**
 */
package com.montages.mcore.annotations.provider;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MExprAnnotationAction;
import com.montages.mcore.annotations.impl.MAnnotationImpl;
import com.montages.mcore.annotations.impl.MExprAnnotationImpl;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractBaseDefinition;
import com.montages.mcore.expressions.MProcessorDefinition;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;

import java.util.ArrayList;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MExprAnnotation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MExprAnnotationItemProvider extends MAnnotationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		if (feature == AnnotationsPackage.eINSTANCE
				.getMExprAnnotation_DoAction()) {
			MExprAnnotationAction ourAction = (MExprAnnotationAction) value;
			switch (ourAction.getValue()) {
			case MExprAnnotationAction.INTO_APPLY_VALUE:
			case MExprAnnotationAction.ARGUMENT_VALUE:
			case MExprAnnotationAction.SUB_CHAIN_VALUE:
			case MExprAnnotationAction.COLLECTION_OP_VALUE:
			case MExprAnnotationAction.LITERAL_CONSTANT_AS_ARGUMENT_VALUE:
			case MExprAnnotationAction.DATA_CONSTANT_AS_ARGUMENT_VALUE:
			case MExprAnnotationAction.INTO_COND_OF_IF_THEN_ELSE_VALUE:
			case MExprAnnotationAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE:
			case MExprAnnotationAction.INTO_ELSE_OF_IF_THEN_ELSE_VALUE:
				if (owner instanceof MExprAnnotation) {
					XUpdate xUpdate = ((MExprAnnotation) owner)
							.doActionUpdate(ourAction);
					return xUpdate == null ? null
							: xUpdate.getContainingTransition()
									.interpreteTransitionAsCommand();

				}
				break;

			}

		}

		return super.createSetCommand(domain, owner, feature, value, index);

	};

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addMultiplicityAsStringPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedSingularPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedTypePackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addVoidTypeAllowedPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedSimpleTypePropertyDescriptor(object);
			}
			addMultiplicityCasePropertyDescriptor(object);
			addAsCodePropertyDescriptor(object);
			addAsBasicCodePropertyDescriptor(object);
			addCollectorPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addEntireScopePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSelfPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeTrgPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeObjPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSimpleTypeConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeLiteralConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeObjectReferenceConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeVariablesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeIteratorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeAccumulatorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeParametersPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeContainerBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeNumberBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSourcePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalEntireScopePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSelfPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeTrgPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeObjPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSourcePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSimpleTypeConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeLiteralConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeObjectReferenceConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeVariablesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeIteratorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeAccumulatorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeParametersPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeNumberBaseDefinitionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeContainerPropertyDescriptor(object);
			}
			addContainingAnnotationPropertyDescriptor(object);
			addContainingExpressionPropertyDescriptor(object);
			addIsComplexExpressionPropertyDescriptor(object);
			addIsSubExpressionPropertyDescriptor(object);
			addCalculatedOwnTypePropertyDescriptor(object);
			addCalculatedOwnMandatoryPropertyDescriptor(object);
			addCalculatedOwnSingularPropertyDescriptor(object);
			addCalculatedOwnSimpleTypePropertyDescriptor(object);
			addSelfObjectTypePropertyDescriptor(object);
			addSelfObjectPackagePropertyDescriptor(object);
			addTargetObjectTypePropertyDescriptor(object);
			addTargetSimpleTypePropertyDescriptor(object);
			addObjectObjectTypePropertyDescriptor(object);
			addExpectedReturnTypePropertyDescriptor(object);
			addExpectedReturnSimpleTypePropertyDescriptor(object);
			addIsReturnValueMandatoryPropertyDescriptor(object);
			addIsReturnValueSingularPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addSrcObjectTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSrcObjectSimpleTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseAsCodePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseDefinitionPropertyDescriptor(object);
			}
			addBaseVarPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addBaseExitTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseExitSimpleTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseExitMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseExitSingularPropertyDescriptor(object);
			}
			addChainEntryTypePropertyDescriptor(object);
			addChainAsCodePropertyDescriptor(object);
			addElement1PropertyDescriptor(object);
			addElement1CorrectPropertyDescriptor(object);
			addElement2EntryTypePropertyDescriptor(object);
			addElement2PropertyDescriptor(object);
			addElement2CorrectPropertyDescriptor(object);
			addElement3EntryTypePropertyDescriptor(object);
			addElement3PropertyDescriptor(object);
			addElement3CorrectPropertyDescriptor(object);
			addCastTypePropertyDescriptor(object);
			addLastElementPropertyDescriptor(object);
			addChainCalculatedTypePropertyDescriptor(object);
			addChainCalculatedSimpleTypePropertyDescriptor(object);
			addChainCalculatedSingularPropertyDescriptor(object);
			addProcessorPropertyDescriptor(object);
			addProcessorDefinitionPropertyDescriptor(object);
			addTypeMismatchPropertyDescriptor(object);
			addChainCodeforSubchainsPropertyDescriptor(object);
			addIsOwnXOCLOpPropertyDescriptor(object);
			addExpectedReturnTypeOfAnnotationPropertyDescriptor(object);
			addExpectedReturnSimpleTypeOfAnnotationPropertyDescriptor(object);
			addIsReturnValueOfAnnotationMandatoryPropertyDescriptor(object);
			addIsReturnValueOfAnnotationSingularPropertyDescriptor(object);
			addUseExplicitOclPropertyDescriptor(object);
			addOclChangedPropertyDescriptor(object);
			addOclCodePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Multiplicity As String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiplicityAsStringPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Multiplicity As String feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_multiplicityAsString_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_multiplicityAsString_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__MULTIPLICITY_AS_STRING, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedType_feature", "_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_TYPE, false, false,
				false, null, getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedMandatory_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_MANDATORY, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedSingular_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_SINGULAR, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Type Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedTypePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Type Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedTypePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedTypePackage_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_TYPE_PACKAGE, false,
				false, false, null,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Void Type Allowed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVoidTypeAllowedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Void Type Allowed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_voidTypeAllowed_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_voidTypeAllowed_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__VOID_TYPE_ALLOWED, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedSimpleType_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Multiplicity Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiplicityCasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Multiplicity Case feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_multiplicityCase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_multiplicityCase_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__MULTIPLICITY_CASE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_asCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_asCode_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the As Basic Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsBasicCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Basic Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_asBasicCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_asBasicCode_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Collector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Collector feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_collector_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_collector_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Entire Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEntireScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Entire Scope feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_entireScope_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_entireScope_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ENTIRE_SCOPE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_BASE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Self feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSelfPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Self feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSelf_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSelf_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SELF,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Trg feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeTrgPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Trg feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeTrg_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeTrg_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_TRG,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Obj feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeObjPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Obj feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeObj_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeObj_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJ,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Simple Type Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSimpleTypeConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Scope Simple Type Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_scopeSimpleTypeConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSimpleTypeConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Literal Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeLiteralConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Literal Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_scopeLiteralConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeLiteralConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Object Reference Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeObjectReferenceConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Scope Object Reference Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_scopeObjectReferenceConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeObjectReferenceConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeVariablesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Variables feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeVariables_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeVariables_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_VARIABLES,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Iterator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeIteratorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Iterator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeIterator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeIterator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ITERATOR,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Accumulator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeAccumulatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Accumulator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeAccumulator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeAccumulator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeParametersPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Parameters feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeParameters_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeParameters_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Container Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeContainerBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Container Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeContainerBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeContainerBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Number Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeNumberBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Number Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeNumberBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeNumberBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Source feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSource_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSource_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SOURCE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Entire Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalEntireScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Entire Scope feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localEntireScope_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localEntireScope_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Self feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSelfPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Self feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeSelf_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSelf_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Trg feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeTrgPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Trg feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeTrg_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeTrg_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Obj feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeObjPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Obj feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeObj_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeObj_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Source feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeSource_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSource_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Simple Type Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSimpleTypeConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Simple Type Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeSimpleTypeConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSimpleTypeConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Literal Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeLiteralConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Literal Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeLiteralConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeLiteralConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Object Reference Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeObjectReferenceConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Object Reference Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeObjectReferenceConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeObjectReferenceConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeVariablesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Variables feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeVariables_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeVariables_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Iterator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeIteratorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Iterator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeIterator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeIterator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Accumulator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeAccumulatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Accumulator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeAccumulator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeAccumulator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeParametersPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Parameters feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeParameters_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeParameters_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Number Base Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeNumberBaseDefinitionPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Number Base Definition feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeNumberBaseDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeNumberBaseDefinition_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeContainerPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Container feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeContainer_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeContainer_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_containingAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_containingAnnotation_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Containing Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_containingExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_containingExpression_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Complex Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsComplexExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Complex Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_isComplexExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isComplexExpression_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Sub Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsSubExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Sub Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_isSubExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isSubExpression_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_calculatedOwnType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_calculatedOwnMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnMandatory_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_calculatedOwnSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnSingular_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_calculatedOwnSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Self Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelfObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Self Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_selfObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_selfObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Self Object Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelfObjectPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Self Object Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_selfObjectPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_selfObjectPackage_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Target Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Target Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_targetObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_targetObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Target Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Target Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_targetSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_targetSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Object Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_objectObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_objectObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expected Return Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectedReturnTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Expected Return Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_expectedReturnType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_expectedReturnType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expected Return Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectedReturnSimpleTypePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Expected Return Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_expectedReturnSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_expectedReturnSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_isReturnValueMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isReturnValueMandatory_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_isReturnValueSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isReturnValueSingular_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Src Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSrcObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Src Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_srcObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_srcObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Src Object Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSrcObjectSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Src Object Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_srcObjectSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_srcObjectSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpressionWithBase_baseAsCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseAsCode_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpressionWithBase_base_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_base_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Definition feature.
		 * The list of possible choices is constructed by OCL self.getScope()
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpressionWithBase_baseDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseDefinition_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION,
				true, false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MAbstractBaseDefinition> result = new ArrayList<MAbstractBaseDefinition>();
				Collection<? extends MAbstractBaseDefinition> superResult = (Collection<? extends MAbstractBaseDefinition>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MExprAnnotationImpl) object)
						.evalBaseDefinitionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Base Var feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseVarPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Var feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpressionWithBase_baseVar_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseVar_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Base Exit Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseExitTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Exit Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpressionWithBase_baseExitType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseExitType_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE,
				false, false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Exit Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseExitSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Exit Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpressionWithBase_baseExitSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseExitSimpleType_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Exit Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseExitMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Exit Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpressionWithBase_baseExitMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseExitMandatory_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Exit Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseExitSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Exit Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpressionWithBase_baseExitSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpressionWithBase_baseExitSingular_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Chain Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainEntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainEntryType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainEntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainAsCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainAsCode_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Element1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element1 feature.
		 * The list of possible choices is constructed by OCL 
		let annotatedProp: mcore::MProperty = 
		self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
		in
		if self.chainEntryType.oclIsUndefined() then 
		OrderedSet{} else
		self.chainEntryType.allProperties()
		endif
		
		--
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element1_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MExprAnnotationImpl) object)
						.evalElement1ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Element1 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement1CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element1 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element1Correct_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element1Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Element2 Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2EntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element2EntryType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element2EntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Element2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 feature.
		 * The list of possible choices is constructed by OCL let annotatedProp: MProperty = 
		self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
		in
		
		if element1.oclIsUndefined() 
		then OrderedSet{}
		else if element1.isOperation
		then OrderedSet{} 
		else if element2EntryType.oclIsUndefined() 
		  then OrderedSet{}
		  else element2EntryType.allProperties() endif
		 endif
		endif
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element2_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MExprAnnotationImpl) object)
						.evalElement2ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Element2 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element2Correct_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element2Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Element3 Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3EntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element3EntryType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element3EntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Element3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 feature.
		 * The list of possible choices is constructed by OCL let annotatedProp: mcore::MProperty = 
		self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
		in
		
		if element2.oclIsUndefined() 
		then OrderedSet{}
		else if element2.isOperation
		then OrderedSet{} 
		else if element3EntryType.oclIsUndefined() 
		  then OrderedSet{}
		  else element3EntryType.allProperties() endif
		 endif
		endif
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element3_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MExprAnnotationImpl) object)
						.evalElement3ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Element3 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element3Correct_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element3Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Cast Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCastTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cast Type feature.
		 * The list of possible choices is constraint by OCL trg.kind = mcore::ClassifierKind::ClassType 
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_castType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_castType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CAST_TYPE, true,
				false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MExprAnnotationImpl) object)
							.evalCastTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Last Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastElementPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Last Element feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_lastElement_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_lastElement_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__LAST_ELEMENT,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainCalculatedType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainCalculatedType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedSimpleTypePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractChain_chainCalculatedSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainCalculatedSimpleType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainCalculatedSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainCalculatedSingular_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Processor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processor feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_processor_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_processor_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ProcessorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Processor Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessorDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processor Definition feature.
		 * The list of possible choices is constructed by OCL 
		let s:SimpleType = self.chainCalculatedSimpleType in
		let t:MClassifier = self.chainCalculatedType in
		let res:OrderedSet(mcore::expressions::MProcessorDefinition) =
		if s = mcore::SimpleType::Boolean
		then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForBoolean()
		         else self.procDefChoicesForBooleans() endif
		else if s = mcore::SimpleType::Integer 
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForInteger()
		         else self.procDefChoicesForIntegers() endif
		else if   s = mcore::SimpleType::Double
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForReal()
		         else self.procDefChoicesForReals() endif
		else if   s = mcore::SimpleType::String
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForString()
		         else self.procDefChoicesForStrings() endif
		else if   s = mcore::SimpleType::Date
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForDate()
		         else self.procDefChoicesForDates() endif
		else if s = mcore::SimpleType::None or 
		  s = mcore::SimpleType::Annotation or 
		  s = mcore::SimpleType::Attribute or 
		  s = mcore::SimpleType::Class or 
		  s = mcore::SimpleType::Classifier or 
		  s = mcore::SimpleType::DataType or 
		  s = mcore::SimpleType::Enumeration or 
		  s = mcore::SimpleType::Feature or 
		  s = mcore::SimpleType::KeyValue or 
		  s = mcore::SimpleType::Literal or 
		  s = mcore::SimpleType::NamedElement or 
		  s = mcore::SimpleType::Object or 
		  s = mcore::SimpleType::Operation or 
		  s = mcore::SimpleType::Package or 
		  s = mcore::SimpleType::Parameter or 
		  s = mcore::SimpleType::Reference or 
		  s = mcore::SimpleType::TypedElement 
		 then if self.chainCalculatedSingular 
		         then self.procDefChoicesForObject()
		         else self.procDefChoicesForObjects()
		                --OrderedSet{Tuple{processor=MProcessor::Head},
		                --                   Tuple{processor=MProcessor::Tail}} 
		                endif
		 else OrderedSet{} endif endif endif endif endif endif
		in res->prepend(null)
		                                   
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_processorDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_processorDefinition_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION,
				true, false, false, null,
				getString("_UI_ProcessorDefinitionPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProcessorDefinition> result = new ArrayList<MProcessorDefinition>();
				Collection<? extends MProcessorDefinition> superResult = (Collection<? extends MProcessorDefinition>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MExprAnnotationImpl) object)
						.evalProcessorDefinitionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Type Mismatch feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeMismatchPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type Mismatch feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MBaseChain_typeMismatch_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MBaseChain_typeMismatch_feature",
						"_UI_MBaseChain_type"),
				ExpressionsPackage.Literals.MBASE_CHAIN__TYPE_MISMATCH, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Chain Codefor Subchains feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCodeforSubchainsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Codefor Subchains feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MBaseChain_chainCodeforSubchains_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MBaseChain_chainCodeforSubchains_feature",
						"_UI_MBaseChain_type"),
				ExpressionsPackage.Literals.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Is Own XOCL Op feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsOwnXOCLOpPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Own XOCL Op feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MBaseChain_isOwnXOCLOp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MBaseChain_isOwnXOCLOp_feature",
						"_UI_MBaseChain_type"),
				ExpressionsPackage.Literals.MBASE_CHAIN__IS_OWN_XOCL_OP, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ProcessorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expected Return Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectedReturnTypeOfAnnotationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Expected Return Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MExprAnnotation_expectedReturnTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_expectedReturnTypeOfAnnotation_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_TYPE_OF_ANNOTATION,
				false, false, false, null,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expected Return Simple Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectedReturnSimpleTypeOfAnnotationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Expected Return Simple Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MExprAnnotation_expectedReturnSimpleTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_expectedReturnSimpleTypeOfAnnotation_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Of Annotation Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueOfAnnotationMandatoryPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Of Annotation Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MExprAnnotation_isReturnValueOfAnnotationMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_isReturnValueOfAnnotationMandatory_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Of Annotation Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueOfAnnotationSingularPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Of Annotation Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MExprAnnotation_isReturnValueOfAnnotationSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_isReturnValueOfAnnotationSingular_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Ocl Changed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOclChangedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Ocl Changed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExprAnnotation_oclChanged_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_oclChanged_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__OCL_CHANGED, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ExplicitOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Ocl Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addOclCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Ocl Code feature.
		 */
		ItemPropertyDescriptor descr = new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExprAnnotation_oclCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_oclCode_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__OCL_CODE, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ExplicitOCLPropertyCategory"), null) {
			@Override
			public OCLExpressionContext getOCLExpressionContext(Object object) {
				return getNonEditableExpressionContext(object);
			}
		};
		itemPropertyDescriptors.add(descr);
	}

	protected OCLExpressionContext getNonEditableExpressionContext(
			Object object) {
		final MModelElement modelElement = ((MAnnotationImpl) object)
				.getAnnotatedElement();

		if (isContextMissing(modelElement)) {
			try {
				generateEcore(modelElement);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		return new OCLExpressionContext(OCLExpressionContext.getEObjectClass(),
				null, null, false);
	}

	private void generateEcore(MModelElement modelElement) throws Exception {
		new Mcore2Ecore().transform(//
				modelElement.eResource().getResourceSet(), //
				(MComponent) EcoreUtil.getRootContainer(modelElement));
	}

	protected boolean isContextMissing(MModelElement element) {
		if (element instanceof MPackage) {
			return ((MPackage) element).getInternalEPackage() == null;
		} else if (element instanceof MClassifier) {
			return ((MClassifier) element).getInternalEClassifier() == null;
		} else if (element instanceof MProperty) {
			return ((MProperty) element)
					.getInternalEStructuralFeature() == null;
		} else if (element instanceof MOperationSignature) {
			return ((MOperationSignature) element)
					.getInternalEOperation() == null;
		} else if (element instanceof MParameter) {
			return ((MParameter) element).getInternalEParameter() == null;
		} else {
			return false;
		}
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExprAnnotation_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_doAction_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__DO_ACTION, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Use Explicit Ocl feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseExplicitOclPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Use Explicit Ocl feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExprAnnotation_useExplicitOcl_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExprAnnotation_useExplicitOcl_feature",
						"_UI_MExprAnnotation_type"),
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__USE_EXPLICIT_OCL,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ExplicitOCLPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					ExpressionsPackage.Literals.MBASE_CHAIN__CALL_ARGUMENT);
			childrenFeatures.add(
					ExpressionsPackage.Literals.MBASE_CHAIN__SUB_EXPRESSION);
			childrenFeatures.add(
					ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_EXPRESSION);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_TUPLE);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_CONSTANT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MExprAnnotation) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MExprAnnotation.class)) {
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_AS_STRING:
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_MANDATORY:
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SINGULAR:
		case AnnotationsPackage.MEXPR_ANNOTATION__VOID_TYPE_ALLOWED:
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__MULTIPLICITY_CASE:
		case AnnotationsPackage.MEXPR_ANNOTATION__AS_CODE:
		case AnnotationsPackage.MEXPR_ANNOTATION__AS_BASIC_CODE:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_COMPLEX_EXPRESSION:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_SUB_EXPRESSION:
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_MANDATORY:
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SINGULAR:
		case AnnotationsPackage.MEXPR_ANNOTATION__CALCULATED_OWN_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__TARGET_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_MANDATORY:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_SINGULAR:
		case AnnotationsPackage.MEXPR_ANNOTATION__SRC_OBJECT_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_AS_CODE:
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE:
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_MANDATORY:
		case AnnotationsPackage.MEXPR_ANNOTATION__BASE_EXIT_SINGULAR:
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_AS_CODE:
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT1_CORRECT:
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT2_CORRECT:
		case AnnotationsPackage.MEXPR_ANNOTATION__ELEMENT3_CORRECT:
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SIMPLE_TYPE:
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CALCULATED_SINGULAR:
		case AnnotationsPackage.MEXPR_ANNOTATION__PROCESSOR:
		case AnnotationsPackage.MEXPR_ANNOTATION__TYPE_MISMATCH:
		case AnnotationsPackage.MEXPR_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_OWN_XOCL_OP:
		case AnnotationsPackage.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY:
		case AnnotationsPackage.MEXPR_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR:
		case AnnotationsPackage.MEXPR_ANNOTATION__USE_EXPLICIT_OCL:
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CHANGED:
		case AnnotationsPackage.MEXPR_ANNOTATION__OCL_CODE:
		case AnnotationsPackage.MEXPR_ANNOTATION__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case AnnotationsPackage.MEXPR_ANNOTATION__CALL_ARGUMENT:
		case AnnotationsPackage.MEXPR_ANNOTATION__SUB_EXPRESSION:
		case AnnotationsPackage.MEXPR_ANNOTATION__CONTAINED_COLLECTOR:
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_EXPRESSION:
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_TUPLE:
		case AnnotationsPackage.MEXPR_ANNOTATION__NAMED_CONSTANT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MBASE_CHAIN__CALL_ARGUMENT,
				ExpressionsFactory.eINSTANCE.createMCallArgument()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MBASE_CHAIN__SUB_EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMSubChain()));

		newChildDescriptors.add(createChildParameter(
				ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR,
				ExpressionsFactory.eINSTANCE.createMCollectionExpression()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMNamedExpression()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_TUPLE,
				ExpressionsFactory.eINSTANCE.createMTuple()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_TUPLE,
				ExpressionsFactory.eINSTANCE.createMNewObjecct()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_CONSTANT,
				ExpressionsFactory.eINSTANCE.createMNamedConstant()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
