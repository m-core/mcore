/**
 */
package com.montages.mcore.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MExplicitlyTyped;
import com.montages.mcore.McorePackage;
import com.montages.mcore.impl.MExplicitlyTypedImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MExplicitlyTyped} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MExplicitlyTypedItemProvider extends MTypedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExplicitlyTypedItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addSimpleTypeStringPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHasSimpleDataTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHasSimpleModelingTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSimpleTypeIsCorrectPropertyDescriptor(object);
			}
			addSimpleTypePropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addTypePackagePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSingularPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addETypeNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addETypeLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCorrectlyTypedPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Simple Type String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypeStringPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type String feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_simpleTypeString_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_simpleTypeString_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_simpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_simpleType_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AdditionalPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Has Simple Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasSimpleDataTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Has Simple Data Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_hasSimpleDataType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_hasSimpleDataType_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Has Simple Modeling Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasSimpleModelingTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Has Simple Modeling Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_hasSimpleModelingType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_hasSimpleModelingType_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Simple Type Is Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypeIsCorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type Is Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_simpleTypeIsCorrect_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_simpleTypeIsCorrect_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Type Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_typePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_typePackage_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__TYPE_PACKAGE, true,
				false, true, null, getString("_UI_AdditionalPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type feature.
		 * The list of possible choices is constraint by OCL trg.selectableClassifier(
		if self.type.oclIsUndefined() then OrderedSet{} else OrderedSet{self.type} endif,
		self.typePackage)
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_type_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_type_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__TYPE, true, false,
				true, null, getString("_UI_AdditionalPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MExplicitlyTypedImpl) object)
							.evalTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_mandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_mandatory_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__MANDATORY, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_singular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_singular_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__SINGULAR, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EType Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addETypeNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EType Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_eTypeName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_eTypeName_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__ETYPE_NAME, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EType Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addETypeLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EType Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_eTypeLabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_eTypeLabel_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__ETYPE_LABEL, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Correctly Typed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectlyTypedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correctly Typed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_correctlyTyped_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_correctlyTyped_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__CORRECTLY_TYPED, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MExplicitlyTyped) object).getETypeName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MExplicitlyTyped.class)) {
		case McorePackage.MEXPLICITLY_TYPED__SIMPLE_TYPE_STRING:
		case McorePackage.MEXPLICITLY_TYPED__HAS_SIMPLE_DATA_TYPE:
		case McorePackage.MEXPLICITLY_TYPED__HAS_SIMPLE_MODELING_TYPE:
		case McorePackage.MEXPLICITLY_TYPED__SIMPLE_TYPE_IS_CORRECT:
		case McorePackage.MEXPLICITLY_TYPED__SIMPLE_TYPE:
		case McorePackage.MEXPLICITLY_TYPED__MANDATORY:
		case McorePackage.MEXPLICITLY_TYPED__SINGULAR:
		case McorePackage.MEXPLICITLY_TYPED__ETYPE_NAME:
		case McorePackage.MEXPLICITLY_TYPED__ETYPE_LABEL:
		case McorePackage.MEXPLICITLY_TYPED__CORRECTLY_TYPED:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
