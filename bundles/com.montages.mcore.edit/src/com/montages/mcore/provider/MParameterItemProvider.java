/**
 */
package com.montages.mcore.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.langlets.acore.abstractions.AbstractionsPackage;
import org.langlets.acore.classifiers.ClassifiersPackage;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MParameter;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.impl.MParameterImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MParameter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MParameterItemProvider extends MExplicitlyTypedAndNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof MParameter) {
			MParameter typedOwner = ((MParameter) owner);

			if (feature == McorePackage.eINSTANCE.getMParameter_DoAction())
				myUpdate = typedOwner.doAction$Update(
						(com.montages.mcore.MParameterAction) value);

		}

		return myUpdate == null
				? super.createSetCommand(domain, owner, feature, value, index)
				: myUpdate.getContainingTransition()
						.interpreteTransitionAsCommand();

	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addALabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAKindBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addARenderedKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingComponentPropertyDescriptor(object);
			}
			addTPackageUriPropertyDescriptor(object);
			addTClassifierNamePropertyDescriptor(object);
			addTFeatureNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addTPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTACoreAStringClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addANamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedNameConstantPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABusinessNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASpecializedClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveDataTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveEnumerationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAsDataTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAsClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAIsStringClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingSignaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addELabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSignatureAsStringPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInternalEParameterPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ALabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addALabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALabel feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_AElement_aLabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_aLabel_feature", "_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__ALABEL, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AKind Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAKindBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKind Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_aKindBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_aKindBase_feature", "_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__AKIND_BASE, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ARendered Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addARenderedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARendered Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_aRenderedKind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_aRenderedKind_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Component feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Component feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_aContainingComponent_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_aContainingComponent_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT,
				false, false, false, null,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TPackage Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTPackageUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TPackage Uri feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tPackageUri_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tPackageUri_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TPACKAGE_URI, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the TClassifier Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTClassifierNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TClassifier Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tClassifierName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tClassifierName_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TCLASSIFIER_NAME, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the TFeature Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTFeatureNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TFeature Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tFeatureName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tFeatureName_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TFEATURE_NAME, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the TPackage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TPackage feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tPackage_feature", "_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TPACKAGE, false, false,
				false, null,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TClassifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TClassifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tClassifier_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TCLASSIFIER, false,
				false, false, null,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TFeature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TFeature feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tFeature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tFeature_feature", "_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TFEATURE, false, false,
				false, null,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the TA Core AString Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTACoreAStringClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the TA Core AString Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AElement_tACoreAStringClass_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AElement_tACoreAStringClass_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__TA_CORE_ASTRING_CLASS,
				false, false, false, null,
				getString("_UI__80ACorePackagePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addANamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AName feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_ANamed_aName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ANamed_aName_feature", "_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__ANAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Name Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedNameConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Name Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ANamed_aUndefinedNameConstant_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ANamed_aUndefinedNameConstant_feature",
						"_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABusiness Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABusinessNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABusiness Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ANamed_aBusinessName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ANamed_aBusinessName_feature", "_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASpecialized Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASpecializedClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASpecialized Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_aSpecializedClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_aSpecializedClassifier_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__ASPECIALIZED_CLASSIFIER,
				false, false, false, null,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveDataTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Data Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_aActiveDataType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_aActiveDataType_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_DATA_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Enumeration feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveEnumerationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Enumeration feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_aActiveEnumeration_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_aActiveEnumeration_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_ENUMERATION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_aActiveClass_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_aActiveClass_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_CLASS, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_aContainingPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_aContainingPackage_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__ACONTAINING_PACKAGE,
				false, false, false, null,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the As Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsDataTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Data Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_asDataType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_asDataType_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AS_DATA_TYPE, false,
				false, false, null,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the As Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_asClass_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_asClass_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AS_CLASS, false, false,
				false, null,
				getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AIs String Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAIsStringClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AIs String Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AClassifier_aIsStringClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AClassifier_aIsStringClassifier_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AIS_STRING_CLASSIFIER,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI__80ACorePackagePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Signature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingSignaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Signature feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MParameter_containingSignature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MParameter_containingSignature_feature",
						"_UI_MParameter_type"),
				McorePackage.Literals.MPARAMETER__CONTAINING_SIGNATURE, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ELabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addELabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ELabel feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MParameter_eLabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MParameter_eLabel_feature", "_UI_MParameter_type"),
				McorePackage.Literals.MPARAMETER__ELABEL, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Signature As String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSignatureAsStringPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Signature As String feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MParameter_signatureAsString_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MParameter_signatureAsString_feature",
						"_UI_MParameter_type"),
				McorePackage.Literals.MPARAMETER__SIGNATURE_AS_STRING, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Internal EParameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInternalEParameterPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Internal EParameter feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MParameter_internalEParameter_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MParameter_internalEParameter_feature",
						"_UI_MParameter_type"),
				McorePackage.Literals.MPARAMETER__INTERNAL_EPARAMETER, true,
				false, true, null,
				getString("_UI_RelationtoECorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MParameter_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MParameter_doAction_feature",
						"_UI_MParameter_type"),
				McorePackage.Literals.MPARAMETER__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MParameter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/EParameter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MParameterImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MParameter.class)) {
		case McorePackage.MPARAMETER__ALABEL:
		case McorePackage.MPARAMETER__AKIND_BASE:
		case McorePackage.MPARAMETER__ARENDERED_KIND:
		case McorePackage.MPARAMETER__TPACKAGE_URI:
		case McorePackage.MPARAMETER__TCLASSIFIER_NAME:
		case McorePackage.MPARAMETER__TFEATURE_NAME:
		case McorePackage.MPARAMETER__ANAME:
		case McorePackage.MPARAMETER__AUNDEFINED_NAME_CONSTANT:
		case McorePackage.MPARAMETER__ABUSINESS_NAME:
		case McorePackage.MPARAMETER__AACTIVE_DATA_TYPE:
		case McorePackage.MPARAMETER__AACTIVE_ENUMERATION:
		case McorePackage.MPARAMETER__AACTIVE_CLASS:
		case McorePackage.MPARAMETER__AIS_STRING_CLASSIFIER:
		case McorePackage.MPARAMETER__ELABEL:
		case McorePackage.MPARAMETER__SIGNATURE_AS_STRING:
		case McorePackage.MPARAMETER__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION,
				AnnotationsFactory.eINSTANCE.createMGeneralAnnotation()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
