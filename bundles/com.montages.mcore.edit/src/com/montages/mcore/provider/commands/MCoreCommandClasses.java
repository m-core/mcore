package com.montages.mcore.provider.commands;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MChainOrApplication;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.MIf;
import com.montages.mcore.expressions.MLiteralValueExpr;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceAction;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.ui.operations.CreateProject;
import com.montages.mcore.ui.operations.GenerateEcore;
import com.montages.mcore.ui.operations.GenerateEditorConfig;
import com.montages.mcore.ui.operations.GenerateTableEditor;
import com.montages.mcore.ui.wizards.WizardData;

public class MCoreCommandClasses {

	public static class MResource_OpenDynamicEditorCommand extends SetCommand {

		public MResource_OpenDynamicEditorCommand(EditingDomain domain,
				EObject owner, EStructuralFeature feature, Object value) {

			super(domain, owner, feature, value);
		}

		public void doExecute() {
			MResource mRes = (MResource) owner;
			try {
				SetCommand command = new SetCommand(domain, mRes,
						ObjectsPackage.eINSTANCE.getMResource_DoAction(),
						MResourceAction.FIX_ALL_TYPES);

				if (command.canExecute()) {
					command.execute();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static class MProperty_ChangeBehaviorActionCommand
			extends SetCommand {

		public MProperty_ChangeBehaviorActionCommand(EditingDomain domain,
				EObject owner, EStructuralFeature feature, Object value) {
			super(domain, owner, feature, value);

		}

		@Override
		protected boolean prepare() {
			boolean retValue = super.prepare();
			oldValue = ((MProperty) owner).getPropertyBehavior();
			return retValue;

		}

		@Override
		public void doUndo() {
			new SetCommand(domain, (MProperty) owner,
					McorePackage.eINSTANCE.getMProperty_PropertyBehavior(),
					getOldValue()).execute();

		}

		@Override
		public void doExecute() {

			new SetCommand(domain, (MProperty) owner,
					McorePackage.eINSTANCE.getMProperty_PropertyBehavior(),
					getSetValue()).execute();

		}

		public PropertyBehavior getSetValue() {
			MPropertyAction k = (com.montages.mcore.MPropertyAction) value;

			switch (k) {
			case INTO_DERIVED:
				return PropertyBehavior.DERIVED;
			case INTO_STORED:
				return PropertyBehavior.STORED;
			case INTO_CONTAINED:
				return PropertyBehavior.CONTAINED;
			default:
				return null;
			}
		}

		@Override
		public Collection<?> doGetAffectedObjects() {

			ArrayList<Object> affectedObj = new ArrayList<Object>();

			if (getSetValue() == PropertyBehavior.DERIVED
					&& ((MProperty) owner).getDirectSemantics() != null
					&& ((MProperty) owner).getDirectSemantics()
							.getResult() != null) {
				affectedObj.add(
						((MProperty) owner).getDirectSemantics().getResult());
				return affectedObj;
			}
			affectedObj.add((MProperty) owner);
			return affectedObj;
		}

	}

	public static class MProperty_IntoDerivedActionCommand
			extends MProperty_ChangeBehaviorActionCommand {

		public MProperty_IntoDerivedActionCommand(EditingDomain domain,
				EObject owner, EStructuralFeature feature, Object value) {
			super(domain, owner, feature, value);
		}

	}

	public static class MExprAnnotation_IntoApplyValue extends SetCommand {

		public MExprAnnotation_IntoApplyValue(EditingDomain domain,
				EObject owner, EStructuralFeature feature, Object value) {
			super(domain, owner, feature, value);

		}

		@Override
		public Collection<?> doGetAffectedObjects() {

			ArrayList<Object> ret = new ArrayList<Object>();

			// TODO: Need to fix for other MExprAnnotations ( Adapt code here)
			if (owner instanceof MResult) {
				MResult result = (MResult) owner;
				if (owner.eContainer() != null) {
					if (!result.getNamedExpression().isEmpty()
							&& !result.getNamedExpression().get(0)
									.allApplications().isEmpty()) {
						// ret.addAll(
						// result.getNamedExpression().get(0).allApplications().get(0).getOperands());
						ret.addAll(result.getNamedExpression().get(0)
								.allApplications().get(0).getOperands());
						ret.add(result.getNamedExpression().get(0)
								.allApplications().get(0));

					} else
						ret.add(owner);

				} else {
					ret.add(owner);
				}
			}

			return ret;
		}

		@Override
		public void doUndo() {

			MResult old = (MResult) oldValue;
			MResult own = (MResult) owner;
			MNamedExpression removeExpr = own.getNamedExpression().get(0);

			new RemoveCommand(domain, owner,
					AnnotationsPackage.eINSTANCE
							.getMExprAnnotation_NamedExpression(),
					removeExpr).execute();

			if (old.getNamedExpression().size() > 1) {
				new AddCommand(domain, owner,
						AnnotationsPackage.eINSTANCE
								.getMExprAnnotation_NamedExpression(),
						old.getNamedExpression()
								.get(old.getNamedExpression().size() - 1));
				new SetCommand(domain, owner,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_Base(),
						old.getBaseVar()).execute();

			} else
				new SetCommand(domain, owner,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_BaseVar(),
						null).execute();

		};

		@Override
		protected boolean prepare() {
			boolean prepare = super.prepare();

			oldValue = EcoreUtil.copy(owner);

			return prepare;
		}

	}

	public static class MClassifier_LabelValueCommand extends SetCommand {

		public MClassifier_LabelValueCommand(EditingDomain domain,
				EObject owner, EStructuralFeature feature, Object value) {
			super(domain, owner, feature, value);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void doUndo() {
			MClassifier mClass = (MClassifier) owner;

			new SetCommand(domain, mClass.getAnnotations(),
					AnnotationsPackage.eINSTANCE
							.getMClassifierAnnotations_Label(),
					null).execute();
		}

		@Override
		public Collection<?> doGetAffectedObjects() {
			MClassifier mClass = (MClassifier) owner;
			ArrayList<Object> retValue = new ArrayList<Object>();
			if (mClass.getAnnotations() != null
					&& mClass.getAnnotations().getLabel() != null) {
				retValue.addAll(mClass.getAnnotations().getLabel()
						.getNamedExpression());
				retValue.add(mClass.getAnnotations().getLabel());
				return retValue;
			}
			retValue.add(mClass);
			return retValue;
		}
	}

	public static class MComponent_Abstract extends SetCommand {

		public MComponent_Abstract(EditingDomain domain, EObject owner,
				EStructuralFeature feature, Object value) {
			super(domain, owner, feature, value);
			// TODO Auto-generated constructor stub
		}

		ArrayList<MProperty> orgDerivedProp = new ArrayList<MProperty>();
		ArrayList<MProperty> abstOfDerivedProp = new ArrayList<MProperty>();
		ArrayList<MProperty> propWithOpposite = new ArrayList<MProperty>();
		ArrayList<MProperty> operations = new ArrayList<MProperty>();
		ArrayList<MProperty> abstOperations = new ArrayList<MProperty>();
		ArrayList<MClassifier> addSuperTypes = new ArrayList<MClassifier>();

		public void doExecute() {
			MComponent mComp = (MComponent) owner;
			MComponent newComp = null;
			if (mComp.getOwnedPackage().get(0).getClassifier().get(0)
					.getProperty().isEmpty()) {
				newComp = new WizardData.Builder()
						.setComponent(mComp.getDomainType(),
								mComp.getDomainName(),
								"Abstract ".concat(mComp.getName()))
						.setPackage(
								"Abstract ".concat(mComp.getOwnedPackage()
										.get(0).getName()),
								mComp.getOwnedPackage().get(0).getClassifier()
										.get(0).getName(),
								"Delete")
						.build().createComponent();
			} else
				newComp = new WizardData.Builder()
						.setComponent(mComp.getDomainType(),
								mComp.getDomainName(),
								"Abstract ".concat(mComp.getName()))
						.setPackage(
								"Abstract ".concat(mComp.getOwnedPackage()
										.get(0).getName()),
								mComp.getOwnedPackage().get(0).getClassifier()
										.get(0).getName(),
								mComp.getOwnedPackage().get(0).getClassifier()
										.get(0).getProperty().get(0)
										.getCalculatedName())
						.build().createComponent();

			newComp.setAbstractComponent(true);
			newComp.setCountImplementations(1);

			// If the root class already has a super type
			if (!mComp.getOwnedPackage().get(0).getClassifier().get(0)
					.getSuperType().isEmpty()) {
				addSuperTypes.add(
						mComp.getOwnedPackage().get(0).getClassifier().get(0));
			}

			mComp.getOwnedPackage().get(0).getClassifier().get(0)
					.setToggleSuperType(newComp.getOwnedPackage().get(0)
							.getClassifier().get(0));

			newComp.getOwnedPackage().get(0).getClassifier().get(0)
					.setAbstractClass(true);

			if (mComp.isSetNamePrefix())
				newComp.setNamePrefix(
						"Abstract " + mComp.getDerivedNamePrefix());
			else
				newComp.setNamePrefix("Abstract");
			newComp.setNamePrefixForFeatures(true);
			if (mComp.getRootPackage().getNamePrefix() != null
					&& !mComp.getRootPackage().getNamePrefix().isEmpty()
					&& mComp.getRootPackage().isSetNamePrefix()) {
				newComp.getOwnedPackage().get(0).setNamePrefix(
						"Abstract " + mComp.getRootPackage().getNamePrefix());
			}

			// Here we save containments and references, because their types
			// need to be the abstract class and not the normal class
			ArrayList<MProperty> specialCases = new ArrayList<MProperty>();
			specialCases = createCompleteSubpackages(mComp, newComp,
					mComp.getRootPackage(), newComp.getRootPackage(),
					specialCases);

			newComp.getOwnedPackage().get(0).getResourceRootClass()
					.getInstance().get(0).getResource().getContainingFolder()
					.unsetResource();

			// Set the super types
			for (MClassifier mClass : addSuperTypes) {
				MClassifier abstractMissSuperType = searchAbstractClassifier(
						mClass, newComp.getRootPackage());
				for (MClassifier superClass : mClass.getSuperType()) {
					// With this if statement we avoid potential errors
					if (superClass.getContainingPackage()
							.getContainingComponent() == mComp) {
						abstractMissSuperType.getSuperType()
								.add(searchAbstractClassifier(superClass,
										newComp.getRootPackage()));
					}
				}
			}

			for (MProperty mprop : specialCases) {
				MClassifier abstractSuperClass = null;

				if (mprop.getType().getKind()
						.equals(ClassifierKind.CLASS_TYPE)) {
					for (MClassifier mClass : mprop.getType().getSuperType()) {
						/*
						 * The "Abstract" is necessary because of the "Abstract"
						 * as prefix "Abstraction" at the end is necessary if
						 * the typed class in the concrete component is abstract
						 */
						if (mprop.getType().getAbstractClass()) {
							// No duplicates of "Abstraction" in the name is
							// implemented!
							if (mprop.getType().getEName()
									.contains("Abstraction")) {
								if (mClass.getEName().equals("Abstract"
										+ mprop.getType().getEName())) {
									abstractSuperClass = mClass;
									break;
								}
							} else {
								if (mClass.getEName()
										.equals("Abstract"
												+ mprop.getType().getEName()
												+ "Abstraction")) {
									abstractSuperClass = mClass;
									break;
								}
							}
						} else {
							if (mClass.getEName().equals(
									"Abstract" + mprop.getType().getEName())) {
								abstractSuperClass = mClass;
								break;
							}
						}

					}
					mprop.setType(abstractSuperClass);
				}
				// Else it is an enum or data type, these types also need to be
				// changed to avoid circles
				else {
					mprop.setType(searchAbstractClassifier(mprop.getType(),
							newComp.getRootPackage()));
				}
			}

			// Sets the opposite correct
			for (MProperty mprop : propWithOpposite) {
				mprop.setOpposite(searchAbstractProperty(mprop.getOpposite(),
						mprop.getType().allProperties()));
			}

			for (int i = 0; i < operations.size(); i++) {
				for (MOperationSignature opSig : operations.get(i)
						.getOperationSignature()) {
					MOperationSignature abstOpSig = McoreFactory.eINSTANCE
							.createMOperationSignature();
					abstOperations.get(i).getOperationSignature()
							.add(abstOpSig);
					for (MParameter mpar : opSig.getParameter()) {
						MParameter abstPar = McoreFactory.eINSTANCE
								.createMParameter();
						abstOpSig.getParameter().add(abstPar);
						if (mpar.isSetName()) {
							abstPar.setName("Abstract " + mpar.getName());
						}

						if (mpar.isSetType()) {
							abstPar.setType(searchAbstractClassifier(
									mpar.getType(), newComp.getRootPackage()));
						} else {
							abstPar.setSimpleType(mpar.getSimpleType());
						}
					}
				}
			}

			/*
			 * handle the derived property to move their semantics to the
			 * abstract property and change the used properties in the semantics
			 * Add the new semantics to the derived property
			 */
			for (int i = 0; i < orgDerivedProp.size(); i++) {
				// Move PropertyAnnotation of original property
				MPropertyAnnotations orgPropAnno = orgDerivedProp.get(i)
						.getDirectSemantics();
				orgPropAnno.setProperty(abstOfDerivedProp.get(i));
				MClassifierAnnotations abstractSemantics = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				abstractSemantics.getPropertyAnnotations().add(orgPropAnno);
				abstOfDerivedProp.get(i).getContainingClassifier()
						.setAnnotations(abstractSemantics);
				// Need recursive do action to change all properties in the
				// semantics!!

				if ((orgPropAnno.getResult().getNamedExpression().isEmpty()
						&& orgPropAnno.getResult().getNamedConstant().isEmpty())
						|| orgPropAnno.getResult()
								.getUseExplicitOcl() == true) {
					if (orgPropAnno.getResult().isSetElement1()) {
						orgPropAnno.getResult()
								.setElement1(searchAbstractProperty(
										orgPropAnno.getResult().getElement1(),
										orgPropAnno.getResult()
												.getChainEntryType()
												.allProperties()));
						if (orgPropAnno.getResult().isSetElement2()) {
							// Changes Element 2 to the abstract property
							orgPropAnno.getResult()
									.setElement2(searchAbstractProperty(
											orgPropAnno.getResult()
													.getElement2(),
											orgPropAnno.getResult()
													.getElement2EntryType()
													.allProperties()));
							if (orgPropAnno.getResult().isSetElement3()) {
								// Changes Element 3 to the abstract property
								orgPropAnno.getResult()
										.setElement1(searchAbstractProperty(
												orgPropAnno.getResult()
														.getElement3(),
												orgPropAnno.getResult()
														.getElement3EntryType()
														.allProperties()));
							}
						}
					}
				} else {
					// Changes all chains in the NamedExpressions
					for (MNamedExpression expr : orgPropAnno.getResult()
							.getNamedExpression()) {
						if (expr.getExpression().getKindLabel().equals("IF")) {
							MIf ifStat = (MIf) expr.getExpression();
							deepExpression(ifStat.getCondition());
							if (ifStat.getThenPart() != null) {
								deepExpression(
										ifStat.getThenPart().getExpression());
							}
							if (ifStat.getElsePart() != null) {
								deepExpression(
										ifStat.getElsePart().getExpression());
							}
						}

						if (expr.getExpression().getKindLabel()
								.equals("CHAIN")) {
							deepExpression((MChain) expr.getExpression());
						}

						if (expr.getExpression().getKindLabel()
								.equals("APPLY")) {
							MApplication apply = (MApplication) (expr
									.getExpression());
							for (MChainOrApplication chainApp : apply
									.getOperands()) {
								deepExpression(chainApp);
							}
						}

						if (expr.getExpression().getKindLabel()
								.equals("LITERAL")) {
							MLiteralValueExpr literal = (MLiteralValueExpr) (expr
									.getExpression());
							if (literal.isSetEnumerationType()) {
								literal.setEnumerationType(
										searchAbstractClassifier(
												literal.getEnumerationType(),
												newComp.getRootPackage()));
								if (literal.isSetValue()) {
									for (MLiteral mLit : literal
											.getAllowedLiterals()) {
										if (mLit.getCalculatedName()
												.equals("Abstract " + literal
														.getValue()
														.getCalculatedName())) {
											literal.setValue(mLit);
											break;
										}
									}
								}
							}
						}

						if (expr.getExpression().getKindLabel()
								.equals("->collect(")) {
							MCollectionExpression coll = (MCollectionExpression) expr
									.getExpression();
							deepExpression(coll.getExpression());
						}
					}
				}

				// Create new PropertyAnnotation
				MPropertyAnnotations newAnno = AnnotationsFactory.eINSTANCE
						.createMPropertyAnnotations();
				newAnno.setProperty(orgDerivedProp.get(i));

				MClassifierAnnotations orgSemantics = orgDerivedProp.get(i)
						.getContainingClassifier().getAnnotations();

				orgSemantics.getPropertyAnnotations().add(newAnno);

				MResult newResult = AnnotationsFactory.eINSTANCE
						.createMResult();
				newResult.setBase(ExpressionBase.SELF_OBJECT);
				newResult.setElement1(abstOfDerivedProp.get(i));
				newAnno.setResult(newResult);
			}
			// Prepare for triggering the action more than one time
			orgDerivedProp.clear();
			abstOfDerivedProp.clear();
			propWithOpposite.clear();
			abstOperations.clear();
			operations.clear();
			addSuperTypes.clear();

			if (!mComp.getOwnedPackage().get(0).getResourceRootClass()
					.getInstance().isEmpty()) {
				mComp.getOwnedPackage().get(0).getResourceRootClass()
						.getInstance().get(0)
						.setDoAction(MObjectAction.COMPLETE_SLOTS);
			}

			IProgressMonitor monitor = new NullProgressMonitor();
			monitor.beginTask("Create Component", 5);

			try {
				CreateProject.create(newComp, SubMonitor.convert(monitor, 1));
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			GenerateTableEditor.generate(newComp,
					SubMonitor.convert(monitor, 1));
			GenerateEcore.transform(newComp, SubMonitor.convert(monitor, 1));

			GenerateEditorConfig.generate(newComp,
					SubMonitor.convert(monitor, 1));

			monitor.done();

			// So the component can load the resource correct after reopening
			String uriString = newComp.getDerivedURI();
			uriString = uriString.replaceFirst("http", "mcore");
			URI fileURI = URI.createURI(uriString);
			fileURI = fileURI
					.appendSegment(newComp.getEName().toLowerCase() + ".mcore");

			Resource resource = mComp.getOwnedPackage().get(0).eResource()
					.getResourceSet().createResource(fileURI);
			resource.getContents().add(newComp);

			fileURI = URI.createURI(uriString);
			fileURI = fileURI.appendSegment(
					newComp.getOwnedPackage().get(0).getEName().toLowerCase()
							+ ".mtableeditor");

			resource = mComp.getOwnedPackage().get(0).eResource()
					.getResourceSet().createResource(fileURI);
			resource.getContents().add(newComp.getMainEditor());
		}

		/**
		 * This method creates a new property with the values of the original
		 * one There are three special values: changeable, hasStorage and
		 * containment
		 * 
		 * @param original
		 * @return
		 */
		public MProperty copyMProperty(MProperty original) {
			MProperty copy = McoreFactory.eINSTANCE.createMProperty();

			if (original.isSetName() && (original.getName() == null
					|| !original.getName().isEmpty())) {

				copy.setName(original.getCalculatedName());
			}

			copy.setChangeable(false);
			copy.setHasStorage(false);

			// If it is a containment it should be added as reference with the
			// correct class
			if (original.getContainment()) {
				copy.setContainment(false);
				copy.setName("Owned " + original.getCalculatedName());
			} else {
				copy.setContainment(original.getContainment());
			}

			if (original.getPropertyBehavior()
					.equals(PropertyBehavior.IS_OPERATION)) {
				operations.add(original);
				abstOperations.add(copy);
			}

			copy.setDescription(original.getDescription());
			copy.setEDefaultValueLiteral(original.getEDefaultValueLiteral());
			copy.setMandatory(original.getMandatory());
			copy.setMultiplicityCase(original.getMultiplicityCase());
			copy.setNotUnsettable(original.getNotUnsettable());
			if (original.isSetOpposite()) {
				copy.setOpposite(original.getOpposite());
				propWithOpposite.add(copy);
			}
			copy.setOrderWithinGroup(original.getOrderWithinGroup());
			copy.setPersisted(false);
			copy.setShortName(original.getShortName());
			copy.setSimpleType(original.getSimpleType());
			copy.setSingular(original.getSingular());
			copy.setSpecialEName(original.getSpecialEName());
			copy.setType(original.getType());
			copy.setTypePackage(original.getTypePackage());
			if (original.getUseCoreTypes() != null) {
				copy.setUseCoreTypes(original.getUseCoreTypes());
			}
			return copy;
		}

		/**
		 * This function adds the abstract classes and properties to the
		 * abstract component. If there are any subpackages this function will
		 * recursive complete them.
		 * 
		 * @param mComp
		 *            The trigger component
		 * @param newComp
		 *            The new created abstract component
		 * @param orgPack
		 *            The package of the original component
		 * @param abstPack
		 *            The 'abstract' package of the abstract component, which
		 *            needs to be completed
		 */
		public ArrayList<MProperty> createCompleteSubpackages(MComponent mComp,
				MComponent newComp, MPackage orgPack, MPackage abstPack,
				ArrayList<MProperty> specialCases) {
			for (int i = 0; i < orgPack.getClassifier().size(); i++) {
				MClassifier mClass = orgPack.getClassifier().get(i);

				MPropertiesGroup group = McoreFactory.eINSTANCE
						.createMPropertiesGroup();
				group.setName("Abstract");

				MClassifierAnnotations abstractSemantics = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				group.setAnnotations(abstractSemantics);

				// The root packages has already the first class and property,
				// so this is a special case
				if (i == 0 && orgPack == mComp.getRootPackage()) {
					// If there is no property in the root class it won't run
					// the for loop, so it needs to remove the first property
					if (mClass.getProperty().isEmpty()) {
						abstPack.getClassifier().get(0).getProperty().remove(0);
					}
					for (int h = 0; h < mClass.getProperty().size(); h++) {
						if (h == 0) {
							abstPack.getClassifier().get(0).getProperty()
									.remove(0);
						}
						MProperty abstractProp = copyMProperty(
								mClass.getProperty().get(h));
						abstPack.getClassifier().get(0).getProperty()
								.add(abstractProp);
						if (mClass.getProperty().get(h).getPropertyBehavior()
								.equals(PropertyBehavior.STORED)) {
							/*
							 * The property/attribute has an enum or a data type
							 * as type, which should be changed Also it
							 * shouldn't get an annotation
							 */
							if (mClass.getProperty().get(h).getSimpleType()
									.equals(SimpleType.NONE)
									&& (mClass.getProperty().get(h).getType()
											.getKind()
											.equals(ClassifierKind.ENUMERATION)
											|| mClass.getProperty().get(h)
													.getType().getKind()
													.equals(ClassifierKind.DATA_TYPE))) {
								specialCases.add(abstractProp);
							}
							// It is a normal property, so the result should be
							// created and added
							else {
								MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
										.createMPropertyAnnotations();
								abstractAnno.setProperty(abstractProp);
								abstractSemantics.getPropertyAnnotations()
										.add(abstractAnno);

								MResult abstractResult = AnnotationsFactory.eINSTANCE
										.createMResult();
								abstractResult
										.setBase(ExpressionBase.SELF_OBJECT);
								abstractResult.setElement1(
										mClass.getProperty().get(h));
								abstractAnno.setResult(abstractResult);
							}
						}
						// Adds the semantics which define that the result of
						// the abstract property is the stored concrete property
						if (mClass.getProperty().get(h).getPropertyBehavior()
								.equals(PropertyBehavior.CONTAINED)) {
							MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
									.createMPropertyAnnotations();
							abstractAnno.setProperty(abstractProp);
							abstractSemantics.getPropertyAnnotations()
									.add(abstractAnno);

							MResult abstractResult = AnnotationsFactory.eINSTANCE
									.createMResult();
							abstractResult.setBase(ExpressionBase.SELF_OBJECT);
							abstractResult
									.setElement1(mClass.getProperty().get(h));
							abstractAnno.setResult(abstractResult);
						}

						// Saves the property if it is derived, because this is
						// a special case, which needs to be handled
						if (mClass.getProperty().get(h).getPropertyBehavior()
								.equals(PropertyBehavior.DERIVED)) {
							orgDerivedProp.add(mClass.getProperty().get(h));
							abstOfDerivedProp.add(abstractProp);
						}

						// Saves the special kind so that its type can be
						// changed later
						if (mClass.getProperty().get(h).getKind()
								.equals(PropertyKind.REFERENCE)) {
							specialCases.add(abstractProp);
						}
					}

					for (int h = 0; h < mClass.getAllPropertyGroups()
							.size(); h++) {
						if (!mClass.getAllPropertyGroups().get(h).getName()
								.equals("Abstract")) {
							MPropertiesGroup propGroup = McoreFactory.eINSTANCE
									.createMPropertiesGroup();
							propGroup.setName("Abstract " + mClass
									.getAllPropertyGroups().get(h).getName());
							// Choose the right eContainer of the property group
							// via index number
							if (mClass.getAllPropertyGroups().get(h)
									.eContainer() instanceof MPropertiesGroup) {
								abstPack.getClassifier().get(0)
										.getAllPropertyGroups()
										.get(mClass.getAllPropertyGroups()
												.indexOf(
														((MPropertiesGroup) mClass
																.getAllPropertyGroups()
																.get(h)
																.eContainer())))
										.getPropertyGroup().add(propGroup);
							} else {
								abstPack.getClassifier().get(0)
										.getPropertyGroup().add(propGroup);
							}

							for (MProperty mprop : mClass.getAllPropertyGroups()
									.get(h).getProperty()) {
								MProperty abstractProp = copyMProperty(mprop);
								propGroup.getProperty().add(abstractProp);

								if (mprop.getPropertyBehavior()
										.equals(PropertyBehavior.STORED)) {
									/*
									 * The property/attribute has an enum or a
									 * data type as type, which should be
									 * changed Also it shouldn't get an
									 * annotation
									 */
									if (mprop.getSimpleType()
											.equals(SimpleType.NONE)
											&& (mprop.getType().getKind()
													.equals(ClassifierKind.ENUMERATION)
													|| mprop.getType().getKind()
															.equals(ClassifierKind.DATA_TYPE))) {
										specialCases.add(abstractProp);
									}
									// Adds the semantics which define that the
									// result of the abstract property is the
									// stored concrete property
									else {
										MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
												.createMPropertyAnnotations();
										abstractAnno.setProperty(abstractProp);
										abstractSemantics
												.getPropertyAnnotations()
												.add(abstractAnno);

										MResult abstractResult = AnnotationsFactory.eINSTANCE
												.createMResult();
										abstractResult.setBase(
												ExpressionBase.SELF_OBJECT);
										abstractResult.setElement1(mprop);
										abstractAnno.setResult(abstractResult);
									}
								}

								if (mprop.getPropertyBehavior()
										.equals(PropertyBehavior.CONTAINED)) {
									MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
											.createMPropertyAnnotations();
									abstractAnno.setProperty(abstractProp);
									abstractSemantics.getPropertyAnnotations()
											.add(abstractAnno);

									MResult abstractResult = AnnotationsFactory.eINSTANCE
											.createMResult();
									abstractResult.setBase(
											ExpressionBase.SELF_OBJECT);
									abstractResult.setElement1(mprop);
									abstractAnno.setResult(abstractResult);
								}

								// Saves the property if it is derived, because
								// this is a special case, which needs to be
								// handled
								if (mprop.getPropertyBehavior()
										.equals(PropertyBehavior.DERIVED)) {
									orgDerivedProp.add(mprop);
									abstOfDerivedProp.add(abstractProp);
								}

								// Saves the special kind so that its type can
								// be changed later
								if (mprop.getKind()
										.equals(PropertyKind.REFERENCE)) {
									specialCases.add(abstractProp);
								}
							}
						}
					}
					mClass.getPropertyGroup().add(group);
				} else {
					if (mClass.getKind().equals(ClassifierKind.CLASS_TYPE)) {
						MClassifier newClass = McoreFactory.eINSTANCE
								.createMClassifier();
						if (mClass.getAbstractClass()
								&& !mClass.getName().contains("Abstraction"))
							newClass.setName(mClass.getName() + " Abstraction");
						else
							newClass.setName(mClass.getName());
						newClass.setAbstractClass(true);
						abstPack.getClassifier().add(newClass);

						// If the class already has a super type
						if (!mClass.getSuperType().isEmpty()) {
							addSuperTypes.add(mClass);
						}

						mClass.setToggleSuperType(newClass);

						for (MProperty mprop : mClass.getProperty()) {
							MProperty abstractProp = copyMProperty(mprop);
							newClass.getProperty().add(abstractProp);

							if (mprop.getPropertyBehavior()
									.equals(PropertyBehavior.STORED)) {
								/*
								 * The property/attribute has an enum or a data
								 * type as type, which should be changed Also it
								 * shouldn't get an annotation
								 */
								if (mprop.getSimpleType()
										.equals(SimpleType.NONE)
										&& (mprop.getType().getKind().equals(
												ClassifierKind.ENUMERATION)
												|| mprop.getType().getKind()
														.equals(ClassifierKind.DATA_TYPE))) {
									specialCases.add(abstractProp);
								}
								// Adds the semantics which define that the
								// result of the abstract property is the stored
								// concrete property
								else {
									MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
											.createMPropertyAnnotations();
									abstractAnno.setProperty(abstractProp);
									abstractSemantics.getPropertyAnnotations()
											.add(abstractAnno);

									MResult abstractResult = AnnotationsFactory.eINSTANCE
											.createMResult();
									abstractResult.setBase(
											ExpressionBase.SELF_OBJECT);
									abstractResult.setElement1(mprop);
									abstractAnno.setResult(abstractResult);
								}
							}

							if (mprop.getPropertyBehavior()
									.equals(PropertyBehavior.CONTAINED)) {
								MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
										.createMPropertyAnnotations();
								abstractAnno.setProperty(abstractProp);
								abstractSemantics.getPropertyAnnotations()
										.add(abstractAnno);

								MResult abstractResult = AnnotationsFactory.eINSTANCE
										.createMResult();
								abstractResult
										.setBase(ExpressionBase.SELF_OBJECT);
								abstractResult.setElement1(mprop);
								abstractAnno.setResult(abstractResult);
							}

							// Saves the property if it is derived, because this
							// is a special case, which needs to be handled
							if (mprop.getPropertyBehavior()
									.equals(PropertyBehavior.DERIVED)) {
								orgDerivedProp.add(mprop);
								abstOfDerivedProp.add(abstractProp);
							}

							// Saves the special kind so that its type can be
							// changed later
							if (mprop.getKind()
									.equals(PropertyKind.REFERENCE)) {
								specialCases.add(abstractProp);
							}
						}

						for (int h = 0; h < mClass.getAllPropertyGroups()
								.size(); h++) {
							if (!mClass.getAllPropertyGroups().get(h).getName()
									.equals("Abstract")) {
								MPropertiesGroup propGroup = McoreFactory.eINSTANCE
										.createMPropertiesGroup();
								propGroup.setName("Abstract "
										+ mClass.getAllPropertyGroups().get(h)
												.getName());
								// Choose the right eContainer of the property
								// group via index number
								if (mClass.getAllPropertyGroups().get(h)
										.eContainer() instanceof MPropertiesGroup) {
									newClass.getAllPropertyGroups().get(mClass
											.getAllPropertyGroups()
											.indexOf(((MPropertiesGroup) mClass
													.getAllPropertyGroups()
													.get(h).eContainer())))
											.getPropertyGroup().add(propGroup);
								} else {
									newClass.getPropertyGroup().add(propGroup);
								}

								for (MProperty mprop : mClass
										.getAllPropertyGroups().get(h)
										.getProperty()) {
									MProperty abstractProp = copyMProperty(
											mprop);
									propGroup.getProperty().add(abstractProp);

									if (mprop.getPropertyBehavior()
											.equals(PropertyBehavior.STORED)) {
										/*
										 * The property/attribute has an enum or
										 * a data type as type, which should be
										 * changed Also it shouldn't get an
										 * annotation
										 */
										if (mprop.getSimpleType()
												.equals(SimpleType.NONE)
												&& (mprop.getType().getKind()
														.equals(ClassifierKind.ENUMERATION)
														|| mprop.getType()
																.getKind()
																.equals(ClassifierKind.DATA_TYPE))) {
											specialCases.add(abstractProp);
										}
										// Adds the semantics which define that
										// the result of the abstract property
										// is the stored concrete property
										else {
											MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
													.createMPropertyAnnotations();
											abstractAnno
													.setProperty(abstractProp);
											abstractSemantics
													.getPropertyAnnotations()
													.add(abstractAnno);

											MResult abstractResult = AnnotationsFactory.eINSTANCE
													.createMResult();
											abstractResult.setBase(
													ExpressionBase.SELF_OBJECT);
											abstractResult.setElement1(mprop);
											abstractAnno
													.setResult(abstractResult);
										}
									}

									if (mprop.getPropertyBehavior().equals(
											PropertyBehavior.CONTAINED)) {
										MPropertyAnnotations abstractAnno = AnnotationsFactory.eINSTANCE
												.createMPropertyAnnotations();
										abstractAnno.setProperty(abstractProp);
										abstractSemantics
												.getPropertyAnnotations()
												.add(abstractAnno);

										MResult abstractResult = AnnotationsFactory.eINSTANCE
												.createMResult();
										abstractResult.setBase(
												ExpressionBase.SELF_OBJECT);
										abstractResult.setElement1(mprop);
										abstractAnno.setResult(abstractResult);
									}

									// Saves the property if it is derived,
									// because this is a special case, which
									// needs to be handled
									if (mprop.getPropertyBehavior()
											.equals(PropertyBehavior.DERIVED)) {
										orgDerivedProp.add(mprop);
										abstOfDerivedProp.add(abstractProp);
									}

									// Saves the special kind so that its type
									// can be changed later
									if (mprop.getKind()
											.equals(PropertyKind.REFERENCE)) {
										specialCases.add(abstractProp);
									}
								}
							}
						}
						mClass.getPropertyGroup().add(group);
					} else {
						if (mClass.getKind().equals(ClassifierKind.DATA_TYPE)) {
							MClassifier newClass = McoreFactory.eINSTANCE
									.createMClassifier();
							newClass.setName(mClass.getName());
							abstPack.getClassifier().add(newClass);
							newClass.getProperty().clear();
							newClass.setSimpleType(mClass.getSimpleType());
							newClass.setInternalEClassifier(
									mClass.getInternalEClassifier());
							// There are no properties
						} else {
							// There is just the MClassifier type Enum left,
							// creates Enum and adds literals
							MClassifier newClass = McoreFactory.eINSTANCE
									.createMClassifier();
							newClass.setName(mClass.getName());
							abstPack.getClassifier().add(newClass);
							newClass.getProperty().clear();
							for (MLiteral mLit : mClass.getLiteral()) {
								MLiteral abstLit = McoreFactory.eINSTANCE
										.createMLiteral();
								abstLit.setName("Abstract " + mLit.getName());
								if (mLit.isSetConstantLabel()
										&& mLit.getConstantLabel() != null) {
									abstLit.setConstantLabel("Abstract "
											+ mLit.getConstantLabel());
								}
								newClass.getLiteral().add(abstLit);
							}
						}
					}
				}
			}

			// Check if there are any subpackes and make the function recursive
			for (MPackage mpack : orgPack.getSubPackage()) {
				MPackage newPack = McoreFactory.eINSTANCE.createMPackage();
				newPack.setName("Abstract " + mpack.getName());
				abstPack.getSubPackage().add(newPack);
				if (mpack.isSetNamePrefix()) {
					newPack.setNamePrefix("Abstract " + mpack.getNamePrefix());
				}
				createCompleteSubpackages(mComp, newComp, mpack, newPack,
						specialCases);
			}
			return specialCases;
		}

		/**
		 * Small search function Selection should search in the selection of
		 * properties the abstract one
		 * 
		 * @param orgProp
		 * @param selection
		 * @return
		 */
		public MProperty searchAbstractProperty(MProperty orgProp,
				EList<MProperty> selection) {
			for (MProperty mprop : selection) {
				String combinedEName = "abstract"
						+ orgProp.getEName().substring(0, 1).toUpperCase()
						+ orgProp.getEName().substring(1);
				if (mprop.getEName().equals(combinedEName)) {
					return mprop;
				}
			}

			for (MProperty mprop : selection) {
				String combinedEName = "abstractOwned"
						+ orgProp.getEName().substring(0, 1).toUpperCase()
						+ orgProp.getEName().substring(1);
				if (mprop.getEName().equals(combinedEName)) {
					return mprop;
				}
			}

			return null;

		}

		/**
		 * Small search function Selection should search in abstract Class, Enum
		 * or Data Type
		 * 
		 * @param orgClassifier
		 * @param abstRootPack
		 * @return
		 */
		public MClassifier searchAbstractClassifier(MClassifier orgClassifier,
				MPackage abstRootPack) {
			ArrayList<MPackage> allAbstPack = new ArrayList<MPackage>();
			allAbstPack.add(abstRootPack);
			allAbstPack.addAll(abstRootPack.getAllSubpackages());

			for (MPackage subPack : allAbstPack) {
				for (MClassifier abstClass : subPack.getClassifier()) {
					if (abstClass.getEName()
							.equals("Abstract" + orgClassifier.getEName())) {
						return abstClass;
					}
				}
			}

			/*
			 * It could be that it is an abstract class in the original model
			 * without "Abstraction" at the end of the name because of that this
			 * search case should also exist
			 */
			for (MPackage subPack : allAbstPack) {
				for (MClassifier abstClass : subPack.getClassifier()) {
					if (abstClass.getEName().equals("Abstract"
							+ orgClassifier.getEName() + "Abstraction")) {
						return abstClass;
					}
				}
			}

			return null;
		}

		public void deepExpression(MChainOrApplication mChainOrApplication) {
			if (mChainOrApplication.getKindLabel().equals("CHAIN")) {
				MChain chain = (MChain) mChainOrApplication;
				if (chain.isSetElement1()) {
					chain.setElement1(
							searchAbstractProperty(chain.getElement1(),
									chain.getChainEntryType().allProperties()));
					if (chain.isSetElement2()) {
						// Changes Element 2 to the abstract property
						chain.setElement2(searchAbstractProperty(
								chain.getElement2(),
								chain.getElement2EntryType().allProperties()));
						if (chain.isSetElement3()) {
							// Changes Element 3 to the abstract property
							chain.setElement1(
									searchAbstractProperty(chain.getElement3(),
											chain.getElement3EntryType()
													.allProperties()));
						}
					}
				}
			}

			if (mChainOrApplication.getKindLabel().equals("APPLY")) {
				MApplication apply = (MApplication) mChainOrApplication;
				for (MChainOrApplication chainApp : apply.getOperands()) {
					deepExpression(chainApp);
				}
			}

			if (mChainOrApplication.getKindLabel().equals("->collect(")) {
				MCollectionExpression coll = (MCollectionExpression) mChainOrApplication;
				deepExpression(coll.getExpression());
			}

			if (mChainOrApplication.getKindLabel().equals("LITERAL")) {
				MLiteralValueExpr literal = (MLiteralValueExpr) mChainOrApplication;
				if (literal.isSetEnumerationType()) {
					literal.setEnumerationType(searchAbstractClassifier(
							literal.getEnumerationType(),
							mChainOrApplication.getSelfObjectPackage()
									.getContainingComponent()
									.getRootPackage()));
					if (literal.isSetValue()) {
						for (MLiteral mLit : literal.getAllowedLiterals()) {
							if (mLit.getCalculatedName().equals("Abstract "
									+ literal.getValue().getCalculatedName())) {
								literal.setValue(mLit);
								break;
							}
						}
					}
				}
			}

		}

		@Override
		protected boolean prepare() {
			boolean prepare = super.prepare();

			oldValue = EcoreUtil.copy(owner);
			return prepare;
		}

		@Override
		public void doUndo() {
			new SetCommand(domain, owner.eContainer(),
					AnnotationsPackage.eINSTANCE
							.getMPropertyAnnotations_Result(),
					oldValue).execute();
		};
	}

	public static class MComponent_Implement extends SetCommand {

		public MComponent_Implement(EditingDomain domain, EObject owner,
				EStructuralFeature feature, Object value) {
			super(domain, owner, feature, value);
			// TODO Auto-generated constructor stub
		}

		ArrayList<MProperty> propWithOpposite = new ArrayList<MProperty>();
		ArrayList<MProperty> propWithType = new ArrayList<MProperty>();

		public void doExecute() {
			MComponent mComp = (MComponent) owner;

			String newName = mComp.getName().replaceFirst("Abstract ", "");
			newName = newName.concat(
					" Implementation " + mComp.getCountImplementations());
			mComp.setCountImplementations(mComp.getCountImplementations() + 1);

			// Why "Delete"? - Because we don't know if the first class even has
			// a property. It could be empty and this would trigger a NPE
			MComponent newComp = new WizardData.Builder()
					.setComponent(mComp.getDomainType(), mComp.getDomainName(),
							newName)
					.setPackage(
							mComp.getOwnedPackage().get(0).getName()
									.replaceFirst("Abstract ", ""),
							mComp.getOwnedPackage().get(0).getClassifier()
									.get(0).getCalculatedName(),
							"Delete")
					.build().createComponent();
			newComp.setAbstractComponent(false);

			newComp.getOwnedPackage().get(0).getClassifier().get(0)
					.getSuperType()
					.add(mComp.getOwnedPackage().get(0).getClassifier().get(0));
			newComp.getOwnedPackage().get(0).getClassifier().get(0)
					.getProperty().remove(0);

			createCompleteSubpackages(mComp, newComp, mComp.getRootPackage(),
					newComp.getRootPackage());

			// set the types for properties
			for (MProperty mProp : propWithType) {
				mProp.setType(searchImplementationClassifier(mProp.getType(),
						newComp.getRootPackage()));
			}
			// Sets the opposite correct
			for (MProperty mprop : propWithOpposite) {
				mprop.setOpposite(searchImplementationProperty(
						mprop.getOpposite(), mprop.getType().allProperties()));
			}

			propWithType.clear();
			propWithOpposite.clear();

			if (!newComp.getOwnedPackage().get(0).getResourceRootClass()
					.getInstance().isEmpty()) {
				newComp.getOwnedPackage().get(0).getResourceRootClass()
						.getInstance().get(0)
						.setDoAction(MObjectAction.COMPLETE_SLOTS);
			}

			IProgressMonitor monitor = new NullProgressMonitor();
			monitor.beginTask("Create Component", 5);

			try {
				CreateProject.create(newComp, SubMonitor.convert(monitor, 1));
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			GenerateTableEditor.generate(newComp,
					SubMonitor.convert(monitor, 1));
			GenerateEcore.transform(newComp, SubMonitor.convert(monitor, 1));

			GenerateEditorConfig.generate(newComp,
					SubMonitor.convert(monitor, 1));

			monitor.done();

			// So the component can load the resource correct after reopening
			String uriString = newComp.getDerivedURI();
			uriString = uriString.replaceFirst("http", "mcore");
			URI fileURI = URI.createURI(uriString);
			fileURI = fileURI
					.appendSegment(newComp.getEName().toLowerCase() + ".mcore");

			Resource resource = mComp.getOwnedPackage().get(0).eResource()
					.getResourceSet().createResource(fileURI);
			resource.getContents().add(newComp);

			fileURI = URI.createURI(uriString);
			fileURI = fileURI.appendSegment(
					newComp.getOwnedPackage().get(0).getEName().toLowerCase()
							+ ".mtableeditor");

			resource = mComp.getOwnedPackage().get(0).eResource()
					.getResourceSet().createResource(fileURI);
			resource.getContents().add(newComp.getMainEditor());
		}

		public void createCompleteSubpackages(MComponent mComp,
				MComponent newComp, MPackage orgPack, MPackage implPack) {
			for (MClassifier abstClass : orgPack.getClassifier()) {
				ArrayList<MProperty> isDerivedProp = new ArrayList<MProperty>();
				ArrayList<MProperty> isStoredProp = new ArrayList<MProperty>();

				MClassifier newImplClass = null;
				// Decide if there is the need to create a new class
				if (mComp.getOwnedPackage().get(0).getClassifier()
						.indexOf(abstClass) == 0) {
					newImplClass = newComp.getOwnedPackage().get(0)
							.getClassifier().get(0);
				} else {
					newImplClass = McoreFactory.eINSTANCE.createMClassifier();
					newImplClass.setName(abstClass.getCalculatedName());
					newImplClass.setToggleSuperType(abstClass);
					// Abstraction is a sign that the class of the concrete
					// component is abstract
					if (abstClass.getCalculatedName().contains("Abstraction")) {
						newImplClass.setAbstractClass(true);
					}
					implPack.getClassifier().add(newImplClass);
				}

				for (MPropertyAnnotations abstPropAnno : abstClass
						.allPropertyAnnotations()) {
					if (abstPropAnno.getResult() != null
							&& abstClass.getOwnedProperty()
									.contains(abstPropAnno.getProperty())) {
						// In the concrete component it is a derived attribute
						isDerivedProp.add(abstPropAnno.getProperty());
					}
				}
				for (MProperty abstProp : abstClass.getOwnedProperty()) {
					if (!isDerivedProp.contains(abstProp)) {
						isStoredProp.add(abstProp);
					}
					if (abstProp.getOpposite() != null) {
						propWithOpposite.add(abstProp);
					}
				}
				MClassifierAnnotations newClassAnno = null;
				if (!isDerivedProp.isEmpty()) {
					newClassAnno = AnnotationsFactory.eINSTANCE
							.createMClassifierAnnotations();
					newImplClass.setAnnotations(newClassAnno);
				}

				for (MProperty abstProp : isDerivedProp) {
					MProperty newDerivedProp = McoreFactory.eINSTANCE
							.createMProperty();
					newImplClass.getProperty().add(newDerivedProp);
					newDerivedProp.setName(abstProp.getCalculatedName());
					newDerivedProp.setHasStorage(false);
					newDerivedProp.setMultiplicityCase(
							abstProp.getMultiplicityCase());
					newDerivedProp.setPersisted(abstProp.getPersisted());

					if (abstProp.getSimpleType() == SimpleType.NONE) {
						newDerivedProp.setType(abstProp.getType());
						propWithType.add(newDerivedProp);
					} else {
						newDerivedProp.setSimpleType(abstProp.getSimpleType());
					}

					// Set the property annotations
					MPropertyAnnotations newDerivedAnno = AnnotationsFactory.eINSTANCE
							.createMPropertyAnnotations();
					newDerivedAnno.setProperty(newDerivedProp);
					MResult newDerivedResult = AnnotationsFactory.eINSTANCE
							.createMResult();
					newDerivedResult.setBase(ExpressionBase.SELF_OBJECT);
					newDerivedResult.setElement1(abstProp);
					newDerivedAnno.setResult(newDerivedResult);
					newClassAnno.getPropertyAnnotations().add(newDerivedAnno);
				}

				MPropertiesGroup abst = null;
				newClassAnno = null;
				if (!isStoredProp.isEmpty()) {
					abst = McoreFactory.eINSTANCE.createMPropertiesGroup();
					abst.setName("Abstract");
					newImplClass.getPropertyGroup().add(abst);
					newClassAnno = AnnotationsFactory.eINSTANCE
							.createMClassifierAnnotations();
					abst.setAnnotations(newClassAnno);
				}

				for (MProperty abstProp : isStoredProp) {
					MProperty newStoredProp = McoreFactory.eINSTANCE
							.createMProperty();
					newImplClass.getProperty().add(newStoredProp);
					newStoredProp.setName(abstProp.getCalculatedName());
					newStoredProp.setHasStorage(true);
					newStoredProp.setMultiplicityCase(
							abstProp.getMultiplicityCase());
					newStoredProp.setPersisted(abstProp.getPersisted());
					newStoredProp.setChangeable(abstProp.getChangeable());

					if (abstProp.getSimpleType() == SimpleType.NONE) {
						newStoredProp.setSimpleType(SimpleType.NONE);
						newStoredProp.setType(abstProp.getType());
						propWithType.add(newStoredProp);
					} else {
						newStoredProp.setSimpleType(abstProp.getSimpleType());
					}

					// Set the property annotations for all not contained
					// properties
					if (!abstProp.getCalculatedName().contains("Owned ")) {
						MPropertyAnnotations newDerivedAnno = AnnotationsFactory.eINSTANCE
								.createMPropertyAnnotations();
						newDerivedAnno.setProperty(abstProp);
						MResult newDerivedResult = AnnotationsFactory.eINSTANCE
								.createMResult();
						newDerivedResult.setBase(ExpressionBase.SELF_OBJECT);
						newDerivedResult.setElement1(newStoredProp);
						newDerivedAnno.setResult(newDerivedResult);
						newClassAnno.getPropertyAnnotations()
								.add(newDerivedAnno);
					} else {
						newStoredProp.setContainment(true);
					}
				}
			}

			// Check if there are any subpackes and make the function recursive
			for (MPackage mpack : orgPack.getSubPackage()) {
				MPackage newPack = McoreFactory.eINSTANCE.createMPackage();
				String newPackName = mpack.getName().replaceFirst("Abstract ",
						"");
				newPack.setName(newPackName);
				implPack.getSubPackage().add(newPack);
				createCompleteSubpackages(mComp, newComp, mpack, newPack);
			}
		}

		/**
		 * Small search function Selection should search in implementation
		 * Class, Enum or Data Type
		 * 
		 * @param orgClassifier
		 * @param implRootPack
		 * @return
		 */
		public MClassifier searchImplementationClassifier(
				MClassifier orgClassifier, MPackage implRootPack) {
			for (MClassifier abstClass : implRootPack.getClassifier()) {
				if (abstClass.getName()
						.equals(orgClassifier.getCalculatedName())) {
					return abstClass;
				}
			}

			for (MPackage subPack : implRootPack.getAllSubpackages()) {
				for (MClassifier abstClass : subPack.getClassifier()) {
					if (abstClass.getName()
							.equals(orgClassifier.getCalculatedName())) {
						return abstClass;
					}
				}
			}

			return null;
		}

		/**
		 * Small search function Selection should search in the selection of
		 * properties the abstract one
		 * 
		 * @param orgProp
		 * @param selection
		 * @return
		 */
		public MProperty searchImplementationProperty(MProperty orgProp,
				EList<MProperty> selection) {
			for (MProperty mprop : selection) {
				if (mprop.getName().equals(orgProp.getCalculatedName())) {
					return mprop;
				}
			}
			return null;

		}

		@Override
		protected boolean prepare() {
			boolean prepare = super.prepare();

			oldValue = EcoreUtil.copy(owner);
			return prepare;
		}

		@Override
		public void doUndo() {
			new SetCommand(domain, owner.eContainer(),
					AnnotationsPackage.eINSTANCE
							.getMPropertyAnnotations_Result(),
					oldValue).execute();
		};
	}

}
