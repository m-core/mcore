/**
 */
package com.montages.mcore.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MOperationSignature;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.impl.MOperationSignatureImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MOperationSignature} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MOperationSignatureItemProvider extends MTypedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignatureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof MOperationSignature) {
			MOperationSignature typedOwner = ((MOperationSignature) owner);

			if (feature == McorePackage.eINSTANCE
					.getMOperationSignature_DoAction())
				myUpdate = typedOwner.doAction$Update(
						(com.montages.mcore.MOperationSignatureAction) value);

		}

		return myUpdate == null
				? super.createSetCommand(domain, owner, feature, value, index)
				: myUpdate.getContainingTransition()
						.interpreteTransitionAsCommand();

	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addOperationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addELabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInternalEOperationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationSignature_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationSignature_doAction_feature",
						"_UI_MOperationSignature_type"),
				McorePackage.Literals.MOPERATION_SIGNATURE__DO_ACTION, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Operation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationSignature_operation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationSignature_operation_feature",
						"_UI_MOperationSignature_type"),
				McorePackage.Literals.MOPERATION_SIGNATURE__OPERATION, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MOperationSignature_containingClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationSignature_containingClassifier_feature",
						"_UI_MOperationSignature_type"),
				McorePackage.Literals.MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ELabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addELabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ELabel feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationSignature_eLabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationSignature_eLabel_feature",
						"_UI_MOperationSignature_type"),
				McorePackage.Literals.MOPERATION_SIGNATURE__ELABEL, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Internal EOperation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInternalEOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Internal EOperation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationSignature_internalEOperation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationSignature_internalEOperation_feature",
						"_UI_MOperationSignature_type"),
				McorePackage.Literals.MOPERATION_SIGNATURE__INTERNAL_EOPERATION,
				true, false, true, null,
				getString("_UI_RelationtoECorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION);
			childrenFeatures
					.add(McorePackage.Literals.MOPERATION_SIGNATURE__PARAMETER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MOperationSignature.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator()
				.getImage("full/obj16/MOperationSignature"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MOperationSignatureImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MOperationSignature.class)) {
		case McorePackage.MOPERATION_SIGNATURE__ELABEL:
		case McorePackage.MOPERATION_SIGNATURE__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
		case McorePackage.MOPERATION_SIGNATURE__PARAMETER:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION,
				AnnotationsFactory.eINSTANCE.createMGeneralAnnotation()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MOPERATION_SIGNATURE__PARAMETER,
				McoreFactory.eINSTANCE.createMParameter()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
