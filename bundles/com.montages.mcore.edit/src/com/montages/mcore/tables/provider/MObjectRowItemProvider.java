/**
 */
package com.montages.mcore.tables.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.provider.McoreEditPlugin;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.TablesFactory;
import com.montages.mcore.tables.TablesPackage;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.tables.MObjectRow} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MObjectRowItemProvider extends ItemProviderAdapter
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectRowItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContainingHeaderRowPropertyDescriptor(object);
			addObjectPropertyDescriptor(object);
			addColumn1ValuesPropertyDescriptor(object);
			addColumn1ReferencedObjectsPropertyDescriptor(object);
			addColumn2ReferencedObjectsPropertyDescriptor(object);
			addColumn3ReferencedObjectsPropertyDescriptor(object);
			addColumn4ReferencedObjectsPropertyDescriptor(object);
			addColumn5ReferencedObjectsPropertyDescriptor(object);
			addColumn1DataValuesPropertyDescriptor(object);
			addColumn1LiteralValuePropertyDescriptor(object);
			addColumn2DataValuesPropertyDescriptor(object);
			addColumn2LiteralValuePropertyDescriptor(object);
			addColumn3DataValuesPropertyDescriptor(object);
			addColumn3LiteralValuePropertyDescriptor(object);
			addColumn4DataValuesPropertyDescriptor(object);
			addColumn4LiteralValuePropertyDescriptor(object);
			addColumn5DataValuesPropertyDescriptor(object);
			addColumn5LiteralValuePropertyDescriptor(object);
			addColumn2ValuesPropertyDescriptor(object);
			addColumn3ValuesPropertyDescriptor(object);
			addColumn4ValuesPropertyDescriptor(object);
			addColumn5ValuesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Containing Header Row feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingHeaderRowPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Header Row feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_containingHeaderRow_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_containingHeaderRow_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__CONTAINING_HEADER_ROW, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_object_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_object_feature", "_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__OBJECT, true, false, true,
				null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column1 Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn1ValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column1 Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column1Values_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column1Values_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN1_VALUES, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column1 Referenced Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn1ReferencedObjectsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Column1 Referenced Objects feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column1ReferencedObjects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column1ReferencedObjects_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column2 Referenced Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn2ReferencedObjectsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Column2 Referenced Objects feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column2ReferencedObjects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column2ReferencedObjects_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column3 Referenced Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn3ReferencedObjectsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Column3 Referenced Objects feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column3ReferencedObjects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column3ReferencedObjects_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column4 Referenced Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn4ReferencedObjectsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Column4 Referenced Objects feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column4ReferencedObjects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column4ReferencedObjects_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column5 Referenced Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn5ReferencedObjectsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Column5 Referenced Objects feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column5ReferencedObjects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column5ReferencedObjects_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column1 Data Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn1DataValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column1 Data Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column1DataValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column1DataValues_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN1_DATA_VALUES, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Column1 Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn1LiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column1 Literal Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column1LiteralValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column1LiteralValue_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN1_LITERAL_VALUE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column2 Data Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn2DataValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column2 Data Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column2DataValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column2DataValues_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN2_DATA_VALUES, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Column2 Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn2LiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column2 Literal Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column2LiteralValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column2LiteralValue_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN2_LITERAL_VALUE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column3 Data Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn3DataValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column3 Data Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column3DataValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column3DataValues_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN3_DATA_VALUES, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Column3 Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn3LiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column3 Literal Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column3LiteralValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column3LiteralValue_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN3_LITERAL_VALUE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column4 Data Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn4DataValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column4 Data Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column4DataValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column4DataValues_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN4_DATA_VALUES, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Column4 Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn4LiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column4 Literal Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column4LiteralValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column4LiteralValue_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN4_LITERAL_VALUE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column5 Data Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn5DataValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column5 Data Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column5DataValues_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column5DataValues_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN5_DATA_VALUES, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Column5 Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn5LiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column5 Literal Value feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column5LiteralValue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column5LiteralValue_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN5_LITERAL_VALUE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column2 Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn2ValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column2 Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column2Values_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column2Values_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN2_VALUES, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column3 Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn3ValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column3 Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column3Values_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column3Values_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN3_VALUES, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column4 Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn4ValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column4 Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column4Values_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column4Values_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN4_VALUES, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column5 Values feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn5ValuesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column5 Values feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectRow_column5Values_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectRow_column5Values_feature",
						"_UI_MObjectRow_type"),
				TablesPackage.Literals.MOBJECT_ROW__COLUMN5_VALUES, false,
				false, false, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					TablesPackage.Literals.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MObjectRow.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MObjectRow"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return "<" + containingFeatureName + ">";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MObjectRow.class)) {
		case TablesPackage.MOBJECT_ROW__COLUMN1_DATA_VALUES:
		case TablesPackage.MOBJECT_ROW__COLUMN2_DATA_VALUES:
		case TablesPackage.MOBJECT_ROW__COLUMN3_DATA_VALUES:
		case TablesPackage.MOBJECT_ROW__COLUMN4_DATA_VALUES:
		case TablesPackage.MOBJECT_ROW__COLUMN5_DATA_VALUES:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case TablesPackage.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				TablesPackage.Literals.MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER,
				TablesFactory.eINSTANCE.createMHeader()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

}
