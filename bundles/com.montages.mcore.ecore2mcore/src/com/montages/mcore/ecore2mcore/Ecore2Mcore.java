package com.montages.mcore.ecore2mcore;

import static org.eclipse.emf.common.util.URI.createPlatformPluginURI;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.WriterLog;

import com.montages.mcore.MComponent;

public class Ecore2Mcore {

	private final static String path = "com.montages.mcore.ecore2mcore/transforms/ecore2mcore/Ecore2Mcore.qvto";
	private final static URI uri = createPlatformPluginURI(path, false);
	private final static TransformationExecutor executor = new TransformationExecutor(uri);

	public static void loadTransformation() {
		executor.loadTransformation();
	}

	public List<MComponent> transform(EObject... contents) throws CoreException {
		loadTransformation();

		final ExecutionContextImpl ctx = new ExecutionContextImpl();
		ctx.setLog(new WriterLog(new OutputStreamWriter(System.out)));

		BasicModelExtent outputs = new BasicModelExtent();
		final ExecutionDiagnostic diag = executor.execute(ctx, 
				new BasicModelExtent(Arrays.asList(contents)),
				outputs);

		if (Diagnostic.ERROR == diag.getSeverity()) {
			throw new CoreException(BasicDiagnostic.toIStatus(diag));
		}

		if (outputs.getContents().isEmpty()) {
			return Collections.<MComponent> emptyList();
		} else {
			List<MComponent> result = new ArrayList<MComponent>();
			for (EObject out: outputs.getContents()) {
				if (out instanceof MComponent) {
					result.add((MComponent) out);
				}
			}			

			return result;
		}
	}

	public MComponent transform(Resource resource) throws IllegalArgumentException {
		if (resource == null) {
			return null;
		}

		if (!resource.isLoaded()) {
			try {
				resource.load(null);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		if (resource.getContents().size() > 1) {
			throw new IllegalArgumentException("Resource should contain only one root object");
		}

		MComponent result = null;
		List<EObject> contents = resource.getContents();
		try {
			List<MComponent> resultList = transform(contents.toArray(new EObject[contents.size()]));
			if (resultList.size() > 0) {
				result = resultList.get(0);
			}
		} catch (CoreException e) {
			e.printStackTrace();
			return null;
		}

		return result;
	}

}
