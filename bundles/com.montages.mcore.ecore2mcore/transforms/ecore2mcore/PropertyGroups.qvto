import Constants;
import Annotations;

modeltype MCORE uses 'http://www.montages.com/mCore/MCore';
modeltype ECORE uses 'http://www.eclipse.org/emf/2002/Ecore';

library PropertyGroups;

--	
--	PropertyGroup
--	


helper EClass::propertyGroups(): Collection(MPropertiesGroup) {
	var propertyGroupFeatures := self.eStructuralFeatures->select(e |  
		e.eAnnotations->exists(source = EDITORCONFIG and details->exists(key='propertyCategory')));
		
	var propertyGroupOperations := self.oclAsType(EClass).eOperations->select(e |  
		e.eAnnotations->exists(source = EDITORCONFIG and details->exists(key='propertyCategory')));
		
		
	var propertyGroups:= propertyGroupFeatures->union(propertyGroupOperations);		
	
	var groups:= propertyGroups->groups();
	
	
	computedGroups := Set{}; // reset glovbal variables
	var done := createPropertyGroups( groups );
	
	return done;
};

property computedGroups : Set(MCORE::MPropertiesGroup) = Set{};

helper createPropertyGroups(groups: Set(String)): Set(MCORE::MPropertiesGroup) {
	var propertyGroups : Set(MCORE::MPropertiesGroup) := Set{};
	groups->forEach( group ) {
		var index := group.indexOf('/');
		if ( index > 0 ) then {
			var propertyGroup := group.createGroupHierarchy();
			propertyGroups := propertyGroups->including(propertyGroup);
		} else {
			var propertyGroup := computedGroups->any(e | e.name = group);
			if ( propertyGroup.oclIsUndefined() ) then {
				propertyGroup := object MPropertiesGroup { name := group };
				computedGroups := computedGroups->including( propertyGroup );
				propertyGroups := propertyGroups->including(propertyGroup);
			} endif;
		} endif;
	};
	return propertyGroups;	
}

helper String::createGroupHierarchy(): MPropertiesGroup {
	var index := self.indexOf('/');
	var group : MPropertiesGroup;
	if index > 0 then {
		group := self.createGroup();
		var next := self.substring(index + 1, self.size());
		if not group.propertyGroup->exists(name = next) then {
			group.propertyGroup += next.createGroupHierarchy();
		} endif;
	} else {
		group := computedGroups->any(name = self);
		if group.oclIsUndefined() then {
			group := object MPropertiesGroup { name := self};
			computedGroups := computedGroups->including(group);
		} endif;
	} 
	endif;
	
	return group;
}

helper String::createGroup(): MPropertiesGroup {
	var index := self.indexOf('/');
	var root := self.substring(1, index-1);
	var first := computedGroups->any(name = root);
	if first.oclIsUndefined() then {
		first := object MPropertiesGroup { name := root};
		computedGroups := computedGroups->including(first);
	} endif;
	return first;
}

helper MPropertiesGroup::hiearchicalShortName(): String =
	if not self.oclAsType(EObject).eContainer().oclIsKindOf(MPropertiesGroup) then self.calculatedShortName else
		self.oclAsType(EObject).eContainer().oclAsType(MPropertiesGroup).hiearchicalShortName().concat("/").concat(self.calculatedShortName)
	endif;


helper Collection(ETypedElement)::groups(): Set(String) =
	self.eAnnotations->select(source = EDITORCONFIG)
		->select(details->exists(key='propertyCategory'))
			->collect(details.value)->reject(e | e = 'true' or e = 'false')->asSet();

helper ETypedElement::hasPropertyGroup(id: String): Boolean =
	self.eAnnotations->exists(source = EDITORCONFIG and details->exists(key='propertyCategory' and value=id));

helper ECORE::ETypedElement::inPropertyGroup(): Boolean =
	self.eAnnotations->exists(source = EDITORCONFIG and details->exists(key='propertyCategory'));
