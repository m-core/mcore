package org.xocl.ecore2editorconfig;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.Variable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;

public class DefaultGenerator extends Generator {

	public DefaultGenerator(ResourceSet resourceSet) {
		super(resourceSet);
	}

	public EditorConfig generate(EPackage ePackage) {
		final URI editorConfigURI = getEditorConfigURI(ePackage);
		final Map<EClass, TableConfig> mapOfTables = new HashMap<EClass, TableConfig>();

		EditorConfig editorConfig = factory.createEditorConfig();

		Resource editorConfigResource = resourceSet.createResource(editorConfigURI);
		editorConfigResource.getContents().add(editorConfig);

		doGenerate(ePackage, editorConfig, mapOfTables);
		resolveContainments(ePackage, mapOfTables);

		return editorConfig;
	}

	private EditorConfig doGenerate(EPackage ePackage, EditorConfig editorConfig, Map<EClass, TableConfig> mapOfTables) {
		IsTableHeaderQuery isTableHeader = createIsTableHEaderQuery(ePackage);
		if (isTableHeader == null) {
			isTableHeader = USE_FIRST;
		}
		for (EClassifier classifier: ePackage.getEClassifiers()) {
			if (classifier instanceof EClass) {
				TableConfig table = createTable((EClass) classifier);
				mapOfTables.put((EClass) classifier, table);

				editorConfig.getTableConfig().add(table);
				if (editorConfig.getHeaderTableConfig() == null && isTableHeader.isTableHeader((EClass)classifier)) {
					editorConfig.setHeaderTableConfig(table);
				}
			}
		}

		for (EPackage subPackage: ePackage.getESubpackages()) {
			doGenerate(subPackage, editorConfig, mapOfTables);
		}

		return editorConfig;
	}

	private void resolveContainments(EPackage ePackage, Map<EClass, TableConfig> mapOfTables) {
		for (EClassifier classifier: ePackage.getEClassifiers()) {
			if (classifier instanceof EClass) {
				EClass eClass = (EClass) classifier;
				TableConfig config = mapOfTables.get(eClass);

				Collection<EStructuralFeature> features = getStructuralFeatures(eClass);

				for (EStructuralFeature feature: features) {
					if (feature instanceof EReference && ((EReference) feature).isContainment()) {
						if (shouldCreateColumn(feature)) {
							EReference reference = (EReference) feature;
							EClass type = (EClass) reference.getEType();
							TableConfig refTable = findTableConfig(resourceSet, type, mapOfTables);

							if (refTable != null) {
								// We use the dynamic API here to create and add the entry to the Map TableConfig#referenceToTableConfigMap 								
								// because the entry of type EReferenceToTableConfigMapEntry defines a property label that must be set and 
								// that is not accessible otherwise.
								//
								EObject entry = EcoreUtil.create(EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY);
								entry.eSet(EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY, reference);
								entry.eSet(EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE, refTable);
								entry.eSet(EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL, XoclHelper.camelCaseToBusiness(reference.getName()));

								@SuppressWarnings("unchecked")
								Collection<EObject> entries = (Collection<EObject>) config.eGet(EditorConfigPackage.Literals.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP);
								entries.add(entry);
							}
						}
					}
				}
			}
		}

		for (EPackage subPackage: ePackage.getESubpackages()) {
			resolveContainments(subPackage, mapOfTables);
		}
	}
	
	protected IsTableHeaderQuery createIsTableHEaderQuery(EPackage ePackage) {
		String ocl = findAnnotationDetails(ePackage, XoclEmfUtil.OCL_ANNOTATION_SOURCE, "rootConstraint");
		if (ocl != null) {
			try {
				return new RootConstraintIsTableHeaderQuery(ocl);
			} catch (ParserException e) {
				throw new RuntimeException("OCL exception evaluating rootConstraint: " + ocl, e);
			}
		}
		return USE_FIRST;
	}

	protected String findAnnotationDetails(EModelElement element, String source, String key) {
		String result = null;
		for (EAnnotation next : element.getEAnnotations()) {
			String nextSource = next.getSource();
			if (XoclEmfUtil.OCL_ANNOTATION_SOURCE.equals(nextSource)) {
				result = next.getDetails().get("rootConstraint");
			}
			if (result != null) {
				break;
			}
		}
		return result;
	}
	
	protected static interface IsTableHeaderQuery {
		public boolean isTableHeader(EClass eClass);
	}
	
	protected static IsTableHeaderQuery USE_FIRST = new IsTableHeaderQuery() {
		
		@Override
		public boolean isTableHeader(EClass eClass) {
			return true;
		}
	};
	
	protected static class RootConstraintIsTableHeaderQuery implements IsTableHeaderQuery {
		
		private static final String VAR_TRG = "trg";
		
		private final OCL OCL_ENV;
		
		private final Query myOCLQuery;
		
		public RootConstraintIsTableHeaderQuery(String ocl) throws ParserException {
			OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());
			
			Variable trgVar = org.eclipse.ocl.ecore.EcoreFactory.eINSTANCE.createVariable();
			trgVar.setName(VAR_TRG);
			trgVar.setType(EcorePackage.eINSTANCE.getEClass());
			OCL_ENV.getEnvironment().addElement(trgVar.getName(), trgVar, false);
			
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(EcorePackage.eINSTANCE.getEClass());
			myOCLQuery = OCL_ENV.createQuery(helper.createQuery(ocl));
		}
		
		@Override
		public boolean isTableHeader(EClass eClass) {
			myOCLQuery.getEvaluationEnvironment().replace(VAR_TRG, eClass);
			Object result = myOCLQuery.evaluate();
			return result instanceof Boolean && ((Boolean)result).booleanValue();
		}
		
	}
}