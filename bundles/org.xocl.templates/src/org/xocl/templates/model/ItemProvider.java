package org.xocl.templates.model;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.common.util.*;

public class ItemProvider
{
  protected static String nl;
  public static synchronized ItemProvider create(String lineSeparator)
  {
    nl = lineSeparator;
    ItemProvider result = new ItemProvider();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";" + NL + NL;
  protected final String TEXT_3 = NL + NL + "/**" + NL + " * This is the item provider adapter for a {@link ";
  protected final String TEXT_4 = "} object." + NL + " * <!-- begin-user-doc -->" + NL + " * <!-- end-user-doc -->" + NL + " * @generated" + NL + " */" + NL + "public class ";
  protected final String TEXT_5 = NL + "\textends ";
  protected final String TEXT_6 = NL + "\timplements";
  protected final String TEXT_7 = NL + "\t\t";
  protected final String TEXT_8 = ",";
  protected final String TEXT_9 = NL + "{";
  protected final String TEXT_10 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic static final ";
  protected final String TEXT_11 = " copyright = ";
  protected final String TEXT_12 = ";";
  protected final String TEXT_13 = NL;
  protected final String TEXT_14 = NL + "\t/**" + NL + "\t * This constructs an instance from a factory and a notifier." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_15 = "(AdapterFactory adapterFactory)" + NL + "\t{" + NL + "\t\tsuper(adapterFactory);" + NL + "\t}" + NL;
  protected final String TEXT_16 = NL;
  protected final String TEXT_17 = NL + " \t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * Override SetCommand for XUpdate execution" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\t\t@Override" + NL + "\tprotected org.eclipse.emf.common.command.Command createSetCommand(" + NL + "\t\t\torg.eclipse.emf.edit.domain.EditingDomain domain," + NL + "\t\t\torg.eclipse.emf.ecore.EObject owner, org.eclipse.emf.ecore.EStructuralFeature feature," + NL + "\t\t\tObject value, int index) {" + NL;
  protected final String TEXT_18 = NL + "\t\t\torg.xocl.semantics.XUpdate myUpdate = null;" + NL + "     if (owner instanceof ";
  protected final String TEXT_19 = ")" + NL + "     {" + NL + "       \t\t";
  protected final String TEXT_20 = " typedOwner = ((";
  protected final String TEXT_21 = ")owner);";
  protected final String TEXT_22 = NL;
  protected final String TEXT_23 = "else";
  protected final String TEXT_24 = " " + NL + "       if (feature == ";
  protected final String TEXT_25 = "())" + NL + "\t\tmyUpdate = typedOwner.";
  protected final String TEXT_26 = "((";
  protected final String TEXT_27 = ") value);    " + NL + "      " + NL + "      " + NL;
  protected final String TEXT_28 = NL + NL + "   }" + NL;
  protected final String TEXT_29 = NL + NL + "\t\treturn myUpdate == null ? super.createSetCommand(domain, owner, feature, value, index) : myUpdate.getContainingTransition().interpreteTransitionAsCommand();" + NL + "" + NL + "\t};" + NL + "\t" + NL + "\t";
  protected final String TEXT_30 = NL + NL + "\t" + NL + "\t" + NL + "\t/**" + NL + "\t * This returns the property descriptors for the adapted class." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_31 = NL + "\t@Override";
  protected final String TEXT_32 = NL + "\tpublic ";
  protected final String TEXT_33 = " getPropertyDescriptors(Object object)" + NL + "\t{" + NL + "\t\tif (itemPropertyDescriptors == null)" + NL + "\t\t{" + NL + "\t\t\tsuper.getPropertyDescriptors(object);" + NL;
  protected final String TEXT_34 = "if (shouldShowAdvancedProperties()) {";
  protected final String TEXT_35 = NL + "\t\t\tadd";
  protected final String TEXT_36 = "PropertyDescriptor(object);";
  protected final String TEXT_37 = "}";
  protected final String TEXT_38 = NL + "\t\t}" + NL + "\t\treturn itemPropertyDescriptors;" + NL + "\t}" + NL;
  protected final String TEXT_39 = NL + "\t/**" + NL + "\t * This adds a property descriptor for the ";
  protected final String TEXT_40 = " feature." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected void add";
  protected final String TEXT_41 = "PropertyDescriptor(Object object)" + NL + "\t{";
  protected final String TEXT_42 = NL + "\t/*" + NL + "\t * This adds a property descriptor for the ";
  protected final String TEXT_43 = " feature.";
  protected final String TEXT_44 = NL + "\t * The list of possible choices is constructed by OCL ";
  protected final String TEXT_45 = NL + "\t * The list of possible choices is constraint by OCL ";
  protected final String TEXT_46 = NL + "\t */" + NL + "\t\titemPropertyDescriptors.add(";
  protected final String TEXT_47 = NL + "  \t\t\tnew ";
  protected final String TEXT_48 = NL + "\t\t\tcreateItemPropertyDescriptor";
  protected final String TEXT_49 = NL + "\t\t\t\t(((";
  protected final String TEXT_50 = ")adapterFactory).getRootAdapterFactory()," + NL + "\t\t\t\t getResourceLocator()," + NL + "\t\t\t\t getString(\"_UI_";
  protected final String TEXT_51 = "_";
  protected final String TEXT_52 = "_feature\"),";
  protected final String TEXT_53 = NL + "\t\t\t\t getString(\"_UI_PropertyDescriptor_description\", \"_UI_";
  protected final String TEXT_54 = "_";
  protected final String TEXT_55 = "_feature\", \"_UI_";
  protected final String TEXT_56 = "_type\"),";
  protected final String TEXT_57 = NL + "\t\t\t\t getString(\"_UI_";
  protected final String TEXT_58 = "_";
  protected final String TEXT_59 = "_description\"),";
  protected final String TEXT_60 = NL + "\t\t\t\t ";
  protected final String TEXT_61 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_62 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_63 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_64 = ",";
  protected final String TEXT_65 = NL + "\t\t\t\t null,";
  protected final String TEXT_66 = NL + "\t\t\t\t ";
  protected final String TEXT_67 = ".";
  protected final String TEXT_68 = ",";
  protected final String TEXT_69 = NL + "\t\t\t\t null,";
  protected final String TEXT_70 = NL + "\t\t\t\t getString(\"";
  protected final String TEXT_71 = "\"),";
  protected final String TEXT_72 = NL + "\t\t\t\t null)";
  protected final String TEXT_73 = NL + "\t\t\t\t new String[] {";
  protected final String TEXT_74 = NL + "\t\t\t\t\t\"";
  protected final String TEXT_75 = "\"";
  protected final String TEXT_76 = ",";
  protected final String TEXT_77 = NL + "\t\t\t\t })";
  protected final String TEXT_78 = NL + "  \t\t{ " + NL + "\t\t\t\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_79 = NL + "\t\t\t\t@Override";
  protected final String TEXT_80 = NL + "\t\t\t\tpublic ";
  protected final String TEXT_81 = "<?> getChoiceOfValues(Object object) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_82 = "<";
  protected final String TEXT_83 = "> result = new ";
  protected final String TEXT_84 = "<";
  protected final String TEXT_85 = ">();" + NL + "\t\t\t\t\t";
  protected final String TEXT_86 = "<? extends ";
  protected final String TEXT_87 = "> superResult = (";
  protected final String TEXT_88 = "<? extends ";
  protected final String TEXT_89 = ">) super.getChoiceOfValues(object);" + NL + "\t\t\t\t\tif (superResult != null) {" + NL + "\t\t\t\t\t\tresult.addAll(superResult);" + NL + "\t\t\t\t\t}";
  protected final String TEXT_90 = NL + "\t\t\t\t\tresult = ((";
  protected final String TEXT_91 = ")object)" + NL + "\t\t\t\t\t\t\t.eval";
  protected final String TEXT_92 = "ChoiceConstruction(result);" + NL + "\t\t\t";
  protected final String TEXT_93 = NL + "\t\t\t\t\tif (!result.contains(null)) {" + NL + "\t\t\t\t\t\tresult.add(null);" + NL + "\t\t\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_94 = NL + "\t\t\t\t\tresult.remove(null);" + NL + "\t\t\t";
  protected final String TEXT_95 = NL + "\t\t\t\t\t";
  protected final String TEXT_96 = "<";
  protected final String TEXT_97 = "> eObjects = (";
  protected final String TEXT_98 = "<";
  protected final String TEXT_99 = ">)(";
  protected final String TEXT_100 = "<?>)new ";
  protected final String TEXT_101 = "<Object>(result);" + NL + "\t\t\t\t\t";
  protected final String TEXT_102 = " resource = ((";
  protected final String TEXT_103 = ")object).eResource();" + NL + "\t\t\t\t\tif (resource != null) {" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_104 = " resourceSet = resource.getResourceSet();" + NL + "\t\t\t\t\t\tif (resourceSet != null) {" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_105 = "<";
  protected final String TEXT_106 = "> visited = new ";
  protected final String TEXT_107 = "<";
  protected final String TEXT_108 = ">(eObjects);" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_109 = " packageRegistry = resourceSet.getPackageRegistry();" + NL + "\t\t\t\t\t\t\tfor (";
  protected final String TEXT_110 = "<String> i = packageRegistry.keySet().iterator(); i.hasNext(); ) {" + NL + "\t\t\t\t\t\t\t\tcollectReachableObjectsOfType(visited, eObjects, packageRegistry.getEPackage(i.next()), ";
  protected final String TEXT_111 = ".getEType());" + NL + "\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\tresult = (";
  protected final String TEXT_112 = "<";
  protected final String TEXT_113 = ">) (";
  protected final String TEXT_114 = "<?>) eObjects;" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}";
  protected final String TEXT_115 = NL + "\t\t\t\t\tfor (";
  protected final String TEXT_116 = "<";
  protected final String TEXT_117 = "> iterator = result.iterator(); iterator" + NL + "\t\t\t\t\t\t\t.hasNext();) {" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_118 = " trg = iterator.next();" + NL + "\t\t\t\t\t\tif (trg == null) {" + NL + "\t\t\t";
  protected final String TEXT_119 = NL + "\t\t\t\t\t\t\titerator.remove();" + NL + "\t\t\t";
  protected final String TEXT_120 = "\t\t\tcontinue;\t\t" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\tif (!((";
  protected final String TEXT_121 = ") object)" + NL + "\t\t\t\t\t\t\t\t.eval";
  protected final String TEXT_122 = "Typed";
  protected final String TEXT_123 = "ChoiceConstraint(trg)) {" + NL + "\t\t\t\t\t\t\titerator.remove();" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}";
  protected final String TEXT_124 = NL + "\t\t\t\t\treturn result;" + NL + "\t\t\t\t}" + NL + "\t\t\t}";
  protected final String TEXT_125 = NL + "  \t\t{ ";
  protected final String TEXT_126 = NL + "\t\t\t\t@Override";
  protected final String TEXT_127 = NL + "\t\t\t\tpublic ";
  protected final String TEXT_128 = " getOCLExpressionContext(Object object) {" + NL + "\t\t\t\t    return ((";
  protected final String TEXT_129 = ")object)" + NL + "\t\t\t\t\t\t\t.get";
  protected final String TEXT_130 = "OCLExpressionContext();" + NL + "\t\t\t\t}" + NL + "\t\t\t}";
  protected final String TEXT_131 = NL + "  \t\t);";
  protected final String TEXT_132 = NL + "\t}" + NL;
  protected final String TEXT_133 = NL + "\t/**" + NL + "\t * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an" + NL + "\t * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or" + NL + "\t * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_134 = NL + "\t@Override";
  protected final String TEXT_135 = NL + "\tpublic ";
  protected final String TEXT_136 = " getChildrenFeatures(Object object)" + NL + "\t{" + NL + "\t\tif (childrenFeatures == null)" + NL + "\t\t{" + NL + "\t\t\tsuper.getChildrenFeatures(object);";
  protected final String TEXT_137 = NL + "\t\t\tchildrenFeatures.add(";
  protected final String TEXT_138 = ");";
  protected final String TEXT_139 = NL + "\t\t}" + NL + "\t\treturn childrenFeatures;" + NL + "\t}" + NL;
  protected final String TEXT_140 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_141 = NL + "\t@Override";
  protected final String TEXT_142 = NL + "\tprotected ";
  protected final String TEXT_143 = " getChildFeature(Object object, Object child)" + NL + "\t{" + NL + "\t\t// Check the type of the specified child object and return the proper feature to use for" + NL + "\t\t// adding (see {@link AddCommand}) it as a child." + NL + "" + NL + "\t\treturn super.getChildFeature(object, child);" + NL + "\t}" + NL;
  protected final String TEXT_144 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_145 = NL + "\t@Override";
  protected final String TEXT_146 = NL + "\tpublic boolean hasChildren(Object object)" + NL + "\t{" + NL + "\t\treturn hasChildren(object, ";
  protected final String TEXT_147 = ");" + NL + "\t}" + NL;
  protected final String TEXT_148 = NL + "\t/**" + NL + "\t * This returns ";
  protected final String TEXT_149 = ".gif." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_150 = NL + "\t@Override";
  protected final String TEXT_151 = NL + "\tpublic Object getImage(Object object)" + NL + "\t{" + NL + "\t\treturn overlayImage(object, getResourceLocator().getImage(\"full/obj16/";
  protected final String TEXT_152 = "\"));";
  protected final String TEXT_153 = NL + "\t}" + NL;
  protected final String TEXT_154 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_155 = NL + "\t@Override";
  protected final String TEXT_156 = NL + "\tprotected boolean shouldComposeCreationImage() " + NL + "\t{" + NL + "\t\treturn true;" + NL + "\t}" + NL;
  protected final String TEXT_157 = NL + "\t/**" + NL + "\t * This returns the label text for the adapted class." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_158 = NL + "\t@Override";
  protected final String TEXT_159 = NL + "\tpublic String getText(Object object)" + NL + "\t{";
  protected final String TEXT_160 = NL + "\t\treturn ((";
  protected final String TEXT_161 = ")object).evalOclLabel();";
  protected final String TEXT_162 = NL + "//Montages Change to show containingFeatureName";
  protected final String TEXT_163 = NL;
  protected final String TEXT_164 = " containingFeature = ((";
  protected final String TEXT_165 = ")object).eContainingFeature();" + NL + "String containingFeatureName = (containingFeature == null?\"\":containingFeature.getName());" + NL;
  protected final String TEXT_166 = NL + "\t\t";
  protected final String TEXT_167 = "<?, ?>";
  protected final String TEXT_168 = " ";
  protected final String TEXT_169 = " = (";
  protected final String TEXT_170 = "<?, ?>";
  protected final String TEXT_171 = ")object;";
  protected final String TEXT_172 = NL + "\t\treturn \"\" + ";
  protected final String TEXT_173 = ".getKey() + \" -> \" + ";
  protected final String TEXT_174 = ".getValue();";
  protected final String TEXT_175 = NL + "\t\tString key = crop(\"\" + ";
  protected final String TEXT_176 = ".getKey());";
  protected final String TEXT_177 = NL + "\t\tString key = \"\" + ";
  protected final String TEXT_178 = ".getKey();";
  protected final String TEXT_179 = NL + "\t\tString value = crop(\"\" + ";
  protected final String TEXT_180 = ".getValue());";
  protected final String TEXT_181 = NL + "\t\tString value = \"\" + ";
  protected final String TEXT_182 = ".getValue();";
  protected final String TEXT_183 = NL + "\t\treturn key + \" -> \" + value;";
  protected final String TEXT_184 = NL + "\t\t";
  protected final String TEXT_185 = " ";
  protected final String TEXT_186 = " = (";
  protected final String TEXT_187 = ")object;" + NL + "\t\t//Montages change from Organizational Unit Marketing to <organizational unit feature> Marketing" + NL + "\t\treturn \"<\"+containingFeatureName+ \">\" + \" \" + ";
  protected final String TEXT_188 = ".";
  protected final String TEXT_189 = "();";
  protected final String TEXT_190 = NL + "\t\tString label = crop(((";
  protected final String TEXT_191 = ")object).";
  protected final String TEXT_192 = "());";
  protected final String TEXT_193 = NL + "\t\tString label = ((";
  protected final String TEXT_194 = ")object).";
  protected final String TEXT_195 = "();";
  protected final String TEXT_196 = NL + "\t\t";
  protected final String TEXT_197 = " labelValue = ((";
  protected final String TEXT_198 = ")object).eGet(";
  protected final String TEXT_199 = ");";
  protected final String TEXT_200 = NL + "\t\t";
  protected final String TEXT_201 = " labelValue = ((";
  protected final String TEXT_202 = ")object).";
  protected final String TEXT_203 = "();";
  protected final String TEXT_204 = NL + "\t\tString label = labelValue == null ? null : labelValue.toString();";
  protected final String TEXT_205 = NL + "\t\t//Montages change from Organizational Unit Marketing to <organizational unit> Marketing" + NL + "\t\treturn label == null || label.length() == 0 ?" + NL + "\t\t\t\"<\"+containingFeatureName+ \">\" :";
  protected final String TEXT_206 = NL + "\t\t\t\"<\"+containingFeatureName+ \">\" + \" \" + label;";
  protected final String TEXT_207 = NL + "\t\t//Montages change from Organizational Unit Marketing to <organizational unit> Marketing" + NL + "\t\treturn \"<\"+containingFeatureName+ \">\";";
  protected final String TEXT_208 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * This handles model notifications by calling {@link #updateChildren} to update any cached" + NL + "\t * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_209 = NL + "\t@Override";
  protected final String TEXT_210 = NL + "\tpublic void notifyChanged(Notification notification)" + NL + "\t{" + NL + "\t\tupdateChildren(notification);";
  protected final String TEXT_211 = NL + NL + "\t\tswitch (notification.getFeatureID(";
  protected final String TEXT_212 = ".class))" + NL + "\t\t{";
  protected final String TEXT_213 = NL + "\t\t\tcase ";
  protected final String TEXT_214 = ":";
  protected final String TEXT_215 = NL + "\t\t\t\tfireNotifyChanged(new ";
  protected final String TEXT_216 = "(notification, notification.getNotifier(), false, true));" + NL + "\t\t\t\treturn;";
  protected final String TEXT_217 = NL + "\t\t\tcase ";
  protected final String TEXT_218 = ":";
  protected final String TEXT_219 = NL + "\t\t\t\tfireNotifyChanged(new ";
  protected final String TEXT_220 = "(notification, notification.getNotifier(), true, false));" + NL + "\t\t\t\treturn;";
  protected final String TEXT_221 = NL + "\t\t\tcase ";
  protected final String TEXT_222 = ":";
  protected final String TEXT_223 = NL + "\t\t\t\tfireNotifyChanged(new ";
  protected final String TEXT_224 = "(notification, notification.getNotifier(), true, true));" + NL + "\t\t\t\treturn;";
  protected final String TEXT_225 = NL + "\t\t}";
  protected final String TEXT_226 = NL + "\t\tsuper.notifyChanged(notification);" + NL + "\t}" + NL;
  protected final String TEXT_227 = NL + "\t/**" + NL + "\t * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children" + NL + "\t * that can be created under this object." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_228 = NL + "\t@Override";
  protected final String TEXT_229 = NL + "\tprotected void collectNewChildDescriptors(";
  protected final String TEXT_230 = " newChildDescriptors, Object object)" + NL + "\t{" + NL + "\t\tsuper.collectNewChildDescriptors(newChildDescriptors, object);";
  protected final String TEXT_231 = NL + NL + "\t\tnewChildDescriptors.add" + NL + "\t\t\t(createChildParameter" + NL + "\t\t\t\t(";
  protected final String TEXT_232 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_233 = ".createEntry" + NL + "\t\t\t\t\t(";
  protected final String TEXT_234 = ",";
  protected final String TEXT_235 = NL + "\t\t\t\t\t ";
  protected final String TEXT_236 = ".create(";
  protected final String TEXT_237 = "))));";
  protected final String TEXT_238 = NL + "\t\t\t\t\t ";
  protected final String TEXT_239 = ".create";
  protected final String TEXT_240 = "())));";
  protected final String TEXT_241 = NL + NL + "\t\tnewChildDescriptors.add" + NL + "\t\t\t(createChildParameter" + NL + "\t\t\t\t(";
  protected final String TEXT_242 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_243 = ".createEntry" + NL + "\t\t\t\t\t(";
  protected final String TEXT_244 = ",";
  protected final String TEXT_245 = NL + "\t\t\t\t\t ";
  protected final String TEXT_246 = ")));";
  protected final String TEXT_247 = NL + "\t\t\t\t\t ";
  protected final String TEXT_248 = ")));";
  protected final String TEXT_249 = NL + "\t\t\t\t\t ";
  protected final String TEXT_250 = ".createFromString(";
  protected final String TEXT_251 = ", ";
  protected final String TEXT_252 = "))));";
  protected final String TEXT_253 = " // TODO: ensure this is a valid literal value";
  protected final String TEXT_254 = NL + NL + "\t\tnewChildDescriptors.add" + NL + "\t\t\t(createChildParameter" + NL + "\t\t\t\t(";
  protected final String TEXT_255 = ",";
  protected final String TEXT_256 = NL + "\t\t\t\t ";
  protected final String TEXT_257 = ".create(";
  protected final String TEXT_258 = ")));";
  protected final String TEXT_259 = NL + "\t\t\t\t ";
  protected final String TEXT_260 = ".create";
  protected final String TEXT_261 = "()));";
  protected final String TEXT_262 = NL + NL + "\t\tnewChildDescriptors.add" + NL + "\t\t\t(createChildParameter" + NL + "\t\t\t\t(";
  protected final String TEXT_263 = ",";
  protected final String TEXT_264 = NL + "\t\t\t\t ";
  protected final String TEXT_265 = "));";
  protected final String TEXT_266 = NL + "\t\t\t\t ";
  protected final String TEXT_267 = "));";
  protected final String TEXT_268 = NL + "\t\t\t\t ";
  protected final String TEXT_269 = ".createFromString(";
  protected final String TEXT_270 = ", ";
  protected final String TEXT_271 = ")));";
  protected final String TEXT_272 = " // TODO: ensure this is a valid literal value";
  protected final String TEXT_273 = NL + "\t}" + NL;
  protected final String TEXT_274 = NL + "\t/**" + NL + "\t * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_275 = NL + "\t@Override";
  protected final String TEXT_276 = NL + "\tpublic String getCreateChildText(Object owner, Object feature, Object child, ";
  protected final String TEXT_277 = " selection)" + NL + "\t{" + NL + "\t\tObject childFeature = feature;" + NL + "\t\tObject childObject = child;" + NL;
  protected final String TEXT_278 = NL + "\t\tif (childFeature instanceof ";
  protected final String TEXT_279 = " && ";
  protected final String TEXT_280 = ".isFeatureMap((EStructuralFeature)childFeature))" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_281 = ".Entry entry = (FeatureMap.Entry)childObject;" + NL + "\t\t\tchildFeature = entry.getEStructuralFeature();" + NL + "\t\t\tchildObject = entry.getValue();" + NL + "\t\t}" + NL;
  protected final String TEXT_282 = NL + "\t\tboolean qualify =";
  protected final String TEXT_283 = NL + "\t\t\tchildFeature == ";
  protected final String TEXT_284 = NL + NL + "\t\tif (qualify)" + NL + "\t\t{" + NL + "\t\t\treturn getString" + NL + "\t\t\t\t(\"_UI_CreateChild_text2\",";
  protected final String TEXT_285 = NL + "\t\t\t\t new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });" + NL + "\t\t}" + NL + "\t\treturn super.getCreateChildText(owner, feature, child, selection);" + NL + "\t}" + NL;
  protected final String TEXT_286 = NL + "\t/**" + NL + "\t * Return the resource locator for this item provider's resources." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_287 = NL + "\t@Override";
  protected final String TEXT_288 = NL + "\tpublic ";
  protected final String TEXT_289 = " getResourceLocator()" + NL + "\t{";
  protected final String TEXT_290 = NL + "\t\treturn ((";
  protected final String TEXT_291 = ")adapterFactory).getResourceLocator();";
  protected final String TEXT_292 = NL + "\t\treturn ";
  protected final String TEXT_293 = ".INSTANCE;";
  protected final String TEXT_294 = NL + "\t}" + NL;
  protected final String TEXT_295 = NL;
  protected final String TEXT_296 = NL + "\t/**" + NL + "\t * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#isWrappingNeeded" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@Override" + NL + "\tprotected boolean isWrappingNeeded(Object object) {" + NL + "\t\t";
  protected final String TEXT_297 = "if (object instanceof ";
  protected final String TEXT_298 = ") {" + NL + "\t\t\treturn true;" + NL + "\t\t}";
  protected final String TEXT_299 = NL + "\t\treturn super.isWrappingNeeded(object);" + NL + "\t}";
  protected final String TEXT_300 = NL;
  protected final String TEXT_301 = NL + "\t/**" + NL + "\t * This adds a property descriptor for the ";
  protected final String TEXT_302 = " feature.";
  protected final String TEXT_303 = NL + "\t * The list of possible choices is constructed by OCL ";
  protected final String TEXT_304 = NL + "\t * The list of possible choices is constraint by OCL ";
  protected final String TEXT_305 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @templateTag IPINS01" + NL + "\t */";
  protected final String TEXT_306 = NL + "\t@Override";
  protected final String TEXT_307 = NL + "\tprotected void add";
  protected final String TEXT_308 = "PropertyDescriptor(Object object)" + NL + "\t{" + NL + "\t\titemPropertyDescriptors.add(";
  protected final String TEXT_309 = NL + "  \t\t\tnew ";
  protected final String TEXT_310 = NL + "\t\t\tcreateItemPropertyDescriptor";
  protected final String TEXT_311 = NL + "\t\t\t\t(((";
  protected final String TEXT_312 = ")adapterFactory).getRootAdapterFactory()," + NL + "\t\t\t\t getResourceLocator()," + NL + "\t\t\t\t getString(\"_UI_";
  protected final String TEXT_313 = "_";
  protected final String TEXT_314 = "_feature\"),";
  protected final String TEXT_315 = NL + "\t\t\t\t getString(\"_UI_PropertyDescriptor_description\", \"_UI_";
  protected final String TEXT_316 = "_";
  protected final String TEXT_317 = "_feature\", \"_UI_";
  protected final String TEXT_318 = "_type\"),";
  protected final String TEXT_319 = NL + "\t\t\t\t getString(\"_UI_";
  protected final String TEXT_320 = "_";
  protected final String TEXT_321 = "_description\"),";
  protected final String TEXT_322 = NL + "\t\t\t\t ";
  protected final String TEXT_323 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_324 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_325 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_326 = ",";
  protected final String TEXT_327 = NL + "\t\t\t\t null,";
  protected final String TEXT_328 = NL + "\t\t\t\t ";
  protected final String TEXT_329 = ".";
  protected final String TEXT_330 = ",";
  protected final String TEXT_331 = NL + "\t\t\t\t null,";
  protected final String TEXT_332 = NL + "\t\t\t\t getString(\"";
  protected final String TEXT_333 = "\"),";
  protected final String TEXT_334 = NL + "\t\t\t\t null)";
  protected final String TEXT_335 = NL + "\t\t\t\t new String[] {";
  protected final String TEXT_336 = NL + "\t\t\t\t\t\"";
  protected final String TEXT_337 = "\"";
  protected final String TEXT_338 = ",";
  protected final String TEXT_339 = NL + "\t\t\t\t })";
  protected final String TEXT_340 = NL + "  \t\t{ " + NL + "\t\t\t\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_341 = NL + "\t\t\t\t@Override";
  protected final String TEXT_342 = NL + "\t\t\t\tpublic Collection<?> getChoiceOfValues(Object object) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_343 = "<";
  protected final String TEXT_344 = "> choice = new ";
  protected final String TEXT_345 = "<";
  protected final String TEXT_346 = ">(" + NL + "\t\t\t\t\t(Collection<? extends ";
  protected final String TEXT_347 = ">) super.getChoiceOfValues(object));";
  protected final String TEXT_348 = NL + "\t\t\t\t\tchoice = ((";
  protected final String TEXT_349 = ")object)" + NL + "\t\t\t\t\t\t\t.eval";
  protected final String TEXT_350 = "ChoiceConstruction(choice);";
  protected final String TEXT_351 = NL + "\t\t\t\t\tfor (";
  protected final String TEXT_352 = "<";
  protected final String TEXT_353 = "> iterator = choice.iterator(); iterator" + NL + "\t\t\t\t\t\t\t.hasNext();) {" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_354 = " trg = iterator.next();" + NL + "\t\t\t\t\t\tif (!((";
  protected final String TEXT_355 = ") object)" + NL + "\t\t\t\t\t\t\t\t.eval";
  protected final String TEXT_356 = "Typed";
  protected final String TEXT_357 = "ChoiceConstraint(trg)) {" + NL + "\t\t\t\t\t\t\titerator.remove();" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}";
  protected final String TEXT_358 = NL + "\t\t\t\t\treturn choice;" + NL + "\t\t\t\t}" + NL + "\t\t\t}";
  protected final String TEXT_359 = NL + "  \t\t);" + NL + "\t}";
  protected final String TEXT_360 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic boolean shouldShowAdvancedProperties() {" + NL + "\t\treturn !";
  protected final String TEXT_361 = ".HIDE_ADVANCED_PROPERTIES;" + NL + "\t}";
  protected final String TEXT_362 = NL + "}";
  protected final String TEXT_363 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 *
 * Copyright (c) 2002-2007 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 */

    GenClass genClass = (GenClass)argument; GenPackage genPackage = genClass.getGenPackage(); GenModel genModel=genPackage.getGenModel();
     final String oclNsURI = "http://www.xocl.org/OCL"; 
     final String overrideOclNsURI = "http://www.xocl.org/OVERRIDE_OCL"; 
     final String expressionOclNsURI = "http://www.xocl.org/EXPRESSION_OCL"; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append(genPackage.getProviderPackageName());
    stringBuffer.append(TEXT_2);
    genModel.addImport("org.eclipse.emf.common.notify.AdapterFactory");
    genModel.addImport("org.eclipse.emf.common.notify.Notification");
    genModel.addImport("org.eclipse.emf.edit.provider.IEditingDomainItemProvider");
    genModel.addImport("org.eclipse.emf.edit.provider.IItemLabelProvider");
    genModel.addImport("org.eclipse.emf.edit.provider.IItemPropertySource");
    genModel.addImport("org.eclipse.emf.edit.provider.IStructuredItemContentProvider");
    genModel.addImport("org.eclipse.emf.edit.provider.ITreeItemContentProvider");
    String _List = genModel.getImportedName(genModel.useGenerics() ? "java.util.List<org.eclipse.emf.edit.provider.IItemPropertyDescriptor>" : "java.util.List");
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(genClass.getProviderClassName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(genClass.getProviderBaseClassName() != null ? genClass.getProviderBaseClassName() : genModel.getImportedName("org.eclipse.emf.edit.provider.ItemProviderAdapter"));
    stringBuffer.append(TEXT_6);
    for (Iterator<String> i = genPackage.getProviderSupportedTypes().iterator(); i.hasNext(); ) {
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genModel.getImportedName(i.next()));
    if (i.hasNext()){
    stringBuffer.append(TEXT_8);
    }
    }
    stringBuffer.append(TEXT_9);
    if (genModel.hasCopyrightField()) {
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genModel.getCopyrightFieldLiteral());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_13);
    }
    stringBuffer.append(TEXT_14);
    stringBuffer.append(genClass.getProviderClassName());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(TEXT_16);
     String s = "aa";
EList<GenFeature> classOfProvider = genClass.getGenFeatures();
ArrayList<GenFeature> derivedChangeableFeat = new ArrayList<GenFeature>();
int m = classOfProvider.size();
boolean hasXUpdate = false;

for(GenFeature feat: classOfProvider)
{
  if (feat.isDerived() && feat.isChangeable())
 {
 
 for(GenOperation eOp: genClass.getGenOperations())
{
if (eOp.getName().equals(feat.getUncapName().concat("$Update")))
{
     hasXUpdate = true;
     derivedChangeableFeat.add(feat);

}
}
    
  }
}
 if (hasXUpdate){
    stringBuffer.append(TEXT_17);
     if (!derivedChangeableFeat.isEmpty()){ 

    stringBuffer.append(TEXT_18);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_19);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_20);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_21);
    for (Iterator<GenFeature> i = derivedChangeableFeat.iterator(); i.hasNext(); ) {
GenFeature genFeat = i.next();
GenOperation eOperationForFeature = null;
String stringPart = "else";
int counter = 0;

for(GenOperation eOp: genClass.getGenOperations())
{
if (eOp.getName().equals(genFeat.getUncapName().concat("$Update")))
   eOperationForFeature= eOp;
}


    stringBuffer.append(TEXT_22);
    if (i.hasNext() && counter >0) {
        counter++; 
       
    stringBuffer.append(TEXT_23);
    
        } 
    stringBuffer.append(TEXT_24);
    stringBuffer.append(genFeat.getQualifiedFeatureAccessorName());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(eOperationForFeature.getName());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(eOperationForFeature.getParameterTypes(", "));
    stringBuffer.append(TEXT_27);
    }
    stringBuffer.append(TEXT_28);
    }
    stringBuffer.append(TEXT_29);
    }
    stringBuffer.append(TEXT_30);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_31);
    }
    stringBuffer.append(TEXT_32);
    stringBuffer.append(_List);
    stringBuffer.append(TEXT_33);
    for (GenFeature genFeature : genClass.getPropertyFeatures()) { 
    boolean isExpertProperty = false;
    if (!genFeature.getPropertyFilterFlags().isEmpty()) {
      for (Iterator<String> j = genFeature.getPropertyFilterFlags().iterator(); j.hasNext();) { String filterFlag = j.next();
        if ("org.eclipse.ui.views.properties.expert".equals(filterFlag)) {
			isExpertProperty = true;
			break;
        } // if ("org.eclipse.ui.views.properties.expert".equals(filterFlag))
      }// for (Iterator<String> j = genFeature.getPropertyFilterFlags().iterator(); j.hasNext();)
  } // if (!genFeature.getPropertyFilterFlags().isEmpty())
  
    if (isExpertProperty) {
    stringBuffer.append(TEXT_34);
    }
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_36);
    if (isExpertProperty) {
    stringBuffer.append(TEXT_37);
    }
    }
    stringBuffer.append(TEXT_38);
    for (GenFeature genFeature : genClass.getPropertyFeatures()) { 
    stringBuffer.append(TEXT_39);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_40);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_41);
    
boolean hasChoiceConstraintOCL = false;
boolean hasChoiceConstructionOCL = false;
boolean hasEcoreReference = false;
boolean hasExpressionOCL = false;
String choiceConstraint = null;
String choiceConstruction = null;

EStructuralFeature eStructuralFeature = (EStructuralFeature)genFeature.getEcoreFeature();
EAnnotation ocl= eStructuralFeature.getEAnnotation(oclNsURI);
if (ocl != null) {
	choiceConstraint = ocl.getDetails().get("choiceConstraint");
	choiceConstruction = ocl.getDetails().get("choiceConstruction");
}
if (choiceConstraint != null) {hasChoiceConstraintOCL = true;}
if (choiceConstruction != null) {hasChoiceConstructionOCL = true;}

if (genFeature.getEcoreFeature() instanceof EReference){
	String instanceClassName = ((EReference) eStructuralFeature).getEReferenceType().getInstanceClassName();
	if ("org.eclipse.emf.ecore.EClass".equals(instanceClassName)
		|| "org.eclipse.emf.ecore.EStructuralFeature".equals(instanceClassName)) {
		hasEcoreReference = true;
	}
} else if (genFeature.getEcoreFeature() instanceof EAttribute) {
    hasExpressionOCL = ((EAttribute)genFeature.getEcoreFeature()).getEAnnotation(expressionOclNsURI) != null;
}

    stringBuffer.append(TEXT_42);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_43);
    if (hasChoiceConstructionOCL) {
    stringBuffer.append(TEXT_44);
    stringBuffer.append(choiceConstruction);
    }
    if (hasChoiceConstraintOCL) {
    stringBuffer.append(TEXT_45);
    stringBuffer.append(choiceConstraint);
    }
    stringBuffer.append(TEXT_46);
    if (hasChoiceConstraintOCL || hasChoiceConstructionOCL || hasEcoreReference || hasExpressionOCL) {
  	//ADDED www.xocl.org
  
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.edit.provider.ItemPropertyDescriptor"));
    } else {
    stringBuffer.append(TEXT_48);
    }
    stringBuffer.append(TEXT_49);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.edit.provider.ComposeableAdapterFactory"));
    stringBuffer.append(TEXT_50);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_51);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(genModel.getNonNLS());
    if (genFeature.getPropertyDescription() == null || genFeature.getPropertyDescription().length() == 0) {
    stringBuffer.append(TEXT_53);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_54);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    stringBuffer.append(genModel.getNonNLS(3));
    } else {
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_58);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_59);
    stringBuffer.append(genModel.getNonNLS());
    }
    stringBuffer.append(TEXT_60);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(genFeature.getProperty() == GenPropertyKind.EDITABLE_LITERAL ? "true" : "false");
    stringBuffer.append(TEXT_62);
    stringBuffer.append(genFeature.isPropertyMultiLine() ? "true" : "false");
    stringBuffer.append(TEXT_63);
    stringBuffer.append(genFeature.isPropertySortChoices() ? "true" : "false");
    stringBuffer.append(TEXT_64);
    if (genFeature.isReferenceType()) {
    stringBuffer.append(TEXT_65);
    } else {
    stringBuffer.append(TEXT_66);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.edit.provider.ItemPropertyDescriptor"));
    stringBuffer.append(TEXT_67);
    stringBuffer.append(genFeature.getPropertyImageName());
    stringBuffer.append(TEXT_68);
    }
    if (genFeature.getPropertyCategory() == null || genFeature.getPropertyCategory().length() == 0) {
    stringBuffer.append(TEXT_69);
    } else {
    stringBuffer.append(TEXT_70);
    stringBuffer.append(genModel.getPropertyCategoryKey(genFeature.getPropertyCategory()));
    stringBuffer.append(TEXT_71);
    stringBuffer.append(genModel.getNonNLS());
    }
    if (genFeature.getPropertyFilterFlags().isEmpty()) {
    stringBuffer.append(TEXT_72);
    } else {
    stringBuffer.append(TEXT_73);
    for (Iterator<String> j = genFeature.getPropertyFilterFlags().iterator(); j.hasNext();) { String filterFlag = j.next();
    if (filterFlag != null && filterFlag.length() > 0) {
    stringBuffer.append(TEXT_74);
    stringBuffer.append(filterFlag);
    stringBuffer.append(TEXT_75);
    if (j.hasNext()) {
    stringBuffer.append(TEXT_76);
    }
    stringBuffer.append(genModel.getNonNLS());
    }
    }
    stringBuffer.append(TEXT_77);
    }
    if (hasChoiceConstraintOCL || hasChoiceConstructionOCL || hasEcoreReference) {
    stringBuffer.append(TEXT_78);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_79);
    }
    stringBuffer.append(TEXT_80);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_81);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_82);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_83);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_84);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_86);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_87);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_88);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_89);
    if (hasChoiceConstructionOCL) {
    stringBuffer.append(TEXT_90);
    stringBuffer.append(genClass.getImportedClassName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_92);
    if (eStructuralFeature.isUnsettable() && !eStructuralFeature.isMany()) {
    stringBuffer.append(TEXT_93);
    } else { 
    stringBuffer.append(TEXT_94);
    }
    } else if (hasEcoreReference) {
    stringBuffer.append(TEXT_95);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_96);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_97);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_98);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_99);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_100);
    stringBuffer.append(genModel.getImportedName("java.util.LinkedList"));
    stringBuffer.append(TEXT_101);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.resource.Resource"));
    stringBuffer.append(TEXT_102);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_103);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.resource.ResourceSet"));
    stringBuffer.append(TEXT_104);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_105);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_106);
    stringBuffer.append(genModel.getImportedName("java.util.HashSet"));
    stringBuffer.append(TEXT_107);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_108);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EPackage.Registry"));
    stringBuffer.append(TEXT_109);
    stringBuffer.append(genModel.getImportedName("java.util.Iterator"));
    stringBuffer.append(TEXT_110);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_111);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_112);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_113);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_114);
    }
    if (hasChoiceConstraintOCL) {
    stringBuffer.append(TEXT_115);
    stringBuffer.append(genModel.getImportedName("java.util.Iterator"));
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_118);
    if (!eStructuralFeature.isUnsettable()) {
    stringBuffer.append(TEXT_119);
    }
    stringBuffer.append(TEXT_120);
    stringBuffer.append(genClass.getImportedClassName());
    stringBuffer.append(TEXT_121);
    if (genClass.isMapEntry()){
    stringBuffer.append(TEXT_122);
    }
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_123);
    }
    stringBuffer.append(TEXT_124);
    } else if (hasExpressionOCL) {
    stringBuffer.append(TEXT_125);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_126);
    }
    stringBuffer.append(TEXT_127);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.expr.OCLExpressionContext"));
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genClass.getImportedClassName());
    stringBuffer.append(TEXT_129);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_130);
    }
    stringBuffer.append(TEXT_131);
    //ItemProvider/addPropertyDescriptor.override.javajetinc
    stringBuffer.append(TEXT_132);
    }
    if (!genClass.getChildrenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_133);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_134);
    }
    stringBuffer.append(TEXT_135);
    stringBuffer.append(genModel.getImportedName(genModel.useGenerics() ? "java.util.Collection<? extends org.eclipse.emf.ecore.EStructuralFeature>" : "java.util.Collection"));
    stringBuffer.append(TEXT_136);
    for (GenFeature genFeature : genClass.getChildrenFeatures()) { 
    stringBuffer.append(TEXT_137);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_138);
    }
    stringBuffer.append(TEXT_139);
    if (!genClass.getChildrenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_140);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_141);
    }
    stringBuffer.append(TEXT_142);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_143);
    }
    }
    if (genClass.needsHasChildrenMethodOverride()) {
    stringBuffer.append(TEXT_144);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_145);
    }
    stringBuffer.append(TEXT_146);
    stringBuffer.append(genModel.isOptimizedHasChildren());
    stringBuffer.append(TEXT_147);
    }
    if (genClass.isImage()) {
    stringBuffer.append(TEXT_148);
    stringBuffer.append(genClass.getName());
    stringBuffer.append(TEXT_149);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_150);
    }
    stringBuffer.append(TEXT_151);
    stringBuffer.append(genClass.getName());
    stringBuffer.append(TEXT_152);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_153);
    }
    if (genModel.getRuntimeVersion().getValue() >= GenRuntimeVersion.EMF26_VALUE && !genModel.isCreationIcons()) {
    stringBuffer.append(TEXT_154);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_155);
    }
    stringBuffer.append(TEXT_156);
    }
    stringBuffer.append(TEXT_157);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_158);
    }
    stringBuffer.append(TEXT_159);
     
EAnnotation ocl= genClass.getEcoreClass().getEAnnotation(oclNsURI);
String oclLabelDef = null;

if (ocl != null) {oclLabelDef = ocl.getDetails().get("label");}
if (oclLabelDef != null) { 
    stringBuffer.append(TEXT_160);
    stringBuffer.append(genClass.getImportedClassName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_161);
     } else {
    stringBuffer.append(TEXT_162);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_164);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_165);
    if (genClass.isMapEntry()) {
    stringBuffer.append(TEXT_166);
    stringBuffer.append(genClass.getImportedInterfaceName());
    if (genModel.useGenerics()) {
    stringBuffer.append(TEXT_167);
    }
    stringBuffer.append(TEXT_168);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_169);
    stringBuffer.append(genClass.getImportedInterfaceName());
    if (genModel.useGenerics()) {
    stringBuffer.append(TEXT_170);
    }
    stringBuffer.append(TEXT_171);
    if (!genClass.getMapEntryKeyFeature().isPropertyMultiLine() && !genClass.getMapEntryValueFeature().isPropertyMultiLine()) {
    stringBuffer.append(TEXT_172);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_173);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_174);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    } else {
    if (genClass.getMapEntryKeyFeature().isPropertyMultiLine()) {
    stringBuffer.append(TEXT_175);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_176);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_177);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_178);
    stringBuffer.append(genModel.getNonNLS());
    }
    if (genClass.getMapEntryValueFeature().isPropertyMultiLine()) {
    stringBuffer.append(TEXT_179);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_180);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_181);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_182);
    stringBuffer.append(genModel.getNonNLS());
    }
    stringBuffer.append(TEXT_183);
    stringBuffer.append(genModel.getNonNLS());
    }
    } else if (genClass.getLabelFeature() != null) { GenFeature labelFeature = genClass.getLabelFeature();
    if (labelFeature.isPrimitiveType() && !labelFeature.getGenClass().isDynamic() && !labelFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_184);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_185);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_186);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_187);
    stringBuffer.append(genClass.getSafeUncapName());
    stringBuffer.append(TEXT_188);
    stringBuffer.append(genClass.getLabelFeature().getGetAccessor());
    stringBuffer.append(TEXT_189);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    } else {
    if (labelFeature.isStringType() && !labelFeature.getGenClass().isDynamic() && !labelFeature.isSuppressedGetVisibility()) {
    if (labelFeature.isPropertyMultiLine()) {
    stringBuffer.append(TEXT_190);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_191);
    stringBuffer.append(labelFeature.getGetAccessor());
    stringBuffer.append(TEXT_192);
    } else {
    stringBuffer.append(TEXT_193);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_194);
    stringBuffer.append(labelFeature.getGetAccessor());
    stringBuffer.append(TEXT_195);
    }
    } else {
    if (labelFeature.isSuppressedGetVisibility() || labelFeature.getGenClass().isDynamic()) {
    stringBuffer.append(TEXT_196);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_198);
    stringBuffer.append(labelFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_199);
    } else {
    stringBuffer.append(TEXT_200);
    stringBuffer.append(labelFeature.getRawImportedType());
    stringBuffer.append(TEXT_201);
    stringBuffer.append(genClass.getImportedInterfaceName());
    stringBuffer.append(genClass.getInterfaceWildTypeArguments());
    stringBuffer.append(TEXT_202);
    stringBuffer.append(labelFeature.getGetAccessor());
    stringBuffer.append(TEXT_203);
    }
    stringBuffer.append(TEXT_204);
    }
    stringBuffer.append(TEXT_205);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_206);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    }
    } else {
    stringBuffer.append(TEXT_207);
    stringBuffer.append(genModel.getNonNLS());
    }
    }
    //ItemProvider/getText.override.javajetinc
    stringBuffer.append(TEXT_208);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_209);
    }
    stringBuffer.append(TEXT_210);
    if (!genClass.getLabelNotifyFeatures().isEmpty() || !genClass.getContentNotifyFeatures().isEmpty() || !genClass.getLabelAndContentNotifyFeatures().isEmpty()) {
    stringBuffer.append(TEXT_211);
    stringBuffer.append(genClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_212);
    if (!genClass.getLabelNotifyFeatures().isEmpty()) {
    for (GenFeature genFeature : genClass.getLabelNotifyFeatures()) { 
    stringBuffer.append(TEXT_213);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_214);
    }
    stringBuffer.append(TEXT_215);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.edit.provider.ViewerNotification"));
    stringBuffer.append(TEXT_216);
    }
    if (!genClass.getContentNotifyFeatures().isEmpty()) {
    for (GenFeature genFeature : genClass.getContentNotifyFeatures()) { 
    stringBuffer.append(TEXT_217);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_218);
    }
    stringBuffer.append(TEXT_219);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.edit.provider.ViewerNotification"));
    stringBuffer.append(TEXT_220);
    }
    if (!genClass.getLabelAndContentNotifyFeatures().isEmpty()) {
    for (GenFeature genFeature : genClass.getLabelAndContentNotifyFeatures()) { 
    stringBuffer.append(TEXT_221);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_222);
    }
    stringBuffer.append(TEXT_223);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.edit.provider.ViewerNotification"));
    stringBuffer.append(TEXT_224);
    }
    stringBuffer.append(TEXT_225);
    }
    stringBuffer.append(TEXT_226);
    if (genModel.isCreationCommands()) {
    stringBuffer.append(TEXT_227);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_228);
    }
    stringBuffer.append(TEXT_229);
    stringBuffer.append(genModel.getImportedName(genModel.useGenerics() ? "java.util.Collection<java.lang.Object>" : "java.util.Collection"));
    stringBuffer.append(TEXT_230);
    for (GenClass.ChildCreationData childCreationData : genClass.getChildCreationData()) { GenFeature createFeature = childCreationData.createFeature; GenFeature delegatedFeature = childCreationData.delegatedFeature; GenClassifier createClassifier = childCreationData.createClassifier;
    if (createFeature.isFeatureMapType()) {
    if (delegatedFeature.isReferenceType()) { GenClass createClass = (GenClass)createClassifier;
    stringBuffer.append(TEXT_231);
    stringBuffer.append(createFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_232);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMapUtil"));
    stringBuffer.append(TEXT_233);
    stringBuffer.append(delegatedFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_234);
    if (createClass.isMapEntry()) { 
    stringBuffer.append(TEXT_235);
    stringBuffer.append(createClass.getGenPackage().getQualifiedEFactoryInstanceAccessor());
    stringBuffer.append(TEXT_236);
    stringBuffer.append(createClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_237);
    } else {
    stringBuffer.append(TEXT_238);
    stringBuffer.append(createClass.getGenPackage().getQualifiedFactoryInstanceAccessor());
    stringBuffer.append(TEXT_239);
    stringBuffer.append(createClass.getName());
    stringBuffer.append(TEXT_240);
    }
    //ItemProvider/newChildDescriptorsReferenceDelegatedFeature.override.javajetinc
    } else { GenDataType createDataType = (GenDataType)createClassifier;
    stringBuffer.append(TEXT_241);
    stringBuffer.append(createFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_242);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMapUtil"));
    stringBuffer.append(TEXT_243);
    stringBuffer.append(delegatedFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_244);
    if (delegatedFeature.isEnumBasedType()) {
    stringBuffer.append(TEXT_245);
    stringBuffer.append(delegatedFeature.getTypeGenEnum().getStaticValue(delegatedFeature.getEcoreFeature().getDefaultValueLiteral()));
    stringBuffer.append(TEXT_246);
    } else if (delegatedFeature.isStringBasedType()) {
    stringBuffer.append(TEXT_247);
    stringBuffer.append(delegatedFeature.getCreateChildValueLiteral());
    stringBuffer.append(TEXT_248);
    stringBuffer.append(genModel.getNonNLS());
    } else { String literal = delegatedFeature.getCreateChildValueLiteral();
    stringBuffer.append(TEXT_249);
    stringBuffer.append(createDataType.getGenPackage().getQualifiedEFactoryInstanceAccessor());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(createDataType.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_251);
    stringBuffer.append(literal);
    stringBuffer.append(TEXT_252);
    if (literal != null) {
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_253);
    }
    }
    //ItemProvider/newChildDescriptorsAttributeDelegatedFeature.override.javajetinc
    }
    } else if (createFeature.isReferenceType()) { GenClass createClass = (GenClass)createClassifier;
    stringBuffer.append(TEXT_254);
    stringBuffer.append(createFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_255);
    if (createClass.isMapEntry()) { 
    stringBuffer.append(TEXT_256);
    stringBuffer.append(createClass.getGenPackage().getQualifiedEFactoryInstanceAccessor());
    stringBuffer.append(TEXT_257);
    stringBuffer.append(createClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_258);
    } else {
    stringBuffer.append(TEXT_259);
    stringBuffer.append(createClass.getGenPackage().getQualifiedFactoryInstanceAccessor());
    stringBuffer.append(TEXT_260);
    stringBuffer.append(createClass.getName());
    stringBuffer.append(TEXT_261);
    }
    //ItemProvider/newChildDescriptorsReferenceFeature.override.javajetinc 
    } else { GenDataType createDataType = (GenDataType)createClassifier;
    stringBuffer.append(TEXT_262);
    stringBuffer.append(createFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_263);
    if (createFeature.isEnumBasedType()) {
    stringBuffer.append(TEXT_264);
    stringBuffer.append(createFeature.getTypeGenEnum().getStaticValue(createFeature.getEcoreFeature().getDefaultValueLiteral()));
    stringBuffer.append(TEXT_265);
    } else if (createFeature.isStringBasedType()) {
    stringBuffer.append(TEXT_266);
    stringBuffer.append(createFeature.getCreateChildValueLiteral());
    stringBuffer.append(TEXT_267);
    stringBuffer.append(genModel.getNonNLS());
    } else { String literal = createFeature.getCreateChildValueLiteral();
    stringBuffer.append(TEXT_268);
    stringBuffer.append(createDataType.getGenPackage().getQualifiedEFactoryInstanceAccessor());
    stringBuffer.append(TEXT_269);
    stringBuffer.append(createDataType.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_270);
    stringBuffer.append(literal);
    stringBuffer.append(TEXT_271);
    if (literal != null) {
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_272);
    }
    }
    //ItemProvider/newChildDescriptorsAttributeFeature.override.javajetinc
    }
    }
    stringBuffer.append(TEXT_273);
    if (!genClass.getSharedClassCreateChildFeatures().isEmpty()) {
    stringBuffer.append(TEXT_274);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_275);
    }
    stringBuffer.append(TEXT_276);
    stringBuffer.append(genModel.getImportedName(genModel.useGenerics() ? "java.util.Collection<?>" : "java.util.Collection"));
    stringBuffer.append(TEXT_277);
    if (genClass.hasFeatureMapCreateChildFeatures()) {
    stringBuffer.append(TEXT_278);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_279);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMapUtil"));
    stringBuffer.append(TEXT_280);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_281);
    }
    stringBuffer.append(TEXT_282);
    for (Iterator<GenFeature> i = genClass.getSharedClassCreateChildFeatures().iterator(); i.hasNext();) { GenFeature createFeature = i.next();
    stringBuffer.append(TEXT_283);
    stringBuffer.append(createFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(i.hasNext() ? " ||" : ";");
    }
    stringBuffer.append(TEXT_284);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_285);
    }
    }
    if (genClass.getProviderExtendsGenClass() == null || genClass.getProviderExtendsGenClass().getGenPackage() != genPackage && (!genPackage.isExtensibleProviderFactory() || genClass.getProviderExtendsGenClass().getGenPackage().isExtensibleProviderFactory() != genPackage.isExtensibleProviderFactory())) {
    stringBuffer.append(TEXT_286);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_287);
    }
    stringBuffer.append(TEXT_288);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.ResourceLocator"));
    stringBuffer.append(TEXT_289);
    if (genPackage.isExtensibleProviderFactory()) {
    stringBuffer.append(TEXT_290);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.edit.provider.IChildCreationExtender"));
    stringBuffer.append(TEXT_291);
    } else {
    stringBuffer.append(TEXT_292);
    stringBuffer.append(genPackage.getImportedEditPluginClassName());
    stringBuffer.append(TEXT_293);
    }
    stringBuffer.append(TEXT_294);
    
	java.util.Set<GenClass> wrappingNeeded = new java.util.HashSet<GenClass>();
	for (GenFeature feature : genClass.getAllGenFeatures()) {
		if (feature.isChildren() && feature.getEcoreFeature() instanceof EReference) {
			EReference ref = (EReference)feature.getEcoreFeature();
			if (!ref.isContainment()) {
				wrappingNeeded.add(feature.getTypeGenClass());
			}
		}
	}

	List<GenClass> sortedWrappingNeeded = new ArrayList<GenClass>(wrappingNeeded);
	Collections.sort(sortedWrappingNeeded, new Comparator<GenClass>() {
		@Override
		public int compare(GenClass o1, GenClass o2) {
			return o1.getQualifiedClassName().compareTo(o2.getQualifiedClassName());
		}
		
	});

    stringBuffer.append(TEXT_295);
    if (!sortedWrappingNeeded.isEmpty()) {
    stringBuffer.append(TEXT_296);
    for(GenClass _class: sortedWrappingNeeded) {
		
    stringBuffer.append(TEXT_297);
    stringBuffer.append(_class.getImportedInterfaceName());
    stringBuffer.append(TEXT_298);
    }
    stringBuffer.append(TEXT_299);
    } //if (!wrappingNeeded.isEmpty())
    stringBuffer.append(TEXT_300);
    }
    
EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
if (eAnnotation != null) {
	Set<String> visited = new HashSet<String>();
	EMap<String, String> details = eAnnotation.getDetails();
	for (Map.Entry<String, String> entry : details) {
		String detailKey = entry.getKey();
		if (detailKey.endsWith("ChoiceConstraint") || detailKey.endsWith("ChoiceConstruction")) {
			String featureName = detailKey.substring(0, detailKey.length()-(detailKey.endsWith("ChoiceConstraint") ? 16 : 18));
			if (visited.contains(featureName)) {
				continue;
			}
			visited.add(featureName);
			for (GenFeature genFeature : genClass.getAllGenFeatures()) {
				//for all genClass.getPropertyFeatures() this is already generated by addropertyDescriptor.override.javajetinc
				if (genClass.getPropertyFeatures().contains(genFeature)) {
					continue;
				}
				if (genFeature.getName().equals(featureName)) {
					boolean hasChoiceConstraintOCL = eAnnotation.getDetails().containsKey(featureName+"ChoiceConstraint");
					boolean hasChoiceConstructionOCL = eAnnotation.getDetails().containsKey(featureName+"ChoiceConstruction");
//@[CopyOf,addropertyDescriptor.override.javajetinc]

    stringBuffer.append(TEXT_301);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_302);
    if (hasChoiceConstructionOCL) {
    stringBuffer.append(TEXT_303);
    stringBuffer.append(eAnnotation.getDetails().get(featureName+"ChoiceConstruction"));
    }
    if (hasChoiceConstraintOCL) {
    stringBuffer.append(TEXT_304);
    stringBuffer.append(eAnnotation.getDetails().get(featureName+"ChoiceConstraint"));
    }
    stringBuffer.append(TEXT_305);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_306);
    }
    stringBuffer.append(TEXT_307);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_308);
    if (hasChoiceConstraintOCL || hasChoiceConstructionOCL) {
  	//ADDED www.xocl.org
  
    stringBuffer.append(TEXT_309);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.edit.provider.ItemPropertyDescriptor"));
    } else {
    stringBuffer.append(TEXT_310);
    }
    stringBuffer.append(TEXT_311);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.edit.provider.ComposeableAdapterFactory"));
    stringBuffer.append(TEXT_312);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_313);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_314);
    stringBuffer.append(genModel.getNonNLS());
    if (genFeature.getPropertyDescription() == null || genFeature.getPropertyDescription().length() == 0) {
    stringBuffer.append(TEXT_315);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_316);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_317);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_318);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    stringBuffer.append(genModel.getNonNLS(3));
    } else {
    stringBuffer.append(TEXT_319);
    stringBuffer.append(genFeature.getGenClass().getName());
    stringBuffer.append(TEXT_320);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_321);
    stringBuffer.append(genModel.getNonNLS());
    }
    stringBuffer.append(TEXT_322);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_323);
    stringBuffer.append(genFeature.getProperty() == GenPropertyKind.EDITABLE_LITERAL ? "true" : "false");
    stringBuffer.append(TEXT_324);
    stringBuffer.append(genFeature.isPropertyMultiLine() ? "true" : "false");
    stringBuffer.append(TEXT_325);
    stringBuffer.append(genFeature.isPropertySortChoices() ? "true" : "false");
    stringBuffer.append(TEXT_326);
    if (genFeature.isReferenceType()) {
    stringBuffer.append(TEXT_327);
    } else {
    stringBuffer.append(TEXT_328);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.edit.provider.ItemPropertyDescriptor"));
    stringBuffer.append(TEXT_329);
    stringBuffer.append(genFeature.getPropertyImageName());
    stringBuffer.append(TEXT_330);
    }
    if (genFeature.getPropertyCategory() == null || genFeature.getPropertyCategory().length() == 0) {
    stringBuffer.append(TEXT_331);
    } else {
    stringBuffer.append(TEXT_332);
    stringBuffer.append(genModel.getPropertyCategoryKey(genFeature.getPropertyCategory()));
    stringBuffer.append(TEXT_333);
    stringBuffer.append(genModel.getNonNLS());
    }
    if (genFeature.getPropertyFilterFlags().isEmpty()) {
    stringBuffer.append(TEXT_334);
    } else {
    stringBuffer.append(TEXT_335);
    for (Iterator<String> j = genFeature.getPropertyFilterFlags().iterator(); j.hasNext();) { String filterFlag = j.next();
    if (filterFlag != null && filterFlag.length() > 0) {
    stringBuffer.append(TEXT_336);
    stringBuffer.append(filterFlag);
    stringBuffer.append(TEXT_337);
    if (j.hasNext()) {
    stringBuffer.append(TEXT_338);
    }
    stringBuffer.append(genModel.getNonNLS());
    }
    }
    stringBuffer.append(TEXT_339);
    }
    if (hasChoiceConstraintOCL || hasChoiceConstructionOCL) {
    stringBuffer.append(TEXT_340);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_341);
    }
    stringBuffer.append(TEXT_342);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_343);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_345);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_346);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_347);
    if (hasChoiceConstructionOCL) {
    stringBuffer.append(TEXT_348);
    stringBuffer.append(genClass.getImportedClassName());
    stringBuffer.append(TEXT_349);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_350);
    }
    if (hasChoiceConstraintOCL) {
    stringBuffer.append(TEXT_351);
    stringBuffer.append(genModel.getImportedName("java.util.Iterator"));
    stringBuffer.append(TEXT_352);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_353);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_354);
    stringBuffer.append(genClass.getImportedClassName());
    stringBuffer.append(TEXT_355);
    if (genClass.isMapEntry()) {
    stringBuffer.append(TEXT_356);
    }
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_357);
    }
    stringBuffer.append(TEXT_358);
    }
    stringBuffer.append(TEXT_359);
    
//@[EndCopyOf,addropertyDescriptor.override.javajetinc]
				}
			}
		}
	}
}

    
boolean hasAdvancedProperties = false;
m_exit:
for (GenFeature genFeature : genClass.getPropertyFeatures()) {
	for (Iterator<String> j = genFeature.getPropertyFilterFlags().iterator(); j.hasNext();) { String filterFlag = j.next();
		if ("org.eclipse.ui.views.properties.expert".equals(filterFlag)) {
			hasAdvancedProperties = true;
			break m_exit;
		} // if ("org.eclipse.ui.views.properties.expert".equals(filterFlag))
	}// for (Iterator<String> j = genFeature.getPropertyFilterFlags().iterator(); j.hasNext();)
}
if (hasAdvancedProperties) { 
    stringBuffer.append(TEXT_360);
    stringBuffer.append( genClass.getGenPackage().getItemProviderAdapterFactoryClassName());
    stringBuffer.append(TEXT_361);
    
} //if (!genClass.etPropertyFeatures().isEmpty()) 

    stringBuffer.append(TEXT_362);
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_363);
    return stringBuffer.toString();
  }
}
