package org.eclipse.emf.codegen.ecore.templates.model.copy;

import org.eclipse.emf.codegen.ecore.genmodel.*;

public class ResourceFactoryClass
{
  protected static String nl;
  public static synchronized ResourceFactoryClass create(String lineSeparator)
  {
    nl = lineSeparator;
    ResourceFactoryClass result = new ResourceFactoryClass();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";";
  protected final String TEXT_3 = NL + NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * The <b>Resource Factory</b> associated with the package." + NL + " * <!-- end-user-doc -->" + NL + " * @see ";
  protected final String TEXT_4 = NL + " * @generated" + NL + " */" + NL + "public class ";
  protected final String TEXT_5 = " extends ";
  protected final String TEXT_6 = NL + "{";
  protected final String TEXT_7 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic static final ";
  protected final String TEXT_8 = " copyright = ";
  protected final String TEXT_9 = ";";
  protected final String TEXT_10 = NL;
  protected final String TEXT_11 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected ";
  protected final String TEXT_12 = " extendedMetaData;" + NL;
  protected final String TEXT_13 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected ";
  protected final String TEXT_14 = " xmlMap = new ";
  protected final String TEXT_15 = "();" + NL;
  protected final String TEXT_16 = NL + "\t/**" + NL + "\t * Creates an instance of the resource factory." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_17 = "()" + NL + "\t{" + NL + "\t\tsuper();";
  protected final String TEXT_18 = NL + "\t\textendedMetaData = new ";
  protected final String TEXT_19 = "(new ";
  protected final String TEXT_20 = "(";
  protected final String TEXT_21 = ".Registry.INSTANCE));" + NL + "\t\textendedMetaData.putPackage(null, ";
  protected final String TEXT_22 = ".eINSTANCE);";
  protected final String TEXT_23 = NL + "\t\txmlMap.setNoNamespacePackage(";
  protected final String TEXT_24 = ".eINSTANCE);";
  protected final String TEXT_25 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * Creates an instance of the resource." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_26 = NL + "\t@Override";
  protected final String TEXT_27 = NL + "\tpublic Resource createResource(URI uri)" + NL + "\t{";
  protected final String TEXT_28 = NL + "\t";
  protected final String TEXT_29 = " result = new ";
  protected final String TEXT_30 = "(uri) {" + NL + "\t";
  protected final String TEXT_31 = NL + "\t\t@Override" + NL + "\t\t";
  protected final String TEXT_32 = NL + "\t\tprotected boolean useUUIDs() { " + NL + "\t\t\treturn true;" + NL + "\t\t}" + NL + "\t};" + NL + "\t";
  protected final String TEXT_33 = " xmlMap = new ";
  protected final String TEXT_34 = "();" + NL + "\txmlMap.setIDAttributeName(\"";
  protected final String TEXT_35 = "\");";
  protected final String TEXT_36 = NL + "\tresult.getDefaultSaveOptions().put(";
  protected final String TEXT_37 = ".OPTION_XML_MAP, xmlMap);" + NL + "\tresult.getDefaultLoadOptions().put(";
  protected final String TEXT_38 = ".OPTION_XML_MAP, xmlMap);" + NL + "\treturn result;";
  protected final String TEXT_39 = NL + "\t\t";
  protected final String TEXT_40 = " result = new ";
  protected final String TEXT_41 = "(uri);" + NL + "\t\tresult.getDefaultSaveOptions().put(";
  protected final String TEXT_42 = ".OPTION_EXTENDED_META_DATA, ";
  protected final String TEXT_43 = "Boolean.TRUE";
  protected final String TEXT_44 = "extendedMetaData";
  protected final String TEXT_45 = ");" + NL + "\t\tresult.getDefaultLoadOptions().put(";
  protected final String TEXT_46 = ".OPTION_EXTENDED_META_DATA, ";
  protected final String TEXT_47 = "Boolean.TRUE";
  protected final String TEXT_48 = "extendedMetaData";
  protected final String TEXT_49 = ");" + NL + "" + NL + "\t\tresult.getDefaultSaveOptions().put(";
  protected final String TEXT_50 = ".OPTION_SCHEMA_LOCATION, Boolean.TRUE);" + NL + "" + NL + "\t\tresult.getDefaultLoadOptions().put(";
  protected final String TEXT_51 = ".OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);" + NL + "\t\tresult.getDefaultSaveOptions().put(";
  protected final String TEXT_52 = ".OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);" + NL + "" + NL + "\t\tresult.getDefaultLoadOptions().put(";
  protected final String TEXT_53 = ".OPTION_USE_LEXICAL_HANDLER, Boolean.TRUE);";
  protected final String TEXT_54 = NL + "\t\tresult.getDefaultLoadOptions().put(";
  protected final String TEXT_55 = ".OPTION_USE_DATA_CONVERTER, Boolean.TRUE);";
  protected final String TEXT_56 = NL + "\t\t";
  protected final String TEXT_57 = " result = new ";
  protected final String TEXT_58 = "(uri);" + NL + "\t\tresult.getDefaultSaveOptions().put(";
  protected final String TEXT_59 = ".OPTION_XML_MAP, xmlMap);" + NL + "\t\tresult.getDefaultLoadOptions().put(";
  protected final String TEXT_60 = ".OPTION_XML_MAP, xmlMap);";
  protected final String TEXT_61 = NL + "\t\tResource result = new ";
  protected final String TEXT_62 = "(uri);";
  protected final String TEXT_63 = NL + "\t\treturn result;";
  protected final String TEXT_64 = " ";
  protected final String TEXT_65 = NL + "\t}" + NL + "" + NL + "} //";
  protected final String TEXT_66 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * Copyright (c) 2002-2006 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(genPackage.getUtilitiesPackageName());
    stringBuffer.append(TEXT_2);
    genModel.getImportedName("org.eclipse.emf.common.util.URI");
    genModel.getImportedName("org.eclipse.emf.ecore.resource.Resource");
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(genPackage.getQualifiedResourceClassName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(genPackage.getImportedResourceFactoryBaseClassName());
    stringBuffer.append(TEXT_6);
    if (genModel.hasCopyrightField()) {
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getCopyrightFieldLiteral());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_10);
    }
    if (genPackage.hasExtendedMetaData() && !genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.ExtendedMetaData"));
    stringBuffer.append(TEXT_12);
    } else if (genPackage.hasXMLMap()) {
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource$XMLMap"));
    stringBuffer.append(TEXT_14);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.impl.XMLMapImpl"));
    stringBuffer.append(TEXT_15);
    }
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_17);
    if (genPackage.hasExtendedMetaData() && !genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.BasicExtendedMetaData"));
    stringBuffer.append(TEXT_19);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.EPackageRegistryImpl"));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EPackage"));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_22);
    } else if (genPackage.hasXMLMap() && !genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_23);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_24);
    }
    stringBuffer.append(TEXT_25);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_26);
    }
    stringBuffer.append(TEXT_27);
     
 boolean isUseUUID = false;
 String uuidAttributeName = "_uuid";
 org.eclipse.emf.ecore.EModelElement ecoreModelElement = genPackage.getEcoreModelElement();
 org.eclipse.emf.ecore.EAnnotation eAnnotation = ecoreModelElement.getEAnnotation("http://www.xocl.org/UUID");
 if (eAnnotation != null) {
	for (java.util.Map.Entry<String, String> entry : eAnnotation.getDetails()) {
		String key = entry.getKey();
		if ("useUUIDs".equals(key)) {
			String value = entry.getValue();
			isUseUUID = Boolean.parseBoolean(value);
		}
			if ("uuidAttributeName".equals(key)) {
			uuidAttributeName = entry.getValue();
		}
	}
 }
 if (isUseUUID) { 
    stringBuffer.append(TEXT_28);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_29);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_30);
     if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_31);
    }
    stringBuffer.append(TEXT_32);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource$XMLMap"));
    stringBuffer.append(TEXT_33);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.impl.XMLMapImpl"));
    stringBuffer.append(TEXT_34);
    stringBuffer.append(uuidAttributeName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_37);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_38);
     } else { 
    if (genPackage.hasExtendedMetaData()) {
    stringBuffer.append(TEXT_39);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_40);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_41);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_42);
    if (genPackage.hasTargetNamespace()){
    stringBuffer.append(TEXT_43);
    }else{
    stringBuffer.append(TEXT_44);
    }
    stringBuffer.append(TEXT_45);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_46);
    if (genPackage.hasTargetNamespace()){
    stringBuffer.append(TEXT_47);
    }else{
    stringBuffer.append(TEXT_48);
    }
    stringBuffer.append(TEXT_49);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_50);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_51);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_52);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_53);
    if (genPackage.isDataTypeConverters() && genPackage.hasDocumentRoot()) {
    stringBuffer.append(TEXT_54);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_55);
    }
    } else if (genPackage.hasXMLMap()) {
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_58);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_59);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_60);
    } else {
    stringBuffer.append(TEXT_61);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_62);
    }
    stringBuffer.append(TEXT_63);
     } 
    stringBuffer.append(TEXT_64);
    //ResourceFactoryClass/createResource.override.javajetinc
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_66);
    return stringBuffer.toString();
  }
}
