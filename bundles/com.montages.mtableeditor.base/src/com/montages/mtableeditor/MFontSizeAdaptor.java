/**
 */
package com.montages.mtableeditor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>MFont Size Adaptor</b></em>', and utility methods for working with
 * them. <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage#getMFontSizeAdaptor()
 * @model
 * @generated
 */
public enum MFontSizeAdaptor implements Enumerator {
	/**
	 * The '<em><b>Minus Three</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUS_THREE_VALUE
	 * @generated
	 * @ordered
	 */
	MINUS_THREE(0, "MinusThree", "- 3"),
	/**
	 * The '<em><b>Minus Two</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUS_TWO_VALUE
	 * @generated
	 * @ordered
	 */
	MINUS_TWO(1, "MinusTwo", "- 2"),
	/**
	 * The '<em><b>Minus One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUS_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	MINUS_ONE(2, "MinusOne", "- 1"),
	/**
	 * The '<em><b>Zero</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	ZERO(3, "Zero", "0"),
	/**
	 * The '<em><b>Plus One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLUS_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	PLUS_ONE(4, "PlusOne", "+ 1"),
	/**
	 * The '<em><b>Plus Two</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLUS_TWO_VALUE
	 * @generated
	 * @ordered
	 */
	PLUS_TWO(5, "PlusTwo", "+ 2"),
	/**
	 * The '<em><b>Plus Three</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLUS_THREE_VALUE
	 * @generated
	 * @ordered
	 */
	PLUS_THREE(6, "PlusThree", "+ 3");

	/**
	 * The '<em><b>Minus Three</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minus Three</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUS_THREE
	 * @model name="MinusThree" literal="- 3"
	 * @generated
	 * @ordered
	 */
	public static final int MINUS_THREE_VALUE = 0;

	/**
	 * The '<em><b>Minus Two</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minus Two</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUS_TWO
	 * @model name="MinusTwo" literal="- 2"
	 * @generated
	 * @ordered
	 */
	public static final int MINUS_TWO_VALUE = 1;

	/**
	 * The '<em><b>Minus One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minus One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUS_ONE
	 * @model name="MinusOne" literal="- 1"
	 * @generated
	 * @ordered
	 */
	public static final int MINUS_ONE_VALUE = 2;

	/**
	 * The '<em><b>Zero</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Zero</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZERO
	 * @model name="Zero" literal="0"
	 * @generated
	 * @ordered
	 */
	public static final int ZERO_VALUE = 3;

	/**
	 * The '<em><b>Plus One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Plus One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLUS_ONE
	 * @model name="PlusOne" literal="+ 1"
	 * @generated
	 * @ordered
	 */
	public static final int PLUS_ONE_VALUE = 4;

	/**
	 * The '<em><b>Plus Two</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Plus Two</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLUS_TWO
	 * @model name="PlusTwo" literal="+ 2"
	 * @generated
	 * @ordered
	 */
	public static final int PLUS_TWO_VALUE = 5;

	/**
	 * The '<em><b>Plus Three</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Plus Three</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLUS_THREE
	 * @model name="PlusThree" literal="+ 3"
	 * @generated
	 * @ordered
	 */
	public static final int PLUS_THREE_VALUE = 6;

	/**
	 * An array of all the '<em><b>MFont Size Adaptor</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final MFontSizeAdaptor[] VALUES_ARRAY = new MFontSizeAdaptor[] { MINUS_THREE, MINUS_TWO, MINUS_ONE,
			ZERO, PLUS_ONE, PLUS_TWO, PLUS_THREE, };

	/**
	 * A public read-only list of all the '<em><b>MFont Size Adaptor</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MFontSizeAdaptor> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MFont Size Adaptor</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MFontSizeAdaptor get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MFontSizeAdaptor result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MFont Size Adaptor</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MFontSizeAdaptor getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MFontSizeAdaptor result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MFont Size Adaptor</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MFontSizeAdaptor get(int value) {
		switch (value) {
		case MINUS_THREE_VALUE:
			return MINUS_THREE;
		case MINUS_TWO_VALUE:
			return MINUS_TWO;
		case MINUS_ONE_VALUE:
			return MINUS_ONE;
		case ZERO_VALUE:
			return ZERO;
		case PLUS_ONE_VALUE:
			return PLUS_ONE;
		case PLUS_TWO_VALUE:
			return PLUS_TWO;
		case PLUS_THREE_VALUE:
			return PLUS_THREE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private MFontSizeAdaptor(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // MFontSizeAdaptor
