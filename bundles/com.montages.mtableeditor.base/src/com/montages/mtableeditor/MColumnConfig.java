/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MClassifier;
import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MColumn Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getClassToCellConfig <em>Class To Cell Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getECName <em>EC Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getECLabel <em>EC Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getECWidth <em>EC Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getWidth <em>Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getMinimumWidth <em>Minimum Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getMaximumWidth <em>Maximum Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getContainingTableConfig <em>Containing Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MColumnConfig#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL aKindBaseDerive='let const1: String = \'Column\' in\nconst1\n'"
 * @generated
 */

public interface MColumnConfig extends MTableEditorElement, MElementWithFont {

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_Name()
	 * @model unsettable="true" required="true" annotation=
	 *        "http://www.montages.com/mCore/MCore mName='name'"
	 * @generated
	 */
	String getName();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MColumnConfig#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Class To Cell Config</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mtableeditor.MClassToCellConfig}.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MClassToCellConfig#getContainingColumnConfig <em>Containing Column Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class To Cell Config</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class To Cell Config</em>' containment reference list.
	 * @see #isSetClassToCellConfig()
	 * @see #unsetClassToCellConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_ClassToCellConfig()
	 * @see com.montages.mtableeditor.MClassToCellConfig#getContainingColumnConfig
	 * @model opposite="containingColumnConfig" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MClassToCellConfig> getClassToCellConfig();

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MColumnConfig#getClassToCellConfig
	 * <em>Class To Cell Config</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isSetClassToCellConfig()
	 * @see #getClassToCellConfig()
	 * @generated
	 */
	void unsetClassToCellConfig();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MColumnConfig#getClassToCellConfig
	 * <em>Class To Cell Config</em>}' containment reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>Class To Cell Config</em>'
	 *         containment reference list is set.
	 * @see #unsetClassToCellConfig()
	 * @see #getClassToCellConfig()
	 * @generated
	 */
	boolean isSetClassToCellConfig();

	/**
	 * Returns the value of the '<em><b>EC Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Name</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>EC Name</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_ECName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation=
	 *        "http://www.xocl.org/OCL derive='let resultofecName: String = if (let e0: Boolean = let chain01: String = name in\nif chain01.trim().oclIsUndefined() \n then null \n else chain01.trim()\n  endif = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen if containingTableConfig.intendedClass.oclIsUndefined()\n  then null\n  else containingTableConfig.intendedClass.name\nendif\n  else name\nendif in\nresultofecName\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special EditorConfig Settings'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getECName();

	/**
	 * Returns the value of the '<em><b>EC Label</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Label</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>EC Label</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_ECLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='label\n'" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special EditorConfig Settings'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getECLabel();

	/**
	 * Returns the value of the '<em><b>EC Width</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Width</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>EC Width</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_ECWidth()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='width\n'" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special EditorConfig Settings'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getECWidth();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #setLabel(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_Label()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Rendering'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	void unsetLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MColumnConfig#getLabel <em>Label</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Label</em>' attribute is set.
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	boolean isSetLabel();

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #isSetWidth()
	 * @see #unsetWidth()
	 * @see #setWidth(Integer)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_Width()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG propertyCategory='Rendering' createColumn='false'"
	 * @generated
	 */
	Integer getWidth();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #isSetWidth()
	 * @see #unsetWidth()
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetWidth()
	 * @see #getWidth()
	 * @see #setWidth(Integer)
	 * @generated
	 */
	void unsetWidth();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MColumnConfig#getWidth <em>Width</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Width</em>' attribute is set.
	 * @see #unsetWidth()
	 * @see #getWidth()
	 * @see #setWidth(Integer)
	 * @generated
	 */
	boolean isSetWidth();

	/**
	 * Returns the value of the '<em><b>Minimum Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minimum Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minimum Width</em>' attribute.
	 * @see #isSetMinimumWidth()
	 * @see #unsetMinimumWidth()
	 * @see #setMinimumWidth(Integer)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_MinimumWidth()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Rendering' createColumn='false'"
	 * @generated
	 */
	Integer getMinimumWidth();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getMinimumWidth <em>Minimum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum Width</em>' attribute.
	 * @see #isSetMinimumWidth()
	 * @see #unsetMinimumWidth()
	 * @see #getMinimumWidth()
	 * @generated
	 */

	void setMinimumWidth(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getMinimumWidth <em>Minimum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMinimumWidth()
	 * @see #getMinimumWidth()
	 * @see #setMinimumWidth(Integer)
	 * @generated
	 */
	void unsetMinimumWidth();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MColumnConfig#getMinimumWidth <em>Minimum Width</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Minimum Width</em>' attribute is set.
	 * @see #unsetMinimumWidth()
	 * @see #getMinimumWidth()
	 * @see #setMinimumWidth(Integer)
	 * @generated
	 */
	boolean isSetMinimumWidth();

	/**
	 * Returns the value of the '<em><b>Maximum Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maximum Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maximum Width</em>' attribute.
	 * @see #isSetMaximumWidth()
	 * @see #unsetMaximumWidth()
	 * @see #setMaximumWidth(Integer)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_MaximumWidth()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Rendering' createColumn='false'"
	 * @generated
	 */
	Integer getMaximumWidth();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getMaximumWidth <em>Maximum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Width</em>' attribute.
	 * @see #isSetMaximumWidth()
	 * @see #unsetMaximumWidth()
	 * @see #getMaximumWidth()
	 * @generated
	 */

	void setMaximumWidth(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MColumnConfig#getMaximumWidth <em>Maximum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMaximumWidth()
	 * @see #getMaximumWidth()
	 * @see #setMaximumWidth(Integer)
	 * @generated
	 */
	void unsetMaximumWidth();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MColumnConfig#getMaximumWidth <em>Maximum Width</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Maximum Width</em>' attribute is set.
	 * @see #unsetMaximumWidth()
	 * @see #getMaximumWidth()
	 * @see #setMaximumWidth(Integer)
	 * @generated
	 */
	boolean isSetMaximumWidth();

	/**
	 * Returns the value of the '<em><b>Containing Table Config</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MTableConfig#getColumnConfig <em>Column Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Table Config</em>' container
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Table Config</em>' container reference.
	 * @see #setContainingTableConfig(MTableConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_ContainingTableConfig()
	 * @see com.montages.mtableeditor.MTableConfig#getColumnConfig
	 * @model opposite="columnConfig" unsettable="true" transient="false"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structure' createColumn='false'"
	 * @generated
	 */
	MTableConfig getContainingTableConfig();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MColumnConfig#getContainingTableConfig
	 * <em>Containing Table Config</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Containing Table Config</em>'
	 *            container reference.
	 * @see #getContainingTableConfig()
	 * @generated
	 */
	void setContainingTableConfig(MTableConfig value);

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link com.montages.mtableeditor.MColumnConfigAction}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mtableeditor.MColumnConfigAction
	 * @see #setDoAction(MColumnConfigAction)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfig_DoAction()
	 * @model transient="true" volatile="true" derived="true" annotation=
	 *        "http://www.montages.com/mCore/MCore mName='do Action'"
	 *        annotation=
	 *        "http://www.xocl.org/OCL derive='mtableeditor::MColumnConfigAction::Do\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MColumnConfigAction getDoAction();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MColumnConfig#getDoAction
	 * <em>Do Action</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mtableeditor.MColumnConfigAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MColumnConfigAction value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MColumnConfigAction trg);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model mClassRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	MCellConfig cellConfigFromClass(MClassifier mClass);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(MColumnConfigAction mColumnConfigAction);

} // MColumnConfig
