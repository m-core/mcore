/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MProperty;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MRow Feature Cell</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getFeature <em>Feature</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getMaximumSize <em>Maximum Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getAverageSize <em>Average Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getMinimumSize <em>Minimum Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MRowFeatureCell#getSizeOfPropertyValue <em>Size Of Property Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL cellKindDerive='\'RowFeatureCell\'\n\n'"
 * @generated
 */

public interface MRowFeatureCell extends MCellConfig {

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #setFeature(MProperty)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_Feature()
	 * @model unsettable="true" required="true" annotation=
	 *        "http://www.xocl.org/OCL choiceConstraint='trg.containingClassifier = class'"
	 * @generated
	 */
	MProperty getFeature();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MRowFeatureCell#getFeature
	 * <em>Feature</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(MProperty value);

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MRowFeatureCell#getFeature
	 * <em>Feature</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isSetFeature()
	 * @see #getFeature()
	 * @see #setFeature(MProperty)
	 * @generated
	 */
	void unsetFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getFeature <em>Feature</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Feature</em>' reference is set.
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @see #setFeature(MProperty)
	 * @generated
	 */
	boolean isSetFeature();

	/**
	 * Returns the value of the '<em><b>Cell Edit Behavior</b></em>' attribute.
	 * The literals are from the enumeration
	 * {@link com.montages.mtableeditor.MCellEditBehaviorOption}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Edit Behavior</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Cell Edit Behavior</em>' attribute.
	 * @see com.montages.mtableeditor.MCellEditBehaviorOption
	 * @see #isSetCellEditBehavior()
	 * @see #unsetCellEditBehavior()
	 * @see #setCellEditBehavior(MCellEditBehaviorOption)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_CellEditBehavior()
	 * @model unsettable="true"
	 * @generated
	 */
	MCellEditBehaviorOption getCellEditBehavior();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Cell Edit Behavior</em>' attribute.
	 * @see com.montages.mtableeditor.MCellEditBehaviorOption
	 * @see #isSetCellEditBehavior()
	 * @see #unsetCellEditBehavior()
	 * @see #getCellEditBehavior()
	 * @generated
	 */
	void setCellEditBehavior(MCellEditBehaviorOption value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetCellEditBehavior()
	 * @see #getCellEditBehavior()
	 * @see #setCellEditBehavior(MCellEditBehaviorOption)
	 * @generated
	 */
	void unsetCellEditBehavior();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cell Edit Behavior</em>' attribute is set.
	 * @see #unsetCellEditBehavior()
	 * @see #getCellEditBehavior()
	 * @see #setCellEditBehavior(MCellEditBehaviorOption)
	 * @generated
	 */
	boolean isSetCellEditBehavior();

	/**
	 * Returns the value of the '<em><b>Cell Locate Behavior</b></em>'
	 * attribute. The literals are from the enumeration
	 * {@link com.montages.mtableeditor.MCellLocateBehaviorOption}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Locate Behavior</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Cell Locate Behavior</em>' attribute.
	 * @see com.montages.mtableeditor.MCellLocateBehaviorOption
	 * @see #isSetCellLocateBehavior()
	 * @see #unsetCellLocateBehavior()
	 * @see #setCellLocateBehavior(MCellLocateBehaviorOption)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_CellLocateBehavior()
	 * @model unsettable="true"
	 * @generated
	 */
	MCellLocateBehaviorOption getCellLocateBehavior();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Cell Locate Behavior</em>' attribute.
	 * @see com.montages.mtableeditor.MCellLocateBehaviorOption
	 * @see #isSetCellLocateBehavior()
	 * @see #unsetCellLocateBehavior()
	 * @see #getCellLocateBehavior()
	 * @generated
	 */
	void setCellLocateBehavior(MCellLocateBehaviorOption value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetCellLocateBehavior()
	 * @see #getCellLocateBehavior()
	 * @see #setCellLocateBehavior(MCellLocateBehaviorOption)
	 * @generated
	 */
	void unsetCellLocateBehavior();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MRowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cell Locate Behavior</em>' attribute is set.
	 * @see #unsetCellLocateBehavior()
	 * @see #getCellLocateBehavior()
	 * @see #setCellLocateBehavior(MCellLocateBehaviorOption)
	 * @generated
	 */
	boolean isSetCellLocateBehavior();

	/**
	 * Returns the value of the '<em><b>Maximum Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maximum Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maximum Size</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_MaximumSize()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='sizeOfPropertyValue->max()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Width Calculating'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getMaximumSize();

	/**
	 * Returns the value of the '<em><b>Average Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Average Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Average Size</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_AverageSize()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let finalCalculation: Real = let e1: Real = let chain11: OrderedSet(Integer)  = sizeOfPropertyValue->asOrderedSet() in\nif chain11->sum().oclIsUndefined() \n then null \n else chain11->sum()\n  endif / let chain12: OrderedSet(Integer)  = sizeOfPropertyValue->asOrderedSet() in\nif chain12->size().oclIsUndefined() \n then null \n else chain12->size()\n  endif in \n if e1.oclIsInvalid() then null else e1 endif in\nlet chain: Real = finalCalculation in\nif chain.round().oclIsUndefined() \n then null \n else chain.round()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Width Calculating'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getAverageSize();

	/**
	 * Returns the value of the '<em><b>Minimum Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minimum Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minimum Size</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_MinimumSize()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='sizeOfPropertyValue->min()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Width Calculating'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getMinimumSize();

	/**
	 * Returns the value of the '<em><b>Size Of Property Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Of Property Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Of Property Value</em>' attribute list.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMRowFeatureCell_SizeOfPropertyValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let potentialDataValue: OrderedSet(mcore::objects::MDataValue)  = let subchain1 : OrderedSet(mcore::objects::MObject)  = if feature.containingClassifier.oclIsUndefined()\n  then OrderedSet{}\n  else feature.containingClassifier.instance\nendif in \n if subchain1 = null \n  then null \n else subchain1.propertyInstance->reject(oclIsUndefined()).internalDataValue->reject(oclIsUndefined())->asOrderedSet() endif \n in\nlet correctDataValue: OrderedSet(mcore::objects::MDataValue)  = potentialDataValue->select(it: mcore::objects::MDataValue | let e0: Boolean = if it.containingPropertyInstance.oclIsUndefined()\n  then null\n  else it.containingPropertyInstance.property\nendif = feature in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet sequenceofSizes: Sequence(Integer)  = correctDataValue->collect(it: mcore::objects::MDataValue | it.dataValue.size())->asSequence()->excluding(null)->asSequence()  in\nlet choice: Sequence(Integer) = if (let e0: Boolean = if ((let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.kind\nendif = mcore::PropertyKind::Attribute in \n if e0.oclIsInvalid() then null else e0 endif))= false \n then false \n else if (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.propertyBehavior\nendif <> mcore::PropertyBehavior::Derived in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.simpleType\nendif <> mcore::SimpleType::None in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.kind\nendif = mcore::PropertyKind::Attribute in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.propertyBehavior\nendif <> mcore::PropertyBehavior::Derived in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.simpleType\nendif <> mcore::SimpleType::None in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen sequenceofSizes\n  else if feature.oclIsUndefined()\n  then null\n  else sequenceofSizes->append(feature.eLabel.size())\nendif\nendif in\nchoice\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Width Calculating'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<Integer> getSizeOfPropertyValue();

} // MRowFeatureCell
