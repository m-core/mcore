/**
 */
package com.montages.mtableeditor;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MEdit Provider Cell</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditProviderCell()
 * @model
 * @generated
 */

public interface MEditProviderCell extends MCellConfig {
} // MEditProviderCell
