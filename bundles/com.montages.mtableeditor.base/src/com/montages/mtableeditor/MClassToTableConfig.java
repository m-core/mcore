/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MClass To Table Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MClassToTableConfig#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MClassToTableConfig#getTableConfig <em>Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MClassToTableConfig#getIntendedPackage <em>Intended Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToTableConfig()
 * @model
 * @generated
 */

public interface MClassToTableConfig extends EObject {

	/**
	 * Returns the value of the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Class</em>' reference.
	 * @see #isSetClass()
	 * @see #unsetClass()
	 * @see #setClass(MClassifier)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToTableConfig_Class()
	 * @model unsettable="true" required="true" annotation=
	 *        "http://www.xocl.org/OCL choiceConstraint='let var1: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n'"
	 * @generated
	 */
	MClassifier getClass_();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' reference.
	 * @see #isSetClass()
	 * @see #unsetClass()
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetClass()
	 * @see #getClass_()
	 * @see #setClass(MClassifier)
	 * @generated
	 */
	void unsetClass();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getClass_ <em>Class</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Class</em>' reference is set.
	 * @see #unsetClass()
	 * @see #getClass_()
	 * @see #setClass(MClassifier)
	 * @generated
	 */
	boolean isSetClass();

	/**
	 * Returns the value of the '<em><b>Table Config</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table Config</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Table Config</em>' reference.
	 * @see #isSetTableConfig()
	 * @see #unsetTableConfig()
	 * @see #setTableConfig(MTableConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToTableConfig_TableConfig()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	MTableConfig getTableConfig();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getTableConfig <em>Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Table Config</em>' reference.
	 * @see #isSetTableConfig()
	 * @see #unsetTableConfig()
	 * @see #getTableConfig()
	 * @generated
	 */
	void setTableConfig(MTableConfig value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getTableConfig <em>Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetTableConfig()
	 * @see #getTableConfig()
	 * @see #setTableConfig(MTableConfig)
	 * @generated
	 */
	void unsetTableConfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getTableConfig <em>Table Config</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Table Config</em>' reference is set.
	 * @see #unsetTableConfig()
	 * @see #getTableConfig()
	 * @see #setTableConfig(MTableConfig)
	 * @generated
	 */
	boolean isSetTableConfig();

	/**
	 * Returns the value of the '<em><b>Intended Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intended Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intended Package</em>' reference.
	 * @see #isSetIntendedPackage()
	 * @see #unsetIntendedPackage()
	 * @see #setIntendedPackage(MPackage)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToTableConfig_IntendedPackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Entry Filtering' createColumn='false'"
	 * @generated
	 */
	MPackage getIntendedPackage();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getIntendedPackage <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Intended Package</em>' reference.
	 * @see #isSetIntendedPackage()
	 * @see #unsetIntendedPackage()
	 * @see #getIntendedPackage()
	 * @generated
	 */
	void setIntendedPackage(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getIntendedPackage <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetIntendedPackage()
	 * @see #getIntendedPackage()
	 * @see #setIntendedPackage(MPackage)
	 * @generated
	 */
	void unsetIntendedPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MClassToTableConfig#getIntendedPackage <em>Intended Package</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Intended Package</em>' reference is set.
	 * @see #unsetIntendedPackage()
	 * @see #getIntendedPackage()
	 * @see #setIntendedPackage(MPackage)
	 * @generated
	 */
	boolean isSetIntendedPackage();

} // MClassToTableConfig
