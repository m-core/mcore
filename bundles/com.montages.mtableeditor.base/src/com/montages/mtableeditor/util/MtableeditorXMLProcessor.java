/**
 */
package com.montages.mtableeditor.util;

import com.montages.mtableeditor.MtableeditorPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class MtableeditorXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MtableeditorXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		MtableeditorPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the MtableeditorResourceFactoryImpl factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new MtableeditorResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new MtableeditorResourceFactoryImpl());
		}
		return registrations;
	}

} // MtableeditorXMLProcessor
