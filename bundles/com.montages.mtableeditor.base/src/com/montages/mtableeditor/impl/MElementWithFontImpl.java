/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mtableeditor.MElementWithFont;
import com.montages.mtableeditor.MFontSizeAdaptor;
import com.montages.mtableeditor.MtableeditorPackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.resource.Resource.Internal;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.eclipse.ocl.util.TypeUtil;

import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.core.util.XoclMutlitypeComparisonUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MElement With Font</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MElementWithFontImpl#getBoldFont <em>Bold Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MElementWithFontImpl#getItalicFont <em>Italic Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MElementWithFontImpl#getFontSize <em>Font Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MElementWithFontImpl#getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MElementWithFontImpl extends MinimalEObjectImpl.Container
		implements MElementWithFont, IXoclInitializable {

	/**
	 * The default value of the '{@link #getBoldFont() <em>Bold Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBoldFont()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BOLD_FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBoldFont() <em>Bold Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBoldFont()
	 * @generated
	 * @ordered
	 */
	protected Boolean boldFont = BOLD_FONT_EDEFAULT;

	/**
	 * This is true if the Bold Font attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean boldFontESet;

	/**
	 * The default value of the '{@link #getItalicFont() <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getItalicFont()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ITALIC_FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getItalicFont() <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getItalicFont()
	 * @generated
	 * @ordered
	 */
	protected Boolean italicFont = ITALIC_FONT_EDEFAULT;

	/**
	 * This is true if the Italic Font attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean italicFontESet;

	/**
	 * The default value of the '{@link #getFontSize() <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFontSize()
	 * @generated
	 * @ordered
	 */
	protected static final MFontSizeAdaptor FONT_SIZE_EDEFAULT = MFontSizeAdaptor.MINUS_THREE;

	/**
	 * The cached value of the '{@link #getFontSize() <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFontSize()
	 * @generated
	 * @ordered
	 */
	protected MFontSizeAdaptor fontSize = FONT_SIZE_EDEFAULT;

	/**
	 * This is true if the Font Size attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fontSizeESet;

	/**
	 * The default value of the '{@link #getDerivedFontOptionsEncoding() <em>Derived Font Options Encoding</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDerivedFontOptionsEncoding()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}' property.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDerivedFontOptionsEncoding
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedFontOptionsEncodingDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MElementWithFontImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MELEMENT_WITH_FONT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MFontSizeAdaptor getFontSize() {
		return fontSize;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFontSize(MFontSizeAdaptor newFontSize) {
		MFontSizeAdaptor oldFontSize = fontSize;
		fontSize = newFontSize == null ? FONT_SIZE_EDEFAULT : newFontSize;
		boolean oldFontSizeESet = fontSizeESet;
		fontSizeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE,
					oldFontSize, fontSize, !oldFontSizeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFontSize() {
		MFontSizeAdaptor oldFontSize = fontSize;
		boolean oldFontSizeESet = fontSizeESet;
		fontSize = FONT_SIZE_EDEFAULT;
		fontSizeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE,
					oldFontSize, FONT_SIZE_EDEFAULT, oldFontSizeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFontSize() {
		return fontSizeESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBoldFont() {
		return boldFont;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setBoldFont(Boolean newBoldFont) {
		Boolean oldBoldFont = boldFont;
		boldFont = newBoldFont;
		boolean oldBoldFontESet = boldFontESet;
		boldFontESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT,
					oldBoldFont, boldFont, !oldBoldFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBoldFont() {
		Boolean oldBoldFont = boldFont;
		boolean oldBoldFontESet = boldFontESet;
		boldFont = BOLD_FONT_EDEFAULT;
		boldFontESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT,
					oldBoldFont, BOLD_FONT_EDEFAULT, oldBoldFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBoldFont() {
		return boldFontESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getItalicFont() {
		return italicFont;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setItalicFont(Boolean newItalicFont) {
		Boolean oldItalicFont = italicFont;
		italicFont = newItalicFont;
		boolean oldItalicFontESet = italicFontESet;
		italicFontESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT,
					oldItalicFont, italicFont, !oldItalicFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItalicFont() {
		Boolean oldItalicFont = italicFont;
		boolean oldItalicFontESet = italicFontESet;
		italicFont = ITALIC_FONT_EDEFAULT;
		italicFontESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT,
					oldItalicFont, ITALIC_FONT_EDEFAULT, oldItalicFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItalicFont() {
		return italicFontESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedFontOptionsEncoding() {
		/**
		 * @OCL ''
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MELEMENT_WITH_FONT;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING;

		if (derivedFontOptionsEncodingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				derivedFontOptionsEncodingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MELEMENT_WITH_FONT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedFontOptionsEncodingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MELEMENT_WITH_FONT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
			return getBoldFont();
		case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
			return getItalicFont();
		case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
			return getFontSize();
		case MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING:
			return getDerivedFontOptionsEncoding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
			setBoldFont((Boolean) newValue);
			return;
		case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
			setItalicFont((Boolean) newValue);
			return;
		case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
			setFontSize((MFontSizeAdaptor) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
			unsetBoldFont();
			return;
		case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
			unsetItalicFont();
			return;
		case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
			unsetFontSize();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
			return isSetBoldFont();
		case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
			return isSetItalicFont();
		case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
			return isSetFontSize();
		case MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING:
			return DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT == null ? getDerivedFontOptionsEncoding() != null
					: !DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT.equals(getDerivedFontOptionsEncoding());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (boldFont: ");
		if (boldFontESet)
			result.append(boldFont);
		else
			result.append("<unset>");
		result.append(", italicFont: ");
		if (italicFontESet)
			result.append(italicFont);
		else
			result.append("<unset>");
		result.append(", fontSize: ");
		if (fontSizeESet)
			result.append(fontSize);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer, int newContainerFeatureID,
			NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__FONT_SIZE };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures
					.toArray(new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder",
					"InitOrder", true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures, new Comparator<EStructuralFeature>() {
				public int compare(EStructuralFeature structuralFeature1, EStructuralFeature structuralFeature2) {
					Object comparedObject1 = initOrderMap.get(structuralFeature1);
					Object comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return XoclMutlitypeComparisonUtil.compare(comparedObject1, comparedObject2);
				}
			});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue",
					"InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MELEMENT_WITH_FONT,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this, new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature, eClass(), annotationKey,
				annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, oclText, helper.getProblems(),
						eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} // MElementWithFontImpl
