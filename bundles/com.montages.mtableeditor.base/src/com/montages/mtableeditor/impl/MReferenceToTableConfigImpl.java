/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MProperty;
import com.montages.mtableeditor.MReferenceToTableConfig;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MtableeditorPackage;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.langlets.autil.AutilPackage;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MReference To Table Config</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MReferenceToTableConfigImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MReferenceToTableConfigImpl#getTableConfig <em>Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MReferenceToTableConfigImpl#getLabel <em>Label</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MReferenceToTableConfigImpl extends MTableEditorElementImpl implements MReferenceToTableConfig {

	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected MProperty reference;

	/**
	 * This is true if the Reference reference has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean referenceESet;

	/**
	 * The cached value of the '{@link #getTableConfig() <em>Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTableConfig()
	 * @generated
	 * @ordered
	 */
	protected MTableConfig tableConfig;

	/**
	 * This is true if the Table Config reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean tableConfigESet;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * This is true if the Label attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean labelESet;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '
	 * {@link #getReference <em>Reference</em>}' property. Is combined with the
	 * choice construction definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getReference
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression referenceChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase
	 * <em>AKind Base</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getAKindBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MReferenceToTableConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getReference() {
		if (reference != null && reference.eIsProxy()) {
			InternalEObject oldReference = (InternalEObject) reference;
			reference = (MProperty) eResolveProxy(oldReference);
			if (reference != oldReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE, oldReference, reference));
			}
		}
		return reference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetReference() {
		return reference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(MProperty newReference) {
		MProperty oldReference = reference;
		reference = newReference;
		boolean oldReferenceESet = referenceESet;
		referenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE, oldReference, reference,
					!oldReferenceESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReference() {
		MProperty oldReference = reference;
		boolean oldReferenceESet = referenceESet;
		reference = null;
		referenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE, oldReference, null, oldReferenceESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReference() {
		return referenceESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Reference</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MProperty
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let var1: Boolean = let e1: Boolean = trg.kind = mcore::PropertyKind::Reference in 
	if e1.oclIsInvalid() then null else e1 endif in
	var1
	
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalReferenceChoiceConstraint(MProperty trg) {
		EClass eClass = MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG;
		if (referenceChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				referenceChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "ReferenceChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(referenceChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "ReferenceChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig getTableConfig() {
		if (tableConfig != null && tableConfig.eIsProxy()) {
			InternalEObject oldTableConfig = (InternalEObject) tableConfig;
			tableConfig = (MTableConfig) eResolveProxy(oldTableConfig);
			if (tableConfig != oldTableConfig) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, oldTableConfig, tableConfig));
			}
		}
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig basicGetTableConfig() {
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTableConfig(MTableConfig newTableConfig) {
		MTableConfig oldTableConfig = tableConfig;
		tableConfig = newTableConfig;
		boolean oldTableConfigESet = tableConfigESet;
		tableConfigESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, oldTableConfig, tableConfig,
					!oldTableConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTableConfig() {
		MTableConfig oldTableConfig = tableConfig;
		boolean oldTableConfigESet = tableConfigESet;
		tableConfig = null;
		tableConfigESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, oldTableConfig, null,
					oldTableConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTableConfig() {
		return tableConfigESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		boolean oldLabelESet = labelESet;
		labelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__LABEL,
					oldLabel, label, !oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLabel() {
		String oldLabel = label;
		boolean oldLabelESet = labelESet;
		label = LABEL_EDEFAULT;
		labelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__LABEL, oldLabel, LABEL_EDEFAULT, oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLabel() {
		return labelESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE:
			if (resolve)
				return getReference();
			return basicGetReference();
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG:
			if (resolve)
				return getTableConfig();
			return basicGetTableConfig();
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__LABEL:
			return getLabel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE:
			setReference((MProperty) newValue);
			return;
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG:
			setTableConfig((MTableConfig) newValue);
			return;
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__LABEL:
			setLabel((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE:
			unsetReference();
			return;
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG:
			unsetTableConfig();
			return;
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__LABEL:
			unsetLabel();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__REFERENCE:
			return isSetReference();
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG:
			return isSetTableConfig();
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG__LABEL:
			return isSetLabel();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (label: ");
		if (labelESet)
			result.append(label);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL aKindBase let const1: String = 'Ref -> Table Config' in
	const1
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAKindBase() {
		EClass eClass = (MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG);
		EStructuralFeature eOverrideFeature = AutilPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} // MReferenceToTableConfigImpl
