/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.McorePackage;
import com.montages.mtableeditor.MCellConfig;
import com.montages.mtableeditor.MCellEditBehaviorOption;
import com.montages.mtableeditor.MCellLocateBehaviorOption;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MClassToTableConfig;
import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MColumnConfigAction;
import com.montages.mtableeditor.MEditProviderCell;
import com.montages.mtableeditor.MEditorConfig;
import com.montages.mtableeditor.MEditorConfigAction;
import com.montages.mtableeditor.MElementWithFont;
import com.montages.mtableeditor.MFontSizeAdaptor;
import com.montages.mtableeditor.MOclCell;
import com.montages.mtableeditor.MReferenceToTableConfig;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MTableConfigAction;
import com.montages.mtableeditor.MTableEditorElement;
import com.montages.mtableeditor.MtableeditorFactory;
import com.montages.mtableeditor.MtableeditorPackage;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.langlets.autil.AutilPackage;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class MtableeditorPackageImpl extends EPackageImpl implements MtableeditorPackage {

	/**
	 * The cached OCL constraint restricting the classes of root elements. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc --> <!--
	 *            end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 *         <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails().get("rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID, choiceConstraint, helper.getProblems(),
						MtableeditorPackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTableEditorElementEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mEditorConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTableConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mColumnConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mClassToCellConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mCellConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mRowFeatureCellEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mOclCellEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mEditProviderCellEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mReferenceToTableConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mClassToTableConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mElementWithFontEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mCellEditBehaviorOptionEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mCellLocateBehaviorOptionEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mFontSizeAdaptorEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mEditorConfigActionEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mTableConfigActionEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mColumnConfigActionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mtableeditor.MtableeditorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MtableeditorPackageImpl() {
		super(eNS_URI, MtableeditorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link MtableeditorPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MtableeditorPackage init() {
		if (isInited)
			return (MtableeditorPackage) EPackage.Registry.INSTANCE.getEPackage(MtableeditorPackage.eNS_URI);

		// Obtain or create and register package
		MtableeditorPackageImpl theMtableeditorPackage = (MtableeditorPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof MtableeditorPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new MtableeditorPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AutilPackage.eINSTANCE.eClass();
		McorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theMtableeditorPackage.createPackageContents();

		// Initialize created meta-data
		theMtableeditorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMtableeditorPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MtableeditorPackage.eNS_URI, theMtableeditorPackage);
		return theMtableeditorPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTableEditorElement() {
		return mTableEditorElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableEditorElement_Kind() {
		return (EAttribute) mTableEditorElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMEditorConfig() {
		return mEditorConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMEditorConfig_HeaderTableConfig() {
		return (EReference) mEditorConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMEditorConfig_MTableConfig() {
		return (EReference) mEditorConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMEditorConfig_MColumnConfig() {
		return (EReference) mEditorConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMEditorConfig_ClassToTableConfig() {
		return (EReference) mEditorConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMEditorConfig_SuperConfig() {
		return (EReference) mEditorConfigEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMEditorConfig_DoAction() {
		return (EAttribute) mEditorConfigEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMEditorConfig__DoAction$Update__MEditorConfigAction() {
		return mEditorConfigEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMEditorConfig__TableConfigFromClass__MClassifier() {
		return mEditorConfigEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMEditorConfig__DoActionUpdate__MEditorConfigAction() {
		return mEditorConfigEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMEditorConfig__ResetEditor() {
		return mEditorConfigEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTableConfig() {
		return mTableConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_Name() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_ColumnConfig() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_ReferenceToTableConfig() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_IntendedPackage() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_IntendedClass() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_ECName() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_ECLabel() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_ECColumnConfig() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_ContainingEditorConfig() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_Label() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_DisplayDefaultColumn() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_DisplayHeader() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_DoAction() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_IntendedAction() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_ComplementAction() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_SplitChildrenAction() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTableConfig_MergeChildrenAction() {
		return (EAttribute) mTableConfigEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTableConfig_ComplementActionTable() {
		return (EReference) mTableConfigEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__DoAction$Update__MTableConfigAction() {
		return mTableConfigEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__TableConfigFromReference__MProperty() {
		return mTableConfigEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__GetAllProperties() {
		return mTableConfigEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__DoActionUpdate__MTableConfigAction() {
		return mTableConfigEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean() {
		return mTableConfigEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction() {
		return mTableConfigEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTableConfig__InvokeSetDoAction__MTableConfigAction() {
		return mTableConfigEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMColumnConfig() {
		return mColumnConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_Name() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMColumnConfig_ClassToCellConfig() {
		return (EReference) mColumnConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_ECName() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_ECLabel() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_ECWidth() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_Label() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_Width() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_MinimumWidth() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_MaximumWidth() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMColumnConfig_ContainingTableConfig() {
		return (EReference) mColumnConfigEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMColumnConfig_DoAction() {
		return (EAttribute) mColumnConfigEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMColumnConfig__DoAction$Update__MColumnConfigAction() {
		return mColumnConfigEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMColumnConfig__CellConfigFromClass__MClassifier() {
		return mColumnConfigEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMColumnConfig__DoActionUpdate__MColumnConfigAction() {
		return mColumnConfigEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMClassToCellConfig() {
		return mClassToCellConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassToCellConfig_Class() {
		return (EReference) mClassToCellConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassToCellConfig_CellConfig() {
		return (EReference) mClassToCellConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassToCellConfig_ContainingColumnConfig() {
		return (EReference) mClassToCellConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassToCellConfig_CellKind() {
		return (EAttribute) mClassToCellConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMCellConfig() {
		return mCellConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMCellConfig_ExplicitHighlightOCL() {
		return (EAttribute) mCellConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCellConfig_ContainingClassToCellConfig() {
		return (EReference) mCellConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMRowFeatureCell() {
		return mRowFeatureCellEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRowFeatureCell_Feature() {
		return (EReference) mRowFeatureCellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRowFeatureCell_CellEditBehavior() {
		return (EAttribute) mRowFeatureCellEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRowFeatureCell_CellLocateBehavior() {
		return (EAttribute) mRowFeatureCellEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRowFeatureCell_MaximumSize() {
		return (EAttribute) mRowFeatureCellEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRowFeatureCell_AverageSize() {
		return (EAttribute) mRowFeatureCellEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRowFeatureCell_MinimumSize() {
		return (EAttribute) mRowFeatureCellEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRowFeatureCell_SizeOfPropertyValue() {
		return (EAttribute) mRowFeatureCellEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMOclCell() {
		return mOclCellEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMOclCell_StringRendering() {
		return (EAttribute) mOclCellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMEditProviderCell() {
		return mEditProviderCellEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMReferenceToTableConfig() {
		return mReferenceToTableConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMReferenceToTableConfig_Reference() {
		return (EReference) mReferenceToTableConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMReferenceToTableConfig_TableConfig() {
		return (EReference) mReferenceToTableConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMReferenceToTableConfig_Label() {
		return (EAttribute) mReferenceToTableConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMClassToTableConfig() {
		return mClassToTableConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassToTableConfig_Class() {
		return (EReference) mClassToTableConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassToTableConfig_TableConfig() {
		return (EReference) mClassToTableConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassToTableConfig_IntendedPackage() {
		return (EReference) mClassToTableConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMElementWithFont() {
		return mElementWithFontEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMElementWithFont_FontSize() {
		return (EAttribute) mElementWithFontEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMElementWithFont_BoldFont() {
		return (EAttribute) mElementWithFontEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMElementWithFont_ItalicFont() {
		return (EAttribute) mElementWithFontEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMElementWithFont_DerivedFontOptionsEncoding() {
		return (EAttribute) mElementWithFontEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMCellEditBehaviorOption() {
		return mCellEditBehaviorOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMCellLocateBehaviorOption() {
		return mCellLocateBehaviorOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMFontSizeAdaptor() {
		return mFontSizeAdaptorEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMEditorConfigAction() {
		return mEditorConfigActionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMTableConfigAction() {
		return mTableConfigActionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMColumnConfigAction() {
		return mColumnConfigActionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MtableeditorFactory getMtableeditorFactory() {
		return (MtableeditorFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mTableEditorElementEClass = createEClass(MTABLE_EDITOR_ELEMENT);
		createEAttribute(mTableEditorElementEClass, MTABLE_EDITOR_ELEMENT__KIND);

		mEditorConfigEClass = createEClass(MEDITOR_CONFIG);
		createEReference(mEditorConfigEClass, MEDITOR_CONFIG__HEADER_TABLE_CONFIG);
		createEReference(mEditorConfigEClass, MEDITOR_CONFIG__MTABLE_CONFIG);
		createEReference(mEditorConfigEClass, MEDITOR_CONFIG__MCOLUMN_CONFIG);
		createEReference(mEditorConfigEClass, MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG);
		createEReference(mEditorConfigEClass, MEDITOR_CONFIG__SUPER_CONFIG);
		createEAttribute(mEditorConfigEClass, MEDITOR_CONFIG__DO_ACTION);
		createEOperation(mEditorConfigEClass, MEDITOR_CONFIG___DO_ACTION$_UPDATE__MEDITORCONFIGACTION);
		createEOperation(mEditorConfigEClass, MEDITOR_CONFIG___TABLE_CONFIG_FROM_CLASS__MCLASSIFIER);
		createEOperation(mEditorConfigEClass, MEDITOR_CONFIG___DO_ACTION_UPDATE__MEDITORCONFIGACTION);
		createEOperation(mEditorConfigEClass, MEDITOR_CONFIG___RESET_EDITOR);

		mTableConfigEClass = createEClass(MTABLE_CONFIG);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__NAME);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__COLUMN_CONFIG);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__INTENDED_PACKAGE);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__INTENDED_CLASS);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__EC_NAME);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__EC_LABEL);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__EC_COLUMN_CONFIG);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG);
		createEReference(mTableConfigEClass, MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__LABEL);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__DISPLAY_HEADER);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__DO_ACTION);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__INTENDED_ACTION);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__COMPLEMENT_ACTION);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__SPLIT_CHILDREN_ACTION);
		createEAttribute(mTableConfigEClass, MTABLE_CONFIG__MERGE_CHILDREN_ACTION);
		createEOperation(mTableConfigEClass, MTABLE_CONFIG___DO_ACTION$_UPDATE__MTABLECONFIGACTION);
		createEOperation(mTableConfigEClass, MTABLE_CONFIG___TABLE_CONFIG_FROM_REFERENCE__MPROPERTY);
		createEOperation(mTableConfigEClass, MTABLE_CONFIG___GET_ALL_PROPERTIES);
		createEOperation(mTableConfigEClass, MTABLE_CONFIG___DO_ACTION_UPDATE__MTABLECONFIGACTION);
		createEOperation(mTableConfigEClass,
				MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MEDITORCONFIG_MTABLECONFIGACTION_ELIST_BOOLEAN);
		createEOperation(mTableConfigEClass, MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MTABLECONFIG_MTABLECONFIGACTION);
		createEOperation(mTableConfigEClass, MTABLE_CONFIG___INVOKE_SET_DO_ACTION__MTABLECONFIGACTION);

		mColumnConfigEClass = createEClass(MCOLUMN_CONFIG);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__NAME);
		createEReference(mColumnConfigEClass, MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__EC_NAME);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__EC_LABEL);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__EC_WIDTH);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__LABEL);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__WIDTH);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__MINIMUM_WIDTH);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__MAXIMUM_WIDTH);
		createEReference(mColumnConfigEClass, MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG);
		createEAttribute(mColumnConfigEClass, MCOLUMN_CONFIG__DO_ACTION);
		createEOperation(mColumnConfigEClass, MCOLUMN_CONFIG___DO_ACTION$_UPDATE__MCOLUMNCONFIGACTION);
		createEOperation(mColumnConfigEClass, MCOLUMN_CONFIG___CELL_CONFIG_FROM_CLASS__MCLASSIFIER);
		createEOperation(mColumnConfigEClass, MCOLUMN_CONFIG___DO_ACTION_UPDATE__MCOLUMNCONFIGACTION);

		mClassToCellConfigEClass = createEClass(MCLASS_TO_CELL_CONFIG);
		createEReference(mClassToCellConfigEClass, MCLASS_TO_CELL_CONFIG__CLASS);
		createEReference(mClassToCellConfigEClass, MCLASS_TO_CELL_CONFIG__CELL_CONFIG);
		createEReference(mClassToCellConfigEClass, MCLASS_TO_CELL_CONFIG__CONTAINING_COLUMN_CONFIG);
		createEAttribute(mClassToCellConfigEClass, MCLASS_TO_CELL_CONFIG__CELL_KIND);

		mCellConfigEClass = createEClass(MCELL_CONFIG);
		createEAttribute(mCellConfigEClass, MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL);
		createEReference(mCellConfigEClass, MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG);

		mRowFeatureCellEClass = createEClass(MROW_FEATURE_CELL);
		createEReference(mRowFeatureCellEClass, MROW_FEATURE_CELL__FEATURE);
		createEAttribute(mRowFeatureCellEClass, MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR);
		createEAttribute(mRowFeatureCellEClass, MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR);
		createEAttribute(mRowFeatureCellEClass, MROW_FEATURE_CELL__MAXIMUM_SIZE);
		createEAttribute(mRowFeatureCellEClass, MROW_FEATURE_CELL__AVERAGE_SIZE);
		createEAttribute(mRowFeatureCellEClass, MROW_FEATURE_CELL__MINIMUM_SIZE);
		createEAttribute(mRowFeatureCellEClass, MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE);

		mOclCellEClass = createEClass(MOCL_CELL);
		createEAttribute(mOclCellEClass, MOCL_CELL__STRING_RENDERING);

		mEditProviderCellEClass = createEClass(MEDIT_PROVIDER_CELL);

		mReferenceToTableConfigEClass = createEClass(MREFERENCE_TO_TABLE_CONFIG);
		createEReference(mReferenceToTableConfigEClass, MREFERENCE_TO_TABLE_CONFIG__REFERENCE);
		createEReference(mReferenceToTableConfigEClass, MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG);
		createEAttribute(mReferenceToTableConfigEClass, MREFERENCE_TO_TABLE_CONFIG__LABEL);

		mClassToTableConfigEClass = createEClass(MCLASS_TO_TABLE_CONFIG);
		createEReference(mClassToTableConfigEClass, MCLASS_TO_TABLE_CONFIG__CLASS);
		createEReference(mClassToTableConfigEClass, MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG);
		createEReference(mClassToTableConfigEClass, MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE);

		mElementWithFontEClass = createEClass(MELEMENT_WITH_FONT);
		createEAttribute(mElementWithFontEClass, MELEMENT_WITH_FONT__BOLD_FONT);
		createEAttribute(mElementWithFontEClass, MELEMENT_WITH_FONT__ITALIC_FONT);
		createEAttribute(mElementWithFontEClass, MELEMENT_WITH_FONT__FONT_SIZE);
		createEAttribute(mElementWithFontEClass, MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING);

		// Create enums
		mEditorConfigActionEEnum = createEEnum(MEDITOR_CONFIG_ACTION);
		mTableConfigActionEEnum = createEEnum(MTABLE_CONFIG_ACTION);
		mColumnConfigActionEEnum = createEEnum(MCOLUMN_CONFIG_ACTION);
		mCellEditBehaviorOptionEEnum = createEEnum(MCELL_EDIT_BEHAVIOR_OPTION);
		mCellLocateBehaviorOptionEEnum = createEEnum(MCELL_LOCATE_BEHAVIOR_OPTION);
		mFontSizeAdaptorEEnum = createEEnum(MFONT_SIZE_ADAPTOR);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AutilPackage theAutilPackage = (AutilPackage) EPackage.Registry.INSTANCE.getEPackage(AutilPackage.eNS_URI);
		McorePackage theMcorePackage = (McorePackage) EPackage.Registry.INSTANCE.getEPackage(McorePackage.eNS_URI);
		SemanticsPackage theSemanticsPackage = (SemanticsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SemanticsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mTableEditorElementEClass.getESuperTypes().add(theAutilPackage.getAElement());
		mEditorConfigEClass.getESuperTypes().add(theMcorePackage.getMEditor());
		mEditorConfigEClass.getESuperTypes().add(this.getMTableEditorElement());
		mTableConfigEClass.getESuperTypes().add(this.getMTableEditorElement());
		mColumnConfigEClass.getESuperTypes().add(this.getMTableEditorElement());
		mColumnConfigEClass.getESuperTypes().add(this.getMElementWithFont());
		mClassToCellConfigEClass.getESuperTypes().add(this.getMTableEditorElement());
		mCellConfigEClass.getESuperTypes().add(this.getMTableEditorElement());
		mCellConfigEClass.getESuperTypes().add(this.getMClassToCellConfig());
		mCellConfigEClass.getESuperTypes().add(this.getMElementWithFont());
		mRowFeatureCellEClass.getESuperTypes().add(this.getMCellConfig());
		mOclCellEClass.getESuperTypes().add(this.getMCellConfig());
		mEditProviderCellEClass.getESuperTypes().add(this.getMCellConfig());
		mReferenceToTableConfigEClass.getESuperTypes().add(this.getMTableEditorElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(mTableEditorElementEClass, MTableEditorElement.class, "MTableEditorElement", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMTableEditorElement_Kind(), ecorePackage.getEString(), "kind", null, 0, 1,
				MTableEditorElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(mEditorConfigEClass, MEditorConfig.class, "MEditorConfig", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMEditorConfig_HeaderTableConfig(), this.getMTableConfig(), null, "headerTableConfig", null, 0,
				1, MEditorConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMEditorConfig_MTableConfig(), this.getMTableConfig(),
				this.getMTableConfig_ContainingEditorConfig(), "mTableConfig", null, 0, -1, MEditorConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMEditorConfig_MColumnConfig(), this.getMColumnConfig(), null, "mColumnConfig", null, 0, -1,
				MEditorConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMEditorConfig_ClassToTableConfig(), this.getMClassToTableConfig(), null, "classToTableConfig",
				null, 0, -1, MEditorConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMEditorConfig_SuperConfig(), this.getMEditorConfig(), null, "superConfig", null, 0, 1,
				MEditorConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMEditorConfig_DoAction(), this.getMEditorConfigAction(), "doAction", null, 0, 1,
				MEditorConfig.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMEditorConfig__DoAction$Update__MEditorConfigAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMEditorConfigAction(), "trg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMEditorConfig__TableConfigFromClass__MClassifier(), this.getMTableConfig(),
				"tableConfigFromClass", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMClassifier(), "mClass", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMEditorConfig__DoActionUpdate__MEditorConfigAction(), theSemanticsPackage.getXUpdate(),
				"doActionUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMEditorConfigAction(), "mEditorConfigAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMEditorConfig__ResetEditor(), ecorePackage.getEBooleanObject(), "resetEditor", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mTableConfigEClass, MTableConfig.class, "MTableConfig", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMTableConfig_Name(), ecorePackage.getEString(), "name", null, 1, 1, MTableConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_ColumnConfig(), this.getMColumnConfig(),
				this.getMColumnConfig_ContainingTableConfig(), "columnConfig", null, 0, -1, MTableConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_ReferenceToTableConfig(), this.getMReferenceToTableConfig(), null,
				"referenceToTableConfig", null, 0, -1, MTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_IntendedPackage(), theMcorePackage.getMPackage(), null, "intendedPackage", null,
				0, 1, MTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_IntendedClass(), theMcorePackage.getMClassifier(), null, "intendedClass", null,
				0, 1, MTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_ECName(), ecorePackage.getEString(), "eCName", null, 0, 1, MTableConfig.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_ECLabel(), ecorePackage.getEString(), "eCLabel", null, 0, 1, MTableConfig.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_ECColumnConfig(), this.getMColumnConfig(), null, "eCColumnConfig", null, 0, -1,
				MTableConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_ContainingEditorConfig(), this.getMEditorConfig(),
				this.getMEditorConfig_MTableConfig(), "containingEditorConfig", null, 0, 1, MTableConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMTableConfig_ComplementActionTable(), this.getMTableConfig(), null, "complementActionTable",
				null, 0, -1, MTableConfig.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_Label(), ecorePackage.getEString(), "label", null, 0, 1, MTableConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_DisplayDefaultColumn(), ecorePackage.getEBooleanObject(), "displayDefaultColumn",
				null, 0, 1, MTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_DisplayHeader(), ecorePackage.getEBooleanObject(), "displayHeader", null, 0, 1,
				MTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_DoAction(), this.getMTableConfigAction(), "doAction", null, 0, 1,
				MTableConfig.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_IntendedAction(), this.getMTableConfigAction(), "intendedAction", null, 0, -1,
				MTableConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_ComplementAction(), ecorePackage.getEBooleanObject(), "complementAction", null,
				0, 1, MTableConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_SplitChildrenAction(), ecorePackage.getEBooleanObject(), "splitChildrenAction",
				null, 0, 1, MTableConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTableConfig_MergeChildrenAction(), ecorePackage.getEBooleanObject(), "mergeChildrenAction",
				null, 0, 1, MTableConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMTableConfig__DoAction$Update__MTableConfigAction(), theSemanticsPackage.getXUpdate(),
				"doAction$Update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfigAction(), "trg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMTableConfig__TableConfigFromReference__MProperty(), this.getMTableConfig(),
				"tableConfigFromReference", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "reference", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMTableConfig__GetAllProperties(), theMcorePackage.getMProperty(), "getAllProperties", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMTableConfig__DoActionUpdate__MTableConfigAction(), theSemanticsPackage.getXUpdate(),
				"doActionUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfigAction(), "mTableConfigAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theSemanticsPackage.getXUpdate(), "xUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMEditorConfig(), "mEditorConfig", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfigAction(), "mTableConfigAction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfig(), "complementTable", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "reset", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theSemanticsPackage.getXUpdate(), "xUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfig(), "mTableConfig", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfigAction(), "mTableConfigAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMTableConfig__InvokeSetDoAction__MTableConfigAction(), ecorePackage.getEJavaObject(),
				"invokeSetDoAction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMTableConfigAction(), "mTableConfigAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mColumnConfigEClass, MColumnConfig.class, "MColumnConfig", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMColumnConfig_Name(), ecorePackage.getEString(), "name", null, 1, 1, MColumnConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMColumnConfig_ClassToCellConfig(), this.getMClassToCellConfig(),
				this.getMClassToCellConfig_ContainingColumnConfig(), "classToCellConfig", null, 0, -1,
				MColumnConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_ECName(), ecorePackage.getEString(), "eCName", null, 0, 1, MColumnConfig.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_ECLabel(), ecorePackage.getEString(), "eCLabel", null, 0, 1,
				MColumnConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_ECWidth(), ecorePackage.getEIntegerObject(), "eCWidth", null, 0, 1,
				MColumnConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_Label(), ecorePackage.getEString(), "label", null, 0, 1, MColumnConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_Width(), ecorePackage.getEIntegerObject(), "width", null, 0, 1,
				MColumnConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_MinimumWidth(), ecorePackage.getEIntegerObject(), "minimumWidth", null, 0, 1,
				MColumnConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_MaximumWidth(), ecorePackage.getEIntegerObject(), "maximumWidth", null, 0, 1,
				MColumnConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMColumnConfig_ContainingTableConfig(), this.getMTableConfig(),
				this.getMTableConfig_ColumnConfig(), "containingTableConfig", null, 0, 1, MColumnConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMColumnConfig_DoAction(), this.getMColumnConfigAction(), "doAction", null, 0, 1,
				MColumnConfig.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMColumnConfig__DoAction$Update__MColumnConfigAction(), theSemanticsPackage.getXUpdate(),
				"doAction$Update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMColumnConfigAction(), "trg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMColumnConfig__CellConfigFromClass__MClassifier(), this.getMCellConfig(),
				"cellConfigFromClass", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMClassifier(), "mClass", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMColumnConfig__DoActionUpdate__MColumnConfigAction(), theSemanticsPackage.getXUpdate(),
				"doActionUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMColumnConfigAction(), "mColumnConfigAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mClassToCellConfigEClass, MClassToCellConfig.class, "MClassToCellConfig", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMClassToCellConfig_Class(), theMcorePackage.getMClassifier(), null, "class", null, 1, 1,
				MClassToCellConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassToCellConfig_CellConfig(), this.getMCellConfig(),
				this.getMCellConfig_ContainingClassToCellConfig(), "cellConfig", null, 1, 1, MClassToCellConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMClassToCellConfig_ContainingColumnConfig(), this.getMColumnConfig(),
				this.getMColumnConfig_ClassToCellConfig(), "containingColumnConfig", null, 0, 1,
				MClassToCellConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassToCellConfig_CellKind(), ecorePackage.getEString(), "cellKind", null, 0, 1,
				MClassToCellConfig.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(mCellConfigEClass, MCellConfig.class, "MCellConfig", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMCellConfig_ExplicitHighlightOCL(), ecorePackage.getEString(), "explicitHighlightOCL", null,
				0, 1, MCellConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMCellConfig_ContainingClassToCellConfig(), this.getMClassToCellConfig(),
				this.getMClassToCellConfig_CellConfig(), "containingClassToCellConfig", null, 0, 1, MCellConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mRowFeatureCellEClass, MRowFeatureCell.class, "MRowFeatureCell", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMRowFeatureCell_Feature(), theMcorePackage.getMProperty(), null, "feature", null, 1, 1,
				MRowFeatureCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRowFeatureCell_CellEditBehavior(), this.getMCellEditBehaviorOption(), "cellEditBehavior",
				null, 0, 1, MRowFeatureCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRowFeatureCell_CellLocateBehavior(), this.getMCellLocateBehaviorOption(),
				"cellLocateBehavior", null, 0, 1, MRowFeatureCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRowFeatureCell_MaximumSize(), ecorePackage.getEIntegerObject(), "maximumSize", null, 0, 1,
				MRowFeatureCell.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRowFeatureCell_AverageSize(), ecorePackage.getEIntegerObject(), "averageSize", null, 0, 1,
				MRowFeatureCell.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRowFeatureCell_MinimumSize(), ecorePackage.getEIntegerObject(), "minimumSize", null, 0, 1,
				MRowFeatureCell.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRowFeatureCell_SizeOfPropertyValue(), ecorePackage.getEIntegerObject(),
				"sizeOfPropertyValue", null, 0, -1, MRowFeatureCell.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mOclCellEClass, MOclCell.class, "MOclCell", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMOclCell_StringRendering(), ecorePackage.getEString(), "stringRendering", null, 1, 1,
				MOclCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mEditProviderCellEClass, MEditProviderCell.class, "MEditProviderCell", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mReferenceToTableConfigEClass, MReferenceToTableConfig.class, "MReferenceToTableConfig",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMReferenceToTableConfig_Reference(), theMcorePackage.getMProperty(), null, "reference", null,
				1, 1, MReferenceToTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMReferenceToTableConfig_TableConfig(), this.getMTableConfig(), null, "tableConfig", null, 0,
				1, MReferenceToTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMReferenceToTableConfig_Label(), ecorePackage.getEString(), "label", null, 0, 1,
				MReferenceToTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mClassToTableConfigEClass, MClassToTableConfig.class, "MClassToTableConfig", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMClassToTableConfig_Class(), theMcorePackage.getMClassifier(), null, "class", null, 1, 1,
				MClassToTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassToTableConfig_TableConfig(), this.getMTableConfig(), null, "tableConfig", null, 1, 1,
				MClassToTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassToTableConfig_IntendedPackage(), theMcorePackage.getMPackage(), null, "intendedPackage",
				null, 0, 1, MClassToTableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mElementWithFontEClass, MElementWithFont.class, "MElementWithFont", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMElementWithFont_BoldFont(), ecorePackage.getEBooleanObject(), "boldFont", null, 0, 1,
				MElementWithFont.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMElementWithFont_ItalicFont(), ecorePackage.getEBooleanObject(), "italicFont", null, 0, 1,
				MElementWithFont.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMElementWithFont_FontSize(), this.getMFontSizeAdaptor(), "fontSize", null, 0, 1,
				MElementWithFont.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMElementWithFont_DerivedFontOptionsEncoding(), ecorePackage.getEString(),
				"derivedFontOptionsEncoding", null, 0, 1, MElementWithFont.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(mEditorConfigActionEEnum, MEditorConfigAction.class, "MEditorConfigAction");
		addEEnumLiteral(mEditorConfigActionEEnum, MEditorConfigAction.DO);
		addEEnumLiteral(mEditorConfigActionEEnum, MEditorConfigAction.ADD_TABLE);
		addEEnumLiteral(mEditorConfigActionEEnum, MEditorConfigAction.RESET_EDITOR);

		initEEnum(mTableConfigActionEEnum, MTableConfigAction.class, "MTableConfigAction");
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.DO);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.UNSET_INTENDED_PACKAGE);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.UNSET_INTENDED_CLASS);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.ADD_EMPTY_COLUMN);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.ADD_COLUMN_WITH_ROW);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.ADD_COLUMN_WITH_OCL);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.ADD_TABLE_REF);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.COMPLEMENT_TABLE);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.MERGE_ABSTRACTION);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.SPLIT_SPECIALIZATIONS);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.MERGE_CONTAINER);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.SPLIT_CHILDREN);
		addEEnumLiteral(mTableConfigActionEEnum, MTableConfigAction.MERGE_CHILDREN);

		initEEnum(mColumnConfigActionEEnum, MColumnConfigAction.class, "MColumnConfigAction");
		addEEnumLiteral(mColumnConfigActionEEnum, MColumnConfigAction.DO);
		addEEnumLiteral(mColumnConfigActionEEnum, MColumnConfigAction.ADD_MROW_FEATURE_CELL);
		addEEnumLiteral(mColumnConfigActionEEnum, MColumnConfigAction.ADD_MOCL_CELL);
		addEEnumLiteral(mColumnConfigActionEEnum, MColumnConfigAction.ADD_MEDIT_PROVIDER_CELL);

		initEEnum(mCellEditBehaviorOptionEEnum, MCellEditBehaviorOption.class, "MCellEditBehaviorOption");
		addEEnumLiteral(mCellEditBehaviorOptionEEnum, MCellEditBehaviorOption.SELECTION);
		addEEnumLiteral(mCellEditBehaviorOptionEEnum, MCellEditBehaviorOption.PULL_DOWN);
		addEEnumLiteral(mCellEditBehaviorOptionEEnum, MCellEditBehaviorOption.READ_ONLY);

		initEEnum(mCellLocateBehaviorOptionEEnum, MCellLocateBehaviorOption.class, "MCellLocateBehaviorOption");
		addEEnumLiteral(mCellLocateBehaviorOptionEEnum, MCellLocateBehaviorOption.NONE);
		addEEnumLiteral(mCellLocateBehaviorOptionEEnum, MCellLocateBehaviorOption.SELECTION);

		initEEnum(mFontSizeAdaptorEEnum, MFontSizeAdaptor.class, "MFontSizeAdaptor");
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.MINUS_THREE);
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.MINUS_TWO);
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.MINUS_ONE);
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.ZERO);
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.PLUS_ONE);
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.PLUS_TWO);
		addEEnumLiteral(mFontSizeAdaptorEEnum, MFontSizeAdaptor.PLUS_THREE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/mdcm/v2
		createV2Annotations();
		// http://www.xocl.org/EXPRESSION_OCL
		createEXPRESSION_OCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source, new String[] { "rootConstraint", "trg.name = \'MEditorConfig\'" });
		addAnnotation(getMTableEditorElement_Kind(), source, new String[] { "derive", "aRenderedKind\n" });
		addAnnotation(mEditorConfigEClass, source, new String[] { "label",
				"let fileType: String = \'mtableeditor\' in\nlet fileName: String = if (let chain: mcore::MPackage = if headerTableConfig.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedPackage\nendif in\nif chain <> null then true else false \n  endif) \n  =true \nthen if headerTableConfig.intendedPackage.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedPackage.eName\nendif else if (let chain: mcore::MClassifier = if headerTableConfig.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedClass\nendif in\nif chain <> null then true else false \n  endif)=true then let subchain1 : mcore::MPackage = if headerTableConfig.intendedClass.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedClass.containingPackage\nendif in \n if subchain1 = null \n  then null \n else subchain1.eName endif \n\n  else let subchain1 : mtableeditor::MClassToCellConfig = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = if headerTableConfig.oclIsUndefined()\n  then OrderedSet{}\n  else headerTableConfig.columnConfig.classToCellConfig->asOrderedSet()\nendif in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif in \n if subchain1 = null \n  then null \n else if subchain1.class.containingPackage.oclIsUndefined()\n  then null\n  else subchain1.class.containingPackage.eName\nendif endif \n\nendif endif in\nlet label: String = let e1: String = fileName.concat(\'.\').concat(fileType) in \n if e1.oclIsInvalid() then null else e1 endif in\nlabel\n" });
		addAnnotation(getMEditorConfig__DoAction$Update__MEditorConfigAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMEditorConfig__TableConfigFromClass__MClassifier(), source, new String[] { "body",
				"let applicableClassToTableConfig: OrderedSet(mtableeditor::MClassToTableConfig)  = classToTableConfig->asOrderedSet()->select(it: mtableeditor::MClassToTableConfig | let e0: Boolean = it.class = mClass in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet chain: OrderedSet(mtableeditor::MTableConfig)  = applicableClassToTableConfig.tableConfig->asOrderedSet() in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n" });
		addAnnotation(getMEditorConfig__DoActionUpdate__MEditorConfigAction(), source, new String[] { "body", "null" });
		addAnnotation(getMEditorConfig__ResetEditor(), source, new String[] { "body", "true\n" });
		addAnnotation(getMEditorConfig_DoAction(), source,
				new String[] { "derive", "mtableeditor::MEditorConfigAction::Do\n" });
		addAnnotation(mTableConfigEClass, source, new String[] { "label", "name\n" });
		addAnnotation(getMTableConfig__DoAction$Update__MTableConfigAction(), source, new String[] { "body", "null" });
		addAnnotation(getMTableConfig__TableConfigFromReference__MProperty(), source,
				new String[] { "body", "null\n" });
		addAnnotation(getMTableConfig__GetAllProperties(), source, new String[] { "body",
				"let allRef: OrderedSet(mcore::MProperty)  = let chain: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in\nchain->iterate(i:mcore::MProperty; r: OrderedSet(mcore::MProperty)=OrderedSet{} | if i.oclIsKindOf(mcore::MProperty) then r->including(i.oclAsType(mcore::MProperty))->asOrderedSet() \n else r endif) in\nlet allRowFeatureCell: OrderedSet(mtableeditor::MRowFeatureCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() \n else r endif) in\nlet var2: OrderedSet(mcore::MProperty)  = let e1: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet()->union(allRef) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif in\nvar2\n" });
		addAnnotation(getMTableConfig__DoActionUpdate__MTableConfigAction(), source, new String[] { "body", "null" });
		addAnnotation(getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean(), source,
				new String[] { "body", "null" });
		addAnnotation(getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMTableConfig__InvokeSetDoAction__MTableConfigAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMTableConfig_IntendedClass(), source, new String[] { "choiceConstraint",
				"let rightPackage: Boolean = if (let chain: mcore::MPackage = intendedPackage in\nif chain <> null then true else false \n  endif) \n  =true \nthen (let e0: Boolean = trg.containingPackage = intendedPackage in \n if e0.oclIsInvalid() then null else e0 endif)\n  else true\nendif in\nlet isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in \n if e1.oclIsInvalid() then null else e1 endif in\nlet intended: Boolean = let e1: Boolean = if (isClass)= false \n then false \n else if (rightPackage)= false \n then false \nelse if ((isClass)= null or (rightPackage)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nintended\n" });
		addAnnotation(getMTableConfig_ECName(), source, new String[] { "derive",
				"let resultofecName: String = if (let e0: Boolean = let chain01: String = name in\nif chain01.trim().oclIsUndefined() \n then null \n else chain01.trim()\n  endif = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen if intendedClass.oclIsUndefined()\n  then null\n  else intendedClass.name\nendif\n  else name\nendif in\nresultofecName\n" });
		addAnnotation(getMTableConfig_ECLabel(), source, new String[] { "derive", "label\n" });
		addAnnotation(getMTableConfig_ECColumnConfig(), source,
				new String[] { "derive", "columnConfig->asOrderedSet()\n" });
		addAnnotation(getMTableConfig_DoAction(), source, new String[] { "derive",
				"mtableeditor::MTableConfigAction::Do\n", "choiceConstruction",
				"let var0: OrderedSet(MTableConfigAction) = intendedAction in var0\r\n->append(MTableConfigAction::AddEmptyColumn)\r\n->append(MTableConfigAction::AddColumnWithRow)\r\n->append(MTableConfigAction::AddColumnWithOcl)\r\n->append(MTableConfigAction::AddTableRef)\r\n->prepend(MTableConfigAction::Do)" });
		addAnnotation(getMTableConfig_IntendedAction(), source, new String[] { "derive",
				"if self.intendedClass.oclIsUndefined()\r\n   then if self.intendedPackage.oclIsUndefined()\r\n   \t\t\t\tthen OrderedSet{}\r\n   \t\t\t\telse OrderedSet{MTableConfigAction::UnsetIntendedPackage}\r\n   \t\t\t\tendif\r\n   else\r\n   let e1: OrderedSet(mtableeditor::MTableConfigAction) = \r\n   let e0: OrderedSet(mtableeditor::MTableConfigAction) = if self.intendedPackage.oclIsUndefined()\r\n   \t\t\t\tthen OrderedSet{MTableConfigAction::UnsetIntendedClass}\r\n   \t\t\t\telse OrderedSet{MTableConfigAction::UnsetIntendedClass, MTableConfigAction::UnsetIntendedPackage}\r\n   \t\t\t\tendif   \t\t\t\t\r\n   \t\t\t\tin if self.columnConfig->isEmpty() and self.referenceToTableConfig->isEmpty() and true\r\n   \t\t\t\t\tthen e0\r\n   \t\t\t\t\telse\r\n   \t\t\t\t\t\tlet e6: OrderedSet(mtableeditor::MTableConfigAction) = \r\n   \t\t\t\t\t\tlet e5: OrderedSet(mtableeditor::MTableConfigAction) = \r\n   \t\t\t\t\t\tlet e4: OrderedSet(mtableeditor::MTableConfigAction) = \r\n  \t\t\t\t\t\tlet e3: OrderedSet(mtableeditor::MTableConfigAction) = \r\n  \t\t\t\t\t\tlet e2: OrderedSet(mtableeditor::MTableConfigAction) = e0\r\n  \t\t\t\t\t\tin if self.intendedClass.allDirectSubTypes()->isEmpty()\r\n   \t\t\t\t\t\t\tthen e2\r\n   \t\t\t\t\t\t\telse e2->prepend(MTableConfigAction::SplitSpecializations)\r\n   \t\t\t\t\t\t\tendif\r\n  \t\t\t\t\t\tin if self.intendedClass.allSuperTypes()->isEmpty()\r\n   \t\t\t\t\t\t\tthen e3\r\n   \t\t\t\t\t\t\telse e3->prepend(MTableConfigAction::MergeAbstraction)\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\t\tin if self.splitChildrenAction = true\r\n   \t\t\t\t\t\t\tthen e4->prepend(MTableConfigAction::SplitChildren)\r\n   \t\t\t\t\t\t\telse e4\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\t\tin if self.mergeChildrenAction = true\r\n   \t\t\t\t\t\t\tthen e5->prepend(MTableConfigAction::MergeChildren)\r\n   \t\t\t\t\t\t\telse e5\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\t\tin if self.intendedClass.classescontainedin->isEmpty()\r\n   \t\t\t\t\t\t\tthen e6\r\n   \t\t\t\t\t\t\telse e6->prepend(MTableConfigAction::MergeContainer)\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\tendif   \t\t\t\t\t\r\n   \t\t\t\t\tin if self.complementAction = false\r\n   \t\t\t\t\tthen e1\r\n   \t\t\t\t\telse e1->prepend(MTableConfigAction::ComplementTable)\r\n   \t\t\t\t\tendif\r\n   endif" });
		addAnnotation(getMTableConfig_ComplementAction(), source, new String[] { "derive",
				"let includesAllContainments: Boolean = if (let e0: Boolean = if (let e0: Boolean = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet()->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif)   in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let chain01: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= false \n then false \nelse if ((let e0: Boolean = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet()->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif)   in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen false\n  else true\nendif in\nlet hasContainments: Boolean = if (let e0: Boolean = if (true)= false \n then false \n else if (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= false \n then false \nelse if ((true)= null or (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen includesAllContainments\n  else false\nendif in\nlet allRowFeatureCell: OrderedSet(mtableeditor::MRowFeatureCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() \n else r endif) in\nlet includesAllProperties: Boolean = if (let e0: Boolean = if (let e0: Boolean = allRowFeatureCell.feature->asOrderedSet()->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() )   in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let chain01: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= false \n then false \nelse if ((let e0: Boolean = allRowFeatureCell.feature->asOrderedSet()->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() )   in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen hasContainments\n  else true\nendif in\nlet hasProperties: Boolean = if (let e0: Boolean = if (true)= false \n then false \n else if (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= false \n then false \nelse if ((true)= null or (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen includesAllProperties\n  else hasContainments\nendif in\nlet resultofComplementAction: Boolean = if (let e0: Boolean = if ( intendedClass.oclIsUndefined())= true \n then true \n else if ( intendedClass.oclIsInvalid())= true \n then true \nelse if (( intendedClass.oclIsUndefined())= null or ( intendedClass.oclIsInvalid())= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen false\n  else hasProperties\nendif in\nresultofComplementAction\n" });
		addAnnotation(getMTableConfig_SplitChildrenAction(), source, new String[] { "derive",
				"let propertiesChildClass: OrderedSet(mcore::MProperty)  = getAllProperties()->asOrderedSet()->select(it: mcore::MProperty | let e0: Boolean = intendedClass <> it.containingClassifier in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet allOclCell: OrderedSet(mtableeditor::MOclCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MOclCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MOclCell) then r->including(i.oclAsType(mtableeditor::MOclCell))->asOrderedSet() \n else r endif)->select(it: mtableeditor::MOclCell | let e0: Boolean = it.class <> intendedClass in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet containsChildren: Boolean = if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen false else if (let e0: Boolean = if (let chain01: OrderedSet(mcore::MProperty)  = propertiesChildClass in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= true \n then true \n else if (let chain02: OrderedSet(mtableeditor::MOclCell)  = allOclCell in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= true \n then true \nelse if ((let chain01: OrderedSet(mcore::MProperty)  = propertiesChildClass in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null or (let chain02: OrderedSet(mtableeditor::MOclCell)  = allOclCell in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif in\ncontainsChildren\n" });
		addAnnotation(getMTableConfig_MergeChildrenAction(), source, new String[] { "derive",
				"let containments: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nlet allChildClasses: OrderedSet(mcore::MClassifier)  = containments.type->asOrderedSet() in\nlet propertiesofChildren: OrderedSet(mcore::MProperty)  = getAllProperties()->asOrderedSet()->select(it: mcore::MProperty | let e0: Boolean = it.containingClassifier <> intendedClass in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet resultofMergeChildren: Boolean = if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen false else if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif)=true then true\n  else let e0: Boolean = not(let e0: Boolean = propertiesofChildren->includesAll(allChildClasses.allNonContainmentFeatures()->asOrderedSet())   in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif\nendif endif in\nresultofMergeChildren\n" });
		addAnnotation(getMColumnConfig__DoAction$Update__MColumnConfigAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMColumnConfig__CellConfigFromClass__MClassifier(), source, new String[] { "body", "null\n" });
		addAnnotation(getMColumnConfig__DoActionUpdate__MColumnConfigAction(), source, new String[] { "body", "null" });
		addAnnotation(getMColumnConfig_ECName(), source, new String[] { "derive",
				"let resultofecName: String = if (let e0: Boolean = let chain01: String = name in\nif chain01.trim().oclIsUndefined() \n then null \n else chain01.trim()\n  endif = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen if containingTableConfig.intendedClass.oclIsUndefined()\n  then null\n  else containingTableConfig.intendedClass.name\nendif\n  else name\nendif in\nresultofecName\n" });
		addAnnotation(getMColumnConfig_ECLabel(), source, new String[] { "derive", "label\n" });
		addAnnotation(getMColumnConfig_ECWidth(), source, new String[] { "derive",
				"let rowAverageSize: OrderedSet(Integer)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() \n else r endif)->collect(it: mtableeditor::MRowFeatureCell | it.averageSize)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet rowMaxSize: OrderedSet(Integer)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() \n else r endif)->collect(it: mtableeditor::MRowFeatureCell | it.maximumSize)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet choice: Integer = if (let e0: Boolean = (let e0: Integer = (rowMaxSize->max() / 2).round() in \n if e0.oclIsInvalid() then null else e0 endif) > rowAverageSize->max() in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen (let e0: Integer = rowAverageSize->max() * 13 in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: Integer = rowMaxSize->max() * 13 in \n if e0.oclIsInvalid() then null else e0 endif)\nendif in\nlet noMaxandMin: Integer = if (let e0: Boolean = choice <= minimumWidth in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen minimumWidth\n  else choice\nendif in\nlet maxandNoMin: Integer = if (let e0: Boolean = choice <= maximumWidth in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen choice\n  else maximumWidth\nendif in\nlet maxandMin: Integer = if (let e0: Boolean = choice >= maximumWidth in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen maximumWidth else if (let e0: Boolean = choice <= minimumWidth in \n if e0.oclIsInvalid() then null else e0 endif)=true then minimumWidth\n  else choice\nendif endif in\nlet maxorMin: Integer = if (let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= false \n then false \n else if (let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= null or (let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen maxandNoMin\n  else noMaxandMin\nendif in\nlet decision: Integer = if (let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= false \n then false \n else if (let chain02: Integer = minimumWidth in\nif chain02 <> null then true else false \n  endif)= false \n then false \n else if (let e0: Boolean = if (let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = not(let chain01: Integer = minimumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = not(let chain01: Integer = minimumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= null or (let chain02: Integer = minimumWidth in\nif chain02 <> null then true else false \n  endif)= null or (let e0: Boolean = if (let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = not(let chain01: Integer = minimumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = not(let chain01: Integer = minimumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen maxandMin\n  else maxorMin\nendif in\nlet oclDecision: Integer = if (let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= false \n then false \n else if (let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = if ( width.oclIsUndefined())= true \n then true \n else if (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= true \n then true \nelse if (( width.oclIsUndefined())= null or (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= null or (let e0: Boolean = not(let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if ( width.oclIsUndefined())= true \n then true \n else if (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= true \n then true \nelse if (( width.oclIsUndefined())= null or (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen maximumWidth else if (let e0: Boolean = if (let chain01: Integer = minimumWidth in\nif chain01 <> null then true else false \n  endif)= false \n then false \n else if (let e0: Boolean = not(let chain01: Integer = minimumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = if ( width.oclIsUndefined())= true \n then true \n else if (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= true \n then true \nelse if (( width.oclIsUndefined())= null or (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let chain01: Integer = minimumWidth in\nif chain01 <> null then true else false \n  endif)= null or (let e0: Boolean = not(let chain01: Integer = minimumWidth in\nif chain01 = 0 then true else false \n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if ( width.oclIsUndefined())= true \n then true \n else if (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= true \n then true \nelse if (( width.oclIsUndefined())= null or (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then minimumWidth\n  else width\nendif endif in\nlet resultofECWidth: Integer = if (let e0: Boolean = if ((let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= true \n then true \n else if (let chain02: Integer = minimumWidth in\nif chain02 <> null then true else false \n  endif)= true \n then true \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= null or (let chain02: Integer = minimumWidth in\nif chain02 <> null then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif))= false \n then false \n else if (let e0: Boolean = not((let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif)= false \n then false \n else if (let chain02: Integer = minimumWidth in\nif chain02 = 0 then true else false \n  endif)= false \n then false \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif)= null or (let chain02: Integer = minimumWidth in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)) in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = if ( width.oclIsUndefined())= true \n then true \n else if (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= true \n then true \nelse if (( width.oclIsUndefined())= null or (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let chain01: OrderedSet(Integer)  = rowMaxSize in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= false \n then false \n else if (let chain02: OrderedSet(Integer)  = rowAverageSize in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= false \n then false \nelse if ((let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= true \n then true \n else if (let chain02: Integer = minimumWidth in\nif chain02 <> null then true else false \n  endif)= true \n then true \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 <> null then true else false \n  endif)= null or (let chain02: Integer = minimumWidth in\nif chain02 <> null then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = not((let e0: Boolean = if (let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif)= false \n then false \n else if (let chain02: Integer = minimumWidth in\nif chain02 = 0 then true else false \n  endif)= false \n then false \nelse if ((let chain01: Integer = maximumWidth in\nif chain01 = 0 then true else false \n  endif)= null or (let chain02: Integer = minimumWidth in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)) in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if ( width.oclIsUndefined())= true \n then true \n else if (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= true \n then true \nelse if (( width.oclIsUndefined())= null or (let chain02: Integer = width in\nif chain02 = 0 then true else false \n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(Integer)  = rowMaxSize in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null or (let chain02: OrderedSet(Integer)  = rowAverageSize in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen decision else if (let e0: Boolean = classToCellConfig->asOrderedSet()->select(it: mtableeditor::MClassToCellConfig | let e0: Boolean = it.cellKind = \'OclCell\' in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->notEmpty() in \n if e0.oclIsInvalid() then null else e0 endif)=true then oclDecision\n  else width\nendif endif in\nresultofECWidth\n" });
		addAnnotation(getMColumnConfig_DoAction(), source,
				new String[] { "derive", "mtableeditor::MColumnConfigAction::Do\n" });
		addAnnotation(getMClassToCellConfig_Class(), source, new String[] { "initValue",
				"if containingColumnConfig.containingTableConfig.oclIsUndefined()\n  then null\n  else containingColumnConfig.containingTableConfig.intendedClass\nendif\n",
				"choiceConstraint",
				"let rightPackage: Boolean = if (let chain: mcore::MPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()\n  then null\n  else containingColumnConfig.containingTableConfig.intendedPackage\nendif in\nif chain <> null then true else false \n  endif) \n  =true \nthen (let e0: Boolean = trg.containingPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()\n  then null\n  else containingColumnConfig.containingTableConfig.intendedPackage\nendif in \n if e0.oclIsInvalid() then null else e0 endif)\n  else true\nendif in\nlet isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in \n if e1.oclIsInvalid() then null else e1 endif in\nlet intended: Boolean = let e1: Boolean = if (isClass)= false \n then false \n else if (rightPackage)= false \n then false \nelse if ((isClass)= null or (rightPackage)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nintended\n" });
		addAnnotation(getMClassToCellConfig_CellKind(), source, new String[] { "derive",
				"if cellConfig.oclIsUndefined()\n  then null\n  else cellConfig.cellKind\nendif\n" });
		addAnnotation(getMRowFeatureCell_Feature(), source, new String[] { "choiceConstraint",
				"trg.containingClassifier = class or trg.containingClassifier.allSuperTypes()->includes(class) or\r\ntrg.containingClassifier.allSubTypes()->includes(class)" });
		addAnnotation(getMRowFeatureCell_MaximumSize(), source,
				new String[] { "derive", "sizeOfPropertyValue->max()" });
		addAnnotation(getMRowFeatureCell_AverageSize(), source, new String[] { "derive",
				"let finalCalculation: Real = let e1: Real = let chain11: OrderedSet(Integer)  = sizeOfPropertyValue->asOrderedSet() in\nif chain11->sum().oclIsUndefined() \n then null \n else chain11->sum()\n  endif / let chain12: OrderedSet(Integer)  = sizeOfPropertyValue->asOrderedSet() in\nif chain12->size().oclIsUndefined() \n then null \n else chain12->size()\n  endif in \n if e1.oclIsInvalid() then null else e1 endif in\nlet chain: Real = finalCalculation in\nif chain.round().oclIsUndefined() \n then null \n else chain.round()\n  endif\n" });
		addAnnotation(getMRowFeatureCell_MinimumSize(), source,
				new String[] { "derive", "sizeOfPropertyValue->min()" });
		addAnnotation(getMRowFeatureCell_SizeOfPropertyValue(), source, new String[] { "derive",
				"let potentialDataValue: OrderedSet(mcore::objects::MDataValue)  = let subchain1 : OrderedSet(mcore::objects::MObject)  = if feature.containingClassifier.oclIsUndefined()\n  then OrderedSet{}\n  else feature.containingClassifier.instance\nendif in \n if subchain1 = null \n  then null \n else subchain1.propertyInstance->reject(oclIsUndefined()).internalDataValue->reject(oclIsUndefined())->asOrderedSet() endif \n in\nlet correctDataValue: OrderedSet(mcore::objects::MDataValue)  = potentialDataValue->select(it: mcore::objects::MDataValue | let e0: Boolean = if it.containingPropertyInstance.oclIsUndefined()\n  then null\n  else it.containingPropertyInstance.property\nendif = feature in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet sequenceofSizes: Sequence(Integer)  = correctDataValue->collect(it: mcore::objects::MDataValue | it.dataValue.size())->asSequence()->excluding(null)->asSequence()  in\nlet choice: Sequence(Integer) = if (let e0: Boolean = if ((let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.kind\nendif = mcore::PropertyKind::Attribute in \n if e0.oclIsInvalid() then null else e0 endif))= false \n then false \n else if (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.propertyBehavior\nendif <> mcore::PropertyBehavior::Derived in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.simpleType\nendif <> mcore::SimpleType::None in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.kind\nendif = mcore::PropertyKind::Attribute in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.propertyBehavior\nendif <> mcore::PropertyBehavior::Derived in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if feature.oclIsUndefined()\n  then null\n  else feature.simpleType\nendif <> mcore::SimpleType::None in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen sequenceofSizes\n  else if feature.oclIsUndefined()\n  then null\n  else sequenceofSizes->append(feature.eLabel.size())\nendif\nendif in\nchoice\n" });
		addAnnotation(getMReferenceToTableConfig_Reference(), source, new String[] { "choiceConstraint",
				"let var1: Boolean = let e1: Boolean = trg.kind = mcore::PropertyKind::Reference in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n" });
		addAnnotation(getMClassToTableConfig_Class(), source, new String[] { "choiceConstraint",
				"let var1: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n" });
		addAnnotation(getMElementWithFont_FontSize(), source, new String[] { "initValue",
				"let adaptor: mtableeditor::MFontSizeAdaptor = mtableeditor::MFontSizeAdaptor::Zero in\nadaptor\n" });
		addAnnotation(getMElementWithFont_DerivedFontOptionsEncoding(), source, new String[] { "derive", "\'\'\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source, new String[] { "hideAdvancedProperties", "null" });
		addAnnotation(getMEditorConfig__TableConfigFromClass__MClassifier(), source,
				new String[] { "propertyCategory", "Additional Concepts", "createColumn", "true" });
		addAnnotation(getMEditorConfig__DoActionUpdate__MEditorConfigAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn", "true" });
		addAnnotation(getMEditorConfig_ClassToTableConfig(), source,
				new String[] { "propertyCategory", "Additional Concepts", "createColumn", "true" });
		addAnnotation(getMEditorConfig_SuperConfig(), source,
				new String[] { "propertyCategory", "Additional Concepts", "createColumn", "false" });
		addAnnotation(getMEditorConfig_DoAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMTableConfig__GetAllProperties(), source,
				new String[] { "propertyCategory", "Structure", "createColumn", "true" });
		addAnnotation(getMTableConfig__DoActionUpdate__MTableConfigAction(), source, new String[] {});
		addAnnotation(getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean(), source,
				new String[] {});
		addAnnotation(getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn", "true" });
		addAnnotation(getMTableConfig__InvokeSetDoAction__MTableConfigAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn", "true" });
		addAnnotation(getMTableConfig_IntendedPackage(), source,
				new String[] { "propertyCategory", "Entry Filtering", "createColumn", "false" });
		addAnnotation(getMTableConfig_IntendedClass(), source,
				new String[] { "propertyCategory", "Entry Filtering", "createColumn", "false" });
		addAnnotation(getMTableConfig_ECName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special Editor Config Settings" });
		addAnnotation(getMTableConfig_ECLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special Editor Config Settings" });
		addAnnotation(getMTableConfig_ECColumnConfig(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special Editor Config Settings" });
		addAnnotation(getMTableConfig_ContainingEditorConfig(), source,
				new String[] { "propertyCategory", "Structure", "createColumn", "false" });
		addAnnotation(getMTableConfig_ComplementActionTable(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Structure" });
		addAnnotation(getMTableConfig_Label(), source,
				new String[] { "createColumn", "true", "propertyCategory", "Rendering" });
		addAnnotation(getMTableConfig_DisplayDefaultColumn(), source,
				new String[] { "propertyCategory", "Rendering", "createColumn", "false" });
		addAnnotation(getMTableConfig_DisplayHeader(), source,
				new String[] { "propertyCategory", "Rendering", "createColumn", "false" });
		addAnnotation(getMTableConfig_DoAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMTableConfig_IntendedAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMTableConfig_ComplementAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMTableConfig_SplitChildrenAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMTableConfig_MergeChildrenAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMColumnConfig__DoActionUpdate__MColumnConfigAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn", "true" });
		addAnnotation(getMColumnConfig_ECName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special EditorConfig Settings" });
		addAnnotation(getMColumnConfig_ECLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special EditorConfig Settings" });
		addAnnotation(getMColumnConfig_ECWidth(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Special EditorConfig Settings" });
		addAnnotation(getMColumnConfig_Label(), source,
				new String[] { "createColumn", "true", "propertyCategory", "Rendering" });
		addAnnotation(getMColumnConfig_Width(), source,
				new String[] { "propertyCategory", "Rendering", "createColumn", "false" });
		addAnnotation(getMColumnConfig_MinimumWidth(), source,
				new String[] { "propertyCategory", "Rendering", "createColumn", "false" });
		addAnnotation(getMColumnConfig_MaximumWidth(), source,
				new String[] { "propertyCategory", "Rendering", "createColumn", "false" });
		addAnnotation(getMColumnConfig_ContainingTableConfig(), source,
				new String[] { "propertyCategory", "Structure", "createColumn", "false" });
		addAnnotation(getMColumnConfig_DoAction(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMClassToCellConfig_ContainingColumnConfig(), source,
				new String[] { "propertyCategory", "Structure", "createColumn", "false" });
		addAnnotation(getMClassToCellConfig_CellKind(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Kind" });
		addAnnotation(getMCellConfig_ContainingClassToCellConfig(), source,
				new String[] { "propertyCategory", "Structure", "createColumn", "false" });
		addAnnotation(getMRowFeatureCell_MaximumSize(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Width Calculating" });
		addAnnotation(getMRowFeatureCell_AverageSize(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Width Calculating" });
		addAnnotation(getMRowFeatureCell_MinimumSize(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Width Calculating" });
		addAnnotation(getMRowFeatureCell_SizeOfPropertyValue(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Width Calculating" });
		addAnnotation(getMReferenceToTableConfig_Label(), source,
				new String[] { "createColumn", "true", "propertyCategory", "Rendering" });
		addAnnotation(getMClassToTableConfig_IntendedPackage(), source,
				new String[] { "propertyCategory", "Entry Filtering", "createColumn", "false" });
		addAnnotation(getMElementWithFont_BoldFont(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Font" });
		addAnnotation(getMElementWithFont_ItalicFont(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Font" });
		addAnnotation(getMElementWithFont_FontSize(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Font" });
		addAnnotation(getMElementWithFont_DerivedFontOptionsEncoding(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Font" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mEditorConfigEClass, source,
				new String[] { "aKindBaseDerive", "let const1: String = \'Editor Config\' in\nconst1\n" });
		addAnnotation(mTableConfigEClass, source,
				new String[] { "aKindBaseDerive", "let const1: String = \'Table\' in\nconst1\n" });
		addAnnotation(mColumnConfigEClass, source,
				new String[] { "aKindBaseDerive", "let const1: String = \'Column\' in\nconst1\n" });
		addAnnotation(mClassToCellConfigEClass, source,
				new String[] { "aKindBaseDerive", "let const1: String = \'Class -> ...\' in\nconst1\n" });
		addAnnotation(mCellConfigEClass, source,
				new String[] { "aKindBaseDerive", "let const1: String = \'Cell\' in\nconst1\n" });
		addAnnotation(mRowFeatureCellEClass, source, new String[] { "cellKindDerive", "\'RowFeatureCell\'\n\n" });
		addAnnotation(mOclCellEClass, source, new String[] { "cellKindDerive", "\'OclCell\'\n\n" });
		addAnnotation(mReferenceToTableConfigEClass, source,
				new String[] { "aKindBaseDerive", "let const1: String = \'Ref -> Table Config\' in\nconst1\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(getMEditorConfig__DoActionUpdate__MEditorConfigAction(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation(getMEditorConfig__ResetEditor(), source, new String[] { "mName", "resetEditor" });
		addAnnotation(getMEditorConfig_MTableConfig(), source, new String[] { "mName", "MTableConfig" });
		addAnnotation(getMEditorConfig_DoAction(), source, new String[] { "mName", "do Action" });
		addAnnotation(mEditorConfigActionEEnum.getELiterals().get(1), source, new String[] { "mName", "AddTable" });
		addAnnotation(getMTableConfig__GetAllProperties(), source, new String[] { "mName", "get All Properties" });
		addAnnotation(getMTableConfig__DoActionUpdate__MTableConfigAction(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation(getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation((getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean())
				.getEParameters().get(3), source, new String[] { "mName", "complement Table" });
		addAnnotation((getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean())
				.getEParameters().get(4), source, new String[] { "mName", "reset" });
		addAnnotation(getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation(getMTableConfig__InvokeSetDoAction__MTableConfigAction(), source,
				new String[] { "mName", "invoke Set Do Action" });
		addAnnotation(getMTableConfig_DoAction(), source, new String[] { "mName", "do Action" });
		addAnnotation(getMTableConfig_IntendedAction(), source, new String[] { "mName", "intended Action" });
		addAnnotation(getMColumnConfig__DoActionUpdate__MColumnConfigAction(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation(getMColumnConfig_Name(), source, new String[] { "mName", "name" });
		addAnnotation(getMColumnConfig_DoAction(), source, new String[] { "mName", "do Action" });
		addAnnotation(mCellEditBehaviorOptionEEnum.getELiterals().get(1), source, new String[] { "mName", "PullDown" });
		addAnnotation(mCellEditBehaviorOptionEEnum.getELiterals().get(2), source, new String[] { "mName", "ReadOnly" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/mdcm/v2</b>. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createV2Annotations() {
		String source = "http://www.xocl.org/mdcm/v2";
		addAnnotation(getMTableConfig_ReferenceToTableConfig(), source, new String[] {});
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMEditorConfig_DoAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_ECName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_ECLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_ECColumnConfig(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_ComplementActionTable(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_Label(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_DoAction(), source, new String[] { "propertyFilterFlags",
				"org.eclipse.ui.views.properties.expert", "propertySortChoices", "false" });
		addAnnotation(getMTableConfig_IntendedAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_ComplementAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_SplitChildrenAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTableConfig_MergeChildrenAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMColumnConfig_ECName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMColumnConfig_ECLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMColumnConfig_ECWidth(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMColumnConfig_Label(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMColumnConfig_DoAction(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassToCellConfig_CellKind(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRowFeatureCell_MaximumSize(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRowFeatureCell_AverageSize(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRowFeatureCell_MinimumSize(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRowFeatureCell_SizeOfPropertyValue(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMReferenceToTableConfig_Label(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMElementWithFont_DerivedFontOptionsEncoding(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EXPRESSION_OCL</b>.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEXPRESSION_OCLAnnotations() {
		String source = "http://www.xocl.org/EXPRESSION_OCL";
		addAnnotation(getMCellConfig_ExplicitHighlightOCL(), source, new String[] { "EXPRESSION", "OCL" });
		addAnnotation(getMOclCell_StringRendering(), source, new String[] {});
	}

} // MtableeditorPackageImpl
