/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.PropertyBehavior;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MEditorConfig;
import com.montages.mtableeditor.MOclCell;
import com.montages.mtableeditor.MReferenceToTableConfig;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MTableConfigAction;
import com.montages.mtableeditor.MtableeditorFactory;
import com.montages.mtableeditor.MtableeditorPackage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.langlets.autil.AutilPackage;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MTable Config</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getColumnConfig <em>Column Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getReferenceToTableConfig <em>Reference To Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getIntendedPackage <em>Intended Package</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getIntendedClass <em>Intended Class</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getECName <em>EC Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getECLabel <em>EC Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getECColumnConfig <em>EC Column Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getContainingEditorConfig <em>Containing Editor Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getComplementActionTable <em>Complement Action Table</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getDisplayDefaultColumn <em>Display Default Column</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getDisplayHeader <em>Display Header</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getIntendedAction <em>Intended Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getComplementAction <em>Complement Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getSplitChildrenAction <em>Split Children Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MTableConfigImpl#getMergeChildrenAction <em>Merge Children Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MTableConfigImpl extends MTableEditorElementImpl implements MTableConfig {

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The cached value of the '{@link #getColumnConfig() <em>Column Config</em>
	 * }' containment reference list. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getColumnConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<MColumnConfig> columnConfig;

	/**
	 * The cached value of the '{@link #getReferenceToTableConfig()
	 * <em>Reference To Table Config</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReferenceToTableConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<MReferenceToTableConfig> referenceToTableConfig;

	/**
	 * The cached value of the '{@link #getIntendedPackage() <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntendedPackage()
	 * @generated
	 * @ordered
	 */
	protected MPackage intendedPackage;

	/**
	 * This is true if the Intended Package reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean intendedPackageESet;

	/**
	 * The cached value of the '{@link #getIntendedClass() <em>Intended Class</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntendedClass()
	 * @generated
	 * @ordered
	 */
	protected MClassifier intendedClass;

	/**
	 * This is true if the Intended Class reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean intendedClassESet;

	/**
	 * The default value of the '{@link #getECName() <em>EC Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getECName()
	 * @generated
	 * @ordered
	 */
	protected static final String EC_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getECLabel() <em>EC Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getECLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String EC_LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getComplementActionTable() <em>Complement Action Table</em>}' reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getComplementActionTable()
	 * @generated
	 * @ordered
	 */
	protected EList<MTableConfig> complementActionTable;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * This is true if the Label attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean labelESet;

	/**
	 * The default value of the '{@link #getDisplayDefaultColumn() <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getDisplayDefaultColumn()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DISPLAY_DEFAULT_COLUMN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDisplayDefaultColumn() <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getDisplayDefaultColumn()
	 * @generated
	 * @ordered
	 */
	protected Boolean displayDefaultColumn = DISPLAY_DEFAULT_COLUMN_EDEFAULT;

	/**
	 * This is true if the Display Default Column attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean displayDefaultColumnESet;

	/**
	 * The default value of the '{@link #getDisplayHeader() <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getDisplayHeader()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DISPLAY_HEADER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDisplayHeader() <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getDisplayHeader()
	 * @generated
	 * @ordered
	 */
	protected Boolean displayHeader = DISPLAY_HEADER_EDEFAULT;

	/**
	 * This is true if the Display Header attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean displayHeaderESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MTableConfigAction DO_ACTION_EDEFAULT = MTableConfigAction.DO;

	/**
	 * The default value of the '{@link #getComplementAction() <em>Complement Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getComplementAction()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COMPLEMENT_ACTION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSplitChildrenAction() <em>Split Children Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSplitChildrenAction()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SPLIT_CHILDREN_ACTION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getMergeChildrenAction() <em>Merge Children Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMergeChildrenAction()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean MERGE_CHILDREN_ACTION_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemtableeditorMTableConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #tableConfigFromReference <em>Table Config From Reference</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #tableConfigFromReference
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression tableConfigFromReferencemcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #getAllProperties <em>Get All Properties</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getAllProperties
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getAllPropertiesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemtableeditorMTableConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatesemanticsXUpdatemtableeditorMEditorConfigmtableeditorMTableConfigActionmtableeditorMTableConfigecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatesemanticsXUpdatemtableeditorMTableConfigmtableeditorMTableConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeSetDoAction <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #invokeSetDoAction
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeSetDoActionmtableeditorMTableConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getIntendedClass <em>Intended Class</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntendedClass
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression intendedClassChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getECName
	 * <em>EC Name</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getECName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eCNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getECLabel
	 * <em>EC Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getECLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eCLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getECColumnConfig <em>EC Column Config</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getECColumnConfig
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eCColumnConfigDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '
	 * {@link #getDoAction <em>Do Action</em>}' property. Is combined with the
	 * choice constraint definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getIntendedAction <em>Intended Action</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getIntendedAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression intendedActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getComplementAction <em>Complement Action</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getComplementAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression complementActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSplitChildrenAction <em>Split Children Action</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSplitChildrenAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression splitChildrenActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMergeChildrenAction <em>Merge Children Action</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMergeChildrenAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression mergeChildrenActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase
	 * <em>AKind Base</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getAKindBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MTableConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MTABLE_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MTABLE_CONFIG__NAME, oldName,
					name, !oldNameESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		String oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MTABLE_CONFIG__NAME, oldName,
					NAME_EDEFAULT, oldNameESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MColumnConfig> getColumnConfig() {
		if (columnConfig == null) {
			columnConfig = new EObjectContainmentWithInverseEList.Unsettable.Resolving<MColumnConfig>(
					MColumnConfig.class, this, MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG,
					MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG);
		}
		return columnConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetColumnConfig() {
		if (columnConfig != null)
			((InternalEList.Unsettable<?>) columnConfig).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetColumnConfig() {
		return columnConfig != null && ((InternalEList.Unsettable<?>) columnConfig).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MReferenceToTableConfig> getReferenceToTableConfig() {
		if (referenceToTableConfig == null) {
			referenceToTableConfig = new EObjectContainmentEList.Unsettable.Resolving<MReferenceToTableConfig>(
					MReferenceToTableConfig.class, this, MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG);
		}
		return referenceToTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReferenceToTableConfig() {
		if (referenceToTableConfig != null)
			((InternalEList.Unsettable<?>) referenceToTableConfig).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReferenceToTableConfig() {
		return referenceToTableConfig != null && ((InternalEList.Unsettable<?>) referenceToTableConfig).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getIntendedPackage() {
		if (intendedPackage != null && intendedPackage.eIsProxy()) {
			InternalEObject oldIntendedPackage = (InternalEObject) intendedPackage;
			intendedPackage = (MPackage) eResolveProxy(oldIntendedPackage);
			if (intendedPackage != oldIntendedPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE, oldIntendedPackage, intendedPackage));
			}
		}
		return intendedPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetIntendedPackage() {
		return intendedPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntendedPackage(MPackage newIntendedPackage) {
		MPackage oldIntendedPackage = intendedPackage;
		intendedPackage = newIntendedPackage;
		boolean oldIntendedPackageESet = intendedPackageESet;
		intendedPackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE,
					oldIntendedPackage, intendedPackage, !oldIntendedPackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIntendedPackage() {
		MPackage oldIntendedPackage = intendedPackage;
		boolean oldIntendedPackageESet = intendedPackageESet;
		intendedPackage = null;
		intendedPackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE,
					oldIntendedPackage, null, oldIntendedPackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIntendedPackage() {
		return intendedPackageESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getIntendedClass() {
		if (intendedClass != null && intendedClass.eIsProxy()) {
			InternalEObject oldIntendedClass = (InternalEObject) intendedClass;
			intendedClass = (MClassifier) eResolveProxy(oldIntendedClass);
			if (intendedClass != oldIntendedClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS, oldIntendedClass, intendedClass));
			}
		}
		return intendedClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetIntendedClass() {
		return intendedClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntendedClass(MClassifier newIntendedClass) {
		MClassifier oldIntendedClass = intendedClass;
		intendedClass = newIntendedClass;
		boolean oldIntendedClassESet = intendedClassESet;
		intendedClassESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS,
					oldIntendedClass, intendedClass, !oldIntendedClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIntendedClass() {
		MClassifier oldIntendedClass = intendedClass;
		boolean oldIntendedClassESet = intendedClassESet;
		intendedClass = null;
		intendedClassESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS,
					oldIntendedClass, null, oldIntendedClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIntendedClass() {
		return intendedClassESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Intended Class</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let rightPackage: Boolean = if (let chain: mcore::MPackage = intendedPackage in
	if chain <> null then true else false 
	endif) 
	=true 
	then (let e0: Boolean = trg.containingPackage = intendedPackage in 
	if e0.oclIsInvalid() then null else e0 endif)
	else true
	endif in
	let isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in 
	if e1.oclIsInvalid() then null else e1 endif in
	let intended: Boolean = let e1: Boolean = if (isClass)= false 
	then false 
	else if (rightPackage)= false 
	then false 
	else if ((isClass)= null or (rightPackage)= null) = true 
	then null 
	else true endif endif endif in 
	if e1.oclIsInvalid() then null else e1 endif in
	intended
	
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalIntendedClassChoiceConstraint(MClassifier trg) {
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		if (intendedClassChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_CLASS;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				intendedClassChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "IntendedClassChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(intendedClassChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass,
					"IntendedClassChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getECName() {
		/**
		 * @OCL let resultofecName: String = if (let e0: Boolean = let chain01: String = name in
		if chain01.trim().oclIsUndefined() 
		then null 
		else chain01.trim()
		endif = '' in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then if intendedClass.oclIsUndefined()
		then null
		else intendedClass.name
		endif
		else name
		endif in
		resultofecName
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__EC_NAME;

		if (eCNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eCNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eCNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getECLabel() {
		/**
		 * @OCL label
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__EC_LABEL;

		if (eCLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eCLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eCLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MColumnConfig> getECColumnConfig() {
		/**
		 * @OCL columnConfig->asOrderedSet()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__EC_COLUMN_CONFIG;

		if (eCColumnConfigDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eCColumnConfigDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eCColumnConfigDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MColumnConfig> result = (EList<MColumnConfig>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfig getContainingEditorConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG)
			return null;
		return (MEditorConfig) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfig basicGetContainingEditorConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG)
			return null;
		return (MEditorConfig) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingEditorConfig(MEditorConfig newContainingEditorConfig,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newContainingEditorConfig,
				MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingEditorConfig(MEditorConfig newContainingEditorConfig) {
		if (newContainingEditorConfig != eInternalContainer()
				|| (eContainerFeatureID() != MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG
						&& newContainingEditorConfig != null)) {
			if (EcoreUtil.isAncestor(this, newContainingEditorConfig))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingEditorConfig != null)
				msgs = ((InternalEObject) newContainingEditorConfig).eInverseAdd(this,
						MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG, MEditorConfig.class, msgs);
			msgs = basicSetContainingEditorConfig(newContainingEditorConfig, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG, newContainingEditorConfig,
					newContainingEditorConfig));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		boolean oldLabelESet = labelESet;
		labelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MTABLE_CONFIG__LABEL, oldLabel,
					label, !oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLabel() {
		String oldLabel = label;
		boolean oldLabelESet = labelESet;
		label = LABEL_EDEFAULT;
		labelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MTABLE_CONFIG__LABEL, oldLabel,
					LABEL_EDEFAULT, oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLabel() {
		return labelESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getDisplayDefaultColumn() {
		return displayDefaultColumn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayDefaultColumn(Boolean newDisplayDefaultColumn) {
		Boolean oldDisplayDefaultColumn = displayDefaultColumn;
		displayDefaultColumn = newDisplayDefaultColumn;
		boolean oldDisplayDefaultColumnESet = displayDefaultColumnESet;
		displayDefaultColumnESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN, oldDisplayDefaultColumn,
					displayDefaultColumn, !oldDisplayDefaultColumnESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDisplayDefaultColumn() {
		Boolean oldDisplayDefaultColumn = displayDefaultColumn;
		boolean oldDisplayDefaultColumnESet = displayDefaultColumnESet;
		displayDefaultColumn = DISPLAY_DEFAULT_COLUMN_EDEFAULT;
		displayDefaultColumnESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN, oldDisplayDefaultColumn,
					DISPLAY_DEFAULT_COLUMN_EDEFAULT, oldDisplayDefaultColumnESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDisplayDefaultColumn() {
		return displayDefaultColumnESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getDisplayHeader() {
		return displayHeader;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayHeader(Boolean newDisplayHeader) {
		Boolean oldDisplayHeader = displayHeader;
		displayHeader = newDisplayHeader;
		boolean oldDisplayHeaderESet = displayHeaderESet;
		displayHeaderESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER,
					oldDisplayHeader, displayHeader, !oldDisplayHeaderESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDisplayHeader() {
		Boolean oldDisplayHeader = displayHeader;
		boolean oldDisplayHeaderESet = displayHeaderESet;
		displayHeader = DISPLAY_HEADER_EDEFAULT;
		displayHeaderESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER,
					oldDisplayHeader, DISPLAY_HEADER_EDEFAULT, oldDisplayHeaderESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDisplayHeader() {
		return displayHeaderESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfigAction getDoAction() {
		/**
		 * @OCL mtableeditor::MTableConfigAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MTableConfigAction result = (MTableConfigAction) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setDoAction(MTableConfigAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MTableConfigAction.UNSET_INTENDED_CLASS_VALUE:
			this.unsetIntendedClass();
			break;

		case MTableConfigAction.UNSET_INTENDED_PACKAGE_VALUE:
			this.unsetIntendedPackage();
			break;
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MTableConfigAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let var0: OrderedSet(MTableConfigAction) = intendedAction in var0
	->append(MTableConfigAction::AddEmptyColumn)
	->append(MTableConfigAction::AddColumnWithRow)
	->append(MTableConfigAction::AddColumnWithOcl)
	->append(MTableConfigAction::AddTableRef)
	->prepend(MTableConfigAction::Do)
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MTableConfigAction> evalDoActionChoiceConstruction(List<MTableConfigAction> choice) {
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__DO_ACTION;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				doActionChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MTableConfigAction> result = new ArrayList<MTableConfigAction>(
					(Collection<MTableConfigAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTableConfigAction> getIntendedAction() {
		/**
		 * @OCL if self.intendedClass.oclIsUndefined()
		then if self.intendedPackage.oclIsUndefined()
			then OrderedSet{}
			else OrderedSet{MTableConfigAction::UnsetIntendedPackage}
			endif
		else
		let e1: OrderedSet(mtableeditor::MTableConfigAction) = 
		let e0: OrderedSet(mtableeditor::MTableConfigAction) = if self.intendedPackage.oclIsUndefined()
			then OrderedSet{MTableConfigAction::UnsetIntendedClass}
			else OrderedSet{MTableConfigAction::UnsetIntendedClass, MTableConfigAction::UnsetIntendedPackage}
			endif   				
			in if self.columnConfig->isEmpty() and self.referenceToTableConfig->isEmpty() and true
				then e0
				else
					let e6: OrderedSet(mtableeditor::MTableConfigAction) = 
					let e5: OrderedSet(mtableeditor::MTableConfigAction) = 
					let e4: OrderedSet(mtableeditor::MTableConfigAction) = 
					let e3: OrderedSet(mtableeditor::MTableConfigAction) = 
					let e2: OrderedSet(mtableeditor::MTableConfigAction) = e0
					in if self.intendedClass.allDirectSubTypes()->isEmpty()
						then e2
						else e2->prepend(MTableConfigAction::SplitSpecializations)
						endif
					in if self.intendedClass.allSuperTypes()->isEmpty()
						then e3
						else e3->prepend(MTableConfigAction::MergeAbstraction)
						endif
					in if self.splitChildrenAction = true
						then e4->prepend(MTableConfigAction::SplitChildren)
						else e4
						endif
					in if self.mergeChildrenAction = true
						then e5->prepend(MTableConfigAction::MergeChildren)
						else e5
						endif
					in if self.intendedClass.classescontainedin->isEmpty()
						then e6
						else e6->prepend(MTableConfigAction::MergeContainer)
						endif
				endif   					
				in if self.complementAction = false
				then e1
				else e1->prepend(MTableConfigAction::ComplementTable)
				endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_ACTION;

		if (intendedActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				intendedActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(intendedActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTableConfigAction> result = (EList<MTableConfigAction>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getComplementAction() {
		/**
		 * @OCL let includesAllContainments: Boolean = if (let e0: Boolean = if (let e0: Boolean = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet()->includesAll(if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif)   in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let chain01: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= false 
		then false 
		else if ((let e0: Boolean = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet()->includesAll(if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif)   in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then false
		else true
		endif in
		let hasContainments: Boolean = if (let e0: Boolean = if (true)= false 
		then false 
		else if (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= false 
		then false 
		else if ((true)= null or (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then includesAllContainments
		else false
		endif in
		let allRowFeatureCell: OrderedSet(mtableeditor::MRowFeatureCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in
		chain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() 
		else r endif) in
		let includesAllProperties: Boolean = if (let e0: Boolean = if (let e0: Boolean = allRowFeatureCell.feature->asOrderedSet()->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= true 
		then true 
		else if ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= true 
		then true 
		else if ( notHidden.directSemantics.oclIsUndefined())= true 
		then true 
		else if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= null or ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true 
		then null 
		else false endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->includesAll(if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allNonContainmentFeatures()
		endif->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= true 
		then true 
		else if ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= true 
		then true 
		else if ( notHidden.directSemantics.oclIsUndefined())= true 
		then true 
		else if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= null or ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true 
		then null 
		else false endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() )   in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let chain01: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet() in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= false 
		then false 
		else if ((let e0: Boolean = allRowFeatureCell.feature->asOrderedSet()->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= true 
		then true 
		else if ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= true 
		then true 
		else if ( notHidden.directSemantics.oclIsUndefined())= true 
		then true 
		else if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= null or ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true 
		then null 
		else false endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->includesAll(if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allNonContainmentFeatures()
		endif->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= true 
		then true 
		else if ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= true 
		then true 
		else if ( notHidden.directSemantics.oclIsUndefined())= true 
		then true 
		else if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout.hideInTable
		endif in
		if chain01 = false then true else false 
		endif)= null or ( if notHidden.directSemantics.oclIsUndefined()
		then null
		else notHidden.directSemantics.layout
		endif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true 
		then null 
		else false endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() )   in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet() in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then hasContainments
		else true
		endif in
		let hasProperties: Boolean = if (let e0: Boolean = if (true)= false 
		then false 
		else if (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allNonContainmentFeatures()
		endif in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= false 
		then false 
		else if ((true)= null or (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allNonContainmentFeatures()
		endif in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then includesAllProperties
		else hasContainments
		endif in
		let resultofComplementAction: Boolean = if (let e0: Boolean = if ( intendedClass.oclIsUndefined())= true 
		then true 
		else if ( intendedClass.oclIsInvalid())= true 
		then true 
		else if (( intendedClass.oclIsUndefined())= null or ( intendedClass.oclIsInvalid())= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then false
		else hasProperties
		endif in
		resultofComplementAction
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__COMPLEMENT_ACTION;

		if (complementActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				complementActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(complementActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSplitChildrenAction() {
		/**
		 * @OCL let propertiesChildClass: OrderedSet(mcore::MProperty)  = getAllProperties()->asOrderedSet()->select(it: mcore::MProperty | let e0: Boolean = intendedClass <> it.containingClassifier in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let allOclCell: OrderedSet(mtableeditor::MOclCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in
		chain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MOclCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MOclCell) then r->including(i.oclAsType(mtableeditor::MOclCell))->asOrderedSet() 
		else r endif)->select(it: mtableeditor::MOclCell | let e0: Boolean = it.class <> intendedClass in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let containsChildren: Boolean = if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif in
		if chain->isEmpty().oclIsUndefined() 
		then null 
		else chain->isEmpty()
		endif) 
		=true 
		then false else if (let e0: Boolean = if (let chain01: OrderedSet(mcore::MProperty)  = propertiesChildClass in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= true 
		then true 
		else if (let chain02: OrderedSet(mtableeditor::MOclCell)  = allOclCell in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= true 
		then true 
		else if ((let chain01: OrderedSet(mcore::MProperty)  = propertiesChildClass in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= null or (let chain02: OrderedSet(mtableeditor::MOclCell)  = allOclCell in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif in
		containsChildren
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__SPLIT_CHILDREN_ACTION;

		if (splitChildrenActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				splitChildrenActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(splitChildrenActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getMergeChildrenAction() {
		/**
		 * @OCL let containments: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif in
		let allChildClasses: OrderedSet(mcore::MClassifier)  = containments.type->asOrderedSet() in
		let propertiesofChildren: OrderedSet(mcore::MProperty)  = getAllProperties()->asOrderedSet()->select(it: mcore::MProperty | let e0: Boolean = it.containingClassifier <> intendedClass in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let resultofMergeChildren: Boolean = if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif in
		if chain->isEmpty().oclIsUndefined() 
		then null 
		else chain->isEmpty()
		endif) 
		=true 
		then false else if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()
		then OrderedSet{}
		else intendedClass.allContainmentReferences()
		endif in
		if chain->isEmpty().oclIsUndefined() 
		then null 
		else chain->isEmpty()
		endif)=true then true
		else let e0: Boolean = not(let e0: Boolean = propertiesofChildren->includesAll(allChildClasses.allNonContainmentFeatures()->asOrderedSet())   in 
		if e0.oclIsInvalid() then null else e0 endif) in 
		if e0.oclIsInvalid() then null else e0 endif
		endif endif in
		resultofMergeChildren
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_CONFIG__MERGE_CHILDREN_ACTION;

		if (mergeChildrenActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				mergeChildrenActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(mergeChildrenActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTableConfig> getComplementActionTable() {
		if (complementActionTable == null) {
			complementActionTable = new EObjectResolvingEList.Unsettable<MTableConfig>(MTableConfig.class, this,
					MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE);
		}
		return complementActionTable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetComplementActionTable() {
		if (complementActionTable != null)
			((InternalEList.Unsettable<?>) complementActionTable).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetComplementActionTable() {
		return complementActionTable != null && ((InternalEList.Unsettable<?>) complementActionTable).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MTableConfigAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE.createXTransition();
		com.montages.mtableeditor.MTableConfigAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), org.xocl.semantics.XUpdateMode.REDEFINE, null,
				triggerValue, null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig tableConfigFromReference(MProperty reference) {

		/**
		 * @OCL null
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (MtableeditorPackage.Literals.MTABLE_CONFIG);
		EOperation eOperation = MtableeditorPackage.Literals.MTABLE_CONFIG.getEOperations().get(1);
		if (tableConfigFromReferencemcoreMPropertyBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				tableConfigFromReferencemcoreMPropertyBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(tableConfigFromReferencemcoreMPropertyBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("reference", reference);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MTableConfig) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getAllProperties() {

		/**
		 * @OCL let allRef: OrderedSet(mcore::MProperty)  = let chain: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in
		chain->iterate(i:mcore::MProperty; r: OrderedSet(mcore::MProperty)=OrderedSet{} | if i.oclIsKindOf(mcore::MProperty) then r->including(i.oclAsType(mcore::MProperty))->asOrderedSet() 
		else r endif) in
		let allRowFeatureCell: OrderedSet(mtableeditor::MRowFeatureCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in
		chain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() 
		else r endif) in
		let var2: OrderedSet(mcore::MProperty)  = let e1: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet()->union(allRef) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif in
		var2
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (MtableeditorPackage.Literals.MTABLE_CONFIG);
		EOperation eOperation = MtableeditorPackage.Literals.MTABLE_CONFIG.getEOperations().get(2);
		if (getAllPropertiesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				getAllPropertiesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getAllPropertiesBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProperty>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MTableConfigAction mTableConfigAction) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();
		switch (mTableConfigAction.getValue()) {
		case MTableConfigAction.ADD_EMPTY_COLUMN_VALUE: {
			int nrOfColumns = getColumnConfig().size() + 1;

			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.ADD_EMPTY_COLUMN_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			MColumnConfig newColumn = (MColumnConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
			currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
					XUpdateMode.REDEFINE, null, "Column ".concat(Integer.toString(nrOfColumns)), null, null);

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newColumn));
			return currentTrigger;
		}

		case MTableConfigAction.ADD_TABLE_REF_VALUE: {
			int nrOfRefs = getReferenceToTableConfig().size() + 1;

			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.ADD_TABLE_REF_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
			currentTrigger.addReferenceUpdate(this,
					MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG, XUpdateMode.ADD,
					XAddUpdateMode.LAST, newRef, null, null);

			List<MProperty> allUsableContainments = new ArrayList<MProperty>();
			List<MProperty> allUsedContainments = new ArrayList<MProperty>();
			for (MProperty mprop : this.getIntendedClass().allProperties()) {
				if (mprop.getPropertyBehavior() == PropertyBehavior.CONTAINED) {
					allUsableContainments.add(mprop);
				}
			}

			// Checks which properties aren't contained and are already in use
			for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
				if ((!allUsedContainments.contains(mref.getReference())) && mref.getReference() != null
						&& mref.getReference().getPropertyBehavior() == PropertyBehavior.CONTAINED) {
					allUsedContainments.add(mref.getReference());
				}
			}

			for (MProperty mprop : allUsedContainments) {
				allUsableContainments.remove(mprop);
			}

			if (!allUsableContainments.isEmpty()) {
				currentTrigger.addReferenceUpdate(newRef,
						MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE, XUpdateMode.REDEFINE, null,
						allUsableContainments.get(0), null, null);
				currentTrigger.addAttributeUpdate(newRef,
						MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
						allUsableContainments.get(0).getCalculatedName(), null, null);

				for (MTableConfig mtab : this.getContainingEditorConfig().getMTableConfig()) {
					if (mtab.getIntendedClass() == allUsableContainments.get(0).getType()
							&& allUsableContainments.get(0).getType() != null) {
						currentTrigger.addReferenceUpdate(newRef,
								MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
								XUpdateMode.REDEFINE, null, mtab, null, null);
						break;
					}
				}

			} else
				currentTrigger.addAttributeUpdate(newRef,
						MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
						"Subtable ".concat(Integer.toString(nrOfRefs)), null, null);

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newRef));
			return currentTrigger;
		}

		case MTableConfigAction.ADD_COLUMN_WITH_ROW_VALUE: {

			int nrOfColumns = getColumnConfig().size() + 1;

			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.ADD_COLUMN_WITH_ROW_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			MColumnConfig newColumn = (MColumnConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());

			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);

			MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

			MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMRowFeatureCell());

			newClassCell.setClass(this.getIntendedClass());
			newClassCell.setCellConfig(newRowCell);
			newRowCell.setClass(this.getIntendedClass());

			currentTrigger.addReferenceUpdate(newColumn,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG, XUpdateMode.REDEFINE, null,
					newRowCell, null, null);

			if (this.getIntendedClass() != null) {
				List<MProperty> allUsableProperties = new ArrayList<MProperty>();
				List<MProperty> allUsedProperties = new ArrayList<MProperty>();
				for (MProperty mprop : this.getIntendedClass().allProperties()) {
					if (mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED) {
						allUsableProperties.add(mprop);
					}
				}

				// Checks which properties aren't contained and are already in
				// use
				for (MColumnConfig column : this.getColumnConfig()) {
					for (MClassToCellConfig classToCell : column.getClassToCellConfig()) {
						if (classToCell instanceof MRowFeatureCell) {
							MRowFeatureCell rowCell = (MRowFeatureCell) classToCell;
							if ((!allUsedProperties.contains(rowCell.getFeature())) && rowCell.getFeature() != null
									&& rowCell.getFeature().getPropertyBehavior() != PropertyBehavior.CONTAINED) {
								allUsedProperties.add(rowCell.getFeature());
							}
						}
					}
				}

				for (MProperty mprop : allUsedProperties) {
					allUsableProperties.remove(mprop);
				}
				if (!allUsableProperties.isEmpty()) {
					currentTrigger.addReferenceUpdate(newRowCell,
							MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE, XUpdateMode.REDEFINE, null,
							allUsableProperties.get(0), null, null);
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
							XUpdateMode.REDEFINE, null, allUsableProperties.get(0).getEName(), null, null);
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL,
							XUpdateMode.REDEFINE, null, allUsableProperties.get(0).getCalculatedName(), null, null);

				} else
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
							XUpdateMode.REDEFINE, null, "column".concat(Integer.toString(nrOfColumns)), null, null);
				currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL,
						XUpdateMode.REDEFINE, null, "Column ".concat(Integer.toString(nrOfColumns)), null, null);
			}

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newColumn));
			return currentTrigger;
		}

		case MTableConfigAction.ADD_COLUMN_WITH_OCL_VALUE: {
			int nrOfColumns = getColumnConfig().size() + 1;

			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.ADD_COLUMN_WITH_OCL_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			MColumnConfig newColumn = (MColumnConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());

			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
			currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
					XUpdateMode.REDEFINE, null, "column".concat(Integer.toString(nrOfColumns)), null, null);
			currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL,
					XUpdateMode.REDEFINE, null, "Column ".concat(Integer.toString(nrOfColumns)), null, null);

			MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

			MOclCell newOclCell = (MOclCell) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMOclCell());

			newClassCell.setClass(this.getIntendedClass());
			newClassCell.setCellConfig(newOclCell);
			newOclCell.setClass(this.getIntendedClass());

			currentTrigger.addReferenceUpdate(newColumn,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG, XUpdateMode.REDEFINE, null,
					newOclCell, null, null);

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newColumn));
			return currentTrigger;
		}

		case MTableConfigAction.COMPLEMENT_TABLE_VALUE: {
			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.ADD_COLUMN_WITH_ROW_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);
			/*
			 * //Currently only properties which are already in Columns and not
			 * hidden for the table editor, no containments as ReferenceTable
			 * List<MProperty> allUsableProperties = new ArrayList<MProperty>();
			 * List<MProperty> allUsedProperties = new ArrayList<MProperty>();
			 * for (MProperty mprop : this.getIntendedClass().allProperties()) {
			 * if (mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED &&
			 * mprop.getIsOperation() == false && (mprop.getDirectSemantics() ==
			 * null || mprop.getDirectSemantics().getLayout() == null ||
			 * mprop.getDirectSemantics().getLayout().getHideInTable() ==
			 * false)) { allUsableProperties.add(mprop); } }
			 * 
			 * //Checks which properties aren't contained, not hidden for the
			 * table editor and are already in use for (MColumnConfig column :
			 * this.getColumnConfig()) { for (MClassToCellConfig classToCell :
			 * column.getClassToCellConfig()) { if (classToCell instanceof
			 * MRowFeatureCell) { MRowFeatureCell rowCell = (MRowFeatureCell)
			 * classToCell; MProperty mprop = rowCell.getFeature(); if
			 * ((!allUsedProperties.contains(mprop)) && mprop != null &&
			 * mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED &&
			 * mprop.getIsOperation() == false && (mprop.getDirectSemantics() ==
			 * null || mprop.getDirectSemantics().getLayout() == null ||
			 * mprop.getDirectSemantics().getLayout().getHideInTable() ==
			 * false)) { allUsedProperties.add(mprop); } } } }
			 * 
			 * for (MProperty mprop : allUsedProperties) {
			 * allUsableProperties.remove(mprop); }
			 * 
			 * for (MProperty mprop : allUsableProperties) { MColumnConfig
			 * newColumn = (MColumnConfig) currentTrigger
			 * .createNextStateNewObject(MtableeditorPackage.eINSTANCE.
			 * getMColumnConfig());
			 * 
			 * currentTrigger.addReferenceUpdate(this,
			 * MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
			 * XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
			 * 
			 * MClassToCellConfig newClassCell = (MClassToCellConfig)
			 * currentTrigger
			 * .createNextStateNewObject(MtableeditorPackage.eINSTANCE.
			 * getMClassToCellConfig());
			 * 
			 * MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
			 * .createNextStateNewObject(MtableeditorPackage.eINSTANCE.
			 * getMRowFeatureCell());
			 * 
			 * newClassCell.setClass(this.getIntendedClass());
			 * newClassCell.setCellConfig(newRowCell);
			 * newRowCell.setClass(this.getIntendedClass());
			 * 
			 * currentTrigger.addReferenceUpdate(newColumn,
			 * MtableeditorPackage.Literals.
			 * MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG, XUpdateMode.REDEFINE, null,
			 * newRowCell, null, null);
			 * currentTrigger.addReferenceUpdate(newRowCell,
			 * MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE,
			 * XUpdateMode.REDEFINE, null, mprop, null, null);
			 * currentTrigger.addAttributeUpdate(newColumn,
			 * MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
			 * XUpdateMode.REDEFINE, null, mprop.getEName(), null, null);
			 * currentTrigger.addAttributeUpdate(newColumn,
			 * MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL,
			 * XUpdateMode.REDEFINE, null, mprop.getCalculatedName(), null,
			 * null); }
			 * 
			 * //checks containments and if a table already exist
			 * List<MProperty> allUsableContainments = new
			 * ArrayList<MProperty>(); List<MProperty> allUsedContainments = new
			 * ArrayList<MProperty>(); for (MProperty mprop :
			 * this.getIntendedClass().allProperties()) { if
			 * (mprop.getPropertyBehavior() == PropertyBehavior.CONTAINED) {
			 * allUsableContainments.add(mprop); } }
			 * 
			 * for (MReferenceToTableConfig mref :
			 * this.getReferenceToTableConfig()) { if
			 * ((!allUsedContainments.contains(mref.getReference())) &&
			 * mref.getReference() != null &&
			 * mref.getReference().getPropertyBehavior() ==
			 * PropertyBehavior.CONTAINED) {
			 * allUsedContainments.add(mref.getReference()); } }
			 * 
			 * for (MProperty mprop : allUsedContainments) {
			 * allUsableContainments.remove(mprop); }
			 * 
			 * List<MTableConfig> newTableSet = new ArrayList<MTableConfig>();
			 * for (MProperty mprop : allUsableContainments) {
			 * MReferenceToTableConfig newRef = (MReferenceToTableConfig)
			 * currentTrigger
			 * .createNextStateNewObject(MtableeditorPackage.eINSTANCE.
			 * getMReferenceToTableConfig());
			 * currentTrigger.addReferenceUpdate(this,
			 * MtableeditorPackage.Literals.
			 * MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG, XUpdateMode.ADD,
			 * XAddUpdateMode.LAST, newRef, null, null);
			 * 
			 * currentTrigger.addReferenceUpdate(newRef,
			 * MtableeditorPackage.Literals.
			 * MREFERENCE_TO_TABLE_CONFIG__REFERENCE, XUpdateMode.REDEFINE,
			 * null, mprop, null, null);
			 * currentTrigger.addAttributeUpdate(newRef,
			 * MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__LABEL,
			 * XUpdateMode.REDEFINE, null, mprop.getCalculatedName(), null,
			 * null);
			 * 
			 * Boolean search = true; for (MTableConfig mtab :
			 * this.getContainingEditorConfig().getMTableConfig()) { if
			 * (mtab.getIntendedClass() == mprop.getType() && mprop.getType() !=
			 * null) { search = false; currentTrigger.addReferenceUpdate(newRef,
			 * MtableeditorPackage.Literals.
			 * MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, XUpdateMode.REDEFINE,
			 * null, mtab, null, null); break; } } //If search is true there is
			 * the need to search for a super class if (search) { for
			 * (MClassifier mClass : mprop.getType().allSuperTypes()) { for
			 * (MTableConfig mtab :
			 * this.getContainingEditorConfig().getMTableConfig()) { if
			 * (mtab.getIntendedClass() == mClass && mClass != null) { search =
			 * false; currentTrigger.addReferenceUpdate(newRef,
			 * MtableeditorPackage.Literals.
			 * MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, XUpdateMode.REDEFINE,
			 * null, mtab, null, null); break; } } } } //If there is also no
			 * super class found, search is still true, then it creates the
			 * table on its own if (search) { MTableConfig newTable =
			 * MtableeditorFactory.eINSTANCE.createMTableConfig();
			 * newTable.setName(mprop.getType().getName());
			 * newTable.setLabel(mprop.getType().getName());
			 * newTable.setIntendedClass(mprop.getType());
			 * newTable.setIntendedPackage(mprop.getType().getContainingPackage(
			 * )); newTable.setDisplayHeader(true);
			 * newTable.setDisplayDefaultColumn(false);
			 * currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(
			 * ), MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG,
			 * XUpdateMode.ADD, XAddUpdateMode.LAST, newTable, null, null);
			 * currentTrigger.addReferenceUpdate(newRef,
			 * MtableeditorPackage.Literals.
			 * MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, XUpdateMode.REDEFINE,
			 * null, newTable, null, null); newTableSet.add(newTable); } }
			 */

			List<MTableConfig> complementTable = new ArrayList<MTableConfig>();
			complementTable.add(this);
			this.doActionUpdate(currentTrigger, this.getContainingEditorConfig(), MTableConfigAction.COMPLEMENT_TABLE,
					complementTable, false);

			// this.getComplementActionTable().clear();
			currentTrigger.invokeOperationCall(this,
					MtableeditorPackage.Literals.MTABLE_CONFIG___INVOKE_SET_DO_ACTION__MTABLECONFIGACTION,
					mTableConfigAction, null, null);
			return currentTrigger;
		}

		case MTableConfigAction.MERGE_ABSTRACTION_VALUE: {
			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.MERGE_ABSTRACTION_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
					MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.REMOVE, null, this, null,
					null);

			ArrayList<MReferenceToTableConfig> referenceIn = new ArrayList<MReferenceToTableConfig>();

			for (MTableConfig mTab : this.getContainingEditorConfig().getMTableConfig()) {
				for (MReferenceToTableConfig mRef : mTab.getReferenceToTableConfig()) {
					if (mRef.getTableConfig().equals(this)) {
						referenceIn.add(mRef);
					}
				}
			}

			// Checks every existing TableConfig if its intendedClass == one of
			// the super classes
			for (MTableConfig goalTab : this.getContainingEditorConfig().getMTableConfig()) {
				for (MClassifier superClass : this.getIntendedClass().getSuperType()) {
					if (superClass.equals(goalTab.getIntendedClass())) {
						for (MReferenceToTableConfig mRef : referenceIn) {
							currentTrigger.addReferenceUpdate(mRef,
									MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, goalTab, null, null);
						}

						// Is the table which will be deleted the header table?
						if (this.equals(this.getContainingEditorConfig().getHeaderTableConfig())) {
							currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
									MtableeditorPackage.Literals.MEDITOR_CONFIG__HEADER_TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, goalTab, null, null);
						}

						/*
						 * Checks if the goalTableConfig has a column with the
						 * same name A Column with the same name is a sign that
						 * the ClassToCell of the baseColumn should be added to
						 * that goalColumn and not as a new column
						 */
						for (MColumnConfig baseColumn : this.getColumnConfig()) {
							for (MColumnConfig goalColumn : goalTab.getColumnConfig()) {
								if (goalColumn.getECName().equals(baseColumn.getECName())) {
									/*
									 * Checks if the goalColumn already has the
									 * same ClassToCell containment or adds them
									 * Therefore it checks if goalClassToCells
									 * with the same cellKind also have the same
									 * input If this is the case it will break
									 * the loop otherwise it will add the
									 * baseClassToCell to the goalColumn
									 */
									for (MClassToCellConfig baseClassToCell : baseColumn.getClassToCellConfig()) {
										Boolean found = false;
										for (MClassToCellConfig goalClassToCell : goalColumn.getClassToCellConfig()) {
											if (baseClassToCell.getCellKind() == goalClassToCell.getCellKind()) {
												if (baseClassToCell.getCellKind().equals("RowFeatureCell")) {
													if (((MRowFeatureCell) baseClassToCell).getFeature()
															.equals(((MRowFeatureCell) goalClassToCell).getFeature())) {
														found = true;
														if (!goalClassToCell.getClass_()
																.equals(baseClassToCell.getClass_())) {
															found = false;
														}
													}
												} else {
													if (baseClassToCell.getCellKind().equals("OclCell")) {
														if (((MOclCell) baseClassToCell)
																.getStringRendering() == ((MOclCell) goalClassToCell)
																		.getStringRendering()) {
															found = true;
															if (!goalClassToCell.getClass_()
																	.equals(baseClassToCell.getClass_())) {
																found = false;
															}
														}
													}
												}
											}
											// if there is a ClassToCell of the
											// same kind and with the same
											// input, there is no need to search
											// anymore
											if (found) {
												break;
											}
										}
										// it doesn't find a goalClassToCell of
										// the same kind and with the same input
										// and add the baseClassToCell
										if (found == false) {
											MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
													.createNextStateNewObject(
															MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

											if (baseClassToCell.getCellKind().equals("RowFeatureCell")) {
												MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
														.createNextStateNewObject(
																MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
												newClassCell.setClass(baseClassToCell.getClass_());
												newClassCell.setCellConfig(newRowCell);
												newRowCell.setClass(baseClassToCell.getClass_());
												MRowFeatureCell mFeatureCell = (MRowFeatureCell) baseClassToCell;
												newRowCell.setFeature(mFeatureCell.getFeature());
												newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
												newRowCell.setCellLocateBehavior(mFeatureCell.getCellLocateBehavior());
												newRowCell.setExplicitHighlightOCL(
														mFeatureCell.getExplicitHighlightOCL());
												currentTrigger.addReferenceUpdate(goalColumn,
														MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
											} else {
												if (baseClassToCell.getCellKind().equals("OclCell")) {
													MOclCell newRowCell = (MOclCell) currentTrigger
															.createNextStateNewObject(
																	MtableeditorPackage.eINSTANCE.getMOclCell());
													newClassCell.setClass(baseClassToCell.getClass_());
													newClassCell.setCellConfig(newRowCell);
													newRowCell.setClass(baseClassToCell.getClass_());
													MOclCell mOclCell = (MOclCell) baseClassToCell;
													newRowCell.setStringRendering(mOclCell.getStringRendering());
													newRowCell.setExplicitHighlightOCL(
															mOclCell.getExplicitHighlightOCL());
													currentTrigger.addReferenceUpdate(goalColumn,
															MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null,
															null);
												}
											}
										}
									}
									break;
								}
								/*
								 * The name and label of the columns aren't the
								 * same what doesn't mean that the goalColumn
								 * doesn't already uses one of the MProperties
								 * -> Currently it just add the column in this
								 * case
								 */
								else {
									// All Columns were checked
									if (goalColumn.equals(
											goalTab.getColumnConfig().get(goalTab.getColumnConfig().size() - 1))) {
										MColumnConfig newColumn = (MColumnConfig) currentTrigger
												.createNextStateNewObject(
														MtableeditorPackage.eINSTANCE.getMColumnConfig());
										newColumn.setName(baseColumn.getName());
										newColumn.setLabel(baseColumn.getLabel());
										newColumn.setWidth(baseColumn.getWidth());

										currentTrigger.addReferenceUpdate(goalTab,
												MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);

										for (MClassToCellConfig mClassToCell : baseColumn.getClassToCellConfig()) {
											MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
													.createNextStateNewObject(
															MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

											if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
												MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
														.createNextStateNewObject(
																MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
												newClassCell.setClass(mClassToCell.getClass_());
												newClassCell.setCellConfig(newRowCell);
												newRowCell.setClass(mClassToCell.getClass_());
												MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
												newRowCell.setFeature(mFeatureCell.getFeature());
												newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
												newRowCell.setCellLocateBehavior(mFeatureCell.getCellLocateBehavior());
												newRowCell.setExplicitHighlightOCL(
														mFeatureCell.getExplicitHighlightOCL());
												currentTrigger.addReferenceUpdate(goalTab,
														MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
												currentTrigger.addReferenceUpdate(newColumn,
														MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
											} else {
												if (mClassToCell.getCellKind().equals("OclCell")) {
													MOclCell newRowCell = (MOclCell) currentTrigger
															.createNextStateNewObject(
																	MtableeditorPackage.eINSTANCE.getMOclCell());
													newClassCell.setClass(mClassToCell.getClass_());
													newClassCell.setCellConfig(newRowCell);
													newRowCell.setClass(mClassToCell.getClass_());
													MOclCell mOclCell = (MOclCell) mClassToCell;
													newRowCell.setStringRendering(mOclCell.getStringRendering());
													newRowCell.setExplicitHighlightOCL(
															mOclCell.getExplicitHighlightOCL());
													currentTrigger.addReferenceUpdate(goalTab,
															MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null,
															null);
													currentTrigger.addReferenceUpdate(newColumn,
															MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null,
															null);
												}
											}
										}
									}
								}
							}
						}
						for (MReferenceToTableConfig baseRef : this.getReferenceToTableConfig()) {
							Boolean found = false;
							for (MReferenceToTableConfig goalRef : goalTab.getReferenceToTableConfig()) {
								if (goalRef.getReference().equals(baseRef.getReference())) {
									found = true;
									break;
								}
							}
							if (found == false) {
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.ADD, XAddUpdateMode.LAST, baseRef, null, null);
							}
						}
						return currentTrigger;
					} else {
						// Checks if all tables were checked.
						if (goalTab.equals(this.getContainingEditorConfig().getMTableConfig()
								.get(this.getContainingEditorConfig().getMTableConfig().size() - 1))) {
							MTableConfig newTable = (MTableConfig) currentTrigger
									.createNextStateNewObject(MtableeditorPackage.Literals.MTABLE_CONFIG);
							currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
									MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.ADD,
									XAddUpdateMode.LAST, newTable, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_HEADER, XUpdateMode.REDEFINE,
									null, true, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN,
									XUpdateMode.REDEFINE, null, false, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__NAME, XUpdateMode.REDEFINE, null,
									superClass.getName(), null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
									superClass.getName(), null, null);
							currentTrigger.addReferenceUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_CLASS, XUpdateMode.REDEFINE,
									null, superClass, null, null);
							currentTrigger.addReferenceUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_PACKAGE, XUpdateMode.REDEFINE,
									null, superClass.getContainingPackage(), null, null);

							for (MReferenceToTableConfig mRef : referenceIn) {
								currentTrigger.addReferenceUpdate(mRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
										XUpdateMode.REDEFINE, null, newTable, null, null);
							}

							for (MColumnConfig mColumn : this.getColumnConfig()) {
								MColumnConfig newColumn = (MColumnConfig) currentTrigger
										.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
								newColumn.setName(mColumn.getName());
								newColumn.setLabel(mColumn.getLabel());
								newColumn.setWidth(mColumn.getWidth());

								currentTrigger.addReferenceUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
										XAddUpdateMode.LAST, newColumn, null, null);

								for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
									MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
											.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

									if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
										MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
												.createNextStateNewObject(
														MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
										newClassCell.setClass(mClassToCell.getClass_());
										newClassCell.setCellConfig(newRowCell);
										newRowCell.setClass(mClassToCell.getClass_());
										MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
										newRowCell.setFeature(mFeatureCell.getFeature());
										newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
										newRowCell.setCellLocateBehavior(mFeatureCell.getCellLocateBehavior());
										newRowCell.setExplicitHighlightOCL(mFeatureCell.getExplicitHighlightOCL());
										currentTrigger.addReferenceUpdate(newTable,
												MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
										currentTrigger.addReferenceUpdate(newColumn,
												MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
									} else {
										if (mClassToCell.getCellKind().equals("OclCell")) {
											MOclCell newRowCell = (MOclCell) currentTrigger.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMOclCell());
											newClassCell.setClass(mClassToCell.getClass_());
											newClassCell.setCellConfig(newRowCell);
											newRowCell.setClass(mClassToCell.getClass_());
											MOclCell mOclCell = (MOclCell) mClassToCell;
											newRowCell.setStringRendering(mOclCell.getStringRendering());
											newRowCell.setExplicitHighlightOCL(mOclCell.getExplicitHighlightOCL());
											currentTrigger.addReferenceUpdate(newTable,
													MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
											currentTrigger.addReferenceUpdate(newColumn,
													MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
										}
									}
								}
							}

							// Adds the references of this table to the
							// container, so they aren't lost
							for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
								MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
										.createNextStateNewObject(
												MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
								newRef.setLabel(mref.getLabel());
								currentTrigger.addReferenceUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
										XUpdateMode.REDEFINE, null, mref.getReference(), null, null);
								if (mref.getTableConfig().equals(this)) {
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, newTable, null, null);
								} else
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, mref.getTableConfig(), null, null);
							}
						}
					}
				}
			}

			return currentTrigger;
		}

		case MTableConfigAction.SPLIT_SPECIALIZATIONS_VALUE: {
			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMTableConfigAction()
					.getEEnumLiteral(MTableConfigAction.SPLIT_SPECIALIZATIONS_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			// This ArrayList is necessary to avoid the out of index error,
			// triggered by undo this action
			ArrayList<MClassifier> subClasses = new ArrayList<MClassifier>(this.getIntendedClass().allDirectSubTypes());
			// will check if the amount of removed columns and references equals
			// the columns of the table
			int removedColumnsRef = 0;

			// Checks every existing TableConfig if its intendedClass == one of
			// its direct sub classes
			for (MClassifier subClass : subClasses) {
				MTableConfig refTab = null;
				for (MTableConfig goalTab : this.getContainingEditorConfig().getMTableConfig()) {
					if (subClass.equals(goalTab.getIntendedClass())) {
						refTab = goalTab;
						/*
						 * Checks if the goalTableConfig has a column with the
						 * same name A Column with the same name is a sign that
						 * the ClassToCell of the baseColumn should be added to
						 * that goalColumn and not as a new column
						 */
						for (MColumnConfig baseColumn : this.getColumnConfig()) {
							ArrayList<MClassToCellConfig> cellsOfSubClass = new ArrayList<MClassToCellConfig>();
							ArrayList<MClassToCellConfig> cellsOfAllSubClasses = new ArrayList<MClassToCellConfig>();
							for (MClassToCellConfig baseClassToCell : baseColumn.getClassToCellConfig()) {
								if (subClass.equals(baseClassToCell.getClass_())) {
									cellsOfSubClass.add(baseClassToCell);
								}
								if (subClasses.contains(baseClassToCell.getClass_())) {
									cellsOfAllSubClasses.add(baseClassToCell);
								}
							}

							// Remove the cells or even the complete column
							if (cellsOfAllSubClasses.size() == baseColumn.getClassToCellConfig().size()) {
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.REMOVE,
										null, baseColumn, null, null);
								removedColumnsRef += 1;
							} else {
								currentTrigger.addReferenceUpdate(baseColumn,
										MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
										XUpdateMode.REMOVE, null, cellsOfSubClass, null, null);
							}

							if (goalTab.getColumnConfig().isEmpty()) {
								// Add the new columns
								MColumnConfig newColumn = (MColumnConfig) currentTrigger
										.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
								newColumn.setName(baseColumn.getName());
								newColumn.setLabel(baseColumn.getLabel());
								newColumn.setWidth(baseColumn.getWidth());
								for (MClassToCellConfig baseClassToCell : cellsOfSubClass) {
									currentTrigger.addReferenceUpdate(newColumn,
											MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
											XUpdateMode.ADD, XAddUpdateMode.LAST, baseClassToCell, null, null);
								}
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
										XAddUpdateMode.LAST, newColumn, null, null);

							} else {
								for (MColumnConfig goalColumn : goalTab.getColumnConfig()) {
									if (goalColumn.getECName().equals(baseColumn.getECName())) {
										/*
										 * Checks if the goalColumn already has
										 * the same ClassToCell containment or
										 * adds them Therefore it checks if
										 * goalClassToCells with the same
										 * cellKind also have the same input If
										 * this is the case it will break the
										 * loop otherwise it will add the
										 * baseClassToCell to the goalColumn
										 */
										for (MClassToCellConfig baseClassToCell : cellsOfSubClass) {
											Boolean found = false;
											for (MClassToCellConfig goalClassToCell : goalColumn
													.getClassToCellConfig()) {
												if (baseClassToCell.getCellKind() == goalClassToCell.getCellKind()) {
													if (baseClassToCell.getCellKind().equals("RowFeatureCell")) {
														if (((MRowFeatureCell) baseClassToCell).getFeature().equals(
																((MRowFeatureCell) goalClassToCell).getFeature())) {
															found = true;
															if (!goalClassToCell.getClass_()
																	.equals(baseClassToCell.getClass_())) {
																found = false;
															}
														}
													} else {
														if (baseClassToCell.getCellKind().equals("OclCell")) {
															if (((MOclCell) baseClassToCell)
																	.getStringRendering() == ((MOclCell) goalClassToCell)
																			.getStringRendering()) {
																found = true;
																if (!goalClassToCell.getClass_()
																		.equals(baseClassToCell.getClass_())) {
																	found = false;
																}
															}
														}
													}
												}
												// if there is a ClassToCell of
												// the same kind and with the
												// same input, there is no need
												// to search anymore
												if (found) {
													break;
												}
											}
											// it doesn't find a goalClassToCell
											// of the same kind and with the
											// same input and add the
											// baseClassToCell
											if (found == false) {
												currentTrigger.addReferenceUpdate(goalColumn,
														MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, baseClassToCell, null,
														null);
											}
										}
										break;
									}
									/*
									 * The name and label of the columns aren't
									 * the same what doesn't mean that the
									 * goalColumn doesn't already uses one of
									 * the MProperties -> Currently it just add
									 * the column in this case
									 */
									else {
										// All Columns were checked
										if (goalColumn.equals(
												goalTab.getColumnConfig().get(goalTab.getColumnConfig().size() - 1))) {
											MColumnConfig newColumn = (MColumnConfig) currentTrigger
													.createNextStateNewObject(
															MtableeditorPackage.eINSTANCE.getMColumnConfig());
											newColumn.setName(baseColumn.getName());
											newColumn.setLabel(baseColumn.getLabel());
											newColumn.setWidth(baseColumn.getWidth());
											for (MClassToCellConfig baseClassToCell : cellsOfSubClass) {
												currentTrigger.addReferenceUpdate(newColumn,
														MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, baseClassToCell, null,
														null);
											}
											currentTrigger.addReferenceUpdate(goalTab,
													MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
										}
									}
								}
							}
						}

						ArrayList<MReferenceToTableConfig> refOfSubClass = new ArrayList<MReferenceToTableConfig>();
						for (MReferenceToTableConfig baseRef : this.getReferenceToTableConfig()) {
							if (baseRef.getReference().getContainingClassifier().equals(subClass)) {
								refOfSubClass.add(baseRef);
								removedColumnsRef += 1;
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.REMOVE, null, baseRef, null, null);
							}
						}
						// Checks references
						for (MReferenceToTableConfig baseRef : refOfSubClass) {
							Boolean found = false;
							for (MReferenceToTableConfig goalRef : goalTab.getReferenceToTableConfig()) {
								if (goalRef.getReference().equals(baseRef.getReference())) {
									found = true;
									break;
								}
							}
							if (found == false) {
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.ADD, XAddUpdateMode.LAST, baseRef, null, null);
							}
						}
					} else {
						// All Tables were checked
						if (goalTab.equals(this.getContainingEditorConfig().getMTableConfig()
								.get(this.getContainingEditorConfig().getMTableConfig().size() - 1))) {
							MTableConfig newTable = (MTableConfig) currentTrigger
									.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMTableConfig());
							refTab = newTable;
							currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
									MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.ADD,
									XAddUpdateMode.LAST, newTable, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_HEADER, XUpdateMode.REDEFINE,
									null, true, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN,
									XUpdateMode.REDEFINE, null, false, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__NAME, XUpdateMode.REDEFINE, null,
									subClass.getName(), null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
									subClass.getName(), null, null);
							currentTrigger.addReferenceUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_CLASS, XUpdateMode.REDEFINE,
									null, subClass, null, null);
							currentTrigger.addReferenceUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_PACKAGE, XUpdateMode.REDEFINE,
									null, subClass.getContainingPackage(), null, null);

							// Adds references with property of sub class
							for (MReferenceToTableConfig mRef : this.getReferenceToTableConfig()) {
								if (mRef.getReference().getContainingClassifier().equals(subClass)) {
									removedColumnsRef += 1;
									currentTrigger.addReferenceUpdate(goalTab,
											MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
											XUpdateMode.REMOVE, null, mRef, null, null);
									currentTrigger.addReferenceUpdate(goalTab,
											MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
											XUpdateMode.ADD, XAddUpdateMode.LAST, mRef, null, null);
								}
							}

							// Adds columns with cells of the sub class
							for (MColumnConfig baseColumn : this.getColumnConfig()) {

								ArrayList<MClassToCellConfig> cellsOfSubClass = new ArrayList<MClassToCellConfig>();
								ArrayList<MClassToCellConfig> cellsOfAllSubClasses = new ArrayList<MClassToCellConfig>();
								for (MClassToCellConfig baseClassToCell : baseColumn.getClassToCellConfig()) {
									if (subClass.equals(baseClassToCell.getClass_())) {
										cellsOfSubClass.add(baseClassToCell);
									}
									if (subClasses.contains(baseClassToCell.getClass_())) {
										cellsOfAllSubClasses.add(baseClassToCell);
									}
								}

								// Remove the cells or even the complete column
								if (cellsOfAllSubClasses.size() == baseColumn.getClassToCellConfig().size()) {
									currentTrigger.addReferenceUpdate(goalTab,
											MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
											XUpdateMode.REMOVE, null, baseColumn, null, null);
									removedColumnsRef += 1;
								} else {
									currentTrigger.addReferenceUpdate(baseColumn,
											MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
											XUpdateMode.REMOVE, null, cellsOfSubClass, null, null);
								}

								if (!cellsOfSubClass.isEmpty()) {
									MColumnConfig newColumn = (MColumnConfig) currentTrigger
											.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
									newColumn.setName(baseColumn.getName());
									newColumn.setLabel(baseColumn.getLabel());
									newColumn.setWidth(baseColumn.getWidth());

									for (MClassToCellConfig baseClassToCell : cellsOfSubClass) {
										currentTrigger.addReferenceUpdate(newColumn,
												MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, baseClassToCell, null, null);
									}

									currentTrigger.addReferenceUpdate(newTable,
											MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
											XAddUpdateMode.LAST, newColumn, null, null);
								}
							}
						}
					}
				}

				// Change the reference of a table which has the sub class as
				// containment
				for (MTableConfig goalTab : this.getContainingEditorConfig().getMTableConfig()) {
					for (MReferenceToTableConfig mRef : goalTab.getReferenceToTableConfig()) {
						if (mRef.getReference().getType().equals(subClass)) {
							currentTrigger.addReferenceUpdate(mRef,
									MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, refTab, null, null);
						}
					}
				}
			}

			// Removes complete table if it is empty
			if ((this.getColumnConfig().size() + this.getReferenceToTableConfig().size()) == removedColumnsRef) {
				currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.REMOVE, null, this,
						null, null);
			}

			return currentTrigger;
		}

		case MTableConfigAction.MERGE_CONTAINER_VALUE: {
			EEnumLiteral eEnumLiteralTrigger = MtableeditorPackage.Literals.MTABLE_CONFIG_ACTION
					.getEEnumLiteral(mTableConfigAction.getValue());
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					eEnumLiteralTrigger, null, null);

			/*
			 * checks for all classes, which contains the intendedClass, if a
			 * table is already existing
			 */
			for (MClassifier containerClass : this.getIntendedClass().getClassescontainedin()) {
				for (MTableConfig goalTab : this.getContainingEditorConfig().getMTableConfig()) {

					// The table already exists with the intendedClass
					if (containerClass.equals(goalTab.getIntendedClass())) {

						// adds the new columns
						for (MColumnConfig mColumn : this.getColumnConfig()) {

							if (goalTab.getColumnConfig().isEmpty()) {
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
										XAddUpdateMode.LAST, this.getColumnConfig(), null, null);
							} else {
								for (MColumnConfig goalColumn : goalTab.getColumnConfig()) {
									if (goalColumn.getECName().equals(mColumn.getECName())) {
										/*
										 * Checks if the goalColumn already has
										 * the same ClassToCell containment or
										 * adds them Therefore it checks if
										 * goalClassToCells with the same
										 * cellKind also have the same input If
										 * this is the case it will break the
										 * loop otherwise it will add the
										 * baseClassToCell to the goalColumn
										 */
										for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
											Boolean found = false;
											for (MClassToCellConfig goalClassToCell : goalColumn
													.getClassToCellConfig()) {
												if (mClassToCell.getCellKind() == goalClassToCell.getCellKind()) {
													if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
														if (((MRowFeatureCell) mClassToCell).getFeature().equals(
																((MRowFeatureCell) goalClassToCell).getFeature())) {
															found = true;
															if (!goalClassToCell.getClass_()
																	.equals(mClassToCell.getClass_())) {
																found = false;
															}
														}
													} else {
														if (mClassToCell.getCellKind().equals("OclCell")) {
															if (((MOclCell) mClassToCell)
																	.getStringRendering() == ((MOclCell) goalClassToCell)
																			.getStringRendering()) {
																found = true;
																if (!goalClassToCell.getClass_()
																		.equals(mClassToCell.getClass_())) {
																	found = false;
																}
															}
														}
													}
												}
												// if there is a ClassToCell of
												// the same kind and with the
												// same input, there is no need
												// to search anymore
												if (found) {
													break;
												}
											}
											// it doesn't find a goalClassToCell
											// of the same kind and with the
											// same input and add the
											// baseClassToCell
											if (found == false) {
												MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
														.createNextStateNewObject(
																MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

												if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
													MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
															.createNextStateNewObject(
																	MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
													newClassCell.setClass(mClassToCell.getClass_());
													newClassCell.setCellConfig(newRowCell);
													newRowCell.setClass(mClassToCell.getClass_());
													MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
													newRowCell.setFeature(mFeatureCell.getFeature());
													newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
													newRowCell.setCellLocateBehavior(
															mFeatureCell.getCellLocateBehavior());
													newRowCell.setExplicitHighlightOCL(
															mFeatureCell.getExplicitHighlightOCL());
													currentTrigger.addReferenceUpdate(goalColumn,
															MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null,
															null);
												} else {
													if (mClassToCell.getCellKind().equals("OclCell")) {
														MOclCell newRowCell = (MOclCell) currentTrigger
																.createNextStateNewObject(
																		MtableeditorPackage.eINSTANCE.getMOclCell());
														newClassCell.setClass(mClassToCell.getClass_());
														newClassCell.setCellConfig(newRowCell);
														newRowCell.setClass(mClassToCell.getClass_());
														MOclCell mOclCell = (MOclCell) mClassToCell;
														newRowCell.setStringRendering(mOclCell.getStringRendering());
														newRowCell.setExplicitHighlightOCL(
																mOclCell.getExplicitHighlightOCL());
														currentTrigger.addReferenceUpdate(goalColumn,
																MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
																XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null,
																null);
													}
												}
											}
										}
										break;
									}
									/*
									 * The name of the columns aren't the same
									 * what doesn't mean that the goalColumn
									 * doesn't already uses one of the
									 * MProperties -> Currently it just add the
									 * column in this case
									 */
									else {
										/*
										 * All Columns were checked It adds the
										 * column by creating a new one and
										 * creating all its cells
										 */
										if (goalColumn.equals(
												goalTab.getColumnConfig().get(goalTab.getColumnConfig().size() - 1))) {
											MColumnConfig newColumn = (MColumnConfig) currentTrigger
													.createNextStateNewObject(
															MtableeditorPackage.eINSTANCE.getMColumnConfig());
											newColumn.setName(mColumn.getName());
											newColumn.setLabel(mColumn.getLabel());
											newColumn.setWidth(mColumn.getWidth());

											currentTrigger.addReferenceUpdate(goalTab,
													MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);

											for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
												MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
														.createNextStateNewObject(
																MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

												if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
													MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
															.createNextStateNewObject(
																	MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
													newClassCell.setClass(mClassToCell.getClass_());
													newClassCell.setCellConfig(newRowCell);
													newRowCell.setClass(mClassToCell.getClass_());
													MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
													newRowCell.setFeature(mFeatureCell.getFeature());
													newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
													newRowCell.setCellLocateBehavior(
															mFeatureCell.getCellLocateBehavior());
													newRowCell.setExplicitHighlightOCL(
															mFeatureCell.getExplicitHighlightOCL());
													currentTrigger.addReferenceUpdate(goalTab,
															MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null,
															null);
													currentTrigger.addReferenceUpdate(newColumn,
															MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null,
															null);
												} else {
													if (mClassToCell.getCellKind().equals("OclCell")) {
														MOclCell newRowCell = (MOclCell) currentTrigger
																.createNextStateNewObject(
																		MtableeditorPackage.eINSTANCE.getMOclCell());
														newClassCell.setClass(mClassToCell.getClass_());
														newClassCell.setCellConfig(newRowCell);
														newRowCell.setClass(mClassToCell.getClass_());
														MOclCell mOclCell = (MOclCell) mClassToCell;
														newRowCell.setStringRendering(mOclCell.getStringRendering());
														newRowCell.setExplicitHighlightOCL(
																mOclCell.getExplicitHighlightOCL());
														currentTrigger.addReferenceUpdate(goalTab,
																MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
																XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null,
																null);
														currentTrigger.addReferenceUpdate(newColumn,
																MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
																XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null,
																null);
													}
												}
											}
										}
									}
								}
							}
						}

						/*
						 * It redefines the reference to the subtable or adds a
						 * new one so the MTE can work correct
						 */
						List<MTableConfig> usedMTab = new ArrayList<MTableConfig>();
						for (MReferenceToTableConfig mref : goalTab.getReferenceToTableConfig()) {
							usedMTab.add(mref.getTableConfig());
						}

						if (usedMTab.contains(this)) {
							for (MReferenceToTableConfig mRef : goalTab.getReferenceToTableConfig()) {
								if (mRef.getTableConfig().equals(this)) {
									currentTrigger.addReferenceUpdate(mRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, goalTab, null, null);
								}
							}
						} else {
							MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
									.createNextStateNewObject(
											MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
							currentTrigger.addReferenceUpdate(goalTab,
									MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
									XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
							currentTrigger.addReferenceUpdate(newRef,
									MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, goalTab, null, null);
							for (MProperty mprop : containerClass.allContainmentReferences()) {
								if (mprop.getType().equals(this.intendedClass)) {
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
											XUpdateMode.REDEFINE, null, mprop, null, null);
									break;
								}
							}
						}

						// Adds the references of this table to the container,
						// so they aren't lost
						for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
							MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
									.createNextStateNewObject(
											MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
							newRef.setLabel(mref.getLabel());
							currentTrigger.addReferenceUpdate(goalTab,
									MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
									XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);

							if (mref.getTableConfig().equals(this)) {
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
										XUpdateMode.REDEFINE, null, goalTab, null, null);
							} else
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
										XUpdateMode.REDEFINE, null, mref.getTableConfig(), null, null);
						}

						break;
					} else {
						// Checks if all tables were checked.
						if (goalTab.equals(this.getContainingEditorConfig().getMTableConfig()
								.get(this.getContainingEditorConfig().getMTableConfig().size() - 1))) {
							MTableConfig newTable = (MTableConfig) currentTrigger
									.createNextStateNewObject(MtableeditorPackage.Literals.MTABLE_CONFIG);
							currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
									MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.ADD,
									XAddUpdateMode.LAST, newTable, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_HEADER, XUpdateMode.REDEFINE,
									null, true, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN,
									XUpdateMode.REDEFINE, null, false, null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__NAME, XUpdateMode.REDEFINE, null,
									containerClass.getName(), null, null);
							currentTrigger.addAttributeUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
									containerClass.getName(), null, null);
							currentTrigger.addReferenceUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_CLASS, XUpdateMode.REDEFINE,
									null, containerClass, null, null);
							currentTrigger.addReferenceUpdate(newTable,
									MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_PACKAGE, XUpdateMode.REDEFINE,
									null, containerClass.getContainingPackage(), null, null);

							for (MColumnConfig mColumn : this.getColumnConfig()) {
								MColumnConfig newColumn = (MColumnConfig) currentTrigger
										.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
								newColumn.setName(mColumn.getName());
								newColumn.setLabel(mColumn.getLabel());
								newColumn.setWidth(mColumn.getWidth());

								currentTrigger.addReferenceUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
										XAddUpdateMode.LAST, newColumn, null, null);

								for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
									MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
											.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

									if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
										MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
												.createNextStateNewObject(
														MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
										newClassCell.setClass(mClassToCell.getClass_());
										newClassCell.setCellConfig(newRowCell);
										newRowCell.setClass(mClassToCell.getClass_());
										MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
										newRowCell.setFeature(mFeatureCell.getFeature());
										newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
										newRowCell.setCellLocateBehavior(mFeatureCell.getCellLocateBehavior());
										newRowCell.setExplicitHighlightOCL(mFeatureCell.getExplicitHighlightOCL());
										currentTrigger.addReferenceUpdate(goalTab,
												MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
										currentTrigger.addReferenceUpdate(newColumn,
												MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
									} else {
										if (mClassToCell.getCellKind().equals("OclCell")) {
											MOclCell newRowCell = (MOclCell) currentTrigger.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMOclCell());
											newClassCell.setClass(mClassToCell.getClass_());
											newClassCell.setCellConfig(newRowCell);
											newRowCell.setClass(mClassToCell.getClass_());
											MOclCell mOclCell = (MOclCell) mClassToCell;
											newRowCell.setStringRendering(mOclCell.getStringRendering());
											newRowCell.setExplicitHighlightOCL(mOclCell.getExplicitHighlightOCL());
											currentTrigger.addReferenceUpdate(goalTab,
													MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
											currentTrigger.addReferenceUpdate(newColumn,
													MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
										}
									}
								}
							}

							// Adds the references of this table to the
							// container, so they aren't lost
							for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
								MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
										.createNextStateNewObject(
												MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
								newRef.setLabel(mref.getLabel());
								currentTrigger.addReferenceUpdate(goalTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
										XUpdateMode.REDEFINE, null, mref.getReference(), null, null);
								if (mref.getTableConfig().equals(this)) {
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, goalTab, null, null);
								} else
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, mref.getTableConfig(), null, null);
							}

							/*
							 * It adds a new one so the MTE can work correct
							 */
							MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
									.createNextStateNewObject(
											MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
							currentTrigger.addReferenceUpdate(goalTab,
									MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
									XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
							currentTrigger.addReferenceUpdate(newRef,
									MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, goalTab, null, null);
							for (MProperty mprop : containerClass.allContainmentReferences()) {
								if (mprop.getType().equals(this.intendedClass)) {
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
											XUpdateMode.REDEFINE, null, mprop, null, null);
									break;
								}
							}
						}
					}
				}
			}

			// currently not working, if there was the need to add a new table
			currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
					MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.REMOVE, null, this, null,
					null);
			return currentTrigger;
		}

		case MTableConfigAction.SPLIT_CHILDREN_VALUE: {
			EEnumLiteral eEnumLiteralTrigger = MtableeditorPackage.Literals.MTABLE_CONFIG_ACTION
					.getEEnumLiteral(mTableConfigAction.getValue());
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					eEnumLiteralTrigger, null, null);

			/*
			 * Gets all containments which have the type MClassifier as Set to
			 * avoid duplicates
			 */
			Set<MClassifier> containedClasses = new HashSet<MClassifier>();
			for (MProperty mprop : this.getIntendedClass().allContainmentReferences()) {
				if (mprop.getType() instanceof MClassifier) {
					containedClasses.add(mprop.getType());
					containedClasses.addAll(getDeepestContainment(mprop.getType()));
				}
			}

			for (MClassifier mClass : containedClasses) {
				/*
				 * Checks first if any contained class has cells in this table,
				 * saves them in the Array and removes them from the table. The
				 * counter checks if the column is empty after removing the
				 * cell(s) and removes if necessary also the column
				 */
				List<MClassToCellConfig> cellsOfContainment = new ArrayList<MClassToCellConfig>();
				List<MReferenceToTableConfig> refOfContainment = new ArrayList<MReferenceToTableConfig>();
				for (MColumnConfig mColumn : this.getColumnConfig()) {
					int counter = 0;
					for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
						if (mClassToCell.getClass_().equals(mClass)) {
							cellsOfContainment.add(mClassToCell);
							counter++;
							currentTrigger.addReferenceUpdate(mColumn,
									MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
									XUpdateMode.REMOVE, null, mClassToCell, null, null);
						}
					}
					if (counter == mColumn.getClassToCellConfig().size()) {
						currentTrigger.addReferenceUpdate(this,
								MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.REMOVE, null,
								mColumn, null, null);
					}
				}

				/*
				 * Checks if any references of the contained class in in the
				 * table, saves them in the Array and removes them.
				 */
				for (MReferenceToTableConfig mRef : this.getReferenceToTableConfig()) {
					for (MProperty mprop : mClass.allContainmentReferences()) {
						if (mRef.getReference().equals(mprop)) {
							refOfContainment.add(mRef);
							currentTrigger.addReferenceUpdate(this,
									MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
									XUpdateMode.REMOVE, null, mRef, null, null);
							break;
						}
					}
				}

				// If there is nothing to move, it will check the next class
				if (cellsOfContainment.isEmpty() == false || refOfContainment.isEmpty() == false) {
					for (MTableConfig mTab : this.getContainingEditorConfig().getMTableConfig()) {
						/*
						 * Checks if a table for the contained class already
						 * exist
						 */
						if (mTab.getIntendedClass().equals(mClass)) {
							/*
							 * Checks and creates new columns or cells
							 */
							if (mTab.getColumnConfig().isEmpty()) {
								for (MClassToCellConfig mClassToCell : cellsOfContainment) {
									MColumnConfig newColumn = (MColumnConfig) currentTrigger
											.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
									newColumn.setName(mClassToCell.getContainingColumnConfig().getName());
									newColumn.setLabel(mClassToCell.getContainingColumnConfig().getLabel());
									newColumn.setWidth(mClassToCell.getContainingColumnConfig().getWidth());
									currentTrigger.addReferenceUpdate(mTab,
											MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
											XAddUpdateMode.LAST, newColumn, null, null);
									currentTrigger.addReferenceUpdate(newColumn,
											MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
											XUpdateMode.ADD, XAddUpdateMode.LAST, mClassToCell, null, null);
								}
							} else {
								// adds the new columns
								for (MColumnConfig mColumn : mTab.getColumnConfig()) {
									for (MClassToCellConfig contClassToCell : cellsOfContainment) {
										if (mColumn.getName()
												.equals(contClassToCell.getContainingColumnConfig().getName())) {
											/*
											 * Checks if the Column already has
											 * the same ClassToCell containment
											 * or adds them Therefore it checks
											 * if ClassToCells with the same
											 * cellKind also have the same input
											 * If this is the case it will break
											 * the loop otherwise it will add
											 * the ClassToCell to the Column
											 */
											for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
												Boolean found = false;
												if (mClassToCell.getCellKind() == contClassToCell.getCellKind()) {
													if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
														if (((MRowFeatureCell) mClassToCell).getFeature().equals(
																((MRowFeatureCell) contClassToCell).getFeature())) {
															found = true;
															if (!contClassToCell.getClass_()
																	.equals(mClassToCell.getClass_())) {
																found = false;
															}
														}
													} else {
														if (mClassToCell.getCellKind().equals("OclCell")) {
															if (((MOclCell) mClassToCell)
																	.getStringRendering() == ((MOclCell) contClassToCell)
																			.getStringRendering()) {
																found = true;
																if (!contClassToCell.getClass_()
																		.equals(mClassToCell.getClass_())) {
																	found = false;
																}
															}
														}
													}
												}
												// if there is a ClassToCell of
												// the same kind and with the
												// same input, there is no need
												// to search anymore
												if (found) {
													break;
												}
												// it doesn't find a ClassToCell
												// of the same kind and with the
												// same input and checked all
												// then it adds the ClassToCell
												if (found == false && mClassToCell.equals(mColumn.getClassToCellConfig()
														.get(mColumn.getClassToCellConfig().size() - 1))) {
													currentTrigger.addReferenceUpdate(mColumn,
															MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
															XUpdateMode.ADD, XAddUpdateMode.LAST, contClassToCell, null,
															null);
												}
											}
										}
										// There is no column
										else {
											if (containedClasses
													.equals(cellsOfContainment.get(cellsOfContainment.size() - 1))) {
												MColumnConfig newColumn = (MColumnConfig) currentTrigger
														.createNextStateNewObject(
																MtableeditorPackage.eINSTANCE.getMColumnConfig());
												newColumn
														.setName(contClassToCell.getContainingColumnConfig().getName());
												newColumn.setLabel(
														contClassToCell.getContainingColumnConfig().getLabel());
												newColumn.setWidth(
														contClassToCell.getContainingColumnConfig().getWidth());
												currentTrigger.addReferenceUpdate(mTab,
														MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
												currentTrigger.addReferenceUpdate(newColumn,
														MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, contClassToCell, null,
														null);
											}
										}
									}
								}
							}

							/*
							 * Checks and adds reference
							 */
							for (MReferenceToTableConfig mRef : refOfContainment) {
								currentTrigger.addReferenceUpdate(mTab,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.ADD, XAddUpdateMode.LAST, mRef, null, null);
							}

							/*
							 * Add the table as reference to this table or
							 * manipulates the already existing reference
							 */
							if (this.getReferenceToTableConfig().isEmpty()) {
								MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
										.createNextStateNewObject(
												MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
								newRef.setLabel(mClass.getName());
								currentTrigger.addReferenceUpdate(this,
										MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
										XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
										XUpdateMode.REDEFINE, null, mTab, null, null);

								for (MProperty mprop : this.getIntendedClass().allContainmentReferences()) {
									if (mprop.getType().equals(mClass)) {
										currentTrigger.addReferenceUpdate(newRef,
												MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
												XUpdateMode.REDEFINE, null, mprop, null, null);
									}
								}
							} else {
								for (MReferenceToTableConfig mRef : this.getReferenceToTableConfig()) {
									if (mRef.getReference().getType().equals(mClass)) {
										currentTrigger.addReferenceUpdate(mRef,
												MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
												XUpdateMode.REDEFINE, null, mTab, null, null);
										break;
									} else {
										if (mRef.equals(this.getReferenceToTableConfig()
												.get(this.getReferenceToTableConfig().size() - 1))) {
											MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
													.createNextStateNewObject(
															MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
											newRef.setLabel(mClass.getName());
											currentTrigger.addReferenceUpdate(this,
													MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
											currentTrigger.addReferenceUpdate(newRef,
													MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
													XUpdateMode.REDEFINE, null, mTab, null, null);

											for (MProperty mprop : this.getIntendedClass().allContainmentReferences()) {
												if (mprop.getType().equals(mClass)) {
													currentTrigger.addReferenceUpdate(newRef,
															MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
															XUpdateMode.REDEFINE, null, mprop, null, null);
												}
											}
										}
									}
								}
							}

						} else {
							/*
							 * All tables were checked but none has the required
							 * intended class So it creates a new table with all
							 * the found columns and references
							 */
							if (mTab.equals(this.getContainingEditorConfig().getMTableConfig()
									.get(this.getContainingEditorConfig().getMTableConfig().size() - 1))) {
								MTableConfig newTable = (MTableConfig) currentTrigger
										.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMTableConfig());
								currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
										MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.ADD,
										XAddUpdateMode.LAST, newTable, null, null);
								currentTrigger.addAttributeUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_HEADER,
										XUpdateMode.REDEFINE, null, true, null, null);
								currentTrigger.addAttributeUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN,
										XUpdateMode.REDEFINE, null, false, null, null);
								currentTrigger.addAttributeUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__NAME, XUpdateMode.REDEFINE, null,
										mClass.getName(), null, null);
								currentTrigger.addAttributeUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
										mClass.getName(), null, null);
								currentTrigger.addReferenceUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_CLASS,
										XUpdateMode.REDEFINE, null, mClass, null, null);
								currentTrigger.addReferenceUpdate(newTable,
										MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_PACKAGE,
										XUpdateMode.REDEFINE, null, mClass.getContainingPackage(), null, null);

								for (MReferenceToTableConfig mRef : refOfContainment) {
									currentTrigger.addReferenceUpdate(newTable,
											MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
											XUpdateMode.ADD, XAddUpdateMode.LAST, mRef, null, null);
								}

								for (MClassToCellConfig mClassToCell : cellsOfContainment) {
									MColumnConfig newColumn = (MColumnConfig) currentTrigger
											.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
									newColumn.setName(mClassToCell.getContainingColumnConfig().getName());
									newColumn.setLabel(mClassToCell.getContainingColumnConfig().getLabel());
									newColumn.setWidth(mClassToCell.getContainingColumnConfig().getWidth());
									currentTrigger.addReferenceUpdate(newTable,
											MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
											XAddUpdateMode.LAST, newColumn, null, null);
									currentTrigger.addReferenceUpdate(newColumn,
											MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
											XUpdateMode.ADD, XAddUpdateMode.LAST, mClassToCell, null, null);
								}

								/*
								 * Add the new table as reference to this table
								 * or manipulates the already existing reference
								 */
								if (this.getReferenceToTableConfig().isEmpty()) {
									MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
											.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
									newRef.setLabel(mClass.getName());
									currentTrigger.addReferenceUpdate(this,
											MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
											XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, newTable, null, null);

									for (MProperty mprop : this.getIntendedClass().allContainmentReferences()) {
										if (mprop.getType().equals(mClass)) {
											currentTrigger.addReferenceUpdate(newRef,
													MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
													XUpdateMode.REDEFINE, null, mprop, null, null);
										}
									}
								} else {
									for (MReferenceToTableConfig mRef : this.getReferenceToTableConfig()) {
										if (mRef.getReference().getType().equals(mClass)) {
											currentTrigger.addReferenceUpdate(mRef,
													MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
													XUpdateMode.REDEFINE, null, newTable, null, null);
											break;
										} else {
											if (mRef.equals(this.getReferenceToTableConfig()
													.get(this.getReferenceToTableConfig().size() - 1))) {
												MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
														.createNextStateNewObject(MtableeditorPackage.eINSTANCE
																.getMReferenceToTableConfig());
												newRef.setLabel(mClass.getName());
												currentTrigger.addReferenceUpdate(this,
														MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
														XUpdateMode.ADD, XAddUpdateMode.LAST, newRef, null, null);
												currentTrigger.addReferenceUpdate(newRef,
														MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
														XUpdateMode.REDEFINE, null, newTable, null, null);

												for (MProperty mprop : this.getIntendedClass()
														.allContainmentReferences()) {
													if (mprop.getType().equals(mClass)) {
														currentTrigger.addReferenceUpdate(newRef,
																MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE,
																XUpdateMode.REDEFINE, null, mprop, null, null);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			return currentTrigger;
		}

		case MTableConfigAction.MERGE_CHILDREN_VALUE: {
			EEnumLiteral eEnumLiteralTrigger = MtableeditorPackage.Literals.MTABLE_CONFIG_ACTION
					.getEEnumLiteral(mTableConfigAction.getValue());
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					eEnumLiteralTrigger, null, null);

			/*
			 * Gets all containments which have the type MClassifier as Set to
			 * avoid duplicates
			 */
			Set<MClassifier> containedClasses = new HashSet<MClassifier>();
			for (MProperty mprop : this.getIntendedClass().allContainmentReferences()) {
				containedClasses.add(mprop.getType());
				containedClasses.addAll(getDeepestContainment(mprop.getType()));
			}

			for (MClassifier mClass : containedClasses) {
				for (MTableConfig mTab : this.getContainingEditorConfig().getMTableConfig()) {
					if (mClass.equals(mTab.getIntendedClass())) {
						currentTrigger = mTab.doActionUpdate(currentTrigger, this, MTableConfigAction.MERGE_CONTAINER);
						break;
					}
				}
			}

			for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
				if (!mref.getTableConfig().equals(this)) {
					currentTrigger.addReferenceUpdate(mref,
							MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, XUpdateMode.REDEFINE,
							null, this, null, null);
				}
			}

			return currentTrigger;

		}

		default:
			EEnumLiteral eEnumLiteralTrigger = MtableeditorPackage.Literals.MTABLE_CONFIG_ACTION
					.getEEnumLiteral(mTableConfigAction.getValue());
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction(), XUpdateMode.REDEFINE, null,
					eEnumLiteralTrigger, null, null);

			currentTrigger.invokeOperationCall(this,
					MtableeditorPackage.Literals.MTABLE_CONFIG___INVOKE_SET_DO_ACTION__MTABLECONFIGACTION,
					mTableConfigAction, null, null);

			// Operations return the Object that shall be focused
			// -> Put focus command on that
			return currentTrigger;

		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doActionUpdate(XUpdate xUpdate, MEditorConfig mEditorConfig, MTableConfigAction mTableConfigAction,
			EList<MTableConfig> complementTable, Boolean reset) {

		/**
		 * @OCL null
		 * @templateTag IGOT01
		 */
		EClass eClass = (MtableeditorPackage.Literals.MTABLE_CONFIG);
		EOperation eOperation = MtableeditorPackage.Literals.MTABLE_CONFIG.getEOperations().get(4);
		if (doActionUpdatesemanticsXUpdatemtableeditorMEditorConfigmtableeditorMTableConfigActionmtableeditorMTableConfigecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				doActionUpdatesemanticsXUpdatemtableeditorMEditorConfigmtableeditorMTableConfigActionmtableeditorMTableConfigecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MTABLE_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				doActionUpdatesemanticsXUpdatemtableeditorMEditorConfigmtableeditorMTableConfigActionmtableeditorMTableConfigecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MTABLE_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("xUpdate", xUpdate);

			evalEnv.add("mEditorConfig", mEditorConfig);

			evalEnv.add("mTableConfigAction", mTableConfigAction);

			evalEnv.add("complementTable", complementTable);

			evalEnv.add("reset", reset);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (XUpdate) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(XUpdate currentTrigger, MEditorConfig parent, MTableConfigAction obj,
			List<MTableConfig> complementTable, Boolean reset) {

		switch (obj.getValue()) {
		/*
		 * This operation exists to add new tables and complement them without
		 * the need to addAndMerge the Trigger -> This always leads to an error
		 * The current Trigger will be used and no new one.
		 */
		case MTableConfigAction.COMPLEMENT_TABLE_VALUE: {

			if (reset) {
				List<MProperty> allUsableProperties = new ArrayList<MProperty>();
				List<MProperty> allUsedProperties = new ArrayList<MProperty>();
				// Only properties which are already in Columns and not hidden
				// for the table editor, no containments as ReferenceTable
				for (MProperty mprop : this.getIntendedClass().getProperty()) {
					if (mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED && mprop.getIsOperation() == false
							&& (mprop.getDirectSemantics() == null || mprop.getDirectSemantics().getLayout() == null
									|| mprop.getDirectSemantics().getLayout().getHideInTable() == false)) {
						allUsableProperties.add(mprop);
					}
				}

				for (MProperty mprop : this.getIntendedClass().allProperties()) {
					if (mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED && mprop.getIsOperation() == false
							&& (mprop.getDirectSemantics() == null || mprop.getDirectSemantics().getLayout() == null
									|| mprop.getDirectSemantics().getLayout().getHideInTable() == false)) {
						if (mprop.getContainment() == false && mprop.getContainingPropertiesContainer() instanceof MPropertiesGroup)
							continue;
						if (!allUsableProperties.contains(mprop)) {
							//the properties, which aren't contained, are contained in a sub or super class
							if (this.getIntendedClass().getSuperType().contains(mprop.getContainingClassifier())) {
								allUsableProperties.add(0, mprop);
							} else
								allUsableProperties.add(mprop);
						}
					}
				}

				// Checks which properties aren't contained, not hidden for the
				// table editor and are already in use
				for (MColumnConfig column : this.getColumnConfig()) {
					for (MClassToCellConfig classToCell : column.getClassToCellConfig()) {
						if (classToCell instanceof MRowFeatureCell) {
							MRowFeatureCell rowCell = (MRowFeatureCell) classToCell;
							MProperty mprop = rowCell.getFeature();
							if ((!allUsedProperties.contains(mprop)) && mprop != null
									&& mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED
									&& mprop.getIsOperation() == false
									&& (mprop.getDirectSemantics() == null
											|| mprop.getDirectSemantics().getLayout() == null
											|| mprop.getDirectSemantics().getLayout().getHideInTable() == false)) {
								allUsedProperties.add(mprop);
							}
						}
					}
				}

				for (MProperty mprop : allUsedProperties) {
					allUsableProperties.remove(mprop);
				}

				for (MProperty mprop : allUsableProperties) {
					MColumnConfig newColumn = (MColumnConfig) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());

					currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
							XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);

					MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

					MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMRowFeatureCell());

					newClassCell.setClass(this.getIntendedClass());
					newClassCell.setCellConfig(newRowCell);
					newRowCell.setClass(this.getIntendedClass());

					currentTrigger.addReferenceUpdate(newColumn,
							MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG, XUpdateMode.REDEFINE,
							null, newRowCell, null, null);
					currentTrigger.addReferenceUpdate(newRowCell,
							MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE, XUpdateMode.REDEFINE, null, mprop,
							null, null);
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
							XUpdateMode.REDEFINE, null, mprop.getEName(), null, null);
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL,
							XUpdateMode.REDEFINE, null, mprop.getCalculatedName(), null, null);
				}

				// checks containments and if a table already exist
				List<MProperty> allUsableContainments = new ArrayList<MProperty>();
				List<MProperty> allUsedContainments = new ArrayList<MProperty>();
				for (MProperty mprop : this.getIntendedClass().allProperties()) {
					if (mprop.getPropertyBehavior() == PropertyBehavior.CONTAINED) {
						allUsableContainments.add(mprop);
					}
				}

				for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
					if ((!allUsedContainments.contains(mref.getReference())) && mref.getReference() != null
							&& mref.getReference().getPropertyBehavior() == PropertyBehavior.CONTAINED) {
						allUsedContainments.add(mref.getReference());
					}
				}

				for (MProperty mprop : allUsedContainments) {
					allUsableContainments.remove(mprop);
				}

				for (MProperty mprop : allUsableContainments) {
					MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
					currentTrigger.addReferenceUpdate(this,
							MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG, XUpdateMode.ADD,
							XAddUpdateMode.LAST, newRef, null, null);

					currentTrigger.addReferenceUpdate(newRef,
							MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE, XUpdateMode.REDEFINE,
							null, mprop, null, null);
					currentTrigger.addAttributeUpdate(newRef,
							MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
							mprop.getCalculatedName(), null, null);

					Boolean search = true;
					// checks if the table is already added in this do action
					for (MTableConfig mtab : complementTable) {
						if (mtab.getIntendedClass() == mprop.getType()) {
							search = false;
							currentTrigger.addReferenceUpdate(newRef,
									MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, mtab, null, null);
							break;
						}
					}

					// If search is true there is the need to search if the
					// class already exists
					if (search) {
						for (MTableConfig mtab : complementTable) {
							if (mtab.getIntendedClass() == mprop.getType() && mprop.getType() != null) {
								search = false;
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
										XUpdateMode.REDEFINE, null, mtab, null, null);
								break;
							}
						}
					}

					// If search is true there is the need to search for a super
					// class
					if (search) {
						for (MClassifier mClass : mprop.getType().allSuperTypes()) {
							for (MTableConfig mtab : complementTable) {
								if (mtab.getIntendedClass() == mClass && mClass != null) {
									search = false;
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, mtab, null, null);
									break;
								}
							}
						}
					}

					// If there is also no super class found, search is still
					// true, then it creates the table on its own
					if (search) {
						MTableConfig newTable = MtableeditorFactory.eINSTANCE.createMTableConfig();
						newTable.setName(mprop.getType().getName());
						newTable.setLabel(mprop.getType().getName());
						newTable.setIntendedClass(mprop.getType());
						newTable.setIntendedPackage(mprop.getType().getContainingPackage());
						newTable.setDisplayHeader(true);
						newTable.setDisplayDefaultColumn(false);
						currentTrigger.addReferenceUpdate(parent,
								MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.ADD,
								XAddUpdateMode.LAST, newTable, null, null);
						currentTrigger.addReferenceUpdate(newRef,
								MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
								XUpdateMode.REDEFINE, null, newTable, null, null);

						complementTable.add(newTable);

						currentTrigger = newTable.doActionUpdate(currentTrigger, parent,
								MTableConfigAction.COMPLEMENT_TABLE, complementTable, true);

					}
				}
				return currentTrigger;
			} else {
				List<MProperty> allUsableProperties = new ArrayList<MProperty>();
				List<MProperty> allUsedProperties = new ArrayList<MProperty>();
				// Only properties which are already in Columns and not hidden
				// for the table editor, no containments as ReferenceTable
				for (MProperty mprop : this.getIntendedClass().allProperties()) {
					if (mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED && mprop.getIsOperation() == false
							&& (mprop.getDirectSemantics() == null || mprop.getDirectSemantics().getLayout() == null
									|| mprop.getDirectSemantics().getLayout().getHideInTable() == false)) {
						allUsableProperties.add(mprop);
					}
				}

				// Checks which properties aren't contained, not hidden for the
				// table editor and are already in use
				for (MColumnConfig column : this.getColumnConfig()) {
					for (MClassToCellConfig classToCell : column.getClassToCellConfig()) {
						if (classToCell instanceof MRowFeatureCell) {
							MRowFeatureCell rowCell = (MRowFeatureCell) classToCell;
							MProperty mprop = rowCell.getFeature();
							if ((!allUsedProperties.contains(mprop)) && mprop != null
									&& mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED
									&& mprop.getIsOperation() == false
									&& (mprop.getDirectSemantics() == null
											|| mprop.getDirectSemantics().getLayout() == null
											|| mprop.getDirectSemantics().getLayout().getHideInTable() == false)) {
								allUsedProperties.add(mprop);
							}
						}
					}
				}

				for (MProperty mprop : allUsedProperties) {
					allUsableProperties.remove(mprop);
				}

				for (MProperty mprop : allUsableProperties) {
					MColumnConfig newColumn = (MColumnConfig) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());

					currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
							XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);

					MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

					MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMRowFeatureCell());

					newClassCell.setClass(this.getIntendedClass());
					newClassCell.setCellConfig(newRowCell);
					newRowCell.setClass(this.getIntendedClass());

					currentTrigger.addReferenceUpdate(newColumn,
							MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG, XUpdateMode.REDEFINE,
							null, newRowCell, null, null);
					currentTrigger.addReferenceUpdate(newRowCell,
							MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE, XUpdateMode.REDEFINE, null, mprop,
							null, null);
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME,
							XUpdateMode.REDEFINE, null, mprop.getEName(), null, null);
					currentTrigger.addAttributeUpdate(newColumn, MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL,
							XUpdateMode.REDEFINE, null, mprop.getCalculatedName(), null, null);
				}

				// checks containments and if a table already exist
				List<MProperty> allUsableContainments = new ArrayList<MProperty>();
				List<MProperty> allUsedContainments = new ArrayList<MProperty>();
				for (MProperty mprop : this.getIntendedClass().allProperties()) {
					if (mprop.getPropertyBehavior() == PropertyBehavior.CONTAINED) {
						allUsableContainments.add(mprop);
					}
				}

				for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
					if ((!allUsedContainments.contains(mref.getReference())) && mref.getReference() != null
							&& mref.getReference().getPropertyBehavior() == PropertyBehavior.CONTAINED) {
						allUsedContainments.add(mref.getReference());
					}
				}

				for (MProperty mprop : allUsedContainments) {
					allUsableContainments.remove(mprop);
				}

				for (MProperty mprop : allUsableContainments) {
					MReferenceToTableConfig newRef = (MReferenceToTableConfig) currentTrigger
							.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMReferenceToTableConfig());
					currentTrigger.addReferenceUpdate(this,
							MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG, XUpdateMode.ADD,
							XAddUpdateMode.LAST, newRef, null, null);

					currentTrigger.addReferenceUpdate(newRef,
							MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__REFERENCE, XUpdateMode.REDEFINE,
							null, mprop, null, null);
					currentTrigger.addAttributeUpdate(newRef,
							MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__LABEL, XUpdateMode.REDEFINE, null,
							mprop.getCalculatedName(), null, null);

					Boolean search = true;
					// checks if the table is already added in this do action
					for (MTableConfig mtab : complementTable) {
						if (mtab.getIntendedClass() == mprop.getType()) {
							search = false;
							currentTrigger.addReferenceUpdate(newRef,
									MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
									XUpdateMode.REDEFINE, null, mtab, null, null);
							break;
						}
					}

					// If search is true there is the need to search if the
					// class already exists
					if (search) {
						for (MTableConfig mtab : parent.getMTableConfig()) {
							if (mtab.getIntendedClass() == mprop.getType() && mprop.getType() != null) {
								search = false;
								currentTrigger.addReferenceUpdate(newRef,
										MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
										XUpdateMode.REDEFINE, null, mtab, null, null);
								break;
							}
						}
					}

					// If search is true there is the need to search for a super
					// class
					if (search) {
						for (MClassifier mClass : mprop.getType().allSuperTypes()) {
							for (MTableConfig mtab : parent.getMTableConfig()) {
								if (mtab.getIntendedClass() == mClass && mClass != null) {
									search = false;
									currentTrigger.addReferenceUpdate(newRef,
											MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
											XUpdateMode.REDEFINE, null, mtab, null, null);
									break;
								}
							}
						}
					}
					// If there is also no super class found, search is still
					// true, then it creates the table on its own
					if (search) {
						MTableConfig newTable = MtableeditorFactory.eINSTANCE.createMTableConfig();
						newTable.setName(mprop.getType().getName());
						newTable.setLabel(mprop.getType().getName());
						newTable.setIntendedClass(mprop.getType());
						newTable.setIntendedPackage(mprop.getType().getContainingPackage());
						newTable.setDisplayHeader(true);
						newTable.setDisplayDefaultColumn(false);
						currentTrigger.addReferenceUpdate(parent,
								MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.ADD,
								XAddUpdateMode.LAST, newTable, null, null);
						currentTrigger.addReferenceUpdate(newRef,
								MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG,
								XUpdateMode.REDEFINE, null, newTable, null, null);

						complementTable.add(newTable);

						currentTrigger = newTable.doActionUpdate(currentTrigger, parent,
								MTableConfigAction.COMPLEMENT_TABLE, complementTable, false);

					}
				}
				return currentTrigger;
			}
		}
		}

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(XUpdate currentTrigger, MTableConfig parent, MTableConfigAction mTableConfigAction) {

		switch (mTableConfigAction.getValue()) {
		/*
		 * This operation exists to add new tables and complement them without
		 * the need to addAndMerge the Trigger -> This always leads to an error
		 * The current Trigger will be used and no new one.
		 */
		case MTableConfigAction.MERGE_CONTAINER_VALUE:
			// adds the new columns
			for (MColumnConfig mColumn : this.getColumnConfig()) {

				if (parent.getColumnConfig().isEmpty()) {
					currentTrigger.addReferenceUpdate(parent, MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
							XUpdateMode.ADD, XAddUpdateMode.LAST, this.getColumnConfig(), null, null);
				} else {
					for (MColumnConfig parentColumn : parent.getColumnConfig()) {
						if (parentColumn.getECName().equals(mColumn.getECName())) {
							/*
							 * Checks if the goalColumn already has the same
							 * ClassToCell containment or adds them Therefore it
							 * checks if goalClassToCells with the same cellKind
							 * also have the same input If this is the case it
							 * will break the loop otherwise it will add the
							 * baseClassToCell to the goalColumn
							 */
							for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
								Boolean found = false;
								for (MClassToCellConfig parentClassToCell : parentColumn.getClassToCellConfig()) {
									if (mClassToCell.getCellKind() == parentClassToCell.getCellKind()) {
										if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
											if (((MRowFeatureCell) mClassToCell).getFeature()
													.equals(((MRowFeatureCell) parentClassToCell).getFeature())) {
												found = true;
												if (!parentClassToCell.getClass_().equals(mClassToCell.getClass_())) {
													found = false;
												}
											}
										} else {
											if (mClassToCell.getCellKind().equals("OclCell")) {
												if (((MOclCell) mClassToCell)
														.getStringRendering() == ((MOclCell) parentClassToCell)
																.getStringRendering()) {
													found = true;
													if (!parentClassToCell.getClass_()
															.equals(mClassToCell.getClass_())) {
														found = false;
													}
												}
											}
										}
									}
									// if there is a ClassToCell of the same
									// kind and with the same input, there is no
									// need to search anymore
									if (found) {
										break;
									}
								}
								// it doesn't find a goalClassToCell of the same
								// kind and with the same input and add the
								// baseClassToCell
								if (found == false) {
									MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
											.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

									if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
										MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
												.createNextStateNewObject(
														MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
										newClassCell.setClass(mClassToCell.getClass_());
										newClassCell.setCellConfig(newRowCell);
										newRowCell.setClass(mClassToCell.getClass_());
										MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
										newRowCell.setFeature(mFeatureCell.getFeature());
										newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
										newRowCell.setCellLocateBehavior(mFeatureCell.getCellLocateBehavior());
										newRowCell.setExplicitHighlightOCL(mFeatureCell.getExplicitHighlightOCL());
										currentTrigger.addReferenceUpdate(parentColumn,
												MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
									} else {
										if (mClassToCell.getCellKind().equals("OclCell")) {
											MOclCell newRowCell = (MOclCell) currentTrigger.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMOclCell());
											newClassCell.setClass(mClassToCell.getClass_());
											newClassCell.setCellConfig(newRowCell);
											newRowCell.setClass(mClassToCell.getClass_());
											MOclCell mOclCell = (MOclCell) mClassToCell;
											newRowCell.setStringRendering(mOclCell.getStringRendering());
											newRowCell.setExplicitHighlightOCL(mOclCell.getExplicitHighlightOCL());
											currentTrigger.addReferenceUpdate(parentColumn,
													MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
										}
									}
								}
							}
							break;
						}
						/*
						 * The name of the columns aren't the same what doesn't
						 * mean that the goalColumn doesn't already uses one of
						 * the MProperties -> Currently it just add the column
						 * in this case
						 */
						else {
							/*
							 * All Columns were checked It adds the column by
							 * creating a new one and creating all its cells
							 */
							if (parentColumn
									.equals(parent.getColumnConfig().get(parent.getColumnConfig().size() - 1))) {
								MColumnConfig newColumn = (MColumnConfig) currentTrigger
										.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMColumnConfig());
								newColumn.setName(mColumn.getName());
								newColumn.setLabel(mColumn.getLabel());
								newColumn.setWidth(mColumn.getWidth());

								currentTrigger.addReferenceUpdate(parent,
										MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG, XUpdateMode.ADD,
										XAddUpdateMode.LAST, newColumn, null, null);

								for (MClassToCellConfig mClassToCell : mColumn.getClassToCellConfig()) {
									MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
											.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

									if (mClassToCell.getCellKind().equals("RowFeatureCell")) {
										MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
												.createNextStateNewObject(
														MtableeditorPackage.eINSTANCE.getMRowFeatureCell());
										newClassCell.setClass(mClassToCell.getClass_());
										newClassCell.setCellConfig(newRowCell);
										newRowCell.setClass(mClassToCell.getClass_());
										MRowFeatureCell mFeatureCell = (MRowFeatureCell) mClassToCell;
										newRowCell.setFeature(mFeatureCell.getFeature());
										newRowCell.setCellEditBehavior(mFeatureCell.getCellEditBehavior());
										newRowCell.setCellLocateBehavior(mFeatureCell.getCellLocateBehavior());
										newRowCell.setExplicitHighlightOCL(mFeatureCell.getExplicitHighlightOCL());
										currentTrigger.addReferenceUpdate(parent,
												MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
										currentTrigger.addReferenceUpdate(newColumn,
												MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
												XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
									} else {
										if (mClassToCell.getCellKind().equals("OclCell")) {
											MOclCell newRowCell = (MOclCell) currentTrigger.createNextStateNewObject(
													MtableeditorPackage.eINSTANCE.getMOclCell());
											newClassCell.setClass(mClassToCell.getClass_());
											newClassCell.setCellConfig(newRowCell);
											newRowCell.setClass(mClassToCell.getClass_());
											MOclCell mOclCell = (MOclCell) mClassToCell;
											newRowCell.setStringRendering(mOclCell.getStringRendering());
											newRowCell.setExplicitHighlightOCL(mOclCell.getExplicitHighlightOCL());
											currentTrigger.addReferenceUpdate(parent,
													MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newColumn, null, null);
											currentTrigger.addReferenceUpdate(newColumn,
													MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
													XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);
										}
									}
								}
							}
						}
					}
				}
			}

			// Adds the references of this table to the container, so they
			// aren't lost
			for (MReferenceToTableConfig mref : this.getReferenceToTableConfig()) {
				currentTrigger.addReferenceUpdate(parent,
						MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG, XUpdateMode.ADD,
						XAddUpdateMode.LAST, mref, null, null);
				currentTrigger.addReferenceUpdate(mref,
						MtableeditorPackage.Literals.MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG, XUpdateMode.REDEFINE,
						null, parent, null, null);
			}

			// currently not working, if there was the need to add a new table
			currentTrigger.addReferenceUpdate(this.getContainingEditorConfig(),
					MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG, XUpdateMode.REMOVE, null, this, null,
					null);
			return currentTrigger;
		}
		return currentTrigger;

	}

	/**
	 * @generated NOT
	 */
	public Set<MClassifier> getDeepestContainment(MClassifier mClass) {
		Set<MClassifier> deepCont = new HashSet<MClassifier>();
		for (MProperty mprop : mClass.allContainmentReferences()) {
			if (mprop.getType() instanceof MClassifier) {
				deepCont.add(mprop.getType());
				if (!mprop.getType().allContainmentReferences().isEmpty()) {
					deepCont.addAll(getDeepestContainment(mprop.getType()));
				}
			}
		}
		return deepCont;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object invokeSetDoAction(MTableConfigAction mTableConfigAction) {

		this.setDoAction(mTableConfigAction);
		return this;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getColumnConfig()).basicAdd(otherEnd, msgs);
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContainingEditorConfig((MEditorConfig) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
			return ((InternalEList<?>) getColumnConfig()).basicRemove(otherEnd, msgs);
		case MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG:
			return ((InternalEList<?>) getReferenceToTableConfig()).basicRemove(otherEnd, msgs);
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			return basicSetContainingEditorConfig(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			return eInternalContainer().eInverseRemove(this, MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG,
					MEditorConfig.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MTABLE_CONFIG__NAME:
			return getName();
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
			return getColumnConfig();
		case MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG:
			return getReferenceToTableConfig();
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE:
			if (resolve)
				return getIntendedPackage();
			return basicGetIntendedPackage();
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS:
			if (resolve)
				return getIntendedClass();
			return basicGetIntendedClass();
		case MtableeditorPackage.MTABLE_CONFIG__EC_NAME:
			return getECName();
		case MtableeditorPackage.MTABLE_CONFIG__EC_LABEL:
			return getECLabel();
		case MtableeditorPackage.MTABLE_CONFIG__EC_COLUMN_CONFIG:
			return getECColumnConfig();
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			if (resolve)
				return getContainingEditorConfig();
			return basicGetContainingEditorConfig();
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE:
			return getComplementActionTable();
		case MtableeditorPackage.MTABLE_CONFIG__LABEL:
			return getLabel();
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			return getDisplayDefaultColumn();
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER:
			return getDisplayHeader();
		case MtableeditorPackage.MTABLE_CONFIG__DO_ACTION:
			return getDoAction();
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_ACTION:
			return getIntendedAction();
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION:
			return getComplementAction();
		case MtableeditorPackage.MTABLE_CONFIG__SPLIT_CHILDREN_ACTION:
			return getSplitChildrenAction();
		case MtableeditorPackage.MTABLE_CONFIG__MERGE_CHILDREN_ACTION:
			return getMergeChildrenAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MTABLE_CONFIG__NAME:
			setName((String) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
			getColumnConfig().clear();
			getColumnConfig().addAll((Collection<? extends MColumnConfig>) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG:
			getReferenceToTableConfig().clear();
			getReferenceToTableConfig().addAll((Collection<? extends MReferenceToTableConfig>) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE:
			setIntendedPackage((MPackage) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS:
			setIntendedClass((MClassifier) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			setContainingEditorConfig((MEditorConfig) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE:
			getComplementActionTable().clear();
			getComplementActionTable().addAll((Collection<? extends MTableConfig>) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__LABEL:
			setLabel((String) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			setDisplayDefaultColumn((Boolean) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER:
			setDisplayHeader((Boolean) newValue);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__DO_ACTION:
			setDoAction((MTableConfigAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MTABLE_CONFIG__NAME:
			unsetName();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
			unsetColumnConfig();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG:
			unsetReferenceToTableConfig();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE:
			unsetIntendedPackage();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS:
			unsetIntendedClass();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			setContainingEditorConfig((MEditorConfig) null);
			return;
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE:
			unsetComplementActionTable();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__LABEL:
			unsetLabel();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			unsetDisplayDefaultColumn();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER:
			unsetDisplayHeader();
			return;
		case MtableeditorPackage.MTABLE_CONFIG__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MTABLE_CONFIG__NAME:
			return isSetName();
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
			return isSetColumnConfig();
		case MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG:
			return isSetReferenceToTableConfig();
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_PACKAGE:
			return isSetIntendedPackage();
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_CLASS:
			return isSetIntendedClass();
		case MtableeditorPackage.MTABLE_CONFIG__EC_NAME:
			return EC_NAME_EDEFAULT == null ? getECName() != null : !EC_NAME_EDEFAULT.equals(getECName());
		case MtableeditorPackage.MTABLE_CONFIG__EC_LABEL:
			return EC_LABEL_EDEFAULT == null ? getECLabel() != null : !EC_LABEL_EDEFAULT.equals(getECLabel());
		case MtableeditorPackage.MTABLE_CONFIG__EC_COLUMN_CONFIG:
			return !getECColumnConfig().isEmpty();
		case MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG:
			return basicGetContainingEditorConfig() != null;
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE:
			return isSetComplementActionTable();
		case MtableeditorPackage.MTABLE_CONFIG__LABEL:
			return isSetLabel();
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			return isSetDisplayDefaultColumn();
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER:
			return isSetDisplayHeader();
		case MtableeditorPackage.MTABLE_CONFIG__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_ACTION:
			return !getIntendedAction().isEmpty();
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION:
			return COMPLEMENT_ACTION_EDEFAULT == null ? getComplementAction() != null
					: !COMPLEMENT_ACTION_EDEFAULT.equals(getComplementAction());
		case MtableeditorPackage.MTABLE_CONFIG__SPLIT_CHILDREN_ACTION:
			return SPLIT_CHILDREN_ACTION_EDEFAULT == null ? getSplitChildrenAction() != null
					: !SPLIT_CHILDREN_ACTION_EDEFAULT.equals(getSplitChildrenAction());
		case MtableeditorPackage.MTABLE_CONFIG__MERGE_CHILDREN_ACTION:
			return MERGE_CHILDREN_ACTION_EDEFAULT == null ? getMergeChildrenAction() != null
					: !MERGE_CHILDREN_ACTION_EDEFAULT.equals(getMergeChildrenAction());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case MtableeditorPackage.MTABLE_CONFIG___DO_ACTION$_UPDATE__MTABLECONFIGACTION:
			return doAction$Update((MTableConfigAction) arguments.get(0));
		case MtableeditorPackage.MTABLE_CONFIG___TABLE_CONFIG_FROM_REFERENCE__MPROPERTY:
			return tableConfigFromReference((MProperty) arguments.get(0));
		case MtableeditorPackage.MTABLE_CONFIG___GET_ALL_PROPERTIES:
			return getAllProperties();
		case MtableeditorPackage.MTABLE_CONFIG___DO_ACTION_UPDATE__MTABLECONFIGACTION:
			return doActionUpdate((MTableConfigAction) arguments.get(0));
		case MtableeditorPackage.MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MEDITORCONFIG_MTABLECONFIGACTION_ELIST_BOOLEAN:
			return doActionUpdate((XUpdate) arguments.get(0), (MEditorConfig) arguments.get(1),
					(MTableConfigAction) arguments.get(2), (EList<MTableConfig>) arguments.get(3),
					(Boolean) arguments.get(4));
		case MtableeditorPackage.MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MTABLECONFIG_MTABLECONFIGACTION:
			return doActionUpdate((XUpdate) arguments.get(0), (MTableConfig) arguments.get(1),
					(MTableConfigAction) arguments.get(2));
		case MtableeditorPackage.MTABLE_CONFIG___INVOKE_SET_DO_ACTION__MTABLECONFIGACTION:
			return invokeSetDoAction((MTableConfigAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		if (nameESet)
			result.append(name);
		else
			result.append("<unset>");
		result.append(", label: ");
		if (labelESet)
			result.append(label);
		else
			result.append("<unset>");
		result.append(", displayDefaultColumn: ");
		if (displayDefaultColumnESet)
			result.append(displayDefaultColumn);
		else
			result.append("<unset>");
		result.append(", displayHeader: ");
		if (displayHeaderESet)
			result.append(displayHeader);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL name
	 * 
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = MtableeditorPackage.Literals.MTABLE_CONFIG;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL aKindBase let const1: String = 'Table' in
	const1
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAKindBase() {
		EClass eClass = (MtableeditorPackage.Literals.MTABLE_CONFIG);
		EStructuralFeature eOverrideFeature = AutilPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} // MTableConfigImpl
