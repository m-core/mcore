/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MProperty;
import com.montages.mtableeditor.MCellEditBehaviorOption;
import com.montages.mtableeditor.MCellLocateBehaviorOption;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MtableeditorPackage;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MRow Feature Cell</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getCellEditBehavior <em>Cell Edit Behavior</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getCellLocateBehavior <em>Cell Locate Behavior</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getMaximumSize <em>Maximum Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getAverageSize <em>Average Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getMinimumSize <em>Minimum Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl#getSizeOfPropertyValue <em>Size Of Property Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MRowFeatureCellImpl extends MCellConfigImpl implements MRowFeatureCell {

	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected MProperty feature;

	/**
	 * This is true if the Feature reference has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean featureESet;

	/**
	 * The default value of the '{@link #getCellEditBehavior() <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCellEditBehavior()
	 * @generated
	 * @ordered
	 */
	protected static final MCellEditBehaviorOption CELL_EDIT_BEHAVIOR_EDEFAULT = MCellEditBehaviorOption.SELECTION;

	/**
	 * The cached value of the '{@link #getCellEditBehavior() <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCellEditBehavior()
	 * @generated
	 * @ordered
	 */
	protected MCellEditBehaviorOption cellEditBehavior = CELL_EDIT_BEHAVIOR_EDEFAULT;

	/**
	 * This is true if the Cell Edit Behavior attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean cellEditBehaviorESet;

	/**
	 * The default value of the '{@link #getCellLocateBehavior() <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCellLocateBehavior()
	 * @generated
	 * @ordered
	 */
	protected static final MCellLocateBehaviorOption CELL_LOCATE_BEHAVIOR_EDEFAULT = MCellLocateBehaviorOption.NONE;

	/**
	 * The cached value of the '{@link #getCellLocateBehavior() <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCellLocateBehavior()
	 * @generated
	 * @ordered
	 */
	protected MCellLocateBehaviorOption cellLocateBehavior = CELL_LOCATE_BEHAVIOR_EDEFAULT;

	/**
	 * This is true if the Cell Locate Behavior attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean cellLocateBehaviorESet;

	/**
	 * The default value of the '{@link #getMaximumSize() <em>Maximum Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumSize()
	 * @generated
	 * @ordered
	 */
	protected static final Integer MAXIMUM_SIZE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAverageSize() <em>Average Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAverageSize()
	 * @generated
	 * @ordered
	 */
	protected static final Integer AVERAGE_SIZE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getMinimumSize() <em>Minimum Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumSize()
	 * @generated
	 * @ordered
	 */
	protected static final Integer MINIMUM_SIZE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '
	 * {@link #getFeature <em>Feature</em>}' property. Is combined with the
	 * choice construction definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getFeature
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression featureChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMaximumSize <em>Maximum Size</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumSize
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression maximumSizeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAverageSize <em>Average Size</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAverageSize
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression averageSizeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMinimumSize <em>Minimum Size</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumSize
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression minimumSizeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSizeOfPropertyValue <em>Size Of Property Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeOfPropertyValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression sizeOfPropertyValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCellKind
	 * <em>Cell Kind</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCellKind
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression cellKindDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MRowFeatureCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MROW_FEATURE_CELL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getFeature() {
		if (feature != null && feature.eIsProxy()) {
			InternalEObject oldFeature = (InternalEObject) feature;
			feature = (MProperty) eResolveProxy(oldFeature);
			if (feature != oldFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MROW_FEATURE_CELL__FEATURE, oldFeature, feature));
			}
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeature(MProperty newFeature) {
		MProperty oldFeature = feature;
		feature = newFeature;
		boolean oldFeatureESet = featureESet;
		featureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MROW_FEATURE_CELL__FEATURE,
					oldFeature, feature, !oldFeatureESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFeature() {
		MProperty oldFeature = feature;
		boolean oldFeatureESet = featureESet;
		feature = null;
		featureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MROW_FEATURE_CELL__FEATURE,
					oldFeature, null, oldFeatureESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFeature() {
		return featureESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Feature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MProperty
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL trg.containingClassifier = class or trg.containingClassifier.allSuperTypes()->includes(class) or
	trg.containingClassifier.allSubTypes()->includes(class)
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalFeatureChoiceConstraint(MProperty trg) {
		EClass eClass = MtableeditorPackage.Literals.MROW_FEATURE_CELL;
		if (featureChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				featureChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "FeatureChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(featureChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "FeatureChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellEditBehaviorOption getCellEditBehavior() {
		return cellEditBehavior;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCellEditBehavior(MCellEditBehaviorOption newCellEditBehavior) {
		MCellEditBehaviorOption oldCellEditBehavior = cellEditBehavior;
		cellEditBehavior = newCellEditBehavior == null ? CELL_EDIT_BEHAVIOR_EDEFAULT : newCellEditBehavior;
		boolean oldCellEditBehaviorESet = cellEditBehaviorESet;
		cellEditBehaviorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR, oldCellEditBehavior, cellEditBehavior,
					!oldCellEditBehaviorESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCellEditBehavior() {
		MCellEditBehaviorOption oldCellEditBehavior = cellEditBehavior;
		boolean oldCellEditBehaviorESet = cellEditBehaviorESet;
		cellEditBehavior = CELL_EDIT_BEHAVIOR_EDEFAULT;
		cellEditBehaviorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR, oldCellEditBehavior,
					CELL_EDIT_BEHAVIOR_EDEFAULT, oldCellEditBehaviorESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCellEditBehavior() {
		return cellEditBehaviorESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellLocateBehaviorOption getCellLocateBehavior() {
		return cellLocateBehavior;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCellLocateBehavior(MCellLocateBehaviorOption newCellLocateBehavior) {
		MCellLocateBehaviorOption oldCellLocateBehavior = cellLocateBehavior;
		cellLocateBehavior = newCellLocateBehavior == null ? CELL_LOCATE_BEHAVIOR_EDEFAULT : newCellLocateBehavior;
		boolean oldCellLocateBehaviorESet = cellLocateBehaviorESet;
		cellLocateBehaviorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR, oldCellLocateBehavior,
					cellLocateBehavior, !oldCellLocateBehaviorESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCellLocateBehavior() {
		MCellLocateBehaviorOption oldCellLocateBehavior = cellLocateBehavior;
		boolean oldCellLocateBehaviorESet = cellLocateBehaviorESet;
		cellLocateBehavior = CELL_LOCATE_BEHAVIOR_EDEFAULT;
		cellLocateBehaviorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR, oldCellLocateBehavior,
					CELL_LOCATE_BEHAVIOR_EDEFAULT, oldCellLocateBehaviorESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCellLocateBehavior() {
		return cellLocateBehaviorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getMaximumSize() {
		/**
		 * @OCL sizeOfPropertyValue->max()
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MROW_FEATURE_CELL;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MROW_FEATURE_CELL__MAXIMUM_SIZE;

		if (maximumSizeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				maximumSizeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(maximumSizeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Integer result = (Integer) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getAverageSize() {
		/**
		 * @OCL let finalCalculation: Real = let e1: Real = let chain11: OrderedSet(Integer)  = sizeOfPropertyValue->asOrderedSet() in
		if chain11->sum().oclIsUndefined() 
		then null 
		else chain11->sum()
		endif / let chain12: OrderedSet(Integer)  = sizeOfPropertyValue->asOrderedSet() in
		if chain12->size().oclIsUndefined() 
		then null 
		else chain12->size()
		endif in 
		if e1.oclIsInvalid() then null else e1 endif in
		let chain: Real = finalCalculation in
		if chain.round().oclIsUndefined() 
		then null 
		else chain.round()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MROW_FEATURE_CELL;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MROW_FEATURE_CELL__AVERAGE_SIZE;

		if (averageSizeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				averageSizeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(averageSizeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Integer result = (Integer) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getMinimumSize() {
		/**
		 * @OCL sizeOfPropertyValue->min()
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MROW_FEATURE_CELL;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MROW_FEATURE_CELL__MINIMUM_SIZE;

		if (minimumSizeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				minimumSizeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(minimumSizeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Integer result = (Integer) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getSizeOfPropertyValue() {
		/**
		 * @OCL let potentialDataValue: OrderedSet(mcore::objects::MDataValue)  = let subchain1 : OrderedSet(mcore::objects::MObject)  = if feature.containingClassifier.oclIsUndefined()
		then OrderedSet{}
		else feature.containingClassifier.instance
		endif in 
		if subchain1 = null 
		then null 
		else subchain1.propertyInstance->reject(oclIsUndefined()).internalDataValue->reject(oclIsUndefined())->asOrderedSet() endif 
		in
		let correctDataValue: OrderedSet(mcore::objects::MDataValue)  = potentialDataValue->select(it: mcore::objects::MDataValue | let e0: Boolean = if it.containingPropertyInstance.oclIsUndefined()
		then null
		else it.containingPropertyInstance.property
		endif = feature in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let sequenceofSizes: Sequence(Integer)  = correctDataValue->collect(it: mcore::objects::MDataValue | it.dataValue.size())->asSequence()->excluding(null)->asSequence()  in
		let choice: Sequence(Integer) = if (let e0: Boolean = if ((let e0: Boolean = if feature.oclIsUndefined()
		then null
		else feature.kind
		endif = mcore::PropertyKind::Attribute in 
		if e0.oclIsInvalid() then null else e0 endif))= false 
		then false 
		else if (let e0: Boolean = if feature.oclIsUndefined()
		then null
		else feature.propertyBehavior
		endif <> mcore::PropertyBehavior::Derived in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let e0: Boolean = if feature.oclIsUndefined()
		then null
		else feature.simpleType
		endif <> mcore::SimpleType::None in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let e0: Boolean = if feature.oclIsUndefined()
		then null
		else feature.kind
		endif = mcore::PropertyKind::Attribute in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if feature.oclIsUndefined()
		then null
		else feature.propertyBehavior
		endif <> mcore::PropertyBehavior::Derived in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if feature.oclIsUndefined()
		then null
		else feature.simpleType
		endif <> mcore::SimpleType::None in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then sequenceofSizes
		else if feature.oclIsUndefined()
		then null
		else sequenceofSizes->append(feature.eLabel.size())
		endif
		endif in
		choice
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MROW_FEATURE_CELL;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE;

		if (sizeOfPropertyValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				sizeOfPropertyValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(sizeOfPropertyValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MROW_FEATURE_CELL, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<Integer> result = (EList<Integer>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MROW_FEATURE_CELL__FEATURE:
			if (resolve)
				return getFeature();
			return basicGetFeature();
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			return getCellEditBehavior();
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			return getCellLocateBehavior();
		case MtableeditorPackage.MROW_FEATURE_CELL__MAXIMUM_SIZE:
			return getMaximumSize();
		case MtableeditorPackage.MROW_FEATURE_CELL__AVERAGE_SIZE:
			return getAverageSize();
		case MtableeditorPackage.MROW_FEATURE_CELL__MINIMUM_SIZE:
			return getMinimumSize();
		case MtableeditorPackage.MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE:
			return getSizeOfPropertyValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MROW_FEATURE_CELL__FEATURE:
			setFeature((MProperty) newValue);
			return;
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			setCellEditBehavior((MCellEditBehaviorOption) newValue);
			return;
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			setCellLocateBehavior((MCellLocateBehaviorOption) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MROW_FEATURE_CELL__FEATURE:
			unsetFeature();
			return;
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			unsetCellEditBehavior();
			return;
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			unsetCellLocateBehavior();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MROW_FEATURE_CELL__FEATURE:
			return isSetFeature();
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			return isSetCellEditBehavior();
		case MtableeditorPackage.MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			return isSetCellLocateBehavior();
		case MtableeditorPackage.MROW_FEATURE_CELL__MAXIMUM_SIZE:
			return MAXIMUM_SIZE_EDEFAULT == null ? getMaximumSize() != null
					: !MAXIMUM_SIZE_EDEFAULT.equals(getMaximumSize());
		case MtableeditorPackage.MROW_FEATURE_CELL__AVERAGE_SIZE:
			return AVERAGE_SIZE_EDEFAULT == null ? getAverageSize() != null
					: !AVERAGE_SIZE_EDEFAULT.equals(getAverageSize());
		case MtableeditorPackage.MROW_FEATURE_CELL__MINIMUM_SIZE:
			return MINIMUM_SIZE_EDEFAULT == null ? getMinimumSize() != null
					: !MINIMUM_SIZE_EDEFAULT.equals(getMinimumSize());
		case MtableeditorPackage.MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE:
			return !getSizeOfPropertyValue().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (cellEditBehavior: ");
		if (cellEditBehaviorESet)
			result.append(cellEditBehavior);
		else
			result.append("<unset>");
		result.append(", cellLocateBehavior: ");
		if (cellLocateBehaviorESet)
			result.append(cellLocateBehavior);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL cellKind 'RowFeatureCell'
	
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCellKind() {
		EClass eClass = (MtableeditorPackage.Literals.MROW_FEATURE_CELL);
		EStructuralFeature eOverrideFeature = MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_KIND;

		if (cellKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				cellKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(cellKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}
} // MRowFeatureCellImpl
