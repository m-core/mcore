/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MReference To Table Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MReferenceToTableConfig#getReference <em>Reference</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MReferenceToTableConfig#getTableConfig <em>Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MReferenceToTableConfig#getLabel <em>Label</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMReferenceToTableConfig()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL aKindBaseDerive='let const1: String = \'Ref -> Table Config\' in\nconst1\n'"
 * @generated
 */

public interface MReferenceToTableConfig extends MTableEditorElement {

	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #isSetReference()
	 * @see #unsetReference()
	 * @see #setReference(MProperty)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMReferenceToTableConfig_Reference()
	 * @model unsettable="true" required="true" annotation=
	 *        "http://www.xocl.org/OCL choiceConstraint='let var1: Boolean = let e1: Boolean = trg.kind = mcore::PropertyKind::Reference in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n'"
	 * @generated
	 */
	MProperty getReference();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MReferenceToTableConfig#getReference
	 * <em>Reference</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Reference</em>' reference.
	 * @see #isSetReference()
	 * @see #unsetReference()
	 * @see #getReference()
	 * @generated
	 */
	void setReference(MProperty value);

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MReferenceToTableConfig#getReference
	 * <em>Reference</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isSetReference()
	 * @see #getReference()
	 * @see #setReference(MProperty)
	 * @generated
	 */
	void unsetReference();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getReference <em>Reference</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Reference</em>' reference is set.
	 * @see #unsetReference()
	 * @see #getReference()
	 * @see #setReference(MProperty)
	 * @generated
	 */
	boolean isSetReference();

	/**
	 * Returns the value of the '<em><b>Table Config</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table Config</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Table Config</em>' reference.
	 * @see #isSetTableConfig()
	 * @see #unsetTableConfig()
	 * @see #setTableConfig(MTableConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMReferenceToTableConfig_TableConfig()
	 * @model unsettable="true"
	 * @generated
	 */
	MTableConfig getTableConfig();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getTableConfig <em>Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Table Config</em>' reference.
	 * @see #isSetTableConfig()
	 * @see #unsetTableConfig()
	 * @see #getTableConfig()
	 * @generated
	 */
	void setTableConfig(MTableConfig value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getTableConfig <em>Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetTableConfig()
	 * @see #getTableConfig()
	 * @see #setTableConfig(MTableConfig)
	 * @generated
	 */
	void unsetTableConfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getTableConfig <em>Table Config</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Table Config</em>' reference is set.
	 * @see #unsetTableConfig()
	 * @see #getTableConfig()
	 * @see #setTableConfig(MTableConfig)
	 * @generated
	 */
	boolean isSetTableConfig();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #setLabel(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMReferenceToTableConfig_Label()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Rendering'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	void unsetLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MReferenceToTableConfig#getLabel <em>Label</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Label</em>' attribute is set.
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	boolean isSetLabel();

} // MReferenceToTableConfig
