/**
 */
package com.montages.mtableeditor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MElement With Font</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MElementWithFont#getBoldFont <em>Bold Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MElementWithFont#getItalicFont <em>Italic Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MElementWithFont#getFontSize <em>Font Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MElementWithFont#getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMElementWithFont()
 * @model abstract="true"
 * @generated
 */

public interface MElementWithFont extends EObject {

	/**
	 * Returns the value of the '<em><b>Font Size</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mtableeditor.MFontSizeAdaptor}.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Font Size</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font Size</em>' attribute.
	 * @see com.montages.mtableeditor.MFontSizeAdaptor
	 * @see #isSetFontSize()
	 * @see #unsetFontSize()
	 * @see #setFontSize(MFontSizeAdaptor)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMElementWithFont_FontSize()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='let adaptor: mtableeditor::MFontSizeAdaptor = mtableeditor::MFontSizeAdaptor::Zero in\nadaptor\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Font'"
	 * @generated
	 */
	MFontSizeAdaptor getFontSize();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MElementWithFont#getFontSize
	 * <em>Font Size</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Font Size</em>' attribute.
	 * @see com.montages.mtableeditor.MFontSizeAdaptor
	 * @see #isSetFontSize()
	 * @see #unsetFontSize()
	 * @see #getFontSize()
	 * @generated
	 */
	void setFontSize(MFontSizeAdaptor value);

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MElementWithFont#getFontSize
	 * <em>Font Size</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isSetFontSize()
	 * @see #getFontSize()
	 * @see #setFontSize(MFontSizeAdaptor)
	 * @generated
	 */
	void unsetFontSize();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MElementWithFont#getFontSize <em>Font Size</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Font Size</em>' attribute is set.
	 * @see #unsetFontSize()
	 * @see #getFontSize()
	 * @see #setFontSize(MFontSizeAdaptor)
	 * @generated
	 */
	boolean isSetFontSize();

	/**
	 * Returns the value of the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bold Font</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Bold Font</em>' attribute.
	 * @see #isSetBoldFont()
	 * @see #unsetBoldFont()
	 * @see #setBoldFont(Boolean)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMElementWithFont_BoldFont()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Font'"
	 * @generated
	 */
	Boolean getBoldFont();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MElementWithFont#getBoldFont
	 * <em>Bold Font</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Bold Font</em>' attribute.
	 * @see #isSetBoldFont()
	 * @see #unsetBoldFont()
	 * @see #getBoldFont()
	 * @generated
	 */
	void setBoldFont(Boolean value);

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MElementWithFont#getBoldFont
	 * <em>Bold Font</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isSetBoldFont()
	 * @see #getBoldFont()
	 * @see #setBoldFont(Boolean)
	 * @generated
	 */
	void unsetBoldFont();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MElementWithFont#getBoldFont <em>Bold Font</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Bold Font</em>' attribute is set.
	 * @see #unsetBoldFont()
	 * @see #getBoldFont()
	 * @see #setBoldFont(Boolean)
	 * @generated
	 */
	boolean isSetBoldFont();

	/**
	 * Returns the value of the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Italic Font</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Italic Font</em>' attribute.
	 * @see #isSetItalicFont()
	 * @see #unsetItalicFont()
	 * @see #setItalicFont(Boolean)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMElementWithFont_ItalicFont()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Font'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getItalicFont();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MElementWithFont#getItalicFont <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Italic Font</em>' attribute.
	 * @see #isSetItalicFont()
	 * @see #unsetItalicFont()
	 * @see #getItalicFont()
	 * @generated
	 */
	void setItalicFont(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MElementWithFont#getItalicFont <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetItalicFont()
	 * @see #getItalicFont()
	 * @see #setItalicFont(Boolean)
	 * @generated
	 */
	void unsetItalicFont();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MElementWithFont#getItalicFont <em>Italic Font</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Italic Font</em>' attribute is set.
	 * @see #unsetItalicFont()
	 * @see #getItalicFont()
	 * @see #setItalicFont(Boolean)
	 * @generated
	 */
	boolean isSetItalicFont();

	/**
	 * Returns the value of the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Font Options Encoding</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Font Options Encoding</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMElementWithFont_DerivedFontOptionsEncoding()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Font'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedFontOptionsEncoding();

} // MElementWithFont
