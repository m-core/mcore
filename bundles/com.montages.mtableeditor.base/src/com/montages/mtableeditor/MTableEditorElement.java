/**
 */
package com.montages.mtableeditor;

import org.langlets.autil.AElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MTable Editor Element</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MTableEditorElement#getKind <em>Kind</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableEditorElement()
 * @model abstract="true"
 * @generated
 */

public interface MTableEditorElement extends AElement {

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableEditorElement_Kind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='aRenderedKind\n'"
	 * @generated
	 */
	String getKind();

} // MTableEditorElement
