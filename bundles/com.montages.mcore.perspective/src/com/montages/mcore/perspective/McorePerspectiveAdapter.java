package com.montages.mcore.perspective;

import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PerspectiveAdapter;

public class McorePerspectiveAdapter extends PerspectiveAdapter {

	@Override
	public void perspectiveActivated(IWorkbenchPage page, IPerspectiveDescriptor perspective) {
		super.perspectiveActivated(page, perspective);

		// ---	Update main menu and cool bar
		// ---	We need this to refresh the toolbar, otherwise
		//		the save button is not visible from beginning.
//		IWorkbenchWindow workbenchWindow =  PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//
//		if (workbenchWindow instanceof WorkbenchWindow) {
//
//			try {
//				((WorkbenchWindow)workbenchWindow).getMenuBarManager().update(true);
//				((WorkbenchWindow)workbenchWindow).getCoolBarManager2().update(true);				
//			} catch (Exception ex) {
//				Activator.getDefault().logError("Problem encountered while activating perspective.", ex);
//			}
//		}
	}
}
