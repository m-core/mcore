package com.montages.mcore.perspective.handlers;

import static org.eclipse.jface.dialogs.MessageDialog.openConfirm;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.operations.McoreOperations;
import com.montages.mcore.ui.operations.McoreOperations.Options;

public class McoreCleanHandler extends McoreControlAbstractHandler {

	private final String confirmMessage = "Are you sure you want to clean this project?";
	
	@Override
	protected void doExcecute(Resource resource, ExecutionEvent event) {
		final Shell shell = HandlerUtil.getActiveShell(event);
		final boolean confirm = openConfirm(shell, "Confirm", confirmMessage);

		if (confirm) {
			super.doExcecute(resource, event);
		}
	}

	@Override
	protected Job createJob(final MComponent component) {
		Job job = new Job("Clean Project") {
			protected IStatus run(IProgressMonitor monitor) {
				return McoreOperations.CleanOperation.execute(component, 
						Collections.<Options, Boolean> emptyMap(), 
						monitor);
			};
		};
		
		return job;
	}

}
