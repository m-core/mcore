package com.montages.mcore.perspective.handlers;

import static com.montages.mcore.util.ResourceService.createResourceSet;
import static com.montages.mcore.util.ResourceService.normalize;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.montages.mcore.perspective.LDEPlugin;
import com.montages.mcore.perspective.menus.McoreControlContribution;
import com.montages.mcore.ui.handlers.McoreSelectionHandler;
import com.montages.mcore.ui.workspace.FileFunctions;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

public abstract class McoreControlAbstractHandler extends McoreSelectionHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		McoreControlContribution control = LDEPlugin.getDefault().getMcoreControlContribution();

		if (control != null) {
			URI uri = control.getSelected();
			if (uri != null) {
				ResourceSet newResourceSet = createResourceSet();
				URI normalized = normalize(newResourceSet, uri);
				String id = PathUtils.buildDomainID(normalized);
				FileFunctions.insureIsSave(FileFunctions.getFile(normalized));

				McoreEditingDomainFactory domainFactory = McoreEditingDomainFactory.getInstance();
				McoreDiagramEditingDoamin domain = domainFactory.exists(id) ? domainFactory.createEditingDomain(id, this) : null;
				boolean domainExists = domain != null;
				if (domainExists) {
					try {
						domain.publish(McoreTransactionEventType.SAVE, this);
					} finally {
						domainFactory.dispose(domain, this);
					}
				}

				ResourceSet resourceSet = domainExists ? domain.getResourceSet() : newResourceSet;
				Resource resource = resourceSet.getResource(normalized, true);
				if (resource != null) {
					doExcecute(resource, event);
				}
			}
		}

		return null;
	}

}
