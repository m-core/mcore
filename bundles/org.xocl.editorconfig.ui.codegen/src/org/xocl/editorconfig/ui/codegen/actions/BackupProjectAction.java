package org.xocl.editorconfig.ui.codegen.actions;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.xocl.core.util.XoclHelper;

public class BackupProjectAction extends WorkspaceModifyOperation {

	public static final String ZIP_EXTENSION = "backup.zip"; //$NON-NLS-1$
	public static final String BACKUP_EXTENSION = "backup"; //$NON-NLS-1$
	public static final String WORKSPACE_FOLDER_PATH = "workspace"; //$NON-NLS-1$
	public static final String MODEL_FOLDER_PATH = "model"; //$NON-NLS-1$
	public static final String METADATA_FOLDER_NAME = ".metadata"; //$NON-NLS-1$
	
	private IProject myProject;
	
	public BackupProjectAction(IFile metamodelFile) {
		myProject = metamodelFile.getProject();
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException,
			InvocationTargetException, InterruptedException {
		
		String timestamp = XoclHelper.timestamp();
		
		String zipName = myProject.getName() + timestamp + ZIP_EXTENSION;
		IFile zipFile = myProject.getFile(zipName);
		
		try {
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile.getLocation().toFile()));

			IFolder modelFolder = myProject.getFolder(MODEL_FOLDER_PATH);
			if ((modelFolder != null) && modelFolder.exists()) {
				zipFolder(modelFolder, out, new IFileFilter() {
					public boolean isAccepted(IResource resource) {
						return (resource instanceof IFolder) 
						|| !BACKUP_EXTENSION.equals(resource.getFileExtension());
					}
				});
			}
			
			final IFolder workspaceFolder = myProject.getFolder(WORKSPACE_FOLDER_PATH);
			if ((workspaceFolder != null) && workspaceFolder.exists()) {
				workspaceFolder.refreshLocal(IResource.DEPTH_INFINITE, new SubProgressMonitor(monitor, 1));
				zipFolder(workspaceFolder, out, new IFileFilter() {
					public boolean isAccepted(IResource resource) {
						return (resource instanceof IFile) 
							|| !resource.getParent().equals(workspaceFolder) 
							|| !METADATA_FOLDER_NAME.equals(resource.getName());
					}
				});
			}

			out.close();
			
			myProject.refreshLocal(IResource.DEPTH_ONE, new SubProgressMonitor(monitor, 1));
		} catch (FileNotFoundException e) {
			throw new InvocationTargetException(e);
		} catch (IOException e) {
			throw new InvocationTargetException(e);
		}
	}
	
	private void zipFolder(IFolder folder, ZipOutputStream zipOutputStream, IFileFilter fileFilter) throws IOException, CoreException {
		byte[] buf = new byte[1024];
		
		for (IResource resource : folder.members()) {
			if (fileFilter.isAccepted(resource)) {
				if (resource instanceof IFile) {
					IFile file = (IFile) resource;
					IPath entryPath = resource.getFullPath().makeRelativeTo(myProject.getFullPath());
					
					zipOutputStream.putNextEntry(new ZipEntry(entryPath.toString()));
					
					InputStream in = file.getContents();
					int len;
					while ((len = in.read(buf)) > 0) {
						zipOutputStream.write(buf, 0, len); 
					}
					
					in.close(); 				
					zipOutputStream.closeEntry();
				} else if (resource instanceof IFolder) {
					zipFolder((IFolder) resource, zipOutputStream, fileFilter);
				}
			}
		}	
	}
	
	private interface IFileFilter {
		public boolean isAccepted(IResource resource);
	}
}