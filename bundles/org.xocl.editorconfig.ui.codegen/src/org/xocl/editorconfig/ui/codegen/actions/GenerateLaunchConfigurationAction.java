package org.xocl.editorconfig.ui.codegen.actions;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.internal.ui.DebugUIPlugin;
import org.eclipse.debug.internal.ui.launchConfigurations.LaunchConfigurationManager;
import org.eclipse.debug.internal.ui.launchConfigurations.LaunchGroupExtension;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.importer.ecore.EcoreImporter;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.core.plugin.PluginRegistry;
import org.eclipse.pde.internal.build.IPDEBuildConstants;
import org.eclipse.pde.internal.core.DependencyManager;
import org.eclipse.pde.internal.launching.launcher.BundleLauncherHelper;
import org.eclipse.pde.internal.launching.launcher.LaunchArgumentsHelper;
import org.eclipse.pde.internal.ui.IPDEUIConstants;
import org.eclipse.pde.launching.IPDELauncherConstants;
import org.eclipse.pde.launching.PDESourcePathProvider;
import org.eclipse.swt.SWT;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.xocl.editorconfig.ui.codegen.utils.GeneratedProjectsNamingConventions;

@SuppressWarnings("restriction")
public class GenerateLaunchConfigurationAction extends WorkspaceModifyOperation {

	private static final String WORKSPACE_FOLDER_PATH = "workspace"; //$NON-NLS-1$

	private IProject myProject;
	private static final String PRODUCT_ID = "org.eclipse.platform.ide"; //$NON-NLS-1$
	private GenModel genModel;
	private String projectShortName; 
//	private final EcoreImporter myEcoreImporter;
//	private final XoclGenerationSettings myXoclGenerationSettings;
//	private final IFile myFile;
	
	public GenerateLaunchConfigurationAction(IFile file, EcoreImporter ecoreImporter, XoclGenerationSettings xoclGenerationSettings) {
		projectShortName = GeneratedProjectsNamingConventions.getMainProjectShortName(file.getFullPath()); ;
//		myEcoreImporter = ecoreImporter;
		genModel = ecoreImporter.getGenModel();
//		myXoclGenerationSettings = xoclGenerationSettings;
		myProject = file.getProject();
	}
	
	public GenerateLaunchConfigurationAction(GenModel genModel, IProject container, String projectShortName) {
		this.projectShortName = projectShortName;
		this.myProject = container;
		this.genModel = genModel;
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException, InvocationTargetException, InterruptedException {
		boolean launchConfigurationFileCreated = createLaunchConfigurationFile();

		if (launchConfigurationFileCreated) {
			myProject.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		}
	}

	private boolean createLaunchConfigurationFile() throws CoreException {
		String launchName = projectShortName; 
	
		ILaunchConfigurationType configType = DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurationType("org.eclipse.pde.ui.RuntimeWorkbench"); //$NON-NLS-1$
		
		ILaunchConfigurationWorkingCopy configuration = configType.newInstance(null, launchName);
		configuration.setContainer(myProject);
		
		configuration.setAttribute(IPDELauncherConstants.ASKCLEAR, true);
		configuration.setAttribute(IPDELauncherConstants.AUTOMATIC_ADD, false);
		configuration.setAttribute(IPDELauncherConstants.AUTOMATIC_VALIDATE, false);
		configuration.setAttribute(IPDELauncherConstants.BOOTSTRAP_ENTRIES, ""); //$NON-NLS-1$
		configuration.setAttribute(IPDELauncherConstants.TRACING_CHECKED, IPDELauncherConstants.TRACING_NONE);
		configuration.setAttribute(IPDELauncherConstants.CONFIG_CLEAR_AREA, true);
		configuration.setAttribute(IPDELauncherConstants.DOCLEAR, false);
		configuration.setAttribute(IPDEUIConstants.DOCLEARLOG, false);
		configuration.setAttribute(IPDELauncherConstants.CONFIG_LOCATION, "${workspace_loc}/.metadata/.plugins/org.eclipse.pde.core/" + launchName); //$NON-NLS-1$
		configuration.setAttribute(IPDELauncherConstants.USE_DEFAULT, false);
		configuration.setAttribute(IPDELauncherConstants.INCLUDE_OPTIONAL, false);
		configuration.setAttribute(IPDELauncherConstants.SHOW_SELECTED_ONLY, false);		
		configuration.setAttribute(IPDELauncherConstants.CONFIG_USE_DEFAULT_AREA, true);
		configuration.setAttribute(IPDELauncherConstants.CONFIG_GENERATE_DEFAULT, true);
		configuration.setAttribute(IPDELauncherConstants.CONFIG_TEMPLATE_LOCATION, "${target_home}" + File.separatorChar + "configuration" + File.separatorChar + "config.ini"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		configuration.setAttribute(IPDELauncherConstants.TRACING, false);

		configuration.setAttribute(IPDELauncherConstants.LOCATION, "${workspace_loc:" + myProject.getName() + "/" + WORKSPACE_FOLDER_PATH + "}"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		configuration.setAttribute(IPDELauncherConstants.PRODUCT, PRODUCT_ID);
		configuration.setAttribute(IPDELauncherConstants.USE_PRODUCT, true);
		
		
		IPluginModelBase[] allEditorPlugins = findRequiredEditorPlugins();
		IPluginModelBase[] workspacePlugins = filterWorkspacePlugins(allEditorPlugins);
		configuration.setAttribute(IPDELauncherConstants.SELECTED_WORKSPACE_PLUGINS, getSelectedPluginsAttribute(workspacePlugins));
			
		IPluginModelBase[] platformPlugins = filterPlatformPlugins(allEditorPlugins);
		configuration.setAttribute(IPDELauncherConstants.SELECTED_TARGET_PLUGINS, getSelectedPluginsAttribute(platformPlugins));
		
		configuration.setAttribute(IPDEUIConstants.LAUNCHER_PDE_VERSION, "3.3"); //$NON-NLS-1$
		configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_SOURCE_PATH_PROVIDER, PDESourcePathProvider.ID);
		configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_JRE_CONTAINER_PATH, JavaRuntime.JRE_CONTAINER);
		if (SWT.getPlatform().equals("cocoa")) {
			configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_USE_START_ON_FIRST_THREAD, true);
		}

		// Set Program/VM arguments with preference values
		String programArgs = LaunchArgumentsHelper.getInitialProgramArguments().trim();
		if (programArgs.length() > 0) {
			configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, programArgs);
		}
		
		String vmArgs = LaunchArgumentsHelper.getInitialVMArguments().trim();
		if (vmArgs.length() > 0)
			configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, vmArgs);
	
		configuration.setAttribute(IPDEUIConstants.APPEND_ARGS_EXPLICITLY, true);
		
		LaunchConfigurationManager launchConfigManager = DebugUIPlugin.getDefault().getLaunchConfigurationManager();
		LaunchGroupExtension launchGroup = launchConfigManager.getLaunchGroup(IDebugUIConstants.ID_RUN_LAUNCH_GROUP);
		List<String> groups = new ArrayList<String>();
		groups.add(launchGroup.getIdentifier());
		configuration.setAttribute(IDebugUIConstants.ATTR_FAVORITE_GROUPS, groups);

//		if (!myXoclGenerationSettings.isGenerated(configuration.getFile())) {
//			return false;
//		}

		configuration.doSave();

		return true;
	}
	
	private IPluginModelBase[] findRequiredEditorPlugins() {
		Set<IPluginModelBase> result = new LinkedHashSet<IPluginModelBase>();
		
		Set<GenModel> referencedGenModels = new HashSet<GenModel>();
		for (GenPackage p : genModel.getUsedGenPackages()) {
			GenModel refModel = p.getGenModel();
			if (refModel != null) {
				referencedGenModels.add(refModel);
			}
		}
		referencedGenModels.add(genModel);
		
		for (GenModel next : referencedGenModels) {
			String basePluginID = next.getModelPluginID();
			String editorID = GeneratedProjectsNamingConventions.getNewEditorPluginID(basePluginID);
			IPluginModelBase editorPluginModel = PluginRegistry.findModel(editorID);
			if (editorPluginModel != null) {
				IPluginModelBase[] nextRequiredPlugins = getRequiredPlugins(editorPluginModel);
				result.addAll(Arrays.asList(nextRequiredPlugins));
			}
		}
		
		return (IPluginModelBase[]) result.toArray(new IPluginModelBase[result.size()]);
	}

	private IPluginModelBase[] filterPlatformPlugins(IPluginModelBase[] models) {
		List<IPluginModelBase> filteredModelList = new ArrayList<IPluginModelBase>();
		for (IPluginModelBase model : models) {
			if (model.getUnderlyingResource() == null) {
				filteredModelList.add(model);
			}
		}
		return filteredModelList.toArray(new IPluginModelBase[filteredModelList.size()]); 
	}

	private IPluginModelBase[] filterWorkspacePlugins(IPluginModelBase[] models) {
		List<IPluginModelBase> filteredModelList = new ArrayList<IPluginModelBase>();
		for (IPluginModelBase model : models) {
			if (model.getUnderlyingResource() != null) {
				filteredModelList.add(model);
			}
		}
		return filteredModelList.toArray(new IPluginModelBase[filteredModelList.size()]); 
	}

	private IPluginModelBase[] getRequiredPlugins(IPluginModelBase editorPluginModel) {
		String launchPluginId = PRODUCT_ID;
		int lastIndexOfDot = PRODUCT_ID.lastIndexOf('.');
		if (lastIndexOfDot > 0) {
			launchPluginId = PRODUCT_ID.substring(0, lastIndexOfDot);
		}

		IPluginModelBase[] initialPlugins = new IPluginModelBase[] {
				editorPluginModel,
				PluginRegistry.findModel("org.eclipse.ui.ide.application"), //$NON-NLS-1$
				PluginRegistry.findModel("org.eclipse.ui.navigator.resources"), //$NON-NLS-1$
				PluginRegistry.findModel("org.xocl.startup"), //$NON-NLS-1$
				PluginRegistry.findModel(launchPluginId),
				PluginRegistry.findModel("org.eclipse.equinox.ds"),
				PluginRegistry.findModel("org.eclipse.equinox.util")
		};
	
		List<IPluginModelBase> externalModelList = new ArrayList<IPluginModelBase>();
		for (IPluginModelBase initialPlugin : initialPlugins) {
			if (initialPlugin != null) {
				externalModelList.add(initialPlugin);
			}
		}
		
		Set<String> dependencyIds = DependencyManager.getDependencies(initialPlugins, true, null);
	
		for (String pluginId : dependencyIds) {
			IPluginModelBase model = PluginRegistry.findModel(pluginId);
			externalModelList.add(model);
		}
		
	
		return externalModelList.toArray(new IPluginModelBase[externalModelList.size()]);
	}

	private String getSelectedPluginsAttribute(IPluginModelBase[] pluginModels) {
		StringBuilder selectedPlugins = new StringBuilder();
		for (IPluginModelBase model : pluginModels) {
			if (selectedPlugins.length() > 0)
				selectedPlugins.append(","); //$NON-NLS-1$ 
	
			String startLevel = "default"; //$NON-NLS-1$
			String autoStart = "default"; //$NON-NLS-1$
			if (model.isFragmentModel()) {
				autoStart = "false"; //$NON-NLS-1$
			} else {
				String modelName = model.getBundleDescription().getSymbolicName();
				if (IPDEBuildConstants.BUNDLE_CORE_RUNTIME.equals(modelName) || IPDEBuildConstants.BUNDLE_DS.equals(modelName)) {
					autoStart = "true"; //$NON-NLS-1$
				}
			}
	
			String value = BundleLauncherHelper.writeBundleEntry(model, startLevel, autoStart);
			selectedPlugins.append(value);
		}
		return selectedPlugins.toString();
	}

}
