package org.xocl.editorconfig.ui.codegen.actions;

public interface ITextMerger {
	public String process(String oldContent, String newContent);
}