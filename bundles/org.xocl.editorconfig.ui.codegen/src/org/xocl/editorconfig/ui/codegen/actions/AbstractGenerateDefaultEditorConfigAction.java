package org.xocl.editorconfig.ui.codegen.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.xocl.editorconfig.EditorConfigPackage;

public abstract class AbstractGenerateDefaultEditorConfigAction extends WorkspaceModifyOperation {
	private URI myEPackageURI;

	private IFile myEditorConfigFile;

	private URI myEditorConfigURI;

	public IFile getEditorConfigFile(URIConverter uriConverter) {
		if (myEditorConfigFile == null) {
			URI editorConfigURI = getEditorConfigURI(uriConverter);
			myEditorConfigFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(editorConfigURI.toPlatformString(true)));
		}
		return myEditorConfigFile;
	}

	public URI getEPackageURI() {
		return myEPackageURI;
	}

	public void setEPackageURI(URI ePackageURI) {
		myEPackageURI = ePackageURI;
	}
	
	public URI getEditorConfigURI(URIConverter uriConverter) {
		if (myEditorConfigURI == null) {
			URI normalized = uriConverter.normalize(myEPackageURI);
			myEditorConfigURI = normalized.trimFileExtension().appendFileExtension(EditorConfigPackage.eINSTANCE.getName());		
		}
		return myEditorConfigURI;
	}
}