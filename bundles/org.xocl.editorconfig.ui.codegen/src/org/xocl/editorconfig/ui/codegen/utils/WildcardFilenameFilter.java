package org.xocl.editorconfig.ui.codegen.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IPath;

public class WildcardFilenameFilter {

	private Pattern pattern;

	public WildcardFilenameFilter(String filter) {
		pattern = WildcardPatternConstructor.createPattern(filter, false);
	}

	public boolean accept(IPath path) {
		Matcher matcher = pattern.matcher(path.makeAbsolute().toString());
		return matcher.matches();
	}
}