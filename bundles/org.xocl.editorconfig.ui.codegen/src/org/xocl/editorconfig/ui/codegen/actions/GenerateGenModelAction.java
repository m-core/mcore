package org.xocl.editorconfig.ui.codegen.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.importer.ecore.EcoreImporter;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.xocl.editorconfig.ui.codegen.XOCLCodegenUIPlugin;
import org.xocl.editorconfig.ui.codegen.utils.GenModelCustomizer;
import org.xocl.editorconfig.ui.codegen.utils.GeneratedProjectsNamingConventions;
import org.xocl.workbench.ide.builder.EcoreModelValidator;

public class GenerateGenModelAction extends WorkspaceModifyOperation {

	EcoreImporter ecoreImporter;

	private final XoclGenerationSettings myXoclGenerationSettings;

	/**
	 * @param ecoreImporter
	 * @param xoclGenerationSettings 
	 */
	public GenerateGenModelAction(EcoreImporter ecoreImporter, XoclGenerationSettings xoclGenerationSettings) {
		this.ecoreImporter = ecoreImporter;
		myXoclGenerationSettings = xoclGenerationSettings;
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException,
	InvocationTargetException, InterruptedException {
		monitor.beginTask("Generate GenModel", 120); //$NON-NLS-1$
		try {
			// Bug 173 and 233:
			// Sets default Java language compliance settings in case it hasn't been done before
			JavaRuntime.getDefaultVMInstall();

			for (EPackage ePackage : ecoreImporter.getEPackages()) {
				ResourceSet rs = ePackage.eResource().getResourceSet();
				URI ePackageURI = ePackage.eResource().getURI();
				if (rs != null) {
					ePackageURI = rs.getURIConverter().normalize(ePackageURI);
				}
				if (ePackageURI.isPlatformResource()) {
					IPath resourcePath = new Path(ePackageURI.toPlatformString(true));
					validateEcore(resourcePath);
					IPath containerPath = GeneratedProjectsNamingConventions.getGenContainerPath(resourcePath);
					IPath genModelPath = containerPath.append(GeneratedProjectsNamingConventions.getGenModelFileName(resourcePath));
					IFile genModelFile = ResourcesPlugin.getWorkspace().getRoot().getFile(genModelPath);
					List<URI> modelLocationURIS = ecoreImporter.getModelLocationURIs();
					if (ecoreImporter.getGenModelContainerPath().equals(containerPath) && genModelFile.exists()) {
						if (myXoclGenerationSettings.isGenerated(genModelFile)) {
							genModelFile.delete(false, new BasicMonitor.EclipseSubProgress(monitor, 40));
						}
					}
					if (genModelFile.exists() && ! modelLocationURIS.contains(ePackageURI)) {
						ResourceSet resourceSet = ecoreImporter.getGenModelResourceSet();
						URI uri = URI.createPlatformResourceURI(genModelPath.toString(), true);
						Resource resource = resourceSet.getResource(uri, false);
						if (resource != null) {
							resource.unload();
						}
						resource = resourceSet.getResource(uri, true);
						GenModel genModel = (GenModel)resource.getContents().get(0);
						GenPackage genPackage = null;
						Iterator<GenPackage> iterator = genModel.getGenPackages().iterator();
						while (iterator.hasNext() && genPackage == null) {
							GenPackage checked = iterator.next();
							if (checked.getEcorePackage().getNsURI().equals(ePackage.getNsURI())) {
								genPackage = checked;
							}
						}
						ecoreImporter.getReferencedGenPackages().add(genPackage);
						ecoreImporter.getReferenceGenPackageConvertInfo(genPackage).setValidReference(true);
					} else {
						ecoreImporter.getEPackageConvertInfo(ePackage).setConvert(true);
					}
				}				
			}
			
			// Hack: EMF utilizes platform plugin genmodel version of this genmodel
			// Workaround: Temporarily remove model EPackages from the ecore importer
			
			List<EPackage> ePackagesCopy = new ArrayList<EPackage>(ecoreImporter.getEPackages());
			List<EPackage> removedEPackages = new ArrayList<EPackage>();
			List<URI> modelLocationURIS = ecoreImporter.getModelLocationURIs();
			for (EPackage ePackage : ePackagesCopy) {
				ResourceSet rs = ePackage.eResource().getResourceSet();
				URI ePackageURI = ePackage.eResource().getURI();
				if (rs != null) {
					ePackageURI = rs.getURIConverter().normalize(ePackageURI);
				}
				if (modelLocationURIS.contains(ePackageURI)) {
					ecoreImporter.getEPackages().remove(ePackage);
					removedEPackages.add(ePackage);
				}
			}
			
			List<GenModel> externalGenModels = ecoreImporter.getExternalGenModels();
						
			// Workaround: Now add removed model EPackages to the ecore importer
			ecoreImporter.getEPackages().addAll(removedEPackages);
			
			List<GenModel> selectedExternalGenModels = new ArrayList<GenModel>();

			genModelCycle : for (GenModel genModel : externalGenModels) {
				for (GenPackage genPackage : genModel.getGenPackages()) {
					for (EPackage ePackage : ecoreImporter.getEPackages()) {
						ResourceSet rs = ePackage.eResource().getResourceSet();
						URI ePackageURI = ePackage.eResource().getURI();
						if (rs != null) {
							ePackageURI = rs.getURIConverter().normalize(ePackageURI);
						}
						if (ecoreImporter.getModelLocationURIs().contains(ePackageURI)) {
							String nsURI = ePackage.getNsURI();
							if ((nsURI != null) && nsURI.equals(genPackage.getEcorePackage().getNsURI())) {
								continue genModelCycle;
							}
						}
					}
				}
				selectedExternalGenModels.add(genModel);
			}

			for (GenModel genModel : selectedExternalGenModels) {
				genPackageCycle:
				for (GenPackage genPackage : genModel.getGenPackages()) {
					for (GenPackage referencedGenPackage : ecoreImporter.getReferencedGenPackages()) {
						if (referencedGenPackage.getNSURI().equals(genPackage.getNSURI())) {
							continue genPackageCycle;
						}
					}
					ecoreImporter.getReferencedGenPackages().add(genPackage);
					ecoreImporter.getReferenceGenPackageConvertInfo(genPackage).setValidReference(true);
				}
			}
			
			ecoreImporter.prepareGenModelAndEPackages(new BasicMonitor.EclipseSubProgress(monitor, 20));
			GenModelCustomizer.adjustSettings(ecoreImporter.getGenModel(), ecoreImporter);

			GenModel genModel = ecoreImporter.getGenModel();
			if (genModel != null) {
				List<GenPackage> genPackages = genModel.getGenPackages();
				if (genPackages != null) {
					for (GenPackage genPackage : genPackages) {
						EPackage ecorePackage = genPackage.getEcorePackage();
						if (ecorePackage != null) {
							String ePackageName = ecorePackage.getName();
							if ((ePackageName != null) && (genModel != null) && ePackageName.equalsIgnoreCase(genModel.getModelName())) {
								int index = genPackages.indexOf(genPackage);
								if (index != 0) {
									genPackages.remove(genPackage);
									genPackages.add(0, genPackage);
								}
								break;
							}
						}
					}
				}
			}
			
			boolean shouldPersistGenModel = true;

			if (shouldPersistGenModel && myXoclGenerationSettings.isGenerated(ecoreImporter.getGenModelPath())) {
				try {
					ecoreImporter.adjustEPackages(new BasicMonitor.EclipseSubProgress(monitor, 40));
				} catch (StringIndexOutOfBoundsException e) {
//					e.printStackTrace();
				}
				ecoreImporter.saveGenModelAndEPackages(new BasicMonitor.EclipseSubProgress(monitor, 40));
			}
		} catch (XoclValidationException e) {
			throw e;
		} catch (Exception e) {
			final Diagnostic diagnostic = BasicDiagnostic.toDiagnostic(e);
			if (PlatformUI.isWorkbenchRunning() && diagnostic.getSeverity() == Diagnostic.ERROR) {
				PlatformUI.getWorkbench().getDisplay().asyncExec(
						new Runnable() {
							public void run() {
								DiagnosticDialog.openProblem(
										PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
										"Error during generation", //$NON-NLS-1$
										"An error has been raised during the generation of the default EMF Generator Model", //$NON-NLS-1$
										diagnostic
								);
							}
						}
				);
			}
			XOCLCodegenUIPlugin.INSTANCE.log(e);
		} finally {
			monitor.done();
		}
	}

	private void validateEcore(final IPath resourcePath) throws XoclValidationException, CoreException {
		IFile ecoreFile = ResourcesPlugin.getWorkspace().getRoot().getFile(resourcePath);
		if ((ecoreFile != null) && (ecoreFile.exists())) {
			new EcoreModelValidator().validate(ecoreFile);
			IMarker[] markers = ecoreFile.findMarkers(EcoreModelValidator.MARKER_TYPE, true, IResource.DEPTH_INFINITE);
			if (markers != null) {
				for (IMarker marker : markers) {
					if (new Integer(IMarker.SEVERITY_ERROR).equals(marker.getAttribute(IMarker.SEVERITY))
							/*|| (new Integer(IMarker.SEVERITY_WARNING).equals(marker.getAttribute(IMarker.SEVERITY)))*/) {
						if (PlatformUI.isWorkbenchRunning()) {
							final int[] responseHolder = new int[1];
							PlatformUI.getWorkbench().getDisplay().syncExec(
									new Runnable() {
										public void run() {
											MessageBox messageBox = new MessageBox(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), SWT.ICON_WARNING | SWT.YES | SWT.NO);
											messageBox.setMessage(NLS.bind(Messages.GenerateGenModelAction_ValidationErrorDialogMessage, resourcePath));
											messageBox.setText(Messages.GenerateGenModelAction_ValidationErrorDialogTitle);
											responseHolder[0] = messageBox.open();
										}
									});
							if (responseHolder[0] == SWT.YES) {
								return;
							}
						}
						throw new XoclValidationException();
					}
				}
			}
		}
	}
}