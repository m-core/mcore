package org.xocl.editorconfig.ui.codegen.actions;

import java.lang.reflect.InvocationTargetException;

public class XoclValidationException extends InvocationTargetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XoclValidationException() {
		super();
	}

	public XoclValidationException(Throwable target, String s) {
		super(target, s);
	}

	public XoclValidationException(Throwable target) {
		super(target);
	}
}