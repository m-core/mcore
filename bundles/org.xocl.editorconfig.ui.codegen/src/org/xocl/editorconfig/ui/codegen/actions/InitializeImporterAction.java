package org.xocl.editorconfig.ui.codegen.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.importer.ecore.EcoreImporter;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.PlatformUI;
import org.xocl.editorconfig.ui.codegen.XOCLCodegenUIPlugin;

public class InitializeImporterAction implements IRunnableWithProgress {
	
	EcoreImporter ecoreImporter;
		
	public InitializeImporterAction (EcoreImporter ecoreImporter) {
		this.ecoreImporter = ecoreImporter;
	}

	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		try {
			ecoreImporter.computeEPackages(new BasicMonitor.EclipseSubProgress(monitor, 40));
			ecoreImporter.adjustEPackages(new BasicMonitor.EclipseSubProgress(monitor, 20));
		} catch (Exception e) {
			final Diagnostic diagnostic = BasicDiagnostic.toDiagnostic(e);
			if (PlatformUI.isWorkbenchRunning() && diagnostic.getSeverity() == Diagnostic.ERROR) {
				PlatformUI.getWorkbench().getDisplay().asyncExec(
					new Runnable() {
						public void run() {
							DiagnosticDialog.openProblem(
								PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
								"Error during generation",
								"An error has been raised during the generation of the default EMF Generator Model",
								diagnostic
							);
						}
					}
				);
			} else {
				XOCLCodegenUIPlugin.INSTANCE.log(e);
			}
		} finally {
			monitor.done();
		}
	}

}
