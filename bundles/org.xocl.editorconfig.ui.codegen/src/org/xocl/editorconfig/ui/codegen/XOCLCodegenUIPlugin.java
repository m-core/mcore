package org.xocl.editorconfig.ui.codegen;

import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.ResourceLocator;

/**
 * The activator class controls the plug-in life cycle
 */
public class XOCLCodegenUIPlugin extends EMFPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.xocl.editorconfig.ui.codegen";
	
	// The shared instance
	public static final XOCLCodegenUIPlugin INSTANCE = new XOCLCodegenUIPlugin();

	private XOCLCodegenUIPlugin() {
		super(new ResourceLocator[]{});
	}	
	
	/**
	 * The actual implementation of the eclipse plugin
	 * to be set as the activator.
	 */
	private static Implementation plugin;
	
	public static class Implementation extends EclipseUIPlugin {
		public Implementation() {
			super();		
			plugin = this;
		}
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * @return the singleton instance.
	 */
	public static Implementation getPlugin()
	{
		return plugin;
	}
	
	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}

	
	
}
