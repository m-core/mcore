package org.xocl.editorconfig.ui.codegen.utils;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;

public class EMFWorkspaceHelper {

	public static String getLocationOf(IFile model) {
		return getLocationOf(model.getFullPath());
	}

	public static String getLocationOf(IPath fullPath) {
		URI uri = URI.createPlatformResourceURI(fullPath.toString(), true);
		return uri.toString();
	}

}
