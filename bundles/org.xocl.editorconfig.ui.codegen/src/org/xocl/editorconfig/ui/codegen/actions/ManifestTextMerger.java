package org.xocl.editorconfig.ui.codegen.actions;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.xocl.editorconfig.ui.codegen.XOCLCodegenUIPlugin;

public class ManifestTextMerger implements ITextMerger {
	private static final String LIST_SEPARATOR = ",\n "; //$NON-NLS-1$
	private static final String LINE_SEPARATOR = "\n"; //$NON-NLS-1$

	public String process(String oldContent, String newContent) {
		try {
			Map<String, String> oldManitestHeaders = ManifestElement.parseBundleManifest(new ByteArrayInputStream(oldContent.getBytes("UTF-8")), new LinkedHashMap<String, String>()); //$NON-NLS-1$
			
			Map<String, String> newManitestHeaders = ManifestElement.parseBundleManifest(new ByteArrayInputStream(newContent.getBytes("UTF-8")), new LinkedHashMap<String, String>()); //$NON-NLS-1$
			
			return mergeManifests(oldManitestHeaders, newManitestHeaders, oldContent);
			
		} catch (Exception e) {
			XOCLCodegenUIPlugin.INSTANCE.log(e);
			return oldContent;
		}
	}

	private String mergeManifests(Map<String, String> oldManitestHeaders,
			Map<String, String> newManitestHeaders, String oldContent) throws BundleException {
		Map <String, String> mergedHeaders = new LinkedHashMap<String, String>(oldManitestHeaders);
		
		boolean mergeOccurred = mergeHeaders(Constants.EXPORT_PACKAGE, oldManitestHeaders, newManitestHeaders, mergedHeaders);
		mergeOccurred |= mergeHeaders(Constants.IMPORT_PACKAGE, oldManitestHeaders, newManitestHeaders, mergedHeaders);
		mergeOccurred |= mergeHeaders(Constants.REQUIRE_BUNDLE, oldManitestHeaders, newManitestHeaders, mergedHeaders);

		if (!mergeOccurred) {
			return oldContent;
		}
		
		StringBuilder result = new StringBuilder();
		for (Map.Entry<String, String> entry : mergedHeaders.entrySet()) {
			result.append(entry.getKey() + ": " + entry.getValue()); //$NON-NLS-1$
			result.append(LINE_SEPARATOR);
		}
		return result.toString();	
	}

	private boolean mergeHeaders(String headerName,
			Map<String, String> oldManitestHeaders,
			Map<String, String> newManitestHeaders,
			Map<String, String> mergedHeaders) throws BundleException {

		String oldHeaderValue = oldManitestHeaders.get(headerName);
		String newHeaderValue = newManitestHeaders.get(headerName);

		if (oldHeaderValue == null) {
			if (newHeaderValue != null) {
				mergedHeaders.put(headerName, newHeaderValue);
			}
			return newHeaderValue != null;
		}
		
		if (newHeaderValue == null) {
			return false;
		}
		
		ManifestElement[] oldManifestElements = ManifestElement.parseHeader(headerName, oldHeaderValue);
		ManifestElement[] newManifestElements = ManifestElement.parseHeader(headerName, newHeaderValue);
		
		List<ManifestElement> mergedManifestElements = new ArrayList<ManifestElement>();
		mergedManifestElements.addAll(Arrays.asList(oldManifestElements));
		
		newManifestElementCycle : for (ManifestElement newManifestElement : newManifestElements) {
			for (ManifestElement oldManifestElement : oldManifestElements) {
				if (newManifestElement.toString().equals(oldManifestElement.toString())) {
					continue newManifestElementCycle;
				}
			}
			mergedManifestElements.add(newManifestElement);
		}
		
		if (mergedManifestElements.size() == oldManifestElements.length) {
			return false;
		}
		
		String mergedHeaderValue = getHeaderValue(headerName, mergedManifestElements);
		mergedHeaders.put(headerName, mergedHeaderValue);
		return true;
	}

	private String getHeaderValue(String headerName,
			List<ManifestElement> mergedManifestElements) {
		StringBuilder result = new StringBuilder(mergedManifestElements.get(0).toString());
		for (int i = 1, n = mergedManifestElements.size(); i < n; i ++) {
			result.append(LIST_SEPARATOR + ' ' + mergedManifestElements.get(i));
		}
		return result.toString();
	}
}
