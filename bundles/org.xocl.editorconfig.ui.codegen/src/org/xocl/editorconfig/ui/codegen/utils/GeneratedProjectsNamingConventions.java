package org.xocl.editorconfig.ui.codegen.utils;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

public class GeneratedProjectsNamingConventions {
	
	public static final String XOCL_MODEL_PROJECT_NAME = "base"; //$NON-NLS-1$
	
	public static final String XOCL_EDIT_PROJECT_NAME = "edit"; //$NON-NLS-1$
	
	public static final String XOCL_EDITOR_PROJECT_NAME = "editor"; //$NON-NLS-1$
		
	public static final String getNewEditPluginID (final String intialModelPluginID) {
		String baseProjectDirectory = getBaseProjectDirectory(intialModelPluginID);
		return baseProjectDirectory.concat(".").concat(XOCL_EDIT_PROJECT_NAME); //$NON-NLS-1$
	}
	
	public static final String getNewEditorPluginID (final String intialModelPluginID) {
		String baseProjectDirectory = getBaseProjectDirectory(intialModelPluginID);
		return baseProjectDirectory.concat(".").concat(XOCL_EDITOR_PROJECT_NAME); //$NON-NLS-1$
	}
	
	public static final String getBaseProjectDirectory(final String modelProjectDirectory) {
		return modelProjectDirectory.endsWith(".".concat(XOCL_MODEL_PROJECT_NAME)) ? modelProjectDirectory.substring(0, modelProjectDirectory.lastIndexOf(XOCL_MODEL_PROJECT_NAME)-1) : modelProjectDirectory; //$NON-NLS-1$
	}
		
	public static final String getNewEditGenerationDirectory(final String modelProjectDirectory, final String sourceFolder) {
		String baseProjectDirectory = getBaseProjectDirectory(modelProjectDirectory);
		return getFullyQualifiedDirectory(baseProjectDirectory, XOCL_EDIT_PROJECT_NAME, sourceFolder);
	}
	
	public static final String getNewEditorGenerationDirectory(final String modelProjectDirectory, final String sourceFolder) {
		String baseProjectDirectory = getBaseProjectDirectory(modelProjectDirectory);
		return getFullyQualifiedDirectory(baseProjectDirectory, XOCL_EDITOR_PROJECT_NAME, sourceFolder);
	}
	
	public static final String getMainProjectShortName (IPath path) {
		IResource ecoreFile = ResourcesPlugin.getWorkspace().getRoot().findMember(path);
		IProject project = ecoreFile.getProject();
		String name = project.getName();
		int lastIndexOfDot = name.lastIndexOf('.');
		if ((lastIndexOfDot >= 0) && (lastIndexOfDot < name.length() - 1)) {
			name = name.substring(lastIndexOfDot + 1);
		}
		return name;
	}
	
	public static final String getGenModelFileName (IPath path) {
		return getMainProjectShortName(path) + '.' + GenModelCustomizer.GENMODEL_FILE_EXTENSION;
	}
	
	public static final IPath getGenContainerPath (final IPath path) {
		String projectName = path.segment(0);
		IPath aPath = path.removeFirstSegments(1).removeLastSegments(1);
		String fullyQualifiedContainerName = getFullyQualifiedDirectory(projectName, XOCL_MODEL_PROJECT_NAME, aPath.toString());
		IPath result = new Path(fullyQualifiedContainerName);
		return result;
	}
	
	public static final IPath getGenModelPath (final IPath path) {
		return getGenContainerPath(path).append(path);
	}
	
	private static final String getFullyQualifiedDirectory(final String modelProjectDirectory, final String XOCLProjectName, final String directory) {
		StringBuilder stringBuilder = new StringBuilder(modelProjectDirectory);
		stringBuilder.append(".").append(XOCLProjectName); //$NON-NLS-1$
		stringBuilder.append(IPath.SEPARATOR).append(directory);
		return stringBuilder.toString();
	}
	
	public static final String getNewBasePackage (final String modelProjectDirectory, final String ePackageName) {
		String relativeModelprojectDirectory = new Path(modelProjectDirectory).makeRelative().toString();
		String[] modelProjectDirectories = relativeModelprojectDirectory.split("\\."); //$NON-NLS-1$
		int indexOfLastBasePackage = modelProjectDirectories.length-1;
		if (modelProjectDirectories[indexOfLastBasePackage].equals(XOCL_MODEL_PROJECT_NAME)) {
			indexOfLastBasePackage -- ;
		}
		if (modelProjectDirectories[indexOfLastBasePackage].equals(ePackageName)) {
			indexOfLastBasePackage --;
		}
		
		StringBuilder stringBuilder = new StringBuilder(modelProjectDirectories[0]);
		for (int i = 1; i <= indexOfLastBasePackage; i++) {
			stringBuilder.append(".").append(modelProjectDirectories[i]); //$NON-NLS-1$
		}
		
		return stringBuilder.toString();
	}

}
