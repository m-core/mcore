package org.xocl.editorconfig.ui.codegen.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.xocl.core.util.XoclHelper;

public class BackupMetamodelCopy extends WorkspaceModifyOperation {

	public static final String COPY_EXTENSION = "backup"; //$NON-NLS-1$
	
	IFile metamodelCopy;

	private final boolean myIsCopy;
	
	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException,
			InvocationTargetException, InterruptedException {

		if ((metamodelCopy == null) || !metamodelCopy.exists()) {
			return;
		}
		
		String timestamp = XoclHelper.timestamp();
		
		IPath destination = metamodelCopy.getFullPath().addFileExtension(timestamp).addFileExtension(COPY_EXTENSION);
		if (myIsCopy) {
			metamodelCopy.copy(destination, true, monitor);
		} else {
			metamodelCopy.move(destination, true, monitor);
		}
	}
	
	/**
	 * @param metamodelCopy
	 */
	public BackupMetamodelCopy(IPath genModelContainerPath, URI metamodelURI, boolean isCopy) {
		myIsCopy = isCopy;
		String metamodelFileName = metamodelURI.lastSegment();
		IPath metamodelCopyPath = genModelContainerPath.append(metamodelFileName);
		metamodelCopy = ResourcesPlugin.getWorkspace().getRoot().getFile(metamodelCopyPath);
	}

	public BackupMetamodelCopy(IFile metamodelCopy, boolean isCopy) {
		this.metamodelCopy = metamodelCopy;
		myIsCopy = isCopy;
	}
}