package org.xocl.editorconfig.ui.codegen.actions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.xocl.editorconfig.ui.codegen.XOCLCodegenUIPlugin;
import org.xocl.editorconfig.ui.codegen.utils.WildcardFilenameFilter;

public class XoclGenerationSettings {
	public static final String XOCLGENERATENOT_FILENAME = "XOCLGENERATENOT"; //$NON-NLS-1$
	private List<WildcardFilenameFilter> myGenerateNotFilters = new ArrayList<WildcardFilenameFilter>();

	public XoclGenerationSettings(IFile projectFile) {
		appendGenerationSettings(projectFile);
	}

	private void appendGenerationSettings(IFile projectFile) {
		try {
			IProject project = projectFile.getProject();
			IResource generateNotResource = project.findMember(XOCLGENERATENOT_FILENAME);
			if (generateNotResource instanceof IFile) {
				IFile generateNotFile = (IFile) generateNotResource;
				if (generateNotFile.exists()) {
					InputStream inputStream = generateNotFile.getContents();
					String charset = generateNotFile.getCharset();
					BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
					String line;
					while ((line = reader.readLine()) != null) {
						if (line.trim().length() != 0) {
							WildcardFilenameFilter filter = new WildcardFilenameFilter(line);
							myGenerateNotFilters.add(filter);
						}
					}
				}
			}
		} catch (Exception e) {
			XOCLCodegenUIPlugin.INSTANCE.log(e);
		}
	}	

	public void appendProject(IFile projectFile) {
		appendGenerationSettings(projectFile);
	}

	public boolean isGenerated(IResource resource) {
		IPath resourcePath = resource.getFullPath();
		return isGenerated(resourcePath);
	}

	public boolean isGenerated(IPath absolutePath) {
		for (WildcardFilenameFilter filter : myGenerateNotFilters) {
			if (filter.accept(absolutePath.makeAbsolute())) {
				return false;
			}
		}
		return true;
	}

}