package org.xocl.common.edit.ui.provider;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class EcoreViewerFilter extends ViewerFilter {

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof Resource) {
			URI uri = ((Resource)element).getURI();
			if (uri != null && uri.fileExtension() != null) {
				return !uri.fileExtension().equals("ecore");
			}
		}
		return true;
	}

}
