package com.montages.mcore.mcore2ecore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;

public class MObject2EObject {

	private final Map<MObject, EObject> processed;

	public MObject2EObject(Map<MObject, EObject> processed) {
		this.processed = processed;
	}

	public EObject transform(MObject mObject, XMLResource res) {
		if (processed.containsKey(mObject)) {
			EObject eo = processed.get(mObject);
			res.setID(eo, mObject.eResource().getURIFragment(mObject));
			return eo;
		}
		
		return Optional.ofNullable(mObject)
			.filter(mo -> mo.getType() != null)
			.map(mo -> findEClass(mo.getType()))
			.filter(eClass -> !eClass.eIsProxy())
			.map(MObject2EObject::createEObjectOrNull)
			.map(eObject -> processEObject(eObject, mObject, res))
			.orElse(null);
	}
	
	private EObject processEObject(EObject eObject, MObject mObject, XMLResource res) {
		processed.put(mObject, eObject);
		res.setID(eObject, mObject.eResource().getURIFragment(mObject));
		setEStructuralFeatures(eObject, mObject, res);

		return eObject;
	}
	
	private static EObject createEObjectOrNull(final EClass eClass) {
		try {
			return EcoreUtil.create(eClass);
		} catch(Exception e) {
			System.out.println("Can't create EObject from eClass: " + eClass);
			return null;
		}
	}

	private void setEStructuralFeatures(EObject eObject, MObject mObject, XMLResource res) {
		for (MPropertyInstance mPropertyInstance: mObject.getPropertyInstance()) {
			MProperty mProperty = mPropertyInstance.getProperty();

			if (mProperty != null) {
				if (mProperty.getIsAttribute()) {
					setEAttributes(eObject, mObject, mPropertyInstance);
				} else if (mProperty.getIsReference()) {
					setEReferences(eObject, mObject, mPropertyInstance, res);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void setEAttributes(EObject eObject, MObject mObject, MPropertyInstance mPropertyInstance) {
		MProperty mProperty = mPropertyInstance.getProperty();
		EAttribute eAttribute = (EAttribute) eObject.eClass().getEStructuralFeature( mProperty.getEName() );

		if (eAttribute != null && !eAttribute.isDerived() && !eAttribute.isTransient() && !eAttribute.isVolatile()) {

			List<String> values = getValues(eAttribute, mPropertyInstance);

			if (eAttribute.isMany()) {
				@SuppressWarnings("rawtypes")
				Collection list = (Collection) eObject.eGet(eAttribute);
				for (String value: values) {
					Object eValue = createValueFromString(eAttribute.getEAttributeType(), value);
					if (eValue != null) {
						list.add(eValue);
					}
				}
			} else if (!values.isEmpty()) {
				String value = values.get(0);

				Object eValue = createValueFromString(eAttribute.getEAttributeType(), value);
				if (eValue != null) {
					eObject.eSet(eAttribute, eValue);
				}
			}
		}
	}

	public static Object createValueFromString(EDataType type, String value) {
		if (value == null || value.trim().isEmpty())
			return null;
		else
			return EcoreUtil.createFromString(type, value);
	}

	private void setEReferences(EObject eObject, MObject mObject, MPropertyInstance mPropertyInstance, XMLResource res) {
		MProperty mProperty = mPropertyInstance.getProperty();
		EStructuralFeature feature = eObject.eClass().getEStructuralFeature( mProperty.getEName() );
		if (feature instanceof EReference) {
			EReference eReference = (EReference) feature; 

			List<MObject> values = getValues( eReference, mPropertyInstance );
			if (eReference.isMany()) {
				for (MObject value: values) {
					setReferenceValue(eObject, mObject, eReference, value, res);
				}
			} else if (!values.isEmpty()) {
				setReferenceValue(eObject, mObject, eReference, values.get(0), res);
			}
		}
	}

	private void setReferenceValue(EObject eObject, MObject mObject, EReference eReference, MObject value, XMLResource res) {
		if (value == null) return;

		if (value.equals(mObject)) {
			if (eReference.isMany()) {
				@SuppressWarnings("unchecked")
				Collection<EObject> values = (Collection<EObject>) eObject.eGet(eReference);
				values.add(eObject);
			} else {
				eObject.eSet(eReference, eObject);
			}
		} else {
			EObject eValue = transform(value, res);
			if (eValue != null) {
				if (eReference.isMany()) {
					@SuppressWarnings("unchecked")
					Collection<EObject> values = (Collection<EObject>) eObject.eGet(eReference);
					values.add(eValue);
				} else {
					eObject.eSet(eReference, eValue);
				}
			}
		}
	}

	private List<String> getValues(EAttribute eAttribute, MPropertyInstance mPropertyInstance) {
		List<String> values = new ArrayList<String>();

		if (!mPropertyInstance.getInternalDataValue().isEmpty()) {
			for (MDataValue dataValue: mPropertyInstance.getInternalDataValue()) {
				if (dataValue.getDataValue() != null)
					values.add( dataValue.getDataValue() );
			}
		} else if (!mPropertyInstance.getInternalLiteralValue().isEmpty()) {
			for (MLiteralValue litValue: mPropertyInstance.getInternalLiteralValue()) {
				if (litValue.getLiteralValue() != null) {
					String value = litValue.getLiteralValue().getEName();

					if (eAttribute.getEAttributeType() instanceof EEnum) {
						EEnum eenum = (EEnum) eAttribute.getEAttributeType();
						EEnumLiteral lit = eenum.getEEnumLiteral(value);

						if (lit != null) {
							value = lit.getLiteral();
						} else {
							value = null;
						}
					}

					if (value != null) {
						values.add( value );	
					}
				}
			}
		}
		return values;
	}

	private List<MObject> getValues(EReference eReference, MPropertyInstance mPropertyInstance) {
		if (eReference.isContainment()) {
			return mPropertyInstance.getInternalContainedObject();
		} else {
			List<MObject> referenced = new ArrayList<MObject>();
			for (MObjectReference ref: mPropertyInstance.getInternalReferencedObject()) {
				referenced.add(ref.getReferencedObject());
			}
			return referenced;
		}
	}

	public EClass findEClass(MClassifier mClassifier) {
		if (mClassifier.getInternalEClassifier() instanceof EClass) {
			return (EClass) mClassifier.getInternalEClassifier();
		} else {
			return null;
		}
	}

}