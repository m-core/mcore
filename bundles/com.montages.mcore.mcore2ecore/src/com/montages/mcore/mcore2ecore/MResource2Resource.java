package com.montages.mcore.mcore2ecore;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;

import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MResource;

public class MResource2Resource {

	private final Map<MObject, EObject> processed = new HashMap<MObject, EObject>();
	private final MObject2EObject transf = new MObject2EObject(processed);

	public void transform(MResource mResource, Resource resource) {
		for (MObject mObject: mResource.getObject()) {
			EObject eObject = transf.transform(mObject, (XMLResource)resource);
			if (eObject != null) {
				resource.getContents().add(eObject);
			}
		}
	}

	public void finish() {
		this.processed.clear(); 
	}
	
}
