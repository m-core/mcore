package org.xocl.delegates;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Internal.SettingDelegate;

public class XOCLSettingDelegateFactory implements SettingDelegate.Factory {

	@Override
	public SettingDelegate createSettingDelegate(EStructuralFeature eStructuralFeature) {
		return new XOCLSettingDelegate(eStructuralFeature);
	}

}
