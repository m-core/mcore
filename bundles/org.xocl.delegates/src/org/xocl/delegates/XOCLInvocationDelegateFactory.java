package org.xocl.delegates;

import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EOperation.Internal.InvocationDelegate;
import org.xocl.delegates.updates.UpdateInvocationDelegate;
import org.eclipse.emf.ecore.EStructuralFeature;

public class XOCLInvocationDelegateFactory implements InvocationDelegate.Factory {

	@Override
	public InvocationDelegate createInvocationDelegate(EOperation operation) {
		if (operation.getName().endsWith("$Update")) {

			int pos = operation.getName().lastIndexOf("$");

			EStructuralFeature feature = operation.getEContainingClass().getEAllStructuralFeatures().stream() //
					.filter(e -> e.getName().equals(operation.getName().substring(0, pos))) //
					.findFirst() //
					.orElse(null);

			if (feature != null && (feature.isDerived() && feature.isChangeable())) {
				return new UpdateInvocationDelegate(feature);
			}
		}
		return new XOCLInvocationDelegate(operation);
	}

}
