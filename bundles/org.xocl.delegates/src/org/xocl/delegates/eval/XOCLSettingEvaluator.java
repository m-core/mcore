package org.xocl.delegates.eval;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.BasicEObjectImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary;

/**
 * XOCL dynamic evaluator for {@link EStructuralFeature}s.
 * 
 */
public class XOCLSettingEvaluator {

	private final EStructuralFeature eStructuralFeature;
	private final Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	public XOCLSettingEvaluator(EStructuralFeature eStructuralFeature) {
		this.eStructuralFeature = eStructuralFeature;
	}

	public Object eval(InternalEObject owner, String expression) {
		OCL ocl = OCL.newInstance(new XoclLibrary.XoclEnvironmentFactory(prepareRegistry(owner)));
		ParsingOptions.setOption(ocl.getEnvironment(), ParsingOptions.implicitRootClass(ocl.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(ocl.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);

		Helper helper = prepareHelper(ocl, owner);
		Object result = null;
		OCLExpression oclExpression = null;

		try {
			oclExpression = helper.createQuery(expression);
		} catch (ParserException e) {
			System.err.println("cannot parse " + expression);
			expression = null;
			e.printStackTrace();
		}

		if (expression != null) {
			Query query = ocl.createQuery(oclExpression);
			XoclErrorHandler.enterContext("org.xocl.delegates", query, owner.eClass(), eStructuralFeature);

			XoclEvaluator xoclEval = new XoclEvaluator((BasicEObjectImpl) owner, cachedValues);
			try {
				result = xoclEval.evaluateElement(eStructuralFeature, query);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	private Helper prepareHelper(OCL ocl, EObject owner) {
		Helper helper = ocl.createOCLHelper();
		helper.setAttributeContext(owner.eClass(), eStructuralFeature);

		EClassifier propertyType = TypeUtil.getPropertyType(ocl.getEnvironment(), owner.eClass(), eStructuralFeature);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName("trg");
		trgVar.setType(propertyType);
		ocl.getEnvironment().addElement("trg", trgVar, true);

		return helper;
	}

	private EPackage.Registry prepareRegistry(EObject owner) {
		EPackage.Registry registry;
		if (owner.eResource() == null || owner.eResource().getResourceSet() == null) {
			registry = owner.eClass().getEPackage().eResource().getResourceSet().getPackageRegistry();
		} else {
			registry = owner.eResource().getResourceSet().getPackageRegistry();
		}

		if (registry != null) {
			registry.putAll(EPackage.Registry.INSTANCE);
		}

		return registry;
	}
}
