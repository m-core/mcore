package org.xocl.delegates.eval;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.BasicEObjectImpl;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary;

public class XOCLOperationEvaluator {

	private final EOperation operation;
	private final Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	public XOCLOperationEvaluator(EOperation operation) {
		this.operation = operation;
	}

	public Object eval(EObject owner, Object... values) {
		OCL ocl = OCL.newInstance(new XoclLibrary.XoclEnvironmentFactory(prepareRegistry(owner)));
		ParsingOptions.setOption(ocl.getEnvironment(), ParsingOptions.implicitRootClass(ocl.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(ocl.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);

		Helper helper = prepareHelper(ocl, owner);
		Object result = null;
		OCLExpression oclExpression = null;

		String body = XoclEmfUtil.findBodyAnnotationText(operation, owner.eClass());

		try {
			oclExpression = helper.createQuery(body);
		} catch (ParserException e) {
			throw new RuntimeException("Cannot parse operation body expression");
		}

		Query query = ocl.createQuery(oclExpression);
		XoclErrorHandler.enterContext("org.xocl.delegates", query, owner.eClass(), operation);

		EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

		for (int i = 0; i < operation.getEParameters().size(); i++) {
			EParameter param = operation.getEParameters().get(i);
			Object value = (values != null && values.length > i) ? values[i] : null;

			evalEnv.add(param.getName(), value);
		}

//		evalEnv.add("trg", trg);
//		evalEnv.add("obj", obj);

		XoclEvaluator xoclEval = new XoclEvaluator((BasicEObjectImpl) owner, cachedValues);
		try {
			result = xoclEval.evaluateElement(operation, query);
		} catch (Exception e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

		return result;
	}

	private Helper prepareHelper(OCL ocl, EObject owner) {
		Helper helper = ocl.createOCLHelper();
		helper.setOperationContext(owner.eClass(), operation);

		return helper;
	}

	private EPackage.Registry prepareRegistry(EObject owner) {
		EPackage.Registry registry;
		if (owner.eResource() == null || owner.eResource().getResourceSet() == null) {
			registry = EPackage.Registry.INSTANCE;
		} else {
			registry = owner.eResource().getResourceSet().getPackageRegistry();
			registry.putAll(EPackage.Registry.INSTANCE);
		}

		return registry;
	}
}
