package org.xocl.delegates.updates;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.xocl.semantics.XUpdateMode;

public class UpdateHelpers {

	public static XUpdateMode getMode(EAnnotation annotation, String updateName) {
		String value = annotation.getDetails().get(updateName + "Mode");
		String mode = value.substring(3, value.length());

		return XUpdateMode.valueOf(mode.toUpperCase());
	}

	public static EOperation findOperation(EClass eClass, String name) {
		if (name == null) {
			return null;
		}

		return eClass.getEAllOperations().stream() //
				.filter(e -> e.getName().equals(name)) //
				.findAny() //
				.orElse(null);
	}

	public static EStructuralFeature findFeature(EClass eClass, String updateName, EAnnotation annotation) {
		String featurePackage = annotation.getDetails().get(updateName + "FeaturePackage");
		String featureClass = annotation.getDetails().get(updateName + "FeatureClass");
		String featureName = annotation.getDetails().get(updateName + "Feature");

		ResourceSet resourceSet = eClass.eResource().getResourceSet();

		EPackage ePackage;
		if (featurePackage != null) {
			ePackage = resourceSet.getPackageRegistry().getEPackage(featurePackage);
		} else {
			ePackage = eClass.getEPackage();
		}

		if (ePackage == null) {
			throw new IllegalArgumentException("Cannot find ePackage");
		}

		EClass classifier;
		if (featureClass != null) {
			classifier = (EClass) ePackage.getEClassifier(featureClass);
		} else {
			classifier = eClass;
		}

		if (classifier == null) {
			throw new IllegalArgumentException("Cannot find eClass");
		}
		
		return classifier.getEStructuralFeature(featureName);
	}

}
