package org.xocl.delegates;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EOperation.Internal.InvocationDelegate;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.BasicEObjectImpl;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary;

public class XOCLInvocationDelegate implements InvocationDelegate {

	private final EOperation operation;
	private final Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	public XOCLInvocationDelegate(EOperation operation) {
		this.operation = operation;
	}

	@Override
	public Object dynamicInvoke(InternalEObject target, EList<?> arguments) throws InvocationTargetException {
		Object result = null;

		EPackage.Registry registry;
		if (target.eResource() == null || target.eResource().getResourceSet() == null) {
			registry = EPackage.Registry.INSTANCE;
		} else {
			registry = target.eResource().getResourceSet().getPackageRegistry();
			registry.putAll(EPackage.Registry.INSTANCE);
		}

		OCL ocl = OCL.newInstance(new XoclLibrary.XoclEnvironmentFactory(registry));
		ParsingOptions.setOption(ocl.getEnvironment(), ParsingOptions.implicitRootClass(ocl.getEnvironment()), EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(ocl.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);

		OCL.Helper helper = ocl.createOCLHelper();
		helper.setOperationContext(operation.getEContainingClass(), operation);
		String body = XoclEmfUtil.findBodyAnnotationText(operation, target.eClass());

		OCLExpression expression = null;
		try {
			expression = helper.createQuery(body);
		} catch (ParserException e) {
			expression = null;
			e.printStackTrace();
		}

		if (expression != null) {

			OCL.Query query = ocl.createQuery(expression);
			XoclErrorHandler.enterContext("org.xocl.delegates", query, target.eClass(), operation);
			XoclEvaluator xoclEval = new XoclEvaluator((BasicEObjectImpl) target, cachedValues);

			if (!arguments.isEmpty()) {
				EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();
				for (int i = 0; i < operation.getEParameters().size(); i++) {
					EParameter param = operation.getEParameters().get(i);
					evalEnv.add(param.getName(), arguments.get(i));
				}
			}

			try {
				result = xoclEval.evaluateElement(operation, query);
			} catch (Exception e) {
				result = null;
				e.printStackTrace();
			}
		}

		return result;
	}

}