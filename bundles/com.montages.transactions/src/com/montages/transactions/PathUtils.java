package com.montages.transactions;

import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.part.FileEditorInput;

import com.montages.common.McoreUtil;

public class PathUtils {

	private static final String DEFAULT_DOMAIN_ID = "com.montages.mcore.diagram.EditingDomain";

	public static String buildDomainID(URI mcoreResourceURI) {
		if (mcoreResourceURI == null) {
			return DEFAULT_DOMAIN_ID;
		}
		String[] pathParts = mcoreResourceURI.toString().split("/");
		StringBuilder builder = new StringBuilder();
		for (int i = pathParts.length - 1; i > pathParts.length - 3; i--) {
			builder.insert(0, pathParts[i]);
			builder.insert(0, '/');
		}
		return builder.toString();
	}

	public static URI getInputURI(IEditorInput input) {
		URI uri = null;
		if (input instanceof URIEditorInput) {
			uri = ((URIEditorInput) input).getURI();
		} else if (input instanceof FileEditorInput) {
			FileEditorInput fileInput = (FileEditorInput) input;
			uri = URI.createFileURI(fileInput.getPath().toString());
		}
		return uri;
	}

	public static String getMcoreResourceFromDiagramNotificationResource(IEditorInput input) {
		return getDomainIDFromDiagram(getInputURI(input));
	}

	public static String getDomainIDFromDiagram(URI diagramFileURI) {
		URI mcoreURI = McoreUtil.getMcoreURIFromDiagram(diagramFileURI);
		return mcoreURI == null ? DEFAULT_DOMAIN_ID :PathUtils.buildDomainID(mcoreURI);
	}
}
