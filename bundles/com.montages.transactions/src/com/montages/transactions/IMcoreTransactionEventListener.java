package com.montages.transactions;


public interface IMcoreTransactionEventListener {

	public void handleMcoreEvent(McoreTransactionEvent event);
}
