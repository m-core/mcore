package com.montages.transactions;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.semantics.commands.DeleteCommandWithOptimizedCrossReferencer;

import com.montages.mcore.MProperty;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsPackage;

public class MCoreDeleteCommand extends DeleteCommandWithOptimizedCrossReferencer {
	private List<MPropertyInstance> myPropertyInstancesToRemove;

	public MCoreDeleteCommand(EditingDomain domain, Collection<?> collection) {
		super(domain, collection);
	}

	@Override
	protected Map<EObject, Collection<Setting>> findReferences(Collection<EObject> eObjects) {
		Map<EObject, Collection<Setting>> allCrossReferences = super.findReferences(eObjects);
		Set<MProperty> deletedProperties = eObjects.stream().filter(MProperty.class::isInstance)
				.map(MProperty.class::cast).collect(Collectors.toSet());

		if (!deletedProperties.isEmpty()) {
			for (Map.Entry<EObject, Collection<Setting>> entry : allCrossReferences.entrySet()) {
				EObject referenceTarget = entry.getKey();
				if (deletedProperties.contains(referenceTarget)) {
					Collection<Setting> settings = entry.getValue();
					if (settings != null) {
						myPropertyInstancesToRemove = settings.stream() //
								.filter(s -> s.getEObject() instanceof MPropertyInstance) //
								.filter(s -> s.getEStructuralFeature() == ObjectsPackage.Literals.MPROPERTY_INSTANCE__PROPERTY)
								.map(s -> (MPropertyInstance) s.getEObject()).collect(Collectors.toList());
					}
				}
			}
		}
		return allCrossReferences;
	}

	@Override
	public void execute() {
		super.execute();
		if (myPropertyInstancesToRemove != null) {
			List<Command> removeSlots = myPropertyInstancesToRemove.stream() //
					.map(next -> domain.createCommand(DeleteCommand.class, // 
							new CommandParameter(null, null, Collections.singletonList(next)))) //
					.filter(Objects::nonNull) //
					.collect(Collectors.toList());

			removeSlots.forEach(this::appendAndExecute);
		}
	}

}
