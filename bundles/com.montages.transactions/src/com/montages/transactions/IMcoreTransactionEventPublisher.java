package com.montages.transactions;


public interface IMcoreTransactionEventPublisher {

	public void publish(McoreTransactionEventType event, Object owner);

}
