package com.montages.transactions;

import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;

public class McoreTransactionEvent {

	private final McoreTransactionEventType myType;

	private final Object myPublisher;

	private final McoreDiagramEditingDoamin myDomain;

	public McoreTransactionEvent(McoreDiagramEditingDoamin domain, McoreTransactionEventType type, Object publisher) {
		myType = type;
		myPublisher = publisher;
		myDomain = domain;
	}

	public Object getPublisher() {
		return myPublisher;
	}

	public McoreTransactionEventType getType() {
		return myType;
	}

	public McoreDiagramEditingDoamin getDomain() {
		return myDomain;
	}
}
