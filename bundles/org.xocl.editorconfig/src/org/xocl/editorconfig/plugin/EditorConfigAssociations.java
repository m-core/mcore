/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;

/**
 * @author Max Stepanov
 *
 */
/* package */ class EditorConfigAssociations {

	private static final String EXTENSION_POINT_ID = EditorConfigPlugin.ID + ".editorConfigAssociations";
	private static final String TAG_MODEL_ASSOCIATION = "editorConfigModelAssociation";
	private static final String ATT_URI = "uri";
	private static final String ATT_CONFIG = "config";
	
	private static EditorConfigAssociations instance = null;
	
	private Map<String, List<URI>> modelAssociations = new HashMap<String, List<URI>>();
	
	/**
	 * 
	 */
	private EditorConfigAssociations() {
		readRegistry();
	}
	
	public static EditorConfigAssociations getInstance() {
		if (instance == null) {
			instance = new EditorConfigAssociations();
		}
		return instance;
	}
	
	private void readRegistry() {
		IConfigurationElement[] elements = Platform.getExtensionRegistry()
							.getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (int i = 0; i < elements.length; ++i) {
			readElement(elements[i]);
		}
	}
	
	private void readElement(IConfigurationElement element) {
		if (TAG_MODEL_ASSOCIATION.equals(element.getName())) {
			String uri = element.getAttribute(ATT_URI);
			if (uri == null || uri.length() == 0) {
				return;
			}
			
			String config = element.getAttribute(ATT_CONFIG);
			if (config == null || config.length() == 0) {
				return;
			}
			URI configURI = URI.createPlatformPluginURI(element.getNamespaceIdentifier()+"/"+config, true);
			
			List<URI> list = modelAssociations.get(uri);
			if (list == null) {
				list = new ArrayList<URI>();
				modelAssociations.put(uri, list);
			}
			list.add(configURI);			
		}
	}
	
	public URI getEditorConfigForURI(String uri) {
		List<URI> list = modelAssociations.get(uri);
		if (list != null) {
			return list.get(0);
		}
		return null;
	}

}
