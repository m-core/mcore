/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.workbench.ide.builder;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.xocl.workbench.ide.IDEPlugin;

/**
 * @author Max Stepanov
 *
 */
public class EcoreModelBuilder extends IncrementalProjectBuilder {

	public static final String BUILDER_ID = "org.xocl.workbench.ide.ecoreModelBuilder";
	
	private EcoreModelValidator validator = new EcoreModelValidator();

	private class SampleDeltaVisitor implements IResourceDeltaVisitor {

		/* (non-Javadoc)
		 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
		 */
		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
				checkResource(resource);
				break;
			case IResourceDelta.REMOVED:
				break;
			case IResourceDelta.CHANGED:
				checkResource(resource);
				break;
			}
			//return true to continue visiting children.
			return true;
		}
	}

	private class SampleResourceVisitor implements IResourceVisitor {
		public boolean visit(IResource resource) {
			checkResource(resource);
			//return true to continue visiting children.
			return true;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.resources.IncrementalProjectBuilder#build(int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor) throws CoreException {
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null || delta.getKind() == IResourceDelta.NO_CHANGE) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	protected void fullBuild(final IProgressMonitor monitor) throws CoreException {
		getProject().accept(new SampleResourceVisitor());
	}

	protected void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		delta.accept(new SampleDeltaVisitor());
	}


	private void checkResource(IResource resource) {
		if (resource instanceof IFile && "ecore".equals(((IFile)resource).getFileExtension())) {
			IFile file = (IFile) resource;
			try {
				validator.validate(file);
			} catch (CoreException e) {
				IDEPlugin.log(e.getStatus());
			}
		}
	}
	
	/* package */static void cleanBuilder(IProject project) {
		try {
			project.accept(new IResourceVisitor() {
				public boolean visit(IResource resource) throws CoreException {
					if (resource instanceof IFile && resource.getName().endsWith(".ecore")) {
						((IFile) resource).deleteMarkers(EcoreModelValidator.MARKER_TYPE, false, IResource.DEPTH_ZERO);
					}
					return true;
				}
			});
		} catch (CoreException e) {
			IDEPlugin.log(e.getStatus());
		}
	}

}
