package org.xocl.workbench.ide.validation;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ui.IStartup;

public class XoclEcoreValidatorStartup implements IStartup {

	public void earlyStartup() {
		EValidator.Registry.INSTANCE.put(
			EcorePackage.eINSTANCE,
			new XoclEcoreValidatorAdapter());
	}
}