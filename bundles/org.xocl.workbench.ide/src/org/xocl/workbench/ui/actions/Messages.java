package org.xocl.workbench.ui.actions;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.workbench.ui.actions.messages"; //$NON-NLS-1$
	public static String OpenOCLEditorActionDelegate_DialogMessage;
	public static String OpenOCLEditorActionDelegate_EditorTitle;
	public static String OpenOCLEditorActionDelegate_OpenFailedError;
	public static String OpenOCLEditorActionDelegate_UnknownAnnotationSourceError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
