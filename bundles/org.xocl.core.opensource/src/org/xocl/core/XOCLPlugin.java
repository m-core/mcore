package org.xocl.core;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.framework.BundleContext;
import org.xocl.internal.core.bundle.Core;

/**
 * The activator class controls the plug-in life cycle
 */
public class XOCLPlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = Core.BUNDLE_ID;

	// The shared instance
	private static XOCLPlugin plugin;

	private IEclipsePreferences prefs;
	/**
	 * The constructor
	 */
	public XOCLPlugin() {
	}

	public void start(BundleContext context) throws Exception {
		plugin = this;
		prefs = InstanceScope.INSTANCE.getNode(PLUGIN_ID);
		super.start(context);
	}

	public void stop(BundleContext context) throws Exception {
		prefs = null;
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static XOCLPlugin getDefault() {
		return plugin;
	}

	public static void log(Throwable e) {
		log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, e.getLocalizedMessage(), e));
	}

	public static void log(String msg) {
		log(new Status(IStatus.INFO, PLUGIN_ID, IStatus.OK, msg, null)); 
	}

	public static void log(IStatus status) {
		getDefault().getLog().log(status);
	}

	public IEclipsePreferences getPrefs() {
		return prefs;
	}
}
