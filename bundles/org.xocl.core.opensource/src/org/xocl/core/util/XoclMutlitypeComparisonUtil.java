package org.xocl.core.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osgi.util.NLS;

public class XoclMutlitypeComparisonUtil {
	@SuppressWarnings("unchecked")
	public static final int compare(Object o1, Object o2) {
		XoclMutlitypeComparisonUtil.Type type = Type.deduceCommonType(o1, o2);
		Object obj1 = Type.convertToType(o1, type);
		Object obj2 = Type.convertToType(o2, type);
		switch (type) {
		case Number:
			return compareNumbers((Number) obj1, (Number) obj2);
		case String:
			return compareStrings((String) obj1, (String) obj2);
		case NumberSequence:
			return compareNumberLists((List<Number>) obj1, (List<Number>) obj2);
		case StringSequence:
			return compareStringLists((List<String>) obj1, (List<String>) obj2);
		}
		return 0;
	}
	
	private static int compareNumbers(Number n1, Number n2) {
		return Double.compare(n1.doubleValue(), n2.doubleValue()); 
	}
	
	private static int compareStrings(String s1, String s2) {
		return s1.compareTo(s2); 
	}
	
	private static int compareNumberLists(List<Number> numberList1, List<Number> numberList2) {
		if (numberList1.isEmpty()) {
			return numberList2.isEmpty() ? 0 : 1;
		}
		for (int i = 0, n = numberList1.size(); i < n; i++) {
			if (numberList2.size() <= i) {
				return -1;
			}
			int resultCandidate = compareNumbers(numberList1.get(i), numberList2.get(i));
			if (resultCandidate != 0) {
				return resultCandidate;
			}
		}
		return 0; 
	}
	
	private static int compareStringLists(List<String> numberList1, List<String> numberList2) {
		if (numberList1.isEmpty()) {
			return numberList2.isEmpty() ? 0 : 1;
		}
		for (int i = 0, n = numberList1.size(); i < n; i++) {
			if (numberList2.size() <= i) {
				return -1;
			}
			int resultCandidate = compareStrings(numberList1.get(i), numberList2.get(i));
			if (resultCandidate != 0) {
				return resultCandidate;
			}
		}
		return 0; 
	}
	
	private static enum Type {
		Number, String, NumberSequence, StringSequence;
		
		public static final XoclMutlitypeComparisonUtil.Type getType(Object object) {
			if (object instanceof Number) {
				return Number;
			} else if (object instanceof String) {
				return String;
			} else if (object instanceof List<?>) {
				List<?> list = (List<?>) object;
				if (list.isEmpty()) {
					return NumberSequence;
				}
				if (list.get(0) instanceof Number) {
					return NumberSequence;
				} else if (list.get(0) instanceof String) {
					return StringSequence;
				}
			}
			throw new RuntimeException(NLS.bind(Messages.XoclMutlitypeComparisonUtil_WrongObjectType0,
					object, (object == null) ? null : object.getClass().getCanonicalName()));
		}
		
		@SuppressWarnings("unchecked")
		public static final Object convertToType(Object object, XoclMutlitypeComparisonUtil.Type type) {
			if (object instanceof Number) {
				switch (type) {
				case String:
					return convertToString((Number) object);
				case NumberSequence:
					return convertToList((Number) object);
				case StringSequence:
					return convertToList(convertToString((Number) object));
				}
			} else if (object instanceof String) {
				switch (type) {
				case StringSequence:
					return convertToList((String) object);
				}
			} else if (object instanceof List<?>) {
				List<?> list = (List<?>) object;
				if (list.isEmpty()) {
					return object;
				}
				if (list.get(0) instanceof Number) {
					switch (type) {
					case StringSequence:
						return convertToStringList((List<Number>) list);
					}
				} 
			}
			return object;
		}
		
		public static final  XoclMutlitypeComparisonUtil.Type deduceCommonType(Object o1, Object o2) {
			XoclMutlitypeComparisonUtil.Type type1 = Type.getType(o1);
			XoclMutlitypeComparisonUtil.Type type2 = Type.getType(o2);
			if (((type1 == Type.NumberSequence) &&  (type2 == Type.String))
					|| ((type2 == Type.NumberSequence) &&  (type1 == Type.String))) {
				return Type.StringSequence;
			}
			if (type1.compareTo(type2) >= 0) {
				return type1;
			}
			return type2;
		}
		
		private static String convertToString(Number number) {
			return number.toString();
		}
		
		private static <T> List<T> convertToList(T object) {
			List<T> list = new ArrayList<T>();
			list.add(object);
			return list;
		}

		private static List<String> convertToStringList(List<Number> numberList) {
			List<String> stringList = new ArrayList<String>();
			for (Number number : numberList) {
				stringList.add(convertToString(number));
			}
			return stringList;
		}
	}
}