package org.xocl.core.util;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.core.util.messages"; //$NON-NLS-1$
	public static String XoclMutlitypeComparisonUtil_WrongObjectType0;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
