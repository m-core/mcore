package org.xocl.internal.core.bundle;


/**
 * @author Marc Moser
 */
public final class Core
{
  public static final String BUNDLE_ID = "org.xocl.core"; //$NON-NLS-1$

//  public static final OMBundle BUNDLE = OMPlatform.INSTANCE.bundle(BUNDLE_ID, Core.class);
//
//  public static final OMTracer DEBUG = BUNDLE.tracer("debug"); //$NON-NLS-1$
//
//  public static final OMLogger LOG = BUNDLE.logger();

  private Core()
  {
  }
}
