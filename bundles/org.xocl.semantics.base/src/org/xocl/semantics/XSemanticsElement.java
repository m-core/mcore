/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XSemantics Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XSemanticsElement#getKindLabel <em>Kind Label</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXSemanticsElement()
 * @model abstract="true"
 * @generated
 */

public interface XSemanticsElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind Label</em>' attribute.
	 * @see org.xocl.semantics.SemanticsPackage#getXSemanticsElement_KindLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'TODO\'\n'"
	 * @generated
	 */
	String getKindLabel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let e: String = indentationSpaces().concat(kindLabel.toUpper()) in \n    if e.oclIsInvalid() then null else e endif\n'"
	 * @generated
	 */
	String renderedKindLabel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(XSemanticsElement)\n  then eContainer().oclAsType(XSemanticsElement).indentLevel() + 1\n  else 0 endif endif\n'"
	 * @generated
	 */
	Integer indentLevel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\nindentationSpaces(numberOfSpaces)\n'"
	 * @generated
	 */
	String indentationSpaces();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let sizeMinusOne: Integer = let e: Integer = size - 1 in \n    if e.oclIsInvalid() then null else e endif in\nif (let e: Boolean = size < 1 in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'\'\n  else (let e: String = indentationSpaces(sizeMinusOne).concat(\' \') in \n    if e.oclIsInvalid() then null else e endif)\nendif\n'"
	 * @generated
	 */
	String indentationSpaces(Integer size);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pAnnotation="http://www.montages.com/mCore/MCore mName='p'"
	 *        annotation="http://www.xocl.org/OCL body='if p=null then \'MISSING\' else if p.trim()=\'\' then \'MISSING\' else p endif endif'"
	 * @generated
	 */
	String stringOrMissing(String p);

} // XSemanticsElement
