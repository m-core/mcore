
/**
 */
package org.xocl.semantics;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XUpdated Typed Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate <em>Contributing Update</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeAddedValue <em>Expected To Be Added Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeRemovedValue <em>Expected To Be Removed Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedResultingValue <em>Expected Resulting Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getExpectationsMet <em>Expectations Met</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getOldValue <em>Old Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getNewValue <em>New Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getInconsistent <em>Inconsistent</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getExecuted <em>Executed</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertPosition <em>Towards End Insert Position</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertValues <em>Towards End Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertPosition <em>In The Middle Insert Position</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertValues <em>In The Middle Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertPosition <em>Towards Beginning Insert Position</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertValues <em>Towards Beginning Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getAsFirstInsertValues <em>As First Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getAsLastInsertValues <em>As Last Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedTypedElement#getDeleteValues <em>Delete Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement()
 * @model abstract="true"
 * @generated
 */

public interface XUpdatedTypedElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Contributing Update</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdate}.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XUpdate#getContributesTo <em>Contributes To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contributing Update</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributing Update</em>' reference list.
	 * @see #isSetContributingUpdate()
	 * @see #unsetContributingUpdate()
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_ContributingUpdate()
	 * @see org.xocl.semantics.XUpdate#getContributesTo
	 * @model opposite="contributesTo" unsettable="true"
	 * @generated
	 */
	EList<XUpdate> getContributingUpdate();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate <em>Contributing Update</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContributingUpdate()
	 * @see #getContributingUpdate()
	 * @generated
	 */
	void unsetContributingUpdate();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate <em>Contributing Update</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Contributing Update</em>' reference list is set.
	 * @see #unsetContributingUpdate()
	 * @see #getContributingUpdate()
	 * @generated
	 */
	boolean isSetContributingUpdate();

	/**
	 * Returns the value of the '<em><b>Expected To Be Added Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected To Be Added Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected To Be Added Value</em>' containment reference.
	 * @see #isSetExpectedToBeAddedValue()
	 * @see #unsetExpectedToBeAddedValue()
	 * @see #setExpectedToBeAddedValue(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_ExpectedToBeAddedValue()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Expectation Testing' createColumn='true'"
	 * @generated
	 */
	XValue getExpectedToBeAddedValue();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeAddedValue <em>Expected To Be Added Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expected To Be Added Value</em>' containment reference.
	 * @see #isSetExpectedToBeAddedValue()
	 * @see #unsetExpectedToBeAddedValue()
	 * @see #getExpectedToBeAddedValue()
	 * @generated
	 */
	void setExpectedToBeAddedValue(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeAddedValue <em>Expected To Be Added Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpectedToBeAddedValue()
	 * @see #getExpectedToBeAddedValue()
	 * @see #setExpectedToBeAddedValue(XValue)
	 * @generated
	 */
	void unsetExpectedToBeAddedValue();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeAddedValue <em>Expected To Be Added Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expected To Be Added Value</em>' containment reference is set.
	 * @see #unsetExpectedToBeAddedValue()
	 * @see #getExpectedToBeAddedValue()
	 * @see #setExpectedToBeAddedValue(XValue)
	 * @generated
	 */
	boolean isSetExpectedToBeAddedValue();

	/**
	 * Returns the value of the '<em><b>Expected To Be Removed Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected To Be Removed Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected To Be Removed Value</em>' containment reference.
	 * @see #isSetExpectedToBeRemovedValue()
	 * @see #unsetExpectedToBeRemovedValue()
	 * @see #setExpectedToBeRemovedValue(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_ExpectedToBeRemovedValue()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Expectation Testing' createColumn='true'"
	 * @generated
	 */
	XValue getExpectedToBeRemovedValue();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeRemovedValue <em>Expected To Be Removed Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expected To Be Removed Value</em>' containment reference.
	 * @see #isSetExpectedToBeRemovedValue()
	 * @see #unsetExpectedToBeRemovedValue()
	 * @see #getExpectedToBeRemovedValue()
	 * @generated
	 */
	void setExpectedToBeRemovedValue(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeRemovedValue <em>Expected To Be Removed Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpectedToBeRemovedValue()
	 * @see #getExpectedToBeRemovedValue()
	 * @see #setExpectedToBeRemovedValue(XValue)
	 * @generated
	 */
	void unsetExpectedToBeRemovedValue();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeRemovedValue <em>Expected To Be Removed Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expected To Be Removed Value</em>' containment reference is set.
	 * @see #unsetExpectedToBeRemovedValue()
	 * @see #getExpectedToBeRemovedValue()
	 * @see #setExpectedToBeRemovedValue(XValue)
	 * @generated
	 */
	boolean isSetExpectedToBeRemovedValue();

	/**
	 * Returns the value of the '<em><b>Expected Resulting Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected Resulting Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected Resulting Value</em>' containment reference.
	 * @see #isSetExpectedResultingValue()
	 * @see #unsetExpectedResultingValue()
	 * @see #setExpectedResultingValue(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_ExpectedResultingValue()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Expectation Testing' createColumn='true'"
	 * @generated
	 */
	XValue getExpectedResultingValue();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedResultingValue <em>Expected Resulting Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expected Resulting Value</em>' containment reference.
	 * @see #isSetExpectedResultingValue()
	 * @see #unsetExpectedResultingValue()
	 * @see #getExpectedResultingValue()
	 * @generated
	 */
	void setExpectedResultingValue(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedResultingValue <em>Expected Resulting Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpectedResultingValue()
	 * @see #getExpectedResultingValue()
	 * @see #setExpectedResultingValue(XValue)
	 * @generated
	 */
	void unsetExpectedResultingValue();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedResultingValue <em>Expected Resulting Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expected Resulting Value</em>' containment reference is set.
	 * @see #unsetExpectedResultingValue()
	 * @see #getExpectedResultingValue()
	 * @see #setExpectedResultingValue(XValue)
	 * @generated
	 */
	boolean isSetExpectedResultingValue();

	/**
	 * Returns the value of the '<em><b>Expectations Met</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expectations Met</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expectations Met</em>' attribute.
	 * @see #isSetExpectationsMet()
	 * @see #unsetExpectationsMet()
	 * @see #setExpectationsMet(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_ExpectationsMet()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Expectation Testing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getExpectationsMet();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectationsMet <em>Expectations Met</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expectations Met</em>' attribute.
	 * @see #isSetExpectationsMet()
	 * @see #unsetExpectationsMet()
	 * @see #getExpectationsMet()
	 * @generated
	 */
	void setExpectationsMet(Boolean value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectationsMet <em>Expectations Met</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpectationsMet()
	 * @see #getExpectationsMet()
	 * @see #setExpectationsMet(Boolean)
	 * @generated
	 */
	void unsetExpectationsMet();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectationsMet <em>Expectations Met</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expectations Met</em>' attribute is set.
	 * @see #unsetExpectationsMet()
	 * @see #getExpectationsMet()
	 * @see #setExpectationsMet(Boolean)
	 * @generated
	 */
	boolean isSetExpectationsMet();

	/**
	 * Returns the value of the '<em><b>Old Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Old Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Value</em>' containment reference.
	 * @see #isSetOldValue()
	 * @see #unsetOldValue()
	 * @see #setOldValue(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_OldValue()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Documentation' createColumn='true'"
	 * @generated
	 */
	XValue getOldValue();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getOldValue <em>Old Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Value</em>' containment reference.
	 * @see #isSetOldValue()
	 * @see #unsetOldValue()
	 * @see #getOldValue()
	 * @generated
	 */
	void setOldValue(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getOldValue <em>Old Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOldValue()
	 * @see #getOldValue()
	 * @see #setOldValue(XValue)
	 * @generated
	 */
	void unsetOldValue();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getOldValue <em>Old Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Old Value</em>' containment reference is set.
	 * @see #unsetOldValue()
	 * @see #getOldValue()
	 * @see #setOldValue(XValue)
	 * @generated
	 */
	boolean isSetOldValue();

	/**
	 * Returns the value of the '<em><b>New Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>New Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Value</em>' containment reference.
	 * @see #isSetNewValue()
	 * @see #unsetNewValue()
	 * @see #setNewValue(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_NewValue()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Documentation' createColumn='true'"
	 * @generated
	 */
	XValue getNewValue();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getNewValue <em>New Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Value</em>' containment reference.
	 * @see #isSetNewValue()
	 * @see #unsetNewValue()
	 * @see #getNewValue()
	 * @generated
	 */
	void setNewValue(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getNewValue <em>New Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNewValue()
	 * @see #getNewValue()
	 * @see #setNewValue(XValue)
	 * @generated
	 */
	void unsetNewValue();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getNewValue <em>New Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>New Value</em>' containment reference is set.
	 * @see #unsetNewValue()
	 * @see #getNewValue()
	 * @see #setNewValue(XValue)
	 * @generated
	 */
	boolean isSetNewValue();

	/**
	 * Returns the value of the '<em><b>Inconsistent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inconsistent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inconsistent</em>' attribute.
	 * @see #isSetInconsistent()
	 * @see #unsetInconsistent()
	 * @see #setInconsistent(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_Inconsistent()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='false'"
	 * @generated
	 */
	Boolean getInconsistent();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInconsistent <em>Inconsistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inconsistent</em>' attribute.
	 * @see #isSetInconsistent()
	 * @see #unsetInconsistent()
	 * @see #getInconsistent()
	 * @generated
	 */
	void setInconsistent(Boolean value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInconsistent <em>Inconsistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInconsistent()
	 * @see #getInconsistent()
	 * @see #setInconsistent(Boolean)
	 * @generated
	 */
	void unsetInconsistent();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInconsistent <em>Inconsistent</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Inconsistent</em>' attribute is set.
	 * @see #unsetInconsistent()
	 * @see #getInconsistent()
	 * @see #setInconsistent(Boolean)
	 * @generated
	 */
	boolean isSetInconsistent();

	/**
	 * Returns the value of the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executed</em>' attribute.
	 * @see #isSetExecuted()
	 * @see #unsetExecuted()
	 * @see #setExecuted(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_Executed()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Transition Execution'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getExecuted();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExecuted <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Executed</em>' attribute.
	 * @see #isSetExecuted()
	 * @see #unsetExecuted()
	 * @see #getExecuted()
	 * @generated
	 */
	void setExecuted(Boolean value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExecuted <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecuted()
	 * @see #getExecuted()
	 * @see #setExecuted(Boolean)
	 * @generated
	 */
	void unsetExecuted();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getExecuted <em>Executed</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Executed</em>' attribute is set.
	 * @see #unsetExecuted()
	 * @see #getExecuted()
	 * @see #setExecuted(Boolean)
	 * @generated
	 */
	boolean isSetExecuted();

	/**
	 * Returns the value of the '<em><b>Towards End Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Towards End Insert Position</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Towards End Insert Position</em>' attribute.
	 * @see #isSetTowardsEndInsertPosition()
	 * @see #unsetTowardsEndInsertPosition()
	 * @see #setTowardsEndInsertPosition(Integer)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_TowardsEndInsertPosition()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='false'"
	 * @generated
	 */
	Integer getTowardsEndInsertPosition();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertPosition <em>Towards End Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Towards End Insert Position</em>' attribute.
	 * @see #isSetTowardsEndInsertPosition()
	 * @see #unsetTowardsEndInsertPosition()
	 * @see #getTowardsEndInsertPosition()
	 * @generated
	 */
	void setTowardsEndInsertPosition(Integer value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertPosition <em>Towards End Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTowardsEndInsertPosition()
	 * @see #getTowardsEndInsertPosition()
	 * @see #setTowardsEndInsertPosition(Integer)
	 * @generated
	 */
	void unsetTowardsEndInsertPosition();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertPosition <em>Towards End Insert Position</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Towards End Insert Position</em>' attribute is set.
	 * @see #unsetTowardsEndInsertPosition()
	 * @see #getTowardsEndInsertPosition()
	 * @see #setTowardsEndInsertPosition(Integer)
	 * @generated
	 */
	boolean isSetTowardsEndInsertPosition();

	/**
	 * Returns the value of the '<em><b>Towards End Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Towards End Insert Values</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Towards End Insert Values</em>' containment reference.
	 * @see #isSetTowardsEndInsertValues()
	 * @see #unsetTowardsEndInsertValues()
	 * @see #setTowardsEndInsertValues(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_TowardsEndInsertValues()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='true'"
	 * @generated
	 */
	XValue getTowardsEndInsertValues();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertValues <em>Towards End Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Towards End Insert Values</em>' containment reference.
	 * @see #isSetTowardsEndInsertValues()
	 * @see #unsetTowardsEndInsertValues()
	 * @see #getTowardsEndInsertValues()
	 * @generated
	 */
	void setTowardsEndInsertValues(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertValues <em>Towards End Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTowardsEndInsertValues()
	 * @see #getTowardsEndInsertValues()
	 * @see #setTowardsEndInsertValues(XValue)
	 * @generated
	 */
	void unsetTowardsEndInsertValues();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertValues <em>Towards End Insert Values</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Towards End Insert Values</em>' containment reference is set.
	 * @see #unsetTowardsEndInsertValues()
	 * @see #getTowardsEndInsertValues()
	 * @see #setTowardsEndInsertValues(XValue)
	 * @generated
	 */
	boolean isSetTowardsEndInsertValues();

	/**
	 * Returns the value of the '<em><b>In The Middle Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In The Middle Insert Position</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In The Middle Insert Position</em>' attribute.
	 * @see #isSetInTheMiddleInsertPosition()
	 * @see #unsetInTheMiddleInsertPosition()
	 * @see #setInTheMiddleInsertPosition(Integer)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_InTheMiddleInsertPosition()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='false'"
	 * @generated
	 */
	Integer getInTheMiddleInsertPosition();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertPosition <em>In The Middle Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In The Middle Insert Position</em>' attribute.
	 * @see #isSetInTheMiddleInsertPosition()
	 * @see #unsetInTheMiddleInsertPosition()
	 * @see #getInTheMiddleInsertPosition()
	 * @generated
	 */
	void setInTheMiddleInsertPosition(Integer value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertPosition <em>In The Middle Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInTheMiddleInsertPosition()
	 * @see #getInTheMiddleInsertPosition()
	 * @see #setInTheMiddleInsertPosition(Integer)
	 * @generated
	 */
	void unsetInTheMiddleInsertPosition();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertPosition <em>In The Middle Insert Position</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In The Middle Insert Position</em>' attribute is set.
	 * @see #unsetInTheMiddleInsertPosition()
	 * @see #getInTheMiddleInsertPosition()
	 * @see #setInTheMiddleInsertPosition(Integer)
	 * @generated
	 */
	boolean isSetInTheMiddleInsertPosition();

	/**
	 * Returns the value of the '<em><b>In The Middle Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In The Middle Insert Values</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In The Middle Insert Values</em>' containment reference.
	 * @see #isSetInTheMiddleInsertValues()
	 * @see #unsetInTheMiddleInsertValues()
	 * @see #setInTheMiddleInsertValues(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_InTheMiddleInsertValues()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='true'"
	 * @generated
	 */
	XValue getInTheMiddleInsertValues();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertValues <em>In The Middle Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In The Middle Insert Values</em>' containment reference.
	 * @see #isSetInTheMiddleInsertValues()
	 * @see #unsetInTheMiddleInsertValues()
	 * @see #getInTheMiddleInsertValues()
	 * @generated
	 */
	void setInTheMiddleInsertValues(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertValues <em>In The Middle Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInTheMiddleInsertValues()
	 * @see #getInTheMiddleInsertValues()
	 * @see #setInTheMiddleInsertValues(XValue)
	 * @generated
	 */
	void unsetInTheMiddleInsertValues();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertValues <em>In The Middle Insert Values</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In The Middle Insert Values</em>' containment reference is set.
	 * @see #unsetInTheMiddleInsertValues()
	 * @see #getInTheMiddleInsertValues()
	 * @see #setInTheMiddleInsertValues(XValue)
	 * @generated
	 */
	boolean isSetInTheMiddleInsertValues();

	/**
	 * Returns the value of the '<em><b>Towards Beginning Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Towards Beginning Insert Position</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Towards Beginning Insert Position</em>' attribute.
	 * @see #isSetTowardsBeginningInsertPosition()
	 * @see #unsetTowardsBeginningInsertPosition()
	 * @see #setTowardsBeginningInsertPosition(Integer)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_TowardsBeginningInsertPosition()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='false'"
	 * @generated
	 */
	Integer getTowardsBeginningInsertPosition();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertPosition <em>Towards Beginning Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Towards Beginning Insert Position</em>' attribute.
	 * @see #isSetTowardsBeginningInsertPosition()
	 * @see #unsetTowardsBeginningInsertPosition()
	 * @see #getTowardsBeginningInsertPosition()
	 * @generated
	 */
	void setTowardsBeginningInsertPosition(Integer value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertPosition <em>Towards Beginning Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTowardsBeginningInsertPosition()
	 * @see #getTowardsBeginningInsertPosition()
	 * @see #setTowardsBeginningInsertPosition(Integer)
	 * @generated
	 */
	void unsetTowardsBeginningInsertPosition();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertPosition <em>Towards Beginning Insert Position</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Towards Beginning Insert Position</em>' attribute is set.
	 * @see #unsetTowardsBeginningInsertPosition()
	 * @see #getTowardsBeginningInsertPosition()
	 * @see #setTowardsBeginningInsertPosition(Integer)
	 * @generated
	 */
	boolean isSetTowardsBeginningInsertPosition();

	/**
	 * Returns the value of the '<em><b>Towards Beginning Insert Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Towards Beginning Insert Values</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Towards Beginning Insert Values</em>' reference.
	 * @see #isSetTowardsBeginningInsertValues()
	 * @see #unsetTowardsBeginningInsertValues()
	 * @see #setTowardsBeginningInsertValues(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_TowardsBeginningInsertValues()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='false'"
	 * @generated
	 */
	XValue getTowardsBeginningInsertValues();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertValues <em>Towards Beginning Insert Values</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Towards Beginning Insert Values</em>' reference.
	 * @see #isSetTowardsBeginningInsertValues()
	 * @see #unsetTowardsBeginningInsertValues()
	 * @see #getTowardsBeginningInsertValues()
	 * @generated
	 */
	void setTowardsBeginningInsertValues(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertValues <em>Towards Beginning Insert Values</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTowardsBeginningInsertValues()
	 * @see #getTowardsBeginningInsertValues()
	 * @see #setTowardsBeginningInsertValues(XValue)
	 * @generated
	 */
	void unsetTowardsBeginningInsertValues();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertValues <em>Towards Beginning Insert Values</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Towards Beginning Insert Values</em>' reference is set.
	 * @see #unsetTowardsBeginningInsertValues()
	 * @see #getTowardsBeginningInsertValues()
	 * @see #setTowardsBeginningInsertValues(XValue)
	 * @generated
	 */
	boolean isSetTowardsBeginningInsertValues();

	/**
	 * Returns the value of the '<em><b>As First Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As First Insert Values</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As First Insert Values</em>' containment reference.
	 * @see #isSetAsFirstInsertValues()
	 * @see #unsetAsFirstInsertValues()
	 * @see #setAsFirstInsertValues(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_AsFirstInsertValues()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='true'"
	 * @generated
	 */
	XValue getAsFirstInsertValues();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getAsFirstInsertValues <em>As First Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As First Insert Values</em>' containment reference.
	 * @see #isSetAsFirstInsertValues()
	 * @see #unsetAsFirstInsertValues()
	 * @see #getAsFirstInsertValues()
	 * @generated
	 */
	void setAsFirstInsertValues(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getAsFirstInsertValues <em>As First Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAsFirstInsertValues()
	 * @see #getAsFirstInsertValues()
	 * @see #setAsFirstInsertValues(XValue)
	 * @generated
	 */
	void unsetAsFirstInsertValues();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getAsFirstInsertValues <em>As First Insert Values</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>As First Insert Values</em>' containment reference is set.
	 * @see #unsetAsFirstInsertValues()
	 * @see #getAsFirstInsertValues()
	 * @see #setAsFirstInsertValues(XValue)
	 * @generated
	 */
	boolean isSetAsFirstInsertValues();

	/**
	 * Returns the value of the '<em><b>As Last Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Last Insert Values</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Last Insert Values</em>' containment reference.
	 * @see #isSetAsLastInsertValues()
	 * @see #unsetAsLastInsertValues()
	 * @see #setAsLastInsertValues(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_AsLastInsertValues()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='true'"
	 * @generated
	 */
	XValue getAsLastInsertValues();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getAsLastInsertValues <em>As Last Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Last Insert Values</em>' containment reference.
	 * @see #isSetAsLastInsertValues()
	 * @see #unsetAsLastInsertValues()
	 * @see #getAsLastInsertValues()
	 * @generated
	 */
	void setAsLastInsertValues(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getAsLastInsertValues <em>As Last Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAsLastInsertValues()
	 * @see #getAsLastInsertValues()
	 * @see #setAsLastInsertValues(XValue)
	 * @generated
	 */
	void unsetAsLastInsertValues();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getAsLastInsertValues <em>As Last Insert Values</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>As Last Insert Values</em>' containment reference is set.
	 * @see #unsetAsLastInsertValues()
	 * @see #getAsLastInsertValues()
	 * @see #setAsLastInsertValues(XValue)
	 * @generated
	 */
	boolean isSetAsLastInsertValues();

	/**
	 * Returns the value of the '<em><b>Delete Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delete Values</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delete Values</em>' containment reference.
	 * @see #isSetDeleteValues()
	 * @see #unsetDeleteValues()
	 * @see #setDeleteValues(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedTypedElement_DeleteValues()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Transition Execution' createColumn='true'"
	 * @generated
	 */
	XValue getDeleteValues();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getDeleteValues <em>Delete Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delete Values</em>' containment reference.
	 * @see #isSetDeleteValues()
	 * @see #unsetDeleteValues()
	 * @see #getDeleteValues()
	 * @generated
	 */
	void setDeleteValues(XValue value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getDeleteValues <em>Delete Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDeleteValues()
	 * @see #getDeleteValues()
	 * @see #setDeleteValues(XValue)
	 * @generated
	 */
	void unsetDeleteValues();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedTypedElement#getDeleteValues <em>Delete Values</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Delete Values</em>' containment reference is set.
	 * @see #unsetDeleteValues()
	 * @see #getDeleteValues()
	 * @see #setDeleteValues(XValue)
	 * @generated
	 */
	boolean isSetDeleteValues();

} // XUpdatedTypedElement
