/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XUpdated Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XUpdatedFeature#getFeature <em>Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedFeature()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Feature\'\n'"
 * @generated
 */

public interface XUpdatedFeature extends XSemanticsElement, XUpdatedTypedElement {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #setFeature(EStructuralFeature)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedFeature_Feature()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	EStructuralFeature getFeature();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedFeature#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #isSetFeature()
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(EStructuralFeature value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedFeature#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFeature()
	 * @see #getFeature()
	 * @see #setFeature(EStructuralFeature)
	 * @generated
	 */
	void unsetFeature();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedFeature#getFeature <em>Feature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Feature</em>' reference is set.
	 * @see #unsetFeature()
	 * @see #getFeature()
	 * @see #setFeature(EStructuralFeature)
	 * @generated
	 */
	boolean isSetFeature();

} // XUpdatedFeature
