/**
 */
package org.xocl.semantics;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XSemantics</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XSemantics#getTransition <em>Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXSemantics()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Semantics\'\n'"
 * @generated
 */

public interface XSemantics extends XSemanticsElement {
	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XTransition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference list.
	 * @see #isSetTransition()
	 * @see #unsetTransition()
	 * @see org.xocl.semantics.SemanticsPackage#getXSemantics_Transition()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<XTransition> getTransition();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XSemantics#getTransition <em>Transition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTransition()
	 * @see #getTransition()
	 * @generated
	 */
	void unsetTransition();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XSemantics#getTransition <em>Transition</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Transition</em>' containment reference list is set.
	 * @see #unsetTransition()
	 * @see #getTransition()
	 * @generated
	 */
	boolean isSetTransition();

} // XSemantics
