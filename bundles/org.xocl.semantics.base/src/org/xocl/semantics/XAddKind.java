/**
 */
package org.xocl.semantics;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>XAdd Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.xocl.semantics.SemanticsPackage#getXAddKind()
 * @model
 * @generated
 */
public enum XAddKind implements Enumerator {
	/**
	 * The '<em><b>First</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIRST_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST(0, "First", "First"),

	/**
	 * The '<em><b>Towards Beginning</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TOWARDS_BEGINNING_VALUE
	 * @generated
	 * @ordered
	 */
	TOWARDS_BEGINNING(1, "TowardsBeginning", "Towards Beginning"),

	/**
	 * The '<em><b>In The Middle</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IN_THE_MIDDLE_VALUE
	 * @generated
	 * @ordered
	 */
	IN_THE_MIDDLE(2, "InTheMiddle", "In The Middle"), /**
														 * The '<em><b>Towards End</b></em>' literal object.
														 * <!-- begin-user-doc -->
														 * <!-- end-user-doc -->
														 * @see #TOWARDS_END_VALUE
														 * @generated
														 * @ordered
														 */
	TOWARDS_END(3, "TowardsEnd", "Towards End"),

	/**
	 * The '<em><b>Last</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LAST_VALUE
	 * @generated
	 * @ordered
	 */
	LAST(4, "Last", "Last");

	/**
	 * The '<em><b>First</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>First</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST
	 * @model name="First"
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_VALUE = 0;

	/**
	 * The '<em><b>Towards Beginning</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Towards Beginning</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOWARDS_BEGINNING
	 * @model name="TowardsBeginning" literal="Towards Beginning"
	 * @generated
	 * @ordered
	 */
	public static final int TOWARDS_BEGINNING_VALUE = 1;

	/**
	 * The '<em><b>In The Middle</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>In The Middle</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IN_THE_MIDDLE
	 * @model name="InTheMiddle" literal="In The Middle"
	 * @generated
	 * @ordered
	 */
	public static final int IN_THE_MIDDLE_VALUE = 2;

	/**
	 * The '<em><b>Towards End</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Towards End</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOWARDS_END
	 * @model name="TowardsEnd" literal="Towards End"
	 * @generated
	 * @ordered
	 */
	public static final int TOWARDS_END_VALUE = 3;

	/**
	 * The '<em><b>Last</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Last</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LAST
	 * @model name="Last"
	 * @generated
	 * @ordered
	 */
	public static final int LAST_VALUE = 4;

	/**
	 * An array of all the '<em><b>XAdd Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final XAddKind[] VALUES_ARRAY = new XAddKind[] { FIRST,
			TOWARDS_BEGINNING, IN_THE_MIDDLE, TOWARDS_END, LAST, };

	/**
	 * A public read-only list of all the '<em><b>XAdd Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<XAddKind> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>XAdd Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static XAddKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			XAddKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>XAdd Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static XAddKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			XAddKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>XAdd Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static XAddKind get(int value) {
		switch (value) {
		case FIRST_VALUE:
			return FIRST;
		case TOWARDS_BEGINNING_VALUE:
			return TOWARDS_BEGINNING;
		case IN_THE_MIDDLE_VALUE:
			return IN_THE_MIDDLE;
		case TOWARDS_END_VALUE:
			return TOWARDS_END;
		case LAST_VALUE:
			return LAST;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private XAddKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //XAddKind
