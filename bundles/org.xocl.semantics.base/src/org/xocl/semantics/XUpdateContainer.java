/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XAbstract Update Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XUpdateContainer#getContainingTransition <em>Containing Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXUpdateContainer()
 * @model abstract="true"
 * @generated
 */

public interface XUpdateContainer extends XSemanticsElement {
	/**
	 * Returns the value of the '<em><b>Containing Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Transition</em>' reference.
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdateContainer_ContainingTransition()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.oclIsKindOf(XTransition)\n   then self.oclAsType(XTransition) \n   else self.eContainer().oclAsType(XUpdateContainer).containingTransition\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	XTransition getContainingTransition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate addReferenceUpdate(EObject updatedObject, EReference updatedReference, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object targetObject, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate addReferenceUpdate(EObject updatedObject, EReference updatedReference, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object targetObject, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint, String persistenceLocation, String alternativePersistencePackage,
			String alternativePersistenceRoot, String alternativePersistenceReference);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate addAndMergeUpdate(XUpdate updateToBeMerged);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='AddAttributeUpdate'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated NOT
	 */
	XUpdate addAttributeUpdate(EObject updatedObject, EAttribute updatedAttribute, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object value, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='AddAttributeUpdate'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate addAttributeUpdate(EObject updatedObject, EAttribute updatedAttribute, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object value, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint, String persistenceLocation, String alternativePersistencePackage,
			String alternativePersistenceRoot, String alternativePersistenceReference);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model operationAnnotation="http://www.montages.com/mCore/MCore mName='operation'" parametersAnnotation="http://www.montages.com/mCore/MCore mName='parameters'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	XUpdate invokeOperationCall(EObject updatedObject, EOperation operation, Object parameters,
			XUpdatedObject afterObjectConstraint, XUpdatedObject beforeObjectConstraint);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	EObject createNextStateNewObject(EClass typeOfNewObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdatedObject nextStateObjectDefinitionFromObject(EObject currentStateObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	Boolean deleteObjectInNextState(EObject objectToBeDeleted);

} // XUpdateContainer
