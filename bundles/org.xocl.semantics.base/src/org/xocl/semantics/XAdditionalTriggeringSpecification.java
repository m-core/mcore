/**
 */
package org.xocl.semantics;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XAdditional Triggering Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXAdditionalTriggeringSpecification()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL triggeringUpdateDerive='additionalTriggeringUpdate\n' kindLabelDerive='\'Additional Triggering\'\n'"
 * @generated
 */

public interface XAdditionalTriggeringSpecification extends XTriggeringSpecification {

	/**
	 * Returns the value of the '<em><b>Additional Triggering Update</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis <em>Additional Triggering By This</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Triggering Update</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Triggering Update</em>' reference.
	 * @see #isSetAdditionalTriggeringUpdate()
	 * @see #unsetAdditionalTriggeringUpdate()
	 * @see #setAdditionalTriggeringUpdate(XTriggeredUpdate)
	 * @see org.xocl.semantics.SemanticsPackage#getXAdditionalTriggeringSpecification_AdditionalTriggeringUpdate()
	 * @see org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis
	 * @model opposite="additionalTriggeringByThis" unsettable="true"
	 * @generated
	 */
	XTriggeredUpdate getAdditionalTriggeringUpdate();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Additional Triggering Update</em>' reference.
	 * @see #isSetAdditionalTriggeringUpdate()
	 * @see #unsetAdditionalTriggeringUpdate()
	 * @see #getAdditionalTriggeringUpdate()
	 * @generated
	 */
	void setAdditionalTriggeringUpdate(XTriggeredUpdate value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAdditionalTriggeringUpdate()
	 * @see #getAdditionalTriggeringUpdate()
	 * @see #setAdditionalTriggeringUpdate(XTriggeredUpdate)
	 * @generated
	 */
	void unsetAdditionalTriggeringUpdate();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Additional Triggering Update</em>' reference is set.
	 * @see #unsetAdditionalTriggeringUpdate()
	 * @see #getAdditionalTriggeringUpdate()
	 * @see #setAdditionalTriggeringUpdate(XTriggeredUpdate)
	 * @generated
	 */
	boolean isSetAdditionalTriggeringUpdate();
} // XAdditionalTriggeringSpecification
