package org.xocl.semantics.commands;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.ChangeCommand;
import org.xocl.semantics.XTransition;

public class CompoundCommandFocus extends CompoundCommand {

	private Object toBeFocused;
	private List<?> toBeFocusedList;
	boolean executedAndUndoAvailable = false;
	private ChangeCommand changeCommand = null;

	public CompoundCommandFocus(List<Command> commandList,
			Object objectsToBeFocused) {
		super(CompoundCommand.MERGE_COMMAND_ALL, XTransition.FOCUSCOMMAND);

		for (Command com : commandList)
			this.append(com);

		if (objectsToBeFocused instanceof List<?>)
			this.toBeFocusedList = (List<?>) objectsToBeFocused;
		else
			toBeFocused = objectsToBeFocused;
	}

	public CompoundCommandFocus(List<Command> commandList, ChangeCommand cmd) {
		super(CompoundCommand.MERGE_COMMAND_ALL, XTransition.FOCUSCOMMAND);

		for (Command com : commandList)
			this.append(com);

		this.changeCommand = cmd;
	}

	@Override
	public void execute() {
		super.execute();
		executedAndUndoAvailable = true;
	};

	@Override
	public void undo() {
		super.undo();
		executedAndUndoAvailable = false;
	};

	@Override
	public void redo() {
		super.redo();
		executedAndUndoAvailable = true;
	};

	@Override
	public Collection<?> getAffectedObjects() {
		// TODO Auto-generated method stub
		if (changeCommand != null)
			return changeCommand.getAffectedObjects();

		if (executedAndUndoAvailable)
			return toBeFocused == null ? toBeFocusedList
					: Collections.singleton(toBeFocused);

		return Collections
				.singleton(super.getAffectedObjects().iterator().next());
	}
}
