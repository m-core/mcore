package org.xocl.semantics.commands;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.core.util.OptimizedUsageCrossReferencer;

/**
 * @see bug #666
 */
public class DeleteCommandWithOptimizedCrossReferencer extends DeleteCommand {
	public DeleteCommandWithOptimizedCrossReferencer(EditingDomain domain, Collection<?> collection) {
		super(domain, collection);
	}

	@Override
	protected Map<EObject, Collection<Setting>> findReferences(Collection<EObject> eObjects) {
		return new OptimizedUsageCrossReferencer(domain.getResourceSet()).findAllUsage(eObjects);
	}
}