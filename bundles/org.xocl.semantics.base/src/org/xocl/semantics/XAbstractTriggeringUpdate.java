/**
 */
package org.xocl.semantics;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XAbstract Triggering Update</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate <em>Triggered Owned Update</em>}</li>
 *   <li>{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionalTriggeringOfThis <em>Additional Triggering Of This</em>}</li>
 *   <li>{@link org.xocl.semantics.XAbstractTriggeringUpdate#getProcesssed <em>Processsed</em>}</li>
 *   <li>{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionallyTriggeringUpdate <em>Additionally Triggering Update</em>}</li>
 *   <li>{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAllOwnedUpdates <em>All Owned Updates</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXAbstractTriggeringUpdate()
 * @model abstract="true"
 * @generated
 */

public interface XAbstractTriggeringUpdate extends XUpdate {
	/**
	 * Returns the value of the '<em><b>Triggered Owned Update</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XTriggeredUpdate}.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XTriggeredUpdate#getOriginallyTriggeringUpdate <em>Originally Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggered Owned Update</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggered Owned Update</em>' containment reference list.
	 * @see #isSetTriggeredOwnedUpdate()
	 * @see #unsetTriggeredOwnedUpdate()
	 * @see org.xocl.semantics.SemanticsPackage#getXAbstractTriggeringUpdate_TriggeredOwnedUpdate()
	 * @see org.xocl.semantics.XTriggeredUpdate#getOriginallyTriggeringUpdate
	 * @model opposite="originallyTriggeringUpdate" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<XTriggeredUpdate> getTriggeredOwnedUpdate();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate <em>Triggered Owned Update</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTriggeredOwnedUpdate()
	 * @see #getTriggeredOwnedUpdate()
	 * @generated
	 */
	void unsetTriggeredOwnedUpdate();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate <em>Triggered Owned Update</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Triggered Owned Update</em>' containment reference list is set.
	 * @see #unsetTriggeredOwnedUpdate()
	 * @see #getTriggeredOwnedUpdate()
	 * @generated
	 */
	boolean isSetTriggeredOwnedUpdate();

	/**
	 * Returns the value of the '<em><b>Additional Triggering Of This</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XAdditionalTriggeringSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Triggering Of This</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Triggering Of This</em>' containment reference list.
	 * @see #isSetAdditionalTriggeringOfThis()
	 * @see #unsetAdditionalTriggeringOfThis()
	 * @see org.xocl.semantics.SemanticsPackage#getXAbstractTriggeringUpdate_AdditionalTriggeringOfThis()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<XAdditionalTriggeringSpecification> getAdditionalTriggeringOfThis();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionalTriggeringOfThis <em>Additional Triggering Of This</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAdditionalTriggeringOfThis()
	 * @see #getAdditionalTriggeringOfThis()
	 * @generated
	 */
	void unsetAdditionalTriggeringOfThis();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionalTriggeringOfThis <em>Additional Triggering Of This</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Additional Triggering Of This</em>' containment reference list is set.
	 * @see #unsetAdditionalTriggeringOfThis()
	 * @see #getAdditionalTriggeringOfThis()
	 * @generated
	 */
	boolean isSetAdditionalTriggeringOfThis();

	/**
	 * Returns the value of the '<em><b>Processsed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processsed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processsed</em>' attribute.
	 * @see #isSetProcesssed()
	 * @see #unsetProcesssed()
	 * @see #setProcesssed(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXAbstractTriggeringUpdate_Processsed()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getProcesssed();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getProcesssed <em>Processsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processsed</em>' attribute.
	 * @see #isSetProcesssed()
	 * @see #unsetProcesssed()
	 * @see #getProcesssed()
	 * @generated
	 */
	void setProcesssed(Boolean value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getProcesssed <em>Processsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProcesssed()
	 * @see #getProcesssed()
	 * @see #setProcesssed(Boolean)
	 * @generated
	 */
	void unsetProcesssed();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getProcesssed <em>Processsed</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Processsed</em>' attribute is set.
	 * @see #unsetProcesssed()
	 * @see #getProcesssed()
	 * @see #setProcesssed(Boolean)
	 * @generated
	 */
	boolean isSetProcesssed();

	/**
	 * Returns the value of the '<em><b>Additionally Triggering Update</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XTriggeredUpdate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additionally Triggering Update</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additionally Triggering Update</em>' reference list.
	 * @see org.xocl.semantics.SemanticsPackage#getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='additionalTriggeringOfThis->asOrderedSet()->collect(it: semantics::XAdditionalTriggeringSpecification | it.additionalTriggeringUpdate)->asOrderedSet()->excluding(null)->asOrderedSet() \n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<XTriggeredUpdate> getAdditionallyTriggeringUpdate();

	/**
	 * Returns the value of the '<em><b>All Owned Updates</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Owned Updates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Owned Updates</em>' reference list.
	 * @see org.xocl.semantics.SemanticsPackage#getXAbstractTriggeringUpdate_AllOwnedUpdates()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='triggeredOwnedUpdate.oclAsType(XUpdate)->union(triggeredOwnedUpdate.allOwnedUpdates)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<XUpdate> getAllOwnedUpdates();

} // XAbstractTriggeringUpdate
