/**
 */
package org.xocl.semantics.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XAdditionalTriggeringSpecification;
import org.xocl.semantics.XOriginalTriggeringSpecification;
import org.xocl.semantics.XTriggeredUpdate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XTriggered Update</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XTriggeredUpdateImpl#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTriggeredUpdateImpl#getOriginallyTriggeringUpdate <em>Originally Triggering Update</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTriggeredUpdateImpl#getAdditionalTriggeringByThis <em>Additional Triggering By This</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XTriggeredUpdateImpl extends XAbstractTriggeringUpdateImpl implements XTriggeredUpdate {
	/**
	 * The cached value of the '{@link #getOriginalTriggeringOfThis() <em>Original Triggering Of This</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalTriggeringOfThis()
	 * @generated
	 * @ordered
	 */
	protected XOriginalTriggeringSpecification originalTriggeringOfThis;

	/**
	 * This is true if the Original Triggering Of This containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean originalTriggeringOfThisESet;

	/**
	 * The cached value of the '{@link #getAdditionalTriggeringByThis() <em>Additional Triggering By This</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalTriggeringByThis()
	 * @generated
	 * @ordered
	 */
	protected EList<XAdditionalTriggeringSpecification> additionalTriggeringByThis;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XTriggeredUpdateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XTRIGGERED_UPDATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XOriginalTriggeringSpecification getOriginalTriggeringOfThis() {
		if (originalTriggeringOfThis != null && originalTriggeringOfThis.eIsProxy()) {
			InternalEObject oldOriginalTriggeringOfThis = (InternalEObject) originalTriggeringOfThis;
			originalTriggeringOfThis = (XOriginalTriggeringSpecification) eResolveProxy(oldOriginalTriggeringOfThis);
			if (originalTriggeringOfThis != oldOriginalTriggeringOfThis) {
				InternalEObject newOriginalTriggeringOfThis = (InternalEObject) originalTriggeringOfThis;
				NotificationChain msgs = oldOriginalTriggeringOfThis.eInverseRemove(this,
						SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE,
						XOriginalTriggeringSpecification.class, null);
				if (newOriginalTriggeringOfThis.eInternalContainer() == null) {
					msgs = newOriginalTriggeringOfThis.eInverseAdd(this,
							SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE,
							XOriginalTriggeringSpecification.class, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS,
							oldOriginalTriggeringOfThis, originalTriggeringOfThis));
			}
		}
		return originalTriggeringOfThis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XOriginalTriggeringSpecification basicGetOriginalTriggeringOfThis() {
		return originalTriggeringOfThis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOriginalTriggeringOfThis(
			XOriginalTriggeringSpecification newOriginalTriggeringOfThis, NotificationChain msgs) {
		XOriginalTriggeringSpecification oldOriginalTriggeringOfThis = originalTriggeringOfThis;
		originalTriggeringOfThis = newOriginalTriggeringOfThis;
		boolean oldOriginalTriggeringOfThisESet = originalTriggeringOfThisESet;
		originalTriggeringOfThisESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, oldOriginalTriggeringOfThis,
					newOriginalTriggeringOfThis, !oldOriginalTriggeringOfThisESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalTriggeringOfThis(XOriginalTriggeringSpecification newOriginalTriggeringOfThis) {
		if (newOriginalTriggeringOfThis != originalTriggeringOfThis) {
			NotificationChain msgs = null;
			if (originalTriggeringOfThis != null)
				msgs = ((InternalEObject) originalTriggeringOfThis).eInverseRemove(this,
						SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE,
						XOriginalTriggeringSpecification.class, msgs);
			if (newOriginalTriggeringOfThis != null)
				msgs = ((InternalEObject) newOriginalTriggeringOfThis).eInverseAdd(this,
						SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE,
						XOriginalTriggeringSpecification.class, msgs);
			msgs = basicSetOriginalTriggeringOfThis(newOriginalTriggeringOfThis, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldOriginalTriggeringOfThisESet = originalTriggeringOfThisESet;
			originalTriggeringOfThisESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, newOriginalTriggeringOfThis,
						newOriginalTriggeringOfThis, !oldOriginalTriggeringOfThisESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetOriginalTriggeringOfThis(NotificationChain msgs) {
		XOriginalTriggeringSpecification oldOriginalTriggeringOfThis = originalTriggeringOfThis;
		originalTriggeringOfThis = null;
		boolean oldOriginalTriggeringOfThisESet = originalTriggeringOfThisESet;
		originalTriggeringOfThisESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, oldOriginalTriggeringOfThis, null,
					oldOriginalTriggeringOfThisESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOriginalTriggeringOfThis() {
		if (originalTriggeringOfThis != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) originalTriggeringOfThis).eInverseRemove(this,
					SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE,
					XOriginalTriggeringSpecification.class, msgs);
			msgs = basicUnsetOriginalTriggeringOfThis(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldOriginalTriggeringOfThisESet = originalTriggeringOfThisESet;
			originalTriggeringOfThisESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, null, null,
						oldOriginalTriggeringOfThisESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOriginalTriggeringOfThis() {
		return originalTriggeringOfThisESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAbstractTriggeringUpdate getOriginallyTriggeringUpdate() {
		if (eContainerFeatureID() != SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE)
			return null;
		return (XAbstractTriggeringUpdate) eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAbstractTriggeringUpdate basicGetOriginallyTriggeringUpdate() {
		if (eContainerFeatureID() != SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE)
			return null;
		return (XAbstractTriggeringUpdate) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOriginallyTriggeringUpdate(XAbstractTriggeringUpdate newOriginallyTriggeringUpdate,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newOriginallyTriggeringUpdate,
				SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginallyTriggeringUpdate(XAbstractTriggeringUpdate newOriginallyTriggeringUpdate) {
		if (newOriginallyTriggeringUpdate != eInternalContainer()
				|| (eContainerFeatureID() != SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE
						&& newOriginallyTriggeringUpdate != null)) {
			if (EcoreUtil.isAncestor(this, newOriginallyTriggeringUpdate))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOriginallyTriggeringUpdate != null)
				msgs = ((InternalEObject) newOriginallyTriggeringUpdate).eInverseAdd(this,
						SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE,
						XAbstractTriggeringUpdate.class, msgs);
			msgs = basicSetOriginallyTriggeringUpdate(newOriginallyTriggeringUpdate, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE, newOriginallyTriggeringUpdate,
					newOriginallyTriggeringUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XAdditionalTriggeringSpecification> getAdditionalTriggeringByThis() {
		if (additionalTriggeringByThis == null) {
			additionalTriggeringByThis = new EObjectWithInverseResolvingEList.Unsettable<XAdditionalTriggeringSpecification>(
					XAdditionalTriggeringSpecification.class, this,
					SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS,
					SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE);
		}
		return additionalTriggeringByThis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAdditionalTriggeringByThis() {
		if (additionalTriggeringByThis != null)
			((InternalEList.Unsettable<?>) additionalTriggeringByThis).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAdditionalTriggeringByThis() {
		return additionalTriggeringByThis != null && ((InternalEList.Unsettable<?>) additionalTriggeringByThis).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS:
			if (originalTriggeringOfThis != null)
				msgs = ((InternalEObject) originalTriggeringOfThis).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, null,
						msgs);
			return basicSetOriginalTriggeringOfThis((XOriginalTriggeringSpecification) otherEnd, msgs);
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetOriginallyTriggeringUpdate((XAbstractTriggeringUpdate) otherEnd, msgs);
		case SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAdditionalTriggeringByThis())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS:
			return basicUnsetOriginalTriggeringOfThis(msgs);
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			return basicSetOriginallyTriggeringUpdate(null, msgs);
		case SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS:
			return ((InternalEList<?>) getAdditionalTriggeringByThis()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			return eInternalContainer().eInverseRemove(this,
					SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE,
					XAbstractTriggeringUpdate.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS:
			if (resolve)
				return getOriginalTriggeringOfThis();
			return basicGetOriginalTriggeringOfThis();
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			if (resolve)
				return getOriginallyTriggeringUpdate();
			return basicGetOriginallyTriggeringUpdate();
		case SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS:
			return getAdditionalTriggeringByThis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS:
			setOriginalTriggeringOfThis((XOriginalTriggeringSpecification) newValue);
			return;
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			setOriginallyTriggeringUpdate((XAbstractTriggeringUpdate) newValue);
			return;
		case SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS:
			getAdditionalTriggeringByThis().clear();
			getAdditionalTriggeringByThis().addAll((Collection<? extends XAdditionalTriggeringSpecification>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS:
			unsetOriginalTriggeringOfThis();
			return;
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			setOriginallyTriggeringUpdate((XAbstractTriggeringUpdate) null);
			return;
		case SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS:
			unsetAdditionalTriggeringByThis();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS:
			return isSetOriginalTriggeringOfThis();
		case SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE:
			return basicGetOriginallyTriggeringUpdate() != null;
		case SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS:
			return isSetAdditionalTriggeringByThis();
		}
		return super.eIsSet(featureID);
	}

} //XTriggeredUpdateImpl
