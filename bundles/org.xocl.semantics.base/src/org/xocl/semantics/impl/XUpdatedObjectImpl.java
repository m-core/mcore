/**
 */
package org.xocl.semantics.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XObjectUpdateType;
import org.xocl.semantics.XUpdatedFeature;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XUpdatedOperation;
import org.xocl.semantics.XUpdatedTypedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XUpdated Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getObjectUpdateType <em>Object Update Type</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getObject <em>Object</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getClassForCreation <em>Class For Creation</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getUpdatedFeature <em>Updated Feature</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getObjectIsFocusedAfterExecution <em>Object Is Focused After Execution</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getXUpdatedTypedElement <em>XUpdated Typed Element</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getUpdatedOperation <em>Updated Operation</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getPersistenceLocation <em>Persistence Location</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedObjectImpl#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XUpdatedObjectImpl extends XSemanticsElementImpl implements XUpdatedObject {
	/**
	 * The default value of the '{@link #getObjectUpdateType() <em>Object Update Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectUpdateType()
	 * @generated
	 * @ordered
	 */
	protected static final XObjectUpdateType OBJECT_UPDATE_TYPE_EDEFAULT = XObjectUpdateType.KEEP;

	/**
	 * The cached value of the '{@link #getObjectUpdateType() <em>Object Update Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectUpdateType()
	 * @generated
	 * @ordered
	 */
	protected XObjectUpdateType objectUpdateType = OBJECT_UPDATE_TYPE_EDEFAULT;

	/**
	 * This is true if the Object Update Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean objectUpdateTypeESet;

	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected EObject object;

	/**
	 * The cached value of the '{@link #getClassForCreation() <em>Class For Creation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassForCreation()
	 * @generated
	 * @ordered
	 */
	protected EClass classForCreation;

	/**
	 * This is true if the Class For Creation reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean classForCreationESet;

	/**
	 * The cached value of the '{@link #getUpdatedFeature() <em>Updated Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdatedFeature()
	 * @generated
	 * @ordered
	 */
	protected EList<XUpdatedFeature> updatedFeature;

	/**
	 * The default value of the '{@link #getObjectIsFocusedAfterExecution() <em>Object Is Focused After Execution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectIsFocusedAfterExecution()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean OBJECT_IS_FOCUSED_AFTER_EXECUTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUpdatedOperation() <em>Updated Operation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdatedOperation()
	 * @generated
	 * @ordered
	 */
	protected EList<XUpdatedOperation> updatedOperation;

	/**
	 * The default value of the '{@link #getPersistenceLocation() <em>Persistence Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersistenceLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String PERSISTENCE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPersistenceLocation() <em>Persistence Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersistenceLocation()
	 * @generated
	 * @ordered
	 */
	protected String persistenceLocation = PERSISTENCE_LOCATION_EDEFAULT;

	/**
	 * This is true if the Persistence Location attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean persistenceLocationESet;

	/**
	 * The default value of the '{@link #getAlternativePersistencePackage() <em>Alternative Persistence Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistencePackage()
	 * @generated
	 * @ordered
	 */
	protected static final String ALTERNATIVE_PERSISTENCE_PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAlternativePersistencePackage() <em>Alternative Persistence Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistencePackage()
	 * @generated
	 * @ordered
	 */
	protected String alternativePersistencePackage = ALTERNATIVE_PERSISTENCE_PACKAGE_EDEFAULT;

	/**
	 * This is true if the Alternative Persistence Package attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean alternativePersistencePackageESet;

	/**
	 * The default value of the '{@link #getAlternativePersistenceRoot() <em>Alternative Persistence Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistenceRoot()
	 * @generated
	 * @ordered
	 */
	protected static final String ALTERNATIVE_PERSISTENCE_ROOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAlternativePersistenceRoot() <em>Alternative Persistence Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistenceRoot()
	 * @generated
	 * @ordered
	 */
	protected String alternativePersistenceRoot = ALTERNATIVE_PERSISTENCE_ROOT_EDEFAULT;

	/**
	 * This is true if the Alternative Persistence Root attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean alternativePersistenceRootESet;

	/**
	 * The default value of the '{@link #getAlternativePersistenceReference() <em>Alternative Persistence Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistenceReference()
	 * @generated
	 * @ordered
	 */
	protected static final String ALTERNATIVE_PERSISTENCE_REFERENCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAlternativePersistenceReference() <em>Alternative Persistence Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlternativePersistenceReference()
	 * @generated
	 * @ordered
	 */
	protected String alternativePersistenceReference = ALTERNATIVE_PERSISTENCE_REFERENCE_EDEFAULT;

	/**
	 * This is true if the Alternative Persistence Reference attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean alternativePersistenceReferenceESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #nextStateFeatureDefinitionFromFeature <em>Next State Feature Definition From Feature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #nextStateFeatureDefinitionFromFeature
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression nextStateFeatureDefinitionFromFeatureecoreEStructuralFeatureBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #nextStateOperationDefinitionFromOperation <em>Next State Operation Definition From Operation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #nextStateOperationDefinitionFromOperation
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression nextStateOperationDefinitionFromOperationecoreEOperationBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectIsFocusedAfterExecution <em>Object Is Focused After Execution</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectIsFocusedAfterExecution
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectIsFocusedAfterExecutionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getXUpdatedTypedElement <em>XUpdated Typed Element</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXUpdatedTypedElement
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression xUpdatedTypedElementDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XUpdatedObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XUPDATED_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getObject() {
		if (object != null && object.eIsProxy()) {
			InternalEObject oldObject = (InternalEObject) object;
			object = eResolveProxy(oldObject);
			if (object != oldObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SemanticsPackage.XUPDATED_OBJECT__OBJECT,
							oldObject, object));
			}
		}
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(EObject newObject) {
		EObject oldObject = object;
		object = newObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATED_OBJECT__OBJECT, oldObject,
					object));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdatedFeature> getUpdatedFeature() {
		if (updatedFeature == null) {
			updatedFeature = new EObjectContainmentEList.Unsettable.Resolving<XUpdatedFeature>(XUpdatedFeature.class,
					this, SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE);
		}
		return updatedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUpdatedFeature() {
		if (updatedFeature != null)
			((InternalEList.Unsettable<?>) updatedFeature).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUpdatedFeature() {
		return updatedFeature != null && ((InternalEList.Unsettable<?>) updatedFeature).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getObjectIsFocusedAfterExecution() {
		/**
		 * @OCL if (eContainer().oclIsTypeOf(XTransition)) then
		eContainer().oclAsType(XTransition)
		.focusObjects->notEmpty() and eContainer().oclAsType(XTransition)
		.focusObjects->notEmpty()->includes(self) else false endif
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XUPDATED_OBJECT;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION;

		if (objectIsFocusedAfterExecutionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				objectIsFocusedAfterExecutionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XUPDATED_OBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(objectIsFocusedAfterExecutionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, SemanticsPackage.Literals.XUPDATED_OBJECT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdatedTypedElement> getXUpdatedTypedElement() {
		/**
		 * @OCL let e1: OrderedSet(semantics::XUpdatedTypedElement)  = let chain: OrderedSet(semantics::XUpdatedOperation)  = updatedOperation->asOrderedSet() in
		chain->iterate(i:semantics::XUpdatedOperation; r: OrderedSet(semantics::XUpdatedTypedElement)=OrderedSet{} | if i.oclIsKindOf(semantics::XUpdatedTypedElement) then r->including(i.oclAsType(semantics::XUpdatedTypedElement))->asOrderedSet() 
		else r endif)->union(updatedFeature->asOrderedSet()) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XUPDATED_OBJECT;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT;

		if (xUpdatedTypedElementDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				xUpdatedTypedElementDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XUPDATED_OBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(xUpdatedTypedElementDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, SemanticsPackage.Literals.XUPDATED_OBJECT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<XUpdatedTypedElement> result = (EList<XUpdatedTypedElement>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdatedOperation> getUpdatedOperation() {
		if (updatedOperation == null) {
			updatedOperation = new EObjectContainmentEList.Unsettable.Resolving<XUpdatedOperation>(
					XUpdatedOperation.class, this, SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION);
		}
		return updatedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUpdatedOperation() {
		if (updatedOperation != null)
			((InternalEList.Unsettable<?>) updatedOperation).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUpdatedOperation() {
		return updatedOperation != null && ((InternalEList.Unsettable<?>) updatedOperation).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPersistenceLocation() {
		return persistenceLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersistenceLocation(String newPersistenceLocation) {
		String oldPersistenceLocation = persistenceLocation;
		persistenceLocation = newPersistenceLocation;
		boolean oldPersistenceLocationESet = persistenceLocationESet;
		persistenceLocationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION, oldPersistenceLocation, persistenceLocation,
					!oldPersistenceLocationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPersistenceLocation() {
		String oldPersistenceLocation = persistenceLocation;
		boolean oldPersistenceLocationESet = persistenceLocationESet;
		persistenceLocation = PERSISTENCE_LOCATION_EDEFAULT;
		persistenceLocationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION, oldPersistenceLocation,
					PERSISTENCE_LOCATION_EDEFAULT, oldPersistenceLocationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPersistenceLocation() {
		return persistenceLocationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAlternativePersistencePackage() {
		return alternativePersistencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlternativePersistencePackage(String newAlternativePersistencePackage) {
		String oldAlternativePersistencePackage = alternativePersistencePackage;
		alternativePersistencePackage = newAlternativePersistencePackage;
		boolean oldAlternativePersistencePackageESet = alternativePersistencePackageESet;
		alternativePersistencePackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE, oldAlternativePersistencePackage,
					alternativePersistencePackage, !oldAlternativePersistencePackageESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAlternativePersistencePackage() {
		String oldAlternativePersistencePackage = alternativePersistencePackage;
		boolean oldAlternativePersistencePackageESet = alternativePersistencePackageESet;
		alternativePersistencePackage = ALTERNATIVE_PERSISTENCE_PACKAGE_EDEFAULT;
		alternativePersistencePackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE, oldAlternativePersistencePackage,
					ALTERNATIVE_PERSISTENCE_PACKAGE_EDEFAULT, oldAlternativePersistencePackageESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAlternativePersistencePackage() {
		return alternativePersistencePackageESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAlternativePersistenceRoot() {
		return alternativePersistenceRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlternativePersistenceRoot(String newAlternativePersistenceRoot) {
		String oldAlternativePersistenceRoot = alternativePersistenceRoot;
		alternativePersistenceRoot = newAlternativePersistenceRoot;
		boolean oldAlternativePersistenceRootESet = alternativePersistenceRootESet;
		alternativePersistenceRootESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT, oldAlternativePersistenceRoot,
					alternativePersistenceRoot, !oldAlternativePersistenceRootESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAlternativePersistenceRoot() {
		String oldAlternativePersistenceRoot = alternativePersistenceRoot;
		boolean oldAlternativePersistenceRootESet = alternativePersistenceRootESet;
		alternativePersistenceRoot = ALTERNATIVE_PERSISTENCE_ROOT_EDEFAULT;
		alternativePersistenceRootESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT, oldAlternativePersistenceRoot,
					ALTERNATIVE_PERSISTENCE_ROOT_EDEFAULT, oldAlternativePersistenceRootESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAlternativePersistenceRoot() {
		return alternativePersistenceRootESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAlternativePersistenceReference() {
		return alternativePersistenceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlternativePersistenceReference(String newAlternativePersistenceReference) {
		String oldAlternativePersistenceReference = alternativePersistenceReference;
		alternativePersistenceReference = newAlternativePersistenceReference;
		boolean oldAlternativePersistenceReferenceESet = alternativePersistenceReferenceESet;
		alternativePersistenceReferenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE,
					oldAlternativePersistenceReference, alternativePersistenceReference,
					!oldAlternativePersistenceReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAlternativePersistenceReference() {
		String oldAlternativePersistenceReference = alternativePersistenceReference;
		boolean oldAlternativePersistenceReferenceESet = alternativePersistenceReferenceESet;
		alternativePersistenceReference = ALTERNATIVE_PERSISTENCE_REFERENCE_EDEFAULT;
		alternativePersistenceReferenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE,
					oldAlternativePersistenceReference, ALTERNATIVE_PERSISTENCE_REFERENCE_EDEFAULT,
					oldAlternativePersistenceReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAlternativePersistenceReference() {
		return alternativePersistenceReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XObjectUpdateType getObjectUpdateType() {
		return objectUpdateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectUpdateType(XObjectUpdateType newObjectUpdateType) {
		XObjectUpdateType oldObjectUpdateType = objectUpdateType;
		objectUpdateType = newObjectUpdateType == null ? OBJECT_UPDATE_TYPE_EDEFAULT : newObjectUpdateType;
		boolean oldObjectUpdateTypeESet = objectUpdateTypeESet;
		objectUpdateTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE,
					oldObjectUpdateType, objectUpdateType, !oldObjectUpdateTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObjectUpdateType() {
		XObjectUpdateType oldObjectUpdateType = objectUpdateType;
		boolean oldObjectUpdateTypeESet = objectUpdateTypeESet;
		objectUpdateType = OBJECT_UPDATE_TYPE_EDEFAULT;
		objectUpdateTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE, oldObjectUpdateType,
					OBJECT_UPDATE_TYPE_EDEFAULT, oldObjectUpdateTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObjectUpdateType() {
		return objectUpdateTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassForCreation() {
		if (classForCreation != null && classForCreation.eIsProxy()) {
			InternalEObject oldClassForCreation = (InternalEObject) classForCreation;
			classForCreation = (EClass) eResolveProxy(oldClassForCreation);
			if (classForCreation != oldClassForCreation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION, oldClassForCreation,
							classForCreation));
			}
		}
		return classForCreation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetClassForCreation() {
		return classForCreation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassForCreation(EClass newClassForCreation) {
		EClass oldClassForCreation = classForCreation;
		classForCreation = newClassForCreation;
		boolean oldClassForCreationESet = classForCreationESet;
		classForCreationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION,
					oldClassForCreation, classForCreation, !oldClassForCreationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetClassForCreation() {
		EClass oldClassForCreation = classForCreation;
		boolean oldClassForCreationESet = classForCreationESet;
		classForCreation = null;
		classForCreationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION, oldClassForCreation, null,
					oldClassForCreationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetClassForCreation() {
		return classForCreationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdatedFeature nextStateFeatureDefinitionFromFeature(EStructuralFeature updatedFeature) {

		for (XUpdatedFeature xUpdatedFeature : getUpdatedFeature()) {
			if (xUpdatedFeature.getFeature() == updatedFeature) {
				return xUpdatedFeature;
			}
		}
		XUpdatedFeature result = SemanticsFactory.eINSTANCE.createXUpdatedFeature();
		getUpdatedFeature().add(result);
		result.setFeature(updatedFeature);
		return result;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdatedOperation nextStateOperationDefinitionFromOperation(EOperation updatedOperation) {

		for (XUpdatedOperation xUpdatedOperation : getUpdatedOperation()) {
			if (xUpdatedOperation.getOperation() == updatedOperation) {
				return (XUpdatedOperation) xUpdatedOperation;
			}
		}
		XUpdatedOperation result = SemanticsFactory.eINSTANCE.createXUpdatedOperation();

		getUpdatedOperation().add(result);

		result.setOperation(updatedOperation);
		return result;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE:
			return ((InternalEList<?>) getUpdatedFeature()).basicRemove(otherEnd, msgs);
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION:
			return ((InternalEList<?>) getUpdatedOperation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE:
			return getObjectUpdateType();
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT:
			if (resolve)
				return getObject();
			return basicGetObject();
		case SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION:
			if (resolve)
				return getClassForCreation();
			return basicGetClassForCreation();
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE:
			return getUpdatedFeature();
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION:
			return getObjectIsFocusedAfterExecution();
		case SemanticsPackage.XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT:
			return getXUpdatedTypedElement();
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION:
			return getUpdatedOperation();
		case SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION:
			return getPersistenceLocation();
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE:
			return getAlternativePersistencePackage();
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT:
			return getAlternativePersistenceRoot();
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE:
			return getAlternativePersistenceReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE:
			setObjectUpdateType((XObjectUpdateType) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT:
			setObject((EObject) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION:
			setClassForCreation((EClass) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE:
			getUpdatedFeature().clear();
			getUpdatedFeature().addAll((Collection<? extends XUpdatedFeature>) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION:
			getUpdatedOperation().clear();
			getUpdatedOperation().addAll((Collection<? extends XUpdatedOperation>) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION:
			setPersistenceLocation((String) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE:
			setAlternativePersistencePackage((String) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT:
			setAlternativePersistenceRoot((String) newValue);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE:
			setAlternativePersistenceReference((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE:
			unsetObjectUpdateType();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT:
			setObject((EObject) null);
			return;
		case SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION:
			unsetClassForCreation();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE:
			unsetUpdatedFeature();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION:
			unsetUpdatedOperation();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION:
			unsetPersistenceLocation();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE:
			unsetAlternativePersistencePackage();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT:
			unsetAlternativePersistenceRoot();
			return;
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE:
			unsetAlternativePersistenceReference();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_UPDATE_TYPE:
			return isSetObjectUpdateType();
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT:
			return object != null;
		case SemanticsPackage.XUPDATED_OBJECT__CLASS_FOR_CREATION:
			return isSetClassForCreation();
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_FEATURE:
			return isSetUpdatedFeature();
		case SemanticsPackage.XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION:
			return OBJECT_IS_FOCUSED_AFTER_EXECUTION_EDEFAULT == null ? getObjectIsFocusedAfterExecution() != null
					: !OBJECT_IS_FOCUSED_AFTER_EXECUTION_EDEFAULT.equals(getObjectIsFocusedAfterExecution());
		case SemanticsPackage.XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT:
			return !getXUpdatedTypedElement().isEmpty();
		case SemanticsPackage.XUPDATED_OBJECT__UPDATED_OPERATION:
			return isSetUpdatedOperation();
		case SemanticsPackage.XUPDATED_OBJECT__PERSISTENCE_LOCATION:
			return isSetPersistenceLocation();
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE:
			return isSetAlternativePersistencePackage();
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT:
			return isSetAlternativePersistenceRoot();
		case SemanticsPackage.XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE:
			return isSetAlternativePersistenceReference();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case SemanticsPackage.XUPDATED_OBJECT___NEXT_STATE_FEATURE_DEFINITION_FROM_FEATURE__ESTRUCTURALFEATURE:
			return nextStateFeatureDefinitionFromFeature((EStructuralFeature) arguments.get(0));
		case SemanticsPackage.XUPDATED_OBJECT___NEXT_STATE_OPERATION_DEFINITION_FROM_OPERATION__EOPERATION:
			return nextStateOperationDefinitionFromOperation((EOperation) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (objectUpdateType: ");
		if (objectUpdateTypeESet)
			result.append(objectUpdateType);
		else
			result.append("<unset>");
		result.append(", persistenceLocation: ");
		if (persistenceLocationESet)
			result.append(persistenceLocation);
		else
			result.append("<unset>");
		result.append(", alternativePersistencePackage: ");
		if (alternativePersistencePackageESet)
			result.append(alternativePersistencePackage);
		else
			result.append("<unset>");
		result.append(", alternativePersistenceRoot: ");
		if (alternativePersistenceRootESet)
			result.append(alternativePersistenceRoot);
		else
			result.append("<unset>");
		result.append(", alternativePersistenceReference: ");
		if (alternativePersistenceReferenceESet)
			result.append(alternativePersistenceReference);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Object'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (SemanticsPackage.Literals.XUPDATED_OBJECT);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} //XUpdatedObjectImpl
