/**
 */
package org.xocl.semantics.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XAdditionalTriggeringSpecification;
import org.xocl.semantics.XTriggeredUpdate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XAbstract Triggering Update</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl#getTriggeredOwnedUpdate <em>Triggered Owned Update</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl#getAdditionalTriggeringOfThis <em>Additional Triggering Of This</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl#getProcesssed <em>Processsed</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl#getAdditionallyTriggeringUpdate <em>Additionally Triggering Update</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl#getAllOwnedUpdates <em>All Owned Updates</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class XAbstractTriggeringUpdateImpl extends XUpdateImpl implements XAbstractTriggeringUpdate {
	/**
	 * The cached value of the '{@link #getTriggeredOwnedUpdate() <em>Triggered Owned Update</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggeredOwnedUpdate()
	 * @generated
	 * @ordered
	 */
	protected EList<XTriggeredUpdate> triggeredOwnedUpdate;

	/**
	 * The cached value of the '{@link #getAdditionalTriggeringOfThis() <em>Additional Triggering Of This</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalTriggeringOfThis()
	 * @generated
	 * @ordered
	 */
	protected EList<XAdditionalTriggeringSpecification> additionalTriggeringOfThis;

	/**
	 * The default value of the '{@link #getProcesssed() <em>Processsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcesssed()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean PROCESSSED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProcesssed() <em>Processsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcesssed()
	 * @generated
	 * @ordered
	 */
	protected Boolean processsed = PROCESSSED_EDEFAULT;

	/**
	 * This is true if the Processsed attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean processsedESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAdditionallyTriggeringUpdate <em>Additionally Triggering Update</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionallyTriggeringUpdate
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression additionallyTriggeringUpdateDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllOwnedUpdates <em>All Owned Updates</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllOwnedUpdates
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allOwnedUpdatesDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XAbstractTriggeringUpdateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XTriggeredUpdate> getTriggeredOwnedUpdate() {
		if (triggeredOwnedUpdate == null) {
			triggeredOwnedUpdate = new EObjectContainmentWithInverseEList.Unsettable.Resolving<XTriggeredUpdate>(
					XTriggeredUpdate.class, this, SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE,
					SemanticsPackage.XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE);
		}
		return triggeredOwnedUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTriggeredOwnedUpdate() {
		if (triggeredOwnedUpdate != null)
			((InternalEList.Unsettable<?>) triggeredOwnedUpdate).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTriggeredOwnedUpdate() {
		return triggeredOwnedUpdate != null && ((InternalEList.Unsettable<?>) triggeredOwnedUpdate).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XAdditionalTriggeringSpecification> getAdditionalTriggeringOfThis() {
		if (additionalTriggeringOfThis == null) {
			additionalTriggeringOfThis = new EObjectContainmentEList.Unsettable.Resolving<XAdditionalTriggeringSpecification>(
					XAdditionalTriggeringSpecification.class, this,
					SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS);
		}
		return additionalTriggeringOfThis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAdditionalTriggeringOfThis() {
		if (additionalTriggeringOfThis != null)
			((InternalEList.Unsettable<?>) additionalTriggeringOfThis).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAdditionalTriggeringOfThis() {
		return additionalTriggeringOfThis != null && ((InternalEList.Unsettable<?>) additionalTriggeringOfThis).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getProcesssed() {
		return processsed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcesssed(Boolean newProcesssed) {
		Boolean oldProcesssed = processsed;
		processsed = newProcesssed;
		boolean oldProcesssedESet = processsedESet;
		processsedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED, oldProcesssed, processsed,
					!oldProcesssedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProcesssed() {
		Boolean oldProcesssed = processsed;
		boolean oldProcesssedESet = processsedESet;
		processsed = PROCESSSED_EDEFAULT;
		processsedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED, oldProcesssed, PROCESSSED_EDEFAULT,
					oldProcesssedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProcesssed() {
		return processsedESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XTriggeredUpdate> getAdditionallyTriggeringUpdate() {
		/**
		 * @OCL additionalTriggeringOfThis->asOrderedSet()->collect(it: semantics::XAdditionalTriggeringSpecification | it.additionalTriggeringUpdate)->asOrderedSet()->excluding(null)->asOrderedSet() 
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE;

		if (additionallyTriggeringUpdateDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				additionallyTriggeringUpdateDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(additionallyTriggeringUpdateDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<XTriggeredUpdate> result = (EList<XTriggeredUpdate>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdate> getAllOwnedUpdates() {
		/**
		 * @OCL triggeredOwnedUpdate.oclAsType(XUpdate)->union(triggeredOwnedUpdate.allOwnedUpdates)
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES;

		if (allOwnedUpdatesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				allOwnedUpdatesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allOwnedUpdatesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XABSTRACT_TRIGGERING_UPDATE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<XUpdate> result = (EList<XUpdate>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	protected XUpdate addAbstractUpdateToTriggeringHierarchy() {
		XTriggeredUpdate newAbstractUpdate = SemanticsFactory.eINSTANCE.createXTriggeredUpdate();
		getTriggeredOwnedUpdate().add(newAbstractUpdate);
		return newAbstractUpdate;
	};

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getTriggeredOwnedUpdate()).basicAdd(otherEnd,
					msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
			return ((InternalEList<?>) getTriggeredOwnedUpdate()).basicRemove(otherEnd, msgs);
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS:
			return ((InternalEList<?>) getAdditionalTriggeringOfThis()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
			return getTriggeredOwnedUpdate();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS:
			return getAdditionalTriggeringOfThis();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED:
			return getProcesssed();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE:
			return getAdditionallyTriggeringUpdate();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES:
			return getAllOwnedUpdates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
			getTriggeredOwnedUpdate().clear();
			getTriggeredOwnedUpdate().addAll((Collection<? extends XTriggeredUpdate>) newValue);
			return;
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS:
			getAdditionalTriggeringOfThis().clear();
			getAdditionalTriggeringOfThis().addAll((Collection<? extends XAdditionalTriggeringSpecification>) newValue);
			return;
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED:
			setProcesssed((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
			unsetTriggeredOwnedUpdate();
			return;
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS:
			unsetAdditionalTriggeringOfThis();
			return;
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED:
			unsetProcesssed();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE:
			return isSetTriggeredOwnedUpdate();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS:
			return isSetAdditionalTriggeringOfThis();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__PROCESSSED:
			return isSetProcesssed();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE:
			return !getAdditionallyTriggeringUpdate().isEmpty();
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES:
			return !getAllOwnedUpdates().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (processsed: ");
		if (processsedESet)
			result.append(processsed);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //XAbstractTriggeringUpdateImpl
