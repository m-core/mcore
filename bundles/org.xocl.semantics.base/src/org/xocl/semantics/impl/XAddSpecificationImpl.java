/**
 */
package org.xocl.semantics.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAddSpecification;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XUpdatedObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XAdd Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XAddSpecificationImpl#getAddKind <em>Add Kind</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XAddSpecificationImpl#getBeforeObject <em>Before Object</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XAddSpecificationImpl#getAfterObject <em>After Object</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XAddSpecificationImpl extends MinimalEObjectImpl.Container implements XAddSpecification {
	/**
	 * The default value of the '{@link #getAddKind() <em>Add Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddKind()
	 * @generated
	 * @ordered
	 */
	protected static final XAddUpdateMode ADD_KIND_EDEFAULT = XAddUpdateMode.FIRST;

	/**
	 * The cached value of the '{@link #getAddKind() <em>Add Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddKind()
	 * @generated
	 * @ordered
	 */
	protected XAddUpdateMode addKind = ADD_KIND_EDEFAULT;

	/**
	 * This is true if the Add Kind attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean addKindESet;

	/**
	 * The cached value of the '{@link #getBeforeObject() <em>Before Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeforeObject()
	 * @generated
	 * @ordered
	 */
	protected XUpdatedObject beforeObject;

	/**
	 * This is true if the Before Object reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean beforeObjectESet;

	/**
	 * The cached value of the '{@link #getAfterObject() <em>After Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAfterObject()
	 * @generated
	 * @ordered
	 */
	protected XUpdatedObject afterObject;

	/**
	 * This is true if the After Object reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean afterObjectESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XAddSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XADD_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject getBeforeObject() {
		if (beforeObject != null && beforeObject.eIsProxy()) {
			InternalEObject oldBeforeObject = (InternalEObject) beforeObject;
			beforeObject = (XUpdatedObject) eResolveProxy(oldBeforeObject);
			if (beforeObject != oldBeforeObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT, oldBeforeObject, beforeObject));
			}
		}
		return beforeObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject basicGetBeforeObject() {
		return beforeObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBeforeObject(XUpdatedObject newBeforeObject) {
		XUpdatedObject oldBeforeObject = beforeObject;
		beforeObject = newBeforeObject;
		boolean oldBeforeObjectESet = beforeObjectESet;
		beforeObjectESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT,
					oldBeforeObject, beforeObject, !oldBeforeObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBeforeObject() {
		XUpdatedObject oldBeforeObject = beforeObject;
		boolean oldBeforeObjectESet = beforeObjectESet;
		beforeObject = null;
		beforeObjectESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT,
					oldBeforeObject, null, oldBeforeObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBeforeObject() {
		return beforeObjectESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject getAfterObject() {
		if (afterObject != null && afterObject.eIsProxy()) {
			InternalEObject oldAfterObject = (InternalEObject) afterObject;
			afterObject = (XUpdatedObject) eResolveProxy(oldAfterObject);
			if (afterObject != oldAfterObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT, oldAfterObject, afterObject));
			}
		}
		return afterObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject basicGetAfterObject() {
		return afterObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAfterObject(XUpdatedObject newAfterObject) {
		XUpdatedObject oldAfterObject = afterObject;
		afterObject = newAfterObject;
		boolean oldAfterObjectESet = afterObjectESet;
		afterObjectESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT,
					oldAfterObject, afterObject, !oldAfterObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAfterObject() {
		XUpdatedObject oldAfterObject = afterObject;
		boolean oldAfterObjectESet = afterObjectESet;
		afterObject = null;
		afterObjectESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT,
					oldAfterObject, null, oldAfterObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAfterObject() {
		return afterObjectESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAddUpdateMode getAddKind() {
		return addKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddKind(XAddUpdateMode newAddKind) {
		XAddUpdateMode oldAddKind = addKind;
		addKind = newAddKind == null ? ADD_KIND_EDEFAULT : newAddKind;
		boolean oldAddKindESet = addKindESet;
		addKindESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XADD_SPECIFICATION__ADD_KIND,
					oldAddKind, addKind, !oldAddKindESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAddKind() {
		XAddUpdateMode oldAddKind = addKind;
		boolean oldAddKindESet = addKindESet;
		addKind = ADD_KIND_EDEFAULT;
		addKindESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XADD_SPECIFICATION__ADD_KIND,
					oldAddKind, ADD_KIND_EDEFAULT, oldAddKindESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAddKind() {
		return addKindESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XADD_SPECIFICATION__ADD_KIND:
			return getAddKind();
		case SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT:
			if (resolve)
				return getBeforeObject();
			return basicGetBeforeObject();
		case SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT:
			if (resolve)
				return getAfterObject();
			return basicGetAfterObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XADD_SPECIFICATION__ADD_KIND:
			setAddKind((XAddUpdateMode) newValue);
			return;
		case SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT:
			setBeforeObject((XUpdatedObject) newValue);
			return;
		case SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT:
			setAfterObject((XUpdatedObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XADD_SPECIFICATION__ADD_KIND:
			unsetAddKind();
			return;
		case SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT:
			unsetBeforeObject();
			return;
		case SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT:
			unsetAfterObject();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XADD_SPECIFICATION__ADD_KIND:
			return isSetAddKind();
		case SemanticsPackage.XADD_SPECIFICATION__BEFORE_OBJECT:
			return isSetBeforeObject();
		case SemanticsPackage.XADD_SPECIFICATION__AFTER_OBJECT:
			return isSetAfterObject();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (addKind: ");
		if (addKindESet)
			result.append(addKind);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //XAddSpecificationImpl
