/**
 */
package org.xocl.semantics.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XAdditionalTriggeringSpecification;
import org.xocl.semantics.XTriggeredUpdate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XAdditional Triggering Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XAdditionalTriggeringSpecificationImpl#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XAdditionalTriggeringSpecificationImpl extends XTriggeringSpecificationImpl
		implements XAdditionalTriggeringSpecification {

	/**
	 * The cached value of the '{@link #getAdditionalTriggeringUpdate() <em>Additional Triggering Update</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalTriggeringUpdate()
	 * @generated
	 * @ordered
	 */
	protected XTriggeredUpdate additionalTriggeringUpdate;
	/**
	 * This is true if the Additional Triggering Update reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean additionalTriggeringUpdateESet;
	/**
	 * The parsed OCL expression for the derivation of '{@link #getTriggeringUpdate <em>Triggering Update</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggeringUpdate
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression triggeringUpdateDeriveOCL;
	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";
	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XAdditionalTriggeringSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XADDITIONAL_TRIGGERING_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTriggeredUpdate getAdditionalTriggeringUpdate() {
		if (additionalTriggeringUpdate != null && additionalTriggeringUpdate.eIsProxy()) {
			InternalEObject oldAdditionalTriggeringUpdate = (InternalEObject) additionalTriggeringUpdate;
			additionalTriggeringUpdate = (XTriggeredUpdate) eResolveProxy(oldAdditionalTriggeringUpdate);
			if (additionalTriggeringUpdate != oldAdditionalTriggeringUpdate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE,
							oldAdditionalTriggeringUpdate, additionalTriggeringUpdate));
			}
		}
		return additionalTriggeringUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTriggeredUpdate basicGetAdditionalTriggeringUpdate() {
		return additionalTriggeringUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdditionalTriggeringUpdate(XTriggeredUpdate newAdditionalTriggeringUpdate,
			NotificationChain msgs) {
		XTriggeredUpdate oldAdditionalTriggeringUpdate = additionalTriggeringUpdate;
		additionalTriggeringUpdate = newAdditionalTriggeringUpdate;
		boolean oldAdditionalTriggeringUpdateESet = additionalTriggeringUpdateESet;
		additionalTriggeringUpdateESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE,
					oldAdditionalTriggeringUpdate, newAdditionalTriggeringUpdate, !oldAdditionalTriggeringUpdateESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdditionalTriggeringUpdate(XTriggeredUpdate newAdditionalTriggeringUpdate) {
		if (newAdditionalTriggeringUpdate != additionalTriggeringUpdate) {
			NotificationChain msgs = null;
			if (additionalTriggeringUpdate != null)
				msgs = ((InternalEObject) additionalTriggeringUpdate).eInverseRemove(this,
						SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS, XTriggeredUpdate.class,
						msgs);
			if (newAdditionalTriggeringUpdate != null)
				msgs = ((InternalEObject) newAdditionalTriggeringUpdate).eInverseAdd(this,
						SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS, XTriggeredUpdate.class,
						msgs);
			msgs = basicSetAdditionalTriggeringUpdate(newAdditionalTriggeringUpdate, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAdditionalTriggeringUpdateESet = additionalTriggeringUpdateESet;
			additionalTriggeringUpdateESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE,
						newAdditionalTriggeringUpdate, newAdditionalTriggeringUpdate,
						!oldAdditionalTriggeringUpdateESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAdditionalTriggeringUpdate(NotificationChain msgs) {
		XTriggeredUpdate oldAdditionalTriggeringUpdate = additionalTriggeringUpdate;
		additionalTriggeringUpdate = null;
		boolean oldAdditionalTriggeringUpdateESet = additionalTriggeringUpdateESet;
		additionalTriggeringUpdateESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE,
					oldAdditionalTriggeringUpdate, null, oldAdditionalTriggeringUpdateESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAdditionalTriggeringUpdate() {
		if (additionalTriggeringUpdate != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) additionalTriggeringUpdate).eInverseRemove(this,
					SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS, XTriggeredUpdate.class, msgs);
			msgs = basicUnsetAdditionalTriggeringUpdate(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAdditionalTriggeringUpdateESet = additionalTriggeringUpdateESet;
			additionalTriggeringUpdateESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE, null, null,
						oldAdditionalTriggeringUpdateESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAdditionalTriggeringUpdate() {
		return additionalTriggeringUpdateESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE:
			if (additionalTriggeringUpdate != null)
				msgs = ((InternalEObject) additionalTriggeringUpdate).eInverseRemove(this,
						SemanticsPackage.XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS, XTriggeredUpdate.class,
						msgs);
			return basicSetAdditionalTriggeringUpdate((XTriggeredUpdate) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE:
			return basicUnsetAdditionalTriggeringUpdate(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE:
			if (resolve)
				return getAdditionalTriggeringUpdate();
			return basicGetAdditionalTriggeringUpdate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE:
			setAdditionalTriggeringUpdate((XTriggeredUpdate) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE:
			unsetAdditionalTriggeringUpdate();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE:
			return isSetAdditionalTriggeringUpdate();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL triggeringUpdate additionalTriggeringUpdate
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public XAbstractTriggeringUpdate basicGetTriggeringUpdate() {
		EClass eClass = (SemanticsPackage.Literals.XADDITIONAL_TRIGGERING_SPECIFICATION);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE;

		if (triggeringUpdateDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				triggeringUpdateDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(triggeringUpdateDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (XAbstractTriggeringUpdate) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Additional Triggering'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (SemanticsPackage.Literals.XADDITIONAL_TRIGGERING_SPECIFICATION);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} //XAdditionalTriggeringSpecificationImpl
