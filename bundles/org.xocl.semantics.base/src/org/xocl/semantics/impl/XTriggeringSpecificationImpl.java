/**
 */
package org.xocl.semantics.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XTriggeringSpecification;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XTriggering Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XTriggeringSpecificationImpl#getFeatureOfTriggeringUpdateAnnotation <em>Feature Of Triggering Update Annotation</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTriggeringSpecificationImpl#getNameofTriggeringUpdateAnnotation <em>Nameof Triggering Update Annotation</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTriggeringSpecificationImpl#getTriggeringUpdate <em>Triggering Update</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class XTriggeringSpecificationImpl extends XSemanticsElementImpl implements XTriggeringSpecification {
	/**
	 * The default value of the '{@link #getNameofTriggeringUpdateAnnotation() <em>Nameof Triggering Update Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameofTriggeringUpdateAnnotation()
	 * @generated
	 * @ordered
	 */
	protected static final String NAMEOF_TRIGGERING_UPDATE_ANNOTATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNameofTriggeringUpdateAnnotation() <em>Nameof Triggering Update Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameofTriggeringUpdateAnnotation()
	 * @generated
	 * @ordered
	 */
	protected String nameofTriggeringUpdateAnnotation = NAMEOF_TRIGGERING_UPDATE_ANNOTATION_EDEFAULT;

	/**
	 * This is true if the Nameof Triggering Update Annotation attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameofTriggeringUpdateAnnotationESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFeatureOfTriggeringUpdateAnnotation <em>Feature Of Triggering Update Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureOfTriggeringUpdateAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression featureOfTriggeringUpdateAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTriggeringUpdate <em>Triggering Update</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggeringUpdate
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression triggeringUpdateDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XTriggeringSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getFeatureOfTriggeringUpdateAnnotation() {
		EStructuralFeature featureOfTriggeringUpdateAnnotation = basicGetFeatureOfTriggeringUpdateAnnotation();
		return featureOfTriggeringUpdateAnnotation != null && featureOfTriggeringUpdateAnnotation.eIsProxy()
				? (EStructuralFeature) eResolveProxy((InternalEObject) featureOfTriggeringUpdateAnnotation)
				: featureOfTriggeringUpdateAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetFeatureOfTriggeringUpdateAnnotation() {
		/**
		 * @OCL if triggeringUpdate.oclIsUndefined()
		then null
		else triggeringUpdate.feature
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION;

		if (featureOfTriggeringUpdateAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				featureOfTriggeringUpdateAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(featureOfTriggeringUpdateAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EStructuralFeature result = (EStructuralFeature) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNameofTriggeringUpdateAnnotation() {
		return nameofTriggeringUpdateAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameofTriggeringUpdateAnnotation(String newNameofTriggeringUpdateAnnotation) {
		String oldNameofTriggeringUpdateAnnotation = nameofTriggeringUpdateAnnotation;
		nameofTriggeringUpdateAnnotation = newNameofTriggeringUpdateAnnotation;
		boolean oldNameofTriggeringUpdateAnnotationESet = nameofTriggeringUpdateAnnotationESet;
		nameofTriggeringUpdateAnnotationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION,
					oldNameofTriggeringUpdateAnnotation, nameofTriggeringUpdateAnnotation,
					!oldNameofTriggeringUpdateAnnotationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNameofTriggeringUpdateAnnotation() {
		String oldNameofTriggeringUpdateAnnotation = nameofTriggeringUpdateAnnotation;
		boolean oldNameofTriggeringUpdateAnnotationESet = nameofTriggeringUpdateAnnotationESet;
		nameofTriggeringUpdateAnnotation = NAMEOF_TRIGGERING_UPDATE_ANNOTATION_EDEFAULT;
		nameofTriggeringUpdateAnnotationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION,
					oldNameofTriggeringUpdateAnnotation, NAMEOF_TRIGGERING_UPDATE_ANNOTATION_EDEFAULT,
					oldNameofTriggeringUpdateAnnotationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNameofTriggeringUpdateAnnotation() {
		return nameofTriggeringUpdateAnnotationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAbstractTriggeringUpdate getTriggeringUpdate() {
		XAbstractTriggeringUpdate triggeringUpdate = basicGetTriggeringUpdate();
		return triggeringUpdate != null && triggeringUpdate.eIsProxy()
				? (XAbstractTriggeringUpdate) eResolveProxy((InternalEObject) triggeringUpdate) : triggeringUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAbstractTriggeringUpdate basicGetTriggeringUpdate() {
		/**
		 * @OCL let nl: semantics::XAbstractTriggeringUpdate = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE;

		if (triggeringUpdateDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				triggeringUpdateDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(triggeringUpdateDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			XAbstractTriggeringUpdate result = (XAbstractTriggeringUpdate) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION:
			if (resolve)
				return getFeatureOfTriggeringUpdateAnnotation();
			return basicGetFeatureOfTriggeringUpdateAnnotation();
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION:
			return getNameofTriggeringUpdateAnnotation();
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE:
			if (resolve)
				return getTriggeringUpdate();
			return basicGetTriggeringUpdate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION:
			setNameofTriggeringUpdateAnnotation((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION:
			unsetNameofTriggeringUpdateAnnotation();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION:
			return basicGetFeatureOfTriggeringUpdateAnnotation() != null;
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION:
			return isSetNameofTriggeringUpdateAnnotation();
		case SemanticsPackage.XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE:
			return basicGetTriggeringUpdate() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nameofTriggeringUpdateAnnotation: ");
		if (nameofTriggeringUpdateAnnotationESet)
			result.append(nameofTriggeringUpdateAnnotation);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //XTriggeringSpecificationImpl
