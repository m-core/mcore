/**
 */
package org.xocl.semantics;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XValue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XValue#getUpdatedObjectValue <em>Updated Object Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getBooleanValue <em>Boolean Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getIntegerValue <em>Integer Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getDoubleValue <em>Double Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getStringValue <em>String Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getDateValue <em>Date Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getLiteralValue <em>Literal Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getIsCorrect <em>Is Correct</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getCreateClone <em>Create Clone</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getEObjectValue <em>EObject Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XValue#getParameterArguments <em>Parameter Arguments</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXValue()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='if self.eContainmentFeature().name=\'expectedToBeAddedValue\'\r\n  then \'Expected Added\'\r\nelse if self.eContainmentFeature().name=\'expectedToBeRemovedValue\'\r\n  then \'Expected Removed\'\r\nelse if self.eContainmentFeature().name=\'expectedResultingValue\'\r\n  then \'Expected Resulting\'\r\nelse if self.eContainmentFeature().name=\'oldValue\'\r\n  then \'Old Value\'\r\nelse if self.eContainmentFeature().name=\'newValue\'\r\n  then \'New Value\'\r\nelse if self.eContainmentFeature().name=\'value\'\r\n  then \'Value\'\r\nelse \'TODO\'.concat(self.eContainmentFeature().name) endif endif endif endif endif endif'"
 * @generated
 */

public interface XValue extends XSemanticsElement {
	/**
	 * Returns the value of the '<em><b>Updated Object Value</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdatedObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Updated Object Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Updated Object Value</em>' reference list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_UpdatedObjectValue()
	 * @model
	 * @generated
	 */
	EList<XUpdatedObject> getUpdatedObjectValue();

	/**
	 * Returns the value of the '<em><b>Boolean Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Boolean}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boolean Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boolean Value</em>' attribute list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_BooleanValue()
	 * @model
	 * @generated
	 */
	EList<Boolean> getBooleanValue();

	/**
	 * Returns the value of the '<em><b>Integer Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Value</em>' attribute list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_IntegerValue()
	 * @model
	 * @generated
	 */
	EList<Integer> getIntegerValue();

	/**
	 * Returns the value of the '<em><b>Double Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Double Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Double Value</em>' attribute list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_DoubleValue()
	 * @model
	 * @generated
	 */
	EList<Double> getDoubleValue();

	/**
	 * Returns the value of the '<em><b>String Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Value</em>' attribute list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_StringValue()
	 * @model
	 * @generated
	 */
	EList<String> getStringValue();

	/**
	 * Returns the value of the '<em><b>Date Value</b></em>' attribute list.
	 * The list contents are of type {@link java.util.Date}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Value</em>' attribute list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_DateValue()
	 * @model
	 * @generated
	 */
	EList<Date> getDateValue();

	/**
	 * Returns the value of the '<em><b>Literal Value</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EEnumLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal Value</em>' reference list.
	 * @see #isSetLiteralValue()
	 * @see #unsetLiteralValue()
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_LiteralValue()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<EEnumLiteral> getLiteralValue();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XValue#getLiteralValue <em>Literal Value</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLiteralValue()
	 * @see #getLiteralValue()
	 * @generated
	 */
	void unsetLiteralValue();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XValue#getLiteralValue <em>Literal Value</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Literal Value</em>' reference list is set.
	 * @see #unsetLiteralValue()
	 * @see #getLiteralValue()
	 * @generated
	 */
	boolean isSetLiteralValue();

	/**
	 * Returns the value of the '<em><b>Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Correct</em>' attribute.
	 * @see #setIsCorrect(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_IsCorrect()
	 * @model annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getIsCorrect();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XValue#getIsCorrect <em>Is Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Correct</em>' attribute.
	 * @see #getIsCorrect()
	 * @generated
	 */
	void setIsCorrect(Boolean value);

	/**
	 * Returns the value of the '<em><b>Create Clone</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Clone</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Create Clone</em>' reference.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_CreateClone()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='Tuple{updatedObjectValue=self.updatedObjectValue, booleanValue=self.booleanValue, integerValue= self.integerValue, doubleValue = self.doubleValue, stringValue=self.stringValue, dateValue=self.dateValue, literalValue = self.literalValue}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	XValue getCreateClone();

	/**
	 * Returns the value of the '<em><b>EObject Value</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject Value</em>' reference list.
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_EObjectValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='updatedObjectValue.object->reject(oclIsUndefined())->asOrderedSet()\n'"
	 * @generated
	 */
	EList<EObject> getEObjectValue();

	/**
	 * Returns the value of the '<em><b>Parameter Arguments</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Object}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Arguments</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Arguments</em>' attribute list.
	 * @see #isSetParameterArguments()
	 * @see #unsetParameterArguments()
	 * @see org.xocl.semantics.SemanticsPackage#getXValue_ParameterArguments()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<Object> getParameterArguments();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XValue#getParameterArguments <em>Parameter Arguments</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParameterArguments()
	 * @see #getParameterArguments()
	 * @generated
	 */
	void unsetParameterArguments();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XValue#getParameterArguments <em>Parameter Arguments</em>}' attribute list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parameter Arguments</em>' attribute list is set.
	 * @see #unsetParameterArguments()
	 * @see #getParameterArguments()
	 * @generated
	 */
	boolean isSetParameterArguments();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	Boolean addOtherValues(XValue otherValues);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model otherXValueAnnotation="http://www.montages.com/mCore/MCore mName='Other XValue'"
	 *        annotation="http://www.xocl.org/OCL body='/* overwritten in Java \052/\nstringValue->intersection(otherXValue.stringValue)->isEmpty() and\nbooleanValue->intersection(otherXValue.booleanValue)->isEmpty() and\nintegerValue->intersection(otherXValue.integerValue)->isEmpty() and\ndoubleValue->intersection(otherXValue.doubleValue)->isEmpty() and\ndateValue->intersection(otherXValue.dateValue)->isEmpty() and\nself.updatedObjectValue->intersection(otherXValue.updatedObjectValue)->isEmpty()\n'"
	 * @generated
	 */
	Boolean disjointFrom(XValue otherXValue);

} // XValue
