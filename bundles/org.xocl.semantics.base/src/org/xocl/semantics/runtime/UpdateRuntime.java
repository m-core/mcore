package org.xocl.semantics.runtime;

import static org.eclipse.emf.common.command.CompoundCommand.LAST_COMMAND_ALL;
import static org.xocl.semantics.SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES;
import static org.xocl.semantics.SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES;
import static org.xocl.semantics.SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES;
import static org.xocl.semantics.SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES;
import static org.xocl.semantics.SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES;
import static org.xocl.semantics.XUpdateMode.REDEFINE;
import static org.xocl.semantics.XUpdateMode.REMOVE;
import static org.xocl.semantics.runtime.ValueHelpers.createValue;

import java.util.List;
import java.util.Objects;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XObjectUpdateType;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;
import org.xocl.semantics.XUpdatedFeature;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XUpdatedOperation;
import org.xocl.semantics.XValue;
import org.xocl.semantics.commands.ChangeCommandWithOperationCall;

public class UpdateRuntime {

	private final AddUpdateCommandFactory ADD = new AddUpdateCommandFactory();
	private final SetUpdateCommandFactory SET = new SetUpdateCommandFactory();
	private final SemanticsFactory FACTORY = SemanticsFactory.eINSTANCE;

	public CompoundCommand asUpdateCommand(List<XUpdatedObject> allObjects) {
		CompoundCommand cmd = new CompoundCommand(LAST_COMMAND_ALL);

		EditingDomain domain = allObjects.stream() //
				.filter(e -> e.getObjectUpdateType() == XObjectUpdateType.KEEP) //
				.map(e -> e.getObject()) //
				.filter(Objects::nonNull) //
				.map(AdapterFactoryEditingDomain::getEditingDomainFor) //
				.findAny() //
				.orElse(null);

		allObjects.forEach(object -> {

			if (domain != null) {
				object.getUpdatedFeature().stream() //
						// only execute updates for features with storage....
						.filter(feature -> !feature.getFeature().isVolatile()) //
						.map((feature) -> {
							if (feature.getFeature().isMany()) {
								return ADD.createCommand(domain, object,
										feature);
							}
							return SET.createCommand(domain, object, feature);
						}) //
						.filter(Objects::nonNull) //
						.forEach(cmd::append);

				object.getUpdatedOperation().stream() //
						.filter(op -> op instanceof XUpdatedOperation)
						.map(op -> new ChangeCommandWithOperationCall( //
								domain.getResourceSet(), //
								object, //
								op)) //
						.filter(Objects::nonNull) //
						.forEach(cmd::append);
			}
		});

		return cmd.isEmpty() ? null : cmd;
	}

	public void initUpdates(List<XUpdate> allUpdates) {
		allUpdates.stream().forEach(this::initUpdate);
	}

	void initUpdate(XUpdate update) {
		if (update.getUpdatedObject() == null) {
			return;
		}

		if (update.getFeature() == null) {
			XUpdatedOperation updatedOperation = update.getUpdatedObject()
					.nextStateOperationDefinitionFromOperation(
							update.getOperation());

			updatedOperation.getParameter()
					.addAll(update.getValue().getParameterArguments());
		} else {
			if (update.getMode() == REDEFINE) {
				initRedefineUpdate(update);
			} else if (update.getMode() == XUpdateMode.ADD) {
				initAddUpdate(update);
			} else if (update.getMode() == REMOVE) {
				initRemoveUpdate(update);
			}
		}
	}

	private void initRedefineUpdate(XUpdate update) {
		XUpdatedObject xObj = update.getUpdatedObject();
		EStructuralFeature eFeature = update.getFeature();
		XUpdatedFeature feature = getXFeature(update);

		if (isInconsistent(feature)) {
			feature.setInconsistent(true);
		} else {
			XValue clonedValue = update.getValue().getCreateClone();
			feature.setAsFirstInsertValues(clonedValue);
			// updatedObject.setPersistenceLocation(updateToBeProcessed.getValue().getUpdatedObjectValue().);

			// Redefine should first clear the content of the feature
			if (feature.getDeleteValues() == null && eFeature.isMany()) {
				feature.setDeleteValues(createValue(xObj.getObject(), eFeature));
			}
		}
	}

	private void initAddUpdate(XUpdate update) {
		XUpdatedFeature feature = getXFeature(update);

		if (!update.getValue().disjointFrom(feature.getDeleteValues())) {
			feature.setInconsistent(true);
		}

		if (update.getAddSpecification() == null) {
			if (feature.getTowardsEndInsertValues() != null) {
				feature.getTowardsEndInsertValues()
						.addOtherValues(update.getValue());
			}
		} else {
			initializeAddValue(update, feature);
		}
	}

	private void initRemoveUpdate(XUpdate update) {
		XUpdatedFeature feature = getXFeature(update);

		if (isRemoveInconsistent(update, feature)) {
			feature.setInconsistent(true);
		}

		if (feature.getDeleteValues() == null) {
			feature.setDeleteValues(FACTORY.createXValue());
		}

		feature.getDeleteValues().addOtherValues(update.getValue());
	}

	private XUpdatedFeature getXFeature(XUpdate update) {
		XUpdatedObject object = update.getUpdatedObject();
		EStructuralFeature eFeature = update.getFeature();

		return object.nextStateFeatureDefinitionFromFeature(eFeature);
	}

	private void initializeAddValue(XUpdate update, XUpdatedFeature feature) {
		EStructuralFeature insertValues = getPositionFeature(
				update.getAddSpecification().getAddKind(), feature);
		if (insertValues != null) {
			XValue value = (XValue) feature.eGet(insertValues);
			if (value == null) {
				feature.eSet(insertValues, value = FACTORY.createXValue());
			}
			value.addOtherValues(update.getValue());
		}
	}

	private EStructuralFeature getPositionFeature(XAddUpdateMode kind,
			XUpdatedFeature feature) {
		switch (kind) {
		case FIRST:
			return XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES;
		case TOWARDS_BEGINNING:
			return XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES;
		case IN_THE_MIDDLE:
			return XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES;
		case TOWARDS_END:
			return XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES;
		case LAST:
			return XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES;
		default:
			return null;
		}
	}

	private boolean isInconsistent(XUpdatedFeature feature) {
		return feature.getAsFirstInsertValues() != null
				|| feature.getTowardsBeginningInsertValues() != null
				|| feature.getInTheMiddleInsertValues() != null
				|| feature.getTowardsEndInsertValues() != null
				|| feature.getAsLastInsertValues() != null
				|| feature.getDeleteValues() != null;
	}

	private boolean isRemoveInconsistent(XUpdate update,
			XUpdatedFeature feature) {
		return !((feature.getAsFirstInsertValues() == null || feature
				.getAsFirstInsertValues().disjointFrom(update.getValue()))
				&& (feature.getTowardsBeginningInsertValues() == null
						|| feature.getTowardsBeginningInsertValues()
								.disjointFrom(update.getValue()))
				&& (feature.getInTheMiddleInsertValues() == null
						|| feature.getInTheMiddleInsertValues()
								.disjointFrom(update.getValue()))
				&& (feature.getTowardsEndInsertValues() == null
						|| feature.getTowardsEndInsertValues()
								.disjointFrom(update.getValue()))
				&& (feature.getAsLastInsertValues() == null
						|| feature.getAsLastInsertValues()
								.disjointFrom(update.getValue())));
	}

}
