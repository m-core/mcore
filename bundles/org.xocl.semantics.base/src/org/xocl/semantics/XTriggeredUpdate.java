/**
 */
package org.xocl.semantics;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XTriggered Update</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}</li>
 *   <li>{@link org.xocl.semantics.XTriggeredUpdate#getOriginallyTriggeringUpdate <em>Originally Triggering Update</em>}</li>
 *   <li>{@link org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis <em>Additional Triggering By This</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXTriggeredUpdate()
 * @model
 * @generated
 */

public interface XTriggeredUpdate extends XAbstractTriggeringUpdate {
	/**
	 * Returns the value of the '<em><b>Original Triggering Of This</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XOriginalTriggeringSpecification#getContainingTriggeredUpdate <em>Containing Triggered Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Triggering Of This</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Triggering Of This</em>' containment reference.
	 * @see #isSetOriginalTriggeringOfThis()
	 * @see #unsetOriginalTriggeringOfThis()
	 * @see #setOriginalTriggeringOfThis(XOriginalTriggeringSpecification)
	 * @see org.xocl.semantics.SemanticsPackage#getXTriggeredUpdate_OriginalTriggeringOfThis()
	 * @see org.xocl.semantics.XOriginalTriggeringSpecification#getContainingTriggeredUpdate
	 * @model opposite="containingTriggeredUpdate" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	XOriginalTriggeringSpecification getOriginalTriggeringOfThis();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Triggering Of This</em>' containment reference.
	 * @see #isSetOriginalTriggeringOfThis()
	 * @see #unsetOriginalTriggeringOfThis()
	 * @see #getOriginalTriggeringOfThis()
	 * @generated
	 */
	void setOriginalTriggeringOfThis(XOriginalTriggeringSpecification value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOriginalTriggeringOfThis()
	 * @see #getOriginalTriggeringOfThis()
	 * @see #setOriginalTriggeringOfThis(XOriginalTriggeringSpecification)
	 * @generated
	 */
	void unsetOriginalTriggeringOfThis();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Original Triggering Of This</em>' containment reference is set.
	 * @see #unsetOriginalTriggeringOfThis()
	 * @see #getOriginalTriggeringOfThis()
	 * @see #setOriginalTriggeringOfThis(XOriginalTriggeringSpecification)
	 * @generated
	 */
	boolean isSetOriginalTriggeringOfThis();

	/**
	 * Returns the value of the '<em><b>Originally Triggering Update</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate <em>Triggered Owned Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Originally Triggering Update</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Originally Triggering Update</em>' container reference.
	 * @see #setOriginallyTriggeringUpdate(XAbstractTriggeringUpdate)
	 * @see org.xocl.semantics.SemanticsPackage#getXTriggeredUpdate_OriginallyTriggeringUpdate()
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate
	 * @model opposite="triggeredOwnedUpdate" unsettable="true" transient="false"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	XAbstractTriggeringUpdate getOriginallyTriggeringUpdate();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XTriggeredUpdate#getOriginallyTriggeringUpdate <em>Originally Triggering Update</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Originally Triggering Update</em>' container reference.
	 * @see #getOriginallyTriggeringUpdate()
	 * @generated
	 */
	void setOriginallyTriggeringUpdate(XAbstractTriggeringUpdate value);

	/**
	 * Returns the value of the '<em><b>Additional Triggering By This</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XAdditionalTriggeringSpecification}.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Triggering By This</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Triggering By This</em>' reference list.
	 * @see #isSetAdditionalTriggeringByThis()
	 * @see #unsetAdditionalTriggeringByThis()
	 * @see org.xocl.semantics.SemanticsPackage#getXTriggeredUpdate_AdditionalTriggeringByThis()
	 * @see org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate
	 * @model opposite="additionalTriggeringUpdate" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<XAdditionalTriggeringSpecification> getAdditionalTriggeringByThis();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis <em>Additional Triggering By This</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAdditionalTriggeringByThis()
	 * @see #getAdditionalTriggeringByThis()
	 * @generated
	 */
	void unsetAdditionalTriggeringByThis();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis <em>Additional Triggering By This</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Additional Triggering By This</em>' reference list is set.
	 * @see #unsetAdditionalTriggeringByThis()
	 * @see #getAdditionalTriggeringByThis()
	 * @generated
	 */
	boolean isSetAdditionalTriggeringByThis();

} // XTriggeredUpdate
