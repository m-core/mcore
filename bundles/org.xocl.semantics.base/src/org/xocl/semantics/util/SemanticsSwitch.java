/**
 */
package org.xocl.semantics.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.xocl.semantics.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xocl.semantics.SemanticsPackage
 * @generated
 */
public class SemanticsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SemanticsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsSwitch() {
		if (modelPackage == null) {
			modelPackage = SemanticsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case SemanticsPackage.XSEMANTICS_ELEMENT: {
			XSemanticsElement xSemanticsElement = (XSemanticsElement) theEObject;
			T result = caseXSemanticsElement(xSemanticsElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XSEMANTICS: {
			XSemantics xSemantics = (XSemantics) theEObject;
			T result = caseXSemantics(xSemantics);
			if (result == null)
				result = caseXSemanticsElement(xSemantics);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XUPDATE_CONTAINER: {
			XUpdateContainer xUpdateContainer = (XUpdateContainer) theEObject;
			T result = caseXUpdateContainer(xUpdateContainer);
			if (result == null)
				result = caseXSemanticsElement(xUpdateContainer);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XTRANSITION: {
			XTransition xTransition = (XTransition) theEObject;
			T result = caseXTransition(xTransition);
			if (result == null)
				result = caseXUpdateContainer(xTransition);
			if (result == null)
				result = caseXSemanticsElement(xTransition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XUPDATE: {
			XUpdate xUpdate = (XUpdate) theEObject;
			T result = caseXUpdate(xUpdate);
			if (result == null)
				result = caseXUpdateContainer(xUpdate);
			if (result == null)
				result = caseXSemanticsElement(xUpdate);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XADD_SPECIFICATION: {
			XAddSpecification xAddSpecification = (XAddSpecification) theEObject;
			T result = caseXAddSpecification(xAddSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XABSTRACT_TRIGGERING_UPDATE: {
			XAbstractTriggeringUpdate xAbstractTriggeringUpdate = (XAbstractTriggeringUpdate) theEObject;
			T result = caseXAbstractTriggeringUpdate(xAbstractTriggeringUpdate);
			if (result == null)
				result = caseXUpdate(xAbstractTriggeringUpdate);
			if (result == null)
				result = caseXUpdateContainer(xAbstractTriggeringUpdate);
			if (result == null)
				result = caseXSemanticsElement(xAbstractTriggeringUpdate);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XTRIGGERING_SPECIFICATION: {
			XTriggeringSpecification xTriggeringSpecification = (XTriggeringSpecification) theEObject;
			T result = caseXTriggeringSpecification(xTriggeringSpecification);
			if (result == null)
				result = caseXSemanticsElement(xTriggeringSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION: {
			XAdditionalTriggeringSpecification xAdditionalTriggeringSpecification = (XAdditionalTriggeringSpecification) theEObject;
			T result = caseXAdditionalTriggeringSpecification(xAdditionalTriggeringSpecification);
			if (result == null)
				result = caseXTriggeringSpecification(xAdditionalTriggeringSpecification);
			if (result == null)
				result = caseXSemanticsElement(xAdditionalTriggeringSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XTRIGGERED_UPDATE: {
			XTriggeredUpdate xTriggeredUpdate = (XTriggeredUpdate) theEObject;
			T result = caseXTriggeredUpdate(xTriggeredUpdate);
			if (result == null)
				result = caseXAbstractTriggeringUpdate(xTriggeredUpdate);
			if (result == null)
				result = caseXUpdate(xTriggeredUpdate);
			if (result == null)
				result = caseXUpdateContainer(xTriggeredUpdate);
			if (result == null)
				result = caseXSemanticsElement(xTriggeredUpdate);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION: {
			XOriginalTriggeringSpecification xOriginalTriggeringSpecification = (XOriginalTriggeringSpecification) theEObject;
			T result = caseXOriginalTriggeringSpecification(xOriginalTriggeringSpecification);
			if (result == null)
				result = caseXTriggeringSpecification(xOriginalTriggeringSpecification);
			if (result == null)
				result = caseXSemanticsElement(xOriginalTriggeringSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XUPDATED_OBJECT: {
			XUpdatedObject xUpdatedObject = (XUpdatedObject) theEObject;
			T result = caseXUpdatedObject(xUpdatedObject);
			if (result == null)
				result = caseXSemanticsElement(xUpdatedObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XUPDATED_FEATURE: {
			XUpdatedFeature xUpdatedFeature = (XUpdatedFeature) theEObject;
			T result = caseXUpdatedFeature(xUpdatedFeature);
			if (result == null)
				result = caseXSemanticsElement(xUpdatedFeature);
			if (result == null)
				result = caseXUpdatedTypedElement(xUpdatedFeature);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XVALUE: {
			XValue xValue = (XValue) theEObject;
			T result = caseXValue(xValue);
			if (result == null)
				result = caseXSemanticsElement(xValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT: {
			XUpdatedTypedElement xUpdatedTypedElement = (XUpdatedTypedElement) theEObject;
			T result = caseXUpdatedTypedElement(xUpdatedTypedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.XUPDATED_OPERATION: {
			XUpdatedOperation xUpdatedOperation = (XUpdatedOperation) theEObject;
			T result = caseXUpdatedOperation(xUpdatedOperation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SemanticsPackage.DUMMY: {
			DUmmy dUmmy = (DUmmy) theEObject;
			T result = caseDUmmy(dUmmy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XSemantics</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XSemantics</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXSemantics(XSemantics object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XUpdate Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XUpdate Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXUpdateContainer(XUpdateContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XTransition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XTransition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXTransition(XTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XUpdate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XUpdate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXUpdate(XUpdate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XAdd Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XAdd Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXAddSpecification(XAddSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XAbstract Triggering Update</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XAbstract Triggering Update</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXAbstractTriggeringUpdate(XAbstractTriggeringUpdate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XTriggering Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XTriggering Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXTriggeringSpecification(XTriggeringSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XAdditional Triggering Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XAdditional Triggering Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXAdditionalTriggeringSpecification(XAdditionalTriggeringSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XTriggered Update</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XTriggered Update</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXTriggeredUpdate(XTriggeredUpdate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XOriginal Triggering Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XOriginal Triggering Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXOriginalTriggeringSpecification(XOriginalTriggeringSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XUpdated Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XUpdated Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXUpdatedObject(XUpdatedObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XUpdated Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XUpdated Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXUpdatedFeature(XUpdatedFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XValue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XValue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXValue(XValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XUpdated Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XUpdated Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXUpdatedTypedElement(XUpdatedTypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XUpdated Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XUpdated Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXUpdatedOperation(XUpdatedOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DUmmy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DUmmy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDUmmy(DUmmy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XSemantics Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XSemantics Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXSemanticsElement(XSemanticsElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SemanticsSwitch
