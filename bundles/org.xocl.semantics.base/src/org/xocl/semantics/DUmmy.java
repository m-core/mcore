
package org.xocl.semantics;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DUmmy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.DUmmy#getText1 <em>Text1</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getDUmmy()
 * @model annotation="http://www.montages.com/mCore/MCore mName='DUmmy'"
 * @generated
 */

public interface DUmmy extends EObject {
	/**
	 * Returns the value of the '<em><b>Text1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text1</em>' attribute.
	 * @see #isSetText1()
	 * @see #unsetText1()
	 * @see #setText1(String)
	 * @see org.xocl.semantics.SemanticsPackage#getDUmmy_Text1()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='text1'"
	 * @generated
	 */
	String getText1();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.DUmmy#getText1 <em>Text1</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text1</em>' attribute.
	 * @see #isSetText1()
	 * @see #unsetText1()
	 * @see #getText1()
	 * @generated
	 */

	void setText1(String value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.DUmmy#getText1 <em>Text1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetText1()
	 * @see #getText1()
	 * @see #setText1(String)
	 * @generated
	 */
	void unsetText1();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.DUmmy#getText1 <em>Text1</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Text1</em>' attribute is set.
	 * @see #unsetText1()
	 * @see #getText1()
	 * @see #setText1(String)
	 * @generated
	 */
	boolean isSetText1();

} // DUmmy
