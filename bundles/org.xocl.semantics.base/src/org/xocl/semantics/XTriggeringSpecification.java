/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XTriggering Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XTriggeringSpecification#getFeatureOfTriggeringUpdateAnnotation <em>Feature Of Triggering Update Annotation</em>}</li>
 *   <li>{@link org.xocl.semantics.XTriggeringSpecification#getNameofTriggeringUpdateAnnotation <em>Nameof Triggering Update Annotation</em>}</li>
 *   <li>{@link org.xocl.semantics.XTriggeringSpecification#getTriggeringUpdate <em>Triggering Update</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXTriggeringSpecification()
 * @model abstract="true"
 * @generated
 */

public interface XTriggeringSpecification extends XSemanticsElement {
	/**
	 * Returns the value of the '<em><b>Feature Of Triggering Update Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Of Triggering Update Annotation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Of Triggering Update Annotation</em>' reference.
	 * @see org.xocl.semantics.SemanticsPackage#getXTriggeringSpecification_FeatureOfTriggeringUpdateAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if triggeringUpdate.oclIsUndefined()\n  then null\n  else triggeringUpdate.feature\nendif\n'"
	 * @generated
	 */
	EStructuralFeature getFeatureOfTriggeringUpdateAnnotation();

	/**
	 * Returns the value of the '<em><b>Nameof Triggering Update Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nameof Triggering Update Annotation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nameof Triggering Update Annotation</em>' attribute.
	 * @see #isSetNameofTriggeringUpdateAnnotation()
	 * @see #unsetNameofTriggeringUpdateAnnotation()
	 * @see #setNameofTriggeringUpdateAnnotation(String)
	 * @see org.xocl.semantics.SemanticsPackage#getXTriggeringSpecification_NameofTriggeringUpdateAnnotation()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Name of Triggering Update Annotation'"
	 * @generated
	 */
	String getNameofTriggeringUpdateAnnotation();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XTriggeringSpecification#getNameofTriggeringUpdateAnnotation <em>Nameof Triggering Update Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nameof Triggering Update Annotation</em>' attribute.
	 * @see #isSetNameofTriggeringUpdateAnnotation()
	 * @see #unsetNameofTriggeringUpdateAnnotation()
	 * @see #getNameofTriggeringUpdateAnnotation()
	 * @generated
	 */
	void setNameofTriggeringUpdateAnnotation(String value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTriggeringSpecification#getNameofTriggeringUpdateAnnotation <em>Nameof Triggering Update Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNameofTriggeringUpdateAnnotation()
	 * @see #getNameofTriggeringUpdateAnnotation()
	 * @see #setNameofTriggeringUpdateAnnotation(String)
	 * @generated
	 */
	void unsetNameofTriggeringUpdateAnnotation();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTriggeringSpecification#getNameofTriggeringUpdateAnnotation <em>Nameof Triggering Update Annotation</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Nameof Triggering Update Annotation</em>' attribute is set.
	 * @see #unsetNameofTriggeringUpdateAnnotation()
	 * @see #getNameofTriggeringUpdateAnnotation()
	 * @see #setNameofTriggeringUpdateAnnotation(String)
	 * @generated
	 */
	boolean isSetNameofTriggeringUpdateAnnotation();

	/**
	 * Returns the value of the '<em><b>Triggering Update</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggering Update</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggering Update</em>' reference.
	 * @see org.xocl.semantics.SemanticsPackage#getXTriggeringSpecification_TriggeringUpdate()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: semantics::XAbstractTriggeringUpdate = null in nl\n'"
	 * @generated
	 */
	XAbstractTriggeringUpdate getTriggeringUpdate();

} // XTriggeringSpecification
