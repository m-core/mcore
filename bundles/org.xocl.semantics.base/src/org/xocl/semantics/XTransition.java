/**
 */
package org.xocl.semantics;

import java.util.Date;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XTransition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XTransition#getExecuteAutomatically <em>Execute Automatically</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getExecuteNow <em>Execute Now</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getTriggeringUpdate <em>Triggering Update</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getUpdatedObject <em>Updated Object</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getDanglingRemovedObject <em>Dangling Removed Object</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getAllUpdates <em>All Updates</em>}</li>
 *   <li>{@link org.xocl.semantics.XTransition#getFocusObjects <em>Focus Objects</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXTransition()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'->\'\n'"
 * @generated
 */

public interface XTransition extends XUpdateContainer {
	/**
	 * Returns the value of the '<em><b>Execute Automatically</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execute Automatically</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execute Automatically</em>' attribute.
	 * @see #isSetExecuteAutomatically()
	 * @see #unsetExecuteAutomatically()
	 * @see #setExecuteAutomatically(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_ExecuteAutomatically()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getExecuteAutomatically();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XTransition#getExecuteAutomatically <em>Execute Automatically</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execute Automatically</em>' attribute.
	 * @see #isSetExecuteAutomatically()
	 * @see #unsetExecuteAutomatically()
	 * @see #getExecuteAutomatically()
	 * @generated
	 */
	void setExecuteAutomatically(Boolean value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTransition#getExecuteAutomatically <em>Execute Automatically</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecuteAutomatically()
	 * @see #getExecuteAutomatically()
	 * @see #setExecuteAutomatically(Boolean)
	 * @generated
	 */
	void unsetExecuteAutomatically();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTransition#getExecuteAutomatically <em>Execute Automatically</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Execute Automatically</em>' attribute is set.
	 * @see #unsetExecuteAutomatically()
	 * @see #getExecuteAutomatically()
	 * @see #setExecuteAutomatically(Boolean)
	 * @generated
	 */
	boolean isSetExecuteAutomatically();

	/**
	 * Returns the value of the '<em><b>Execute Now</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execute Now</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execute Now</em>' attribute.
	 * @see #setExecuteNow(Boolean)
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_ExecuteNow()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getExecuteNow();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XTransition#getExecuteNow <em>Execute Now</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execute Now</em>' attribute.
	 * @see #getExecuteNow()
	 * @generated
	 */
	void setExecuteNow(Boolean value);

	/**
	 * Returns the value of the '<em><b>Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Time</em>' attribute.
	 * @see #isSetExecutionTime()
	 * @see #unsetExecutionTime()
	 * @see #setExecutionTime(Date)
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_ExecutionTime()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Date getExecutionTime();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XTransition#getExecutionTime <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Time</em>' attribute.
	 * @see #isSetExecutionTime()
	 * @see #unsetExecutionTime()
	 * @see #getExecutionTime()
	 * @generated
	 */
	void setExecutionTime(Date value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTransition#getExecutionTime <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecutionTime()
	 * @see #getExecutionTime()
	 * @see #setExecutionTime(Date)
	 * @generated
	 */
	void unsetExecutionTime();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTransition#getExecutionTime <em>Execution Time</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Execution Time</em>' attribute is set.
	 * @see #unsetExecutionTime()
	 * @see #getExecutionTime()
	 * @see #setExecutionTime(Date)
	 * @generated
	 */
	boolean isSetExecutionTime();

	/**
	 * Returns the value of the '<em><b>Triggering Update</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XTriggeredUpdate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggering Update</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggering Update</em>' containment reference list.
	 * @see #isSetTriggeringUpdate()
	 * @see #unsetTriggeringUpdate()
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_TriggeringUpdate()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<XTriggeredUpdate> getTriggeringUpdate();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTransition#getTriggeringUpdate <em>Triggering Update</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTriggeringUpdate()
	 * @see #getTriggeringUpdate()
	 * @generated
	 */
	void unsetTriggeringUpdate();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTransition#getTriggeringUpdate <em>Triggering Update</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Triggering Update</em>' containment reference list is set.
	 * @see #unsetTriggeringUpdate()
	 * @see #getTriggeringUpdate()
	 * @generated
	 */
	boolean isSetTriggeringUpdate();

	/**
	 * Returns the value of the '<em><b>Updated Object</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdatedObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Updated Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Updated Object</em>' containment reference list.
	 * @see #isSetUpdatedObject()
	 * @see #unsetUpdatedObject()
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_UpdatedObject()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<XUpdatedObject> getUpdatedObject();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTransition#getUpdatedObject <em>Updated Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUpdatedObject()
	 * @see #getUpdatedObject()
	 * @generated
	 */
	void unsetUpdatedObject();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTransition#getUpdatedObject <em>Updated Object</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Updated Object</em>' containment reference list is set.
	 * @see #unsetUpdatedObject()
	 * @see #getUpdatedObject()
	 * @generated
	 */
	boolean isSetUpdatedObject();

	/**
	 * Returns the value of the '<em><b>Dangling Removed Object</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dangling Removed Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dangling Removed Object</em>' containment reference list.
	 * @see #isSetDanglingRemovedObject()
	 * @see #unsetDanglingRemovedObject()
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_DanglingRemovedObject()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<EObject> getDanglingRemovedObject();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTransition#getDanglingRemovedObject <em>Dangling Removed Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDanglingRemovedObject()
	 * @see #getDanglingRemovedObject()
	 * @generated
	 */
	void unsetDanglingRemovedObject();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTransition#getDanglingRemovedObject <em>Dangling Removed Object</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Dangling Removed Object</em>' containment reference list is set.
	 * @see #unsetDanglingRemovedObject()
	 * @see #getDanglingRemovedObject()
	 * @generated
	 */
	boolean isSetDanglingRemovedObject();

	/**
	 * Returns the value of the '<em><b>All Updates</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Updates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Updates</em>' reference list.
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_AllUpdates()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.triggeringUpdate.oclAsType(XUpdate)->union(self.triggeringUpdate.allOwnedUpdates)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<XUpdate> getAllUpdates();

	/**
	 * Returns the value of the '<em><b>Focus Objects</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdatedObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Focus Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Focus Objects</em>' reference list.
	 * @see #isSetFocusObjects()
	 * @see #unsetFocusObjects()
	 * @see org.xocl.semantics.SemanticsPackage#getXTransition_FocusObjects()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Focus Objects '"
	 * @generated
	 */
	EList<XUpdatedObject> getFocusObjects();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XTransition#getFocusObjects <em>Focus Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFocusObjects()
	 * @see #getFocusObjects()
	 * @generated
	 */
	void unsetFocusObjects();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XTransition#getFocusObjects <em>Focus Objects</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Focus Objects</em>' reference list is set.
	 * @see #unsetFocusObjects()
	 * @see #getFocusObjects()
	 * @generated
	 */
	boolean isSetFocusObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate executeNow$Update(Boolean trg);

	/**
	 * @generated NOT 	
	 * @return
	 */
	Command interpreteTransitionAsCommand();

	/**
	 * @generated NOT
	 */
	public static final String FOCUSCOMMAND = "Focus";

} // XTransition
