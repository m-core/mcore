/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui;

/**
 * @author Max Stepanov
 *
 */
public enum Zoom {

	SMALLER("-2", "-2", 0.8), //$NON-NLS-1$ //$NON-NLS-2$
	NORMAL("", "", 1.0), //$NON-NLS-1$ //$NON-NLS-2$
	LARGER("+4", "+4", 18.0/13.0), //$NON-NLS-1$ //$NON-NLS-2$
	LARGEST("+10", "+8", 24.0/13.0); //$NON-NLS-1$ //$NON-NLS-2$
	
	private String defaultFont;
	private String headerFont;
	private double columnWidthRatio;
	
	Zoom(String defaultFont, String headerFont, double columnWidthRatio) {
		this.defaultFont = defaultFont;
		this.headerFont = headerFont;
		this.columnWidthRatio = columnWidthRatio;
	}

	/**
	 * @return the defaultFont
	 */
	public String getDefaultFont() {
		return defaultFont;
	}

	/**
	 * @return the headerFont
	 */
	public String getHeaderFont() {
		return headerFont;
	}

	/**
	 * @return the columnWidthRatio
	 */
	public double getColumnWidthRatio() {
		return columnWidthRatio;
	}

	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		String name = name().toLowerCase();
		return Character.toUpperCase(name.charAt(0))+name.substring(1);
	}
}
