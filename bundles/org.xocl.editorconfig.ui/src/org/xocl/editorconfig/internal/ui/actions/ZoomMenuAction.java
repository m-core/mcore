/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.xocl.editorconfig.internal.ui.Zoom;
import org.xocl.editorconfig.ui.IEditorConfigViewer;

/**
 * @author Max Stepanov
 *
 */
public final class ZoomMenuAction extends UpdatableAction {

	private static final String ZOOM_MENU_ACTION_ID = "zoom_menu_action_id"; //$NON-NLS-1$
	
	private final IEditorConfigViewer editorConfigViewer;
	private MenuManager menuManager;
	private final List<ZoomAction> actions = new ArrayList<ZoomAction>();

	public ZoomMenuAction(IEditorConfigViewer editorConfigViewer) {
		super(Messages.ZoomMenuAction_Text, IAction.AS_DROP_DOWN_MENU);
		setId(ZOOM_MENU_ACTION_ID);
		this.editorConfigViewer = editorConfigViewer;
		setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.ui", "icons/full/obj16/font.gif")); //$NON-NLS-1$ //$NON-NLS-2$
		setMenuCreator(new IMenuCreator() {

			public Menu getMenu(Menu parent) {
				return getMenuManager().createContextMenu(parent.getParent());
			}

			public Menu getMenu(Control parent) {
				return getMenuManager().createContextMenu(parent);
			}

			public void dispose() {
				if (menuManager != null) {
					menuManager.removeAll();
					menuManager.dispose();
					menuManager = null;
				}
			}
		});
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.internal.ui.actions.IUpdate#update()
	 */
	public void update() {
		for (ZoomAction action : actions) {
			action.update();
		}
	}

	private MenuManager getMenuManager() {
		if (menuManager == null) {
			menuManager = new MenuManager();
			createZoomActions();
			for (ZoomAction action : actions) {
				menuManager.add(action);
			}
			menuManager.addMenuListener(new IMenuListener() {
				public void menuAboutToShow(IMenuManager manager) {
					update();
				}
			});
		}
		return menuManager;
	}
	
	protected void createZoomActions() {
		if (!actions.isEmpty()) {
			return;
		}
		for (Zoom zoom : Zoom.values()) {
			actions.add(new ZoomAction(zoom.toString(), zoom));
			
		}
	}
	
	private class ZoomAction extends UpdatableAction {
		
		private final Zoom zoom;
		
		public ZoomAction(String text, Zoom zoom) {
			super(text, IAction.AS_RADIO_BUTTON);
			this.zoom = zoom;
		}

		@Override
		public void run() {
			if (isChecked()) {
				editorConfigViewer.setZoom(zoom);
			}
		}

		public void update() {
			setChecked(editorConfigViewer.getZoom() == zoom);
		}
	}
}