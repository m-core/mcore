/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui.actions;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.xocl.editorconfig.internal.ui.Activator;
import org.xocl.editorconfig.ui.IEditorConfigViewer;

/**
 * @author Max Stepanov
 *
 */
public final class ReloadEditorConfigAction extends UpdatableAction {
	
	private final IEditorConfigViewer editorConfigViewer;
	
	public ReloadEditorConfigAction(IEditorConfigViewer editorConfigViewer) {
		super(Messages.ReloadEditorConfigAction_Text, AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID, "/icons/full/etool16/refresh.png")); //$NON-NLS-1$
		this.editorConfigViewer = editorConfigViewer;
	}

	@Override
	public void run() {
		editorConfigViewer.reloadEditorConfig();
	}

	public void update() {
		setEnabled(editorConfigViewer.getEditorConfigURI() != null);
	}
}