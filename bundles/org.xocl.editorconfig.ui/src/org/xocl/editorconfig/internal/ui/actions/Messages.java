package org.xocl.editorconfig.internal.ui.actions;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.editorconfig.internal.ui.actions.messages"; //$NON-NLS-1$
	public static String RefreshAllAction_Text;
	public static String RefreshAllAction_Tooltip;
	public static String ReloadEditorConfigAction_Text;
	public static String SelectEditorConfigAction_Text;
	public static String ZoomMenuAction_Text;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
