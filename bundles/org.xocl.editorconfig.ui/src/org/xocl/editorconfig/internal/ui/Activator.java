/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.internal.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Max Stepanov
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.xocl.editorconfig.ui"; //$NON-NLS-1$
	
	public static final String REFRESH_ALL_MODE_BUTTON_IMAGE_ID =  "RefreshAllAction.REFRESH_ALL_MODE_BUTTON_IMAGE_ID"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);
		initRefreshAllModeButtonImage(reg);
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static void log(Throwable e) {
		log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, e.getLocalizedMessage(), e));
	}

	public static void log(String msg) {
		log(new Status(IStatus.INFO, PLUGIN_ID, IStatus.OK, msg, null)); 
	}

	public static void log(IStatus status) {
		getDefault().getLog().log(status);
	}

	public static URI getEditorConfigForEditorID(String editorId) {
        return EditorConfigPartAssociations.getInstance().getEditorConfig(editorId);
	}
	
	private static void initRefreshAllModeButtonImage(ImageRegistry imageRegistry) {
    	ImageDescriptor baseImageDescriptor = imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/full/etool16/refresh_nav.gif"); //$NON-NLS-1$ //$NON-NLS-2$
    	if (baseImageDescriptor == null) {
    		return;
    	}
    	Image baseImage = baseImageDescriptor.createImage();
    	ImageDescriptor lockDecoratorId = imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/full/ovr16/lock.gif"); //$NON-NLS-1$
    	DecorationOverlayIcon decoratedImageDescriptor = new DecorationOverlayIcon(baseImage, lockDecoratorId, IDecoration.BOTTOM_RIGHT);
    	Image decoratedImage = decoratedImageDescriptor.createImage();
    	baseImage.dispose();
    	imageRegistry.put(REFRESH_ALL_MODE_BUTTON_IMAGE_ID, decoratedImage);
	}

}
