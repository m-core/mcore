/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

/**
 * @author Max Stepanov
 *
 */
public abstract class UpdatableAction extends Action implements IUpdate {

	/**
	 * @param text
	 */
	public UpdatableAction(String text) {
		super(text);
	}

	/**
	 * @param text
	 * @param image
	 */
	public UpdatableAction(String text, ImageDescriptor image) {
		super(text, image);
	}

	/**
	 * @param text
	 * @param style
	 */
	public UpdatableAction(String text, int style) {
		super(text, style);
	}

}
