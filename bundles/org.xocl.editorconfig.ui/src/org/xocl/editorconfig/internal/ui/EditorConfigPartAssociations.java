/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.internal.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;

/**
 * @author max
 *
 */
/* package */ class EditorConfigPartAssociations {

	private static final String EXTENSION_POINT_ID = Activator.PLUGIN_ID + ".editorConfigPartAssociations"; //$NON-NLS-1$
	private static final String TAG_ASSOCIATION = "editorConfigPartAssociation"; //$NON-NLS-1$
	private static final String ATT_EDITOR_ID = "editorID"; //$NON-NLS-1$
	private static final String ATT_CONFIG = "config"; //$NON-NLS-1$
	
	private static EditorConfigPartAssociations instance = null;
	
	private Map<String, List<URI>> associations = new HashMap<String, List<URI>>();
	
	/**
	 * 
	 */
	private EditorConfigPartAssociations() {
		readRegistry();
	}
	
	public static EditorConfigPartAssociations getInstance() {
		if (instance == null) {
			instance = new EditorConfigPartAssociations();
		}
		return instance;
	}
	
	private void readRegistry() {
		IConfigurationElement[] elements = Platform.getExtensionRegistry()
							.getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (int i = 0; i < elements.length; ++i) {
			readElement(elements[i]);
		}
	}
	
	private void readElement(IConfigurationElement element) {
		if (!element.getName().equals(TAG_ASSOCIATION)) {
			return;
		}
		
		String editorId = element.getAttribute(ATT_EDITOR_ID);
		if (editorId == null || editorId.length() == 0) {
			return;
		}
		
		String config = element.getAttribute(ATT_CONFIG);
		if (config == null || config.length() == 0) {
			return;
		}
		URI configURI = URI.createPlatformPluginURI(element.getNamespaceIdentifier()+"/"+config, true); //$NON-NLS-1$
		
		List<URI> list = associations.get(editorId);
		if (list == null) {
			list = new ArrayList<URI>();
			associations.put(editorId, list);
		}
		list.add(configURI);
	}
	
	public URI getEditorConfig(String editorId) {
		List<URI> list = associations.get(editorId);
		if (list != null) {
			return list.get(0);
		}
		return null;
	}

}
