/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.internal.ui.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.emf.common.ui.dialogs.WorkspaceResourceDialog;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * @author Max Stepanov
 *
 */
public class SelectEditorConfigDialog extends ResourceDialog {

	private static final String FILE_EXTENSION = "editorconfig"; //$NON-NLS-1$
	
	private class ResourceVisitor implements IResourceVisitor {
		private boolean found = false;
		
		public boolean visit(IResource resource) throws CoreException {
			if (resource instanceof IFile) {
				if (FILE_EXTENSION.equals(((IFile) resource).getFileExtension())) {
					found = true;
				}
				return false;
			}
			return !found;
		}
		
		public boolean isFound() {
			return found;
		}
	}
	
	/**
	 * @param parent
	 */
	public SelectEditorConfigDialog(Shell parent) {
		super(parent, Messages.SelectEditorConfigDialog_Title, SWT.OPEN | SWT.SINGLE);
	}
	
	private List<ViewerFilter> getViewerFilters() {
		List<ViewerFilter> list = new ArrayList<ViewerFilter>();
		list.add(new ViewerFilter() {
			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element) {
				if (element instanceof IFile) {
					return FILE_EXTENSION.equals(((IFile) element).getFileExtension());
				} else if (element instanceof IFolder || element instanceof IProject) {
					ResourceVisitor visitor = new ResourceVisitor();
					try {
						((IResource) element).accept(visitor);
						return visitor.isFound();
					} catch (CoreException e) {
					}
				}
				return true;
			}
		});
		return list;
	}
	
	private String[] getFilterExtensions() {
		return new String[] { "*."+FILE_EXTENSION }; //$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.common.ui.dialogs.ResourceDialog#prepareBrowseFileSystemButton(org.eclipse.swt.widgets.Button)
	 */
	@Override
	protected void prepareBrowseFileSystemButton(Button browseFileSystemButton) {
		browseFileSystemButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				FileDialog fileDialog = new FileDialog(getShell(), style);
				fileDialog.setFilterExtensions(getFilterExtensions());
				if (fileDialog.open() == null) {
					return;
				}

				String filterPath = fileDialog.getFilterPath();
				String fileName = fileDialog.getFileName();
				if (fileName != null) {
					uriField.setText(URI.createFileURI(
							filterPath + File.separator + fileName).toString());
				}
			}
		});
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.common.ui.dialogs.ResourceDialog#prepareBrowseWorkspaceButton(org.eclipse.swt.widgets.Button)
	 */
	@Override
	protected void prepareBrowseWorkspaceButton(Button browseWorkspaceButton) {
		browseWorkspaceButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				IFile file = null;
				IFile[] files = WorkspaceResourceDialog.openFileSelection(getShell(), null, null,
								false, null, getViewerFilters());
				if (files.length != 0) {
					file = files[0];
				}

				if (file != null) {
					uriField.setText(URI.createPlatformResourceURI(
								file.getFullPath().toString(), true).toString());
				}
			}
		});      
	}

}
