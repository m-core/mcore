/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.internal.ui.preferences;

/**
 * Constant definitions for plug-in preferences
 * 
 * @author Max Stepanov
 */
public class PreferenceConstants {

	public static final String P_PATH = "pathPreference"; //$NON-NLS-1$

	public static final String P_BOOLEAN = "booleanPreference"; //$NON-NLS-1$

	public static final String P_CHOICE = "choicePreference"; //$NON-NLS-1$

	public static final String P_STRING = "stringPreference"; //$NON-NLS-1$
	
}
