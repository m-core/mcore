/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.ui;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.ITableItemFontProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.emf.edit.provider.IWrapperItemProvider;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditProviderCell;
import org.xocl.editorconfig.RowFeatureCell;

/**
 * @author Max Stepanov
 *
 */
/* package */ class TableConfigLabelProvider implements ITableItemLabelProvider, ITableItemFontProvider {

	private static final String EMPTY_STRING = ""; //$NON-NLS-1$
	protected static final String DISPLAY_DEFAULT_COLUMN = new String();

	private TableConfigDescriptor tableConfig;
	private AdapterFactory adapterFactory;
	private int columnCount = 0;
	private boolean displayDefaultColumn = true;
	private int shift;
	
	/**
	 * @param tableConfig
	 */
	public TableConfigLabelProvider(TableConfigDescriptor tableConfig, AdapterFactory adapterFactory) {
		this.tableConfig = tableConfig;
		this.adapterFactory = adapterFactory;
		if (tableConfig != null) {
			columnCount = tableConfig.getColumn().size();
			displayDefaultColumn = tableConfig.isDisplayDefaultColumn();
		}
		shift = displayDefaultColumn ? 1 : 1 /* 0 */;
	}
	
	/**
	 * @return the tableConfig
	 */
	/* package */ TableConfigDescriptor getTableConfig() {
		return tableConfig;
	}

	private IItemPropertyDescriptor getItemPropertyDescriptor(EObject eObject, CellConfig cellConfig) {
		if (cellConfig instanceof RowFeatureCell) {
			EStructuralFeature eFeature = ((RowFeatureCell) cellConfig).getFeature();
			if (eFeature != null) {
			      IItemPropertySource itemPropertySource = (IItemPropertySource)adapterFactory.adapt(eObject, IItemPropertySource.class);
			      if (itemPropertySource != null) {
			    	  return itemPropertySource.getPropertyDescriptor(eObject, eFeature);
			      }
			}
		}
		return null;
	}
	
	/*
	 * Returns Cellconfig for object c from index columnindex
	 */
	public CellConfig getCell(Object object, int columnIndex)
	{
		EList<ColumnConfig> column = tableConfig.getColumn();
		if(!column.isEmpty() && column.size() > columnIndex)
		return column.get(columnIndex).getCellConfig(((EObject) object).eClass());
		
		return null;

	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemLabelProvider#getColumnText(java.lang.Object, int)
	 */
	public String getColumnText(Object object, int columnIndex) {
		if (object instanceof IWrapperItemProvider) {
			object = ((IWrapperItemProvider)object).getValue();
		}
		if (object instanceof ITableItemLabelProvider) {
			return ((ITableItemLabelProvider) object).getColumnText(object, columnIndex);
		}
		if (columnIndex == 0 && (displayDefaultColumn || object instanceof Resource)) {
			return DISPLAY_DEFAULT_COLUMN;
		}
		columnIndex -= shift;
		if (0 <= columnIndex && columnIndex < columnCount) {
			if (object instanceof EObject) {
				EObject eObject = (EObject) object;
				CellConfig cellConfig = tableConfig.getColumn().get(columnIndex).getCellConfig(eObject.eClass());

				IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor(eObject, cellConfig);
				if (itemPropertyDescriptor != null) {
					IItemLabelProvider itemLabelProvider = itemPropertyDescriptor.getLabelProvider(eObject);
					return itemLabelProvider.getText(itemPropertyDescriptor.getPropertyValue(eObject));
				}
				if (cellConfig != null) {
					if (cellConfig instanceof EditProviderCell) {
						return DISPLAY_DEFAULT_COLUMN;
					}
					Object value = cellConfig.getCellValue(eObject);
					return String.valueOf(value);
				}
			}
		}
		return EMPTY_STRING;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	public Object getColumnImage(Object object, int columnIndex) {
		if (object instanceof IWrapperItemProvider) {
			object = ((IWrapperItemProvider)object).getValue();
		}
		if (object instanceof ITableItemLabelProvider) {
			return ((ITableItemLabelProvider) object).getColumnImage(object, columnIndex);
		}
		if (columnIndex == 0) {
			return DISPLAY_DEFAULT_COLUMN;
		}
		columnIndex -= shift;
		if (columnIndex < columnCount) {
			if (object instanceof EObject) {
				EObject eObject = (EObject) object;
				CellConfig cellConfig = tableConfig.getColumn().get(columnIndex).getCellConfig(eObject.eClass());
				if (cellConfig instanceof EditProviderCell) {
					return DISPLAY_DEFAULT_COLUMN;
				}
				IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor(eObject, cellConfig);
				if (itemPropertyDescriptor != null) {
					IItemLabelProvider itemLabelProvider = itemPropertyDescriptor.getLabelProvider(eObject);
					return itemLabelProvider.getImage(itemPropertyDescriptor.getPropertyValue(eObject));
				}
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemFontProvider#getFont(java.lang.Object, int)
	 */
	public Object getFont(Object object, int columnIndex) {
		if (object instanceof IWrapperItemProvider) {
			object = ((IWrapperItemProvider)object).getValue();
		}
		if (object instanceof ITableItemFontProvider) {
			return ((ITableItemFontProvider) object).getFont(object, columnIndex);
		}
		if (columnIndex == 0 && (displayDefaultColumn || object instanceof Resource)) {
			return null;
		}
		columnIndex -= shift;
		if (0 <= columnIndex && columnIndex < columnCount) {
			if (object instanceof EObject) {
				EObject eObject = (EObject) object;
				CellConfig cellConfig = tableConfig.getColumn().get(columnIndex).getCellConfig(eObject.eClass());

				if (cellConfig != null) {
					return cellConfig.getCellFont();
				}
			}
		}
		return null;
	}		
}