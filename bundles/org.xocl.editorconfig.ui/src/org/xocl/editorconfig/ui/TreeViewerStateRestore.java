package org.xocl.editorconfig.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

public class TreeViewerStateRestore {

	private final TreeSelection myOriginalSelection;
	private final AdapterFactoryEditingDomain myDomain;
	private final TreePath[] myOriginalExpansion;

	public static TreeViewerStateRestore forDomainViewer(
			AdapterFactoryEditingDomain domain, Viewer viewer) {
		if (viewer instanceof TreeViewer) {
			return new TreeViewerStateRestore(domain, (TreeViewer) viewer);
		}
		return null;
	}

	public TreeViewerStateRestore(AdapterFactoryEditingDomain domain,
			TreeViewer viewer) {
		this.myDomain = domain;
		myOriginalSelection = (TreeSelection) viewer.getSelection();
		myOriginalExpansion = viewer.getExpandedTreePaths();
	}

	public void restoreViewer(Viewer viewer) {
		if (isDisposed(viewer)) {
			return;
		}
		try {
			viewer.getControl().setRedraw(false);
			if (viewer instanceof TreeViewer) {
				restoreExpansion((TreeViewer) viewer);
			}
			restoreSelection(viewer);
		} finally {
			viewer.getControl().setRedraw(true);
		}
	}

	private static boolean isDisposed(Viewer viewer) {
		return viewer.getControl() == null || viewer.getControl().isDisposed();
	}

	public void restoreExpansion(TreeViewer viewer) {
		viewer.refresh(true);
		TreePath[] resolved = resolveTreePaths(myOriginalExpansion);

		//int maxLevel = -1;
		//for (int i = 0; i < resolved.length; i++) {
		//	maxLevel = Math.max(maxLevel, resolved[i].getSegmentCount());
		//}
		//if (maxLevel >= 0) {
			//viewer.expandToLevel(maxLevel);
			viewer.setExpandedTreePaths(resolved);
		//}
	}
	
	public void restoreSelection(ISelectionProvider viewer) {
		viewer.setSelection(resolveTreeSelection(myOriginalSelection));
	}

	private TreeSelection resolveTreeSelection(TreeSelection selection) {
		TreePath[] resolvedPaths = resolveTreePaths(selection.getPaths());
		if (resolvedPaths.length == 0) {
			return TreeSelection.EMPTY;
		}
		return new TreeSelection(resolvedPaths, selection.getElementComparer());
	}

	private TreePath[] resolveTreePaths(TreePath[] paths) {
		List<TreePath> resolvedPaths = new ArrayList<TreePath>(paths.length);
		for (TreePath next : paths) {
			TreePath resolved = resolveTreePath(next);
			if (resolved != null) {
				resolvedPaths.add(resolved);
			}
		}
		return (TreePath[]) resolvedPaths.toArray(//
				new TreePath[resolvedPaths.size()]);
	}

	private TreePath resolveTreePath(TreePath path) {
		Object[] resolvedSegments = new Object[path.getSegmentCount()];
		for (int i = 0; i < resolvedSegments.length; i++) {
			Object next = path.getSegment(i);
			if (next instanceof EObject) {
				EObject resolved = resolveEObject((EObject) next);
				if (resolved == null || resolved.eIsProxy()) {
					// the whole path can't be resolved
					return null;
				}
				next = resolved;
			}
			resolvedSegments[i] = next;
		}
		return new TreePath(resolvedSegments);
	}

	private EObject resolveEObject(EObject eObject) {
		if (!eObject.eIsProxy()) {
			return eObject;
		}
		return EcoreUtil.resolve(eObject, myDomain.getResourceSet());
	}

}
