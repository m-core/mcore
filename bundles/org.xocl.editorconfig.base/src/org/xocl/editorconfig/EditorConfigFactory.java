/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigFactory.java,v 1.1 2008/09/02 13:07:16 stepanovxocl Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.xocl.editorconfig.EditorConfigPackage
 * @generated
 */
public interface EditorConfigFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EditorConfigFactory eINSTANCE = org.xocl.editorconfig.impl.EditorConfigFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Editor Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Editor Config</em>'.
	 * @generated
	 */
	EditorConfig createEditorConfig();

	/**
	 * Returns a new object of class '<em>Table Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Config</em>'.
	 * @generated
	 */
	TableConfig createTableConfig();

	/**
	 * Returns a new object of class '<em>Column Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Column Config</em>'.
	 * @generated
	 */
	ColumnConfig createColumnConfig();

	/**
	 * Returns a new object of class '<em>Row Feature Cell</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Row Feature Cell</em>'.
	 * @generated
	 */
	RowFeatureCell createRowFeatureCell();

	/**
	 * Returns a new object of class '<em>Ocl Cell</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ocl Cell</em>'.
	 * @generated
	 */
	OclCell createOclCell();

	/**
	 * Returns a new object of class '<em>Edit Provider Cell</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Edit Provider Cell</em>'.
	 * @generated
	 */
	EditProviderCell createEditProviderCell();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EditorConfigPackage getEditorConfigPackage();

} //EditorConfigFactory
