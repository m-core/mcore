/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigSwitch.java,v 1.6 2010/01/03 17:44:38 xocl.kutterxocl Exp $
 */
package org.xocl.editorconfig.util;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditProviderCell;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.OclCell;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xocl.editorconfig.EditorConfigPackage
 * @generated
 */
public class EditorConfigSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EditorConfigPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigSwitch() {
		if (modelPackage == null) {
			modelPackage = EditorConfigPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		} else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(
					eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case EditorConfigPackage.EDITOR_CONFIG: {
			EditorConfig editorConfig = (EditorConfig) theEObject;
			T result = caseEditorConfig(editorConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY: {
			@SuppressWarnings("unchecked")
			Map.Entry<EClass, TableConfig> eClassToTableConfigMapEntry = (Map.Entry<EClass, TableConfig>) theEObject;
			T result = caseEClassToTableConfigMapEntry(eClassToTableConfigMapEntry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY: {
			@SuppressWarnings("unchecked")
			Map.Entry<EReference, TableConfig> eReferenceToTableConfigMapEntry = (Map.Entry<EReference, TableConfig>) theEObject;
			T result = caseEReferenceToTableConfigMapEntry(eReferenceToTableConfigMapEntry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.TABLE_CONFIG: {
			TableConfig tableConfig = (TableConfig) theEObject;
			T result = caseTableConfig(tableConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.COLUMN_CONFIG: {
			ColumnConfig columnConfig = (ColumnConfig) theEObject;
			T result = caseColumnConfig(columnConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY: {
			@SuppressWarnings("unchecked")
			Map.Entry<EClass, CellConfig> eClassToCellConfigMapEntry = (Map.Entry<EClass, CellConfig>) theEObject;
			T result = caseEClassToCellConfigMapEntry(eClassToCellConfigMapEntry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.CELL_CONFIG: {
			CellConfig cellConfig = (CellConfig) theEObject;
			T result = caseCellConfig(cellConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.ROW_FEATURE_CELL: {
			RowFeatureCell rowFeatureCell = (RowFeatureCell) theEObject;
			T result = caseRowFeatureCell(rowFeatureCell);
			if (result == null)
				result = caseCellConfig(rowFeatureCell);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.OCL_CELL: {
			OclCell oclCell = (OclCell) theEObject;
			T result = caseOclCell(oclCell);
			if (result == null)
				result = caseCellConfig(oclCell);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EditorConfigPackage.EDIT_PROVIDER_CELL: {
			EditProviderCell editProviderCell = (EditProviderCell) theEObject;
			T result = caseEditProviderCell(editProviderCell);
			if (result == null)
				result = caseCellConfig(editProviderCell);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Editor Config</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Editor Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEditorConfig(EditorConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass To Table Config Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass To Table Config Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassToTableConfigMapEntry(
			Map.Entry<EClass, TableConfig> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EReference To Table Config Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EReference To Table Config Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEReferenceToTableConfigMapEntry(
			Map.Entry<EReference, TableConfig> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Config</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableConfig(TableConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Column Config</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Column Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseColumnConfig(ColumnConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass To Cell Config Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass To Cell Config Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassToCellConfigMapEntry(Map.Entry<EClass, CellConfig> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cell Config</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cell Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCellConfig(CellConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Row Feature Cell</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Row Feature Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRowFeatureCell(RowFeatureCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ocl Cell</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ocl Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOclCell(OclCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Edit Provider Cell</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Edit Provider Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEditProviderCell(EditProviderCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //EditorConfigSwitch
