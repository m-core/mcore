/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigXMLProcessor.java,v 1.1 2009/02/16 12:21:04 stepanovxocl Exp $
 */
package org.xocl.editorconfig.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.xocl.editorconfig.EditorConfigPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EditorConfigXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		EditorConfigPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the EditorConfigResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION,
					new EditorConfigResourceFactoryImpl());
			registrations.put(STAR_EXTENSION,
					new EditorConfigResourceFactoryImpl());
		}
		return registrations;
	}

} //EditorConfigXMLProcessor
