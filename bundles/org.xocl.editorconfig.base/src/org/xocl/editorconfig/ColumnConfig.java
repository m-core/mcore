/**
 * <copyright>
 * </copyright>
 *
 * $Id: ColumnConfig.java,v 1.4 2009/09/21 10:19:19 xocl.stepanovxocl Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.ColumnConfig#getName <em>Name</em>}</li>
 *   <li>{@link org.xocl.editorconfig.ColumnConfig#getLabel <em>Label</em>}</li>
 *   <li>{@link org.xocl.editorconfig.ColumnConfig#getWidth <em>Width</em>}</li>
 *   <li>{@link org.xocl.editorconfig.ColumnConfig#getFont <em>Font</em>}</li>
 *   <li>{@link org.xocl.editorconfig.ColumnConfig#getCellConfigMap <em>Cell Config Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getColumnConfig()
 * @model
 * @generated
 */
public interface ColumnConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getColumnConfig_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.ColumnConfig#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getColumnConfig_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.ColumnConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getColumnConfig_Width()
	 * @model
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.ColumnConfig#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);

	/**
	 * Returns the value of the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Font</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font</em>' attribute.
	 * @see #setFont(URI)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getColumnConfig_Font()
	 * @model dataType="org.xocl.editorconfig.Font"
	 * @generated
	 */
	URI getFont();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.ColumnConfig#getFont <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font</em>' attribute.
	 * @see #getFont()
	 * @generated
	 */
	void setFont(URI value);

	/**
	 * Returns the value of the '<em><b>Cell Config Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type {@link org.xocl.editorconfig.CellConfig},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Config Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Config Map</em>' map.
	 * @see org.xocl.editorconfig.EditorConfigPackage#getColumnConfig_CellConfigMap()
	 * @model mapType="org.xocl.editorconfig.EClassToCellConfigMapEntry<org.eclipse.emf.ecore.EClass, org.xocl.editorconfig.CellConfig>"
	 * @generated
	 */
	EMap<EClass, CellConfig> getCellConfigMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model eClassRequired="true"
	 * @generated
	 */
	CellConfig getCellConfig(EClass eClass);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rowRequired="true"
	 * @generated
	 */
	Object getColumnValue(EObject row);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.xocl.editorconfig.Font"
	 * @generated
	 */
	URI getColumnFont();

} // ColumnConfig
