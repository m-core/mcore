/**
 * <copyright>
 * </copyright>
 *
 * $Id: OclCell.java,v 1.1 2008/09/02 13:07:16 stepanovxocl Exp $
 */
package org.xocl.editorconfig;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ocl Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.OclCell#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.xocl.editorconfig.OclCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getOclCell()
 * @model
 * @generated
 */
public interface OclCell extends CellConfig {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' attribute.
	 * @see #setExpression(String)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getOclCell_Expression()
	 * @model required="true"
	 * @generated
	 */
	String getExpression();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.OclCell#getExpression <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' attribute.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(String value);

	/**
	 * Returns the value of the '<em><b>Cell Locate Behavior</b></em>' attribute.
	 * The default value is <code>"Selection"</code>.
	 * The literals are from the enumeration {@link org.xocl.editorconfig.CellLocateBehaviorOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Locate Behavior</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Locate Behavior</em>' attribute.
	 * @see org.xocl.editorconfig.CellLocateBehaviorOption
	 * @see #setCellLocateBehavior(CellLocateBehaviorOption)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getOclCell_CellLocateBehavior()
	 * @model default="Selection"
	 * @generated
	 */
	CellLocateBehaviorOption getCellLocateBehavior();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.OclCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cell Locate Behavior</em>' attribute.
	 * @see org.xocl.editorconfig.CellLocateBehaviorOption
	 * @see #getCellLocateBehavior()
	 * @generated
	 */
	void setCellLocateBehavior(CellLocateBehaviorOption value);

} // OclCell
