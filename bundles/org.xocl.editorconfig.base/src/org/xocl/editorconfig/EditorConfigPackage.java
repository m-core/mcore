/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigPackage.java,v 1.17 2011/07/20 17:04:53 mtg.kutter Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xocl.editorconfig.EditorConfigFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/GENMODEL prefix='EditorConfig' GENMODEL_modelName='EditorConfig'"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'EditorConfig\''"
 * @generated
 */
public interface EditorConfigPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "org.xocl.editorconfig.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "editorconfig";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.xocl.org/EditorConfig";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "xocl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EditorConfigPackage eINSTANCE = org.xocl.editorconfig.impl.EditorConfigPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.EditorConfigImpl <em>Editor Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.EditorConfigImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEditorConfig()
	 * @generated
	 */
	int EDITOR_CONFIG = 0;

	/**
	 * The feature id for the '<em><b>Table Config</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG__TABLE_CONFIG = 0;

	/**
	 * The feature id for the '<em><b>Header Table Config</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG__HEADER_TABLE_CONFIG = 1;

	/**
	 * The feature id for the '<em><b>Class To Table Config Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP = 2;

	/**
	 * The feature id for the '<em><b>Super Config</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG__SUPER_CONFIG = 3;

	/**
	 * The number of structural features of the '<em>Editor Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.EClassToTableConfigMapEntryImpl <em>EClass To Table Config Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.EClassToTableConfigMapEntryImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEClassToTableConfigMapEntry()
	 * @generated
	 */
	int ECLASS_TO_TABLE_CONFIG_MAP_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Intended Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__INTENDED_PACKAGE = 2;

	/**
	 * The number of structural features of the '<em>EClass To Table Config Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_TABLE_CONFIG_MAP_ENTRY_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl <em>EReference To Table Config Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEReferenceToTableConfigMapEntry()
	 * @generated
	 */
	int EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL = 2;

	/**
	 * The number of structural features of the '<em>EReference To Table Config Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.TableConfigImpl <em>Table Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.TableConfigImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getTableConfig()
	 * @generated
	 */
	int TABLE_CONFIG = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__NAME = 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__LABEL = 1;

	/**
	 * The feature id for the '<em><b>Display Default Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN = 2;

	/**
	 * The feature id for the '<em><b>Display Header</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__DISPLAY_HEADER = 3;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__FONT = 4;

	/**
	 * The feature id for the '<em><b>Column</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__COLUMN = 5;

	/**
	 * The feature id for the '<em><b>Reference To Table Config Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP = 6;

	/**
	 * The feature id for the '<em><b>Intended Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__INTENDED_PACKAGE = 7;

	/**
	 * The feature id for the '<em><b>Intended Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG__INTENDED_CLASS = 8;

	/**
	 * The number of structural features of the '<em>Table Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CONFIG_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.ColumnConfigImpl <em>Column Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.ColumnConfigImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getColumnConfig()
	 * @generated
	 */
	int COLUMN_CONFIG = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_CONFIG__NAME = 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_CONFIG__LABEL = 1;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_CONFIG__WIDTH = 2;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_CONFIG__FONT = 3;

	/**
	 * The feature id for the '<em><b>Cell Config Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_CONFIG__CELL_CONFIG_MAP = 4;

	/**
	 * The number of structural features of the '<em>Column Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_CONFIG_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl <em>EClass To Cell Config Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEClassToCellConfigMapEntry()
	 * @generated
	 */
	int ECLASS_TO_CELL_CONFIG_MAP_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EClass To Cell Config Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_CELL_CONFIG_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.CellConfigImpl <em>Cell Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.CellConfigImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getCellConfig()
	 * @generated
	 */
	int CELL_CONFIG = 6;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL_CONFIG__FONT = 0;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL_CONFIG__COLOR = 1;

	/**
	 * The number of structural features of the '<em>Cell Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL_CONFIG_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.RowFeatureCellImpl <em>Row Feature Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.RowFeatureCellImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getRowFeatureCell()
	 * @generated
	 */
	int ROW_FEATURE_CELL = 7;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_FEATURE_CELL__FONT = CELL_CONFIG__FONT;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_FEATURE_CELL__COLOR = CELL_CONFIG__COLOR;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_FEATURE_CELL__FEATURE = CELL_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cell Edit Behavior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR = CELL_CONFIG_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cell Locate Behavior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR = CELL_CONFIG_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Row Feature Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_FEATURE_CELL_FEATURE_COUNT = CELL_CONFIG_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.OclCellImpl <em>Ocl Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.OclCellImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getOclCell()
	 * @generated
	 */
	int OCL_CELL = 8;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CELL__FONT = CELL_CONFIG__FONT;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CELL__COLOR = CELL_CONFIG__COLOR;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CELL__EXPRESSION = CELL_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cell Locate Behavior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CELL__CELL_LOCATE_BEHAVIOR = CELL_CONFIG_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ocl Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_CELL_FEATURE_COUNT = CELL_CONFIG_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.impl.EditProviderCellImpl <em>Edit Provider Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.impl.EditProviderCellImpl
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEditProviderCell()
	 * @generated
	 */
	int EDIT_PROVIDER_CELL = 9;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDIT_PROVIDER_CELL__FONT = CELL_CONFIG__FONT;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDIT_PROVIDER_CELL__COLOR = CELL_CONFIG__COLOR;

	/**
	 * The number of structural features of the '<em>Edit Provider Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDIT_PROVIDER_CELL_FEATURE_COUNT = CELL_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.CellEditBehaviorOption <em>Cell Edit Behavior Option</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.CellEditBehaviorOption
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getCellEditBehaviorOption()
	 * @generated
	 */
	int CELL_EDIT_BEHAVIOR_OPTION = 10;

	/**
	 * The meta object id for the '{@link org.xocl.editorconfig.CellLocateBehaviorOption <em>Cell Locate Behavior Option</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.editorconfig.CellLocateBehaviorOption
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getCellLocateBehaviorOption()
	 * @generated
	 */
	int CELL_LOCATE_BEHAVIOR_OPTION = 11;

	/**
	 * The meta object id for the '<em>Font</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.URI
	 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getFont()
	 * @generated
	 */
	int FONT = 12;

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.EditorConfig <em>Editor Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Editor Config</em>'.
	 * @see org.xocl.editorconfig.EditorConfig
	 * @generated
	 */
	EClass getEditorConfig();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.editorconfig.EditorConfig#getTableConfig <em>Table Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Table Config</em>'.
	 * @see org.xocl.editorconfig.EditorConfig#getTableConfig()
	 * @see #getEditorConfig()
	 * @generated
	 */
	EReference getEditorConfig_TableConfig();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.editorconfig.EditorConfig#getHeaderTableConfig <em>Header Table Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Header Table Config</em>'.
	 * @see org.xocl.editorconfig.EditorConfig#getHeaderTableConfig()
	 * @see #getEditorConfig()
	 * @generated
	 */
	EReference getEditorConfig_HeaderTableConfig();

	/**
	 * Returns the meta object for the map '{@link org.xocl.editorconfig.EditorConfig#getClassToTableConfigMap <em>Class To Table Config Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Class To Table Config Map</em>'.
	 * @see org.xocl.editorconfig.EditorConfig#getClassToTableConfigMap()
	 * @see #getEditorConfig()
	 * @generated
	 */
	EReference getEditorConfig_ClassToTableConfigMap();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.editorconfig.EditorConfig#getSuperConfig <em>Super Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Config</em>'.
	 * @see org.xocl.editorconfig.EditorConfig#getSuperConfig()
	 * @see #getEditorConfig()
	 * @generated
	 */
	EReference getEditorConfig_SuperConfig();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EClass To Table Config Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EClass To Table Config Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="key value intendedPackage" 
	 *        keyType="org.eclipse.emf.ecore.EClass" keyRequired="true"
	 *        keyAnnotation="http://www.xocl.org/OCL choiceConstraint='let p:ecore::EPackage = self.intendedPackage\r\nin \r\nif p.oclIsUndefined() then true\r\nelse if p.eClassifiers->includes(trg) then true\r\nelse p.eSubpackages.eClassifiers->includes(trg)\r\nendif endif'"
	 *        valueType="org.xocl.editorconfig.TableConfig" valueRequired="true"
	 *        intendedPackageType="org.eclipse.emf.ecore.EPackage"
	 * @generated
	 */
	EClass getEClassToTableConfigMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToTableConfigMapEntry()
	 * @generated
	 */
	EReference getEClassToTableConfigMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToTableConfigMapEntry()
	 * @generated
	 */
	EReference getEClassToTableConfigMapEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Intended Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Intended Package</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToTableConfigMapEntry()
	 * @generated
	 */
	EReference getEClassToTableConfigMapEntry_IntendedPackage();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EReference To Table Config Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EReference To Table Config Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model features="key value label" 
	 *        keyType="org.eclipse.emf.ecore.EReference" keyRequired="true"
	 *        keyAnnotation="http://www.xocl.org/OCL choiceConstraint='let \r\n  tc:editorconfig::TableConfig = self.eContainer().oclAsType(editorconfig::TableConfig) in\r\nlet ic:ecore::EClass = tc.intendedClass in\r\nlet intended:Boolean = \r\n  if not ic.oclIsUndefined()\r\n  then\r\n    ic.eAllReferences->includes(trg)\r\n  else true endif in \r\nlet unique:Boolean =\r\n  self.key=trg or not (tc.referenceToTableConfigMap.key->includes(trg))\r\nin\r\ntrg.containment and intended and unique'"
	 *        valueType="org.xocl.editorconfig.TableConfig" valueRequired="true"
	 *        labelDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getEReferenceToTableConfigMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEReferenceToTableConfigMapEntry()
	 * @generated
	 */
	EReference getEReferenceToTableConfigMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEReferenceToTableConfigMapEntry()
	 * @generated
	 */
	EReference getEReferenceToTableConfigMapEntry_Value();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEReferenceToTableConfigMapEntry()
	 * @generated
	 */
	EAttribute getEReferenceToTableConfigMapEntry_Label();

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.TableConfig <em>Table Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Config</em>'.
	 * @see org.xocl.editorconfig.TableConfig
	 * @generated
	 */
	EClass getTableConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.TableConfig#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getName()
	 * @see #getTableConfig()
	 * @generated
	 */
	EAttribute getTableConfig_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.TableConfig#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getLabel()
	 * @see #getTableConfig()
	 * @generated
	 */
	EAttribute getTableConfig_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.TableConfig#isDisplayDefaultColumn <em>Display Default Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Default Column</em>'.
	 * @see org.xocl.editorconfig.TableConfig#isDisplayDefaultColumn()
	 * @see #getTableConfig()
	 * @generated
	 */
	EAttribute getTableConfig_DisplayDefaultColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.TableConfig#isDisplayHeader <em>Display Header</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Header</em>'.
	 * @see org.xocl.editorconfig.TableConfig#isDisplayHeader()
	 * @see #getTableConfig()
	 * @generated
	 */
	EAttribute getTableConfig_DisplayHeader();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.TableConfig#getFont <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getFont()
	 * @see #getTableConfig()
	 * @generated
	 */
	EAttribute getTableConfig_Font();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.editorconfig.TableConfig#getColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Column</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getColumn()
	 * @see #getTableConfig()
	 * @generated
	 */
	EReference getTableConfig_Column();

	/**
	 * Returns the meta object for the map '{@link org.xocl.editorconfig.TableConfig#getReferenceToTableConfigMap <em>Reference To Table Config Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Reference To Table Config Map</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getReferenceToTableConfigMap()
	 * @see #getTableConfig()
	 * @generated
	 */
	EReference getTableConfig_ReferenceToTableConfigMap();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.editorconfig.TableConfig#getIntendedPackage <em>Intended Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Intended Package</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getIntendedPackage()
	 * @see #getTableConfig()
	 * @generated
	 */
	EReference getTableConfig_IntendedPackage();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.editorconfig.TableConfig#getIntendedClass <em>Intended Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Intended Class</em>'.
	 * @see org.xocl.editorconfig.TableConfig#getIntendedClass()
	 * @see #getTableConfig()
	 * @generated
	 */
	EReference getTableConfig_IntendedClass();

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.ColumnConfig <em>Column Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Column Config</em>'.
	 * @see org.xocl.editorconfig.ColumnConfig
	 * @generated
	 */
	EClass getColumnConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.ColumnConfig#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.xocl.editorconfig.ColumnConfig#getName()
	 * @see #getColumnConfig()
	 * @generated
	 */
	EAttribute getColumnConfig_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.ColumnConfig#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.xocl.editorconfig.ColumnConfig#getLabel()
	 * @see #getColumnConfig()
	 * @generated
	 */
	EAttribute getColumnConfig_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.ColumnConfig#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see org.xocl.editorconfig.ColumnConfig#getWidth()
	 * @see #getColumnConfig()
	 * @generated
	 */
	EAttribute getColumnConfig_Width();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.ColumnConfig#getFont <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font</em>'.
	 * @see org.xocl.editorconfig.ColumnConfig#getFont()
	 * @see #getColumnConfig()
	 * @generated
	 */
	EAttribute getColumnConfig_Font();

	/**
	 * Returns the meta object for the map '{@link org.xocl.editorconfig.ColumnConfig#getCellConfigMap <em>Cell Config Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Cell Config Map</em>'.
	 * @see org.xocl.editorconfig.ColumnConfig#getCellConfigMap()
	 * @see #getColumnConfig()
	 * @generated
	 */
	EReference getColumnConfig_CellConfigMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EClass To Cell Config Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EClass To Cell Config Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EClass" keyRequired="true"
	 *        keyAnnotation="http://www.xocl.org/OCL choiceConstraint='let cc:editorconfig::ColumnConfig = self.eContainer().oclAsType(editorconfig::ColumnConfig)\r\nin \r\nlet tc:editorconfig::TableConfig = cc.eContainer().oclAsType(editorconfig::TableConfig) in\r\nlet  ic:ecore::EClass = tc.intendedClass in \r\nlet intended:Boolean = if not ic.oclIsUndefined()\r\n  then   \r\n      trg=ic or trg.eAllSuperTypes->includes(ic)\r\n  else\r\n    let ip:ecore::EPackage = tc.intendedPackage in\r\n    if ip.oclIsUndefined() then true \r\n    else \r\n      if ip.eClassifiers->includes(trg) then true\r\n      else ip.eSubpackages.eClassifiers->includes(trg)\r\n      endif\r\n    endif\r\n  endif\r\nin\r\nlet unique:Boolean = \r\n  self.key = trg or not (cc.cellConfigMap.key->includes(trg))\r\nin \r\nintended and unique'"
	 *        valueType="org.xocl.editorconfig.CellConfig" valueContainment="true" valueResolveProxies="true" valueRequired="true"
	 * @generated
	 */
	EClass getEClassToCellConfigMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToCellConfigMapEntry()
	 * @generated
	 */
	EReference getEClassToCellConfigMapEntry_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToCellConfigMapEntry()
	 * @generated
	 */
	EReference getEClassToCellConfigMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.CellConfig <em>Cell Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cell Config</em>'.
	 * @see org.xocl.editorconfig.CellConfig
	 * @generated
	 */
	EClass getCellConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.CellConfig#getFont <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font</em>'.
	 * @see org.xocl.editorconfig.CellConfig#getFont()
	 * @see #getCellConfig()
	 * @generated
	 */
	EAttribute getCellConfig_Font();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.CellConfig#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see org.xocl.editorconfig.CellConfig#getColor()
	 * @see #getCellConfig()
	 * @generated
	 */
	EAttribute getCellConfig_Color();

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.RowFeatureCell <em>Row Feature Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Row Feature Cell</em>'.
	 * @see org.xocl.editorconfig.RowFeatureCell
	 * @generated
	 */
	EClass getRowFeatureCell();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.editorconfig.RowFeatureCell#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see org.xocl.editorconfig.RowFeatureCell#getFeature()
	 * @see #getRowFeatureCell()
	 * @generated
	 */
	EReference getRowFeatureCell_Feature();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.RowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cell Edit Behavior</em>'.
	 * @see org.xocl.editorconfig.RowFeatureCell#getCellEditBehavior()
	 * @see #getRowFeatureCell()
	 * @generated
	 */
	EAttribute getRowFeatureCell_CellEditBehavior();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.RowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cell Locate Behavior</em>'.
	 * @see org.xocl.editorconfig.RowFeatureCell#getCellLocateBehavior()
	 * @see #getRowFeatureCell()
	 * @generated
	 */
	EAttribute getRowFeatureCell_CellLocateBehavior();

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.OclCell <em>Ocl Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ocl Cell</em>'.
	 * @see org.xocl.editorconfig.OclCell
	 * @generated
	 */
	EClass getOclCell();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.OclCell#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see org.xocl.editorconfig.OclCell#getExpression()
	 * @see #getOclCell()
	 * @generated
	 */
	EAttribute getOclCell_Expression();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.editorconfig.OclCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cell Locate Behavior</em>'.
	 * @see org.xocl.editorconfig.OclCell#getCellLocateBehavior()
	 * @see #getOclCell()
	 * @generated
	 */
	EAttribute getOclCell_CellLocateBehavior();

	/**
	 * Returns the meta object for class '{@link org.xocl.editorconfig.EditProviderCell <em>Edit Provider Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edit Provider Cell</em>'.
	 * @see org.xocl.editorconfig.EditProviderCell
	 * @generated
	 */
	EClass getEditProviderCell();

	/**
	 * Returns the meta object for enum '{@link org.xocl.editorconfig.CellEditBehaviorOption <em>Cell Edit Behavior Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cell Edit Behavior Option</em>'.
	 * @see org.xocl.editorconfig.CellEditBehaviorOption
	 * @generated
	 */
	EEnum getCellEditBehaviorOption();

	/**
	 * Returns the meta object for enum '{@link org.xocl.editorconfig.CellLocateBehaviorOption <em>Cell Locate Behavior Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cell Locate Behavior Option</em>'.
	 * @see org.xocl.editorconfig.CellLocateBehaviorOption
	 * @generated
	 */
	EEnum getCellLocateBehaviorOption();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.common.util.URI <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Font</em>'.
	 * @see org.eclipse.emf.common.util.URI
	 * @model instanceClass="org.eclipse.emf.common.util.URI"
	 * @generated
	 */
	EDataType getFont();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EditorConfigFactory getEditorConfigFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.EditorConfigImpl <em>Editor Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.EditorConfigImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEditorConfig()
		 * @generated
		 */
		EClass EDITOR_CONFIG = eINSTANCE.getEditorConfig();

		/**
		 * The meta object literal for the '<em><b>Table Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITOR_CONFIG__TABLE_CONFIG = eINSTANCE
				.getEditorConfig_TableConfig();

		/**
		 * The meta object literal for the '<em><b>Header Table Config</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITOR_CONFIG__HEADER_TABLE_CONFIG = eINSTANCE
				.getEditorConfig_HeaderTableConfig();

		/**
		 * The meta object literal for the '<em><b>Class To Table Config Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP = eINSTANCE
				.getEditorConfig_ClassToTableConfigMap();

		/**
		 * The meta object literal for the '<em><b>Super Config</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITOR_CONFIG__SUPER_CONFIG = eINSTANCE
				.getEditorConfig_SuperConfig();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.EClassToTableConfigMapEntryImpl <em>EClass To Table Config Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.EClassToTableConfigMapEntryImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEClassToTableConfigMapEntry()
		 * @generated
		 */
		EClass ECLASS_TO_TABLE_CONFIG_MAP_ENTRY = eINSTANCE
				.getEClassToTableConfigMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__KEY = eINSTANCE
				.getEClassToTableConfigMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__VALUE = eINSTANCE
				.getEClassToTableConfigMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Intended Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__INTENDED_PACKAGE = eINSTANCE
				.getEClassToTableConfigMapEntry_IntendedPackage();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl <em>EReference To Table Config Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEReferenceToTableConfigMapEntry()
		 * @generated
		 */
		EClass EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY = eINSTANCE
				.getEReferenceToTableConfigMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY = eINSTANCE
				.getEReferenceToTableConfigMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE = eINSTANCE
				.getEReferenceToTableConfigMapEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL = eINSTANCE
				.getEReferenceToTableConfigMapEntry_Label();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.TableConfigImpl <em>Table Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.TableConfigImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getTableConfig()
		 * @generated
		 */
		EClass TABLE_CONFIG = eINSTANCE.getTableConfig();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_CONFIG__NAME = eINSTANCE.getTableConfig_Name();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_CONFIG__LABEL = eINSTANCE.getTableConfig_Label();

		/**
		 * The meta object literal for the '<em><b>Display Default Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN = eINSTANCE
				.getTableConfig_DisplayDefaultColumn();

		/**
		 * The meta object literal for the '<em><b>Display Header</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_CONFIG__DISPLAY_HEADER = eINSTANCE
				.getTableConfig_DisplayHeader();

		/**
		 * The meta object literal for the '<em><b>Font</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_CONFIG__FONT = eINSTANCE.getTableConfig_Font();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_CONFIG__COLUMN = eINSTANCE.getTableConfig_Column();

		/**
		 * The meta object literal for the '<em><b>Reference To Table Config Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP = eINSTANCE
				.getTableConfig_ReferenceToTableConfigMap();

		/**
		 * The meta object literal for the '<em><b>Intended Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_CONFIG__INTENDED_PACKAGE = eINSTANCE
				.getTableConfig_IntendedPackage();

		/**
		 * The meta object literal for the '<em><b>Intended Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_CONFIG__INTENDED_CLASS = eINSTANCE
				.getTableConfig_IntendedClass();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.ColumnConfigImpl <em>Column Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.ColumnConfigImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getColumnConfig()
		 * @generated
		 */
		EClass COLUMN_CONFIG = eINSTANCE.getColumnConfig();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLUMN_CONFIG__NAME = eINSTANCE.getColumnConfig_Name();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLUMN_CONFIG__LABEL = eINSTANCE.getColumnConfig_Label();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLUMN_CONFIG__WIDTH = eINSTANCE.getColumnConfig_Width();

		/**
		 * The meta object literal for the '<em><b>Font</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLUMN_CONFIG__FONT = eINSTANCE.getColumnConfig_Font();

		/**
		 * The meta object literal for the '<em><b>Cell Config Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLUMN_CONFIG__CELL_CONFIG_MAP = eINSTANCE
				.getColumnConfig_CellConfigMap();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl <em>EClass To Cell Config Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEClassToCellConfigMapEntry()
		 * @generated
		 */
		EClass ECLASS_TO_CELL_CONFIG_MAP_ENTRY = eINSTANCE
				.getEClassToCellConfigMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY = eINSTANCE
				.getEClassToCellConfigMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE = eINSTANCE
				.getEClassToCellConfigMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.CellConfigImpl <em>Cell Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.CellConfigImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getCellConfig()
		 * @generated
		 */
		EClass CELL_CONFIG = eINSTANCE.getCellConfig();

		/**
		 * The meta object literal for the '<em><b>Font</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CELL_CONFIG__FONT = eINSTANCE.getCellConfig_Font();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CELL_CONFIG__COLOR = eINSTANCE.getCellConfig_Color();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.RowFeatureCellImpl <em>Row Feature Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.RowFeatureCellImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getRowFeatureCell()
		 * @generated
		 */
		EClass ROW_FEATURE_CELL = eINSTANCE.getRowFeatureCell();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROW_FEATURE_CELL__FEATURE = eINSTANCE
				.getRowFeatureCell_Feature();

		/**
		 * The meta object literal for the '<em><b>Cell Edit Behavior</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR = eINSTANCE
				.getRowFeatureCell_CellEditBehavior();

		/**
		 * The meta object literal for the '<em><b>Cell Locate Behavior</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR = eINSTANCE
				.getRowFeatureCell_CellLocateBehavior();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.OclCellImpl <em>Ocl Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.OclCellImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getOclCell()
		 * @generated
		 */
		EClass OCL_CELL = eINSTANCE.getOclCell();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCL_CELL__EXPRESSION = eINSTANCE.getOclCell_Expression();

		/**
		 * The meta object literal for the '<em><b>Cell Locate Behavior</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCL_CELL__CELL_LOCATE_BEHAVIOR = eINSTANCE
				.getOclCell_CellLocateBehavior();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.impl.EditProviderCellImpl <em>Edit Provider Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.impl.EditProviderCellImpl
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getEditProviderCell()
		 * @generated
		 */
		EClass EDIT_PROVIDER_CELL = eINSTANCE.getEditProviderCell();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.CellEditBehaviorOption <em>Cell Edit Behavior Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.CellEditBehaviorOption
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getCellEditBehaviorOption()
		 * @generated
		 */
		EEnum CELL_EDIT_BEHAVIOR_OPTION = eINSTANCE.getCellEditBehaviorOption();

		/**
		 * The meta object literal for the '{@link org.xocl.editorconfig.CellLocateBehaviorOption <em>Cell Locate Behavior Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.editorconfig.CellLocateBehaviorOption
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getCellLocateBehaviorOption()
		 * @generated
		 */
		EEnum CELL_LOCATE_BEHAVIOR_OPTION = eINSTANCE
				.getCellLocateBehaviorOption();

		/**
		 * The meta object literal for the '<em>Font</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.common.util.URI
		 * @see org.xocl.editorconfig.impl.EditorConfigPackageImpl#getFont()
		 * @generated
		 */
		EDataType FONT = eINSTANCE.getFont();

	}

} //EditorConfigPackage
