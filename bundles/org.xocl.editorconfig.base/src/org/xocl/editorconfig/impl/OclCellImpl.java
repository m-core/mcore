/**
 * <copyright>
 * </copyright>
 *
 * $Id: OclCellImpl.java,v 1.15 2010/11/30 08:04:06 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.editorconfig.CellLocateBehaviorOption;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.OclCell;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ocl Cell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.OclCellImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.OclCellImpl#getCellLocateBehavior <em>Cell Locate Behavior</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OclCellImpl extends CellConfigImpl implements OclCell {
	/**
	 * The default value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected String expression = EXPRESSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCellLocateBehavior() <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellLocateBehavior()
	 * @generated
	 * @ordered
	 */
	protected static final CellLocateBehaviorOption CELL_LOCATE_BEHAVIOR_EDEFAULT = CellLocateBehaviorOption.SELECTION;

	/**
	 * The cached value of the '{@link #getCellLocateBehavior() <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellLocateBehavior()
	 * @generated
	 * @ordered
	 */
	protected CellLocateBehaviorOption cellLocateBehavior = CELL_LOCATE_BEHAVIOR_EDEFAULT;

	/**
	 * OCL environment that evaluates cell expression
	 */
	private static OCL oclEnv = OCL.newInstance();

	/**
	 * Parsed cell expression
	 */
	private OCLExpression oclExpression;

	/**
	 * Set OCL environment options.
	 */
	static {
		ParsingOptions.setOption(oclEnv.getEnvironment(), ParsingOptions
				.implicitRootClass(oclEnv.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OclCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.OCL_CELL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(String newExpression) {
		String oldExpression = expression;
		expression = newExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.OCL_CELL__EXPRESSION, oldExpression,
					expression));
	}

	/**
	 * Returns the OCL expression context for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag GFI03
	 * @generated NOT
	 */
	public OCLExpressionContext getExpressionOCLExpressionContext() {
		return new OCLExpressionContext(
				((EClassToCellConfigMapEntryImpl) eContainer()).getKey(), null,
				new EClassifier[] { OCLExpressionContext
						.getOCLStandardLibrary().getString() });
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellLocateBehaviorOption getCellLocateBehavior() {
		return cellLocateBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCellLocateBehavior(
			CellLocateBehaviorOption newCellLocateBehavior) {
		CellLocateBehaviorOption oldCellLocateBehavior = cellLocateBehavior;
		cellLocateBehavior = newCellLocateBehavior == null ? CELL_LOCATE_BEHAVIOR_EDEFAULT
				: newCellLocateBehavior;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.OCL_CELL__CELL_LOCATE_BEHAVIOR,
					oldCellLocateBehavior, cellLocateBehavior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getCellValue(EObject row) {
		if (oclExpression == null) {
			String expressionText = getExpression();
			OCL.Helper oclHelper = oclEnv.createOCLHelper();
			EClass eContext = ((EClassToCellConfigMapEntryImpl) eContainer())
					.getKey();
			oclHelper.setContext(eContext);
			Variable rowVar = EcoreFactory.eINSTANCE.createVariable();
			rowVar.setName("row");
			rowVar.setType(eContext);
			// add it to the global OCL environment
			oclEnv.getEnvironment().deleteElement(rowVar.getName());
			oclEnv.getEnvironment().addElement(rowVar.getName(), rowVar, true);
			try {
				oclExpression = oclHelper.createQuery(expressionText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, expressionText,
						oclHelper.getProblems(), eClass(), eClass()
								.getEAllOperations().get(0)); /* TODO: remove explicit operation index*/
			}
		}
		OCL.Query oclQuery = oclEnv.createQuery(oclExpression);
		XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, oclQuery,
				EditorConfigPackage.Literals.OCL_CELL, "getCellValue");
		try {
			EvaluationEnvironment<EClassifier, ?, ?, EClass, EObject> evalEnv = oclQuery
					.getEvaluationEnvironment();
			evalEnv.clear();
			evalEnv.add("row", row);
			Object result = oclQuery.evaluate(row);
			return result;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.OCL_CELL__EXPRESSION:
			return getExpression();
		case EditorConfigPackage.OCL_CELL__CELL_LOCATE_BEHAVIOR:
			return getCellLocateBehavior();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.OCL_CELL__EXPRESSION:
			setExpression((String) newValue);
			return;
		case EditorConfigPackage.OCL_CELL__CELL_LOCATE_BEHAVIOR:
			setCellLocateBehavior((CellLocateBehaviorOption) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.OCL_CELL__EXPRESSION:
			setExpression(EXPRESSION_EDEFAULT);
			return;
		case EditorConfigPackage.OCL_CELL__CELL_LOCATE_BEHAVIOR:
			setCellLocateBehavior(CELL_LOCATE_BEHAVIOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.OCL_CELL__EXPRESSION:
			return EXPRESSION_EDEFAULT == null ? expression != null
					: !EXPRESSION_EDEFAULT.equals(expression);
		case EditorConfigPackage.OCL_CELL__CELL_LOCATE_BEHAVIOR:
			return cellLocateBehavior != CELL_LOCATE_BEHAVIOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (expression: ");
		result.append(expression);
		result.append(", cellLocateBehavior: ");
		result.append(cellLocateBehavior);
		result.append(')');
		return result.toString();
	}

} //OclCellImpl
