/**
 * <copyright>
 *
 * Copyright (c) 2007, 2008 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *   Lucas Bigeardel - EMF Search integrations
 *
 * </copyright>
 *
 * $Id: TargetMetamodel.java,v 1.2 2014/07/23 13:34:47 mtg.hillairet Exp $
 */

package org.xocl.common.ui.ocl.engine;


/**
 * Enumeration of supported target metamodels.
 * 
 * @author Christian W. Damus (cdamus)
 */
public enum TargetMetamodel {
    Ecore,
    UML
}
