/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.common.ui.dialogs;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.TextEvent;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.xocl.common.ui.ocl.engine.TargetMetamodel;
import org.xocl.common.ui.widgets.OCLExpressionWidget;

/**
 * @author Max Stepanov
 *
 */
public class OCLExpressionDialog extends Dialog {
    /**
     * The title of the dialog.
     */
    private String title;

    /**
     * The message to display, or <code>null</code> if none.
     */
    private String message;

    /**
     * The input value; the empty string by default.
     */
    private String value = "";//$NON-NLS-1$

    /**
     * The evaluation context, or <code>null</code> if none.
     */
    private EClassifier context;
    
    /**
     * Variables
     */
    private Variable[] variables;

    /**
     * Result type
     */
    private EClassifier[] resultTypes;

    /**
     * Input text widget.
     */
    private OCLExpressionWidget text;

    /**
     * Error message label widget.
     */
    private Text errorMessageText;
    
    /**
     * Error message string.
     */
    private String errorMessage;

    private final boolean isEditable;

    /**
     * Creates an input dialog with OK and Cancel buttons. Note that the dialog
     * will have no visual representation (no widgets) until it is told to open.
     * <p>
     * Note that the <code>open</code> method blocks for input dialogs.
     * </p>
     * 
     * @param parentShell
     *            the parent shell, or <code>null</code> to create a top-level
     *            shell
     * @param dialogTitle
     *            the dialog title, or <code>null</code> if none
     * @param dialogMessage
     *            the dialog message, or <code>null</code> if none
     * @param initialValue
     *            the initial input value, or <code>null</code> if none
     *            (equivalent to the empty string)
     * @param context
     *            an evaluation context, or <code>null</code> if none
     */
    public OCLExpressionDialog(Shell parentShell, String dialogTitle,
            String dialogMessage, String initialValue, EClassifier context, Variable[] variables, EClassifier[] resultTypes) {
    	this(parentShell, dialogTitle, dialogMessage, initialValue, context, variables, resultTypes, true);
    }
    public OCLExpressionDialog(Shell parentShell, String dialogTitle,
            String dialogMessage, String initialValue, EClassifier context, Variable[] variables, EClassifier[] resultTypes, boolean editable) {
        super(parentShell);
        setShellStyle(getShellStyle() | SWT.RESIZE);

        this.title = dialogTitle;
        message = dialogMessage;
        if (initialValue == null) {
			value = "";//$NON-NLS-1$
		} else {
			value = initialValue;
		}
        this.context = context;
        this.variables = variables;
        this.resultTypes = resultTypes;
        isEditable = editable;
    }

    /*
     * (non-Javadoc) Method declared on Dialog.
     */
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.OK_ID) {
            value = text.getExpression();
        } else {
            value = null;
        }
        super.buttonPressed(buttonId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        if (title != null) {
			shell.setText(title);
		}
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
     */
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		if (isEditable) {
			createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		}
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
    /*
     * (non-Javadoc) Method declared on Dialog.
     */
    protected Control createDialogArea(Composite parent) {
        // create composite
        Composite composite = (Composite) super.createDialogArea(parent);

        // create message
        if (message != null) {
            Label label = new Label(composite, SWT.WRAP);
            label.setText(message);
            GridData data = new GridData(GridData.GRAB_HORIZONTAL 
            		| GridData.HORIZONTAL_ALIGN_FILL
                    | GridData.BEGINNING);
            data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
            label.setLayoutData(data);
            label.setFont(parent.getFont());
        }

        text = new OCLExpressionWidget(composite);
        TextViewer viewer = (TextViewer) text.getViewer();
		text.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).create());

		((StyledText)viewer.getControl()).setWordWrap(true);
		text.setTargetMetamodel(TargetMetamodel.Ecore);
		text.setContext(context);
		text.setVariables(variables);
		text.setResultTypes(resultTypes);
		text.setExpression((String)getValue());
        
		((StyledText)viewer.getControl()).addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validateInput();
            }
        });
		viewer.addTextListener(new ITextListener() {
			public void textChanged(TextEvent event) {
				if (errorMessage != null) {
					validateInput();
					value = text.getExpression();
				}
			}
		});
        errorMessageText = new Text(composite, SWT.READ_ONLY | SWT.WRAP);
        errorMessageText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
                | GridData.HORIZONTAL_ALIGN_FILL));
        errorMessageText.setBackground(errorMessageText.getDisplay()
                .getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        errorMessageText.setForeground(errorMessageText.getDisplay().getSystemColor(SWT.COLOR_RED));
        // Set the error message text
        // See https://bugs.eclipse.org/bugs/show_bug.cgi?id=66292
        setErrorMessage(errorMessage);

        applyDialogFont(composite);
        
        validateInput();
        return composite;
    }

    /**
     * Returns the string typed into this input dialog.
     * 
     * @return the input string
     */
    public String getValue() {
        return value;
    }

    /**
     * Validates the input.
     * <p>
     * The default implementation of this framework method delegates the request
     * to the supplied input validator object; if it finds the input invalid,
     * the error message is displayed in the dialog's message line. This hook
     * method is called whenever the text changes in the input field.
     * </p>
     */
    protected void validateInput() {
    	Diagnostic diagnostic = text.validateExpression();
        if (diagnostic.getSeverity() != Diagnostic.OK) {
			StringBuffer sb = new StringBuffer();
			printDiagnostic(sb, diagnostic, "");
			if (sb.charAt(sb.length()-1) == '\n') {
				sb.setLength(sb.length()-1);
			}
			if (diagnostic.getSeverity() == Diagnostic.ERROR) {
				setErrorMessage("Error: "+sb.toString());
			} else {
				setWarningMessage("Warning: "+sb.toString());				
			}
        } else {
	        setErrorMessage(null);
        }
    }

	private static void printDiagnostic(StringBuffer sb, Diagnostic diagnostic, String indent) {
		if (diagnostic.getMessage() != null) {
			sb.append(indent).append(diagnostic.getMessage()).append('\n');
		}
		for (Diagnostic child : diagnostic.getChildren()) {
			printDiagnostic(sb, child, indent + "  ");
		}		
	}

    /**
     * Sets or clears the error message.
     * If not <code>null</code>, the OK button is disabled.
     * 
     * @param errorMessage
     *            the error message, or <code>null</code> to clear
     * @since 3.0
     */
    public void setErrorMessage(String errorMessage) {
    	this.errorMessage = errorMessage;
    	if (errorMessageText != null && !errorMessageText.isDisposed()) {
    		errorMessageText.setText(errorMessage == null ? " \n " : errorMessage); //$NON-NLS-1$
    		errorMessageText.setForeground(errorMessageText.getDisplay().getSystemColor(SWT.COLOR_RED));
    		// Disable the error message text control if there is no error, or
    		// no error text (empty or whitespace only).  Hide it also to avoid
    		// color change.
    		// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=130281
    		boolean hasError = errorMessage != null && (StringConverter.removeWhiteSpaces(errorMessage)).length() > 0;
    		errorMessageText.setEnabled(hasError);
    		errorMessageText.setVisible(hasError);
    		errorMessageText.getParent().update();
    		// Access the ok button by id, in case clients have overridden button creation.
    		// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=113643
    		Control button = getButton(IDialogConstants.OK_ID);
    		if (button != null) {
        		// temporary enable saving invalid OCL expressions
    			button.setEnabled(errorMessage == null || true);
    		}
    	}
    }

    /**
     * Sets or clears the warning message.
     * If not <code>null</code>, the OK button is disabled.
     * 
     * @param errorMessage
     *            the error message, or <code>null</code> to clear
     * @since 3.0
     */
    public void setWarningMessage(String errorMessage) {
    	this.errorMessage = errorMessage;
    	if (errorMessageText != null && !errorMessageText.isDisposed()) {
    		errorMessageText.setText(errorMessage == null ? " \n " : errorMessage); //$NON-NLS-1$
    		errorMessageText.setForeground(errorMessageText.getDisplay().getSystemColor(SWT.COLOR_DARK_YELLOW));
    		// Disable the error message text control if there is no error, or
    		// no error text (empty or whitespace only).  Hide it also to avoid
    		// color change.
    		// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=130281
    		boolean hasError = errorMessage != null && (StringConverter.removeWhiteSpaces(errorMessage)).length() > 0;
    		errorMessageText.setEnabled(hasError);
    		errorMessageText.setVisible(hasError);
    		errorMessageText.getParent().update();
    		// Access the ok button by id, in case clients have overridden button creation.
    		// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=113643
//    		Control button = getButton(IDialogConstants.OK_ID);
//    		if (button != null) {
//    			button.setEnabled(true);
//    		}
    	}
    }
}
