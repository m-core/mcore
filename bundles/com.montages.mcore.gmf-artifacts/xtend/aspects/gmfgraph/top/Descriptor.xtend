package aspects.gmfgraph.top

import com.google.inject.Inject
import gmfgraph.Runtime
import gmfgraph.Utils_qvto
import gmfgraph.top.Figure
import org.eclipse.gmf.gmfgraph.ChildAccess
import org.eclipse.gmf.gmfgraph.FigureDescriptor
import org.eclipse.gmf.gmfgraph.RealFigure
import xpt.Common

class Descriptor extends gmfgraph.top.Descriptor {
	
	@Inject Runtime xptRuntime;
	@Inject Figure  xptFigure;
	@Inject extension Common;
	@Inject extension Utils_qvto;
	
	override body(FigureDescriptor it) '''	
		«FOR acc : accessors.filter[a|!allCustomAccessors(it).map[typedFigure].exists[f|f == a.figure]]»
			«accessorField(acc)»
		«ENDFOR»		
	
		«xptFigure.ClassBody(it.actualFigure, compilationUnitName(it))»
		
		«FOR acc : accessors.filter[a|!allCustomAccessors(it).map[typedFigure].exists[f|f == a.figure]]»
			«accessorToField(acc)»
			«extraLineBreak»
		«ENDFOR»
	'''
	
	override accessorField(ChildAccess it) '''
		«generatedMemberComment»
		private «xptRuntime.fqn(it.figure)» «childAccessVariableName(it)»; 
	'''
	
	override accessorToField(ChildAccess it) '''
		«generatedMemberComment»
		public «xptRuntime.fqn(figure)» «accessor»() {
			return «childAccessVariableName(it)»;
		}
	'''
	
	def String childAccessVariableName(ChildAccess it) {
		if (it.figure == null) {
			return it.figureFieldName;	
		}
		return figureVariableName(it.figure);
	}
	
	def String figureVariableName(org.eclipse.gmf.gmfgraph.Figure it) {
		if (it instanceof RealFigure) {
			return figureVariableName(it as RealFigure, 0);
		}
		return it.figureFieldName;
	}
}