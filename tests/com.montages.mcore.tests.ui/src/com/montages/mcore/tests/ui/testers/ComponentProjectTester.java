package com.montages.mcore.tests.ui.testers;

import static org.junit.Assert.assertEquals;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

import com.montages.mcore.tests.ui.mock.ComponentMock;

public class ComponentProjectTester {

	private final ResourcesTester resourcesTester = new ResourcesTester();

	public void test(ComponentMock mock, 
			String expectedProject,
			String expectedMcoreFile,
			String expectedEcoreFile,
			String expectedEditorFile) {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		assertEquals(expectedProject, mock.project());

		IProject project = resourcesTester.assertProjectIsOk(expectedProject, root);
		IFolder modelFolder = resourcesTester.assertFolderIsOk("model", project);
		resourcesTester.assertFolderIsOk("META-INF", project);

		resourcesTester.assertFileIsOk(expectedMcoreFile, modelFolder);
		resourcesTester.assertFileIsOk(expectedEcoreFile, modelFolder);
		resourcesTester.assertFileIsOk(expectedEditorFile, modelFolder);
		resourcesTester.assertFileIsOk("plugin.xml", project);
	}

}
