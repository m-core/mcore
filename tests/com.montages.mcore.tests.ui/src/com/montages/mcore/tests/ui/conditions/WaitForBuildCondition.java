package com.montages.mcore.tests.ui.conditions;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swtbot.swt.finder.waits.DefaultCondition;

public class WaitForBuildCondition extends DefaultCondition implements ListenerCondition {

	private IWorkspace workspace = ResourcesPlugin.getWorkspace();
	private boolean done = false;
	private int found = 0;
	
	private String expectedMcore;
	private String expectedEcore;
	
	public WaitForBuildCondition(String expectedMcore, String expectedEcore) {
		this.expectedMcore = expectedMcore;
		this.expectedEcore = expectedEcore;
	}

	private final IResourceChangeListener listener = new IResourceChangeListener() {
		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			if (event.getType() == IResourceChangeEvent.POST_CHANGE) {
				try {
					event.getDelta().accept(new IResourceDeltaVisitor() {
						@Override
						public boolean visit(IResourceDelta delta) throws CoreException {
							if (delta.getResource() != null && delta.getResource().getName() != null) {
								if (delta.getResource().getName().equals(expectedMcore + ".mcore") ||
										delta.getResource().getName().equals(expectedEcore + ".ecore") || 
										delta.getResource().getName().equals(expectedEcore + ".editorconfig")) {
									found += 1;
								}
								if (found == 3) {
									done = true;
								}
								return true;
							}
							return true;
						}
					});
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
	};

	@Override
	public void removeListener(){
		workspace.removeResourceChangeListener(listener);
	}

	@Override
	public void addListener(){
		workspace.addResourceChangeListener(listener);
	}

	@Override
	public boolean test() throws Exception {
		return done;
	}

	@Override
	public String getFailureMessage() {
		return null;
	}

}