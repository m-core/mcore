package com.montages.mcore.tests.ui.testers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.Path;

public class ResourcesTester {

	public IProject assertProjectIsOk(String name, IWorkspaceRoot root) {
		IProject project = root.getProject(name);
		assertNotNull(project);
		assertTrue(project.exists());
		return project;
	}

	public IFolder assertFolderIsOk(String name, IProject container) {
		IFolder modelFolder = container.getFolder(name);
		assertNotNull(modelFolder);
		assertTrue(modelFolder.exists());
		assertTrue(modelFolder.isAccessible());
		return modelFolder;
	}

	public IFile assertFileIsOk(String fileName, IContainer folder) {
		IFile file = folder.getFile(new Path(fileName));
		assertNotNull(file);
		assertTrue(file.exists());
		assertTrue(file.isAccessible());

		return file;
	}

}
