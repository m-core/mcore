package com.montages.mcore.tests.ui.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.montages.mcore.tests.ui.tests.ComponentReferencesTest;
import com.montages.mcore.tests.ui.tests.CreateProjectTest;

@RunWith(Suite.class)
@SuiteClasses({
	CreateProjectTest.class,
	ComponentReferencesTest.class
})
public class TestSuite {}
