package com.montages.mcore.tests.ui.testers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;

import com.montages.common.CommonPlugin;
import com.montages.mcore.tests.ui.mock.ComponentMock;

public class URIMappingTester {

	public void test(ComponentMock mock, 
			String expectedLocation, 
			String expectedURI, 
			String expectedNormalizedLoation, 
			String expectedNormalizedURI) {

		Map<URI, URI> uriMap = new HashMap<URI, URI>();
		CommonPlugin.computeWorkspaceModels(uriMap);

		URI modelURI = mock.fullURI();
		URI folderURI = mock.folderURI();
		
		assertEquals(URI.createURI(expectedLocation), folderURI);
		assertEquals(URI.createURI(expectedURI), modelURI);

		assertNotNull(uriMap.get(modelURI));
		assertNotNull(uriMap.get(folderURI));

		assertEquals(URI.createURI(expectedNormalizedLoation), uriMap.get(folderURI));
		assertEquals(URI.createURI(expectedNormalizedURI), uriMap.get(modelURI));
	}

}
