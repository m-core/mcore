package com.montages.mcore.fluent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;

public class MFluent {

	public interface IFluent {}

	private Map<String, MFluentComp> components = new HashMap<String, MFluentComp>();

	public MFluent() {}

	public MFluentComp createComponent(Resource resource, String name, String domain, String type) {
		MComponent c = McoreFactory.eINSTANCE.createMComponent();
		c.setName(name);
		c.setDomainName(domain);
		c.setDomainType(type);
		resource.getContents().add(c);

		MFluentComp fc = new MFluentComp(c); 
		components.put(c.getName(), fc);

		return fc;
	}

	public static class MFluentComp implements IFluent {
		public final MComponent component;
		private Map<String, MFluentPackage> packages = new HashMap<String, MFluentPackage>();

		MFluentComp(MComponent component) {
			this.component = component;
		}

		public MFluentComp addPackage(String name) {
			create(name);
			return this;
		}
		
		public MFluentPackage getOrCreate(String name) {
			if (packages.containsKey(name))
				return packages.get(name);
			else return create(name);
		}

		private MFluentPackage create(String name) {
			MPackage p = McoreFactory.eINSTANCE.createMPackage();
			p.setName(name);

			component.getOwnedPackage().add(p);
			MFluentPackage f = new MFluentPackage(this, p);
			packages.put(name, f);
			return f;
		}

	}

	public static class MFluentPackage implements IFluent {
		public final MPackage mPackage;
		public final MFluentComp parent;

		private Map<String, MFluentClass> classes = new HashMap<String, MFluentClass>();

		MFluentPackage(MFluentComp parent, MPackage mPackage) {
			this.parent = parent;
			this.mPackage = mPackage;
		}

		public MFluentPackage addClassifier(String name) {
			create(name);
			return this;
		}

		public MFluentClass getOrCreate(String name) {
			if (classes.containsKey(name)) {
				return classes.get(name);
			} else {
				return create(name);
			}
		}

		private MFluentClass create(String name) {
			MClassifier c = McoreFactory.eINSTANCE.createMClassifier();
			c.setName(name);
			mPackage.getClassifier().add(c);
			MFluentClass f = new MFluentClass(this, c);
			classes.put(name, f);
			return f;
		}
	}

	public static class MFluentClass implements IFluent {
		public final MClassifier classifier;
		public final MFluentPackage parent;
		private Map<String, MFluentProp> properties = new HashMap<String, MFluentProp>();

		public MFluentClass(MFluentPackage parent, MClassifier classifier) {
			this.parent = parent;
			this.classifier = classifier;
		}

		public MFluentClass addProperty(String name, SimpleType type) {
			create(name, type);
			return this;
		}

		public MFluentClass supertypes(MFluentClass... supers) {
			if (supers != null) {
				for (MFluentClass c: supers) {
					classifier.getSuperType().add(c.classifier);
				}
			}
			return this;
		}

		public MFluentClass addProperty(String name, MClassifier type) {
			create(name, type);
			return this;
		}
		
		public MFluentClass addDerivedProperty(MProperty p, MClassifier type, MPropertyAnnotations ann) {
			if (classifier.getAnnotations() == null) {
				classifier.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
			}
			ann.setProperty(p);
			p.setPersisted(false);
			p.setSingular(false);
			p.setType(type);
			classifier.getAnnotations().getPropertyAnnotations().add(ann);
			
			return this;
		}

		public MFluentClass addDerivedProperty(MProperty p, SimpleType type, boolean many, MPropertyAnnotations ann) {
			if (classifier.getAnnotations() == null) {
				classifier.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
			}
			ann.setProperty(p);
			p.setPersisted(false);
			p.setSingular(!many);
			p.setSimpleType(type);
			classifier.getAnnotations().getPropertyAnnotations().add(ann);
			
			return this;
		}

		public MFluentClass addDerivedProperty(String name, SimpleType type, boolean many, MPropertyAnnotations ann) {
			if (classifier.getAnnotations() == null) {
				classifier.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
			}

			MProperty p = create(name, type).property;
			p.setPersisted(false);
			p.setSingular(!many);

			if (ann != null) {
				ann.setProperty(p);
				classifier.getAnnotations().getPropertyAnnotations().add(ann);
			}

			return this;
		}

		public MFluentClass addDerivedProperty(String name, MClassifier type, MPropertyAnnotations ann) {
			if (classifier.getAnnotations() == null) {
				classifier.setAnnotations(AnnotationsFactory.eINSTANCE.createMClassifierAnnotations());
			}
			MProperty p = create(name, type).property;
			p.setPersisted(false);
			p.setSingular(false);

			if (ann != null) {
				ann.setProperty(p);
				classifier.getAnnotations().getPropertyAnnotations().add(ann);
			}

			return this;
		}

		public MFluentProp get(String name) {
			if (properties.containsKey(name)) {
				return properties.get(name);
			} else {
				return create(name, SimpleType.NONE);
			}
		}

		public MFluentProp create(String name, SimpleType type) {
			MProperty p = McoreFactory.eINSTANCE.createMProperty();
			p.setName(name);
			p.setSimpleType(type);
			classifier.getProperty().add(p);
			MFluentProp f = new MFluentProp(this, p);
			properties.put(name, f);
			return f;
		}

		public MFluentProp create(String name, MClassifier type) {
			MProperty p = McoreFactory.eINSTANCE.createMProperty();
			p.setName(name);
			p.setType(type);
			classifier.getProperty().add(p);
			MFluentProp f = new MFluentProp(this, p);
			properties.put(name, f);
			return f;
		}
	}

	public static class MFluentProp implements IFluent {
		public final MProperty property;
		public final MFluentClass parent;
		
		public MFluentProp(MFluentClass parent, MProperty property) {
			this.parent = parent;
			this.property = property;
		}

		public MFluentProp addSignature(MParameter... parameters) {
			MOperationSignature s = McoreFactory.eINSTANCE.createMOperationSignature();
			if (parameters != null && parameters.length > 0) {
				s.getParameter().addAll(Arrays.asList(parameters));
			}
			property.getOperationSignature().add(s);

			return this;
		}
	}
	
	public MParameter createParameter(String name, MClassifier type, boolean many) {
		MParameter p = McoreFactory.eINSTANCE.createMParameter();
		p.setName(name);
		p.setType(type);
		if (many) p.setSingular(false);

		return p;
	}

	public MParameter createParameter(String name, SimpleType type, boolean many) {
		MParameter p = McoreFactory.eINSTANCE.createMParameter();
		p.setName(name);
		p.setSimpleType(type);
		if (many) p.setSingular(false);

		return p;
	}

	public MPropertyAnnotations createResultAnnotation(String value) {
		MPropertyAnnotations pann = AnnotationsFactory.eINSTANCE.createMPropertyAnnotations();
		MResult res = AnnotationsFactory.eINSTANCE.createMResult();
		res.setUseExplicitOcl(true);
		res.setValue(value);
		pann.setResult(res);
		
		return pann;
	}

	protected MPackage getCoreData(ResourceSet resourceSet) {
		Resource resource = resourceSet.getResource(URI.createURI("mcore://www.langlets.org/coreTypes"), true);
		MComponent c = (MComponent) resource.getContents().get(0);
		return c.getOwnedPackage().get(0).getSubPackage().get(0);
	}

	protected MClassifier find(MPackage p, String name) {
		for (MClassifier c: p.getClassifier()) {
			if (c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <F extends IFluent> F get(String path, Class<F> cast) {
		String[] split = path.split("\\.");
		if (split.length > 0) {
			MFluentComp fc = components.get(split[0]);
			if (split.length > 1) {
				MFluentPackage fp = fc.getOrCreate(split[1]);
				if (split.length > 2) {
					MFluentClass fcc = fp.getOrCreate(split[2]);
					if (split.length > 3) {
						MFluentProp fpp = fcc.get(split[3]);
						return (F) fpp;
					} else {
						return (F) fcc;
					}
				} else {
					return (F) fp;
				}
			} else {
				return (F) fc;
			}
		}
		return null;
	}

}
