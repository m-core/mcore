package com.montages.mcore.tests.ecore2mcore;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.junit.Ignore;
import org.junit.Test;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.ecore2mcore.Ecore2Mcore;
import com.montages.mcore.tests.support.TestSupport;

public class Ecore2McoreTest extends TestSupport {

	@Ignore
	@Test
	public void testCreateComponentAndRootPackage() throws CoreException {
		Ecore2Mcore t = new Ecore2Mcore();
		List<MComponent> l = t.transform(createModel());

		assertEquals(1, l.size());
		assertEquals(McorePackage.Literals.MCOMPONENT, l.get(0).eClass());

		MComponent c = l.get(0);
		assertEquals("Foo", c.getName());
		assertEquals("org", c.getDomainType());
		assertEquals("foo", c.getDomainName());

		assertEquals(1, c.getOwnedPackage().size());

		MPackage p = c.getOwnedPackage().get(0);
		assertEquals("Foo", p.getName());
	}

	@Test
	public void testCreateClassesAndHierarchies() throws CoreException {
		Ecore2Mcore t = new Ecore2Mcore();
		List<MComponent> l = t.transform(createModel());

		assertEquals(1, l.size());
		assertEquals(McorePackage.Literals.MCOMPONENT, l.get(0).eClass());

		MComponent c = l.get(0);
		MPackage p = c.getOwnedPackage().get(0);

		assertEquals(2, p.getClassifier().size());

		MClassifier c1 = p.getClassifier().get(0);
		MClassifier c2 = p.getClassifier().get(1);

		assertEquals("A", c1.getName());
		assertEquals("B", c2.getName());
	}

	private EPackage createModel() {
		EPackage p = EcoreFactory.eINSTANCE.createEPackage();
		p.setName("foo");
		p.setNsPrefix("foo");
		p.setNsURI("http://www.acme.org/foo/foo");

		EClass c1 = EcoreFactory.eINSTANCE.createEClass();
		c1.setName("A");

		EClass c2 = EcoreFactory.eINSTANCE.createEClass();
		c2.setName("B");

		c2.getESuperTypes().add(c1);

		p.getEClassifiers().add(c1);
		p.getEClassifiers().add(c2);

		return p;
	}

}
