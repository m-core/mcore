package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Ignore;
import org.junit.Test;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.SimpleType;
import com.montages.mcore.fluent.MFluent;
import com.montages.mcore.fluent.MFluent.MFluentComp;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.tests.support.TestSupport;

public class ResourceTest extends TestSupport {

	@Test
	public void testInputInSameResource() throws IOException, CoreException {
		Mcore2Ecore t = new Mcore2Ecore();
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.createResource(URI.createURI("test.mcore"));
		MComponent c = McoreFactory.eINSTANCE.createMComponent();
		r.getContents().add(c);

		t.transform(rs, c);

		assertEquals(r, c.eResource());		
	}

	@Test
	public void testEachResultResources() throws CoreException {
		Mcore2Ecore t = new Mcore2Ecore();
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.createResource(URI.createURI("test.mcore"));

		MFluent f = new MFluent();
		MFluentComp fc = f.createComponent(r, "test", "foo", "org")
			.addPackage("p1")
			.addPackage("p2")
			.addPackage("p3")
			.addPackage("p4");

		List<Resource> results = t.transform(rs, fc.component);

		assertEquals(r, fc.component.eResource());
		assertEquals(4, results.size());

		Resource r1 = results.get(0);
		Resource r2 = results.get(1);
		Resource r3 = results.get(2);
		Resource r4 = results.get(3);

		assertEquals(1, r1.getContents().size());
		assertEquals(1, r2.getContents().size());
		assertEquals(1, r3.getContents().size());
		assertEquals(1, r4.getContents().size());

		assertEquals(fc.getOrCreate("p1").mPackage.getInternalEPackage(), r1.getContents().get(0));
		assertEquals(fc.getOrCreate("p2").mPackage.getInternalEPackage(), r2.getContents().get(0));
		assertEquals(fc.getOrCreate("p3").mPackage.getInternalEPackage(), r3.getContents().get(0));
		assertEquals(fc.getOrCreate("p4").mPackage.getInternalEPackage(), r4.getContents().get(0));
	}

	@Ignore
	@Test
	public void testCrossReferences() throws CoreException {
		Mcore2Ecore t = new Mcore2Ecore();

		ResourceSet rs = new ResourceSetImpl();
		Resource r1 = rs.createResource(URI.createURI("test1.mcore"));
		Resource r2 = rs.createResource(URI.createURI("test2.mcore"));

		MComponent c1 = createComponent("c1", "foo", "org");
		MComponent c2 = createComponent("c2", "foo", "org");
		
		MPackage p1 = createPackage(c1, "p1");
		MPackage p2 = createPackage(c2, "p2");
		
		MClassifier a = createClassifier(p1, "A");
		createProperty(a, "a", SimpleType.STRING);

		MClassifier b = createClassifier(p2, "B");
		createProperty(b, "b", SimpleType.STRING);
		
		MClassifier c = createClassifier(p2, "C");
		createProperty(c, "c", SimpleType.STRING);

		b.getSuperType().add(a);
		b.getSuperType().add(c);

		r1.getContents().add(c1);
		r2.getContents().add(c2);

		List<Resource> result = t.transform(rs, c1);

		assertEquals(2, result.size());
		assertEquals(1, result.get(0).getContents().size());
		assertEquals(1, result.get(1).getContents().size());
		assertEquals(EcorePackage.Literals.EPACKAGE, result.get(0).getContents().get(0).eClass());
		assertEquals(EcorePackage.Literals.EPACKAGE, result.get(1).getContents().get(0).eClass());
		
		EPackage first = (EPackage) result.get(0).getContents().get(0);
		EPackage second = (EPackage) result.get(1).getContents().get(0);
		
		assertEquals(1, first.getEClassifiers().size());
		assertEquals(first, p1.getInternalEPackage());
		
		assertEquals(2, second.getEClassifiers().size());
		assertEquals(second, p2.getInternalEPackage());

		EClass firstA = (EClass) first.getEClassifiers().get(0);
		assertEquals("A", firstA.getName());
		assertEquals(firstA, a.getInternalEClassifier());

		EClass firstB = (EClass) second.getEClassifiers().get(0);
		assertEquals("B", firstB.getName());
		assertEquals(firstB, b.getInternalEClassifier());
		
		EClass firstC = (EClass) second.getEClassifiers().get(1);
		assertEquals("C", firstC.getName());
		assertEquals(firstC, c.getInternalEClassifier());
		
		assertEquals(2, firstB.getESuperTypes().size());
		assertEquals(firstA, firstB.getESuperTypes().get(0));
		assertEquals(firstC, firstB.getESuperTypes().get(1));

		assertEquals(a.getInternalEClassifier(), firstB.getESuperTypes().get(0));
		assertEquals(a.getInternalEClassifier(), ((EClass) b.getInternalEClassifier()).getESuperTypes().get(0));

		assertEquals(c.getInternalEClassifier(), firstB.getESuperTypes().get(1));
		assertEquals(c.getInternalEClassifier(), ((EClass) b.getInternalEClassifier()).getESuperTypes().get(1));
	}

}
