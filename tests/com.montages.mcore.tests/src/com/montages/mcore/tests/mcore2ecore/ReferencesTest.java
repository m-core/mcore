package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.junit.Before;
import org.junit.Test;

import com.montages.mcore.MComponent;
import com.montages.mcore.tests.support.TestSupport;

public class ReferencesTest extends TestSupport {

	private MComponent component;
	private List<EPackage> results;

	@Before
	public void tearUp() throws IOException, CoreException {
		component = loadComponent("test-2.mcore");
		results = getResults(component);
	}
	
	@Test
	public void testClassReferences() {
		EPackage actual = results.get(0);
		EClassifier a = actual.getEClassifier("A");
		assertNotNull(a);
		assertTrue(a instanceof EClass);
		
		EClassifier b = actual.getEClassifier("B");
		assertNotNull(b);
		assertTrue(b instanceof EClass);
		
		EClass aC = (EClass) a;
		
		EStructuralFeature _bs = aC.getEStructuralFeature("bs");
		assertNotNull(_bs);
		assertTrue(_bs instanceof EReference);
		assertEquals(b, _bs.getEType());
		assertEquals(0, _bs.getLowerBound());
		assertEquals(-1, _bs.getUpperBound());
		
		EStructuralFeature _oneB = aC.getEStructuralFeature("oneB");
		assertNotNull(_oneB);
		assertTrue(_oneB instanceof EReference);
		assertEquals(b, _oneB.getEType());
		assertEquals(0, _oneB.getLowerBound());
		assertEquals(1, _oneB.getUpperBound());
	}
	
}
