package com.montages.mcore.tests.mcore2ecore;

import org.junit.Ignore;

import com.montages.mcore.tests.support.TestSupport;

@Ignore
public class OverrideTest extends TestSupport {

//	@Test
//	public void testOverride() throws CoreException {
//		ResourceSet rs = new ResourceSetImpl();
//
//		List<Resource> results = new Mcore2Ecore().transform(rs, createTestOverrideModel(rs));
//
//		assertEquals(2, results.size());
//		assertEquals(1, results.get(0).getContents().size());
//		assertEquals(1, results.get(1).getContents().size());
//		assertEquals(EcorePackage.Literals.EPACKAGE, results.get(0).getContents().get(0).eClass());
//		assertEquals(EcorePackage.Literals.EPACKAGE, results.get(1).getContents().get(0).eClass());
//
//		EClass a = (EClass) ((EPackage) results.get(0).getContents().get(0)).getEClassifier("A");
//
//		assertNotNull(a);
//		assertNotNull(a.getEStructuralFeature("bs"));
//		assertTrue(a.getEStructuralFeature("bs").isDerived());
//
//		testDerivedAnnotation(a.getEStructuralFeature("bs"), "OrderedSet{}");
//
//		EClass d = (EClass) ((EPackage) results.get(1).getContents().get(0)).getEClassifier("D");
//
//		assertNotNull(d);
//		assertTrue(d.getESuperTypes().contains(a));
//		assertEquals(2, d.getEAllStructuralFeatures().size());
//		testOverrideAnnotation(d, "bs", "bbs");
//	}
//	
//	@Test
//	public void testOverrideNoSuperResult() throws CoreException {
//		ResourceSet rs = new ResourceSetImpl();
//
//		List<Resource> results = new Mcore2Ecore().transform(rs, createTestOverrideNoSuperResult(rs));
//
//		assertEquals(2, results.size());
//		assertEquals(1, results.get(0).getContents().size());
//		assertEquals(1, results.get(1).getContents().size());
//		assertEquals(EcorePackage.Literals.EPACKAGE, results.get(0).getContents().get(0).eClass());
//		assertEquals(EcorePackage.Literals.EPACKAGE, results.get(1).getContents().get(0).eClass());
//
//		EClass a = (EClass) ((EPackage) results.get(0).getContents().get(0)).getEClassifier("A");
//
//		assertNotNull(a);
//		assertNotNull(a.getEStructuralFeature("bs"));
//		assertTrue(a.getEStructuralFeature("bs").isDerived());
//
//		testDerivedAnnotation(a.getEStructuralFeature("bs"), "OrderedSet{}");
//
//		EClass d = (EClass) ((EPackage) results.get(1).getContents().get(0)).getEClassifier("D");
//
//		assertNotNull(d);
//		assertTrue(d.getESuperTypes().contains(a));
//		assertEquals(2, d.getEAllStructuralFeatures().size());
//		testOverrideAnnotation(d, "bs", "bbs");
//	}
//
//	@Test
//	public void testOverrideNoSuperResultSingleSimpleType() throws CoreException {
//		ResourceSet rs = new ResourceSetImpl();
//
//		List<Resource> results = new Mcore2Ecore().transform(rs, createTestOverrideNoSuperResultSingleSimple(rs));
//
//		assertEquals(2, results.size());
//		assertEquals(1, results.get(0).getContents().size());
//		assertEquals(1, results.get(1).getContents().size());
//		assertEquals(EcorePackage.Literals.EPACKAGE, results.get(0).getContents().get(0).eClass());
//		assertEquals(EcorePackage.Literals.EPACKAGE, results.get(1).getContents().get(0).eClass());
//
//		EClass a = (EClass) ((EPackage) results.get(0).getContents().get(0)).getEClassifier("A");
//
//		assertNotNull(a);
//		assertNotNull(a.getEStructuralFeature("test"));
//		assertTrue(a.getEStructuralFeature("test").isDerived());
//
//		testDerivedAnnotation(a.getEStructuralFeature("test"), "let e:String = null in e");
//
//		EClass d = (EClass) ((EPackage) results.get(1).getContents().get(0)).getEClassifier("D");
//
//		assertNotNull(d);
//		assertTrue(d.getESuperTypes().contains(a));
//		assertEquals(2, d.getEAllStructuralFeatures().size());
//		testOverrideAnnotation(d, "test", "'Hello'");
//		
//		Diagnostic diagnostic = Diagnostician.INSTANCE.validate(a);
//		assertEquals(Diagnostic.OK, diagnostic.getSeverity());
//
//		diagnostic = Diagnostician.INSTANCE.validate(a.getEStructuralFeature("test"));
//		assertEquals(Diagnostic.OK, diagnostic.getSeverity());
//	}
//
//	private void testDerivedAnnotation(EStructuralFeature feature, String expected) {
//		EAnnotation annotation = feature.getEAnnotation("http://www.xocl.org/OCL");
//		assertNotNull(annotation);
//		assertTrue(annotation.getDetails().containsKey("derive"));
//		assertEquals(expected, annotation.getDetails().get("derive"));
//	}
//
//	private void testOverrideAnnotation(EClass eClass, String feature, String expected) {
//		EAnnotation annotation = eClass.getEAnnotation("http://www.xocl.org/OVERRIDE_OCL");
//		assertNotNull(annotation);
//		assertTrue(annotation.getDetails().containsKey(feature + "Derive"));
//		assertEquals(expected, annotation.getDetails().get(feature + "Derive"));
//	}
//
//	private MComponent[] createTestOverrideModel(ResourceSet rs) {
//		MFluent f = new MFluent();
//
//		Resource res1 = rs.createResource(URI.createURI("m1.mcore"));
//		Resource res2 = rs.createResource(URI.createURI("m2.mcore"));
//
//		MFluentComp c1 = f.createComponent(res1, "c1", "test", "org")
//			.getOrCreate("p1")
//			.addClassifier("A")
//			.addClassifier("B")
//			.getOrCreate("A")
//				.addDerivedProperty("bs", 
//						f.get("c1.p1.B", MFluentClass.class).classifier, 
//						f.createResultAnnotation("OrderedSet{}"))
//				.parent.parent;
//
//		MFluentComp c2 = f.createComponent(res2, "c2", "test", "org")
//				.getOrCreate("p2")
//				.addClassifier("C")
//				.addClassifier("D")
//				.getOrCreate("D")
//					.supertypes(f.get("c1.p1.A", MFluentClass.class))
//					.addProperty("bbs", f.get("c1.p1.B", MFluentClass.class).classifier)
//					.addDerivedProperty(f.get("c1.p1.A.bs", MFluentProp.class).property, 
//							f.get("c1.p1.B", MFluentClass.class).classifier, 
//							f.createResultAnnotation("bbs"))
//				.parent.parent;
//
//		return new MComponent[] {c1.component, c2.component};
//	}
//
//	private MComponent[] createTestOverrideNoSuperResultSingleSimple(ResourceSet rs) {
//		MFluent f = new MFluent();
//
//		Resource res1 = rs.createResource(URI.createURI("m1.mcore"));
//		Resource res2 = rs.createResource(URI.createURI("m2.mcore"));
//
//		MFluentComp c1 = f.createComponent(res1, "c1", "test", "org")
//			.getOrCreate("p1")
//			.addClassifier("A")
//			.addClassifier("B")
//			.getOrCreate("A")
//				.addDerivedProperty("test", 
//						SimpleType.STRING,
//						false,
//						null)
//				.parent.parent;
//
//		MFluentComp c2 = f.createComponent(res2, "c2", "test", "org")
//				.getOrCreate("p2")
//				.addClassifier("C")
//				.addClassifier("D")
//				.getOrCreate("D")
//					.supertypes(f.get("c1.p1.A", MFluentClass.class))
//					.addProperty("bbs", f.get("c1.p1.B", MFluentClass.class).classifier)
//					.addDerivedProperty(f.get("c1.p1.A.test", MFluentProp.class).property, 
//							SimpleType.STRING,
//							false,
//							f.createResultAnnotation("'Hello'"))
//				.parent.parent;
//
//		return new MComponent[] {c1.component, c2.component};
//	}
//
//	private MComponent[] createTestOverrideNoSuperResult(ResourceSet rs) {
//		MFluent f = new MFluent();
//
//		Resource res1 = rs.createResource(URI.createURI("m1.mcore"));
//		Resource res2 = rs.createResource(URI.createURI("m2.mcore"));
//
//		MFluentComp c1 = f.createComponent(res1, "c1", "test", "org")
//			.getOrCreate("p1")
//			.addClassifier("A")
//			.addClassifier("B")
//			.getOrCreate("A")
//				.addDerivedProperty("bs", 
//						f.get("c1.p1.B", MFluentClass.class).classifier, 
//						null)
//				.parent.parent;
//
//		MFluentComp c2 = f.createComponent(res2, "c2", "test", "org")
//				.getOrCreate("p2")
//				.addClassifier("C")
//				.addClassifier("D")
//				.getOrCreate("D")
//					.supertypes(f.get("c1.p1.A", MFluentClass.class))
//					.addProperty("bbs", f.get("c1.p1.B", MFluentClass.class).classifier)
//					.addDerivedProperty(f.get("c1.p1.A.bs", MFluentProp.class).property, 
//							f.get("c1.p1.B", MFluentClass.class).classifier, 
//							f.createResultAnnotation("bbs"))
//				.parent.parent;
//
//		return new MComponent[] {c1.component, c2.component};
//	}
}
