package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.Test;

import com.montages.mcore.MComponent;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.util.ResourceService;

public class UpdateAnnotationTest {

	private final URI baseURI = URI.createURI("platform:/plugin/com.montages.mcore.tests/tests");

	@Test
	public void testAnnotations() throws CoreException, IOException {
		Mcore2Ecore t = new Mcore2Ecore();

		ResourceSet resourceSet = ResourceService.createResourceSet();
		MComponent a = (MComponent) resourceSet.getResource(baseURI.appendSegment("a.mcore"), true).getContents()
				.get(0);
		MComponent b = (MComponent) resourceSet.getResource(baseURI.appendSegment("b.mcore"), true).getContents()
				.get(0);

		t.transform(resourceSet, b);
		t.transform(resourceSet, a);

		EClass classifier = (EClass) a.getOwnedPackage().get(0).getInternalEPackage().getEClassifier("A");

		assertNotNull(classifier.getEStructuralFeature("b").getEType());

		Resource resource = a.getOwnedPackage().get(0).getInternalEPackage().eResource();
		resource.save(System.out, null);

		assertEquals(0, resource.getErrors().size());
	}
}
