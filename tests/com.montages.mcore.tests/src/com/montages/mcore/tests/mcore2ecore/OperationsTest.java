package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.*;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Test;

import com.montages.mcore.SimpleType;
import com.montages.mcore.fluent.MFluent;
import com.montages.mcore.fluent.MFluent.MFluentComp;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.tests.support.TestSupport;

public class OperationsTest extends TestSupport {

	@Test
	public void testClassOperations() throws CoreException {
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.createResource(URI.createURI("test.mcore"));

		MFluent f = new MFluent();
		MFluentComp fc = f.createComponent(r, "test", "foo", "org")
			.getOrCreate("p")
				.getOrCreate("A")
					.addProperty("op1", SimpleType.STRING).get("op1")
						.addSignature().parent
					.addProperty("op2", SimpleType.INTEGER).get("op2")
						.addSignature(f.createParameter("a", SimpleType.STRING, true))
			.parent.parent.parent;

		List<Resource> result = new Mcore2Ecore().transform(rs, fc.component);
		assertEquals(1, result.size());
		assertEquals(1, result.get(0).getContents().size());
		assertEquals(EcorePackage.Literals.EPACKAGE, result.get(0).getContents().get(0).eClass());

		EPackage p = (EPackage) result.get(0).getContents().get(0);

		assertEquals(1, p.getEClassifiers().size());
		EClass c = (EClass) p.getEClassifier("A");
		assertNotNull(c);

		assertEquals(0, c.getEStructuralFeatures().size());
		assertEquals(2, c.getEOperations().size());
	}

}
