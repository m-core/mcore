package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.Ignore;
import org.junit.Test;

import com.montages.mcore.MComponent;
import com.montages.mcore.mcore2ecore.MResource2Resource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.tests.support.TestSupport;

public class ObjectsTest extends TestSupport {

	@Ignore
	@Test
	public void testTwoResourcesWithCrossReferences() throws IOException {
		MComponent component = loadComponent("test-res-refs.mcore");

		ResourceSet rs = component.eResource().getResourceSet();

		assertEquals(1, component.getResourceFolder().size());
		MResourceFolder folder = component.getResourceFolder().get(0);
		assertEquals(2, folder.getResource().size());

		Resource first = rs.createResource(URI.createURI("first.xmi"));
		Resource second = rs.createResource(URI.createURI("second.xmi"));

		MResource2Resource t = new MResource2Resource();
		t.transform(folder.getResource().get(0), first);
		t.transform(folder.getResource().get(1), second);
		t.finish();
		
		assertEquals(1, first.getContents().size());
		assertEquals(1, second.getContents().size());
		
		EObject firstRoot = first.getContents().get(0);
		assertEquals("A", firstRoot.eClass().getName());

		EObject secondRoot = second.getContents().get(0);
		assertEquals("A", secondRoot.eClass().getName());
		
		EClass eClass = firstRoot.eClass();
		EStructuralFeature idF = eClass.getEStructuralFeature("id");
		EStructuralFeature aF = eClass.getEStructuralFeature("a");
		
		assertNotNull(idF);
		assertNotNull(aF);
		
		assertEquals("1", firstRoot.eGet(idF));
		assertEquals("2", secondRoot.eGet(idF));
		assertEquals(secondRoot, firstRoot.eGet(aF));
		assertEquals(firstRoot, secondRoot.eGet(aF));
	}

}
