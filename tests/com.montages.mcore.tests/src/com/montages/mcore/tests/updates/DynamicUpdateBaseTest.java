package com.montages.mcore.tests.updates;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.junit.Rule;
import org.junit.Test;

public class DynamicUpdateBaseTest {

	URI typeURI = URI.createURI("http://www.langlets.org/updateAnnotationsTest01/UpdateAnnotationsTest01");

	@Rule
	public DynamicUpdateFixture fixture = new DynamicUpdateFixture( //
			"updateannotationstest01.ecore", "My.updateannotationstest01");

	@Test
	public void testSetText() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		assertNotEquals("Hello", root.eGet(classifier.getEStructuralFeature("text2")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("text1"), "Hello"));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals("Hello", root.eGet(classifier.getEStructuralFeature("text2")));
	}

	@Test
	public void testSetDate() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		Date now = new Date();
		assertNotEquals(now, root.eGet(classifier.getEStructuralFeature("date2")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("date1"), now));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(now, root.eGet(classifier.getEStructuralFeature("date2")));
	}

	@Test
	public void testSetBoolean() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		assertEquals(Boolean.TRUE, root.eGet(classifier.getEStructuralFeature("flag2")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("flag1"), false));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(Boolean.FALSE, root.eGet(classifier.getEStructuralFeature("flag2")));
	}

	@Test
	public void testSetInteger() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		assertEquals(2, root.eGet(classifier.getEStructuralFeature("number2")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("number1"), 3));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(3, root.eGet(classifier.getEStructuralFeature("number2")));
	}

	@Test
	public void testSetDouble() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		assertEquals(4.7, root.eGet(classifier.getEStructuralFeature("factor2")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("factor1"), 5.7));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(5.7, root.eGet(classifier.getEStructuralFeature("factor2")));
	}

	@Test
	public void testSetSingleReference() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		List<?> children = (List<?>) root.eGet(classifier.getEStructuralFeature("containedObjects"));
		Object selected = root.eGet(classifier.getEStructuralFeature("unaryReference2"));

		assertEquals(children.get(1), selected);

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("unaryReference1"), children.get(0)));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(children.get(0), root.eGet(classifier.getEStructuralFeature("unaryReference2")));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testAddManyAttribute() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		List<Integer> selected = (List<Integer>) root.eGet(classifier.getEStructuralFeature("naryNumber2"));

		assertTrue(selected.isEmpty());

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("naryNumber1Add"), 2));

		assertTrue(command.canExecute());
		command.execute();

		assertThat((List<Integer>) root.eGet(classifier.getEStructuralFeature("naryNumber2")), hasItems(2));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testAddManyReference() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		List<EObject> children = (List<EObject>) root.eGet(classifier.getEStructuralFeature("containedObjects"));
		List<EObject> selected = (List<EObject>) root.eGet(classifier.getEStructuralFeature("naryReference2"));

		assertThat(selected, hasItems(children.get(4), children.get(5)));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("naryReference1Add"), children.get(0)));

		assertTrue(command.canExecute());
		command.execute();

		assertThat((List<EObject>) root.eGet(classifier.getEStructuralFeature("naryReference2")),
				hasItems(children.get(0), children.get(4), children.get(5)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRemoveManyReference() {
		EObject root = fixture.getModel().getContents().get(0);
		EClass classifier = fixture.findClass(typeURI.appendFragment("//UpdateAnnotationsTest01"));

		List<EObject> children = (List<EObject>) root.eGet(classifier.getEStructuralFeature("containedObjects"));
		List<EObject> selected = (List<EObject>) root.eGet(classifier.getEStructuralFeature("naryReference2"));

		assertThat(selected, hasItems(children.get(4), children.get(5)));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, classifier.getEStructuralFeature("naryReference2Remove"), children.get(4)));

		assertTrue(command.canExecute());
		command.execute();

		assertThat((List<EObject>) root.eGet(classifier.getEStructuralFeature("naryReference2")),
				hasItems(children.get(5)));
	}
}
