package com.montages.mcore.tests.updates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.junit.Rule;
import org.junit.Test;

public class DynamicUpdateInheritTest {

	URI typeURI = URI.createURI("http://www.langlets.org/testUpdateInheritance/TestUpdateInheritance");

	@Rule
	public DynamicUpdateFixture fixture = new DynamicUpdateFixture( //
			"testupdateinheritance.ecore", "My.testupdateinheritance");

	@Test
	public void testUpdateExecuteOnTypeA() {
		EObject a = fixture.getModel().getContents().get(0).eContents().get(0);

		assertNull(a.eGet(a.eClass().getEStructuralFeature("aEffect")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(a, a.eClass().getEStructuralFeature("trigger"), true));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(true, a.eGet(a.eClass().getEStructuralFeature("aEffect")));
	}

	@Test
	public void testUpdateExecuteOnTypeB() {
		EObject b = fixture.getModel().getContents().get(0).eContents().get(1);

		assertNull(b.eGet(b.eClass().getEStructuralFeature("aEffect")));

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(b, b.eClass().getEStructuralFeature("trigger"), true));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(true, b.eGet(b.eClass().getEStructuralFeature("aEffect")));
	}

}
