package com.montages.mcore.tests.updates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.junit.Rule;
import org.junit.Test;

public class TriggerIndirectChangeTest {

	@Rule
	public DynamicUpdateFixture fixture = new DynamicUpdateFixture( //
			"triggerindirectchange/mypackage.ecore", "triggerindirectchange/My.mypackage");

	@Test
	public void testUpdateExecuteOnTypeA() {
		EObject root = fixture.getModel().getContents().get(0);
		assertEquals(4, root.eContents().size());

		EObject a4 = root.eContents().get(1);
		EObject a3 = root.eContents().get(2);

		assertEquals("4", a4.eGet(a4.eClass().getEStructuralFeature("objectID")));
		assertEquals("3", a3.eGet(a3.eClass().getEStructuralFeature("objectID")));

		fixture.getEditingDomain()
				.createCommand(SetCommand.class,
						new CommandParameter(a4, a4.eClass().getEStructuralFeature("intendedNewLocation"), a3))
				.execute();

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(a4, a4.eClass().getEStructuralFeature("triggerOC"), true));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(3, root.eContents().size());
		assertEquals(1, a3.eContents().size());

		assertEquals(a4, a3.eContents().get(0));
	}

}
