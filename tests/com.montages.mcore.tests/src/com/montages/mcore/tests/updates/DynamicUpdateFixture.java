package com.montages.mcore.tests.updates;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.junit.rules.ExternalResource;
import org.osgi.framework.Bundle;

import com.montages.mcore.dynamic.DynamicEditingDomainFactory;
import com.montages.mcore.dynamic.DynamicSetup;

public class DynamicUpdateFixture extends ExternalResource {

	private final URI baseURI = URI.createURI("platform:/plugin/com.montages.mcore.tests/tests/updates");

	private AdapterFactoryEditingDomain editingDomain;
	private Resource model;

	private final String ecoreName;
	private final String testName;

	public DynamicUpdateFixture(String ecoreName, String testName) {
		this.ecoreName = ecoreName;
		this.testName = testName;
	}

	public EClass findClass(URI uri) {
		return (EClass) editingDomain.getResourceSet().getEObject(uri, false);
	}

	@Override
	protected void before() throws Throwable {
		Bundle bundle = Platform.getBundle("com.montages.mcore.tests");
		URL url = bundle.getResource("tests/updates/" + testName);

		editingDomain = new DynamicEditingDomainFactory().createEditingDomain();
		model = new DynamicSetup() //
				.withDomain(editingDomain) //
				.withPackages(baseURI.appendSegments(ecoreName.split("/"))) //
				.withInstance(URI.createURI(FileLocator.resolve(url).toURI().toString())) //
				.setup();
	}

	public AdapterFactoryEditingDomain getEditingDomain() {
		return editingDomain;
	}

	public Resource getModel() {
		return model;
	}
}
