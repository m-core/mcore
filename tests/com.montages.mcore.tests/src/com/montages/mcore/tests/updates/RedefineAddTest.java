package com.montages.mcore.tests.updates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.junit.Rule;
import org.junit.Test;

public class RedefineAddTest {

	@Rule
	public DynamicUpdateFixture fixture = new DynamicUpdateFixture( //
			"redefinenary/mypackage.ecore", "redefinenary/My.mypackage");

	@Test
	public void testRedefine() {
		EObject root = fixture.getModel().getContents().get(0);

		assertEquals(0, root.eContents().size());

		{

			Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
					new CommandParameter(root, root.eClass().getEStructuralFeature("numberOfChildrenByRedefine"), 2));

			assertTrue(command.canExecute());
			command.execute();
		}

		assertEquals(2, root.eContents().size());

		{

			Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
					new CommandParameter(root, root.eClass().getEStructuralFeature("numberOfChildrenByRedefine"), 2));

			assertTrue(command.canExecute());
			command.execute();
		}

		assertEquals(2, root.eContents().size());
	}

	@Test
	public void testAdd() {
		EObject root = fixture.getModel().getContents().get(0);

		assertEquals(0, root.eContents().size());

		{

			Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
					new CommandParameter(root, root.eClass().getEStructuralFeature("numberOfChildrenByAdd"), 2));

			assertTrue(command.canExecute());
			command.execute();
		}

		assertEquals(2, root.eContents().size());

		{

			Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
					new CommandParameter(root, root.eClass().getEStructuralFeature("numberOfChildrenByAdd"), 2));

			assertTrue(command.canExecute());
			command.execute();
		}

		assertEquals(4, root.eContents().size());
	}

	@Test
	public void testAddOne() {
		EObject root = fixture.getModel().getContents().get(0);

		assertEquals(0, root.eContents().size());

		{

			Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
					new CommandParameter(root, root.eClass().getEStructuralFeature("addOneChild"), 1));

			assertTrue(command.canExecute());
			command.execute();
		}

		assertEquals(1, root.eContents().size());

		EObject object = root.eContents().get(0);
		assertEquals(1, object.eGet(object.eClass().getEStructuralFeature("number")));

		{

			Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
					new CommandParameter(root, root.eClass().getEStructuralFeature("addOneChild"), 2));

			assertTrue(command.canExecute());
			command.execute();
		}

		assertEquals(2, root.eContents().size());

		object = root.eContents().get(0);
		assertEquals(2, object.eGet(object.eClass().getEStructuralFeature("number")));

		object = root.eContents().get(1);
		assertEquals(1, object.eGet(object.eClass().getEStructuralFeature("number")));
	}

}
