package com.montages.mcore.tests.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Test;

import com.montages.common.CommonPlugin;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.tests.support.TestSupport;
import com.montages.mcore.util.McoreResourceFactoryImpl;

public class McoreResourceTest extends TestSupport {

	@Test
	public void testSaveResource() throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().put("mcore", new McoreResourceFactoryImpl());
		
		Map<URI, URI> map = new HashMap<URI, URI>();
		CommonPlugin.computePluginModels(map);
		resourceSet.getURIConverter().getURIMap().putAll(map);

		MComponent c = createComponent("foo", "bar", "org");
		MPackage p = createPackage(c, "p");
		MClassifier a = createClassifier(p, "A");
		MClassifier string = find(getCoreData(resourceSet), "String");
		createProperty(a, "string", string);

//		c.eResource().save(System.out, null);
	}

}
